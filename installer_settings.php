<?php
/**
 * Global flag to indicate the site is in installation mode.
 */

error_reporting(0);

defined('DS') OR define('DS', DIRECTORY_SEPARATOR);

define('ROOT_PATH', rtrim($_SERVER['DOCUMENT_ROOT'], "/"));
define('ROOT_HOST', rtrim("http://" . $_SERVER['HTTP_HOST'], "/"));

//define('TEMP_FOLDER', str_replace("\\", "/", dirname(dirname(dirname(__FILE__)))));
define('TEMP_FOLDER', str_replace("\\", "/", dirname((__FILE__))));
define('SITE_FOLDER', ltrim(str_replace(ROOT_PATH, "", TEMP_FOLDER), "/"));

define('SITE_PATH', ROOT_PATH . DS . SITE_FOLDER . DS);
define('SITE_URL', ROOT_HOST . "/" . SITE_FOLDER . "/");
define('SECURE_URL', ROOT_HOST . "/" . SITE_FOLDER . "/");

$site_path = SITE_PATH;
$site_url = SITE_URL;

$installer_path = SITE_PATH."installer".DS;
$installer_url = SITE_URL."installer/";

$installer_image_path = $installer_path."assets".DS."images".DS;
$installer_image_url = $installer_url."assets/images/";

$installer_sql_path = SITE_PATH."installer".DS."assets".DS."mysql".DS."project.sql";
$installer_pgsql_path = SITE_PATH."installer".DS."assets".DS."mysql".DS."migrate_pgsql.sql";
$installer_sqlsrv_path = SITE_PATH."installer".DS."assets".DS."mysql".DS."migrate_sqlsrv.sql";


define('REQUIREMENT_INFO', -1);

/**
 * Requirement severity -- Requirement successfully met.
 */
define('REQUIREMENT_OK', 0);

/**
 * Requirement severity -- Warning condition; proceed but flag warning.
 */
define('REQUIREMENT_WARNING', 1);

/**
 * Requirement severity -- Error condition; abort installation.
 */
define('REQUIREMENT_ERROR', 2);

/**
 * File permission check -- File exists.
 */
define('FILE_EXIST', 1);

/**
 * File permission check -- File is readable.
 */
define('FILE_READABLE', 2);

/**
 * File permission check -- File is writable.
 */
define('FILE_WRITABLE', 4);

/**
 * File permission check -- File is executable.
 */
define('FILE_EXECUTABLE', 8);

/**
 * File permission check -- File does not exist.
 */
define('FILE_NOT_EXIST', 16);

/**
 * File permission check -- File is not readable.
 */
define('FILE_NOT_READABLE', 32);

/**
 * File permission check -- File is not writable.
 */
define('FILE_NOT_WRITABLE', 64);

/**
 * File permission check -- File is not executable.
 */
define('FILE_NOT_EXECUTABLE', 128);
define('MYSQL_VERSION', "5.3.0");
define('PGSQL_VERSION', "8.0");
define('SQLSRV_VERSION', "10.0");
define('INSTALLER_PHP_VERSION', "5.3.0");

if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') 
{
        // for windows
        define('IMAGE_MAGIC_INSTALL_DIR', "C:/wamp/bin/php/php5.4.16/");
}
else if (strtoupper(substr(PHP_OS, 0, 3)) === 'DAR') 
{
        // For MAC
        define('IMAGE_MAGIC_INSTALL_DIR', "/usr/local/bin");
}
else 
{
        // for linux
        define('IMAGE_MAGIC_INSTALL_DIR', "/usr/bin");    
}

/* End of file installer_settings.php */
/* Location: ./installer_settings.php */


