<?php
if (!function_exists('text2image')) {

    /**
     * text2image function
     *
     * @param	array	$params	String to print, font size, font file, font color etc..
     * 
     */
    function text2image($params = array())
    {
        /* initialize parameters */
        $text = $params['text'];
        $fontSize = !empty($params['font_size']) ? $params['font_size'] : 10;
        $font_file = !empty($params['font_file']) ? $params['font_file'] : "public/styles/fonts/roboto-light-webfont.ttf";
        $font_color = !empty($params['font_color']) ? $params['font_color'] : "#000";
        $upload_path = !empty($params['upload_path']) ? $params['upload_path'] : "application/cache/temp/";
        $blur_intensity = !empty($params['blur_intensity']) ? $params['blur_intensity'] : null;

        $size = imageTTFBBox($fontSize, 0, $font_file, $text);
        $im = imageCreateTrueColor(abs($size[4]) + abs($size[0]) + 10, abs($size[5]) + abs($size[1]) + 10);
        imageSaveAlpha($im, true);
        ImageAlphaBlending($im, false);
        $transparentColor = imagecolorallocatealpha($im, 200, 200, 200, 127);
        imagefill($im, 0, 0, $transparentColor);


        /* calculate Rgb val for font color */
        if (substr($font_color, 0, 1) == "#") {
            $font_color = substr($font_color, 1);
        }
        $R = substr($font_color, 0, 2);
        $G = substr($font_color, 2, 2);
        $B = substr($font_color, 4, 2);
        $R = hexdec($R);
        $G = hexdec($G);
        $B = hexdec($B);

        $text_color = imagecolorallocate($im, $R, $G, $B); //black text
        imagettftextblur($im, $fontSize, 0, 0, 20, $text_color, $font_file, $text, $blur_intensity);

        $image_name = md5($text) . '.png';

        imagepng($im, $upload_path . '/' . $image_name);
        return $upload_path . '/' . $image_name;
    }
}

if (!function_exists('imagettftextblur')) {

    /**
     * imagettftextblur function
     *
     * @return	string
     */
    function imagettftextblur(&$image, $size, $angle, $x, $y, $color, $fontfile, $text, $blur_intensity = null)
    {
        $blur_intensity = !is_null($blur_intensity) && is_numeric($blur_intensity) ? (int) $blur_intensity : 0;
        if ($blur_intensity > 0) {
            $text_shadow_image = imagecreatetruecolor(imagesx($image), imagesy($image));
            imagefill($text_shadow_image, 0, 0, imagecolorallocate($text_shadow_image, 0x00, 0x00, 0x00));
            imagettftext($text_shadow_image, $size, $angle, $x, $y, imagecolorallocate($text_shadow_image, 0xFF, 0xFF, 0xFF), $fontfile, $text);
            for ($blur = 1; $blur <= $blur_intensity; $blur++)
                imagefilter($text_shadow_image, IMG_FILTER_GAUSSIAN_BLUR);
            for ($x_offset = 0; $x_offset < imagesx($text_shadow_image); $x_offset++) {
                for ($y_offset = 0; $y_offset < imagesy($text_shadow_image); $y_offset++) {
                    $visibility = (imagecolorat($text_shadow_image, $x_offset, $y_offset) & 0xFF) / 255;
                    if ($visibility > 0)
                        imagesetpixel($image, $x_offset, $y_offset, imagecolorallocatealpha($image, ($color >> 16) & 0xFF, ($color >> 8) & 0xFF, $color & 0xFF, (1 - $visibility) * 127));
                }
            }
            imagedestroy($text_shadow_image);
        } else {
            return imagettftext($image, $size, $angle, $x, $y, $color, $fontfile, $text);
        }
    }
}

if (!function_exists('pr')) {

    /**
     * pr function
     *
     * @param	string	$data	String to print
     * @param	int	$num	Number to exit
     */
    function pr($var, $ex = 0)
    {
        echo "<pre>";

        print_r($var);

        echo "</pre>";

        echo get_caller_method();

        if ($ex == 1) {
            exit;
        }
    }
}

// ------------------------------------------------------------------------

if (!function_exists('get_caller_method')) {

    /**
     * get_caller_method function
     *
     * @return	string
     */
    function get_caller_method()
    {
        $traces = debug_backtrace();

        if (isset($traces[2])) {
            return $traces[2]['function'];
        }

        return null;
    }
}
