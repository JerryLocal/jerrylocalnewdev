<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* load the MX_Loader class */

//require APPPATH . "third_party/MX/Loader.php";

class HB_Loader extends MX_Loader {

    public function database($params = '', $return = FALSE, $query_builder = NULL) {
        // Grab the super object
        $CI = & get_instance();

        // Do we even need to load the database class?
        if ($return === FALSE && $query_builder === NULL && isset($CI->db) && is_object($CI->db) && !empty($CI->db->conn_id)) {
            return FALSE;
        }

        require_once(APPPATH . 'database/DB.php');

        if ($return === TRUE) {
            return HB_DB($params, $query_builder);
        }

        // Initialize the db variable. Needed to prevent
        // reference errors with some configurations
        $CI->db = '';

        // Load the DB class
        $CI->db = & HB_DB($params, $query_builder);
        return $this;
    }

    /** Load a module model * */
    public function model($model, $object_name = NULL, $connect = FALSE) {
        if (is_array($model))
            return $this->models($model);

        ($_alias = $object_name) OR $_alias = basename($model);

        if (in_array($_alias, $this->_ci_models, TRUE))
            return $this;

        /* check module */
        list($path, $_model) = Modules::find(strtolower($model), $this->_module, 'models/');

        if ($path == FALSE) {
            /* check application & packages */
            parent::model($model, $object_name, $connect);
        } else {
            class_exists('CI_Model', FALSE) OR load_class('Model', 'core');

            if ($connect !== FALSE && !class_exists('CI_DB', FALSE)) {
                if ($connect === TRUE)
                    $connect = '';
                $this->database($connect, FALSE, TRUE);
            }
            Modules::load_file($_model, $path);

            // Related to extending current model from above model... releted to CIT operations.
            if (CI::$APP->config->item('cu_model_prx') != "" && file_exists($path . ucfirst(CI::$APP->config->item('cu_model_prx')) . $_model . ".php")) {
                $_model = ucfirst(CI::$APP->config->item('cu_model_prx')) . $_model;
                Modules::load_file($_model, $path);
            }

            $model = ucfirst($_model);
            CI::$APP->$_alias = new $model();

            $this->_ci_models[] = $_alias;
        }
        return $this;
    }

}

/* End of file HB_Loader.php */
/* Location: ./application/core/HB_Loader.php */