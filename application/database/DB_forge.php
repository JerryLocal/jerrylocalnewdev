<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once BASEPATH . 'database/DB_forge.php';

abstract class HB_DB_forge extends CI_DB_forge {
    
}

/* End of file DB_forge.php */
/* Location: ./application/database/DB_forge.php */

