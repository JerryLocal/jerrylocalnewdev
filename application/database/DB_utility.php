<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once BASEPATH . 'database/DB_utility.php';

abstract class HB_DB_utility extends CI_DB_utility {
    
}

/* End of file DB_utility.php */
/* Location: ./application/database/DB_utility.php */
