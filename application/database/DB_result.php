<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once BASEPATH . 'database/DB_result.php';

class HB_DB_result extends CI_DB_result {

    /**
     * Query result.  "array" version.
     *
     * @access	public
     * @return	array
     */
    function result_single_array() {
        if (count($this->result_single_array) > 0)
		{
            return $this->result_single_array;
        }

		// In the event that query caching is on, the result_id variable
		// will not be a valid resource so we'll simply return an empty
		// array.
		if ( ! $this->result_id OR $this->num_rows === 0)
		{
            return array();
        }

        is_null($this->row_data) OR $this->data_seek(0);
        while ($row = $this->_fetch_assoc()) 
        {
            $this->result_single_array[] = $row['single_col'];
        }

        return $this->result_single_array;
    }

    // --------------------------------------------------------------------

    /**
     * Query result.  "array" version.
     *
     * @access	public
     * @return	array
     */
    function result_combo_array() {
        if (count($this->result_combo_array) > 0)
		{
            return $this->result_combo_array;
        }

		// In the event that query caching is on, the result_id variable
		// will not be a valid resource so we'll simply return an empty
		// array.
		if ( ! $this->result_id OR $this->num_rows === 0)
		{
            return array();
        }

        is_null($this->row_data) OR $this->data_seek(0);
        while ($row = $this->_fetch_assoc()) 
        {
            $this->result_combo_array[$row['combo1']] = $row['combo2'];
        }

        return $this->result_combo_array;
    }

    /**
     * Query result.  "array" version.
     *
     * @access	public
     * @return	array
     */
    function result_assoc_array($_assoc_field) {
        if (count($this->result_assoc_array) > 0)
		{
            return $this->result_assoc_array;
        }

		// In the event that query caching is on, the result_id variable
		// will not be a valid resource so we'll simply return an empty
		// array.
		if ( ! $this->result_id OR $this->num_rows === 0)
		{
            return array();
        }

        is_null($this->row_data) OR $this->data_seek(0);
        while ($row = $this->_fetch_assoc()) 
        {
            $this->result_assoc_array[$row[$_assoc_field]][] = $row;
        }
        return $this->result_assoc_array;
    }

}

/* End of file DB_result.php */
/* Location: ./application/database/DB_result.php */