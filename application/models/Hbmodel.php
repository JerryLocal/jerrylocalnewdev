<?php

class Hbmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }


    function getData($ws_name , $array) {
    	$obj = modules::load('webservice/'.strtolower($ws_name));
		$data = $obj->handler($array,true);
		return $data;
    }

}

