<?php

class Emailer extends CI_Model {

    public $ids_arr = array();
    public $num_totrec = "";
    protected $module_array = array();
    public $main_table = "";
    public $primary_key = "";

    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->main_table = "mod_system_email";
    }

    public function getEmailContent($vEmailCode = 'MEMBER_REGISTER') {
        $this->db->select('iEmailTemplateId, vEmailCode, vEmailTitle, vFromName, vFromEmail, vBccEmail, eEmailFormat, vEmailSubject, tEmailMessage, vEmailFooter, eStatus');
        $this->db->from($this->main_table);
        $this->db->where("vEmailCode", $vEmailCode);
        $emailDataObj = $this->db->get();
        $emailData = is_object($emailDataObj) ? $emailDataObj->result_array() : array();
        return $emailData;
    }

    public function send_mail($data, $type = "MEMBER_REGISTER") {

        $emailData = $this->getEmailContent($type);
        $tEmailMessage = $emailData[0]['tEmailMessage'];
        $vEmailSubject = $emailData[0]['vEmailSubject'];
        switch ($type) {
            case "MEMBER_REGISTER":
                $emailencry = base64_encode($data['vEmail']);
                $comfirmUrl = site_url() . 'confirm.html?user=' . $emailencry;
                $findarray = array("#COMPANY_NAME#", "#NAME#", "#USERNAME#", "#PASSWORD#", "#EMAIL#", "#CONFIRM_URL#");
                $replacearray = array('Company Name', $data['vName'], $data['vUserName'], $data['vPassword'], $data['vEmail'], $comfirmUrl);
                break;
            case "FORGOT_PASSWORD":
                $findarray = array("#COMPANY_NAME#", "#vName#", "#vUserName#", "#vPassword#", "#vEmail#",'#LOGIN_URL#','#SITE_URL#');
                $replacearray = array('Company Name', $data['vName'], $data['vUserName'], $data['vPassword'], $data['vEmail'], $this->config->item('site_url'), $this->config->item('site_url'));
                break;
            case "FRONT_FORGOT_PASSWORD":
                $findarray = array("#COMPANY_NAME#", "#vName#", "#vUserName#", "#vPassword#", "#vEmail#",'#LOGIN_URL#','#SITE_URL#');
                $replacearray = array('Company Name', $data['vName'], $data['vUserName'], $data['vPassword'], $data['vEmail'], $this->config->item('site_url')."login.html", $this->config->item('site_url'));
                break;
        }

        $vBody = str_replace($findarray, $replacearray, $tEmailMessage);
        $subject = str_replace($findarray, $replacearray, $vEmailSubject);
        
        $this->load->library('email');
        $this->email->from($emailData[0]['vFromEmail'], 'admin');
        $this->email->to($data['vEmail']);
        $this->email->subject($subject);
        $this->email->message($vBody);
        $success = $this->email->send();
        //echo $this->email->print_debugger();die;
        return $success;
    }

}

