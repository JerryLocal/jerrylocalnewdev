<?php echo $this->js->add_js('admin/bootstrap.min.js'); ?>
<?php echo $this->css->add_css('admin/bootstrap.min.css'); ?>  
<link rel="stylesheet" media="print" href="<?php echo $this->config->item('site_url') . 'public/styles/print_css.css'; ?>">

<style type="text/css">
    h1.invoice_heading{padding-bottom:28px !important;}
    .address, .phone, .gts, .invoice_date, .invoice_number  {margin-bottom:10px;font-weight:normal;font-style:normal;text-decoration:none;color:#333333;}
    .name{margin-bottom: 1px;}
</style>
<div class="headingfix">
    <div class="heading" style="border: none; border-bottom: solid 1px #c1c1c1; margin: 0 1%; width: 98% !important; height: auto;" id="top_heading_fix"> <!--<h3 style="margin-top:0px;">Shipping Order :: Invoice </h3>-->
        <h3 style="padding: 5px 0; margin: 0; text-align:right; width:100%;">ORDER DETAIL</h3>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing" style="font-size:14px;">
        <div id="shippedorderlisting" class="frm-view-block frm-stand-view">
            <div class="container">
                <!--<h1 align="center" class="invoice_heading">TAX INVOICE</h1>-->
                <div class="row">
                    <table width="100%" border="0" class="invoice_seller_buyer">
                        <tr>
                            <td scope="col" width="33%" valign="top">
                                <table width="100%">
                                    <tr>
                                        <th scope="col" align="left"><strong style="font-size:18px; line-height:26px;">Order Id : <?php echo $invoice_arr['mo_order_number_pre']; ?><?php echo $invoice_arr['mo_order_number']; ?></th>
                                    </tr>
                                    <tr>
                                        <td><strong>Sub Order Id : <?php echo $invoice_arr['mso_sub_order_number_pre']; ?><?php echo $invoice_arr['mso_sub_order_number']; ?></strong></td>
                                    </tr>	
                                    <tr>
                                        <td><strong>Invoice No : </strong><?php echo $invoice_arr['mso_seller_invoice_no']; ?></td>
                                    </tr>

                                    <tr>
                                        <td><strong>Invoice Date : </strong><?php echo $invoice_arr['mso_seller_invoice_date']; ?></td>
                                    </tr>
                                </table>
                            </td>
                            <td scope="col" width="33%" valign="top">
                                <table width="100%">
                                    <tr>
                                        <th scope="col" width="50%" align="left"><strong style="font-size:18px; line-height:26px;">SELLER</strong></th>
                                    </tr>
                                    <tr>
                                        <td><strong><?php echo $invoice_arr['msd_store_name']; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $invoice_arr['msd_address_seller']; ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Phone Number : </strong><?php echo $invoice_arr['msd_contact_number']; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;"><strong>Email : </strong><a href="mailto:<?php echo $invoice_arr['ma_seller_email']; ?>"><?php echo $invoice_arr['ma_seller_email']; ?></a> </td>
                                    </tr>
                                    <?php 
                                    if(!empty($invoice_arr['msd_g_s_t_no'])){
                                    $gst = substr_replace($invoice_arr['msd_g_s_t_no'], "-", 3, 0); ?>
                                    <tr>
                                        <td><strong>GST Number : </strong><?php echo substr_replace($gst, "-", 7, 0); ?></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </td>
                            <td scope="col" width="34%" style="text-align:left;" valign="top">
                                <table width="100%">
                                    <tr>
                                        <th scope="col" width="50%" style="text-align:left;"><strong style="font-size:18px; line-height:26px;">BUYER</strong></th>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;"><strong><?php echo $invoice_arr['mo_buyer_name']; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;"><?php echo $invoice_arr['mo_buyer_address1']; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;"><strong>Phone Number : </strong><?php echo $invoice_arr['mo_buyer_phone']; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;"><strong>Email : </strong><a href="mailto:<?php echo $invoice_arr['mo_buyer_email']; ?>"><?php echo $invoice_arr['mo_buyer_email']; ?></a> </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div class="row" style="margin-top:30px;">
                        <div class="product_detail">
                            <!--<h2 align="center">Product Details</h2>-->
                            <div class="product_table" style="border:none; background: #c3c3c3;">
                                <table width="100%" border="0">
                                    <tr>
                                        <th scope="col" style="text-align:left; background:#e4e4e4; padding:4px;">Product</th>
                                        <!--<th scope="col" style="text-align:left; background:#e4e4e4; padding:4px;">Product SKU</th>-->
                                        <th scope="col" style="text-align:right; background:#e4e4e4; padding:4px;">Qty</th>
                                        <th scope="col" style="text-align:right; background:#e4e4e4; padding:4px;">Unit Price</th>    
                                        <!--<th scope="col" style="text-align:left; background:#e4e4e4;">Shipping Cost</th>-->    
                                        <th scope="col" style="text-align:right; background:#e4e4e4; padding:4px;">Total</th>
                                    </tr>
                                    <tr class="bg_tr" style="background: #fff;">
                                        <td style="padding:4px;"><?php echo $invoice_arr['mso_product_name']; ?></td>
                                        <!--<td style="padding:4px;"><%$vProductSku%></td>-->
                                        <td style="padding:4px; text-align: right;"><?php echo $invoice_arr['mso_product_qty']; ?></td>
                                        <td style="padding:4px; text-align: right;"><?php echo $this->config->item('CURRENCY_SYMBOL') . number_format($invoice_arr['mso_product_sale_price'], 2) ?></td>
                                        <!--<td><%$this->general->get_formated_currency_details($fShippingCost)%></td>-->
                                        <td style="padding:4px; text-align: right;"><?php echo $this->config->item('CURRENCY_SYMBOL') . number_format($invoice_arr['mso_total_cost'], 2) ?></td>
                                    </tr>	
                                </table>
                            </div>
                            <table width="100%" border="0" style="float: right; margin: 15px 0; font-size: 16px; color: #717171; text-align: right;">
                                <tr class="bg_tr">
                                    <td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td width="34%" style="text-align:left;">Sub Total</td>
                                    <td style="text-align:right;"><?php echo $this->config->item('CURRENCY_SYMBOL') . number_format($invoice_arr['mso_product_sale_price'], 2) ?></td>
                                </tr>
                                <tr class="bg_tr">
                                    <td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="text-align:left;">Shipping</td>
                                    <td style="text-align:right;"><?php echo $this->config->item('CURRENCY_SYMBOL') . number_format($invoice_arr['mso_shipping_cost'], 2) ?></td>
                                </tr>	
                                <tr class="bg_tr">
                                    <td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="color:#99153b; font-size:16px; text-align:left;">Total (Incl GST at rate Shown)</td>
                                    <td style="color:#99153b; font-size:16px; text-align: right;"><?php echo $this->config->item('CURRENCY_SYMBOL') . number_format($invoice_arr['mso_total_cost'], 2) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
<!--
                    <div class="row">
                        <div class="prise">
                            <h2 style="font-size: 13px; font-weight: bold; font-style: normal; text-decoration: none;  color: #333333; text-align: right;">This invoice is issued by JerryLocal Ltd on behalf of the Seller</h2>
                        </div>
                    </div>
-->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Old invoice format -->
<!--
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout"></div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="shippedorderlisting" class="frm-view-block frm-stand-view">
            <div class="container">
                <h1 align="center" class="invoice_heading">TAX INVOICE</h1>
                <div class="row">
                    <table width="100%">
                        <tr>
                            <td width="50%" valign="top">
                                <div class="seller fleft">                                   
                                    <div class="main"><h5><strong>SELLER</strong><h5></div>
                                                <div class="clear"></div>
                                                <div class="name"><?php echo $invoice_arr['msd_store_name']; ?></div>
                                                <div class="address"><?php echo $invoice_arr['msd_address_seller']; ?></div>
                                                <div class=""><span>Phone Number</span> : <?php echo $invoice_arr['msd_contact_number']; ?></div>
                                                <?php $gst = substr_replace($invoice_arr['msd_g_s_t_no'], "-", 3, 0); ?>
                                                <div class=""><span>GST Number</span> : <?php echo substr_replace($gst, "-", 7, 0); ?></div>
                                                <div class=""><span>Invoice Date</span> : <?php echo $invoice_arr['mso_seller_invoice_date']; ?></div>
                                                <div class=""><span>Invoice No</span> : <?php echo $invoice_arr['mso_seller_invoice_no']; ?></div>
                                                </div>
                                                </td>
                                                <td width="50%" valign="top">
                                                    <div class="buyer fright">
                                                        <div class="main"><h5><strong>BUYER</strong></h5></div>
                                                        <div class="clear"></div>
                                                        <div class="name"><?php echo $invoice_arr['mo_buyer_name']; ?></div>
                                                        <div class="address"><?php echo $invoice_arr['mo_buyer_address1']; ?></div>
                                                        <div class="phone"><span>Phone Number</span> : <?php echo $invoice_arr['mo_buyer_phone']; ?></div>
                                                        <div class="invoice_date"><span>Email</span> : <?php echo $invoice_arr['mo_buyer_email']; ?></div>
                                                    </div>
                                                </td>
                                                </tr>
                                                </table>
                                                <div class="clear"></div>
                                                </div>
                <div class="row">
                    <div class="product_detail">
                        <h2>Product Detail</h2>                        
                        <div class="product_table">
                            <table width="100%" border="0" class="data-table">
                                <tr>
                                    <th scope="col" align="left">Product Description</th>
                                    <th scope="col" align="left">Product SKU</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Product Price</th>    
                                    <!-- <th scope="col">Shipping Cost</th>     -->
<!--                                    <th scope="col">Total Price</th>
                                </tr>
                                <tr class="bg_tr">
                                    <td width="40%"><?php echo $invoice_arr['mso_product_name']; ?></td>
                                    <td width="15%"><?php echo $invoice_arr['mp_sku']; ?></td>
                                    <td align="center" width="15%"><?php echo $invoice_arr['mso_product_qty']; ?></td>
                                    <td align="center" width="15%"><?php echo $this->config->item('CURRENCY_SYMBOL') . number_format($invoice_arr['mso_product_sale_price'], 2) ?></td>
                                    <!-- < td align="center"><%$this->config->item('CURRENCY_SYMBOL')%><%number_format($invoice_arr.mso_shipping_cost,2)%></td> -->
<!--                                    <td align="center" width="15%"><?php echo $this->config->item('CURRENCY_SYMBOL') . number_format($invoice_arr['mso_total_cost'], 2) ?></td>
                                </tr>
                                <tr class="bg_tr small">
                                    <td><b>Order No: </b><?php echo $invoice_arr['mo_order_number_pre']; ?><?php echo $invoice_arr['mo_order_number']; ?></td>
                                    <td></td>
                                    <td align="center">Sub Total</td>
                                    <td></td>                                    
                                    <td align="center"><?php echo $this->config->item('CURRENCY_SYMBOL') . number_format($invoice_arr['mso_product_sale_price'], 2) ?></td>
                                </tr>
                                <tr class="bg_tr small">
                                    <td><b>Sub Order No: </b><?php echo $invoice_arr['mso_sub_order_number_pre']; ?><?php echo $invoice_arr['mso_sub_order_number']; ?></td>
                                    <td></td>
                                    <td align="center">Shipping</td>
                                    <td></td>                                    
                                    <td align="center"><?php echo $this->config->item('CURRENCY_SYMBOL') . number_format($invoice_arr['mso_shipping_cost'], 2) ?></td>

                                </tr>	
                                <tr class="bg_tr">
                                    <td colspan="4" align="right">Total Price (Incl GST at rate Shown)</td>
                                    <td align="center"><?php echo $this->config->item('CURRENCY_SYMBOL') . number_format($invoice_arr['mso_total_cost'], 2) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="prise">
                        <h4>This invoice is issued by JerryLocal Ltd on behalf of the Seller</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->