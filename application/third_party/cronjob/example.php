<?php
require_once 'CronExpression.php';
require_once 'FieldInterface.php';
require_once 'AbstractField.php';
require_once 'DayOfMonthField.php';
require_once 'DayOfWeekField.php';
require_once 'FieldFactory.php';
require_once 'HoursField.php';
require_once 'MinutesField.php';
require_once 'MonthField.php';
require_once 'YearField.php';

$cron = Cron\CronExpression::factory('* * * * 6,0');
echo $cron->getNextRunDate()->format('Y-m-d H:i:s');
echo "<hr>";

$cron = Cron\CronExpression::factory('@daily');
$cron->isDue();
echo $cron->getNextRunDate()->format('Y-m-d H:i:s');
echo "<hr>";
echo $cron->getPreviousRunDate()->format('Y-m-d H:i:s');
echo "<hr>";
// Works with complex expressions
$cron = Cron\CronExpression::factory('3-59/15 2,6-12 */15 1 2-5');
echo $cron->getNextRunDate()->format('Y-m-d H:i:s');
echo "<hr>";
// Calculate a run date two iterations into the future
$cron = Cron\CronExpression::factory('@daily');
echo $cron->getNextRunDate(null, 2)->format('Y-m-d H:i:s');
echo "<hr>";
// Calculate a run date relative to a specific time
$cron = Cron\CronExpression::factory('@monthly');
echo $cron->getNextRunDate('2010-01-12 00:00:00')->format('Y-m-d H:i:s');