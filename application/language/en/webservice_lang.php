<?php  

//getMenu1_v1 Messages
    $lang["get_menu1_v1"]["MenuName_required"] = "Please enter a value for the MenuName field." ;
    $lang["get_menu1_v1"]["finish_success_1"] = "Error in geting data " ;
    $lang["get_menu1_v1"]["finish_success"] = "" ;

//setProductReview Messages
    $lang["set_product_review"]["iMstProductsId_required"] = "Please enter a value for the iMstProductsId field." ;
    $lang["set_product_review"]["iLogedUserId_required"] = "Please enter a value for the iLogedUserId field." ;
    $lang["set_product_review"]["vName_required"] = "Please enter a value for the vName field." ;
    $lang["set_product_review"]["vEmail_required"] = "Please enter a value for the vEmail field." ;
    $lang["set_product_review"]["iRate_required"] = "Please enter a value for the iRate field." ;
    $lang["set_product_review"]["tReview_required"] = "Please enter a value for the tReview field." ;
    $lang["set_product_review"]["finish_success_1"] = "Error While inserting the Rating " ;
    $lang["set_product_review"]["finish_success"] = "Thank You for Rating Out Product" ;
    $lang["set_product_review"]["trn_product_rating_finish_success"] = "" ;

//getNewsDetail Messages
    $lang["get_news_detail"]["iNewsId_required"] = "Please enter a value for the iNewsId field." ;
    $lang["get_news_detail"]["finish_success"] = "" ;

//viewCounter Messages
    $lang["view_counter"]["pid_required"] = "Please enter a value for the pid field." ;
    $lang["view_counter"]["mst_products_finish_success_1"] = "There is some issue." ;
    $lang["view_counter"]["mst_products_finish_success"] = "Counter Updated." ;

//my_order Messages
    $lang["my_order"]["vBuyerEmail_required"] = "Please enter a value for the vBuyerEmail field." ;
    $lang["my_order"]["mst_order_finish_success"] = "No Data Found" ;
    $lang["my_order"]["finish_success"] = "" ;

//validate product for cart Messages
    $lang["validate_product_for_cart"]["iProductId_required"] = "Please enter a value for the iProductId field." ;
    $lang["validate_product_for_cart"]["iQuantity_required"] = "Please enter a value for the iQuantity field." ;
    $lang["validate_product_for_cart"]["mst_products_finish_success_1"] = "Product detial" ;
    $lang["validate_product_for_cart"]["mst_products_finish_success_3"] = "Store didn't allow to buy  #iQuantity#  quantity at a time." ;
    $lang["validate_product_for_cart"]["mst_products_finish_success_2"] = "Requested quantity or item not in stock!" ;
    $lang["validate_product_for_cart"]["mst_products_finish_success"] = "Requested quantity or item not in stock!" ;

//notification Messages
    $lang["notification"]["iUserId_required"] = "Please enter a value for the iUserId field." ;
    $lang["notification"]["mst_order_finish_success"] = "" ;
    $lang["notification"]["finish_success"] = "" ;

//trackOrder Messages
    $lang["track_order"]["iOrderId_required"] = "Please enter a value for the iOrderId field." ;
    $lang["track_order"]["vEmailId_required"] = "Please enter a value for the vEmailId field." ;
    $lang["track_order"]["vEmailId_email"] = "Please enter valid email address for the vEmailId field." ;
    $lang["track_order"]["mst_order_finish_success_1"] = "" ;
    $lang["track_order"]["mst_order_finish_success"] = "Sub Order Found" ;
    $lang["track_order"]["finish_success"] = "" ;

//getStoreProduct Messages
    $lang["get_store_product"]["iStoreId_required"] = "Please enter a value for the iStoreId field." ;
    $lang["get_store_product"]["mst_products_finish_success"] = "Sorry ,Store is not available" ;
    $lang["get_store_product"]["finish_success"] = "" ;

//getMenu1 Messages
    $lang["get_menu1"]["MenuName_required"] = "Please enter a value for the MenuName field." ;
    $lang["get_menu1"]["finish_success_1"] = "Error in geting data " ;
    $lang["get_menu1"]["finish_success"] = "" ;

//insProductTrend Messages
    $lang["ins_product_trend"]["pid_required"] = "Please enter a value for the pid field." ;
    $lang["ins_product_trend"]["trn_view_count_finish_success"] = "Already Exist." ;
    $lang["ins_product_trend"]["finish_success"] = "View Updated" ;

//getProductBySearch Messages
    $lang["get_product_by_search"]["cid_required"] = "Please enter a value for the cid field." ;
    $lang["get_product_by_search"]["finish_success"] = "" ;

//EmailInvoice Messages
    $lang["email_invoice"]["oid_required"] = "Please enter a value for the oid field." ;
    $lang["email_invoice"]["mst_order_finish_success"] = "Your Mail Send Successfully" ;
    $lang["email_invoice"]["mst_order_finish_success_1"] = "Error while Email you" ;

//getReturnOrderDetail Messages
    $lang["get_return_order_detail"]["iOrderId_required"] = "Please enter a value for the iOrderId field." ;
    $lang["get_return_order_detail"]["finish_success"] = "Success" ;
    $lang["get_return_order_detail"]["mst_sub_order_finish_success"] = "No Order Found" ;

//getProductImages Messages
    $lang["get_product_images"]["pid_required"] = "Please enter a value for the pid field." ;
    $lang["get_product_images"]["finish_success"] = "no image found" ;
    $lang["get_product_images"]["finish_success_1"] = "" ;

//signup Messages
    $lang["signup"]["firstname_required"] = "Please enter a value for the firstname field." ;
    $lang["signup"]["firstname_/^[a-zA-Z ]+$/"] = "Please only enter letters for the firstname field." ;
    $lang["signup"]["lastname_required"] = "Please enter a value for the lastname field." ;
    $lang["signup"]["lastname_/^[a-zA-Z ]+$/"] = "Please only enter letters for the lastname field." ;
    $lang["signup"]["email_required"] = "Please enter a value for the email field." ;
    $lang["signup"]["email_email"] = "Please enter valid email address for the email field." ;
    $lang["signup"]["password_required"] = "Please enter a value for the password field." ;
    $lang["signup"]["password_minlength"] = "Please enter minimum length for the password field." ;
    $lang["signup"]["finish_success_1"] = "There is some problem while creating account." ;
    $lang["signup"]["finish_success_2"] = "Email Already Exist , Please use different Email." ;
    $lang["signup"]["finish_success"] = "Account Created successfully." ;

//getStoreRating Messages
    $lang["get_store_rating"]["iStoreId_required"] = "Please enter a value for the store_id field." ;
    $lang["get_store_rating"]["finish_success"] = "" ;
    $lang["get_store_rating"]["finish_success_1"] = "No Data Found" ;

//getProduct Messages
    $lang["get_product"]["cid_required"] = "Please enter a value for the iProdcutId field." ;
    $lang["get_product"]["finish_success_1"] = "No Record Found" ;
    $lang["get_product"]["finish_success"] = "Record Found" ;

//getStateCity Messages
    $lang["get_state_city"]["state_id_required"] = "Please enter a value for the state_id field." ;
    $lang["get_state_city"]["mod_city_finish_success"] = "" ;

//returnOrder Messages
    $lang["return_order"]["iOrderId_required"] = "Please enter a value for the iOrderId field." ;
    $lang["return_order"]["ePreference_required"] = "Please enter a value for the ePreference field." ;
    $lang["return_order"]["mst_sub_order_finish_success_1"] = "No Order available" ;
    $lang["return_order"]["mst_sub_order_finish_success"] = "Return Request is successfully Added" ;

//getCountryState Messages
    $lang["get_country_state"]["countryid_required"] = "Please enter a value for the countryid field." ;
    $lang["get_country_state"]["finish_success"] = "" ;

//getFilterData Messages
    $lang["get_filter_data"]["catId_required"] = "Please enter a value for the catId field." ;
    $lang["get_filter_data"]["mst_category_options_finish_success"] = "No data found" ;
    $lang["get_filter_data"]["finish_success"] = "Data Found" ;

//cancelOrder Messages
    $lang["cancel_order"]["oid_required"] = "Please enter a value for the oid field." ;
    $lang["cancel_order"]["mst_sub_order_finish_success"] = "Order Cancelled." ;
    $lang["cancel_order"]["mst_sub_order_finish_success_1"] = "Error While Cancelling your order" ;

//GetCategoryBlock_Filter Messages
    $lang["get_category_block_filter"]["cid_required"] = "Please enter a value for the cid field." ;

//signin Messages
    $lang["signin"]["email_required"] = "Please enter a value for the email field." ;
    $lang["signin"]["email_email"] = "Please enter valid email address for the email field." ;
    $lang["signin"]["password_required"] = "Please enter a value for the password field." ;
    $lang["signin"]["mod_customer_finish_success"] = "No user Found" ;
    $lang["signin"]["finish_success"] = "Record Found" ;

//getStoreData Messages
    $lang["get_store_data"]["iStoreId_required"] = "Please enter a value for the iMstStoreDetailId field." ;
    $lang["get_store_data"]["finish_success"] = "Store Data Get. " ;

//getOrderDetail_v1 Messages
    $lang["get_order_detail_v1"]["oid_required"] = "Please enter a value for the oid field." ;
    $lang["get_order_detail_v1"]["iBuyerId_required"] = "Please enter a value for the iBuyerId field." ;
    $lang["get_order_detail_v1"]["mst_order_finish_success"] = "No Order Available" ;
    $lang["get_order_detail_v1"]["finish_success"] = "" ;

//getWishlist Messages
    $lang["get_wishlist"]["iAdminID_required"] = "Please enter a value for the iAdminID field." ;
    $lang["get_wishlist"]["finish_success_1"] = "" ;
    $lang["get_wishlist"]["finish_success"] = "No Record Found" ;

//getProductReview Messages
    $lang["get_product_review"]["iProductID_required"] = "Please enter a value for the iProductID field." ;
    $lang["get_product_review"]["finish_success"] = "successfully review Get" ;
    $lang["get_product_review"]["finish_success_1"] = "No Review Available " ;

//getStoreAvgBlock Messages
    $lang["get_store_avg_block"]["iStoreId_required"] = "Please enter a value for the iStoreId field." ;
    $lang["get_store_avg_block"]["finish_success"] = "" ;

//placeOrder Messages
    $lang["place_order"]["iBuyerId_required"] = "Please enter a value for the iBuyerId field." ;
    $lang["place_order"]["fShippingCost_required"] = "Please enter a value for the fShippingCost field." ;
    $lang["place_order"]["fSubTotal_required"] = "Please enter a value for the fSubTotal field." ;
    $lang["place_order"]["fOrderTotal_required"] = "Please enter a value for the fOrderTotal field." ;
    $lang["place_order"]["ePaymentStatus_required"] = "Please enter a value for the ePaymentStatus field." ;
    $lang["place_order"]["dDate_required"] = "Please enter a value for the dDate field." ;
    $lang["place_order"]["vBuyerEmail_required"] = "Please enter a value for the vBuyerEmail field." ;
    $lang["place_order"]["vBuyerIp_required"] = "Please enter a value for the vBuyerIp field." ;
    $lang["place_order"]["vBuyerName_required"] = "Please enter a value for the vBuyerName field." ;
    $lang["place_order"]["vBuyerAddress1_required"] = "Please enter a value for the vBuyerAddress1 field." ;
    $lang["place_order"]["vBuyerArea_required"] = "Please enter a value for the vBuyerArea field." ;
    $lang["place_order"]["iBuyerCountryId_required"] = "Please enter a value for the iBuyerCountryId field." ;
    $lang["place_order"]["iBuyerCityId_required"] = "Please enter a value for the iBuyerCityId field." ;
    $lang["place_order"]["iBuyerStateId_required"] = "Please enter a value for the iBuyerStateId field." ;
    $lang["place_order"]["vBuyerPinCode_required"] = "Please enter a value for the vBuyerPinCode field." ;
    $lang["place_order"]["vBuyerPhone_required"] = "Please enter a value for the vBuyerPhone field." ;
    $lang["place_order"]["order_item_required"] = "Please enter a value for the order_item field." ;
    $lang["place_order"]["tPaymentDetail_required"] = "Please enter a value for the tPaymentDetail field." ;
    $lang["place_order"]["mst_order_finish_success"] = "Order Failed." ;
    $lang["place_order"]["mst_order_finish_success_1"] = "Order Placed Successfully" ;

//GetLeftFilters Messages
    $lang["get_left_filters"]["catId_required"] = "Please enter a value for the catId field." ;
    $lang["get_left_filters"]["finish_success"] = "Data found" ;
    $lang["get_left_filters"]["finish_success_1"] = "" ;

//test Messages
    $lang["test"]["categoryId_required"] = "Please enter a value for the categoryId field." ;
    $lang["test"]["finish_success"] = "Success." ;

//getFaq Messages
    $lang["get_faq"]["Type_required"] = "Please enter a value for the Type field." ;
    $lang["get_faq"]["finish_success_1"] = "Data Retrieve." ;
    $lang["get_faq"]["finish_success"] = "No Record Found." ;

//setStoreRating Messages
    $lang["set_store_rating"]["storeid_required"] = "Please enter a value for the storeid field." ;
    $lang["set_store_rating"]["description_required"] = "Please enter a value for the description field." ;
    $lang["set_store_rating"]["email_required"] = "Please enter a value for the email field." ;
    $lang["set_store_rating"]["loginuserid_required"] = "Please enter a value for the loginuserid field." ;
    $lang["set_store_rating"]["rating_required"] = "Please enter a value for the rating field." ;
    $lang["set_store_rating"]["iSellerId_required"] = "Please enter a value for the iSellerId field." ;
    $lang["set_store_rating"]["finish_success_1"] = "Error while inserting data " ;
    $lang["set_store_rating"]["finish_success"] = "Data Inserted Successfully" ;

//InviteFriend Messages
    $lang["invite_friend"]["ToEmail_required"] = "Please enter a value for the ToEmail field." ;
    $lang["invite_friend"]["FromEmail_required"] = "Please enter a value for the FromEmail field." ;
    $lang["invite_friend"]["Subject_required"] = "Please enter a value for the Subject field." ;
    $lang["invite_friend"]["finish_success"] = "" ;

//setWishlist Messages
    $lang["set_wishlist"]["iAdminId_required"] = "Please enter a value for the iAdminId field." ;
    $lang["set_wishlist"]["iProductId_required"] = "Please enter a value for the iProductId field." ;
    $lang["set_wishlist"]["finish_success"] = "Your Product is been added in wishlist " ;
    $lang["set_wishlist"]["finish_success_1"] = "Error in Inserting Wishlist" ;
    $lang["set_wishlist"]["trn_product_wishlist_finish_success"] = "Alread Product available in your wishlist" ;

//SetNewsLetter Messages
    $lang["set_news_letter"]["iUserID_required"] = "Please enter a value for the iUserID field." ;
    $lang["set_news_letter"]["vEmail_required"] = "Please enter a value for the field." ;
    $lang["set_news_letter"]["vEmail_email"] = "Please enter valid email address." ;
    $lang["set_news_letter"]["vName_required"] = "Please enter a value for the vName field." ;
    $lang["set_news_letter"]["eStatus_required"] = "Please enter a value for the eStatus field." ;
    $lang["set_news_letter"]["finish_success"] = "Record Inserted." ;
    $lang["set_news_letter"]["finish_success_1"] = "There is some error please try again late." ;

//getNewsByDate Messages
    $lang["get_news_by_date"]["dStartDate_required"] = "Please enter a value for the dStartDate field." ;
    $lang["get_news_by_date"]["dEndDate_required"] = "Please enter a value for the dEndDate field." ;
    $lang["get_news_by_date"]["finish_success"] = "" ;
    $lang["get_news_by_date"]["finish_success_1"] = "No News Available" ;

//getIndividualProduct Messages
    $lang["get_individual_product"]["iProductId_required"] = "Please enter a value for the iProductId field." ;
    $lang["get_individual_product"]["finish_success_1"] = "No Product Found." ;
    $lang["get_individual_product"]["finish_success"] = "" ;

//getRelatedProduct Messages
    $lang["get_related_product"]["iProductId_required"] = "Please enter a value for the iProductId field." ;
    $lang["get_related_product"]["finish_success"] = "Data Found" ;

//getCustomerAddress Messages
    $lang["get_customer_address"]["iCustomerId_required"] = "Please enter a value for the iCustomerId field." ;
    $lang["get_customer_address"]["mst_buyer_address_finish_success_1"] = "Default address" ;
    $lang["get_customer_address"]["mst_buyer_address_finish_success"] = "Please fill your delivery address." ;

//addAddress Messages
    $lang["add_address"]["iAdminId_required"] = "Please enter a value for the iAdminId field." ;
    $lang["add_address"]["vName_required"] = "Please enter a value for the vName field." ;
    $lang["add_address"]["vAddress1_required"] = "Please enter a value for the vAddress1 field." ;
    $lang["add_address"]["vArea_required"] = "Please enter a value for the vArea field." ;
    $lang["add_address"]["iCountryId_required"] = "Please enter a value for the iCountryId field." ;
    $lang["add_address"]["iStateId_required"] = "Please enter a value for the iStateId field." ;
    $lang["add_address"]["iCityId_required"] = "Please enter a value for the iCityId field." ;
    $lang["add_address"]["vPinCode_required"] = "Please enter a value for the vPinCode field." ;
    $lang["add_address"]["vPhone_required"] = "Please enter a value for the vPhone field." ;
    $lang["add_address"]["mst_buyer_address_finish_success"] = "Added successfully." ;

//sendEmailToFriend Messages
    $lang["send_email_to_friend"]["FromEmail_required"] = "Please enter a value for the FromEmail field." ;
    $lang["send_email_to_friend"]["FromEmail_email"] = "Please enter valid email address for the FromEmail field." ;
    $lang["send_email_to_friend"]["FromName_required"] = "Please enter a value for the FromName field." ;
    $lang["send_email_to_friend"]["Subject_required"] = "Please enter a value for the Subject field." ;
    $lang["send_email_to_friend"]["ProductName_required"] = "Please enter a value for the ProductName field." ;
    $lang["send_email_to_friend"]["productUrl_required"] = "Please enter a value for the productUrl field." ;
    $lang["send_email_to_friend"]["finish_success"] = "Success" ;

//orderDetail Messages
    $lang["order_detail"]["iMstOrderId_required"] = "Please enter a value for the iMstOrderId field." ;
    $lang["order_detail"]["mst_order_finish_success"] = "No Order available" ;
    $lang["order_detail"]["finish_success"] = "" ;

//getOrderDetail Messages
    $lang["get_order_detail"]["oid_required"] = "Please enter a value for the oid field." ;
    $lang["get_order_detail"]["oid_number"] = "Please enter valid number for the oid field." ;
    $lang["get_order_detail"]["finish_success_1"] = "" ;
    $lang["get_order_detail"]["finish_success"] = "" ;

//delWishlist Messages
    $lang["del_wishlist"]["iAdminId_required"] = "Please enter a value for the iAdminId field." ;
    $lang["del_wishlist"]["iProductId_required"] = "Please enter a value for the iProductId field." ;
    $lang["del_wishlist"]["finish_success"] = "Product removed from your wishlist." ;
    $lang["del_wishlist"]["trn_product_wishlist_finish_success"] = "Error in deleting your wishlist" ;

//getHomeCategory Messages
    $lang["get_home_category"]["no_child_found"] = "No Child Category Found." ;
    $lang["get_home_category"]["no_main_category_found"] = "No Record Found" ;
    $lang["get_home_category"]["child_categories_found"] = "Child Categories Found" ;

//getProductCategory Messages
    $lang["get_product_category"]["finish_success"] = "Product Found." ;
    $lang["get_product_category"]["finish_success_1"] = "No Record Found" ;

//getDealOfDay Messages
    $lang["get_deal_of_day"]["no_deals"] = "Oops" ;
    $lang["get_deal_of_day"]["has_deals"] = "Product Found." ;

//getWebsiteStatastic Messages
    $lang["get_website_statastic"]["finish_success"] = "" ;

//searchProducts Messages
    $lang["search_products"]["finish_success_search_query"] = "" ;

//GetSitemap Messages
    $lang["get_sitemap"]["finish_success_1"] = "" ;
    $lang["get_sitemap"]["finish_success"] = "" ;

//getNews Messages
    $lang["get_news"]["mst_news_finish_success"] = "" ;
    $lang["get_news"]["finish_success"] = "News Data get Successfully" ;
    $lang["get_news"]["finish_success_1"] = "" ;
    $lang["get_news"]["finish_success_2"] = "" ;

//getMenu Messages
    $lang["get_menu"]["finish_success_1"] = "" ;
    $lang["get_menu"]["finish_success_2"] = "" ;

//quicksearchInner Messages
    $lang["quicksearch_inner"]["mst_products_finish_success_1"] = "" ;
    $lang["quicksearch_inner"]["mst_products_finish_success"] = "" ;

//quicksearchInnerBySort Messages
    $lang["quicksearch_inner_by_sort"]["mst_products_finish_success"] = "No data found" ;
    $lang["quicksearch_inner_by_sort"]["finish_success"] = "" ;

//dealOfTheDay Messages
    $lang["deal_of_the_day"]["trn_dealoftheday_finish_success"] = "No Deal Available" ;
    $lang["deal_of_the_day"]["trn_dealoftheday_finish_success_1"] = "" ;
    $lang["deal_of_the_day"]["trn_dealoftheday_finish_success_2"] = "" ;

//getBanners Messages
    $lang["get_banners"]["finish_success"] = "Record Found." ;

//getMenuTree Messages
    $lang["get_menu_tree"]["finish_success_1"] = "" ;
    $lang["get_menu_tree"]["finish_success"] = "no menu entry found" ;

//getFeatureProduct Messages
    $lang["get_feature_product"]["finish_success_1"] = "No Feature Product available" ;
    $lang["get_feature_product"]["finish_success"] = "" ;

//getCountry Messages
    $lang["get_country"]["mod_country_finish_success"] = "" ;

//product_invoice Messages
    $lang["product_invoice"]["mst_sub_order_finish_success"] = "Error While Generating Your Invoice" ;
    $lang["product_invoice"]["mst_sub_order_finish_success_1"] = "" ;

//test1 Messages
    $lang["test1"]["finish_success"] = "success" ;

//dealOfTheDayByPrice Messages
    $lang["deal_of_the_day_by_price"]["trn_dealoftheday_finish_success_1"] = "" ;
    $lang["deal_of_the_day_by_price"]["finish_success"] = "" ;
    $lang["deal_of_the_day_by_price"]["trn_dealoftheday_finish_success"] = "" ;
    $lang["deal_of_the_day_by_price"]["trn_dealoftheday_finish_success_2"] = "" ;

//getSalesStatistics Messages
    $lang["get_sales_statistics"]["finish_success"] = "" ;

//quicksearch Messages
    $lang["quicksearch"]["mst_products_finish_success"] = "No Result Found" ;
    $lang["quicksearch"]["finish_success"] = "" ;

//getPopularProducts Messages
    $lang["get_popular_products"]["finish_success"] = "Product Found." ;
    $lang["get_popular_products"]["finish_success_1"] = "Random Records Found" ;

//getJobs Messages
    $lang["get_jobs"]["finish_success"] = "available jobs" ;
    $lang["get_jobs"]["finish_success_1"] = "Oops.. system didn't have any job entry" ;