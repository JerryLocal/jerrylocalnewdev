<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

Class PaymentExpress
{
    
    protected $CI;
    function __construct() {
        $this->CI = & get_instance();
        $this->ppx_username =$this->CI->config->item('PAYMENT_EXPRESS_USERNAME');
        $this->ppx_password =$this->CI->config->item('PAYMENT_EXPRESS_PASSWORD');
        $this->ppx_key =$this->CI->config->item('PAYMENT_EXPRESS_KEY');
        $this->wsdl="https://sec.paymentexpress.com/pxf/pxf.svc?wsdl";
        $this->response=array("success"=>1,'messaage'=>'');
    }
    
    public function processPayment($param = array()) {
        
       if($this->getTransactionId($param)){
            if($this->capturePayment($param)){
                $this->getTransaction();
            }
       }
       return $this->response;
    }
    
    public function getTransactionId($param = array()) {
        $data = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://paymentexpress.com">
                <SOAP-ENV:Body>
                <ns1:GetTransactionId>
                <ns1:username>'.$this->ppx_username.'</ns1:username>
                <ns1:password>'.$this->ppx_key.'</ns1:password>
                <ns1:tranDetail>
                    <ns1:amount>'.$param['amount'].'</ns1:amount>
                    <ns1:currency>NZD</ns1:currency>
                    <ns1:enableAddBillCard>false</ns1:enableAddBillCard>
                    <ns1:merchantReference>'.$param['merchantReference'].'</ns1:merchantReference>
                    <ns1:returnUrl>'.$param['returnUrl'].'</ns1:returnUrl>
                    <ns1:txnRef>'.$param['txnRef'].'</ns1:txnRef>
                    <ns1:txnType>Purchase</ns1:txnType>
                </ns1:tranDetail>
                </ns1:GetTransactionId>
                </SOAP-ENV:Body>
                </SOAP-ENV:Envelope>';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->wsdl);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        // SSL security
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: text/xml;charset=\"utf-8\"", "Accept: text/xml", "Cache-Control: no-cache", "Pragma: no-cache", "SOAPAction: \"http://paymentexpress.com/IPxFusion/GetTransactionId\"", "Content-length: " . strlen($data)));
        
        $response = curl_exec($ch);
        
        if (curl_errno($ch)) {
            $this->response['success']=0;
            $this->response['messaage']='Curl error: ' . curl_error($ch) . '. Please contact server administrator.';
            return false;
        }
        
        curl_close($ch);
        
        $xml_parser = xml_parser_create();
        if (!(xml_parse_into_struct($xml_parser, $response, $vals, $index))) {
            $this->response['success']=0;
            $this->response['messaage']="Error while parsing response from PX Fusion. Line " . xml_get_current_line_number($xml_parser) . '. Please contact server administrator.';;
            return false;            
        }
        xml_parser_free($xml_parser);
        
        $parsed_xml = array();

        foreach ($vals as $val) $parsed_xml[$val['tag']] = $val['value'];
        
        if (!$parsed_xml['A:SUCCESS']) {
            $this->response['success']=0;
            $this->response['messaage']='Error! There was a problem getting a transaction id from DPS, please contact the server administrator.';
            return false;            
        } 
        else {
            $this->transactionId   = $parsed_xml["A:TRANSACTIONID"];
            $this->session_id       = $parsed_xml["A:SESSIONID"];
        }

        $this->response['success']=1;
        $this->response['messaage']='';
        $this->response['transactionId']=$this->transactionId;
        $this->response['session_id']=$this->session_id;

        return true;
    }




    private function capturePayment($param=array()){
        $curlPost = array(
            'SessionId' => $this->session_id,
            'Add' => 'Add',
            'CardHolderName' =>  $param['CardHolderName'],
            'CardNumber' =>  $param['CardNumber'],
            'Cvc2' =>  $param['Cvc2'],
            'ExpiryMonth' =>  str_pad($param['ExpiryMonth'], 2, "0", STR_PAD_LEFT),
            'ExpiryYear' =>  $param['ExpiryYear'],
        );        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://sec.paymentexpress.com/pxmi3/pxfusionauth');
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_HEADER, 1); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);            
        $data = curl_exec($ch); 
        curl_close($ch);

        return true;
    }



    private function getTransaction( $try = 0){
            $data = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://paymentexpress.com">
                          <SOAP-ENV:Body>
                            <ns1:GetTransaction>
                              <ns1:username>'.$this->ppx_username.'</ns1:username>
                              <ns1:password>'.$this->ppx_key.'</ns1:password>
                                <ns1:transactionId>' . $this->transactionId . '</ns1:transactionId>
                            </ns1:GetTransaction>
                          </SOAP-ENV:Body>
                        </SOAP-ENV:Envelope>';
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $this->wsdl); 
            curl_setopt($ch, CURLOPT_VERBOSE, 0);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: text/xml;charset=\"utf-8\"", 
                                                            "Accept: text/xml", 
                                                            "Cache-Control: no-cache", 
                                                            "Pragma: no-cache", 
                                                            "SOAPAction: \"http://paymentexpress.com/IPxFusion/GetTransaction\"", 
                                                            "Content-length: ".strlen($data))
            );          
            $response = curl_exec($ch); 
            if(curl_errno($ch)){ 
                $this->response['success']=0;
                $this->response['messaage']='Curl error: ' . curl_error($ch) . '. Please contact server administrator.';
                return false;
                //die( 'Curl error: ' . curl_error($ch) . '. Please contact server administrator.' ); 
            }
            
            curl_close($ch); 
            
            $xml_parser = xml_parser_create(); 
            if(!(xml_parse_into_struct($xml_parser, $response, $vals, $index))){ 
                $this->response['success']=0;
                $this->response['messaage']="Error while parsing response from PX Fusion. Line " . xml_get_current_line_number($xml_parser) . '. Please contact server administrator.' ;
                return false;
            }
            xml_parser_free($xml_parser);
            
            $parsed_xml=array();
            foreach($vals as $val)
                $parsed_xml[$val['tag']]=$val['value'];
            
            if ( ! isset($parsed_xml["STATUS"]) )
            {
                $this->response['success']=0;
                $this->response['messaage']='Error! There was a problem getting response from DPS, please contact the server administrator.';
                return false;
            }
            else
            {
                $this->response['status'] = $transaction_details['status']          = $parsed_xml["STATUS"];
                $this->response['transactionId'] =$transaction_details['transactionId']   = $parsed_xml["TRANSACTIONID"];
            }
        
         
            switch($transaction_details['status']){
                case 0:
                    //'approved';
                    $this->response['success']=1;
                    break;
                case 1:
                    //declined
                    $this->response['success']=0;
                    $this->response['message']='Your transaction was declined. Please check your credit card details and try again.';                                        
                    break;
                case 2:
                    //transient error, retry
                    if( $try<10 ) {
                        //retry
                        $this->getTransaction($try+1);
                    } else {
                        $this->response['success']=1;
                        $this->response['message']='Transaction declined due to transient error';                                        
                    }
                    break;
                case 3:
                    //'invalid data';
                    if( $try<5 ) {
                        //retry
                        $this->getTransaction($try+1);
                    } else {                        
                        $this->response['success']=0;
                        $this->response['message']='Invalid data submitted in form post.';   
                    }
                    break;
                case 4:
                    //'result cannot be determined at this time, retry';
                    if( $try<10 ) {
                        //retry
                         $this->getTransaction($try+1);
                    } else {
                        $this->response['success']=1;
                        $this->response['message']='result cannot be determined at this time.';   
                    }
                    break;
                case 5:
                        $this->response['success']=0;
                        $this->response['message']='Transaction did not proceed due to being attempted after timeout timestamp or having been cancelled by a CancelTransaction call.';   
                    break;
                case 6:
                        $this->response['success']=0;
                        $this->response['message']='No transaction found (SessionId query failed to return a transaction record - transaction not yet attempted).';   
                    break;
            }
            return true;
    }
}

