<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Generalfront
{

   
    public function __construct()
    {
        $this->CI = & get_instance();
        $this->CI->db->max_fetch_limit = $this->CI->config->item('db_max_limit');
    }
    

    function paginate($reload, $page, $tpages) {
        $adjacents = 2;
        $prevlabel = "&lsaquo; Prev";
        $nextlabel = "Next &rsaquo;";
        $out = "";
        
        // previous
        if ($page == 1) {
            $out.= "<li><a>" . $prevlabel . "</a></li>\n";              
        } elseif ($page == 2) {
            $out.= "<li><a  href=\"" . $reload . "\">" . $prevlabel . "</a>\n</li>";
        } else {
            $out.= "<li><a  href=\"" . $reload . "&amp;page=" . ($page - 1) . "\">" . $prevlabel . "</a>\n</li>";
        }
      
        $pmin = ($page > $adjacents) ? ($page - $adjacents) : 1;
        $pmax = ($page < ($tpages - $adjacents)) ? ($page + $adjacents) : $tpages;
        for ($i = $pmin; $i <= $pmax; $i++) {
            if ($i == $page) {
                $out.= "<li  class=\"active\"><a>" . $i . "</a></li>\n";
            } elseif ($i == 1) {
                $out.= "<li><a  href=\"" . $reload . "\">" . $i . "</a>\n</li>";
            } else {
                $out.= "<li><a  href=\"" . $reload . "&amp;page=" . $i . "\">" . $i . "</a>\n</li>";
            }
        }
        
        if ($page < ($tpages - $adjacents)) {
            $out.= "<li><a style='font-size:11px' href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $tpages . "</a></li>\n";
        }
        // next
        if ($page < $tpages) {
            $out.= "<li><a  href=\"" . $reload . "&amp;page=" . ($page + 1) . "\">" . $nextlabel . "</a>\n</li>";
        } else {
            $out.= "<li><a>" . $nextlabel . "</a></li>\n";
        }
        $out.= "";        
        return $out;
    }   
    
    function paginateWithAjax($reload, $page, $tpages) {
        $adjacents = 2;
        $prevlabel = "&lsaquo; Prev";
        $nextlabel = "Next &rsaquo;";
        $out = "";
        
        // previous
        if ($page == 1) {
            $out.= "<li><a>" . $prevlabel . "</a></li>\n";              
        } elseif ($page == 2) {
            $out.= "<li><a  href='javascript:' onClick='return pagingAjax(\"" . $reload . "\",\"\");'>" . $prevlabel . "</a>\n</li>";
        } else {
            $out.= "<li><a  href='javascript:' onClick='return pagingAjax(\"" . $reload . "\",\"" . ($page - 1) . "\");'>" . $prevlabel . "</a>\n</li>";
        }
      
        $pmin = ($page > $adjacents) ? ($page - $adjacents) : 1;
        $pmax = ($page < ($tpages - $adjacents)) ? ($page + $adjacents) : $tpages;
        for ($i = $pmin; $i <= $pmax; $i++) {
            if ($i == $page) {
                $out.= "<li  class=\"active\"><a>" . $i . "</a></li>\n";
            } elseif ($i == 1) {
                $out.= "<li><a  href='javascript:' onClick='return pagingAjax(\"" . $reload . "\",\"\");'>" . $i . "</a>\n</li>";
            } else {
                $out.= "<li><a  href='javascript:' onClick='return pagingAjax(\"" . $reload . "\",\"" .$i. "\");'>" . $i . "</a>\n</li>";
            }
        }
        
        if ($page < ($tpages - $adjacents)) {
            $out.= "<li><a onClick='return pagingAjax(\"" . $reload . "\",\"\",\"" .$tpages. "\");'>" . $tpages . "</a></li>\n";
        }
        // next
        if ($page < $tpages) {
            $out.= "<li><a  href='javascript:' onClick='return pagingAjax(\"" . $reload . "\",\"" . ($page + 1) . "\");'>" . $nextlabel . "</a>\n</li>";
        } else {
            $out.= "<li><a>" . $nextlabel . "</a></li>\n";
        }
        $out.= "";        
        return $out;
    }

    function paginateWithAjaxByFilterSearch($reload, $page, $tpages) {
        $adjacents = 2;
        $prevlabel = "&lsaquo; Prev";
        $nextlabel = "Next &rsaquo;";
        $out = "";
        
        // previous
        if ($page == 1) {
            $out.= "<li><a>" . $prevlabel . "</a></li>\n";              
        } elseif ($page == 2) {
            $out.= "<li><a  href='javascript:' onClick='return pagingAjaxBySearchFilter(\"" . $reload . "\",\"\");'>" . $prevlabel . "</a>\n</li>";
        } else {
            $out.= "<li><a  href='javascript:' onClick='return pagingAjaxBySearchFilter(\"" . $reload . "\",\"" . ($page - 1) . "\");'>" . $prevlabel . "</a>\n</li>";
        }
      
        $pmin = ($page > $adjacents) ? ($page - $adjacents) : 1;
        $pmax = ($page < ($tpages - $adjacents)) ? ($page + $adjacents) : $tpages;
        for ($i = $pmin; $i <= $pmax; $i++) {
            if ($i == $page) {
                $out.= "<li  class=\"active\"><a>" . $i . "</a></li>\n";
            } elseif ($i == 1) {
                $out.= "<li><a  href='javascript:' onClick='return pagingAjaxBySearchFilter(\"" . $reload . "\",\"\");'>" . $i . "</a>\n</li>";
            } else {
                $out.= "<li><a  href='javascript:' onClick='return pagingAjaxBySearchFilter(\"" . $reload . "\",\"" .$i. "\");'>" . $i . "</a>\n</li>";
            }
        }
        
        if ($page < ($tpages - $adjacents)) {
            $out.= "<li><a onClick='return pagingAjaxBySearchFilter(\"" . $reload . "\",\"\",\"" .$tpages. "\");'>" . $tpages . "</a></li>\n";
        }
        // next
        if ($page < $tpages) {
            $out.= "<li><a  href='javascript:' onClick='return pagingAjaxBySearchFilter(\"" . $reload . "\",\"" . ($page + 1) . "\");'>" . $nextlabel . "</a>\n</li>";
        } else {
            $out.= "<li><a>" . $nextlabel . "</a></li>\n";
        }
        $out.= "";        
        return $out;
    }

    function getMenuTree($header = array()) {
        $header['query'] = $this->CI->general->getCleanURL($header['query']);
        $headerarr = array();
        foreach ($header['menu'] as $head) {
            $headerarr[$head['mm_menu_key']] = array('mm_title' => $head['mm_title'],);
            foreach ($header['query'] as $items) {
                if ($head['mm_mst_menu_id'] == $items['mmi_mst_menu_id']) {
                    $headerarr[$head['mm_menu_key']]['items'][] = array(
                        'mmi_mst_menu_items_id' => $items['mmi_mst_menu_items_id'],
                        'mmi_mst_menu_id' => $items['mmi_mst_menu_id'],
                        'mmi_title' => $items['mmi_title'],
                        'mmi_parent_id' => $items['mmi_parent_id'],
                        'mmi_external_link' => $items['mmi_external_link'],
                        'mmi_resource_type' => $items['mmi_resource_type'],
                        'mmi_login_req' => $items['mmi_login_req'],
                    );
                }
            }
        }
        foreach ($headerarr as $key => $value) {

            if(count($headerarr[$key]['items']) > 0){
                $arr = $this->CI->general->BuildTree($headerarr[$key]['items'], $parentId = 0);    
                $headerarr[$key]['items'] = $arr;
            }
        }
        return $headerarr;
    }    
    
    function FrontMenu() {        
        $this->CI->load->model('hbmodel');
        $params = array();
        $ws_name = 'get_menu_tree';        
        $menu_arr = $this->CI->hbmodel->getData($ws_name, $params);
        $menu_details = $merchant_details_arr['data'];
        $menuArr = $this->getMenuTree($menu_arr['data']);  
        // pr($menuArr);exit();      
        return $menuArr;
    }

    /* 0651
     * Check captcha is valid or not
     */

    function captchaIsValid($captcha = '') {
        if ($captcha != '') {
            $this->CI->load->library('captcha');
            $response = $this->CI->captcha->valid($captcha);
            if ($response['is_valid'] == 1) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    // Below function to check if user emailid is exist or not

    function checkemailExist($field,$value,$tablname,$returnstring=false){

        if ($this->CI->db->field_exists($field,$tablname)){
            $this->CI->db->select($field);
            $this->CI->db->from($tablname);
            $this->CI->db->where($field,$value);
            $result = $this->CI->db->get()->result_array();
            if(count($result) > 0 && !empty($result)){
                if($returnstring) {
                  return 'false';
                } else {
                  return false;
                }
            }else{
                if($returnstring) {
                  return 'true';
                } else {
                  return true;
                }
            }
            
        }else{
            return "Field is not exist.";
        }
    }
    
    //Below Function For Create Country Dropdown
    function assignCountryDropDown($val = "") {
       $this->CI->load->library('dropdown');
       $this->CI->db->select('iCountryId,vCountry');
       $whereSql = 'mod_country.eStatus ="Active"';
       $this->CI->db->where($whereSql);
       $this->CI->db->order_by("vCountry ASC");
       $array = $this->CI->db->get('mod_country')->result_array();
       $arr = array();
       foreach ($array as $key => $value) {
           $arr[$value['iCountryId']] = $value['vCountry'];
       }
       $this->CI->dropdown->combo("array", "vCountry", $arr);
    }

    function assignStateDataDropDown($cid = "") {
        
        $this->CI->load->library('dropdown');
        $this->CI->db->select('iStateId,vState');

        if($cid != "")
           $this->CI->db->where('iCountryId', $cid);
        $this->CI->db->where('eStatus', 'Active');
        $array = $this->CI->db->get('mod_state')->result_array();
        $arr = array();
        $arr[''] = 'Select State';
        foreach ($array as $key => $value) {
          $arr[$value['iStateId']] = $value['vState'];
        }
        $this->CI->dropdown->combo("array", "vState", $arr);
        
    }
    
    function assignCityDataDropDown($cid = "") {
        $this->CI->load->library('dropdown');
        $this->CI->db->select('iCityId,vCity');
        
        if($cid != "")
          $this->CI->db->where('iStateId', $cid);
        $this->CI->db->where('eStatus', 'Active');
        $array = $this->CI->db->get('mod_city')->result_array();
        $arr = array();
        $arr[''] = 'Select City';
        foreach ($array as $key => $value) {
            $arr[$value['iCityId']] = $value['vCity'];
        }
        $this->CI->dropdown->combo("array", "vCity", $arr);
    }    


   //Below Function will Return single column value/values from the table 
   //Added By Sarvil Ajwaliya
   function getRowData($checkColumn, $checkValue, $tablename, $onerowlony = true) {
       $this->CI->db->select('*');
       $this->CI->db->from($tablename);
       $this->CI->db->where($checkColumn, $checkValue);
       $resultArray = $this->CI->db->get()->result_array();
       $retunResult = array();
       if (is_array($resultArray) && count($resultArray) > 0) {
           foreach ($resultArray as $key => $value) {
               $retunResult[] = $value;
           }
           if ($onerowlony) {
               $retunResult = $retunResult[0];
           }
       }
       return $retunResult;
   }


   function getColumnValue($checkColumn, $checkValue, $returnColumn, $tablename, $returnsinglevalue = true) {
       $this->CI->db->select($returnColumn);
       $this->CI->db->from($tablename);
       $this->CI->db->where($checkColumn, $checkValue);
       $resultArray = $this->CI->db->get()->result_array();
       
       if ($returnsinglevalue) {
           $retunResult = '';
           if (is_array($resultArray) && count($resultArray) > 0) {
               foreach ($resultArray as $key => $value) {
                   $retunResult = $value[$returnColumn];
               }
           }
           return $retunResult;
       } else {

           $retunResult = array();

           if (is_array($resultArray) && count($resultArray) > 0) {
               foreach ($resultArray as $key => $value) {
                   $retunResult[] = $value[$returnColumn];
               }
           }
           return $retunResult;
       }
   }

   /* use this function for redirecting user into dashboard page based on login type
     call into login function - so if user already looged in then redirect into dashboard page */

   function redirectLogin($flag = true) {
       
       $eLoginType = $this->CI->session->userdata("eLoginType");
       $userid = $this->CI->session->userdata("iUserId");
       $return_flag = false;
       if (intval($userid) > 0 && $eLoginType == "Member") {
           $return_flag = true;
       } else {

           if ($flag) {               
               redirect($this->CI->config->item('site_url') . 'login.html');
           }
       }
       return $return_flag;
   }
   
   // Created By JJ 
   function productRatingFormat($rating = 0){
        $str = '';
        $str .='<ul class="star">';
        for ($i=0; $i < 5; $i++) { 
              if($i < $rating){
                  $str.='<li class="sprite star-fill"></li>';    
              }else{
                  $str.='<li class="sprite star-strock"></li>';                    
              }            
          }
        $str .='</ul>';
        return $str;
   }
   
   function checkIsPrductInWishlist(){
      $iUserId = $this->CI->session->userdata('iUserId');

      $this->CI->db->select('iMstProductsId');
      $this->CI->db->from('trn_product_wishlist');
      $this->CI->db->where('iAdminId',$iUserId);
      $result = $this->CI->db->get()->result_array();
      $productid = array();
      foreach ($result as $key => $value) {
        $productid[] = $value['iMstProductsId'];
      }      
      return $productid;
   }


   function updateWishlistCount($iMstProductsId,$opt){
      $this->CI->db->select('iWishlistState');
      $this->CI->db->from('mst_products');
      $this->CI->db->where('iMstProductsId',$iMstProductsId);
      $result = $this->CI->db->get()->row_array();
      if($opt == '+'){
        $count = $result['iWishlistState']+1;
      }elseif($opt == '-'){
        $count = $result['iWishlistState']-1;
      }
      
      $data = array('iWishlistState'=>$count);
      $this->CI->db->where('iMstProductsId',$iMstProductsId);
      $this->CI->db->update('mst_products',$data);

   }

   function return_cancel_order_status($input_params =array()){
        foreach ($input_params['data']['main_order'] as $key => $subord){

          $date = date('Y-m-d',strtotime('+'.$subord['mso_return_period'].' Days', strtotime($subord['mso_delivered_date'])));
          if(($subord['mso_item_status'] == 'Delivered')&& (date('Y-m-d') < $date) && strtolower($subord['mp_do_return']) == 'yes')
          {
            $input_params['data']['main_order'][$key]['return_ord'] = 1;
          }
          if($subord['mso_item_status'] == 'Pending' || $subord['mso_item_status'] == 'In-Process'){
            $input_params['data']['main_order'][$key]['cancel_ord'] = 1;
          }
          if($subord['mso_item_status'] == 'Delivered' || $subord['mso_item_status'] == 'Close'){
            $input_params['data']['main_order'][$key]['invoice_ord'] = 1;
          }
        }
        return $input_params;
     }
  
   // get maximum price
   function getMaxPriceForFilter($cid, $dealofday=false){
      // $qry = "SELECT MAX(mp.fSalePrice) as price FROM `trn_product_categories` as tpc LEFT JOIN mst_products as mp ON mp.iMstProductsId = tpc.iMstProductsId WHERE `iMstCategoriesId` = ".$cid;
      if(!$dealofday) {
        $qry = "SELECT MAX(mp.fSalePrice) as price FROM `trn_product_categories` as tpc LEFT JOIN mst_products as mp ON mp.iMstProductsId = tpc.iMstProductsId WHERE `iMstCategoriesId` IN (".$cid.")";
      } else {
        $qry = "SELECT MAX(mp.fSalePrice) as price FROM `trn_dealoftheday` as td LEFT JOIN mst_products as mp ON mp.iMstProductsId = td.iMstProductsId WHERE 1=1";
      }

      $retAr = $this->CI->db->query($qry)->row_array();
      if($retAr['price'] > 0){
          return $retAr['price'];  
      }else{
          return $retAr['price'] = 0;  
      }   
   }

  function getProductCount($cid){
    $qry = "SELECT COUNT(pc.iMstProductsId) as total FROM trn_product_categories AS pc LEFT JOIN mst_products AS p ON p.iMstProductsId=pc.iMstProductsId WHERE pc.iMstCategoriesId = '".$cid."' AND p.eStatus='Active'";
    $retAr = $this->CI->db->query($qry)->row_array();    
    return $retAr['total'];
  }

  function BuildTree($ar = array(), $parentId = 1) {
      //pr($ar);exit();
      $branch = array();
      $elements = $ar;
      foreach ($elements as $element) {
          if ($element['iParentId'] == $parentId) {
              $children = $this->BuildTree($elements, $element['iMstCategoriesId']);
              #pr($children);
              if ($children) {
                  $element['children'] = $children;
              }
              $branch[] = $element;
          }
      }
      $return_array = $branch;
      //pr($return_array);exit;
      return $return_array;
  }

  function category_nlevel_list($category, $child=0, $id="") {
    if($child) {
      $html = "<ul".($child ? ' class="collapse" id="'.preg_replace("![^a-z0-9]+!i", "_", $id).'"' : "").">";
    }
    foreach($category as $key=>$value) {
      // pr($value);
      $html .= "<li>";
      if(isset($value['children']) && !empty($value['children'])) {
        $pregreplace_value = preg_replace("![^a-z0-9]+!i", "_", $value['vTitle']);
        $html .= "<a href='#".$pregreplace_value."' aria-expanded='true' data-toggle='collapse' class='".$pregreplace_value."'><i class='fa fa-chevron-down'></i>".$value['vTitle']."<span>(".$this->getProductCount($value['iMstCategoriesId']).")</span></a>";
        $html .= $this->category_nlevel_list($value['children'], 1, $value['vTitle']);
      } else {
        $html .= "<a href='javascript:;' onclick='return productListingAjax(\"".$value['vCleanUrl']."\");'>".$value['vTitle']."<span>(".$this->getProductCount($value['iMstCategoriesId']).")</span></a>";
      }
      $html .= "</li>";
    }
    if($child) {
      $html .= "</ul>";
    }
    return $html;
  }

  function category_nlevel_list_by_id($catId, $child=0, $elemId='') {
    $this->CI->db->select("iMstCategoriesId, vTitle, iParentId, (SELECT vCleanUrl FROM trn_clean_url WHERE eResourceType='C' AND iResourceId=mst_categories.iMstCategoriesId) AS vCleanUrl");
    $this->CI->db->from("mst_categories");
    $this->CI->db->where("iParentId", $catId);
    $result = $this->CI->db->get()->result_array();
    if($child) {
      $html = "<ul class='collapse' id='".$elemId."'>";
    }
    foreach($result as $key=>$value) {
      $this->CI->db->select("iMstCategoriesId");
      $this->CI->db->from("mst_categories");
      $this->CI->db->where("iParentId", $value['iMstCategoriesId']);
      $result1 = $this->CI->db->get()->result_array();

      $pregreplace_value = preg_replace("![^a-z0-9]+!i", "_", $value['vTitle']);
      $html .= "<li>";

      if(!empty($result1)) {
        $html .= "<a href='#".$pregreplace_value."' aria-expanded='true' data-toggle='collapse' class='".$pregreplace_value."' onclick='return productListingAjax(\"".$value['iMstCategoriesId']."\", \"".$value['vCleanUrl']."\", \"".$pregreplace_value."\");'><i class='fa fa-chevron-down'></i>".$value['vTitle']."<span>(".$this->getProductCount($value['iMstCategoriesId']).")</span></a>";
      } else {
        $html .= "<a class='".$pregreplace_value."' href='javascript:;' onclick='return productListingAjax(\"".$value['iMstCategoriesId']."\", \"".$value['vCleanUrl']."\", \"".$pregreplace_value."\");'>".$value['vTitle']."<span>(".$this->getProductCount($value['iMstCategoriesId']).")</span></a>";
      }
      $html .= $this->category_nlevel_list_by_id($value['iMstCategoriesId'], 1, $pregreplace_value);
      $html .= "</li>";
    }
    // $html .= "</li>";
    if($child) {
      $html .= "</ul>";
    }
    if(strlen($html)<=5) {
      $html = "";
    }
    return $html;
  }


  function categorywalkup_nlevel_list_by_id($catId, $child=0, $elemId='') {
    $this->CI->db->select("iMstCategoriesId");
    $this->CI->db->from("mst_categories");
    $this->CI->db->where("iParentId", $catId);
    $result = $this->CI->db->get()->result_array();

    $p2c_ids = array();
    foreach($result as $key=>$value) {
      $p2c_ids[] = $value['iMstCategoriesId'];
      $this->CI->db->select("iMstCategoriesId");
      $this->CI->db->from("mst_categories");
      $this->CI->db->where("iParentId", $value['iMstCategoriesId']);
      $result1 = $this->CI->db->get()->result_array();

      if(!empty($result1)) {
        $ids = $this->categorywalkup_nlevel_list_by_id($value['iMstCategoriesId'], 1, $pregreplace_value);
        if(is_array($ids)) {
          $temp = array();
          foreach($ids as  $k=>$v) {
            $temp[] = $v;
          }
          $p2c_ids[] = implode(",", $temp);
        }
      }
    }
    return $p2c_ids;
  }

  function getParentId($catid){
      $this->CI->db->select("iParentId");
      $this->CI->db->from("mst_categories");
      $this->CI->db->where("iMstCategoriesId",$catid);
      $result = $this->CI->db->get()->result_array();
      return $result[0]['iParentId'];    
  }
  function getBrandCount($title, $return=false){
    $this->CI->db->select('iTrnProductOptionId');
    $this->CI->db->from('trn_product_option');
    // $this->CI->db->like('vOptionValue', $title);
    $this->CI->db->where('vOptionValue', $title);    
    if($return) {
      return $this->CI->db->get()->num_rows();
    } else {
      echo $this->CI->db->get()->num_rows();
    }
  }

  function getProductByBrand($title){    
    $qry = "SELECT iProductId FROM trn_product_option WHERE vOptionValue IN (".$title.")";    
    $result = $this->CI->db->query($qry)->result_array();
    if(count($result) > 0){
        foreach ($result as $key => $value) {
               $arr[] = $value['iProductId'];  
        }  
    }
    $result =  implode(',', $arr);   
    return $result;
  }


  function getProductsBasedOnPids($pid){
    $qry = "SELECT * FROM mst_products WHERE iMstProductsId IN (".$pid.")";  
    $result = $this->CI->db->query($qry)->result_array();
    return $result;
  }

  function getCategoryWalkupIds($cat_id){
    $category_ids = $this->CI->general->sitemapCategoryWalkup(array('mc_mst_categories_id'=>$cat_id));
    
    foreach ($category_ids as $key => $value) {
        $cids[] = $value['iMstCategoriesId'];
    }
    $cid = implode(',',$cids);
    return $cid;
  }

  function getProductByFilter($filter) {
    $array = explode(",", $filter['filter']);
    $optionName = $optionValue = array();
    $freeshippingoption = $otheroptionname = "";
    foreach($array as $key=>$value) {
      $arr = explode("|", $value);
      if(!in_array($arr[0], $optionName) && $arr[0]!="freeshipping") {
        $optionName[] = $arr[0];
      }
      if($arr[1]=="other") {
        $otheroptionname = $arr[1];
      } elseif($arr[1]=="freeshipping") {
        $freeshippingoption = $arr[1];
      } else {
        $optionValue[] = $arr[1];
      }
    }
    if(count($optionName) > 1) {
      $having = "HAVING COUNT(iProductId)> 1";
    } else {
      $having = "HAVING COUNT(iProductId)>= 1";
    }
    $qry = "SELECT iProductId FROM trn_product_option WHERE vCategoryOptionValue IN ('".implode("','", $optionName)."') AND vOptionValue IN ('".implode("','", $optionValue)."') GROUP BY iProductId ".$having;

    $result = $this->CI->db->query($qry)->result_array();
    if(count($result) > 0){
      foreach ($result as $key => $value) {
        $arr1[] = $value['iProductId'];
      }
    }
    $filtered_product_result = implode(',', $arr1);
    if($otheroptionname=="other") {
      if($filtered_product_result) {
        $wsql = "AND iProductId NOT IN (".$filtered_product_result.")";
      } else {
        $wsql = "";
      }
      $qry = "SELECT iProductId FROM trn_product_option WHERE 1=1 ".$wsql;
      $result = $this->CI->db->query($qry)->result_array();
      $productid_of_po = array();
      foreach($result as $key=>$value) {
        $productid_of_po[] = $value['iProductId'];
      }
      $productid_of_po = ($filtered_product_result ? $filtered_product_result."," : "").implode(",", $productid_of_po);
      
      $qry = "SELECT iMstProductsId FROM trn_product_categories WHERE iMstCategoriesId IN (".$filter['cid'].") AND iMstProductsId NOT IN (".$productid_of_po.")";
      $result = $this->CI->db->query($qry)->result_array();
      $other_product_id = array();
      foreach($result as $key=>$value) {
        $other_product_id[] = $value['iMstProductsId'];
      }
      $other_product_id = implode(",",$other_product_id);
      $filtered_product_result = ($filtered_product_result) ? $filtered_product_result.",".$other_product_id : $other_product_id;
    }
    if($freeshippingoption=="freeshipping") {
      if($filtered_product_result) {
        $checkedfilterpid = " AND p.iMstProductsId IN (".$filtered_product_result.")";
      } else {
        $checkedfilterpid = "";
      }
      $qry = "SELECT p.iMstProductsId FROM mst_products AS p LEFT JOIN trn_product_categories AS pc ON pc.iMstProductsId=p.iMstProductsId WHERE 1=1 AND p.eFreeShipping='Yes' AND pc.iMstCategoriesId IN (".$filter['cid'].") ".$checkedfilterpid;
      $result = $this->CI->db->query($qry)->result_array();
      $freeshipping_product_id = array();
      foreach($result as $key=>$value) {
        $freeshipping_product_id[] = $value['iMstProductsId'];
      }
      $freeshipping_product_id = implode(",", $freeshipping_product_id);
      $filtered_product_result .= ($filtered_product_result ? $filtered_product_result."," : "").$freeshipping_product_id;
    }
    return $filtered_product_result;
  }

  function getCategoryOptionsAvailable($postdata, $filtertext=false) {
    $this->CI->db->select("cov.vValue, o.vTitle");
    $this->CI->db->from("mst_category_options AS co");
    $this->CI->db->join("mst_options AS o", "o.iMstOptionsId=co.iMstOptionsId");
    $this->CI->db->join("mst_category_option_values AS cov", "cov.iMstCategoryOptionsId=co.iMstCategoryOptionsId");
    if($filtertext) {
      $this->CI->db->where("o.vTitle", $postdata['filter']);
      if(isset($postdata['key'])) {
        $this->CI->db->like("cov.vValue", $postdata['key']);
      }
    }
    $this->CI->db->where("iMstCategoriesId", $postdata['catid']);
    $result = $this->CI->db->get()->result_array();
    return $result;
  }

  function getPrimaryOption(){    
      $this->CI->db->select("iMstCategoriesId, vTitle");
      $this->CI->db->from("mst_categories");
      $this->CI->db->where("iParentId",0);
      $result = $this->CI->db->get()->result_array();
      return $result;
  }
  function insertaddress($address,$iUserId){
      $this->CI->db->select('iAdminId');
      $this->CI->db->from('mst_buyer_address');
      $this->CI->db->where('iAdminId',$iUserId);
      $result = $this->CI->db->get()->result_array();   

      if(count($result) > 0 && !empty($result)){  
        $data['iAdminId'] = $iUserId;
        $data['vName'] = $address['vName'];
        $data['vAddress1'] = $address['vAddress1'];
        $data['vAddress2'] = $address['vAddress2'];
        $data['vArea'] = $address['vArea'];
        $data['iCountryId'] = $address['iCountryId'];
        $data['iStateId'] = $address['iStateId'];
        $data['iCityId'] = $address['iCityId'];
        $data['vPinCode'] = $address['vPinCode'];
        $data['vPhone'] = $address['vPhone'];
        $this->CI->db->where("iAdminId",$iUserId);        
        $this->CI->db->update('mst_buyer_address',$data);        
          
      }else{
          $data['iAdminId'] = $iUserId;
          $data['vName'] = $address['vName'];
          $data['vAddress1'] = $address['vAddress1'];
          $data['vAddress2'] = $address['vAddress2'];
          $data['vArea'] = $address['vArea'];
          $data['iCountryId'] = $address['iCountryId'];
          $data['iStateId'] = $address['iStateId'];
          $data['iCityId'] = $address['iCityId'];
          $data['vPinCode'] = $address['vPinCode'];
          $data['vPhone'] = $address['vPhone'];
          $this->CI->db->insert($data);
      }
  }

  function productid_to_categoryid($pids) {
    $this->CI->db->select("iMstCategoriesId");
    $this->CI->db->from("trn_product_categories AS pc");
    $where = "FIND_IN_SET(iMstProductsId, '".$pids."')";
    $this->CI->db->where($where, FALSE, FALSE);
    $this->CI->db->group_by("iMstCategoriesId");
    $result = $this->CI->db->get()->result_array();
    return $result;
  }

  function getDisableFilterValue($post) {
    $this->CI->db->select("CONCAT((po.vCategoryOptionValue), ('|'), (po.vOptionValue)) AS filterValue, po.vCategoryOptionValue AS vValue, po.vOptionValue AS vTitle");
    $this->CI->db->from("trn_product_categories AS pc");
    $this->CI->db->join("trn_product_option AS po", "po.iProductId=pc.iMstProductsId");
    $this->CI->db->where("pc.iMstCategoriesId IN (".$post['cid'].")", FALSE, FALSE);
    $this->CI->db->group_by("filterValue");
    $result = $this->CI->db->get()->result_array();
    return $result;
  }

  function getTotalProductForFilter($post, $option="", $optionvalue="") {
    $this->CI->db->select("pc.iMstProductsId");
    $this->CI->db->from("trn_product_categories AS pc");
    $this->CI->db->join("trn_product_option AS po", "po.iProductId=pc.iMstProductsId");
    $this->CI->db->where("pc.iMstCategoriesId IN (".$post['cid'].")", FALSE, FALSE);
    if(!empty($option)) {
      if(!empty($optionvalue)) {
        $this->CI->db->like("po.vCategoryOptionValue", $option);
        $this->CI->db->like("po.vOptionValue", $optionvalue, "both");
      }
    }
    $this->CI->db->group_by("pc.iMstProductsId");
    $result = $this->CI->db->get()->result_array();
    return count($result);
  }

  function getOrderNumber($orderid,$Table){
    if($Table == 'Order'){
        $this->CI->db->select("mo.vOrderNumberPre,mo.iOrderNumber");
        $this->CI->db->from("mst_order AS mo");
        $this->CI->db->where("mo.iMstOrderId IN (".$orderid.")", FALSE, FALSE);
        $orderdetail = $this->CI->db->get()->result_array();

    }elseif($Table == 'Suborder'){
        $this->CI->db->select("mso.vSubOrderNumberPre,mso.iSubOrderNumber");
        $this->CI->db->from("mst_sub_order AS mso");
        $this->CI->db->where("mso.iMstSubOrderId IN (".$orderid.")", FALSE, FALSE);
        $orderdetail = $this->CI->db->get()->result_array();
    }else
    {}
    return $orderdetail;
  }

  function getCleanURL($type='',$iResourceId){
    $this->CI->db->select("vCleanUrl");
    $this->CI->db->from("trn_clean_url");
    $this->CI->db->where("iResourceId",$iResourceId);
    if($type != ''){
      $this->CI->db->where("eResourceType",$type);  
    }
    $URL = $this->CI->db->get()->row_array();
    return $URL['vCleanUrl'];
  }

  function updatePlacedOrderQTY($data = array()){
    $oldQty = $this->getColumnValue('iMstProductsId',$data['iMstProductsId'],'iStock','mst_products');
    $data['iStock'] = $oldQty-$data['iStock'];    
    $this->CI->db->where(array('iMstProductsId'=>$data['iMstProductsId'],'iStoreId'=>$data['iStoreId']));
    $this->CI->db->update('mst_products',$data);
  }

  function filterKeywordListProductCount($params) {
    $query = "SELECT COUNT(pc.iMstProductsId) AS count, CONCAT(po.vCategoryOptionValue, '|', po.vOptionValue) AS filterValue From trn_product_categories AS pc LEFT JOIN trn_product_option AS po ON po.iProductId=pc.iMstProductsId JOIN mst_products AS p ON p.iMstProductsId=po.iProductId WHERE pc.iMstCategoriesId IN (".$params['cid'].") AND p.eStatus = 'Active' AND p.fSalePrice >= '".$params['start_price']."' AND p.fSalePrice <= '".$params['end_price']."' GROUP BY filterValue";
    $result = $this->CI->db->query($query)->result_array();
    $counter = array();
    foreach($result as $value) {
      $counter[$value['filterValue']] = $value['count'];
    }
    return $counter;
  }

  function getCategoriesAsPerIds($parentid) {
    $query = "SELECT iMstCategoriesId, vTitle FROM mst_categories WHERE iParentId='".$parentid."'";
    $result = $this->CI->db->query($query)->result_array();
    return $result;
  }

  function categorydetails($catids) {
    $query = "SELECT iMstCategoriesId, vTitle FROM mst_categories WHERE iMstCategoriesId IN (".$catids.")";
    $result = $this->CI->db->query($query)->result_array();
    foreach($result as $key=>$value) {
      $vCleanUrl = $this->CI->general->CleanUrl("c", $value['iMstCategoriesId']);
      $result[$key]['vCleanUrl'] = $vCleanUrl;
    }
    return $result;
  }

  public function sendMail($data = array(), $type_format = "CONTACT_US", $attachments = array()) {      
      $this->CI->load->library('email');
      $this->CI->load->model('hbmodel');

      if (is_array($data) && count($data) > 0) {
          if (!empty($data['vEmail'])) {
              $to = $data['vEmail'];
          } else {
              $to = $this->CI->config->item('EMAIL_ADMIN');
          }
          $mailarr = $this->CI->general->getSystemEmailData($type_format);
          $email_var = $this->CI->general->getVariablesByTemplate($mailarr[0]['iEmailTemplateId']);
          $emailDataArr = $this->CI->general->replaceEmailTemplate($mailarr[0], $email_var, $data);

          $from_name = $emailDataArr['vFromName'];
          $from = $emailDataArr['vFromEmail'];
          $subject = $emailDataArr['vEmailSubject'];
          $body = $emailDataArr['vEmailContent'];
          $cc = $data['vCCEmail'];
          $bcc = $data['vBCCEmail'];    
          
          $this->CI->email->from($from, $from_name);          
          $this->CI->email->to($to);
          $this->CI->email->subject($subject);
          $this->CI->email->message($body);
          
          // Below code for the send invoice in email - start
          $this->CI->load->library('mpdfpl');

          $params = array();
          // $params['oid'] = $data['mso_mst_sub_order_id'];
          $params['oid'] = $data['sub_order_id'];          
          $ws_name = 'product_invoice';
          $invoice_arr = $this->CI->hbmodel->getData($ws_name, $params);
          
          $data = array();
          $data['mo_buyer_address1'] = $invoice_arr['data'][0]['msd_address_seller'];
          $data['mc1_country'] = $invoice_arr['data'][0]['mc_country_seller'];
          $data['ms_state'] = $invoice_arr['data'][0]['ms_state_seller'];
          $data['mc2_city'] = $invoice_arr['data'][0]['mc_city_seller'];
          $data['mo_buyer_pin_code'] = $invoice_arr['data'][0]['msd_pin_code_seller'];
          $buyer_address = $this->CI->general->getBuyerShippingAddressExpandedView('', '', $data);
          $invoice_arr['data'][0]['msd_address_seller'] = $buyer_address;

          $data = array();
          $data['mo_buyer_area'] = $invoice_arr['data'][0]['mo_buyer_area'];
          $data['mo_buyer_address1'] = $invoice_arr['data'][0]['mo_buyer_address1'];
          $data['mc1_country'] = $invoice_arr['data'][0]['mc_country_buyer'];
          $data['ms_state'] = $invoice_arr['data'][0]['ms_state_buyer'];
          $data['mc2_city'] = $invoice_arr['data'][0]['mc1_city_buyer'];
          $data['mo_buyer_pin_code'] = $invoice_arr['data'][0]['mo_buyer_pin_code'];
          $buyer_address = $this->CI->general->getBuyerShippingAddressExpandedView('', '', $data);
          $invoice_arr['data'][0]['mo_buyer_address1'] = $buyer_address;                    
          
          
          $htm = $this->CI->load->view('Invoice', array('invoice_arr' => $invoice_arr['data'][0]), true);
          $this->CI->mpdfpl->generate_pdf($htm,'Invoice'.$params['oid']);
          $attachments[0] = $this->CI->config->item('temppdf_file_path') .'Invoice'.$params['oid'].'.pdf';

          
          for ($i = 0; $i < count($attachments); $i++) {
              $this->CI->email->attach($attachments[$i]);
              if(count($attachments)>1){
                  $vAttachment.=$attachments[$i].'|||';
              }else{
                  $vAttachment.=$attachments[$i];    
              }
          }                

          // Above code for the send invoice in email - end
          if($success = $this->CI->email->send()){              
              $DelFilePath = $this->CI->config->item('temppdf_file_path') .'Invoice'.$params['oid'].'.pdf';
              if (file_exists($DelFilePath)) { 
                unlink ($DelFilePath); 
              }                            
          }
          return $success;          
      } else {
          return FALSE;
      }
  }
 /*function insertToViewCount($postarr){
    $pid = $postarr['pid'];
    $ip = $postarr['ip'];
    $data['vIp'] = $ip;
    $data['iProductId'] = $pid;  

    $this->CI->db->select("iViewState");    
    $this->CI->db->from("mst_products");    
    $this->CI->db->where(array("iMstProductsId"=>$pid));  
    $viewArr = $this->CI->db->get()->row_array();  
    $iViewState = $viewArr['iViewState']+1;
    //echo $iViewState.'---'.$pid;exit;
    $sql = "UPDATE mst_products SET iViewState = ".$iViewState." WHERE iMstProductsId = ".$pid;
    echo $sql;exit;
    $result = $this->CI->db->query($sql);

    if($result){
      $this->CI->db->insert("trn_view_count",$data);    
      $insert_id = $this->CI->db->insert_id();
      return $insert_id;  
    }else{
      return '';
    }
    
  }

  function checkProductInViewCount($postarr){
    $pid = $postarr['pid'];
    $ip = $postarr['ip'];     
    $this->CI->db->select("*");    
    $this->CI->db->from("trn_view_count");    
    $this->CI->db->where(array("vIp"=>$ip,"iProductId"=>$pid));    
    $returnArr = $this->CI->db->get()->result_array();    
    if(count($returnArr) > 0){
      //$this->insertToViewCount($postarr);
    }
    return true;
    //return count($returnArr);
  }*/

  function checkSubcriber($vEmail){
    if(!empty($vEmail)){
      $this->CI->db->select("vEmail,eStatus");
      $this->CI->db->from("mst_newsletter_subscription");
      $this->CI->db->where("vEmail",$vEmail);                         
      $resultArr = $this->CI->db->get()->row_array();          
      if(count($resultArr) > 0){
        if($resultArr['eStatus'] == "Subscribe"){
          return false;
        }else{
          $data = array('eStatus'=>"Subscribe");
          $this->CI->db->where('vEmail',$vEmail);
          $this->CI->db->update('mst_newsletter_subscription',$data);              

          return false;
        }

      }else{
        return true;
      }
    }
  }
}

/* End of file Generalfront.php */
/* Location: ./application/libraries/Generalfront.php */