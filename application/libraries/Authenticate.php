<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate
{

    protected $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->authenticate();
    }

    protected function authenticate()
    {
        $admin_allow_arr = array(
            'user' => array(
                'login' => array('entry', 'entry_a', 'logout', 'sess_expire', 'resetpassword', 'resetpassword_action', 'forgot_password_action')
            )
        );
        $front_allow_arr = array(
            'user' => array(
                'user' => array('index', 'login', 'login_action', 'check_user_email', 'register', 'register_action','signup_action', 'forgotpassword', 'forgotpassword_action', 'staticpage','change_password','my_address','profile','facebookLogin','googlePlusLogin','inviteFriend','notification','check_customer_email','check_customer_email_for_forgotpassword','validateUserDetail','unsubscribe_newsletter')
            ),
            "content" => array(
                'content' => array('index', 'staticpage')
            ),
            "home" => array(
                'home' => array('index','getRecentPrduct')
            ),
            "faq" => array(
                'faq' => array('index','sellerFAQ')
            ),
            "contactus" => array(
                'contactus' => array('index','contact','resetCaptcha','check_captcha','contact_seller')
            ),
            "career" => array(
                'career' => array('career')
            ),
            "newslatter" => array(
                'newslatter' => array('subscribeNewsLatter')
            ),
            "wishlist" => array(
                'wishlist' => array('index','removeWishlist','addToWishlist','sendtofriend','sendMailToFriend')
            ),
            "news" => array(
                'news' => array('index','newsdetail','filterNews')
            ),
            "sellerregistration" => array(
                'sellerregistration' => array('index','setUserData','setStoreDetail','setBankDetail','getStateDataOnCountry','getCityDataOnState','checkStoreNameExist')
            ),
            "productlist" => array(
                'productlist' => array('index','productList','productDetail','ProductReview','addProductreview','productListBySort','quicksearch','searchProducts','dealOfTheDay','dealOfDayBySort','searchResult','searchResultBySort','checkCategoryOpton','loadfilterlist','maxpricebycategory','checkDisableFilterValue','filterKeywordListProductCounter', 'filterKeywordListProductCounterAjax')
            ),
            "store" => array(
                'store' => array('index','StoreData','StoreReview','addStoreReview','AboutStore','ReturnPolicy','otherproduct')
            ),
            "order" => array(
                'order' => array('index','TrackOrder','myOrder','OrderDetail','setReturnOrder','ReturnOrder')
            ),
            "sitemap" => array(
                'sitemap' => array('index', 'sitemaplist')
            ),  
			"cart"=>array(
                'cart'=>array('index','add','cart_info','removeitem_from_cart','update_cart')
            ),
            "checkout"=>array(
                'myshoppingcart'=>array('index','cartItem')
            )           
        );
        $webservice_allow_arr = array('rest', 'webservice', 'wsengine', 'nsengine', 'notification');

        $current_method = $this->CI->router->method;
        $current_module = $this->CI->router->fetch_module();
        $current_class = $this->CI->router->class;
        if ($this->CI->config->item('is_admin') == '1') {
            $temp_auth_arr = $admin_allow_arr[$current_module][$current_class];
            if (!is_array($temp_auth_arr) || !in_array($current_method, $temp_auth_arr)) {
                if (!$this->checkValidAuth('Admin')) {
                    if ($this->CI->input->is_ajax_request()) {
                        @header("Cit-auth-requires: 1");
                        echo "<script>callAdminSessionExpired();</script>";
                    } else {
                        $this->CI->load->library("general");
                        redirect($this->CI->general->getAdminEncodeURL('user/login/entry', 1) . "?_=" . time());
                    }
                    exit;
                }
            }
        } else {
            if (!$this->checkValidAuth('Member')) {
                
            }

            $temp_auth_arr = $front_allow_arr[$current_module][$current_class];
            if (!is_array($temp_auth_arr) || !in_array($current_method, $temp_auth_arr)) {
                if (!in_array($current_module, $webservice_allow_arr)) {
                    if (!$this->checkValidAuth('Member')) {
                        $redirect = true;
                        $cookiearr = $this->CI->cookie->read('userarray');
                        $sess_prefix = $this->CI->config->item("sess_prefix");
                        if (is_array($cookiearr) && trim($cookiearr[$sess_prefix . 'username']) != '' && trim($cookiearr[$sess_prefix . 'password']) != '') {
                            $user_name = $cookiearr[$sess_prefix . 'username'];
                            $password = $cookiearr[$sess_prefix . 'password'];
                            $this->CI->load->model('user/model_user');
                            $identity = $this->CI->model_user->authenticate($user_name, $password);
                            if ($identity['errorCode'] == 1) {
                                $cookiedata = array(
                                    $sess_prefix . 'username' => $user_name,
                                    $sess_prefix . 'password' => $password
                                );
                                $this->CI->cookie->write('userarray', $cookiedata);
                                $redirect = false;
                            }
                        }
                        if ($redirect) {
                            redirect($this->CI->config->item('site_url') . "login.html");
                            exit;
                        }
                    }
                }
            }
        }
    }

    public function checkValidAuth($eType)
    {
        $flag = false;
        if ($eType == 'Admin') {
            if (is_object($this->CI->session) && $this->CI->session->userdata('iAdminId') > 0) {
                $flag = true;
            }
        } elseif ($eType == 'Member') {
            if (is_object($this->CI->session) && $this->CI->session->userdata('iUserId') > 0) {
                $flag = true;
            }
        }
        return $flag;
    }
}

/* End of file Authenticate.php */
/* Location: ./application/libraries/Authenticate.php */    