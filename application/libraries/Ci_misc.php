<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Ci_misc {

    protected $CI;

    function __construct() {
        $this->CI = & get_instance();
    }

    /*
     * Code will be generated dynamically
     * Please do not write or change the content below this line
     * Five hashes must be there on either side of string.
     */

    function getModuleArray() {
        $db_app_list = array();
        #####GENERATED_LANGMODULES_FUNCTION_START#####
       $db_app_list = array(
                "accepted_requests"=>$this->CI->lang->line("ACCEPTED_REQUESTS_ACCEPTED_REQUESTS"),
                "admin"=>$this->CI->lang->line("ADMIN_ADMIN"),
                "bannermanagement"=>$this->CI->lang->line("BANNERMANAGEMENT_BANNER_MANAGEMENT"),
                "cancelledorderslisiting"=>$this->CI->lang->line("CANCELLEDORDERSLISITING_CANCELLED_ORDERS_LISITING"),
                "categorymanagement"=>$this->CI->lang->line("CATEGORYMANAGEMENT_CATEGORY_MANAGEMENT"),
                "categoryoptions"=>$this->CI->lang->line("CATEGORYOPTIONS_CATEGORY_OPTIONS"),
                "city"=>$this->CI->lang->line("CITY_CITY"),
                "closedorderslisiting"=>$this->CI->lang->line("CLOSEDORDERSLISITING_CLOSED_ORDERS_LISITING"),
                "confirmedorderslisiting"=>$this->CI->lang->line("CONFIRMEDORDERSLISITING_CONFIRMED_ORDER_LISITING"),
                "country"=>$this->CI->lang->line("COUNTRY_COUNTRY"),
                "customer"=>$this->CI->lang->line("CUSTOMER_CUSTOMER"),
                "customers"=>$this->CI->lang->line("CUSTOMERS_CUSTOMERS"),
                "dealoftheday"=>$this->CI->lang->line("DEALOFTHEDAY_DEAL_OF_THE_DAY"),
                "deliveredorderlisting"=>$this->CI->lang->line("DELIVEREDORDERLISTING_DELIVERED_ORDER_LISTING"),
                "download"=>$this->CI->lang->line("DOWNLOAD_DOWNLOAD"),
                "editprofile"=>$this->CI->lang->line("EDITPROFILE_EDIT_PROFILE"),
                "faq"=>$this->CI->lang->line("FAQ_FAQ"),
                "faqcategory"=>$this->CI->lang->line("FAQCATEGORY_FAQ_CATEGORY"),
                "featureproduct"=>$this->CI->lang->line("FEATUREPRODUCT_FEATURE_PRODUCT"),
                "getorderexpandedview"=>$this->CI->lang->line("GETORDEREXPANDEDVIEW_GET_ORDER_EXPANDED_VIEW"),
                "getrefundexpandedview"=>$this->CI->lang->line("GETREFUNDEXPANDEDVIEW_GET_REFUND_EXPANDED_VIEW"),
                "getreturnexpandedview"=>$this->CI->lang->line("GETRETURNEXPANDEDVIEW_GET_RETURN_EXPANDED_VIEW"),
                "group"=>$this->CI->lang->line("GROUP_GROUP"),
                "homecategory"=>$this->CI->lang->line("HOMECATEGORY_HOME_CATEGORY"),
                "jobs"=>$this->CI->lang->line("JOBS_JOBS"),
                "loghistory"=>$this->CI->lang->line("LOGHISTORY_LOG_HISTORY"),
                "logistic"=>$this->CI->lang->line("LOGISTIC_LOGISTIC"),
                "menuitems"=>$this->CI->lang->line("MENUITEMS_MENU_ITEMS"),
                "menumanagement"=>$this->CI->lang->line("MENUMANAGEMENT_MENU_MANAGEMENT"),
                "neworderlisting"=>$this->CI->lang->line("NEWORDERLISTING_NEW_ORDER_LISTING"),
                "newrequest"=>$this->CI->lang->line("NEWREQUEST_NEW_REQUESTS"),
                "news"=>$this->CI->lang->line("NEWS_NEWS"),
                "newslettersubscription"=>$this->CI->lang->line("NEWSLETTERSUBSCRIPTION_NEWSLETTER__SUBSCRIPTION"),
                "newslettergroup"=>$this->CI->lang->line("NEWSLETTERGROUP_NEWSLETTER_GROUP"),
                "newslettertemplate"=>$this->CI->lang->line("NEWSLETTERTEMPLATE_NEWSLETTER_TEMPLATE"),
                "optionvaluemaster"=>$this->CI->lang->line("OPTIONVALUEMASTER_OPTION_VALUE_MASTER"),
                "options"=>$this->CI->lang->line("OPTIONS_OPTIONS"),
                "pending_refund__v1"=>$this->CI->lang->line("PENDING_REFUND__V1_PAID_REFUND"),
                "pendingrefund"=>$this->CI->lang->line("PENDINGREFUND_PENDING_REFUND"),
                "productimages"=>$this->CI->lang->line("PRODUCTIMAGES_PRODUCT_IMAGES"),
                "productmanagement"=>$this->CI->lang->line("PRODUCTMANAGEMENT_PRODUCT_MANAGEMENT"),
                "productoptions"=>$this->CI->lang->line("PRODUCTOPTIONS_PRODUCT_OPTIONS"),
                "product_options_seller"=>$this->CI->lang->line("PRODUCT_OPTIONS_SELLER_PRODUCT_OPTIONS_SELLER"),
                "productreview"=>$this->CI->lang->line("PRODUCTREVIEW_PRODUCT_REVIEW"),
                "productreviewforseller"=>$this->CI->lang->line("PRODUCTREVIEWFORSELLER_PRODUCT_REVIEW_FOR_SELLER"),
                "reasonhistory"=>$this->CI->lang->line("REASONHISTORY_REASON_HISTORY"),
                "refundedlisting"=>$this->CI->lang->line("REFUNDEDLISTING_REFUNDED_LISTING"),
                "rejected_requests"=>$this->CI->lang->line("REJECTED_REQUESTS_REJECTED_REQUESTS"),
                "return_request_closed"=>$this->CI->lang->line("RETURN_REQUEST_CLOSED_RETURN_ITEM_CLOSED"),
                "return_item_replaced"=>$this->CI->lang->line("RETURN_ITEM_REPLACED_RETURN_ITEM_REPLACED"),
                "return_items_received"=>$this->CI->lang->line("RETURN_ITEMS_RECEIVED_RETURN_ITEMS_RECEIVED"),
                "seller"=>$this->CI->lang->line("SELLER_SELLER"),
                "seller_management"=>$this->CI->lang->line("SELLER_MANAGEMENT_SELLER_MANAGEMENT"),
                "seller_product_management"=>$this->CI->lang->line("SELLER_PRODUCT_MANAGEMENT_SELLER_PRODUCT_MANAGEMENT"),
                "sellersettlementreport"=>$this->CI->lang->line("SELLERSETTLEMENTREPORT_SELLER_SETTLEMENT_REPORT"),
                "sendnewsletter"=>$this->CI->lang->line("SENDNEWSLETTER_SEND_NEWSLETTER"),
                "settelment"=>$this->CI->lang->line("SETTELMENT_SETTELMENT"),
                "shippedorderlisting"=>$this->CI->lang->line("SHIPPEDORDERLISTING_SHIPPED_ORDER_LISTING"),
                "slidermanagement"=>$this->CI->lang->line("SLIDERMANAGEMENT_SLIDER_MANAGEMENT"),
                "state"=>$this->CI->lang->line("STATE_STATE"),
                "staticpages"=>$this->CI->lang->line("STATICPAGES_STATIC_PAGES"),
                "storereview"=>$this->CI->lang->line("STOREREVIEW_STORE_REVIEW"),
                "storereviewforseller"=>$this->CI->lang->line("STOREREVIEWFORSELLER_STORE_REVIEW_FOR_SELLER"),
                "storestaticpage"=>$this->CI->lang->line("STORESTATICPAGE_STORE_STATIC_PAGE"),
                "systememails"=>$this->CI->lang->line("SYSTEMEMAILS_SYSTEM_EMAILS"),
                "unsettledtransactions"=>$this->CI->lang->line("UNSETTLEDTRANSACTIONS_UNSETTLED_TRANSACTIONS"),
                "upload_bulk_image"=>$this->CI->lang->line("UPLOAD_BULK_IMAGE_UPLOAD_BULK_IMAGE"),
                "upload_bulk_image_for_seller"=>$this->CI->lang->line("UPLOAD_BULK_IMAGE_FOR_SELLER_UPLOAD_BULK_IMAGE_FOR_SELLER"),
                "uploadcsv"=>$this->CI->lang->line("UPLOADCSV_UPLOAD_CSV"),
                "upload_csv_for_seller"=>$this->CI->lang->line("UPLOAD_CSV_FOR_SELLER_UPLOAD_CSV_FOR_SELLER"),
                "usertoadminmessage"=>$this->CI->lang->line("USERTOADMINMESSAGE_USER_TO_ADMIN_MESSAGE"),
                "usertoseller"=>$this->CI->lang->line("USERTOSELLER_USER_TO_SELLER"),
                "usertosellerbyadmin"=>$this->CI->lang->line("USERTOSELLERBYADMIN_USER_TO_SELLER_BY_ADMIN")); 
            #####GENERATED_LANGMODULES_FUNCTION_END#####
        return $db_app_list;
    }

    /*
     * Code will be generated dynamically
     * Please do not write or change the content below this line
     * Five hashes must be there on either side of string.
     */

    function getBulkEmailModules() {
        $module_arr = array();
        #####GENERATED_BULKEMAIL_MODULES_START#####
$module_arr["admin"] = $this->CI->lang->line("ADMIN_ADMIN");
        #####GENERATED_BULKEMAIL_MODULES_END#####
        return $module_arr;
    }

    function getBulkEmailModuleFields($params_arr = array()) {
        $module_name = $params_arr["module_name"];
        $field_arr = array();
        switch ($module_name) {
            #####GENERATED_BULKEMAIL_FIELDS_START#####

                    case "admin" :
                        break;
                            #####GENERATED_BULKEMAIL_FIELDS_END#####
        }
        return $field_arr;
    }

    function getBulkEmailModuleData($module_name = "") {
        $data = array();
        switch ($module_name) {
            #####GENERATED_BULKEMAIL_DATA_START#####

        case "admin" :
            $this->CI->load->model("user/model_admin");
            $extra_cond = $this->model_admin->extra_cond;
            $data = $this->CI->model_admin->getData($extra_cond, "", "", "", "", "Yes");
            break;
            #####GENERATED_BULKEMAIL_DATA_END#####
        }
        return $data;
    }

    function getBulkEmailModuleListFields($module_name = "") {
        $data = array();
        switch ($module_name) {
            #####GENERATED_BULKEMAIL_LIST_FIELD_START#####

        case "admin" :
            $this->CI->load->model("user/model_admin");
            $data = $this->CI->model_admin->getListConfiguration();
            break;
            #####GENERATED_BULKEMAIL_LIST_FIELD_END#####
        }
        return $data;
    }

    function getPushNotifyModules($params_arr = array()) {
        $module_arr = array();
        #####GENERATED_PUSHNOTIFY_MODULES_START#####
$module_arr["neworderlisting"] = $this->CI->lang->line("NEWORDERLISTING_NEW_ORDER_LISTING");
        #####GENERATED_PUSHNOTIFY_MODULES_END#####
        return $module_arr;
    }

    function getPushNotifyModuleListFields($module_name = "") {
        $data = array();
        switch ($module_name) {
            #####GENERATED_PUSHNOTIFY_LIST_FIELD_START#####

        case "neworderlisting" :
            $this->CI->load->model("neworderlisting/model_neworderlisting");
            $data = $this->CI->model_neworderlisting->getListConfiguration();
            break;
            #####GENERATED_PUSHNOTIFY_LIST_FIELD_END#####
        }
        return $data;
    }

    function getPushNotifyModuleFields($params_arr = array()) {
        $module_name = $params_arr["module_name"];
        $field_arr = array();
        switch ($module_name) {
            #####GENERATED_PUSHNOTIFY_FIELDS_START#####

                    case "neworderlisting" :
                        break;
                            #####GENERATED_PUSHNOTIFY_FIELDS_END#####
        }
        return $field_arr;
    }

    function getPushNotifyModuleData($module_name = "") {
        $data = array();
        switch ($module_name) {
            #####GENERATED_PUSHNOTIFY_DATA_START#####

        case "neworderlisting" :
            $this->CI->load->model("neworderlisting/model_neworderlisting");
            $extra_cond = $this->model_neworderlisting->extra_cond;
            $data = $this->CI->model_neworderlisting->getData($extra_cond, "", "", "", "", "Yes");
            break;
            #####GENERATED_PUSHNOTIFY_DATA_END#####
        }
        return $data;
    }

}

/* End of file Ci_misc.php */
/* Location: ./application/libraries/Ci_misc.php */