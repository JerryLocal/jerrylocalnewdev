<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Ci_admin
{

    protected $CI;

    function __construct()
    {
        $this->CI = & get_instance();
    }

    function addMultiLingualFile()
    {
        if ($this->CI->config->item('is_admin') == 1) {
            $multi_lingual_project = $this->CI->config->item('MULTI_LINGUAL_PROJECT');
            if ($multi_lingual_project == "Yes") {
                $default_lang = $this->CI->session->userdata("DEFAULT_LANG");
                $default_lang_folder = strtolower($default_lang);
                if (file_exists(APPPATH . "language" . DS . $default_lang_folder . DS . "general_lang.php")) {
                    $this->CI->lang->load('general', $default_lang_folder);
                } else {
                    $this->CI->lang->load('general', "en");
                }
            } else {
                $this->CI->lang->load('general', "en");
            }
        }
    }
}

/* End of file Ci_admin.php */
/* Location: ./application/libraries/Ci_admin.php */