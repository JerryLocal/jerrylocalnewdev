<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Wsexternal
{

    protected $CI;

    function __construct()
    {
        $this->CI = & get_instance();
        $this->CI->load->library('oauth_client');
        $this->CI->load->library('http');
    }

    /**
     * process_external_api method is used to perform exteranl api urls
     * @param array $ws_details to webservices inputs
     * @param array $input_params to input params
     * @param array $ws_header_input_arr to header input params
     * @param array $ws_url_input_arr to url input params
     * @return array 
     */
    function process_external_api($ws_details = array(), $input_params = array(), $ws_header_input_arr = array(), $ws_url_input_arr = array(), $ws_static_params = array(), $ws_flow_id = 0)
    {
        require_once(APPPATH . 'third_party/XML2Array/xml2Array.php');
        require_once(APPPATH . 'third_party/hash/Hash.php');
        if (empty($ws_url_input_arr)) {
            $ws_url_input_arr = $input_params;
        }
        $ws_url = $ws_details['api_url'];
        $ws_url_type = $ws_details['api_format'];
        $api_method = $ws_details['api_method'];
        $input_type = $ws_details['input_type'];
        $specific_key = $ws_details['loopingkey'];
        $array_type = $ws_details['selectkey'];
        $results_json = $ws_details['mainjson'];
        $sub_results_json = $ws_details['parentchildjson'];
        $json_post = $ws_details['json_post'];
        $request_fields = $ws_details["request_fields"];
        $function_arr = $ws_details['php_function'];
        $api_details = $ws_details['api_detail'];
        $static_values = $ws_details['static_values'];
        $oauth_ver = $api_details['vOauthVersion'];

        $results = json_decode($results_json, true);
        $sub_results = json_decode($sub_results_json, true);
        $sub_results = is_array($sub_results) ? array_filter($sub_results) : array();

        if (is_array($ws_url_input_arr) && count($ws_url_input_arr) > 0) {
            foreach ((array) $ws_url_input_arr as $key => $val) {
                $ws_url = str_replace("{%REQUEST." . $key . "%}", urlencode(trim($ws_url_input_arr[$key])), $ws_url);
            }
        }
        $extra_headers = array();
        if (is_array($ws_header_input_arr) && count($ws_header_input_arr) > 0) {
            foreach ((array) $ws_header_input_arr as $k => $v) {
                preg_match_all('/"(.*?)"/', $v, $matches);
                for ($j = 0; $j < count($matches[1]); $j++) {
                    $v = str_replace($matches[1][$j], urlencode(trim($matches[1][$j])), $v);
                }
                $extra_headers[] = $k . ":" . $v;
            }
        }

        try {
            if ($api_method == "GET") {

                if (is_array($input_params) && count($input_params) > 0) {
                    foreach ($input_params as $key => $val) {
                        if (is_array($request_fields) && count($request_fields) > 0) {
                            $request_key_arr = array_keys($request_fields);
                            if (in_array($key, $request_key_arr) && $val != '') {
                                $post_query .= "&" . $key . "=" . urlencode(trim($val));
                            }
                        }
                    }
                }
                if ($post_query != "") {
                    $post_query = utf8_encode(trim($post_query, "&"));
                    if (stristr($ws_url, "?") === false) {
                        $post_query = "?" . $post_query;
                    } else {
                        $post_query = "&" . $post_query;
                    }
                }
                $ws_url .= $post_query;

                if ($api_details == '') {
                    if (!empty($ws_header_input_arr)) {
                        foreach ($ws_header_input_arr as $key => $val) {
                            $ws_header_input_arr[$key] = urlencode($val);
                        }
                    }
                    $success = $this->CI->oauth_client->CallAPICustom($ws_url, $api_method, array(), array('FailOnAccessError' => true), $ws_header_input_arr, $upload);

                    $success = $this->CI->oauth_client->Finalize($success);
                    if (is_array($upload) || is_object($upload)) {
                        if (is_object($upload)) {
                            $upload = (array) $upload;
                        }
                        $result_str = json_encode($upload);
                        if ($ws_url_type == '') {
                            $ws_url_type = "JSON";
                        }
                    } else if (strpos($upload, '<') === 0) {
                        $result_str = $upload;
                        if ($ws_url_type == '') {
                            $ws_url_type = "XML";
                        }
                    }
                } else {

                    if ($api_details['vAuthType'] == 'noauth') {
                        // no auth versions
                        $result_str = $this->curl_get($ws_url, $extra_headers); //GET
                    } else {
                        // regular request
                        $this->CI->oauth_client->server = $ws_details['api_detail']['vApiName'];
                        $this->CI->oauth_client->client_id = $api_details['vClientId'];
                        $this->CI->oauth_client->client_secret = $api_details['vClientSecret'];
                        if (($success = $this->CI->oauth_client->Initialize($api_details))) {

                            if (intval($oauth_ver) == 1) {
                                $oauthtokenstr = $ws_header_input_arr['Authorization'];
                                $oauthtokenstrarr = explode(',', $oauthtokenstr);
                                foreach ($oauthtokenstrarr as $key => $value) {
                                    if (!empty($value) && (strstr($value, $api_details['vAccessTokenVariable']) || strstr($value, 'oauth_token'))) {
                                        $new_val = explode('=', $value);
                                        $auth_token = trim($new_val[1], '"');
                                    }
                                }
                                $access_token = array(
                                    "value" => $auth_token,
                                    "secret" => $ws_details['api_secret_key'],
                                    "authorized" => 1
                                );
                            } else {
                                $access_token = array(
                                    "value" => $input_params[$api_details['vAccessTokenVariable']],
                                    "authorized" => 1
                                );
                            }

                            $this->CI->oauth_client->StoreAccessToken($access_token);

                            if (($success = $this->CI->oauth_client->Process())) {

                                if (strlen($this->CI->oauth_client->access_token)) {

                                    $success = $this->CI->oauth_client->CallAPI($ws_url, $api_method, array(), array('FailOnAccessError' => true), $upload);
                                }
                            }
                            $success = $this->CI->oauth_client->Finalize($success);

                            if (is_array($upload) || is_object($upload)) {
                                if (is_object($upload)) {
                                    $upload = (array) $upload;
                                }
                                $result_str = json_encode($upload);
                                if ($ws_url_type == '') {
                                    $ws_url_type = "JSON";
                                }
                            } else if (strpos($upload, '<') === 0) {
                                $result_str = $upload;
                                if ($ws_url_type == '') {
                                    $ws_url_type = "XML";
                                }
                            }
                        }
                    }
                }
            } else if ($api_method == "JSONPOST" || $api_method == "XMLPOST") {

                if (is_array($_REQUEST) && count($_REQUEST) > 0) {
                    foreach ((array) $_REQUEST as $k => $v) {
                        $json_post = str_replace("{%REQUEST." . $k . "%}", $v, $json_post);
                    }
                }
                $post_params = $json_post;
                if ($ws_details['api_detail']['vApiName'] == '' || $ws_details['api_detail']['vAuthType'] == 'noauth') {
                    $success = $this->CI->oauth_client->CallAPICustom($ws_url, $api_method, $post_params, array_merge(array('FailOnAccessError' => true), $file_array), $ws_header_input_arr, $upload);
                } else {
                    $this->CI->oauth_client->server = $ws_details['api_detail']['vApiName'];
                    $this->CI->oauth_client->client_id = $api_details['vClientId'];
                    $this->CI->oauth_client->client_secret = $api_details['vClientSecret'];
                    if (($success = $this->CI->oauth_client->Initialize($api_details))) {
                        $oauthtokenstr = $_REQUEST['token'];
                        $oauthtokenstrarr = explode(',', $oauthtokenstr);
                        foreach ($oauthtokenstrarr as $key => $value) {
                            if (!empty($value) && strstr($value, $api_details['vAccessTokenVariable'])) {
                                $new_val = explode('=', $value);
                                $auth_token = trim($new_val[1], '"');
                            }
                        }
                        $access_token = array(
                            "value" => $auth_token,
                            "secret" => $ws_details['api_secret_key'],
                            "authorized" => 1
                        );

                        $this->CI->oauth_client->StoreAccessToken($access_token);
                        $this->CI->oauth_client->Process();

                        $this->CI->oauth_client->access_token = $auth_token;
                        if (strlen($this->CI->oauth_client->access_token)) {
                            $success = $this->CI->oauth_client->CallAPI($ws_url, $api_method, $post_params, array_merge(array('FailOnAccessError' => true), $file_array), $upload);
                        }
                    }
                }

                if (is_array($upload) || is_object($upload)) {
                    if (is_object($upload)) {
                        $upload = (array) $upload;
                    }
                    $result_str = json_encode($upload);
                    if ($ws_url_type == '') {
                        $ws_url_type = "JSON";
                    }
                } else if (strpos($upload, '<') === 0) {
                    $result_str = $upload;
                    if ($ws_url_type == '') {
                        $ws_url_type = "XML";
                    }
                }
            } else {
                /* POST CHANGES */
                $tokenstring = $input_params['access_token'];
                unset($input_params['access_token']);

                if ($input_type == 'raw') {
                    if (is_array($_REQUEST) && count($_REQUEST) > 0) {
                        foreach ((array) $_REQUEST as $k => $v) {
                            $json_post = str_replace("{%REQUEST." . $k . "%}", $v, $json_post);
                        }
                    }
                    $input_params = $json_post;
                    $RequestContentType = $ws_header_input_arr['Content-Type'];
                    $options = array('FailOnAccessError' => true, 'RequestContentType' => urldecode($RequestContentType));
                } else {
                    foreach ($ws_static_params as $key => $value) {
                        if ($value != '') {
                            preg_match_all("/{%REQUEST\.([a-zA-Z0-9_-]{1,})/i", $value, $preg_all_arr);
                            if (strstr($value, '{%REQUEST') !== false) {
                                if (isset($preg_all_arr[1]) && is_array($preg_all_arr[1]) && count($preg_all_arr[1]) > 0) {
                                    $file_parameter = $preg_all_arr[1][0];
                                    if (isset($_FILES[$file_parameter])) {
                                        $_FILES[$key] = $_FILES[$file_parameter];
                                        if ($file_parameter != $key) {
                                            unset($_FILES[$file_parameter]);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $file_params = $_FILES;

                    foreach ($file_params as $key => $val) {
                        if (isset($input_params[$key])) {
                            $input_params[$key] = $file_params[$key]['tmp_name'];
                        }
                    }

                    $file_array = array();
                    if (isset($file_params) && !empty($file_params)) {

                        $file_array['Files'] = array(
                            key($file_params) => array()
                        );
                    }
                    $input_params = array_filter($input_params);
                    $options = array_merge(array('FailOnAccessError' => true), $file_array);
                }

                if ($ws_details['api_detail']['vApiName'] == '' || $ws_details['api_detail']['vAuthType'] == 'noauth') {
                    foreach ($ws_header_input_arr as $key => $val) {
                        if ($key == 'Authorization') {
                            $ws_header_input_arr[$key] = urlencode($val);
                        }
                    }
                    $success = $this->CI->oauth_client->CallAPICustom($ws_url, $api_method, $input_params, $options, $ws_header_input_arr, $upload);
                } else {
                    $this->CI->oauth_client->server = $ws_details['api_detail']['vApiName'];
                    $this->CI->oauth_client->client_id = $api_details['vClientId'];
                    $this->CI->oauth_client->client_secret = $api_details['vClientSecret'];
                    if (($success = $this->CI->oauth_client->Initialize($api_details))) {

                        if (intval($oauth_ver) == 1) {
                            $oauthtokenstr = $ws_header_input_arr['Authorization'];
                            $oauthtokenstrarr = explode(',', $oauthtokenstr);
                            foreach ($oauthtokenstrarr as $key => $value) {
                                if (!empty($value) && (strstr($value, $api_details['vAccessTokenVariable']) || strstr($value, 'oauth_token'))) {
                                    $new_val = explode('=', $value);
                                    $auth_token = trim($new_val[1], '"');
                                }
                            }
                            $access_token = array(
                                "value" => $auth_token,
                                "secret" => $ws_details['api_secret_key'],
                                "authorized" => 1
                            );
                        } else {
                            if (strstr($tokenstring, $api_details['vAccessTokenVariable']) || strstr($tokenstring, 'oauth_token')) {
                                $new_val = explode('=', $tokenstring);
                                $auth_token = trim($new_val[1], '"');
                            }
                            $access_token = array(
                                "value" => $auth_token,
                                "authorized" => 1
                            );
                        }

                        $this->CI->oauth_client->StoreAccessToken($access_token);
                        $this->CI->oauth_client->Process();

                        $oauthtokenstr = $_REQUEST['token'];
                        $oauthtokenstrarr = explode(',', $oauthtokenstr);
                        foreach ($oauthtokenstrarr as $key => $value) {
                            if (!empty($value) && (strstr($value, $api_details['vAccessTokenVariable']) || strstr($value, 'oauth_token'))) {
                                $new_val = explode('=', $value);
                                $auth_token = trim($new_val[1], '"');
                            }
                        }
                        $this->CI->oauth_client->access_token = $auth_token;
                        if (strlen($this->CI->oauth_client->access_token)) {
                            $success = $this->CI->oauth_client->CallAPI($ws_url, $api_method, $input_params, array_merge(array('FailOnAccessError' => true), $file_array), $upload);
                        }
                    }
                    /* POST CHANGES */
                }

                $success = $this->CI->oauth_client->Finalize($success);
                if (is_array($upload) || is_object($upload)) {
                    if (is_object($upload)) {
                        $upload = (array) $upload;
                    }
                    $result_str = json_encode($upload);
                    if ($ws_url_type == '') {
                        $ws_url_type = "JSON";
                    }
                } else if (strpos($upload, '<') === 0) {
                    $result_str = $upload;
                    if ($ws_url_type == '') {
                        $ws_url_type = "XML";
                    }
                }
            }


            if ($ws_url_type == "JSON") {
                $output_array_temp = json_decode($result_str, 1);
            } else if ($ws_url_type == "XML") {
                if (!is_array($output_array_temp)) {
                    $output_array_temp = XML2Array::createArray($result_str);
                }
            } else if ($ws_url_type == "OTHER") {
                parse_str($result_str, $output_array_temp);
            }

            if ($array_type == "multiple") {
                $output_array['HBMPMASTER'] = $output_array_temp;
                $specific_key = str_replace("#*#", ".", $specific_key);
                $output_array1 = Hash::extract($output_array, $specific_key);
                $output_array['HBMPMASTER'] = $output_array1;
            } else {
                $output_array['HBMPMASTER'] = $output_array_temp;
            }
            $ser_output_array = Hash::flatten($output_array);
            $f_output_array = $inner_array = array();

            if ($array_type == "multiple") {
                if (is_array($ser_output_array) && count($ser_output_array) > 0) {
                    foreach ($ser_output_array as $k => $v) {
                        $k = str_replace("HBMPMASTER.", "", $k);
                        foreach ((array) $results as $key => $val) {
                            $old_val = $val;
                            $last_key = end(explode(".", $key));
                            $first_key = current(explode(".", $val));
                            $first_k = current(explode(".", $k));

                            $first_arr = explode(".", $k);
                            $first_key_arr = explode(".", $val);

                            $val = preg_replace('/' . $first_key . '/', $first_k, $val, 1);
                            $key = preg_replace('/' . $first_key . '/', $first_k, $key, 1);

                            if ($sub_results[$old_val] == "yes") {
                                foreach ($sub_results as $nk => $nv) {
                                    if ($nk == $old_val) {
                                        break;
                                    }
                                    if (strpos($val, $nk) === 0) {
                                        $ink = current(explode(".", str_replace($nk . ".", "", $k)));
                                        $rpk = current(explode(".", str_replace($nk . ".", "", $val)));
                                        $val = str_replace($nk . '.' . $rpk, $nk . "." . $ink, $val);
                                        $inner_array[$nk . '.0'] = $nk . "." . $ink;
                                    }
                                }
                                $looping_key = $val;
                                $last_looping_key = end(explode(".", $val));
                            }
                            foreach ($inner_array as $ik => $iv) {
                                if (strpos($val, $ik) === 0) {
                                    $val = $key = str_replace($ik, $iv, $val);
                                }
                            }
                            if (strpos($k, $looping_key) === 0) {
                                $new_k = str_replace($looping_key . ".", "", $k);
                                $new_k_a = current(explode(".", $new_k));
                                $val = str_replace($last_looping_key . ".0", $last_looping_key . "." . $new_k_a, $val);
                                $key = str_replace($last_looping_key . ".0", $last_looping_key . "." . $new_k_a, $key);
                            }
                            if ($k === $val) {
                                $val = "HBMPMASTER." . $val;
                                $hm_arr = Hash::extract($output_array, $val);
                                $hm_key = key($hm_arr);
                                $current_key_arr = explode(".", $key);
                                array_pop($current_key_arr);
                                $new_key = implode(".", $current_key_arr);
                                $temp_str = Hash::extract($f_output_array, $new_key);
                                if (!is_array($temp_str[0]) && $temp_str[0] != "") {
                                    $inner_curr_key = array_pop($current_key_arr);
                                    $temp_arr = array();
                                    $temp_arr[$inner_curr_key] = $temp_str[0];
                                    $f_output_array = Hash::insert($f_output_array, $new_key, $temp_arr);
                                }
                                $current_key_arr = explode(".", $key);
                                $last_key = end($current_key_arr);
                                if (is_numeric($last_key)) {
                                    array_pop($current_key_arr);
                                    $new_key = implode(".", $current_key_arr);
                                    $ret_hm_arr = $this->apply_php_function($hm_arr, $function_arr);
                                    $f_output_array = Hash::insert($f_output_array, $new_key, $ret_hm_arr);
                                } else {
                                    $ret_hm_arr = $this->apply_php_function($hm_arr, $function_arr, $last_key);
                                    $f_output_array = Hash::insert($f_output_array, $key, $ret_hm_arr[$hm_key]);
                                }
                            }
                        }
                    }
                }
            } else {
                if (is_array($results) && count($results) > 0) {
                    foreach ((array) $results as $key => $val) {
                        if (is_array($ser_output_array) && count($ser_output_array) > 0) {
                            foreach ((array) $ser_output_array as $k => $v) {
                                $old_val = $val;
                                $new_k = str_replace("HBMPMASTER.", "", $k);
                                $first_k = current(explode(".", $new_k));
                                $first_key = current(explode(".", $val));

                                $first_arr = explode(".", $new_k);
                                $first_key_arr = explode(".", $val);

                                foreach ($results as $k1 => $v1) {
                                    if ($sub_results[$v1] == "yes") {
                                        if (strpos($val, $v1) === 0) {
                                            if (strpos($new_k, $v1) === 0) {
                                                $llop = str_replace($v1 . ".", "", $new_k);
                                                $llop_elem = current(explode(".", $llop));
                                                $llop1 = str_replace($v1 . ".", "", $val);
                                                $llop_elem1 = current(explode(".", $llop1));
                                                $val = str_replace($v1 . "." . $llop_elem1, $v1 . "." . $llop_elem, $val);
                                                $key = str_replace($k1 . "." . $llop_elem1, $k1 . "." . $llop_elem, $key);
                                            }
                                        }
                                    }
                                }
                                $new_val = $val;
                                $new_key = $key;
                                if ($sub_results[$old_val] == "yes") {
                                    $looping_key = $val;
                                    $looping_left_key = $key;
                                    if (strpos($val, '.') !== false) {
                                        $last_looping_key = end(explode(".", $val));
                                    } else {
                                        $last_looping_key = $val;
                                    }
                                }

                                $new_val_t = str_replace("^", ".", $new_val);

                                if ($new_k === $new_val_t) {
                                    $val_temp = "HBMPMASTER." . $new_val;

                                    $hm_arr = Hash::extract($output_array, $val_temp);
                                    if (count($hm_arr) > 0) {
                                        $hm_key = key($hm_arr);
                                        $current_key_arr = explode(".", $key);
                                        array_pop($current_key_arr);
                                        $new_key_1 = implode(".", $current_key_arr);
                                        $temp_str = Hash::extract($f_output_array, $new_key_1);
                                        if (!is_array($temp_str[0]) && $temp_str[0] != "") {
                                            $inner_curr_key = array_pop($current_key_arr);
                                            $temp_arr = array();
                                            $temp_arr[$inner_curr_key] = $temp_str[0];
                                            $f_output_array = Hash::insert($f_output_array, $new_key_1, $temp_arr);
                                        }
                                        $current_key_arr = explode(".", $new_key);
                                        $last_key = end($current_key_arr);
                                        if (is_numeric($last_key)) {
                                            array_pop($current_key_arr);
                                            $new_key_2 = implode(".", $current_key_arr);
                                            $ret_hm_arr = $this->apply_php_function($hm_arr, $function_arr);
                                            $f_output_array = Hash::insert($f_output_array, $new_key_2, $ret_hm_arr);
                                        } else {
                                            $ret_hm_arr = $this->apply_php_function($hm_arr, $function_arr, $last_key);
                                            $f_output_array = Hash::insert($f_output_array, $new_key, $ret_hm_arr[$hm_key]);
                                        }
                                    } else {
                                        $val_temp = str_replace("^", ".", $val_temp);
                                        $hm_value = $ser_output_array[$val_temp];
                                        $hm_arr[] = $hm_value;

                                        $ret_hm_arr = $this->apply_php_function($hm_arr, $function_arr, $last_key);
                                        $f_output_array = Hash::insert($f_output_array, $new_key, $ret_hm_arr);
                                    }
                                }
                            }
                        }
                    }
                }
                if (is_array($f_output_array[0]) && count($f_output_array[0]) > 0) {
                    $f_output_array = $f_output_array[0];
                }
            }
            $success = 1;
            $message = "API executed successfully.";
        } catch (Exception $e) {
            $success = 0;
            $message = 'Something goes wrong. ' . $e->getMessage();
            $f_output_array = array();
        }
        $status_array = array();
        if (!empty($output_array_temp['error']['message'])) {
            $f_output_array[0]['error_message'] = $status_array['error_message'] = $output_array_temp['error']['message'];
            $f_output_array[0]['error_type'] = $status_array['error_type'] = $output_array_temp['error']['type'];
            $f_output_array[0]['error_code'] = $status_array['error_code'] = $output_array_temp['error']['code'];
        } else if (!empty($this->CI->oauth_client->error)) {
            $f_output_array[0]['error_message'] = $status_array['error_code'] = $this->CI->oauth_client->error;
        }
        $status_array['status_code'] = $this->CI->oauth_client->response_status;

        $ret_arr["success"] = $success;
        $ret_arr["message"] = $message;
        $ret_arr["status"] = $status_array;
        $ret_arr["data"] = $f_output_array;

        return $ret_arr;
    }

    /**
     * curl_post method is used to send a POST request using cURL 
     * @param string $url to request specified url
     * @param array $post values to send post request params
     * @param array $header_params to send extra header information
     * @param string $ws_method to process different post formats
     * @param array $options for cURL execution
     * @return string 
     */
    function curl_post($url = '', $post = NULL, $header_params = array(), $ws_method = '', array $options = array())
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        if ($ws_method == "JSONPOST" || $ws_method == "XMLPOST") {
            if ($post !== NULL) {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_POST, TRUE);

                $str = $post;
                curl_setopt($curl, CURLOPT_POSTFIELDS, $str);

                if ($ws_method == "JSONPOST") {
                    $str_val = 'application/json';
                } else {
                    $str_val = 'text/xml';
                }
                $header_params = (is_array($header_params)) ? $header_params : array();
                $header_params = array_merge($header_params, array(
                    'Content-Type: ' . $str_val,
                    'Content-Length: ' . strlen($post))
                );
            }
        } else if ($ws_method == "PUT") {
            if ($post !== NULL) {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                $str = http_build_query($post);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
            }
        } else if ($ws_method == "DELETE") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        } else {
            if ($post !== NULL) {
                curl_setopt($curl, CURLOPT_POST, TRUE);
                $str = http_build_query($post);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
            }
        }
        if ($proxy != NULL) {
            curl_setopt($curl, CURLOPT_PROXY, $proxy);
        }
        if ($userpass != NULL) {
            curl_setopt($curl, CURLOPT_USERPWD, $userpass);
        }
        curl_setopt($curl, CURLOPT_USERAGENT, 'Googlebot/2.1 (http://www.googlebot.com/bot.html)');
        curl_setopt($curl, CURLOPT_REFERER, "http://www.google.com/bot.html");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);
        curl_setopt($curl, CURLOPT_COOKIEJAR, "cookie.txt");
        curl_setopt($curl, CURLOPT_COOKIEFILE, "cookie.txt");
        #curl_setopt($curl, CURLOPT_MAXREDIRS, 5);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLINFO_HEADER_OUT, TRUE);
        if (is_array($header_params) && count($header_params) > 0) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header_params);
        }

        $contents = curl_exec($curl);
        if ($ws_method == "DELETE" || $ws_method == "PUT") {
            if (trim($contents) == "") {
                $contents_arr["success"] = "1";
                $contents_arr["response"] = "success";
                $contents = json_encode($contents_arr);
            }
        } else if (!$contents) {
            $contents_arr["success"] = "0";
            $contents_arr['message'] = curl_error($curl);
            $contents = json_encode($contents_arr);
        } else {
            $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $header_response = substr($contents, 0, $header_size);
            $contents = substr($contents, $header_size);
            $header_parse_arr = $this->get_headers_from_curl_response($header_response);
            if ($header_parse_arr['Content-Encoding'] == "gzip") {
                $contents = gzdecode($contents);
            } else if ($header_parse_arr['Content-Encoding'] == "deflate") {
                $contents = gzinflate($contents);
            }
        }
        curl_close($curl);

        return $contents;
    }

    /**
     * curl_get method is used to send a GET request using cURL 
     * @param string $url to request specified url
     * @param array $extra_header to send extra header information
     * @param array $get values to send get request params
     * @param array $options for cURL execution
     * @return string 
     */
    function curl_get($url = '', array $extra_header = array(), array $get = NULL, array $options = array())
    {
        global $index;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        if ($post_data !== NULL) {
            curl_setopt($curl, CURLOPT_POST, TRUE);
            $str = http_build_query($post_data);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
        }
        if ($proxy != NULL) {
            curl_setopt($curl, CURLOPT_PROXY, $proxy);
        }
        if ($userpass != NULL) {
            curl_setopt($curl, CURLOPT_USERPWD, $userpass);
        }
        curl_setopt($curl, CURLOPT_USERAGENT, 'Googlebot/2.1 (http://www.googlebot.com/bot.html)');
        curl_setopt($curl, CURLOPT_REFERER, "http://www.google.com/bot.html");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);
        curl_setopt($curl, CURLOPT_COOKIEJAR, "cookie.txt");
        curl_setopt($curl, CURLOPT_COOKIEFILE, "cookie.txt");
        #curl_setopt($curl, CURLOPT_MAXREDIRS, 5);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLINFO_HEADER_OUT, TRUE);
        if (count($extra_header) > 0) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $extra_header);
        }
        if (!$contents = curl_exec($curl)) {
            $contents = file_get_contents($url);
        } else {
            $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $header_response = substr($contents, 0, $header_size);
            $contents = substr($contents, $header_size);
            $header_parse_arr = $this->get_headers_from_curl_response($header_response);
            if ($header_parse_arr['Content-Encoding'] == "gzip") {
                $contents = gzdecode($contents);
            } else if ($header_parse_arr['Content-Encoding'] == "deflate") {
                $contents = gzinflate($contents);
            }
        }

        curl_close($curl);
        $index++;
        return $contents;
    }

    function get_headers_from_curl_response($response = '')
    {
        $headers = array();
        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));
        foreach (explode("\r\n", $header_text) as $i => $line) {
            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                list ($key, $value) = explode(': ', $line);
                $headers[$key] = $value;
            }
        }
        return $headers;
    }

    /**
     * apply_php_function method is used to perform php function on external api params
     * @param array $arr to perform function execution on params
     * @param array $function_arr to specifiy respected functions
     * @param string $key to send extra key information
     * @return array 
     */
    function apply_php_function($arr = array(), $function_arr = array(), $key = '')
    {
        if (is_array($arr) && count($arr) > 0) {
            foreach ((array) $arr as $k => $v) {
                if ($key != '') {
                    $func_key = $key;
                } else {
                    $func_key = $k;
                }
                if (isset($function_arr[$func_key])) {
                    $php_function = $function_arr[$func_key];
                    if (function_exists($php_function)) {
                        $fvalue[$k] = call_user_func($php_function, $v);
                    } else if (method_exists($this->CI->general, $php_function)) {
                        $fvalue[$k] = $this->CI->general->$php_function($v);
                    }
                } else {
                    $fvalue = $arr;
                }
            }
        }
        return $fvalue;
    }

    function get_parent_childpath($to_hierarchy = array(), $key = '0', $str_key = '')
    {
        $str = "";
        $str_key .= $key . ".";

        $new_array = array();
        if (is_array($to_hierarchy) && count($to_hierarchy) > 0) {
            foreach ($to_hierarchy as $key => $val) {
                if (isset($val['children']) && count($val['children']) > 0) {
                    if ($val['nested'] == "yes") {
                        $val['id'] = $val['id'] . ".0";
                        $li_id = trim($str_key . $val['id'], ".");
                        $new_array[$li_id] = "";
                    } else {
                        $li_id = trim($str_key . $val['id'], ".");
                    }
                    $new_array[$li_id] = "";
                    $ret_arr = $this->get_parent_childpath($val['children'], $val['id'], $str_key);
                    if (is_array($ret_arr) && is_array($new_array))
                        $new_array = Hash::merge($new_array, $ret_arr);
                } else {
                    $li_id = trim($str_key . $val['id'], ".");
                    $ret_arr[$li_id] = "";
                    if (is_array($ret_arr) && is_array($new_array))
                        $new_array = Hash::merge($new_array, $ret_arr);
                }
            }
        }
        return $new_array;
    }
}

/* End of file Wsexternal.php */
/* Location: ./application/libraries/Wsexternal.php */
