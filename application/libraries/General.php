<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class General {

    protected $CI;
    protected $_email_subject;
    protected $_email_content;
    protected $_push_content;
    protected $_notify_error;
    protected $_expression_eval;
    protected $_aws_avial_obj;
    protected $_aws_avail_buckets;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->db->max_fetch_limit = $this->CI->config->item('db_max_limit');
    }

    function custom_stripslashes($input = '') {
        return stripslashes($input);
    }

    function force_download($file, $dir) {
        if ((isset($file)) && (file_exists($dir . $file))) {
            header("Content-type: application/force-download");
            header('Content-Disposition: inline; filename="' . $dir . $file . '"');
            header("Content-Transfer-Encoding: Binary");
            header("Content-length: " . filesize($dir . $file));
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile("$dir$file");
        } else {
            echo "No file selected";
        } //end if 
        exit;
    }

    function custom_file_put_contents($file_name, $data) {
        file_put_contents($file_name, $data);
    }

    function custom_file_exist($path) {
        $flag = file_exists($path);
        return $flag;
    }

    function custom_filesize($path) {
        filesize($path);
    }

    function custom_readfile($path) {
        readfile($readfile);
    }

    function custom_error() {
        error_reporting(E_ALL);
    }

    public function logOutChecking() {
        $ret = FALSE;
        if (!$this->CI->session->userdata("isLoggedIn")) {
            $ret = TRUE;
        } else if ($this->CI->session->userdata("timeOut") > 0) {
            $diffTime = (time() - ($this->CI->session->userdata('loggedAt'))) / 60;
            if ($diffTime > ($this->CI->session->userdata('timeOut'))) {
                $ret = TRUE;
            }
        }
        if ($ret === TRUE) {
            $currArr['hashVal'] = $_REQUEST['hashValue'];
            $this->logInOutEntry($this->CI->session->userdata("iAdminId"), 'Admin', $currArr);
            $this->CI->session->sess_destroy();
        }
        return $ret;
    }

    public function logInOutEntry($id = '', $user_type = 'Admin', $extra_arr = array()) {
        $this->CI->load->model('user/model_loghistory');
        $this->CI->load->model('user/model_admin');
        $data = $log = array();
        $data["dLastAccess"] = date("Y-m-d H:i:s");
        $res_admin = $this->CI->model_admin->update($data, $id);
        if ($this->CI->session->userdata("iLogId") != '') {
            $log['vCurrentUrl'] = $extra_arr['hashVal'];
            $log['dLogoutDate'] = date("Y-m-d H:i:s");
            $res_log = $this->CI->model_loghistory->update($log, $this->CI->session->userdata("iLogId"));
        } else {
            $log['iUserId'] = $id;
            $log['vIP'] = $this->getHTTPRealIPAddr();
            $log['eUserType'] = $user_type;
            $log['dLoginDate'] = date("Y-m-d H:i:s");
            $log_id = $this->CI->model_loghistory->insert($log);
            $this->CI->session->set_userdata("iLogId", $log_id);
        }
    }

    public function closedFancyFrame() {
        if ($_REQUEST['hideCtrl'] == "true") {
            return "true";
        } else {
            return "false";
        }
    }

    public function getcopyrighttext() {
        $copyrighttext = str_replace("#CURRENT_YEAR#", date('Y'), $this->CI->systemsettings->getSettings('COPYRIGHTED_TEXT'));
        $copyrighttext = str_replace("#COMPANY_NAME#", $this->CI->systemsettings->getSettings('COMPANY_NAME'), $copyrighttext);
        return $copyrighttext;
    }

    /** Generalized functions :: START */
    public function getRandomNumber($len = "15") {
        $better_token = strtoupper(md5(uniqid(rand(), TRUE)));
        $better_token = substr($better_token, 1, $len);
        return $better_token;
    }

    public function truncateChars($str = '', $len = 25) {
        if (trim($str) == '' || $len < 3) {
            return $str;
        }
        if (strlen($str) > $len) {
            return substr($str, 0, ($len - 3)) . "...";
        } else {
            return $str;
        }
    }

    public function getHTTPRealIPAddr() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED'];
        } else if (!empty($_SERVER['HTTP_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (!empty($_SERVER['HTTP_FORWARDED'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_FORWARDED'];
        } else { // return remote address
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function getDateOnly($date = '') {
        if ($date != '0000-00-00' && $date != '0000-00-00 00:00:00' && $date != '') {
            return date("M d, Y", strtotime($date));
        } else {
            return '---';
        }
    }

    public function getDayOnly($date = '') {
        if ($date != '0000-00-00' && $date != '0000-00-00 00:00:00' && $date != '') {
            return date("l", strtotime($date));
        } else {
            return '---';
        }
    }

    public function getTimeOnly($date = '') {
        if ($date != '0000-00-00' && $date != '0000-00-00 00:00:00' && $date != '') {
            return date("h:i A", strtotime($date));
        } else {
            return '---';
        }
    }

    public function getDateTime($date, $format, $top = FALSE) {
        if ($date != '0000-00-00' && $date != '0000-00-00 00:00:00' && $date != '') {
            return ($top) ? date($format, $date) : date($format, strtotime($date));
        } else {
            return '---';
        }
    }

    public function dateSystemFormat($value = '') {
        $format = $this->getAdminPHPFormats("date");
        if ($format == "d/m/Y") {
            return $this->dateCustomFormat($format, $value);
        } else {
            return $this->dateDefinedFormat($format, $value);
        }
    }

    public function dateTimeSystemFormat($value = '') {
        $format = $this->getAdminPHPFormats("date_and_time");
        if ($format == "d/m/Y h:i A") {
            return $this->dateTimeCustomFormat($format, $value);
        } else {
            return $this->dateTimeDefinedFormat($format, $value);
        }
    }

    public function timeSystemFormat($value = '') {
        $format = $this->getAdminPHPFormats("time");
        return $this->timeDefinedFormat($format, $value);
    }

    public function dateDefinedFormat($format = '', $value = '') {
        if ($format == '' || $value == '') {
            return '';
        } else if ($value == "0000-00-00" || $value == "0000-00-00 00:00:00" || $value == "00:00:00") {
            return '';
        }
        return date($format, strtotime($value));
    }

    public function dateTimeDefinedFormat($format = '', $value = '') {
        if ($format == '' || $value == '') {
            return '';
        } else if ($value == "0000-00-00" || $value == "0000-00-00 00:00:00" || $value == "00:00:00") {
            return '';
        }
        return date($format, strtotime($value));
    }

    public function timeDefinedFormat($format = '', $value = '') {
        if ($format == '' || $value == '') {
            return '';
        } else if ($value == "0000-00-00" || $value == "0000-00-00 00:00:00" || $value == "00:00:00") {
            return '';
        }
        return date($format, strtotime($value));
    }

    public function dateCustomFormat($format = '', $value = '') {
        if ($format == '' || $value == '') {
            return '';
        } else if ($value == "0000-00-00" || $value == "0000-00-00 00:00:00" || $value == "00:00:00") {
            return '';
        }
        if ($format == 'd/m/Y') {
            $format = 'd.m.Y';
        }
        $value = date($format, strtotime($value));
        $value = str_replace(".", "/", $value);
        return $value;
    }

    public function dateTimeCustomFormat($format = '', $value = '') {
        if ($format == '' || $value == '') {
            return '';
        } else if ($value == "0000-00-00" || $value == "0000-00-00 00:00:00" || $value == "00:00:00") {
            return '';
        }
        if ($format == 'd/m/Y h:i A') {
            $format = 'd.m.Y h:i A';
        }
        $value = date($format, strtotime($value));
        $value = str_replace(".", "/", $value);
        return $value;
    }

    public function formatServerDate($date_format = '', $new_date = '') {
        if ($new_date == "" || $new_date == "0000-00-00" || $new_date == "0000-00-00 00:00:00" || $new_date == "00:00:00") {
            return $new_date;
        }
        if (in_array($date_format, array('d/m/Y', 'd.m.Y')) && strstr($new_date, "/") !== FALSE) {
            $new_date_arr = @explode("/", $new_date);
            $new_date = $new_date_arr[2] . "-" . $new_date_arr[1] . "-" . $new_date_arr[0];
        }
        $return_date = (strtotime($new_date)) ? date("Y-m-d", strtotime($new_date)) : "";
        return $return_date;
    }

    public function formatServerDateTime($date_format = '', $new_date = '') {
        if ($new_date == "" || $new_date == "0000-00-00" || $new_date == "0000-00-00 00:00:00" || $new_date == "00:00:00") {
            return $new_date;
        }
        if (in_array($date_format, array('d/m/Y h:i A', 'd.m.Y h:i A')) && strstr($new_date, "/") !== FALSE) {
            $new_date_arr = @explode("/", $new_date);
            $new_date = $new_date_arr[0] . "-" . $new_date_arr[1] . "-" . $new_date_arr[2];
        }
        $return_date = (strtotime($new_date)) ? date("Y-m-d H:i:s", strtotime($new_date)) : "";
        return $return_date;
    }

    public function navigationDateTime($value = '') {
        if ($value == '') {
            return '---';
        } else if ($value == "0000-00-00" || $value == "0000-00-00 00:00:00" || $value == "00:00:00") {
            return '---';
        }
        return date("M d, y h:i:s a", strtotime($value));
    }

    public function getDateTimeToDisplay($date = '') {
        $retval = "---";
        if (trim($date) != "" && trim($date) != "0000-00-00 00:00:00" && trim($date) != "0000-00-00") {
            $retval = date("d/m/Y h:i A", strtotime($date));
        }

        return $retval;
    }

    public function getDateToDisplay($date = '') {
        $retval = "---";
        if (trim($date) != "" && trim($date) != "0000-00-00 00:00:00" && trim($date) != "0000-00-00") {
            $retval = date("d/m/Y", strtotime($date));
        }
        return $retval;
    }

    public function getPhoneMaskedView($format = '', $value = '') {
        if ($value == '') {
            return '---';
        }
        $format = ($format != "") ? $format : $this->CI->config->item("ADMIN_PHONE_FORMAT");
        $splitFormat = str_split(trim($format));
        $splitValue = str_split(trim($value));
        $retPhone = '';
        for ($i = 0, $j = 0; $i < count($splitFormat); $i++) {
            if (ctype_alnum($splitFormat[$i]) || $splitFormat[$i] == "*") {
                $retPhone .= $splitValue[$j];
                $j++;
            } else {
                $retPhone .= $splitFormat[$i];
            }
        }
        return $retPhone;
    }

    public function getPhoneUnmaskedView($format = '', $value = '') {
        if ($value == '') {
            return '';
        }
        $format = ($format != "") ? $format : $this->CI->config->item("ADMIN_PHONE_FORMAT");
        $splitFormat = str_split(trim($format));
        $splitValue = str_split(trim($value));
        $retPhone = '';
        for ($i = 0; $i < count($splitValue); $i++) {
            if (ctype_alnum($splitValue[$i])) {
                $retPhone .= $splitValue[$i];
            }
        }
        return $retPhone;
    }

    public function renderAdminURL($link = '', $module = '') {
        $external_url = $this->isExternalURL($link);
        $admin_url = $this->CI->config->item("admin_url");
        if ($external_url) {
            $return_link = $string;
        } else {
            $return_link = $admin_url . "#" . $this->getAdminEncodeURL($link, 0);
        }
        return $return_link;
    }

    public function isExternalURL($url = '') {
        $flag = FALSE;
        if ($url != "") {
            $url = strtolower(trim($url));
            if (substr($url, 0, 8) == 'https://')
                $flag = TRUE;
            else if (substr($url, 0, 7) == 'http://')
                $flag = TRUE;
        }
        return $flag;
    }

    public function getCurlResponse($data_url = "") {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_URL, $data_url);
        $data = curl_exec($ch);
        if (in_array(intval(curl_errno($ch)), array(1, 2, 3, 5, 6, 7, 8))) {
            $data = @file_get_contents($data_url);
        }
        curl_close($ch);
        return $data;
    }

    public function forbidden_message($err_message = '') {
        $render_arr['err_message'] = $err_message;
        echo $this->CI->parser->parse($this->CI->config->item('ADMIN_FORBIDDEN_TEMPLATE') . ".tpl", $render_arr, TRUE);
        exit;
    }

    public function from_camel_case($str = '') {
        $str = substr($str, 1);
        $str[0] = strtolower($str[0]);
        $func = create_function('$c', 'return "_" . strtolower($c[1]);');
        return preg_replace_callback('/([A-Z])/', $func, $str);
    }

    public function to_camel_case($str = '') {
        $str = substr($str, 1);
        $str[0] = strtolower($str[0]);
        $func = create_function('$c', 'return "" . strtoupper($c[1]);');
        return preg_replace_callback('/(_)/', $func, $str);
    }

    public function concat_string($str_1 = '', $str_2 = '') {
        return $str_1 . $str_2;
    }

    public function escape_str($str = '') {
        return $this->CI->db->escape($str);
    }

    public function escape_like_str($str = '') {
        return $this->CI->db->escape_like_str($str);
    }

    /** Generalized functions :: END */
    public function get_image($params = array()) {

        if (isset($params['pk']) && $params['pk'] != "") {
            $folder_name = $params['pk'];
        } else {
            $folder_name = "";
        }
        #$file_name = $params['prefix'] . $params['image_name'];
        $file_name = $params['image_name'];
        $path = $params['path'];
        $ext = $params['ext'];
        $color = $params['color'];
        $default_arr = $this->CI->config->item('IMAGE_EXTENSION_ARR');
        $extension_arr = @explode(",", $ext);
        $intersect_arr = array_intersect($default_arr, $extension_arr);
        if (is_array($intersect_arr) && count($intersect_arr) > 0) {
            $default_image = $this->CI->config->item('images_url') . "noimage.gif";
        } else {
            $default_image = "";
        }
        $final_image_url = '';
        if ($path != "") {
            $image_path = $this->CI->config->item('upload_path') . $path . DS;
            $image_url = $this->CI->config->item('upload_url') . $path . "/";
            if ($folder_name != "") {
                $image_final_path = $image_path . $folder_name . DS;
                $image_final_url = $image_url . $folder_name . "/";
            } else {
                $image_final_path = $image_path;
                $image_final_url = $image_url;
            }
            if (file_exists($image_final_path . $file_name) && $file_name != "") {
                if (trim($params['height']) != "" || trim($params['width']) != "") {
                    $enc_image_url = base64_encode($image_final_url . $file_name);
                    $final_image_url = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=' . $params['height'] . '&width=' . $params['width'] . "&color=" . $color;
                } else {
                    $final_image_url = $image_final_url . $file_name;
                }
                return $final_image_url;
            }
        }
        if ($default_image != '' && (trim($params['height']) != "" || trim($params['width']) != "")) {
            $enc_image_url = base64_encode($default_image);
            $final_image_url = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=' . $params['height'] . '&width=' . $params['width'] . "&color=" . $color;
        } else {
            $final_image_url = $default_image;
        }
        return $final_image_url;
    }

    public function get_image_server($image_arr = array()) {
        $folder_arr = $this->getServerUploadPathURL($image_arr['path']);
        $ext = $image_arr['ext'];
        $default_arr = $this->CI->config->item('IMAGE_EXTENSION_ARR');
        $extension_arr = @explode(",", $ext);
        $intersect_arr = array_intersect($default_arr, $extension_arr);
        if (is_array($intersect_arr) && count($intersect_arr) > 0) {
            $default_image = $this->CI->config->item('images_url') . "noimage.gif";
        } else {
            $default_image = "";
        }
        if ($folder_arr['status']) {
            $original_path = $folder_arr['folder_path'];
            $original_url = $folder_arr['folder_url'];
            $file_name = $image_arr['image_name'];
            if (isset($image_arr['pk']) && $image_arr['pk'] != "") {
                $image_final_path = $original_path . $image_arr['pk'] . DS;
            } else {
                $image_final_path = $original_path;
            }
            $source_url = $original_url . md5($image_final_path) . "/" . $file_name;
            $file_exists = $this->checkFileExistsOnServer($source_url);
            $color = $image_arr['color'];

            if ($file_exists && $file_name != '') {
                if ((trim($image_arr['height']) != "" || trim($image_arr['width']) != "")) {
                    $enc_image_url = base64_encode($source_url);
                    $final_image_url = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=' . $image_arr['height'] . '&width=' . $image_arr['width'] . "&color=" . $color;
                } else {
                    $final_image_url = $source_url;
                }
            } else if ($default_image != "" && (trim($image_arr['height']) != "" || trim($image_arr['width']) != "")) {
                $enc_image_url = base64_encode($default_image);
                $final_image_url = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=' . $image_arr['height'] . '&width=' . $image_arr['width'] . "&color=" . $color;
            } else {
                $final_image_url = $default_image;
            }
        } else {
            if ($default_image != "" && (trim($image_arr['height']) != "" || trim($image_arr['width']) != "")) {
                $enc_image_url = base64_encode($default_image);
                $final_image_url = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=' . $image_arr['height'] . '&width=' . $image_arr['width'] . "&color=" . $color;
            } else {
                $final_image_url = $default_image;
            }
        }
        return $final_image_url;
    }

    public function get_image_aws($image_arr = array()) {
        $folder_arr = $this->getAWSServerUploadPathURL($image_arr['path']);
        $ext = $image_arr['ext'];
        $default_arr = $this->CI->config->item('IMAGE_EXTENSION_ARR');
        $extension_arr = @explode(",", $ext);
        $intersect_arr = array_intersect($default_arr, $extension_arr);
        if (is_array($intersect_arr) && count($intersect_arr) > 0) {
            $default_image = $this->CI->config->item('images_url') . "noimage.gif";
        } else {
            $default_image = "";
        }
        if ($folder_arr['status']) {
            if ($image_arr['pk'] != "") {
                $original_url = $folder_arr['folder_url'] . $image_arr['pk'] . '/';
                $file_path = $image_arr['path'] . $image_arr['pk'] . '/';
                $bucket_folder = $folder_arr['bucket_folder'] . '/' . $image_arr['pk'];
            } else {
                $original_url = $folder_arr['folder_url'];
                $file_path = $image_arr['path'];
                $bucket_folder = $folder_arr['bucket_folder'];
            }
            $file_name = $image_arr['image_name'];
            $source_url = $original_url . $file_name;
            #$file_exists = $this->checkFileExistsOnAWSServer($file_path, $file_name);
            $file_exists = $this->checkFileExistsOnAWSObject($folder_arr['bucket_files'], $file_name, $folder_arr['bucket_name'], $bucket_folder);
            $color = $image_arr['color'];

            $default_image = $this->CI->config->item('images_url') . "noimage.gif";

            if ($file_exists && $file_name != '') {
                if ((trim($image_arr['height']) != "" || trim($image_arr['width']) != "")) {
                    $enc_image_url = base64_encode($source_url);
                    $final_image_url = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=' . $image_arr['height'] . '&width=' . $image_arr['width'] . "&color=" . $color;
                } else {
                    $final_image_url = $source_url;
                }
            } else if ($default_image != "" && (trim($image_arr['height']) != "" || trim($image_arr['width']) != "")) {
                $enc_image_url = base64_encode($default_image);
                $final_image_url = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=' . $image_arr['height'] . '&width=' . $image_arr['width'] . "&color=" . $color;
            } else {
                $final_image_url = $default_image;
            }
        } else {
            if ($default_image != "" && (trim($image_arr['height']) != "" || trim($image_arr['width']) != "")) {
                $enc_image_url = base64_encode($default_image);
                $final_image_url = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=' . $image_arr['height'] . '&width=' . $image_arr['width'] . "&color=" . $color;
            } else {
                $final_image_url = $default_image;
            }
        }
        return $final_image_url;
    }

    public function file_upload($photopath = '', $vphoto = '', $vphoto_name = '', $vaild_ext = '') {
        $msg = "";
        $vphotofile = '';
        if (!empty($vphoto_name) && is_file($vphoto)) {
            // Remove Dots from File name
            $file_arr = explode(".", $vphoto_name);
            $ext = $file_arr[count($file_arr) - 1];
            $vaild_ext_arr = explode(",", strtoupper($vaild_ext));
            if (trim($vaild_ext) == "" || trim($vaild_ext) == "*" || in_array(strtoupper($ext), $vaild_ext_arr)) {
                //$vphotofile=$file.".".$ext;
                $vphotofile = $vphoto_name;
                if (!is_dir($photopath)) {
                    $this->createFolder($photopath);
                }
                $ftppath = $photopath . $vphotofile;
                if (!copy($vphoto, $ftppath)) {
                    $vphotofile = '';
                    $msg = "Uploading file(s) is failed.!";
                } else {
                    $msg = "File(s) uploaded successfully.!";
                }
            } else {
                $vphotofile = '';
                $msg = "File extension is not valid, vaild ext. are  $vaild_ext !";
            }
        } else {
            $vphotofile = '';
            $msg = "Upload file path not found";
        }
        $ret[0] = $vphotofile;
        $ret[1] = $msg;
        return $ret;
    }

    public function image_upload($photo = '', $path = '', $filename = '') {
        $photo_name_str = base64_decode($photo);
        if (trim($filename) == '') {
            $filename = 'base-image-' . date('YmdHis') . rand(1000, 9999) . '.jpg';
        }
        if (!is_dir($path)) {
            $this->createFolder($path);
        }
        $filename_path = $path . $prefix . $filename;
        if (!$handle = fopen($filename_path, 'w')) {
            $filename = '';
            $msg = "Cannot create file ($filename)";
        }
        // Write $somecontent to our opened file.
        if (fwrite($handle, $photo_name_str) === FALSE) {
            $filename = '';
            $msg = "Cannot write to file ($filename)";
        }
        @fclose($handle);
        return $filename;
    }

    public function imageupload_base64($photopath = '', $photo = '', $filename = '') {
        $cam_pic = str_replace(" ", "+", $photo);
        $cam_image = str_replace("data:image/jpeg;base64,", "", $cam_pic);
        $cam_image = str_replace("data:image/png;base64,", "", $cam_image);
        $photo_name_str = base64_decode($cam_image);
        if ($filename == '') {
            $filename = 'webcam_image_' . date('YmdHis') . '.png';
        }
        $filename_path = $photopath . $filename;
        if (!$handle = fopen($filename_path, 'w')) {
            $filename = '';
            $msg = "Cannot open file ($filename)";
        }
        // Write $somecontent to our opened file.
        if (fwrite($handle, $photo_name_str) === FALSE) {
            $filename = FALSE;
            $msg = "Cannot write to file ($filename)";
        }
        @fclose($handle);
        $ret[0] = $filename;
        $ret[1] = $msg;
        return $ret;
    }

    public function get_file_attributes($vphoto_name = '') {
        if (trim($vphoto_name) == "") {
            $extension = "jpg";
            $file_name = "base-image-" . date("YmdHis") . rand(1000, 9999) . "." . $extension;
            return array($file_name, $extension);
        }
        $file_arr = explode(".", $vphoto_name);
        $temp_arr = array();
        for ($i = 0; $i < count($file_arr) - 1; $i++) {
            $temp_arr[] = $file_arr[$i];
        }
        $file = @implode("_", $temp_arr);
        $file = str_replace(" ", "_", $file);
        $extension = $file_arr[count($file_arr) - 1];
        $file_name = $file . "-" . date("YmdHis") . rand(1000, 9999) . "." . $extension;
        return array($file_name, $extension);
    }

    public function do_image_replacement($photo_val = '') {
        $value_pic = str_replace(" ", "+", $photo_val);
        $value_photo = str_replace("data:image/jpeg;base64,", "", $value_pic);
        $value_photo = str_replace("data:image/png;base64,", "", $value_photo);
        return $value_photo;
    }

    public function do_image_mime_operations($value_photo = '') {
        $f = finfo_open();
        $mime_type = finfo_buffer($f, base64_decode($value_photo), FILEINFO_MIME_TYPE);
        if (strstr($mime_type, "image/")) {
            $filename = "base-image-" . date("YmdHis") . rand(1000, 9999) . "." . str_replace("image/", "", $mime_type);
        } else {
            $filename = "";
        }
        return $filename;
    }

    public function getTablePrimaryKey($table_name = '') {
        if ($table_name != "") {
            $tbl_fields = $this->CI->db->field_data($table_name);
            if (is_array($tbl_fields) && count($tbl_fields) > 0) {
                foreach ((array) $tbl_fields as $field) {
                    if ($field->primary_key) {
                        $pkkey = $field->name;
                        break;
                    }
                }
            }
        }
        return $pkkey;
    }

    public function getAWSServerUploadPathURL($folder_name = '') {
        $folder_name = trim($folder_name);
        $ret_arr['status'] = FALSE;
        if ($folder_name == "") {
            $ret_arr['status'] = FALSE;
        } else {
            $bucket_name = $this->CI->config->item('AWS_BUCKET_NAME');
            if (is_array($this->_aws_avail_buckets) && array_key_exists($bucket_name, $this->_aws_avail_buckets)) {
                $available_bucket = $this->_aws_avail_buckets[$bucket_name];
            } else {
                $available_bucket = $this->isAWSBucketAvailable($bucket_name);
            }
            if ($available_bucket !== FALSE) {
                $this->_aws_avail_buckets[$bucket_name] = $available_bucket;
                $original_path = "";
                $original_url = "http://" . $bucket_name . ".s3.amazonaws.com/" . $folder_name . "/";

                $ret_arr['status'] = TRUE;
                $ret_arr['folder_path'] = $original_path;
                $ret_arr['folder_url'] = $original_url;
                $ret_arr['bucket_name'] = $bucket_name;
                $ret_arr['bucket_folder'] = $folder_name;
                $ret_arr['bucket_files'] = $available_bucket;
            }
        }
        return $ret_arr;
    }

    public function getServerUploadPathURL($folder_name = '') {
        $custom_file_path = $this->CI->config->item('CIS_FILE_PATH');
        $folder_name = trim($folder_name);
        $ret_arr['status'] = FALSE;
        if ($folder_name == "") {
            $ret_arr['status'] = FALSE;
        } else {
            $original_path = $custom_file_path . $folder_name . '/';
            $original_url = $this->CI->config->item('CIS_BASE_URL');
            $ret_arr['status'] = TRUE;
            $ret_arr['folder_path'] = $original_path;
            $ret_arr['folder_url'] = $original_url;
        }
        return $ret_arr;
    }

    public function getAdminUploadPathURL($folder_name = '') {
        $upload_path = $this->CI->config->item('upload_path');
        $upload_url = $this->CI->config->item('upload_url');
        $folder_name = trim($folder_name);
        $folder_orgi = $this->getImageNestedFolders($folder_name);
        $ret_arr['status'] = FALSE;
        if ($folder_name == "") {
            $ret_arr['status'] = FALSE;
        } else {
            $folder_path = $upload_path . $folder_orgi;
            if (is_dir($folder_path)) {
                $original_path = $upload_path . $folder_orgi . DS;
                $original_url = $upload_url . $folder_name . "/";
                $ret_arr['status'] = TRUE;
                $ret_arr['folder_path'] = $original_path;
                $ret_arr['folder_url'] = $original_url;
            }
        }
        return $ret_arr;
    }

    public function isAWSBucketAvailable($bucket_name = '') {
        $bucket_available = TRUE;
        $s3 = $this->getAWSConnectionObject();
        $res = $s3->getBucket($bucket_name);
        if (!is_array($res)) {
            $created = $s3->putBucket($bucket_name, S3::ACL_PUBLIC_READ, $this->CI->config->item('AWS_END_POINT'));
            if (!$created) {
                $bucket_available = FALSE;
            } else {
                $bucket_available = array();
            }
        } else {
            $bucket_available = array_keys($res);
        }
        return $bucket_available;
    }

    public function checkFileExistsOnAWSServer($folder_name = '', $file_name = '') {
        $bucket_name = $this->CI->config->item('AWS_BUCKET_NAME');
        $object_fodler = $bucket_name . "/" . $folder_name;
        $s3 = $this->getAWSConnectionObject();
        $res = $s3->getObjectInfo($object_fodler, $file_name);
        return $res;
    }

    public function checkFileExistsOnAWSObject($object_arr = array(), $file_name = '', $bucket_name = '', $folder_name = '') {
        $res = FALSE;
        if (!is_array($object_arr) || count($object_arr) == 0 || $folder_name == '') {
            return $res;
        }

        $final_file = $folder_name . "/" . $file_name;

        if (in_array($final_file, $object_arr)) {
            return TRUE;
        }
        return $res;
    }

    public function uploadAWSData($temp_file = '', $folder_name = '', $file_name = '') {
        $bucket_name = $this->CI->config->item('AWS_BUCKET_NAME');
        $object_folder = $bucket_name . "/" . $folder_name;
        $s3 = $this->getAWSConnectionObject();
        $response = $s3->putObjectFile($temp_file, $object_folder, $file_name, S3::ACL_PUBLIC_READ);
        return $response;
    }

    public function deleteAWSFileData($folder_name = '', $file_name = '') {
        $bucket_name = $this->CI->config->item('AWS_BUCKET_NAME');
        $object_fodler = $bucket_name . "/" . $folder_name;
        $response = FALSE;
        if (trim($file_name) != "" && trim($bucket_name) != "" && trim($folder_name) != "") {
            $s3 = $this->getAWSConnectionObject();
            $response = $s3->deleteObject($object_fodler, $file_name);
        }
        return $response;
    }

    public function getAWSConnectionObject() {
        if (is_object($this->_aws_avial_obj)) {
            return $this->_aws_avial_obj;
        }
        $site_path = $this->CI->config->item('site_path');

        $AWS_ACCESSKEY = $this->CI->config->item('AWS_ACCESSKEY');
        $AWS_SECRECTKEY = $this->CI->config->item('AWS_SECRECTKEY');
        $AWS_SSL_VERIFY = ($this->CI->config->item('AWS_SSL_VERIFY') == "Yes") ? TRUE : FALSE;
        $AWS_END_POINT = $this->CI->config->item('AWS_END_POINT');
        $AWS_SERVER_REGION = (trim($AWS_END_POINT)) ? "s3-" . trim($AWS_END_POINT) . ".amazonaws.com" : FALSE;

        require_once (APPPATH . "third_party/aws_s3/S3.php");
        if ($AWS_SERVER_REGION) {
            $this->_aws_avial_obj = new S3($AWS_ACCESSKEY, $AWS_SECRECTKEY, $AWS_SSL_VERIFY, $AWS_SERVER_REGION);
        } else {
            $this->_aws_avial_obj = new S3($AWS_ACCESSKEY, $AWS_SECRECTKEY, $AWS_SSL_VERIFY);
        }

        return $this->_aws_avial_obj;
    }

    public function checkFileExistsOnServer($server_url = '') {
        $return_val = FALSE;
        $ch = curl_init($server_url);
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);
        curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // $retcode >= 400 -> not found, $retcode = 200, found.
        curl_close($ch);
        if ($retcode == 200) {
            $return_val = TRUE;
        }
        return $return_val;
    }

    public function uploadServerData($path = "public/", $file_src = '', $file_org = '') {
        $post_url = $this->CI->config->item('CIS_POST_URL');
        $auth_code = $this->CI->config->item('CIS_AUTH_CODE');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_URL, $post_url);
        //most importent curl assues @filed as file field
        $post_array = array(
            "folder_name" => $path,
            "folder_md5" => md5($path),
            "file_id" => 'sample',
            "file_name" => $file_org,
            "file_md5" => md5_file($file_src),
            "file_sha1" => sha1_file($file_src)
        );
        $auth = hash_hmac('md5', http_build_query($post_array), $auth_code);
        $post_array['file'] = "@" . $file_src;
        $post_array['auth'] = $auth;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_array);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function createFolder($path = '') {
        $root_path = $this->CI->config->item('base_path');
        $pathfolder = @explode(DS, str_replace($root_path, "", $path));
        $realpath = "";
        for ($p = 0; $p < count($pathfolder); $p++) {
            if ($pathfolder[$p] != '') {
                $realpath = $realpath . $pathfolder[$p] . DS;
                $makefolder = $root_path . DS . $realpath;
                if (!is_dir($makefolder)) {
                    $old_umask = @umask(0);
                    @mkdir($makefolder, 0777);
                    @chmod($makefolder, 0777);
                    @chown($makefolder, get_current_user());
                    @umask($old_umask);
                }
            }
        }
        return $makefolder;
    }

    public function createPermission($file_name = '') {
        if (file_exists($file_name)) {
            @chmod($file_name, 0777);
            @chown($file_name, get_current_user());
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function createUploadFolderIfNotExists($folder_name = '') {
        if ($folder_name == "") {
            return FALSE;
        }
        $upload_folder = $this->CI->config->item('upload_path') . $folder_name . DS;
        $this->createFolder($upload_folder);
        return TRUE;
    }

    public function uploadFilesOnSaveForm($file_arr = array(), $id = '') {
        if (!is_array($file_arr) || count($file_arr) == 0) {
            return;
        }
        foreach ($file_arr as $key => $val) {
            $file_name = $val['file_name'];
            $folder_name = $val['folder_name'];
            $id_wise = $val['id_wise'];
            $old_file = $val['old_file'];
            $temp_file_path = $this->CI->config->item('admin_upload_temp_path') . $file_name;
            if ($id_wise == 'Yes' && $id != '') {
                $this->createUploadFolderIfNotExists($folder_name . DS . $id);
                $dest_file_path = $this->CI->config->item('upload_path') . $folder_name . DS . $id . DS . $file_name;
                $old_file_path = $this->CI->config->item('upload_path') . $folder_name . DS . $id . DS . $old_file;
            } else {
                $this->createUploadFolderIfNotExists($folder_name);
                $dest_file_path = $this->CI->config->item('upload_path') . $folder_name . DS . $file_name;
                $old_file_path = $this->CI->config->item('upload_path') . $folder_name . DS . $old_file;
            }
            if (file_exists($temp_file_path)) {
                if (@copy($temp_file_path, $dest_file_path)) {
                    @unlink($temp_file_path);
                    if (file_exists($old_file_path) && $old_file != '') {
                        @unlink($old_file_path);
                    }
                }
            }
        }
    }

    public function getImageNestedFolders($folder_name = '') {
        if (strpos($folder_name, '/') !== FALSE) {
            $folder_name_arr = @explode("/", $folder_name);
            $folder_name = implode(DS, $folder_name_arr);
        }
        return $folder_name;
    }

    public function languageTranslation($src, $dest, $text) {
        $dest = strtolower($dest);
        $src = strtolower($src);
        // using bing translation api
        $appId = $this->CI->config->item('SYSTEM_LANG_APP_ID');
        $text = urlencode($text);
        $txt = '';
        $use_curl = function_exists('curl_version') ? TRUE : FALSE;
        if (trim($appId) != '') {
            $data_url = "http://api.microsofttranslator.com/v2/Http.svc/Translate?appId=" . $appId . "&text=" . $text . "&from=$src&to=$dest";
            if ($use_curl) {
                $trans = $this->getCurlResponse($data_url);
            } else {
                $trans = file_get_contents($data_url);
            }
            $tr = $this->xml2array($trans, 1);
            $txt = (isset($tr['string']) && is_string($tr['string'])) ? $tr['string'] : '';
            $txt = trim($txt);
        }
        // using frengly.com api
        if (trim($txt) == '') {
            $username = $this->CI->config->item('SYSTEM_LANG_USERNAME');
            $password = $this->CI->config->item('SYSTEM_LANG_PASSWORD');
            $data_url = "http://syslang.com?src=$src&dest=$dest&text=$text&email=$username&password=$password&outformat=xml";
            if ($use_curl) {
                $trans = $this->getCurlResponse($data_url);
                $tr = $this->xml2array($trans, 0);
            } else {
                $trans = file_get_contents($data_url);
                $tr = $this->xml2array($trans, 0);
            }
            $txt = (isset($tr['root']['translation'])) ? ($tr['root']['translation']) : "";
            $txt = trim($txt);
            sleep(3); // The minimum time between API call is 3s.
        }
        return $txt;
    }

    public function xml2array($contents, $get_attributes = 1, $priority = 'tag') {
        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);
        if (!$xml_values)
            return; //Hmm...
        $xml_array = array();
        $parents = array();
        $opened_tags = array();
        $arr = array();
        $current = & $xml_array;
        $repeated_tag_index = array();
        if (is_array($xml_values) && count($xml_values) > 0) {
            foreach ($xml_values as $data) {
                unset($attributes, $value);
                extract($data);
                $result = array();
                $attributes_data = array();
                if (isset($value)) {
                    if ($priority == 'tag')
                        $result = $value;
                    else
                        $result['value'] = $value;
                }
                if (isset($attributes) and $get_attributes) {
                    foreach ($attributes as $attr => $val) {
                        if ($priority == 'tag')
                            $attributes_data[$attr] = $val;
                        else
                            $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
                    }
                }
                if ($type == "open") {
                    $parent[$level - 1] = & $current;
                    if (!is_array($current) or ( !in_array($tag, array_keys($current)))) {
                        $current[$tag] = $result;
                        if ($attributes_data)
                            $current[$tag . '_attr'] = $attributes_data;
                        $repeated_tag_index[$tag . '_' . $level] = 1;
                        $current = & $current[$tag];
                    }
                    else {
                        if (isset($current[$tag][0])) {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                            $repeated_tag_index[$tag . '_' . $level] ++;
                        } else {
                            $current[$tag] = array(
                                $current[$tag],
                                $result
                            );
                            $repeated_tag_index[$tag . '_' . $level] = 2;
                            if (isset($current[$tag . '_attr'])) {
                                $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                                unset($current[$tag . '_attr']);
                            }
                        }
                        $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                        $current = & $current[$tag][$last_item_index];
                    }
                } elseif ($type == "complete") {
                    if (!isset($current[$tag])) {
                        $current[$tag] = $result;
                        $repeated_tag_index[$tag . '_' . $level] = 1;
                        if ($priority == 'tag' and $attributes_data)
                            $current[$tag . '_attr'] = $attributes_data;
                    }
                    else {
                        if (isset($current[$tag][0]) and is_array($current[$tag])) {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                            if ($priority == 'tag' and $get_attributes and $attributes_data) {
                                $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                            }
                            $repeated_tag_index[$tag . '_' . $level] ++;
                        } else {
                            $current[$tag] = array(
                                $current[$tag],
                                $result
                            );
                            $repeated_tag_index[$tag . '_' . $level] = 1;
                            if ($priority == 'tag' and $get_attributes) {
                                if (isset($current[$tag . '_attr'])) {
                                    $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                                    unset($current[$tag . '_attr']);
                                }
                                if ($attributes_data) {
                                    $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                                }
                            }
                            $repeated_tag_index[$tag . '_' . $level] ++; //0 and 1 index is already taken
                        }
                    }
                } elseif ($type == 'close') {
                    $current = & $parent[$level - 1];
                }
            }
        }
        return ($xml_array);
    }

    public function pushTestNotification($device_id = '', $notify_arr = array(), $device_type = '') {
        if (empty($device_id)) {
            return FALSE;
        }
        if ($device_type == "android" || strlen($device_id) > 70) {
            $success = $this->androidNotification($device_id, $notify_arr['message'], $notify_arr);
        } else {
            //ios push notification
            $success = $this->iOSNotification($device_id, $notify_arr);
        }
        // push notification end .....
        return $success;
    }

    public function iOSNotification($device_id = '', $notify_arr = array()) {
        try {
            if (empty($device_id)) {
                throw new Exception("Device token not found..!");
            }
            $cache_temp_path = $this->CI->config->item('admin_upload_cache_path');
            $upload_settings_path = $this->CI->config->item('settings_files_path');
            // push notification start .....
            $deviceToken = $device_id;
            //$message = "Push Notification Done";
            $max_size = 246;
            $badge = 0;
            $sound = 'received5.caf';
            $upload_pem_file = $this->CI->config->item('PUSH_NOTIFY_PEM_FILE');
            if (file_exists($upload_settings_path . $upload_pem_file) && $upload_pem_file != "") {
                $pem_file = $upload_settings_path . $upload_pem_file;
            } else {
                $pem_file = $this->CI->config->item('site_path') . 'apns-dev.pem';
            }
            if (!file_exists($pem_file)) {
                throw new Exception("Certificates(PEM) file not found..!");
            }

            $data = array();

            if ($notify_arr['title']) {
                $data['aps']['alert']['action-loc-key'] = $notify_arr['title'];
            }
            $data['aps']['badge'] = ($notify_arr['badge'] != "") ? intval($notify_arr['badge']) : intval($badge);
            $data['aps']['sound'] = ($notify_arr['sound'] != "") ? $notify_arr['sound'] : $sound;

            if ($notify_arr['code']) {
                $data['code'] = $notify_arr['code'];
            }
            if ($notify_arr['id']) {
                $data['id'] = $notify_arr['id'];
            }

            if (is_array($notify_arr['others']) && count($notify_arr['others']) > 0) {
                $data = array_merge($data, $notify_arr['others']);
            }
            $data = $this->array_map_recursive("trim", $data);
            $data['aps']['badge'] = intval($data['aps']['badge']);
            $temp_src = json_encode($data);
            $allow_size = $max_size - (strlen($temp_src));

            if ($allow_size > 0) {
                $message = htmlentities(trim($notify_arr['message']));
                $message = str_replace(array("\n", "\r"), " ", $message);
                if (strlen($message) > $allow_size) {
                    $message = substr($message, 0, ($allow_size - 3)) . "...";
                }
            } else {
                $message = '...';
            }
            $data['aps']['alert']['body'] = html_entity_decode($message);
            $payload = json_encode($data);
            $this->_push_content = $payload;

            if (strlen($payload) > 256) {
                throw new Exception("Payload length exceeds maximum characters(255)..!");
            }

            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $pem_file);
            if ($this->CI->config->item('PUSH_NOTIFY_SENDING_MODE') == "sandbox") {
                $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
            } else {
                $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
            }
            if (!$fp) {
                throw new Exception("Faliure while stream socket connection..!");
            }
            $msg = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n", strlen($payload)) . $payload;
            #print $deviceToken." sending message :" . $payload . "\n";exit;
            fwrite($fp, $msg);
            fclose($fp);

            $success = 1;
            $message = "Push notification send successfully..!";
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
            $this->_notify_error = $message;
        }

        if ($_ENV['debug_action']) {
            $f = @fopen($cache_temp_path . 'ios_notification.html', 'a+');
            @fwrite($f, '<br/>');
            @fwrite($f, 'Date : ' . date('Y-m-d H:i:s') . '<br/>');
            @fwrite($f, print_r("Device Token : " . $device_id, TRUE) . '<br/>');
            @fwrite($f, print_r("Data : " . json_encode($data), TRUE) . '<br/>');
            @fwrite($f, print_r("Resource Id : " . $fp, TRUE) . '<br/>');
            @fwrite($f, print_r("Status : " . $success, TRUE) . '<br/>');
            @fwrite($f, print_r("Message : " . $message, TRUE) . '<br/>');
            @fclose($f);
        }

        return $success;
        // push notification end .....
    }

    public function androidNotification($device_id = '', $message = 'hi', $extra = array()) {
        $result = '';
        try {
            if (empty($device_id)) {
                throw new Exception("Device token not found..!");
            }

            $cache_temp_path = $this->CI->config->item('admin_upload_cache_path');
            $temp_key = $this->CI->config->item('PUSH_NOTIFY_ANDROID_KEY');

            if (empty($temp_key)) {
                throw new Exception("Push notification Authorization key not found..!");
            }
            // Set POST variables
            $url = 'https://android.googleapis.com/gcm/send';
            // Replace with real client registration IDs
            $registrationIDs = array($device_id);
            $apiKey = $temp_key;

            $message_arr = array("message" => $message);
            $data = array_merge($message_arr, $extra);

            $this->_push_content = json_encode($data);

            $fields = array(
                'registration_ids' => $registrationIDs,
                'data' => $data,
            );

            $headers = array(
                'Authorization: key=' . $apiKey,
                'Content-Type: application/json'
            );

            if (!function_exists('curl_version')) {
                throw new Exception("CURL is not installed in this server..!");
            }

            // Open connection
            $ch = curl_init();
            if (!$ch) {
                throw new Exception("CURL intialization fails..!");
            }
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            //curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            //     curl_setopt($ch, CURLOPT_POST, TRUE);
            //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            // Execute post
            $result = curl_exec($ch);
            // Close connection
            curl_close($ch);

            if (!$result) {
                $error = curl_error($ch) || "CURL execution fails..!";
                throw new Exception();
            }

            $success = 1;
            $message = "Push notification send successfully..!";
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
            $this->_notify_error = $message;
        }

        if ($_ENV['debug_action']) {
            $f = fopen($cache_temp_path . 'android_notification.html', 'a+');
            @fwrite($f, '<br/>');
            @fwrite($f, 'Date : ' . date('Y-m-d H:i:s') . '<br/>');
            @fwrite($f, print_r("Device Token : " . json_encode($registrationIDs), TRUE) . '<br/>');
            @fwrite($f, print_r("Data : " . json_encode($data), TRUE) . '<br/>');
            @fwrite($f, print_r("Response : " . $result, TRUE) . '<br/>');
            @fwrite($f, print_r("Resource Id : " . $ch, TRUE) . '<br/>');
            @fwrite($f, print_r("Status : " . $success, TRUE) . '<br/>');
            @fwrite($f, print_r("Message : " . $message, TRUE) . '<br/>');
            @fclose($f);
        }

        return $success;
    }

    public function insertPushNotification($push_arr = array(), $from = "api") {
        $unique_id = $this->getPushNotifyUnique();
        $send_type = $this->CI->config->item('PUSH_NOTIFY_SENDING_TYPE');
        if ($send_type == "runtime" && $from == "api") {
            $notify_arr = array();
            $notify_arr['mode'] = $push_arr['mode'];
            $notify_arr['message'] = $push_arr['message'];
            $notify_arr['title'] = $push_arr['title'];
            $notify_arr['badge'] = intval($push_arr['badge']);
            $notify_arr['sound'] = $push_arr['sound'];
            $notify_arr['code'] = $push_arr['code'];
            $notify_arr['id'] = $unique_id;
            $vars_arr = $push_arr['variables'];
            if (is_array($vars_arr) && count($vars_arr) > 0) {
                foreach ($vars_arr as $vk => $vv) {
                    if ($vv['key'] != "" && $vv['send'] == "Yes") {
                        $notify_arr['others'][$vv['key']] = $vv['value'];
                    }
                }
            }
            $success = $this->pushTestNotification($push_arr['device_id'], $notify_arr);
        }
        if ($from == "admin") {
            $this->CI->load->model('tools/push');
        } else {
            $this->CI->load->model('rest/rest');
        }
        $insert_arr = array();
        $insert_arr['vUniqueId'] = $unique_id;
        $insert_arr['vDeviceId'] = $push_arr['device_id'];
        $insert_arr['eMode'] = $this->CI->config->item('PUSH_NOTIFY_SENDING_MODE');
        $insert_arr['eNotifyCode'] = $push_arr['code'];
        $insert_arr['vSound'] = $push_arr['sound'];
        $insert_arr['vBadge'] = $push_arr['badge'];
        $insert_arr['vTitle'] = $push_arr['title'];
        $insert_arr['tMessage'] = $push_arr['message'];
        $insert_arr['tVarsJSON'] = $push_arr['variables'];
        if ($push_arr['device_type'] != "") {
            $insert_arr['eDeviceType'] = $push_arr['device_type'];
        } else {
            if (strlen($device_id) > 70) {
                $insert_arr['eDeviceType'] = "Android";
            } else {
                $insert_arr['eDeviceType'] = "iOS";
            }
        }
        $insert_arr['dtAddDateTime'] = date("Y-m-d H:i:s");
        if ($send_type == "runtime" && $from == "api") {
            $insert_arr['tSendJSON'] = $this->getPushNotifyOutput("body");
            $insert_arr['dtExeDateTime'] = date("Y-m-d H:i:s");
            if ($success) {
                $insert_arr['eStatus'] = "Executed";
            } else {
                $insert_arr['tError'] = $this->getNotifyErrorOutput();
                $insert_arr['eStatus'] = 'Failed';
            }
        } else {
            $insert_arr['eStatus'] = 'Pending';
        }
        if ($from == "admin") {
            $pid = $this->CI->push->insertPushNotify($insert_arr);
        } else {
            $pid = $this->CI->rest->insertPushNotify($insert_arr);
        }
        if ($from == "admin") {
            if (!$pid) {
                return array(FALSE, FALSE);
            }
            return array($unique_id, $pid);
        } else {
            if ($send_type == "runtime") {
                if (!$success) {
                    return FALSE;
                }
                return $success;
            } else {
                if (!$pid) {
                    return FALSE;
                }
                return $pid;
            }
        }
    }

    public function getPushNotifyUnique() {
//        $db_unique_arr = $this->CI->db->select_single("mod_push_notifications", "vUniqueId");
//        $unique_id = substr(md5(uniqid(rand(), TRUE)), 0, 5);
//        while (in_array($unique_id, $db_unique_arr)) {
//            $unique_id = substr(md5(uniqid(rand(), TRUE)), 0, 5);
//        }
        $unique_id = substr(md5(uniqid(rand(), TRUE)), 0, 5);
        return $unique_id;
    }

    public function array_map_recursive($callback = '', $array = array()) {
        if (is_array($array) && count($array) > 0) {
            foreach ($array as $key => $value) {
                if (is_array($array[$key])) {
                    $array[$key] = $this->array_map_recursive($callback, $array[$key]);
                } else {
                    $array[$key] = call_user_func($callback, $array[$key]);
                }
            }
        }
        return $array;
    }

    public function sendMail_old($data = array(), $type_format = "CONTACT_US") {
        if (is_array($data) && count($data) > 0) {
            if (!empty($data['vEmail'])) {
                $to = $data['vEmail'];
            } else {
                $to = $this->CI->config->item('EMAIL_ADMIN');
            }
            $mailarr = $this->getSystemEmailData($type_format);
            $email_var = $this->getVariablesByTemplate($mailarr[0]['iEmailTemplateId']);
            $emailDataArr = $this->replaceEmailTemplate($mailarr[0], $email_var, $data);

            $from_name = $emailDataArr['vFromName'];
            $from = $emailDataArr['vFromEmail'];
            $subject = $emailDataArr['vEmailSubject'];
            $body = $emailDataArr['vEmailContent'];
            $cc = $data['vCCEmail'];
            $bcc = $data['vBCCEmail'];

            $success = $this->CISendMail($to, $subject, $body, $from, $from_name, $cc, $bcc);
            return $success;
        } else {
            return FALSE;
        }
    }

    public function sendMail($data = array(), $type_format = "CONTACT_US") {
        if (is_array($data) && count($data) > 0) {
            if (!empty($data['vEmail'])) {
                $to = $data['vEmail'];
            } else {
                $to = $this->CI->config->item('EMAIL_ADMIN');
            }
            $mailarr = $this->getSystemEmailData($type_format);
            $email_var = $this->getVariablesByTemplate($mailarr[0]['iEmailTemplateId']);
            $emailDataArr = $this->replaceEmailTemplate($mailarr[0], $email_var, $data);

            $from_name = $emailDataArr['vFromName'];
            $from = $emailDataArr['vFromEmail'];
            $subject = $emailDataArr['vEmailSubject'];
            $body = $emailDataArr['vEmailContent'];
            $cc = $emailDataArr['vCcEmail'];
            $bcc = $emailDataArr['vBccEmail'];

            $success = $this->CISendMail($to, $subject, $body, $from, $from_name, $cc, $bcc);
            return $success;
        } else {
            return FALSE;
        }
    }

    public function getSystemEmailData($type = '') {
        $this->CI->db->where("vEmailCode", $type);
        $mail_data_obj = $this->CI->db->get('mod_system_email');
        $mail_data_arr = is_object($mail_data_obj) ? $mail_data_obj->result_array() : array();
        return $mail_data_arr;
    }

    public function getVariablesByTemplate($iEmailTemplateId) {
        $this->CI->db->where("iEmailTemplateId", $iEmailTemplateId);
        $mail_var_data_obj = $this->CI->db->get('mod_system_email_vars');
        $mail_var_data_arr = is_object($mail_var_data_obj) ? $mail_var_data_obj->result_array() : array();
        return $mail_var_data_arr;
    }

    public function getSearchReplaceArrays($config_arr = array()) {
        $find_arr = $replace_arr = array();
        $return_arr['find'] = array();
        $return_arr['replace'] = array();
        if (!is_array($config_arr) || count($config_arr) == 0) {
            return $return_arr;
        }
        foreach ($config_arr as $key => $val) {
            $find_val = trim($key);
            switch (TRUE) {
                case (substr($find_val, 0, 8) == "#SYSTEM."):
                    $temp_find_val = trim($find_val, "#");
                    $temp_find_val = trim(substr_replace($temp_find_val, "", 0, 7));
                    $replace_val = $this->CI->config->item($temp_find_val);
                    break;
                default:
                    $replace_val = $val;
                    break;
            }
            $find_arr[] = $find_val;
            $replace_arr[] = $replace_val;
        }
        $return_arr['find'] = $find_arr;
        $return_arr['replace'] = $replace_arr;
        return $return_arr;
    }

    public function replaceEmailTemplate_Old($template_arr = array(), $variable_arr = array(), $data = array()) {
        $vEmailCode = $template_arr['vEmailCode'];
        $subject = ($data['vSubject'] == "") ? $template_arr['vEmailSubject'] : $data['vSubject'];
        $tEmailMessage = stripslashes($template_arr['tEmailMessage']);

        $vFromEmail = $data['vFromEmail'];
        $vFromEmail = ($vFromEmail == "") ? $template_arr['vFromEmail'] : $vFromEmail;
        if ($vFromEmail == "") {
            $vFromEmail = $this->CI->config->item('EMAIL_ADMIN');
        }

        $vFromName = $data['vFromName'];
        $vFromName = ($vFromName == "") ? $template_arr['vFromName'] : $vFromName;
        if ($vFromName == '') {
            $vFromName = 'Admin';
        }

        $config_arr = array();
        if (is_array($variable_arr) && count($variable_arr) > 0) {
            foreach ((array) $variable_arr as $key => $val) {
                $varName = $val['vVarName'];
                $config_arr[$varName] = $data[trim($varName, "#")];
            }
        }
        $admin_url = $this->CI->config->item("admin_url");
        $site_url = $this->CI->config->item("site_url");
        $template_arr['vEmailFooter'] = str_replace("#COMPANY_NAME#", $this->CI->config->item('COMPANY_NAME'), $template_arr['vEmailFooter']);

        $config_arr['#COMPANY_NAME#'] = $this->CI->config->item('COMPANY_NAME');
        $config_arr['#SITE_URL#'] = $site_url;
        $config_arr['#SITE_LOGO#'] = "<img src='" . $this->CI->config->item('site_url') . "images/admin/logo.png'/>";
        $config_arr['#MAIL_FOOTER#'] = $template_arr['vEmailFooter'];

        switch ($vEmailCode) {
            case "ADMIN_REGISTER":
                $config_arr['#SITE_URL#'] = $admin_url;
                break;
            case "NEWSLETTER":
                $config_arr['#SITE_URL#'] = $admin_url;
                break;
        }
        $list_array = $this->getSearchReplaceArrays($config_arr);
        $find_arr = $list_array['find'];
        $replace_arr = $list_array['replace'];

        $body = str_replace($find_arr, $replace_arr, $tEmailMessage);
        $subject = str_replace($find_arr, $replace_arr, $subject);
        //$from = $vFromName . " < " . $vFromEmail . " >";
        $from = $vFromEmail;
        $from_name = $vFromName;

        $returnArr['vEmailSubject'] = $subject;
        $returnArr['vEmailContent'] = $body;
        $returnArr['vFromEmail'] = $from;
        $returnArr['vFromName'] = $from_name;
        return $returnArr;
    }

    public function replaceEmailTemplate($template_arr = array(), $variable_arr = array(), $data = array()) {
        $vEmailCode = $template_arr['vEmailCode'];
        $subject = ($data['vSubject'] == "") ? $template_arr['vEmailSubject'] : $data['vSubject'];
        $tEmailMessage = stripslashes($template_arr['tEmailMessage']);

        $cc_email = ($data['vCCEmail'] == '') ? $template_arr['vCcEmail'] : $data['vCCEmail'];
        $bcc_email = ($data['vBCCEmail'] == '') ? $template_arr['vBccEmail'] : $data['vBCCEmail'];

        //$vFromEmail = $data['vFromEmail'];
        $vFromEmail = ($template_arr['vFromEmail'] != "") ? $template_arr['vFromEmail'] : $data['vFromEmail'];
        if ($vFromEmail == "") {
            $vFromEmail = $this->CI->config->item('EMAIL_ADMIN');
        }

        $vFromName = $data['vFromName'];
        $vFromName = ($vFromName == "") ? $template_arr['vFromName'] : $vFromName;
        if ($vFromName == '') {
            $vFromName = 'Admin';
        }

        $config_arr = array();
        if (is_array($variable_arr) && count($variable_arr) > 0) {
            foreach ((array) $variable_arr as $key => $val) {
                $varName = $val['vVarName'];
                $config_arr[$varName] = $data[trim($varName, "#")];
            }
        }
        $admin_url = $this->CI->config->item("admin_url");
        $site_url = $this->CI->config->item("site_url");
        $template_arr['vEmailFooter'] = str_replace("#COMPANY_NAME#", $this->CI->config->item('COMPANY_NAME'), $template_arr['vEmailFooter']);

        $config_arr['#COMPANY_NAME#'] = $this->CI->config->item('COMPANY_NAME');
        $config_arr['#SITE_URL#'] = $site_url;
        $config_arr['#SITE_LOGO#'] = "<img src='" . $this->CI->config->item('site_url') . "images/admin/logo.png'/>";
        $config_arr['#MAIL_FOOTER#'] = $template_arr['vEmailFooter'];

        switch ($vEmailCode) {
            case "ADMIN_REGISTER":
                $config_arr['#SITE_URL#'] = $admin_url;
                break;
            case "NEWSLETTER":
                $config_arr['#SITE_URL#'] = $admin_url;
                break;
        }
        $list_array = $this->getSearchReplaceArrays($config_arr);
        $find_arr = $list_array['find'];
        $replace_arr = $list_array['replace'];

        $body = str_replace($find_arr, $replace_arr, $tEmailMessage);
        $subject = str_replace($find_arr, $replace_arr, $subject);
        //$from = $vFromName . " < " . $vFromEmail . " >";
        $from = $vFromEmail;
        $from_name = $vFromName;

        $returnArr['vEmailSubject'] = $subject;
        $returnArr['vEmailContent'] = $body;
        $returnArr['vFromEmail'] = $from;
        $returnArr['vFromName'] = $from_name;
        $returnArr['vCcEmail'] = $cc_email;
        $returnArr['vBccEmail'] = $bcc_email;
        return $returnArr;
    }

    public function CISendMail($to = '', $subject = '', $body = '', $from = '', $from_name = '', $cc = '', $bcc = '', $attach = array()) {
        $success = FALSE;
        try {
            if (empty($to)) {
                throw new Exception("Receiver email address is missing..!");
            }
            if (empty($body) || trim($body) == "") {
                throw new Exception("Email body content is missing..!");
            }
            $this->_email_subject = $subject;
            $this->_email_content = $body;

            $this->CI->load->library('email');
            $this->CI->email->from($from, $from_name);
            $this->CI->email->reply_to($from, $from_name);
            $this->CI->email->to($to);
            if (!empty($cc)) {
                $this->CI->email->cc($cc);
            }
            if (!empty($bcc)) {
                $this->CI->email->bcc($bcc);
            }
            $this->CI->email->subject($subject);
            $this->CI->email->message($body);
            //attachment section
            if (is_array($attach) && count($attach) > 0) {
                foreach ($attach as $ak => $av) {
                    $this->email->attach($av['filename'], $av['newname']);
                }
            }
            $success = $this->CI->email->send();
            //echo $this->CI->email->print_debugger();die;
            if (!$success) {
                throw new Exception($this->CI->email->print_debugger(array("subject")));
            }
            $message = "Email send successfully..!";
        } catch (Exception $e) {
            $message = $e->getMessage();
            $this->_notify_error = $message;
        }
        return $success;
    }

    public function sendSMSNotification($to_no = '', $message = '') {
        $active_api = $this->CI->config->item("SMS_ACTIVE_API");
        if ($active_api == "") {
            $this->_notify_error = "SMS API is not activated. Please configure SMS settings.";
            return FALSE;
        }
        $response = FALSE;
        $active_api = strtolower($active_api);
        $from_no = $this->CI->config->item("SMS_FROM_NUMBER");
        if ($active_api == "nexmo") {
            $auth['api_key'] = $this->CI->config->item("SMS_NX_API_KEY");
            $auth['api_secret'] = $this->CI->config->item("SMS_NX_API_SECRET");
            $response = $this->sendSMSNexmo($auth, $to_no, $message['message'], $from_no);
        } else if ($active_api == "clickatell") {
            $auth['user'] = $this->CI->config->item("SMS_CA_API_USER");
            $auth['password'] = $this->CI->config->item("SMS_CA_API_PWD");
            $auth['api_id'] = $this->CI->config->item("SMS_CA_API_ID");
            $response = $this->sendSMSClickatell($auth, $to_no, $message['message'], $from_no);
        }
        return $response;
    }

    public function sendSMSNexmo($auth = array(), $to = '', $message = '', $from = '') {
        // load library
        $this->CI->load->library('nexmo', $auth);
        // set response format: xml or json, default json
        $this->CI->nexmo->set_format('json');
        $message_arr = array(
            'text' => $message
        );
        $response = $this->CI->nexmo->send_message($from, $to, $message_arr);
        if ($response['messages'][0]['status'] == 0) {
            return TRUE;
        } else {
            $this->_notify_error = $response['messages'][0]['error-text'] || "SMS sending failed.";
            return FALSE;
        }
    }

    public function sendSMSClickatell($auth = array(), $to = '', $message = '', $from = '') {
        $this->CI->load->library('clickatel', $auth);
        // you can send your custom message after buying the credits only
        $response = $this->CI->clickatel->send_sms($to, $message);
        if ($response === FALSE) {
            $this->_notify_error = $this->CI->clickatel->last_reply();
        }
        return $response;
    }

    public function generateDesktopCustomLink($link_data_arr = array(), $input_params = array()) {
        $return_link = "";
        $return_link_arr = $extra_attr_arr = array();
        if (is_array($link_data_arr) && count($link_data_arr) > 0) {
            foreach ($link_data_arr as $inner_key => $inner_val) {
                $extra_param_arr = $inner_val['extra_params'];
                $module_name = $inner_val['module_name'];
                $folder_name = $inner_val['folder_name'];
                $module_type = $inner_val['module_type'];
                $module_page = $inner_val['module_page'];
                $open_on = $inner_val['open'];
                $custom_module_link = $inner_val['custom_link'];
                $apply_condition = $inner_val['apply'];
                $conditions_block = $inner_val['block'];
                if ($apply_condition == "Yes") {
                    $conditionflag = $this->checkConditionalBlock($conditions_block, $input_params);
                    if (!$conditionflag) {
                        continue;
                    }
                }
                if ($module_type == "Module") {
                    if ($module_page == "Add" || $module_page == "Update" || $module_page == "View") {
                        $return_link = $admin_url . "#" . $this->getAdminEncodeURL($folder_name . "/" . $module_name . "/add", 0);
                    } else {
                        $return_link = $admin_url . "#" . $this->getAdminEncodeURL($folder_name . "/" . $module_name . "/index", 0);
                    }
                } else {
                    $external_url = $this->isExternalURL($custom_module_link);
                    if ($external_url) {
                        $return_link = $custom_module_link;
                    } else {
                        $return_link = $admin_url . "#" . $custom_module_link;
                    }
                }
                if (is_array($extra_param_arr) && count($extra_param_arr) > 0) {
                    for ($i = 0; $i < count($extra_param_arr); $i++) {
                        $extra_var_val = $extra_param_arr[$i]['req_val'];
                        $extra_var_type = $extra_param_arr[$i]['req_mod'];
                        $extra_var = $extra_param_arr[$i]['req_var'];
                        if (trim($extra_var_val) == "") {
                            continue;
                        }
                        $req_val = $this->parseConditionFieldValue($extra_var_type, $extra_var_val, $input_params);
                        if ($extra_var != "" && @in_array($extra_var, $decryptArr))
                            $return_link .= "|" . $extra_var . "|" . $this->getAdminEncodeURL($req_val);
                        else
                            $return_link .= "|" . $extra_var . "|" . $req_val;
                    }
                }
                if (is_array($input_params) && count($input_params) > 0) {
                    foreach ($input_params as $key => $val) {
                        $find_array[] = "@" . $key . "@";
                        $replace_array[] = $val;
                    }
                }
                $return_link = str_replace($find_array, $replace_array, $return_link);
                if ($open_on == "Popup" && trim($return_link) != "") {
                    $return_link .= "|hideCtrl|true";
                }
                $extra_attr_arr['class'] = "inline-edit-link";
                $extra_attr_arr['target'] = "_self";
                if ($open_on == "NewPage") {
                    $extra_attr_arr['target'] = "_blank";
                } else if ($open_on == "Popup") {
                    $extra_attr_arr['class'] = "inline-edit-link fancybox-hash-iframe";
                    $return_link .= "|hideCtrl|true";
                }
                break;
            }
        }
        $return_link_arr = $extra_attr_arr;
        $return_link_arr['link'] = $return_link;
        return $return_link_arr;
    }

    public function checkConditionalBlock($condition_data_arr = array(), $input_params = array()) {
        $conditionflag = FALSE;
        $condition_type = $condition_data_arr['oper'];
        $conditions_array = $condition_data_arr['conditions'];
        if (is_array($conditions_array) && count($conditions_array) > 0) {

            if ($condition_type == "AND") {
                $conditionflag = TRUE;
            } else {
                $conditionflag = FALSE;
            }
            for ($i = 0; $i < count($conditions_array); $i++) {
                $type = $conditions_array[$i]['type'];
                $operator = $conditions_array[$i]['oper'];
                $operand_1 = $conditions_array[$i]['mod_1'];
                $value_passed_1 = $conditions_array[$i]['val_1'];
                $operand_2 = $conditions_array[$i]['mod_2'];
                $value_passed_2 = $conditions_array[$i]['val_2'];

                $value_1 = $this->parseConditionFieldValue($operand_1, $value_passed_1, $input_params);
                $value_2 = $this->parseConditionFieldValue($operand_2, $value_passed_2, $input_params);

                $value_1 = $this->getDataTypeWiseResult($type, $value_1, TRUE);
                $value_2 = $this->getDataTypeWiseResult($type, $value_2, FALSE);
                $result = $this->compareDataValues($operator, $value_1, $value_2);

                if ($condition_type == "OR") {
                    $conditionflag = $conditionflag || $result;
                    if ($conditionflag) {
                        break;
                    }
                } else {
                    $conditionflag = $conditionflag && $result;
                    if (!$conditionflag) {
                        break;
                    }
                }
            }
        }
        return $conditionflag;
    }

    public function getDataTypeWiseResult($type = '', $value = '', $lf = FALSE) {
        if (is_array($value)) {
            if ($type == "array_count" && $lf === TRUE) {
                return count($value);
            }
            return $value;
        }
        switch ($type) {
            case 'integer':
                $result = (int) $value;
                break;
            case 'float':
                $result = (float) $value;
                break;
            case 'date':
                if ($value != "" && $value != "0000-00-00" && $value != "0000-00-00 00:00:00" && $value != "00:00:00") {
                    $result = date("Y-m-d", strtotime($value));
                } else {
                    $result = "0000-00-00";
                }
                break;
            case 'date_and_time':
                if ($value != "" && $value != "0000-00-00" && $value != "0000-00-00 00:00:00" && $value != "00:00:00") {
                    $result = date("Y-m-d H:i:s", strtotime($value));
                } else {
                    $result = "0000-00-00 00:00:00";
                }
                break;
            case 'time':
                if ($value != "" && $value != "0000-00-00" && $value != "0000-00-00 00:00:00" && $value != "00:00:00") {
                    $result = date("H:i:s", strtotime($value));
                } else {
                    $result = "00:00:00";
                }
                break;
            default :
                $result = (string) $value;
                break;
        }
        return $result;
    }

    public function compareDataValues($operator = '', $value_1 = '', $value_2 = '') {
        $flag = FALSE;
        switch ($operator) {
            case 'ne':
                $flag = ($value_1 != $value_2) ? TRUE : FALSE;
                break;
            case 'nl'://old
                $flag = (strtolower($value_1) != strtolower($value_2)) ? TRUE : FALSE;
                break;
            case 'lt':
                $flag = ($value_1 < $value_2) ? TRUE : FALSE;
                break;
            case 'le':
                $flag = ($value_1 <= $value_2) ? TRUE : FALSE;
                break;
            case 'gt':
                $flag = ($value_1 > $value_2) ? TRUE : FALSE;
                break;
            case 'ge':
                $flag = ($value_1 >= $value_2) ? TRUE : FALSE;
                break;
            case "lis"://old
            case "bw"://new
                $value_1 = strtolower($value_1);
                $value_2 = strtolower($value_2);
                $length_2 = strlen($value_2);
                $string_2 = substr($value_1, 0, $length_2);
                $flag = ($string_2 == $value_2) ? TRUE : FALSE;
                break;
            case "lie"://old
            case "ew"://new
                $value_1 = strtolower($value_1);
                $value_2 = strtolower($value_2);
                $length_2 = strlen($value_2);
                $string_2 = substr($value_1, -$length_2);
                $flag = ($string_2 == $value_2) ? TRUE : FALSE;
                break;
            case "lib"://old
            case "cn"://new
                $value_1 = strtolower($value_1);
                $value_2 = strtolower($value_2);
                $flag = (strpos($value_1, $value_2) !== FALSE) ? TRUE : FALSE;
                break;
            case "nle"://old
            case "bn"://new
                $value_1 = strtolower($value_1);
                $value_2 = strtolower($value_2);
                $length_2 = strlen($value_2);
                $string_2 = substr($value_1, 0, $length_2);
                $flag = ($string_2 != $value_2) ? TRUE : FALSE;
                break;
            case "nls"://old
            case "en"://new
                $value_1 = strtolower($value_1);
                $value_2 = strtolower($value_2);
                $length_2 = strlen($value_2);
                $string_2 = substr($value_1, -$length_2);
                $flag = ($string_2 != $value_2) ? TRUE : FALSE;
                break;
            case "nlb"://old
            case "nc"://new
                $value_1 = strtolower($value_1);
                $value_2 = strtolower($value_2);
                $flag = (strpos($value_1, $value_2) === FALSE) ? TRUE : FALSE;
                break;
            case 'in':
                $value2 = (is_array($value_2)) ? $value_2 : explode(",", $value_2);
                $flag = (in_array($value_1, $value2)) ? TRUE : FALSE;
                break;
            case 'ni':
                $value2 = (is_array($value_2)) ? $value_2 : explode(",", $value_2);
                $flag = (!(in_array($value_1, $value2))) ? TRUE : FALSE;
                break;
            case "nu":
            case "em":
                $value_1 = trim($value_1);
                $flag = (is_null($value_1) || empty($value_1) || $value_1 == '') ? TRUE : FALSE;
                break;
            case "nn":
            case "nem":
                $value_1 = trim($value_1);
                $flag = (!is_null($value_1) && !empty($value_1) && $value_1 != '') ? TRUE : FALSE;
                break;
            case 'ia':
                $flag = (is_array($value_1)) ? true : false;
                break;
            case 'na':
                $flag = (!(is_array($value_1))) ? true : false;
                break;
            case 'li'://old
                $flag = (strtolower($value_1) == strtolower($value_2)) ? TRUE : FALSE;
                break;
            default :
                $flag = ($value_1 == $value_2) ? TRUE : FALSE;
                break;
        }
        return $flag;
    }

    public function parseConditionFieldValue($operand = '', $value_passed = '', $row_arr = array(), $id = '') {
        global $CI;
        $value_return = $value_passed;
        switch ($operand) {
            case "Variable":
                $value_return = $row_arr[$value_passed];
                break;
            case "Request":
                $value_return = $_REQUEST[$value_passed];
                break;
            case "Server":
                $value_return = $_SERVER[$value_passed];
                break;
            case "Session":
                $value_return = $this->CI->session->userdata($value_passed);
                break;
            case "System":
                $value_return = $this->CI->config->item($value_passed);
                break;
            case "Function":
                if (method_exists($this, $value_passed)) {
                    $value_return = $this->$value_passed($row_arr, $id);
                }
                break;
            default :
                $value_return = $value_passed;
                break;
        }
        return $value_return;
    }

    public function getReplacedInputParams($message = '', $inputparams = array()) {
        $message = $this->processSystemHashMatch($message);
        if (is_array($inputparams) && count($inputparams) > 0) {
            foreach ($inputparams as $key => $value) {
                if (is_array($value)) {
                    continue;
                }
                $hash_key = '#' . $key . '#';
                if (strstr($message, $hash_key)) {
                    $message = str_replace($hash_key, $value, $message);
                }
            }
        }
        return $message;
    }

    public function processRequestPregMatch($param = '', $input_params = array()) {
        if ($param != "") {
            if (strstr($param, '{%REQUEST') !== FALSE) {
                preg_match_all("/{%REQUEST\.([a-zA-Z0-9_-]{1,})/i", $param, $preg_all_arr);
                if (isset($preg_all_arr[1]) && is_array($preg_all_arr[1]) && count($preg_all_arr[1]) > 0) {
                    foreach ((array) $preg_all_arr[1] as $key => $value) {
                        if (is_array($input_params[$value])) {
                            continue;
                        }
                        if (strstr($param, '{%REQUEST') !== FALSE) {
                            $param = str_replace("{%REQUEST." . $value . "%}", $input_params[$value], $param);
                        }
                    }
                }
            }
        }
        return $param;
    }

    public function processServerPregMatch($param = '', $input_params = array()) {
        if ($param != "") {
            if (strstr($param, '{%SERVER') !== FALSE) {
                preg_match_all("/{%SERVER\.([a-zA-Z0-9_-]{1,})/i", $param, $preg_all_arr);
                if (isset($preg_all_arr[1]) && is_array($preg_all_arr[1]) && count($preg_all_arr[1]) > 0) {
                    foreach ((array) $preg_all_arr[1] as $key => $value) {
                        if (strstr($param, '{%SERVER') !== FALSE) {
                            $param = str_replace('{%SERVER.' . $value . '%}', $_SERVER[$value], $param);
                        }
                    }
                }
            }
        }
        return $param;
    }

    public function processSystemPregMatch($param = '', $input_params = array()) {
        if ($param != "") {
            if (strstr($param, '{%SYSTEM') !== FALSE) {
                preg_match_all("/{%SYSTEM\.([a-zA-Z0-9_-]{1,})/i", $param, $preg_all_arr);
                if (isset($preg_all_arr[1]) && is_array($preg_all_arr[1]) && count($preg_all_arr[1]) > 0) {
                    foreach ((array) $preg_all_arr[1] as $key => $value) {
                        if (strstr($param, '{%SYSTEM') !== FALSE) {
                            $param = str_replace('{%SYSTEM.' . $value . '%}', $this->config->item($value), $param);
                        }
                    }
                }
            }
        }
        return $param;
    }

    public function processSystemHashMatch($param = '', $input_params = array()) {
        if ($param != "") {
            preg_match_all("/#SYSTEM\.([a-zA-Z0-9_-]{1,})/i", $param, $preg_all_arr);
            if (strstr($param, '#SYSTEM') !== FALSE) {
                if (isset($preg_all_arr[1]) && is_array($preg_all_arr[1]) && count($preg_all_arr[1]) > 0) {
                    foreach ((array) $preg_all_arr[1] as $key => $value) {
                        if (strstr($param, '#SYSTEM') !== FALSE) {
                            $param = str_replace('#SYSTEM.' . $value . '#', $input_params[$value], $param);
                        }
                    }
                }
            }
        }
        return $param;
    }

    public function getAdminExtraCondtion($table_name = '', $table_alias = '') {
        $table_arr = $this->getAdminTableDetails($table_name);
        if (!is_array($table_arr) || count($table_arr) == 0 || $table_name == "") {
            return;
        }
        if ($table_alias != "") {
            $where_cond = $this->CI->db->protect($table_alias . "." . $table_arr['admin_field']) . " <> " . $this->CI->db->escape($table_arr['admin_value']);
        } else {
            $where_cond = $this->CI->db->protect($table_arr['admin_field']) . " <> " . $this->CI->db->escape($table_arr['admin_value']);
        }
        return $where_cond;
    }

    public function isAdminDataRecord($id = '', $mode = '', $table_name = '', $field_name = '') {
        $retArr['success'] = 0;
        $table_arr = $this->getAdminTableDetails($table_name);
        try {
            switch ($table_name) {
                case 'mod_admin':
                    $this->CI->db->select("*");
                    $this->CI->db->from("mod_admin");
                    $this->CI->db->where($table_arr['admin_primary'], $id);
                    $admin_data_obj = $this->CI->db->get();
                    $admin_data = is_object($admin_data_obj) ? $admin_data_obj->result_array() : array();
                    if ($admin_data[0][$table_arr['admin_field']] == $table_arr['admin_value']) {
                        if ($mode == "Delete") {
                            $msg = "Can not delete admin ";
                            throw new Exception($msg);
                        } else {
                            if (in_array($field_name, $table_arr['restrict_field'])) {
                                $msg = "Can not edit admin ";
                                throw new Exception($msg);
                            }
                        }
                    } else if ($admin_data[0][$table_arr['admin_primary']] == $table_info['admin_id']) {
                        if ($mode == "Delete") {
                            $msg = "Can not delete your self ";
                            throw new Exception($msg);
                        } else {
                            if (in_array($field_name, $table_info['session_field'])) {
                                $msg = "Can not edit your self ";
                                throw new Exception($msg);
                            }
                        }
                    }
                    break;
                case 'mod_group_master':
                    $this->CI->db->select("*");
                    $this->CI->db->from("mod_group_master");
                    $this->CI->db->where($table_arr['admin_primary'], $id);
                    $admin_data_obj = $this->CI->db->get();
                    $admin_data = is_object($admin_data_obj) ? $admin_data_obj->result_array() : array();
                    if ($admin_data[0][$table_arr['admin_field']] == $table_arr['admin_value']) {
                        if ($mode == "Delete") {
                            $msg = "Can not delete admin ";
                            throw new Exception($msg);
                        } else {
                            if (in_array($field_name, $table_arr['restrict_field'])) {
                                $msg = "Can not edit admin ";
                                throw new Exception($msg);
                            }
                        }
                    }
                    break;
            }
        } catch (Exception $e) {
            $msg = $e->getMessage();
            $retArr['success'] = 1;
            $retArr['message'] = $msg;
        }
        return $retArr;
    }

    public function getAdminDataRecords($table_name = '') {
        $table_arr = $this->getAdminTableDetails($table_name);
        $admin_records = array();
        switch ($table_name) {
            case 'mod_admin':
                $this->CI->db->select("*");
                $this->CI->db->from("mod_admin");
                $this->CI->db->where($table_arr['admin_primary'], $table_arr['admin_id']);
                $this->CI->db->or_where($table_arr['admin_field'], $table_arr['admin_value']);
                $db_admin_data_obj = $this->CI->db->get();
                $db_admin_data = is_object($db_admin_data_obj) ? $db_admin_data_obj->result_array() : array();
                break;
            case 'mod_group_master':
                $this->CI->db->select("*");
                $this->CI->db->from("mod_group_master");
                $this->CI->db->where($table_arr['admin_field'], $table_arr['admin_value']);
                $db_admin_data_obj = $this->CI->db->get();
                $db_admin_data = is_object($db_admin_data_obj) ? $db_admin_data_obj->result_array() : array();
                break;
        }
        if (is_array($db_admin_data) && count($db_admin_data) > 0) {
            foreach ((array) $db_admin_data as $key => $val) {
                $admin_records[] = $val[$table_arr['admin_primary']];
            }
        }
        return $admin_records;
    }

    public function getAdminTableDetails($table_name = '') {
        switch ($table_name) {
            case 'mod_admin':
                $table_info['admin_primary'] = "iAdminId";
                $table_info['admin_field'] = "vUserName";
                $table_info['admin_value'] = $this->CI->config->item("ADMIN_USER_NAME");
                $table_info['restrict_field'] = array("vUserName", "iGroupId", "eStatus");
                $table_info['session_field'] = array("eStatus");
                $table_info['admin_id'] = $this->CI->session->userdata('iAdminId');
                break;
            case 'mod_group_master':
                $table_info['admin_primary'] = "iGroupId";
                $table_info['admin_field'] = "vGroupCode";
                $table_info['admin_value'] = $this->CI->config->item("ADMIN_GROUP_NAME");
                $table_info['restrict_field'] = array("vGroupCode", "eStatus");
                break;
        }
        return $table_info;
    }

    public function deleteReferenceModules($del_modules = array(), $main_data = array(), $physical_data_remove = "") {
        if (!is_array($del_modules) || count($del_modules) == 0) {
            return FALSE;
        }
        if (!is_array($main_data) || count($main_data) == 0) {
            return FALSE;
        }
        $PHYSICAL_REC_DELETE = $this->CI->config->item('PHYSICAL_RECORD_DELETE');
        foreach ($main_data as $mKey => $mVal) {
            foreach ($del_modules as $dKey => $dVal) {
                $module_name = $dVal['module'];
                $folder_name = $dVal['folder'];
                $rel_source = $dVal['rel_source'];
                $rel_target = $dVal['rel_target'];
                $del_cond = $dVal['extra_cond'];
                $model_name = "model_" . $module_name;
                if ($folder_name == "" || $module_name == "") {
                    continue;
                }
                $this->CI->load->model($folder_name . "/" . $model_name);
                $form_config = $this->CI->$model_name->getFormConfiguration();
                $file_arr = $data_arr = array();
                if (is_array($form_config) && count($form_config) > 0) {
                    foreach ($form_config as $fKey => $fVal) {
                        if ($fVal['type'] == "file" && $fVal['file_upload'] == 'Yes') {
                            $file_arr[] = $fVal;
                        }
                    }
                }
                $extra_cond = $this->CI->db->protect($this->CI->$model_name->table_alias . "." . $rel_source) . " = " . $this->CI->db->escape($mVal[$rel_target]);
                if ($del_cond != "") {
                    $extra_cond .= " AND " . $del_cond;
                }
                if (is_array($file_arr) && count($file_arr) > 0) {
                    $data_arr = $this->CI->$model_name->getData($extra_cond);
                }

                $this->CI->$model_name->physical_data_remove = $physical_data_remove;
                $res = $this->CI->$model_name->delete($extra_cond, "Yes");
                if ($res) {
                    if (is_array($file_arr) && count($file_arr) > 0) {
                        $this->deleteMediaFiles($file_arr, $data_arr, $physical_data_remove);
                    }
                }
            }
        }
        return $res;
    }

    public function deleteMediaFiles($config_arr = array(), $data_arr = array(), $physical_data_remove = "") {
        if (!is_array($config_arr) || count($config_arr) == 0) {
            return FALSE;
        }
        if (!is_array($data_arr) || count($data_arr) == 0) {
            return FALSE;
        }
        if ($this->CI->config->item('PHYSICAL_RECORD_DELETE') && $physical_data_remove == "No") {
            return FALSE;
        }
        foreach ($data_arr as $dKey => $dVal) {
            foreach ($config_arr as $cKey => $cVal) {
                if ($cVal['type'] == "file" && $cVal['file_upload'] == 'Yes') {
                    $folder_name = $cVal['file_folder'];
                    $file_name = $dVal[$cVal['vFieldName']];
                    if ($cVal['file_keep'] != '') {
                        $each_folder = $dVal[$cVal['file_keep']];
                        $file_path = $this->CI->config->item('upload_path') . $folder_name . DS . $each_folder . DS . $file_name . DS;
                    } else {
                        $file_path = $this->CI->config->item('upload_path') . $folder_name . DS . $file_name;
                    }
                    if (file_exists($file_path) && $file_name != "") {
                        $res = @unlink($file_path);
                    }
                }
            }
        }
        return $res;
    }

    public function getSingleColArray($data_arr = array(), $index = "") {
        $retArr = array();
        if (!is_array($data_arr) || count($data_arr) == 0 || $index == "") {
            return $retArr;
        }
        foreach ((array) $data_arr as $key => $val) {
            $retArr[] = $val[$index];
        }
        return $retArr;
    }

    public function getPhysicalRecordWhere($vTableName = '', $vTableAlias = '', $mode = "AR") {
        $extra_cond = '';
        if ($this->CI->config->item('PHYSICAL_RECORD_DELETE')) {
            if ($vTableAlias != "") {
                $extra_cond = $this->CI->db->protect($vTableAlias . ".iSysRecDeleted") . " <> " . $this->CI->db->escape(1);
            } else {
                $extra_cond = $this->CI->db->protect($vTableName . ".iSysRecDeleted") . " <> " . $this->CI->db->escape(1);
            }
        }
        if ($mode == "AR") {
            if ($extra_cond != "") {
                $this->CI->db->where($extra_cond, FALSE, FALSE);
            }
        } else {
            return $extra_cond;
        }
    }

    public function getPhysicalRecordUpdate($vTableAlias = '') {
        $update_arr = array();
        if ($vTableAlias != "") {
            $update_arr[$vTableAlias . ".iSysRecDeleted"] = 1;
        } else {
            $update_arr["iSysRecDeleted"] = 1;
        }
        return $update_arr;
    }

    public function getMD5EncryptString($type = '', $item = '') {
        $admin_url = $this->CI->config->item("admin_url");
        if (in_array($type, array('JavaScript', 'ListPrefer', 'FlowAdd', 'FlowEdit', 'DetailView'))) {
            $str = $admin_url . "_" . $this->CI->session->userdata('iAdminId');
        } else {
            $str = $admin_url;
        }
        switch ($type) {
            //local storage related
            case 'JavaScript':
                $suffix = "JS";
                break;
            case 'ListPrefer':
                $suffix = "LP";
                break;
            //file cache related
            case 'FlowAdd':
                $suffix = "FA";
                break;
            case 'FlowEdit':
                $suffix = "FE";
                break;
            case 'AppCache':
                $suffix = "AC";
                break;
            case 'AppCacheJS':
                $suffix = "AJ";
                break;
            case 'AppCacheCSS':
                $suffix = "AS";
                break;
            //cookie related
            case 'DetailView':
                $suffix = "DV";
                break;
            case 'RememberMe':
                $suffix = "RM";
                break;
        }
        if ($suffix) {
            $str .= "_" . strtolower($suffix);
        }
        if ($item != "") {
            $str .= "_" . strtolower($item);
        }
        $enc_str = md5($str);
        return $enc_str;
    }

    public function trackModuleNavigation($type = "", $navigType = '', $navigAction = 'Viewed', $entityURL = '', $entityName = '', $recName = '', $extra_attr = '') {
        $NAVIGATION_LOG_REQ = $this->CI->config->item('NAVIGATION_LOG_REQ');
        $LogNavOn = (strtolower($NAVIGATION_LOG_REQ) == "y") ? TRUE : FALSE;
        $iAdminId = $this->CI->session->userdata('iAdminId');
        $query_str = '';
        if ($_REQUEST['hashValue'] != "") {
            $query_str = ltrim($_REQUEST['hashValue'], "#");
        } else if ($_REQUEST['extra_hstr'] != "") {
            $query_str = ltrim($_REQUEST['extra_hstr'], "#");
        }
        if (!empty($iAdminId) && $LogNavOn && in_array($type, array("Dashboard", "Module"))) {
            $this->CI->load->model('general/model_navigation');
            if ($type == "Dashboard") {
                $this->CI->db->select("m.vMenuDisplay AS subMenu");
                $this->CI->db->select("m.vURL AS subURL");
                $this->CI->db->select("(SELECT " . $this->CI->db->protect("s.vMenuDisplay") . " FROM " . $this->CI->db->protect("mod_admin_menu") . " AS " . $this->CI->db->protect("s1") . " WHERE " . $this->CI->db->protect("s2.iAdminMenuId") . " = " . $this->CI->db->protect("m.iParentId") . ") AS " . $this->CI->db->protect("mainMenu"), FALSE);
                $this->CI->db->select("(SELECT " . $this->CI->db->protect("s.vURL") . " FROM " . $this->CI->db->protect("mod_admin_menu") . " AS " . $this->CI->db->protect("s2") . " WHERE " . $this->CI->db->protect("s2.iAdminMenuId") . " = " . $this->CI->db->protect("m.iParentId") . ") AS " . $this->CI->db->protect("mainURL"), FALSE);
                $this->CI->db->where("m.vDashBoardPage", $entityName);
                $this->CI->db->order_by("mainMenu", "DESC");
                $this->CI->db->limit(1);
                $db_data_obj = $this->CI->db->get("mod_admin_menu AS m");
                $db_data = is_object($db_data_obj) ? $db_data_obj->result_array() : array();

                $main_menu = $db_data[0]['mainMenu'];
                $sub_menu = $db_data[0]['subMenu'];
                $sup_str = $db_data[0]['mainURL'];
                $navig_str = $entityURL;
            } else if ($type == "Module") {
                $this->CI->db->select("m.vMenuDisplay AS subMenu");
                $this->CI->db->select("m.vURL AS subURL");
                $this->CI->db->select("s.vMenuDisplay AS mainMenu");
                $this->CI->db->select("s.vURL AS mainURL");
                $this->CI->db->join('mod_admin_menu AS s', 's.iAdminMenuId = m.iParentId', 'left');
                $this->CI->db->where("m.vModuleName", $entityName);
                $this->CI->db->order_by("mainMenu", "DESC");
                $this->CI->db->limit(1);
                $db_data_obj = $this->CI->db->get("mod_admin_menu AS m");
                $db_data = is_object($db_data_obj) ? $db_data_obj->result_array() : array();

                $main_menu = $db_data[0]['mainMenu'];
                $sub_menu = $db_data[0]['subMenu'];
                $sup_str = $db_data[0]['mainURL'];
                $navig_str = $entityURL;
            }
            if ($query_str != "") {
                $navig_str .= "|" . $query_str;
            }
            if ($extra_attr != "") {
                $navig_str .= "|" . $extra_attr;
            }
            $sup_str_url = '';
            if ($sup_str != "") {
                $sup_str = trim($sup_str, "|");
                $sup_arr = explode("|", $sup_str);
                $sup_str_url = $this->getAdminEncodeURL($sup_arr[0]);
            }
            $insert_navig_arr['iAdminId'] = $iAdminId;
            $insert_navig_arr['vMainMenu'] = $main_menu;
            $insert_navig_arr['vSubMenu'] = $sub_menu;
            $insert_navig_arr['vRecordName'] = addslashes($recName);
            $insert_navig_arr['vSupQString'] = $sup_str_url;
            $insert_navig_arr['vNavigQString'] = $this->converNavigationVars($navig_str);
            $insert_navig_arr['eNavigAction'] = $navigAction;
            $insert_navig_arr['eNavigType'] = $navigType;
            $insert_navig_arr['dTimeStamp'] = date("Y-m-d H:i:s");
            $iNavigationId = $this->CI->model_navigation->insert($insert_navig_arr);
            return $iNavigationId;
        } else {
            return FALSE;
        }
    }

    public function trackCustomNavigation($navigType = "List", $navigAction = "Viewed", $entityURL = '', $menuCond = '', $recName = '', $extra_attr = '') {
        $NAVIGATION_LOG_REQ = $this->CI->config->item('NAVIGATION_LOG_REQ');
        $LogNavOn = (strtolower($NAVIGATION_LOG_REQ) == "y") ? TRUE : FALSE;
        $iAdminId = $this->CI->session->userdata('iAdminId');
        $query_str = '';
        if ($_REQUEST['hashValue'] != "") {
            $query_str = ltrim($_REQUEST['hashValue'], "#");
        } else if ($_REQUEST['extra_hstr'] != "") {
            $query_str = ltrim($_REQUEST['extra_hstr'], "#");
        }
        if (!empty($iAdminId) && $LogNavOn && $menuCond != "") {
            $this->CI->load->model('general/model_navigation');

            $this->CI->db->select("m.vMenuDisplay AS subMenu");
            $this->CI->db->select("m.vURL AS subURL");
            $this->CI->db->select("s.vMenuDisplay AS mainMenu");
            $this->CI->db->select("s.vURL AS mainURL");
            $this->CI->db->join('mod_admin_menu AS s', 's.iAdminMenuId = m.iParentId', 'left');
            $this->CI->db->where($menuCond, FALSE, FALSE);

            $this->CI->db->order_by("mainMenu", "DESC");
            $this->CI->db->limit(2);
            $db_data_obj = $this->CI->db->get("mod_admin_menu AS m");
            $db_data = is_object($db_data_obj) ? $db_data_obj->result_array() : array();

            $main_menu = $db_data[0]['mainMenu'];
            $sub_menu = $db_data[0]['subMenu'];
            $sup_str = $db_data[0]['mainURL'];
            $navig_str = $entityURL;

            if ($query_str != "") {
                $navig_str .= "|" . $query_str;
            }
            if ($extra_attr != "") {
                $navig_str .= "|" . $extra_attr;
            }

            $sup_str_url = '';
            if ($sup_str != "") {
                $sup_str = trim($sup_str, "|");
                $sup_arr = explode("|", $sup_str);
                $sup_str_url = $this->getAdminEncodeURL($sup_arr[0]);
            }

            $insert_navig_arr['iAdminId'] = $iAdminId;
            $insert_navig_arr['vMainMenu'] = $main_menu;
            $insert_navig_arr['vSubMenu'] = $sub_menu;
            $insert_navig_arr['vRecordName'] = addslashes($recName);
            $insert_navig_arr['vSupQString'] = $sup_str;
            $insert_navig_arr['vNavigQString'] = $this->converNavigationVars($navig_str);
            $insert_navig_arr['eNavigAction'] = $navigAction;
            $insert_navig_arr['eNavigType'] = $navigType;
            $insert_navig_arr['dTimeStamp'] = date("Y-m-d H:i:s");
            $iNavigationId = $this->CI->model_navigation->insert($insert_navig_arr);
            return $iNavigationId;
        } else {
            return FALSE;
        }
    }

    public function converNavigationVars($navig_str = '', $flag = FALSE) {
        $navig_str = trim($navig_str, "|");
        $navig_arr = explode("|", $navig_str);
        if (!is_array($navig_arr) || count($navig_arr) == 0) {
            return $navig_str;
        }
        $decrypt_arr = $this->CI->config->item("FRAMEWORK_ENCRYPTS");
        $repeat_arr = $format_url = array();
        for ($i = 1; $i < count($navig_arr); $i+=2) {
            $key = $navig_arr[$i];
            $param = $navig_arr[$i + 1];
            if (in_array($key, $repeat_arr)) {
                continue;
            }
            $repeat_arr[] = $key;
            $format_url[$i] = $key;
            if (in_array($key, $decrypt_arr) && $param != "") {
                $format_url[$i + 1] = $this->getAdminEncodeURL($param);
            } else {
                $format_url[$i + 1] = $param;
            }
        }
        if ($flag === TRUE) {
            $final_str['module'] = $navig_arr[0];
            $final_str['params'] = @implode("|", $format_url);
        } else {
            $final_str = $navig_arr[0] . "|" . @implode("|", $format_url);
        }
        return $final_str;
    }

    public function allowStripSlashes() {
        if ((version_compare(PHP_VERSION, '5.3.0') >= 0)) {
            return TRUE;
        } else if ((version_compare(PHP_VERSION, '5.3.0') < 0)) {
            if (!get_magic_quotes_gpc()) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getRequestURLParams($omit_flag = FALSE) {
        $FRAMEWORK_VARS = $this->CI->config->item('FRAMEWORK_VARS');
        $FRAMEWORK_OMITS = $this->CI->config->item('FRAMEWORK_OMITS');
        $request_query_url = '';
        if ($this->CI->input->get_post("rfMod") != "" && $this->CI->input->get_post("rfField") != "") {
            $request_query_url .= "&rfMod=" . $this->getAdminEncodeURL($this->CI->input->get_post("rfMod")) . "&rfFod=" . $this->getAdminEncodeURL($this->CI->input->get_post("rfFod")) . "&rfField=" . $this->CI->input->get_post("rfField") . "&rfhtmlID=" . $this->CI->input->get_post("rfhtmlID");
        } else if ($this->CI->input->get_post("loadGrid") != "") {
            $request_query_url .= "&loadGrid=" . $this->CI->input->get_post("loadGrid");
        }
        if ($this->CI->input->get_post("rmPopup") == "true") {
            $request_query_url .= "&rmPopup=" . $this->CI->input->get_post("rmPopup") . "&rmNum=" . $this->CI->input->get_post("rmNum") . "&rmMode=" . $this->CI->input->get_post("rmMode");
        }
        if ($this->CI->input->get_post("tEditFP") == "true") {
            $request_query_url .= '&tEditFP=true';
        }
        if ($this->CI->input->get_post("hideCtrl") == "true") {
            $request_query_url .= '&hideCtrl=true';
        }
        $req_arr = (is_array($_GET)) ? $_GET : array();
        $req_arr = (is_array($_POST)) ? array_merge($req_arr, $_POST) : $req_arr;
        if (is_array($req_arr) && count($req_arr) > 0) {
            foreach ($req_arr as $g_key => $g_val) {
                if (!in_array($g_key, $FRAMEWORK_VARS)) {
                    $request_query_url .= '&' . $g_key . '=' . $g_val;
                } else if ($omit_flag && in_array($g_key, $FRAMEWORK_OMITS)) {
                    $request_query_url .= '&' . $g_key . '=' . $g_val;
                }
            }
        }
        return $request_query_url;
    }

    public function getRequestHASHParams() {
        $FRAMEWORK_VARS = $this->CI->config->item('FRAMEWORK_VARS');
        $qString = $this->CI->config->item('qString');
        $request_hash_url = '';
        if ($this->CI->input->get_post("rfMod") != "" && $this->CI->input->get_post("rfField") != "") {
            $request_hash_url .= "|rfMod|" . $this->getAdminEncodeURL($this->CI->input->get_post("rfMod")) . "|rfFod|" . $this->getAdminEncodeURL($this->CI->input->get_post("rfFod")) . "|rfField|" . $this->CI->input->get_post("rfField") . "|rfhtmlID|" . $this->CI->input->get_post("rfhtmlID");
        } else if ($this->CI->input->get_post("loadGrid") != "") {
            $request_hash_url .= "|loadGrid|" . $this->CI->input->get_post("loadGrid");
        }
        if ($this->CI->input->get_post("rmPopup") == "true") {
            $request_hash_url .= "|rmPopup|" . $this->CI->input->get_post("rmPopup") . "|rmNum|" . $this->CI->input->get_post("rmNum") . "|rmMode|" . $this->CI->input->get_post("rmMode");
        }
        if ($this->CI->input->get_post("tEditFP") == "true") {
            $request_hash_url .= '|tEditFP|true';
        }
        if ($this->CI->input->get_post("hideCtrl") == "true") {
            $request_hash_url .= '|hideCtrl|true';
        }
        $req_arr = (is_array($_GET)) ? $_GET : array();
        $req_arr = (is_array($_POST)) ? array_merge($req_arr, $_POST) : $req_arr;
        if ($qString == "false") {
            return $request_hash_url;
        }
        if (is_array($req_arr) && count($req_arr) > 0) {
            foreach ($req_arr as $g_key => $g_val) {
                if (!in_array($g_key, $FRAMEWORK_VARS)) {
                    $request_hash_url .= '|' . $g_key . '|' . $g_val;
                }
            }
        }
        return $request_hash_url;
    }

    public function getHASHFilterParams($hash_str = '') {
        $FRAMEWORK_VARS = $this->CI->config->item('FRAMEWORK_VARS');
        $request_hash_str = '';
        $hash_str = trim($hash_str, "|");
        $hash_arr = explode("|", $hash_str);
        if (is_array($hash_arr) && count($hash_arr) > 0) {
            for ($i = 0; $i < count($hash_arr); $i+=2) {
                if (!in_array(trim($hash_arr[$i]), $FRAMEWORK_VARS)) {
                    $request_hash_str .= '|' . trim($hash_arr[$i]) . '|' . $hash_arr[$i + 1];
                }
            }
        }
        return $request_hash_str;
    }

    public function displayKeyValueData($selected_val = "", $combo_arr = array(), $is_optgroup = FALSE) {
        $retStr = "";
        if ($is_optgroup == FALSE) {
            if (is_array($combo_arr) && count($combo_arr) > 0) {
                $str = "";
                foreach ($combo_arr as $key => $val) {
                    if (is_array($selected_val) && count($selected_val) > 0) {
                        if (in_array($key, $selected_val)) {
                            $str .= $val . ",";
                        }
                    } else {
                        if ($selected_val == $key) {
                            $str = $val;
                        }
                    }
                }
            }
            $retStr = rtrim($str, ',');
        } else {
            if (is_array($combo_arr) && count($combo_arr) > 0) {
                $str = "";
                foreach ($combo_arr as $key => $val) {
                    foreach ($val as $innerkey => $innerval) {
                        if (is_array($selected_val) && count($selected_val) > 0) {
                            if (in_array($innerkey, $selected_val)) {
                                $str .= $innerval . ",";
                            }
                        } else {
                            if ($selected_val == $innerkey) {
                                $str = $innerval;
                            }
                        }
                    }
                }
                $retStr = rtrim($str, ',');
            }
        }
        return $retStr;
    }

    public function getTokenKeyValueJSON($selected_val = '', $combo_arr = array()) {
        $token_arr = $return_arr = array();
        $keys_arr = (is_array($combo_arr)) ? array_keys($combo_arr) : array();

        $select_arr = explode(',', $selected_val);
        $j = 0;
        for ($i = 0; $i < count($select_arr); $i++) {
            if (in_array($select_arr[$i], $keys_arr)) {
                $token_arr[$j]['id'] = $select_arr[$i];
                $token_arr[$j]['val'] = $combo_arr[$select_arr[$i]];
                $j++;
            }
        }
        $return_arr = $token_arr;
        $json_string = json_encode($return_arr);
        $json_string = htmlspecialchars($json_string, ENT_QUOTES & ~ENT_COMPAT, 'UTF-8');
        return $json_string;
    }

    public function getMinAndMaxYears($type = '') {
        $ret_year = "";
        if ($type == 'min') {
            $ret_year = date("Y") - 100;
        } else if ($type == "Max") {
            $ret_year = date("Y") + 100;
        }
        return $ret_year;
    }

    public function getAdminLangFlagHTML($htmlID = '', $exlang_arr = array(), $lang_info = array()) {
        $return_data = "";
        $admin_flag_path = $this->CI->config->item('admin_lang_flag_path');
        $admin_flag_url = $this->CI->config->item('admin_lang_flag_url');
        $show_all_text = $this->CI->lang->line('GENERIC_SHOW_ALL');
        $hide_all_text = $this->CI->lang->line('GENERIC_HIDE_ALL');
        if (is_array($exlang_arr) && count($exlang_arr) > 0) {
            foreach ($exlang_arr as $key => $val) {
                $lang_title = $lang_info[$val]['vLangTitle'];
                $lang_image_path = $admin_flag_path . $lang_info[$val]['vLangImage'];
                $lang_image_url = $admin_flag_url . $lang_info[$val]['vLangImage'];
                if (!file_exists($lang_image_path)) {
                    $lang_image_url = $admin_flag_url . "NATO.png";
                }
                $return_data .= '<a href="javascript://"  onclick="showAdminLanguageArea(this, \'single\', \'' . $htmlID . '\', \'' . $val . '\')" class="lang-anchor tip" title="' . $lang_title . '">
                                    <img src="' . $lang_image_url . '" alt="' . $lang_title . '">
                                </a>&nbsp;';
            }
        }
        $return_data .= '<strong>|&nbsp;</strong>';
        $return_data .= '<a href="javascript://"  onclick="showAdminLanguageArea(this, \'all\', \'' . $htmlID . '\')" class="show-all-anchor tip" aria-show-text="' . $show_all_text . '" aria-hide-text="' . $hide_all_text . '" title="' . $show_all_text . '" aria-describedby="ui-tooltip-3"><span class="icon16 cut-icon-expand"></span></a>';
        return $return_data;
    }

    public function getMinMaxDateEntry($type = 'min', $date_fomat = '') {
        $start_date = date('Y-m-d');
        $return_time = "";
        if ($type == "min") {
            $minimum_date = $date_fomat;
            if (trim($minimum_date) != '') {
                $min_date_arr = explode("::", $minimum_date);
                $minDatestr = strtotime($min_date_arr[0] . " year " . $min_date_arr[1] . " month " . $min_date_arr[2] . " day", $start_date);
                $return_time = ceil(($minDatestr - $start_date) / 86400);
            }
        } else if ($type == "max") {
            $maximum_date = $date_fomat;
            if (trim($maximum_date) != '') {
                $max_date_arr = explode("::", $maximum_date);
                $maxDatestr = strtotime($max_date_arr[0] . " year " . $max_date_arr[1] . " month " . $max_date_arr[2] . " day", $start_date);
                $return_time = ceil(($maxDatestr - $start_date) / 86400);
            }
        }
        return $return_time;
    }

    public function getCustomHashLink($redirect_link = "") {
        $admin_url = $this->CI->config->item('admin_url');
        $link = "";
        if ($redirect_link != "") {
            if ($this->isExternalURL($redirect_link)) {
                $link = $redirect_link;
            } else {
                $link = $admin_url . $redirect_link;
            }
        }
        return $link;
    }

    /**
     * insertExecutedNotify method is used to insert data inside mod_executed_notifications.
     * 
     * @param array $insert_arr array of data to be inserted.
     * 
     * @return numeric $success inserted id will be return.
     */
    public function insertExecutedNotify($insert_arr = array()) {
        $success = $this->CI->db->insert("mod_executed_notifications", $insert_arr);
        return $success;
    }

    public function getEmailOutput($type = 'subject') {
        if ($type == "subject") {
            $ret_str = $this->_email_subject;
            $this->_email_subject = '';
        } else if ($type == "content") {
            $ret_str = $this->_email_content;
            $this->_email_content = '';
        }
        return $ret_str;
    }

    public function getPushNotifyOutput($type = 'body') {
        if ($type == "body") {
            $ret_str = $this->_push_content;
            $this->_push_content = '';
        }
        return $ret_str;
    }

    public function getNotifyErrorOutput() {
        $ret_str = $this->_notify_error;
        $this->_notify_error = '';
        return $ret_str;
    }

    public function getQueryLogFiles() {
        $admin_log_path = $this->CI->config->item('admin_query_log_path');
        $log_files = array();
        if (@is_dir($admin_log_path)) {
            $handle = opendir($admin_log_path);
            while (FALSE !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    if (count($log_files) > 100) {
                        break;
                    }
                    $fdate = filemtime($admin_log_path . DS . $file);
                    $log_files[$fdate] = $file;
                    $i++;
                }
            }
            krsort($log_files);
            $log_files = array_values($log_files);
        }
        return $log_files;
    }

    public function makeLableNameFromString($str = '') {
        // spcial chars code generation
        $special_char_find = array(
            "!", "#", "$", "%", "&", "(", ")", "*", "+",
            ",", "-", ".", "/", ":", ";", "<", "=", ">",
            "?", "@", "[", "]", "^", "{", "|", "}", "~"
        );
        $special_char_replace = array(
            "_c33", "_c35", "_c36", "_c37", "_c38", "_c40", "_c41", "_c42", "_c43",
            "_c44", "_c45", "_c46", "_c47", "_c58", "_c59", "_c60", "_c61", "_c62",
            "_c63", "_c64", "_c91", "_c93", "_c94", "_c123", "_c124", "_c125", "_c126"
        );
        $str = str_replace($special_char_find, $special_char_replace, $str);

        $str = strtolower(preg_replace("/[^A-Za-z0-9_]/", '', str_replace(' ', '_', trim($str))));
        $str = trim($str, "_");
        return $str;
    }

    public function getDisplayLabel($vModule = '', $label_text = '', $type = 'tpl') {
        $return_label = $label_text;
        if ($label_text != "") {
            $lablename = $this->makeLableNameFromString($label_text);
            $final_label = strtoupper($vModule) . "_" . strtoupper($lablename);
            if ($type == "label") {
                $return_label = $final_label;
            } elseif ($type == "php") {
                $return_label = "\$this->lang->line('" . $final_label . "')";
            } else {
                $return_label = "<%\$this->lang->line('" . $final_label . "')%>";
            }
        }

        return $return_label;
    }

    public function replaceDisplayLabel($label_text = '', $find_text = '', $repace_text = '') {
        $return_label = $label_text;
        if ($label_text != "") {
            $return_label = str_replace($find_text, $repace_text, $label_text);
        }
        return $return_label;
    }

    public function getEnumValuesLabels($module = '', $status_array = array()) {
        $status_array_lang = $status_array;
        if (is_array($status_array) && count($status_array) > 0) {
            for ($i = 0; $i < count($status_array); $i++) {
                $label = $this->getDisplayLabel($module, $status_array[$i], 'label');
                $status_array_lang[$i] = "js_lang_label." . $label;
            }
        }
        return $status_array_lang;
    }

    public function processMessageLabel($message_label = '', $data_arr = array()) {
        $return_text = $message_label;
        if ($message_label != "") {
            $return_text = $this->CI->lang->line($message_label);
            $return_text = $this->renderMessageLabel($return_text, $data_arr);
        }
        return $return_text;
    }

    public function renderMessageLabel($message = '', $data_arr = array()) {
        if (!$message || !is_array($data_arr) || count($data_arr) == 0 || (strstr($message, '#') !== FALSE)) {
            return $message;
        }
        foreach ($data_arr as $key => $value) {
            $hash_key = '#' . $key . '#';
            if (strstr($message, $hash_key)) {
                $message = str_replace($hash_key, $value, $message);
            }
        }
        return $message;
    }

    public function parseTPLMessage($message_label = '') {
        $return_text = $message_label;
        if ($message_label != "") {
            $return_text = $this->CI->lang->line($message_label);
            $return_text = str_replace('"', '\"', ($return_text));
        }
        return $return_text;
    }

    public function parseLabelMessage($search_label = '', $find_text = '', $repace_label = '') {
        $search_text = $this->CI->lang->line($search_label);
        $repace_text = $this->CI->lang->line($repace_label);
        if ($search_text != "" && $find_text != "") {
            $search_text = str_replace($find_text, $repace_text, $search_text);
        }
        return $search_text;
    }

    public function processQuotes($text = '', $type = 'double') {
        if ($text != "") {
            $text = str_replace('"', '\"', ($text));
            $text = str_replace(array("\r", "\n"), '', $text);
        }
        return $text;
    }

    public function validateFileSize($permitted_file_size = '', $upload_size = '') {
        $upload_size_kb = ceil($upload_size / 1024);
        if ($permitted_file_size >= $upload_size_kb) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function loadEncryptLibrary() {
        if (class_exists("Ci_encrypt", FALSE)) {
            $this->CI->ci_encrypt = new Ci_encrypt();
        } else {
            $this->CI->load->library("ci_encrypt");
        }
    }

    public function isAdminEncodeActive() {
        $this->loadEncryptLibrary();
        $is_active = $this->CI->ci_encrypt->isEncryptionActive();
        return $is_active;
    }

    public function getAdminEncodeURL($url = '', $ret_whole_url = 0, $is_url = FALSE) {
        $this->loadEncryptLibrary();
        $url_t = $tURL = $url;
        $admin_url = $this->CI->config->item("admin_url");
        if ($this->CI->config->item("ADMIN_URL_ENCRYPTION") == 'Y') {
            if ($url != "") {
                $url_t = str_replace($admin_url, "", $url);
                if ($url_t != "") {
                    $url_t = $this->CI->ci_encrypt->encrypt($url_t, $is_url);
                }
            }
        }
        if ($ret_whole_url == 1) {
            $tURL = $admin_url . $url_t;
        } else {
            $tURL = $url_t;
        }
        return $tURL;
    }

    public function getAdminDecodeURL($url = '', $ret_whole_url = 0, $is_url = FALSE) {
        $this->loadEncryptLibrary();
        $url_t = $tURL = $url;
        $admin_url = $this->CI->config->item("admin_url");
        if ($this->CI->config->item("ADMIN_URL_ENCRYPTION") == 'Y') {
            if ($url != "") {
                $url_t = str_replace($admin_url, "", $url);
                if ($url_t != "") {
                    $url_t = $this->CI->ci_encrypt->decrypt($url_t, $is_url);
                }
            }
        }
        if ($ret_whole_url == 1) {
            $tURL = $admin_url . $url_t;
        } else {
            $tURL = $url_t;
        }
        return $tURL;
    }

    public function getCustomEncryptMode($ret = FALSE) {
        $ret_arr['Add'] = $this->getAdminEncodeURL("Add");
        $ret_arr['View'] = $this->getAdminEncodeURL("View");
        $ret_arr['Update'] = $this->getAdminEncodeURL("Update");
        $ret_arr['Search'] = $this->getAdminEncodeURL("Search");
        if ($ret === TRUE) {
            $ret_arr_enc = $ret_arr;
        } else {
            $ret_arr_enc = json_encode($ret_arr);
        }
        return $ret_arr_enc;
    }

    public function getCustomEncryptURL($code = '', $ret = FALSE) {
        $general_url = array();

        $general_url['dashboard_index'] = "dashboard/dashboard/sitemap";
        $general_url['dashboard_sitemap'] = "dashboard/dashboard/sitemap";
        $general_url['dashboard_sequence'] = "dashboard/dashboard/dashboard_sequence_a";
        $general_url['filter_dashboard'] = "dashboard/dashboard/filter_dashboard_block";
        $general_url['autoload_dashboard'] = "dashboard/dashboard/autoload_dashboard_block";

        $general_url['general_navigation_index'] = "general/navigation/index";
        $general_url['general_navigation_flush'] = "general/navigation/flush_record";
        $general_url['general_clear_query_log'] = "general/navigation/clear_query_log";
        $general_url['general_clear_query_cache'] = "general/navigation/clear_query_cache";
        $general_url['general_query_log_page'] = "general/navigation/query_log_page";
        $general_url['general_query_log'] = "general/navigation/query_log";
        $general_url['general_error_log'] = "general/navigation/error_log";
        $general_url['general_preferences_change'] = "general/navigation/change_preferences";
        $general_url['general_grid_render_action'] = "general/gridactions/grid_render_action";
        $general_url['general_grid_submit_action'] = "general/gridactions/grid_submit_action";

        $general_url['general_language_change'] = "general/multilingual/language_change";
        $general_url['general_language_conversion'] = "general/multilingual/language_conversion";

        $general_url['user_login_entry'] = "user/login/entry";
        $general_url['user_login_logout'] = "user/login/logout";

        $general_url['user_auto_logoff'] = "user/login/auto_logoff";
        $general_url['user_sess_expire'] = "user/login/sess_expire";
        $general_url['user_notify_events'] = "user/login/notify_events";
        $general_url['user_manifest'] = "user/login/manifest";
        $general_url['user_tbcontent'] = "user/login/tbcontent";
        $general_url['user_top_panel'] = "user/login/get_top_panel";
        $general_url['user_bot_panel'] = "user/login/get_bot_panel";

        $general_url['systememail_variables'] = "tools/systememails/getVariables";
        if ($ret === TRUE) {
            $general_url['user_login_index'] = "user/login/index";
            $general_url['user_login_entry_a'] = "user/login/entry_a";
            // $general_url['user_login_logout'] = "user/login/logout";
            $general_url['user_forgot_password_action'] = "user/login/forgot_password_action";
            $general_url['user_changepassword'] = "user/login/changepassword";
            $general_url['user_changepassword_action'] = "user/login/changepassword_action";
            $general_url['user_resetpassword'] = "user/login/resetpassword";
            $general_url['user_resetpassword_action'] = "user/login/resetpassword_action";

            $general_url['settings_index'] = "tools/settings/index";
            $general_url['settings_action'] = "tools/settings/settings_action";
            $general_url['settings_upload_files'] = "tools/settings/uploadSettingFiles";

            $general_url['bulkmail_action'] = "tools/bulkemail/bulkmail_action";
            $general_url['bulkmail_sendto'] = "tools/bulkemail/ajax_bulk_mail";
            $general_url['bulkmail_variables'] = "tools/bulkemail/ajax_bulk_temp_variables";
            //Notify realtes urls
            $general_url['pushnotify_action'] = "tools/pushnotify/pushnotify_action";
            $general_url['pushnotify_variables'] = "tools/pushnotify/pushnotify_variables";
            $general_url['pushnotify_module_fields'] = "tools/pushnotify/pushnotify_module_fields";
            $general_url['pushnotify_select_fields'] = "tools/pushnotify/pushnotify_select_fields";
            //Backup related urls
            $general_url['backup_index'] = "tools/backup/index";
            $general_url['backup_table_backup'] = "tools/backup/table_backup";
            $general_url['backup_backup_form_a'] = "tools/backup/backup_form_a";
            $general_url['backup_create_backup'] = "tools/backup/create_backup";
            $general_url['backup_backup_delete'] = "tools/backup/backup_delete";
            $general_url['backup_backup_download_a'] = "tools/backup/backup_download_a";
            //Import related urls
            $general_url['import_index'] = "tools/import/index";
            $general_url['import_upload'] = "tools/import/upload";
            $general_url['import_read'] = "tools/import/read";
            $general_url['import_process'] = "tools/import/process";
            $general_url['import_info'] = "tools/import/import_info";
            $general_url['import_valid'] = "tools/import/import_valid";
            $general_url['import_history'] = "tools/import/import_history";
            $general_url['import_gdrive_manager'] = "tools/import/gdrive_manager";
            $general_url['import_gdrive_config'] = "tools/import/gdrive_config";
            $general_url['import_gdrive_auth'] = "tools/import/gdrive_auth";
            $general_url['import_get_gdrive_data'] = "tools/import/get_gdrive_data";
            $general_url['import_save_gdrive_data'] = "tools/import/save_gdrive_data";
            $general_url['import_get_weburl_data'] = "tools/import/get_weburl_data";
            $general_url['import_dropbox_auth'] = "tools/import/dropbox_auth";
            $general_url['import_get_dropbox_data'] = "tools/import/get_dropbox_data";
            $general_url['import_save_dropbox_data'] = "tools/import/save_dropbox_data";
        }
        $ret_arr = array();
        foreach ($general_url as $key => $val) {
            if (is_array($code) && count($code) > 0) {
                if (in_array($key, $code)) {
                    $ret_arr[$key] = $this->getAdminEncodeURL($val, 0, TRUE);
                }
            } else if ($code != "") {
                if ($code == $key) {
                    $ret_arr[$key] = $this->getAdminEncodeURL($val, 0, TRUE);
                }
            } else {
                $ret_arr[$key] = $this->getAdminEncodeURL($val, 0, TRUE);
            }
        }
        if ($ret === TRUE) {
            $ret_arr_enc = $ret_arr;
        } else {
            $ret_arr_enc = json_encode($ret_arr);
        }
        return $ret_arr_enc;
    }

    public function getGeneralEncryptList($folder_name = '', $module_name = '') {
        $folder_name = trim($folder_name);
        $module_name = trim($module_name);
        $func_enc_arr = array();
        if ($folder_name == "" || $module_name == "") {
            return $func_enc_arr;
        }
        $func_dec_arr = $this->getGeneralEncryptFunc();
        if (!is_array($func_dec_arr) || count($func_dec_arr) == 0) {
            return $func_enc_arr;
        }
        $enc_module_str = $folder_name . "/" . $module_name . "/";
        foreach ($func_dec_arr as $key => $val) {
            $func_enc_arr[$key] = $this->getAdminEncodeURL($enc_module_str . $val);
        }
        return $func_enc_arr;
    }

    public function getGeneralEncryptFunc() {
        $gen_arr = array(
            "index" => "index",
            "listing" => "listing",
            "export" => "export",
            "add" => "add",
            "add_action" => "addAction",
            "inline_edit_action" => "inlineEditAction",
            "form_edit_action" => "formEditAction",
            "get_list_options" => "getListOptions",
            "get_form_options" => "getFormOptions",
            "get_source_options" => "getSourceOptions",
            "parent_source_options" => "parentSourceOptions",
            "get_chosen_auto_complete" => "getChosenAutoComplete",
            "get_token_auto_complete" => "getTokenAutoComplete",
            "get_search_auto_complete" => "getSearchAutoComplete",
            "get_left_search_content" => "getLeftSearchContent",
            "download_list_file" => "downloadListFile",
            "upload_form_file" => "uploadFormFile",
            "download_form_file" => "downloadFormFile",
            "delete_form_file" => "deleteFormFile",
            "get_tab_wise_block" => "getTabWiseBlock",
            "save_tab_wise_block" => "saveTabWiseBlock",
            "get_self_switch_to" => "getSelfSwitchTo",
            "get_parent_switch_to" => "getParentSwitchTo",
            "child_data_add" => "childDataAdd",
            "child_data_save" => "childDataSave",
            "child_data_delete" => "childDataDelete",
            "get_subgrid_block" => "getSubgridBlock",
            "get_detail_view_block" => "getDetailViewBlock", //NR
            "expand_subgrid_view" => "expandSubgridView", //NR
            "expand_subgrid_list" => "expandSubgridList", //NR
            "top_detail_view" => "topDetailView", //NR
            "get_relation_module" => "getRelationModule", //NR
            "add_action_popup" => "addActionPopup",
            "print_record" => "printRecord",
            "process_configuration" => "processConfiguration", //NR
        );
        return $gen_arr;
    }

    public function writeMultilingualStaticPages($data_arr = array(), $unique_name = '') {
        $prlang = $this->CI->config->item("PRIME_LANG");
        $exlang_arr = $this->CI->config->item("OTHER_LANG");
        $unique_data = $this->CI->input->get_post($unique_name);
        $this->writeStaticPageContent($data_arr['vPageCode'], $prlang, $unique_data);

        if (is_array($exlang_arr) && count($exlang_arr) > 0) {
            $lang_data = $this->CI->input->get_post("lang" . $unique_name);
            for ($i = 0; $i < count($exlang_arr); $i++) {
                if ($exlang_arr[$i]) {
                    $this->writeStaticPageContent($data_arr['vPageCode'], $exlang_arr[$i], $lang_data[$exlang_arr[$i]]);
                }
            }
        }
        return TRUE;
    }

    public function writeStaticPageContent($page_code = '', $lang_code = '', $data = '', $binary = FALSE) {
        $page_name = $page_code . '_' . strtolower($lang_code) . '.tpl';
        $file_path = $this->CI->config->item('static_pages_path') . $page_name;
        $handle = fopen($file_path, 'w');
        if ($binary) {
            fwrite($handle, $data);
        } else {
            fputs($handle, $data);
        }
        fclose($handle);
        return TRUE;
    }

    public function getCustomAddEditPageURL($module_name = '', $normal_url = '', $custom_url = '') {
        if (trim($custom_url) == "") {
            return $normal_url;
        } else {
            $return_arr = $this->converNavigationVars($custom_url, TRUE);
            if (trim($return_arr['module']) == "") {
                return $normal_url;
            }
            $return_url = $this->getAdminEncodeURL($return_arr['module']) . "|" . $return_arr['params'];
        }
        return $return_url;
    }

    public function getModuleListDropdown() {
        $this->CI->load->library('ci_misc');
        $ret_arr = array();
        $lang_dashboard = $this->CI->lang->line("LANGUAGELABELS_DASHBOARD");
        $lang_generic = $this->CI->lang->line("LANGUAGELABELS_GENERIC");
        $lang_action = $this->CI->lang->line("LANGUAGELABELS_ACTION");
        $lang_front = $this->CI->lang->line("LANGUAGELABELS_FRONT");
        $db_app_list[$lang_dashboard] = array('Dashboard' => $lang_dashboard);
        $db_app_list[$lang_generic] = array('Generic' => $lang_generic);
        $db_app_list[$lang_action] = array('Action' => $lang_action);
        $db_app_list[$lang_front] = array('Front' => $lang_front);
        $module_array = $this->CI->ci_misc->getModuleArray();
        $db_app_list[$this->CI->lang->line("LANGUAGELABELS_MODULE")] = $module_array;
        if (is_array($db_app_list) && count($db_app_list) > 0) {
            foreach ($db_app_list as $key => $value) {
                foreach ($value as $inn_key => $inn_val) {
                    $ret_arr[] = array(
                        "id" => $inn_key,
                        "val" => $inn_val,
                        "grpVal" => $key
                    );
                }
            }
        }
        return $ret_arr;
    }

    public function checkUserAccountStatus() {
        if ($this->CI->config->item('ACCOUNT_CHECK_ENABLE')) {
            $user_acc_status = $this->CI->config->item('USER_ACC_STATUS_INFO');
            if (in_array($user_acc_status, array("Expired", "Closed"))) {
                $login_arr = $this->getCustomEncryptURL("user_login_entry", TRUE);
                $redir_uri = $this->CI->config->item("admin_url") . $login_arr["user_login_entry"] . "?_=" . time();
                if ($user_acc_status == "Closed") {
                    $err_msg = $this->processMessageLabel('ACTION_YOUR_ACCOUNT_CLOSED_PLEASE_TRY_AGAIN_OR_CONTACT_ADMINISTRATOR');
                } else {
                    $err_msg = $this->processMessageLabel('ACTION_YOUR_ACCOUNT_EXPIRED_PLEASE_TRY_AGAIN_OR_CONTACT_ADMINISTRATOR');
                }
                $this->CI->session->set_flashdata('failure', $err_msg);
                redirect($redir_uri);
            }
        }
        return;
    }

    public function makeNotificationLink($value = '', $id = '', $data = array()) {
        if ($data['men_notification_type'] == 'EmailNotify') {
            $ret_str = '<a title="Click Here" href="' . $this->CI->config->item('admin_url') . '#' . $this->getAdminEncodeURL("notifications/notifications/viewContent") . '|id|' . $this->getAdminEncodeURL($id) . '" class="fancybox-popup"> Click Here </a>';
            return $ret_str;
        } else {
            return $value;
        }
    }

    public function getAppCacheStatus() {
        $ret_val = "No";
        if ($this->CI->session->userdata('iAdminId') != '' && $this->CI->config->item("ADMIN_ASSETS_APPCACHE") == 'Y') {
            $cookie_str = $this->CI->ci_local->read($this->getMD5EncryptString("AppCache"), -1);
            $cookie_js = $this->CI->ci_local->read($this->getMD5EncryptString("AppCacheJS"), -1);
            $cookie_css = $this->CI->ci_local->read($this->getMD5EncryptString("AppCacheCSS"), -1);
            $common_js_dir = $this->CI->config->item('js_cache_path') . "compiled/" . $cookie_js . "/";
            $common_css_dir = $this->CI->config->item('css_cache_path') . "compiled/" . $cookie_css . "/";
            if ($cookie_str == "Yes" && is_dir($common_js_dir) && is_dir($common_css_dir)) {
                if (file_exists($common_js_dir . "main_common.js") && file_exists($common_css_dir . "main_common.css")) {
                    $ret_val = "Yes";
                }
            }
        }

        return $ret_val;
    }

    public function getJSLanguageLables() {
        return $this->CI->js->js_labels_src();
    }

    public function getServerThemeArr() {
        $this->CI->load->library("ci_theme");
        $this->CI->ci_theme->setServerThemeSettings();
        $theme_settings_arr = $this->CI->ci_theme->getServerThemeSettings();
        return $theme_settings_arr;
    }

    public function getClientThemeJSON($flag = FALSE) {
        $this->CI->load->library("ci_theme");
        $this->CI->ci_theme->setClientThemeSettings();
        $theme_settings_arr = $this->CI->ci_theme->getClientThemeSettings();
        if ($flag === TRUE) {
            return $theme_settings_arr;
        } else {
            return json_encode($theme_settings_arr);
        }
    }

    public function getAdminPHPFormats($type = '') {
        $this->CI->load->library('filter');
        switch ($type) {
            case 'date':
                $fmt = $this->CI->config->item('ADMIN_DATE_FORMAT');
                break;
            case 'date_and_time':
                $fmt = $this->CI->config->item('ADMIN_DATE_TIME_FORMAT');
                break;
            case 'time':
                $fmt = $this->CI->config->item('ADMIN_TIME_FORMAT');
                break;
            case 'phone':
                $phone_format = $this->CI->config->item('ADMIN_PHONE_FORMAT');
                $fmt = (trim($phone_format) != '') ? $phone_format : "(999) 999-9999";
                break;
        }
        if (in_array($type, array("date", "date_and_time", "time"))) {
            $format = $this->CI->filter->dateTimePHPFormats($type, $fmt);
        } else {
            $format = $fmt;
        }
        return $format;
    }

    public function getAdminJSFormats($type = '', $key = '') {
        $this->CI->load->library('filter');
        switch ($type) {
            case 'date':
                $fmt_arr = $this->CI->filter->dateTimeJSFormats('date', $this->CI->config->item('ADMIN_DATE_FORMAT'));
                break;
            case 'date_and_time':
                $fmt_arr = $this->CI->filter->dateTimeJSFormats('date_and_time', $this->CI->config->item('ADMIN_DATE_TIME_FORMAT'));
                break;
            case 'time':
                $fmt_arr = $this->CI->filter->dateTimeJSFormats('time', $this->CI->config->item('ADMIN_TIME_FORMAT'));
                break;
        }
        if ($key != '') {
            return $fmt_arr[$key];
        } else {
            return $fmt_arr;
        }
    }

    public function getAdminJSMoments($type = '', $key = '') {
        $this->CI->load->library('filter');
        if ($key != "") {
            switch ($type) {
                case 'date':
                    $fmt_arr = $this->CI->filter->dateTimeJSFormats('date', $this->CI->config->item('ADMIN_DATE_FORMAT'));
                    break;
                case 'date_and_time':
                    $fmt_arr = $this->CI->filter->dateTimeJSFormats('date_and_time', $this->CI->config->item('ADMIN_DATE_TIME_FORMAT'));
                    break;
                case 'time':
                    $fmt_arr = $this->CI->filter->dateTimeJSFormats('time', $this->CI->config->item('ADMIN_TIME_FORMAT'));
                    break;
            }
            $fmt = $fmt_arr[$key];
        } else {
            switch ($type) {
                case 'date':
                    $fmt = $this->CI->filter->dateTimeJSMoments('date', $this->CI->config->item('ADMIN_DATE_FORMAT'));
                    break;
                case 'date_and_time':
                    $fmt = $this->CI->filter->dateTimeJSMoments('date_and_time', $this->CI->config->item('ADMIN_DATE_TIME_FORMAT'));
                    break;
                case 'time':
                    $fmt = $this->CI->filter->dateTimeJSMoments('time', $this->CI->config->item('ADMIN_TIME_FORMAT'));
                    break;
            }
        }
        return $fmt;
    }

    public function getAdminTPLFormats($flag = FALSE) {
        $this->CI->load->library('filter');
        $phone_format = $this->CI->config->item('ADMIN_PHONE_FORMAT');
        $thousand_sep = $this->CI->config->item('ADMIN_THOUSAND_SEPARATOR');
        $decimal_sep = $this->CI->config->item('ADMIN_DECIMAL_SEPARATOR');
        $decimal_plc = $this->CI->config->item('ADMIN_DECIMAL_PLACES');
        $currency_pre = $this->CI->config->item('ADMIN_CURRENCY_PREFIX');
        $currency_suf = $this->CI->config->item('ADMIN_CURRENCY_SUFFIX');

        $ret_arr = array();

        $ret_arr['date']['format'] = $this->CI->filter->dateTimeJSFormats('date', $this->CI->config->item('ADMIN_DATE_FORMAT'));
        $ret_arr['date']['moment'] = $this->CI->filter->dateTimeJSMoments('date', $this->CI->config->item('ADMIN_DATE_FORMAT'));

        $ret_arr['date_and_time']['format'] = $this->CI->filter->dateTimeJSFormats('date_and_time', $this->CI->config->item('ADMIN_DATE_TIME_FORMAT'));
        $ret_arr['date_and_time']['moment'] = $this->CI->filter->dateTimeJSMoments('date_and_time', $this->CI->config->item('ADMIN_DATE_TIME_FORMAT'));

        $ret_arr['time']['format'] = $this->CI->filter->dateTimeJSFormats('time', $this->CI->config->item('ADMIN_TIME_FORMAT'));
        $ret_arr['time']['moment'] = $this->CI->filter->dateTimeJSMoments('time', $this->CI->config->item('ADMIN_TIME_FORMAT'));

        $ret_arr['phone_format'] = (trim($phone_format) != '') ? $phone_format : "(999) 999-9999";
        $ret_arr['thousand_seperator'] = ($thousand_sep == 'space' || $thousand_sep == 'none') ? ($thousand_sep == 'none' ? '' : ' ') : ',';
        $ret_arr['decimal_seperator'] = $decimal_sep == 'comma' ? ',' : '.';
        $ret_arr['decimal_places'] = $decimal_plc != '' ? $decimal_plc : '2';
        $ret_arr['currency_prefix'] = $currency_pre != '' ? $currency_pre : '';
        $ret_arr['currency_suffix'] = $currency_suf != '' ? $currency_suf : '';

        if ($flag === TRUE) {
            return $ret_arr;
        } else {
            return json_encode($ret_arr);
        }
    }

    public function getExpressionEvalObject() {
        if (is_object($this->_expression_eval)) {
            return $this->_expression_eval;
        }
        require_once(APPPATH . 'third_party/eqEOS/eos.class.php');
        $this->_expression_eval = new eqEOS();
        return $this->_expression_eval;
    }

    public function evaluateMathExpression($expression = '') {
        if (trim($expression) == '') {
            return $expression;
        }
        $eos = $this->getExpressionEvalObject();
        $value = $eos->solveIF($expression);
        return $value;
    }

    public function getResizedLogoImage($path = '', $url = '') {
        if (!file_exists($path)) {
            return FALSE;
        }
        $logo_max_width = 220;
        $logo_max_height = 57;
        $logo_bg_color = '01bbe4';
        $logo_image_info = getimagesize($path);
        $logo_height = $logo_width = '';
        if (intval($logo_image_info[0]) > $logo_max_width) {
            $logo_width = $logo_max_width;
        }
        if (intval($logo_image_info[1]) > $logo_max_height) {
            $logo_height = $logo_max_height;
        }
        if ($logo_height == '' || $logo_width == '') {
            return $url;
        }
        $enc_image_url = base64_encode($url);
        $logo_image_url = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=' . $logo_height . '&width=' . $logo_width . '&color=' . $logo_bg_color;
        return $logo_image_url;
    }

    public function getLangRequestValue() {
        $config_lang = $this->CI->config->item("SYSTEM_LANG_PARAM");
        $config_lang = ($config_lang != "") ? $config_lang : "lang_id";
        $lang_id = $this->CI->input->get_post($config_lang, TRUE);
        $lang_code = ($lang_id != "") ? $lang_id : "EN";
        return $lang_code;
    }

    public function getDBQueriesList() {
        $db_queries = $this->CI->db->queries;
        $db_query_times = $this->CI->db->query_times;
        $queries_log = array();
        $skip_arr = array(
            //skip queries
            "SELECT CASE WHEN (@@OPTIONS | 256) = @@OPTIONS THEN 1 ELSE 0 END AS qi",
            "SELECT  TOP " . $this->CI->config->item("db_max_limit") . " " . $this->CI->db->protect("vName") . ", " . $this->CI->db->protect("vValue") . " FROM " . $this->CI->db->protect("mod_setting"),
            "SELECT " . $this->CI->db->protect("vName") . ", " . $this->CI->db->protect("vValue") . " FROM " . $this->CI->db->protect("mod_setting"),
            "SELECT " . $this->CI->db->protect("mwt.*") . " FROM " . $this->CI->db->protect("mod_ws_token") . " AS " . $this->CI->db->protect("mwt") . " WHERE " . $this->CI->db->protect("mwt.vWSToken"),
            "UPDATE " . $this->CI->db->protect("mod_ws_token") . " SET " . $this->CI->db->protect("dLastAccess"),
            'SELECT  TOP 25000 "vName", "vValue" FROM "mod_setting"'
        );
        for ($i = 0, $j = 0; $i < count($db_queries); $i++) {
            $query = $db_queries[$i];
            $query = str_replace(array("\n", "\r"), " ", $query);
            foreach ($skip_arr as $needle) {
                if (strpos($query, $needle) === FALSE) {
                    continue 1;
                } else {
                    continue 2;
                }
            }
            $queries_log[$j]['query'] = $query;
            $queries_log[$j]['time(ms)'] = round(($db_query_times[$i] * 1000), 3);
            $j++;
        }
        $queries_log[0]['count'] = count($queries_log);
        return $queries_log;
    }

    /**
     * Code will be generated dynamically
     * Please do not write or change the content below this line
     * Five hashes must be there on either side of string.
     */
    #####GENERATED_CUSTOM_FUNCTION_START#####

    function BuildTree($ar = array(), $parentId = 0) {
        //pr($ar);exit();
        $branch = array();
        $elements = $ar;
        foreach ($elements as $element) {
            if ($element['mmi_parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['mmi_mst_menu_items_id']);
                #pr($children);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        $return_array = $branch;
        //pr($return_array);exit;
        return $return_array;
    }

    function Menu($header = array()) {
        //pr($header);exit();
        $header['query'] = $this->getCleanURL($header['query']);
        $headerarr = array();
        foreach ($header['menu'] as $head) {
            $headerarr[$head['mm_menu_key']] = array(
                'mm_title' => $head['mm_title'],
            );
            foreach ($header['query'] as $items) {
                if ($head['mm_mst_menu_id'] == $items['mmi_mst_menu_id']) {
                    $headerarr[$head['mm_menu_key']]['items'][] = array(
                        'mmi_mst_menu_items_id' => $items['mmi_mst_menu_items_id'],
                        'mmi_mst_menu_id' => $items['mmi_mst_menu_id'],
                        'mmi_title' => $items['mmi_title'],
                        'mmi_parent_id' => $items['mmi_parent_id'],
                        'mmi_external_link' => $items['mmi_external_link'],
                        'mmi_resource_type' => $items['mmi_resource_type'],
                        'mmi_login_req' => $items['mmi_login_req'],
                    );
                }
            }
        }

        $arr = $this->BuildTree($headerarr['HEADERMENU']['items'], $parentId = 0);
        //pr($arr);
        //pr($headerarr);
        //exit();
        return $return_arr[0]['menu_items'] = $headerarr;
    }

    function getFullName($mode = '', $id = '', $parID = '') {
        $_POST['ma_name'] = $_POST['mp_first_name'] . ' ' . $_POST['mp_last_name'];
        $ret_arr['success'] = true;
        return $ret_arr;
    }

    function getCleanURL($items = array()) {
        foreach ($items as $key => $item) {
            //if($item['mmi_resource_type'] != 'Static_Page' && $item['mmi_resource_type'] != 'External_Link'){
            if ($item['mmi_resource_type'] == 'Category') {
                /*
                  $this->CI->db->select('vCleanUrl');
                  $this->CI->db->from('trn_clean_url');
                  $this->CI->db->where(array('eResourceType'=>'C','iResourceId'=>$item['mmi_category_id']));
                  $result = $this->CI->db->get()->result_array();
                  $items[$key]['mmi_external_link'] = $result[0]['vCleanUrl'];
                 */
                $items[$key]['mmi_external_link'] = @$this->CleanUrl('C', $item['mmi_category_id']);
            }
            if ($item['mmi_resource_type'] == 'Product') {
                /*
                  $this->CI->db->select('vCleanUrl');
                  $this->CI->db->from('trn_clean_url');
                  $this->CI->db->where(array('eResourceType'=>'P','iResourceId'=>$item['mmi_product_id']));
                  $result = $this->CI->db->get()->result_array();
                  $items[$key]['mmi_external_link'] = $result[0]['vCleanUrl'];
                 */
                $items[$key]['mmi_external_link'] = @$this->CleanUrl('P', $item['mmi_product_id']);
            }

            //}
            if ($item['mmi_resource_type'] == 'Static_Page') {
                $this->CI->db->select('vUrl');
                $this->CI->db->from('mod_page_settings');
                $this->CI->db->where('iPageId', $item['mmi_static_page_id']);
                $result = $this->CI->db->get()->result_array();
                $items[$key]['mmi_external_link'] = $result[0]['vUrl'];
            }
        }
        return $items;
    }

    function GetOtherStoreData($mode = '', $id = '', $parID = '') {
        $_POST['ma_user_name'] = $_POST['ma_email'];
        if (isset($_POST['msd_contact_name']) && !empty($_POST['msd_contact_name']) && ($_POST['tab_id'] == 2 || $_POST['tab_id'] == 3)) {

            $data = array(
                'vName' => $_POST['msd_contact_name'],
                'vPhonenumber' => $_POST['msd_contact_number'],
            );
            $this->CI->db->where('iAdminId', $_POST['id']);
            $this->CI->db->update('mod_admin', $data);
        }
        $ret_arr['success'] = true;
        return $ret_arr;
    }

    function getLastStorecode($mode = '', $value = '', $data = array(), $id = '', $field_name = '', $field_id = '') {
        if ($mode == 'Add') {
            $this->CI->db->select('iMstStoreDetailId,vStoreCode');
            $this->CI->db->from('mst_store_detail');
            $this->CI->db->order_by("iMstStoreDetailId", "desc");
            $this->CI->db->limit(1);
            $lastid = $this->CI->db->get()->result_array();

            $newid = $lastid[0]['iMstStoreDetailId'] + 1;
            return 'store' . $newid;
        } else {
            return $data['msd_store_code'];
        }
    }

    function getSalesStatistics($mode = '', $id = '', $parID = '') {
        $this->CI->db->select(count('*'));
        $this->CI->db->from('mod_admin');
        $total = $this->CI->db->get()->result_array();

        $this->CI->db->select(count('*'));
        $this->CI->db->from('mod_admin');
        $this->CI->db->where('eStatus', 'Active');
        $live = $this->CI->db->get()->result_array();

        $ret_arr['total'] = $total;
        $ret_arr['live'] = $live;

        $ret_arr['success'] = true;
        return $ret_arr;
    }

    function Breadcrumb($type, $resourceid, $pagename = '') {
        // Type = p (product), c(category),s(static),v(seller),d(dynamic pages)
        $domain = $this->CI->config->item('site_url');
        $seprator = '<span> > </span>';
        $RootLocation = '<li><a href="' . $domain . 'index.html">Home</a>' . $seprator . '</li>';
        $Start = '<div class="breadcrumbs"><ul>';
        $End = '</ul></div>';
        if ($type == 'P') {
            $this->CI->db->select('iMstCategoriesId');
            $this->CI->db->from('trn_product_categories');
            $this->CI->db->where('iMstProductsId', $resourceid);
            $ProductArray = $this->CI->db->get()->row_array();
            $ProductCategory = $ProductArray['iMstCategoriesId'];
            $CategoryArray = @$this->CategoryWalkup($ProductCategory);
            $CatString = '';
            foreach ($CategoryArray as $value) {
                $url = $domain . 'c' . DIRECTORY_SEPARATOR . $value['iMstCategoriesId'] . DIRECTORY_SEPARATOR . $value['vCleanUrl'];
                $CatString .= "<li><a href='" . $url . "'>" . $value['vTitle'] . "</a>" . $seprator . "</li>";
            }
            $myBreadcrumb = $RootLocation . $CatString . '<li><strong>' . $pagename . '</strong></li>';
        } elseif ($type == 'C') {
            // get all parent category name and url
            $CategoryArray = @$this->CategoryWalkup($resourceid);
            $CatString = '';
            foreach ($CategoryArray as $value) {
                $url = $domain . strtolower($type) . DIRECTORY_SEPARATOR . $value['iMstCategoriesId'] . DIRECTORY_SEPARATOR . $value['vCleanUrl'];
                $CatString .= "<li><a href='" . $url . "'>" . $value['vTitle'] . "</a>" . $seprator . "</li>";
            }
            $myBreadcrumb = $RootLocation . $CatString . '<li><strong>' . $pagename . '</strong></li>';
        } elseif ($type == 'S' || $type == 'V') {
            // get Static Page
            if (!empty($pagename)) {
                $myBreadcrumb = $RootLocation . '<li><strong>' . $pagename . '</strong></li>';
            } else {
                $myBreadcrumb = $RootLocation;
            }
        } elseif ($type == 'D') {
            $GetDynamicPageArrs = @$this->DynamicPageURL();
            $GetDynamicPageAr = $GetDynamicPageArrs["" . $resourceid . ""];
            $DynamicPageLink = "<li><a href='" . $GetDynamicPageAr[1] . "'>" . $GetDynamicPageAr[0] . "</a>" . $seprator . "</li>";
            if (empty($pagename)) {
                $myBreadcrumb = $RootLocation . $DynamicPageLink;
            } else {
                $myBreadcrumb = $RootLocation . $DynamicPageLink . '<li><strong>' . $pagename . '</strong></li>';
            }
        } elseif ($type == 'CLEAN') {
            $myBreadcrumb = $RootLocation . '<li><strong>' . $resourceid . '</strong></li>';
        } elseif ($type == 'STORE') {
            // Get Store link
            $vendorlink = '';
            $storeinfo = explode('|_A1B_|', $resourceid);
            if (!empty($storeinfo[0]) && is_numeric($storeinfo[0])) {
                $VendorURL = @$this->CleanUrl('v', $storeinfo[0]);
            } else {
                $VendorURL = $domain . 'store' . DIRECTORY_SEPARATOR . $storeinfo[0] . '.html';
            }
            if (!empty($storeinfo[1])) {
                $VendorName = $storeinfo[1];
            }
            if (!empty($VendorURL) && !empty($VendorName)) {
                $vendorlink = "<li><a href='" . $VendorURL . "'>" . $VendorName . "</a>" . $seprator . "</li>";
            }
            // pagename
            if (!empty($pagename)) {
                $pagename = '<li><strong>' . $pagename . '</strong></li>';
            }
            $myBreadcrumb = $RootLocation . $vendorlink . $pagename;
        } else {
            $myBreadcrumb = $RootLocation;
        }
        return $Start . $myBreadcrumb . $End;
    }

    function CleanUrl($type = '', $resourceid = 0) {
        $url = '';
        $domain = $this->CI->config->item('site_url');
        if (strtolower($type) == 'p') {
            $this->CI->db->select('vCleanUrl');
            $this->CI->db->from('trn_clean_url');
            $this->CI->db->where('eResourceType', 'P');
            $this->CI->db->where('iResourceId', $resourceid);
            $result = $this->CI->db->get()->result_array();
            if (empty($result[0]['vCleanUrl'])) {
                $result[0]['vCleanUrl'] = '#';
            }
            $url = $domain . strtolower($type) . DIRECTORY_SEPARATOR . $resourceid . DIRECTORY_SEPARATOR . $result[0]['vCleanUrl'];
        } elseif (strtolower($type) == 'c') {
            $this->CI->db->select('vCleanUrl');
            $this->CI->db->from('trn_clean_url');
            $this->CI->db->where('eResourceType', 'C');
            $this->CI->db->where('iResourceId', $resourceid);
            $result = $this->CI->db->get()->result_array();
            if (empty($result[0]['vCleanUrl'])) {
                $result[0]['vCleanUrl'] = '#';
            }
            $url = $domain . strtolower($type) . DIRECTORY_SEPARATOR . $resourceid . DIRECTORY_SEPARATOR . $result[0]['vCleanUrl'];
        } elseif (strtolower($type) == 's') {
            $this->CI->db->select('vUrl');
            $this->CI->db->from('mod_page_settings');
            $this->CI->db->where('iPageId', $resourceid);
            $this->CI->db->where('eStatus', 'Active');
            $result = $this->CI->db->get()->result_array();
            $url = $domain . $result[0]['vUrl'];
        } elseif (strtolower($type) == 'v') {
            $this->CI->db->select('vCleanUrl');
            $this->CI->db->from('trn_clean_url');
            $this->CI->db->where('eResourceType', 'V');
            $this->CI->db->where('iResourceId', $resourceid);
            $result = $this->CI->db->get()->result_array();
            if (empty($result[0]['vCleanUrl'])) {
                $result[0]['vCleanUrl'] = '#';
            }
            $url = $domain . 'store' . DIRECTORY_SEPARATOR . $resourceid . DIRECTORY_SEPARATOR . $result[0]['vCleanUrl'];
        } elseif (strtolower($type) == 'vp') {
            $url = $domain . 'store' . DIRECTORY_SEPARATOR . $resourceid . DIRECTORY_SEPARATOR . 'products.html';
        } elseif (strtolower($type) == 'd') {
            $GetDynamicPageArrs = @$this->DynamicPageURL();
            $GetDynamicPageAr = $GetDynamicPageArrs["" . $resourceid . ""];
            $url = $domain . $GetDynamicPageAr[1];
        } else {
            $url = '#';
        }
        return $url;
    }

    function DynamicPageURL() {
        $DynamicPages = array(
            'FAQ' => array(
                'Faq',
                'faq.html',
            ),
            'SITEMAP' => array(
                'Sitemap',
                'sitemap.html',
            ),
            'NEWS' => array(
                'News',
                'news.html',
            ),
            'MY_ACCOUNT' => array(
                'My Account',
                'my-account.html',
            ),
            'ORDER_HISTORY' => array(
                'My Order',
                'my-orders.html',
            ),
            'INVITE_FRIEND' => array(
                'Invite Friend',
                'invite-friend.html',
            ),
            'USER_PROFILE' => array(
                'Personal Information',
                'my-profile.html',
            ),
            'USER_ADDRESS' => array(
                'My Address',
                'my-address.html',
            ),
            'NOTIFICATION' => array(
                'Notification',
                'notification.html',
            ),
            'CAREERS' => array(
                'Careers',
                'career.html',
            ),
            'CONTACT_US' => array(
                'Contact Us',
                'contact.html',
            ),
            'WISH_LIST' => array(
                'Wishlist',
                'wishlist.html',
            ),
            'CONTACT_US_STORE' => array(
                'Contact Us',
                'store_contact.html',
            ),
            'STORE_REGISTRATION' => array(
                'Seller Registration',
                'seller-zone.html',
            ),
            'DEAL_ITEM' => array(
                'Deal Items',
                'deal-items.html',
            ),
            'SEARCH' => array(
                'Search',
                'search.html',
            ),
        );
        return $DynamicPages;
    }

    function OrderBtn($input_params = '') {
        $Stock = $input_params['query'][0]['mp_stock'];
        $ProductId = $input_params['query'][0]['mp_mst_products_id'];
        $Price = $input_params['query'][0]['mp_regular_price'];
        $Saleprice = $input_params['query'][0]['mp_sale_price'];
        $Status = $input_params['query'][0]['mp_status'];
        // $LowAvlLimitNotification = $input_params['query'][0]['mp_low_avl_limit_notification'];
        //$ProductId=0,$Price=0,$Saleprice=0,$Stock=0
        $OutOfStockBtn = '<div class="out_of_stock"><a>Out of Stock</a></div>';
        $OrderBtn = '';
        if ($Stock > 0/* && $Stock > $LowAvlLimitNotification */
        ) {
            if ($Status == "Inactive") {
                $OrderBtn = $OutOfStockBtn;
            } elseif ($Saleprice > 0 || $Price > 0) {
                $OrderBtn = '<a title="Add to cart" class="atc-btn-handler btn add-to-card atc-btn-handler align-left" id="flycart_add_to_cart_' . $ProductId . '" data-id="' . $ProductId . '" data-rel="' . $ProductId . '"><span>Add to Cart</span></a>';
            }
            /* elseif($Saleprice == 0 && $Price > 0){
              $OrderBtn = '<a title="Add to cart" class="atc-btn-handler btn add-to-card align-left" id="flycart_add_to_cart_0" data-rel="'.$ProductId.'"><span>Add to Cart</span></a>';
              }elseif($Saleprice > 0 && $Price == 0){
              $OrderBtn = '<a title="Add to cart" class="atc-btn-handler btn add-to-card align-left" id="flycart_add_to_cart_0" data-rel="'.$ProductId.'"><span>Add to Cart</span></a>';
              } */ else {
                $OrderBtn = $OutOfStockBtn;
            }
        } elseif ($Status == "Inactive") {
            $OrderBtn = $OutOfStockBtn;
        } else {
            $OrderBtn = $OutOfStockBtn;
        }
        $return_array[0]['OrderBtn'] = $OrderBtn;
        return $return_array;
    }

    function ProductPriceFormat($FormatType = 1, $Price = 0, $SalePrice = 0, $Diff = 0) {
        $return = '';
        $currency_symbol = $this->CI->config->item('CURRENCY_SYMBOL');
        $price_suffix = $this->CI->config->item('ADMIN_CURRENCY_SUFFIX');
        if ($FormatType == 1) {
            if ($SalePrice > 0 && $Price > 0) {
                if ($Price > $SalePrice) {
                    $span = '<span>' . $currency_symbol . ' ' . number_format($Price, 2) . $price_suffix . '</span>';
                }
                $return = '<div class="price">' . $currency_symbol . ' ' . number_format($SalePrice, 2) . $price_suffix . $span . '</div>';
            } elseif ($SalePrice = 0 && $Price > 0) {
                $return = '<div class="price">' . $currency_symbol . '' . number_format($Price, 2) . $price_suffix . '</div>';
            } else {
                $return = '<div class="price">' . $currency_symbol . '' . number_format($Price, 2) . $price_suffix . '</div>';
            }
        }
        if ($FormatType == 2) {
            if ($SalePrice > 0 && $Price > 0) {
                if ($Price > $SalePrice) {
                    $span2 = '<span class="price-through">' . $currency_symbol . '' . number_format($Price, 2) . $price_suffix . '</span> <span class="price-parsent">(' . $Diff . '%)</span>';
                }
                $return = '<div class="price-box"><span id="product-price-65" class="regular-price">' . $span2 . '</span> </div>';

                $return .= '<div class="price-box"> <span id="product-price-66" class="regular-price"> <span class="price">' . $currency_symbol . '' . number_format($SalePrice, 2) . $price_suffix . '</span> </span> </div>';
            } elseif ($SalePrice = 0 && $Price > 0) {

                $return .= '<div class="price-box"> <span id="product-price-66" class="regular-price"> <span class="price">' . $currency_symbol . '' . number_format($SalePrice, 2) . $price_suffix . '</span> </span> </div>';
            } else {
                $return = '<div class="price-box"> <span id="product-price-66" class="regular-price"> <span class="price">' . $currency_symbol . '' . number_format($Price, 2) . $price_suffix . '</span> </span> </div>';
            }
        }
        if ($FormatType == 3) {
            $Diff = $Price - $SalePrice;
            if ($SalePrice > 0 && $Price > 0) {
                if ($Price > $SalePrice) {
                    $emtag = '<em>' . $currency_symbol . '<span>' . number_format($Price, 2) . $price_suffix . '</span></em>';
                    $div = '<div class="availability"> ( you save ' . $currency_symbol . number_format($Diff, 2) . ' ) </div>';
                }
                $return = '<div class="price-availability-block clearfix"><div class="price"><strong><span>' . $currency_symbol . '</span>' . number_format($SalePrice, 2) . $price_suffix . '</strong>' . $emtag . '</div>' . $div . '</div>';
            } elseif ($SalePrice = 0 && $Price > 0) {
                $return = '<div class="price-availability-block clearfix"><div class="price"><strong><span>' . $currency_symbol . '</span>' . number_format($Price, 2) . $price_suffix . '</strong> </div></div>';
            }
        }
        return $return;
    }

    function ProductBlockGenerator($pid, $name, $price = 0, $saleprice = 0, $rating, $avg = 0, $stock = 0, $diff = 0, $date, $wishlist = 0, $img, $class = 'atc-btn-handler') {
        // To get clean URL for product
        $ProductUrl = $this->cleanUrl("P", $pid);

        // Get Low Available Limit Notification
        // $iLowAvlLimitNotifcation = $this->CI->generalfront->getColumnValue("iMstProductsId", $pid, "iLowAvlLimitNotifcation", "mst_products");
        // Get Product status
        $eStatus = $this->CI->generalfront->getColumnValue("iMstProductsId", $pid, "eStatus", "mst_products");

        // To check is product in wishlist
        $WishlistArr = $this->CI->generalfront->checkIsPrductInWishlist();
        if (in_array($pid, $WishlistArr)) {
            $cls = 'in-wishlist';
        }
        if ($wishlist == '') {
            $wishlist = 0;
        }

        // To get price in a format
        $formatedPrice = $this->ProductPriceFormat(1, $price, $saleprice, $diff);

        // To check is prduct added recently within 48 hrs
        $ad_dt = strtotime($date);
        $current_dt = strtotime(date('Y-m-d H:i:s'));
        $diffTime = intval($current_dt - $ad_dt);
        $hrs = round(($diffTime / 60) / 60);
        $str = '';
        if ($hrs < 48) {
            $diffrance = '<div class="new">NEW</div>';
        } else {
            if ($price > 0 && $saleprice > 0) {
                if ($diff > 0) {
                    $diffrance = '<div class="off">' . $diff . '%<br>off</div>';
                } else {
                    $diffrance = '';
                }
            }
        }

        // set path for the images of products
        if (strpos($img, 'http://') !== false || strpos($img, 'https://') !== false) {
            $getimage = $img;
        } else {
            if (empty($img)) {
                $getimage = $this->CI->config->item("images_url") . "no-image-cat.jpg";
            } else {
                $getimagepath = $this->CI->config->item("product_img_path") . $img;
                if ($this->custom_file_exist($getimagepath)) {
                    $getimage = $this->CI->config->item("product_img") . $img;
                    $params['height'] = 150;
                    $params['width'] = 150;
                    $enc_image_url = base64_encode($getimage);
                    $getimage = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=' . $params['height'] . '&width=' . $params['width'] . "&color=" . $color;
                } else {
                    $getimage = $this->CI->config->item("images_url") . "no-image-cat.jpg";
                }
            }
        }

        // To set name of the product
        if (strlen($name) > 40) {
            $name = substr($name, 0, 40) . '...';
        }

        $str .= '<div class="dealof-box">' . $diffrance . '<a href="' . $ProductUrl . '" data-rel="' . $pid . '"><div class="img"><img class="img-responsive" height="150" alt="deal" src="' . $getimage . '"></div></a><p><a href="' . $ProductUrl . '" data-rel="' . $pid . '">' . $name . '</a></p>' . $formatedPrice . '<ul class="star">';

        // To set rating star for the products
        for ($i = 0; $i < 5; $i++) {
            if ($i < $rating) {
                $str .= '<li class="sprite star-fill"></li>';
            } else {
                $str .= '<li class="sprite star-strock"></li>';
            }
        }

        $str .= "</ul><div class='addto-cart'>";
        if ($stock > 0/* && $stock > $iLowAvlLimitNotifcation */ && $price > 0 && $saleprice > 0 && $eStatus == "Active") {
            $str .= "<button class='add-to-card " . $class . "' data-id=" . $pid . ">Add to Cart</button>";
        } else {
            $str .= "<button class='out_of_stock'>Out of Stock</button>";
        }
        $str .= " <button class='add-to-wish add_wish_list " . $cls . " cls" . $pid . "' data-id=" . $pid . " onclick=addtowishlist(" . $pid . ")>(" . $wishlist . ")</button>";
        $str .= "</div></div>";
        return $str;
    }

    function getRandomProducts($cat_id = 0) {
        $this->CI->db->select('tpc.iMstProductsId,mp.*');
        $this->CI->db->from('trn_product_categories as tpc');
        $where = "iMstCategoriesId IN (" . $cat_id . ") AND mp.eStatus = 'Active'";
        $this->CI->db->where($where);
        $this->CI->db->join('mst_products as mp', 'mp.iMstProductsId = tpc.iMstProductsId');
        $this->CI->db->order_by('iMstCategoriesId', 'RANDOM');
        $this->CI->db->limit(10);
        $random_product = $this->CI->db->get()->result_array();
        foreach ($random_product as $key => $value) {
            if (isset($value['vDefaultImg']) && !empty($value['vDefaultImg'])) {
                $enc_image_url = base64_encode($this->CI->config->item('product_img') . $value['vDefaultImg']);
                $final_image_url = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=150&width=150&color=';
            } else {
                $enc_image_url = base64_encode($this->CI->config->item('images_url') . 'no-image-cat.jpg');
                $final_image_url = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=150&width=150&color=';
            }
            $random_product[$key]['vDefaultImg'] = $final_image_url;
        }
        return $random_product;
    }

    function GetProductBySort($input_params = '') {
        $cat_id = $input_params['cid'];
        $sort_id = $input_params['iSearchId'];
        $price_id = $input_params['iPriceId'];
        $start_price = $input_params['start_price'];
        $end_price = $input_params['end_price'];
        $this->CI->db->start_cache();
        $this->CI->db->select('tpc.iMstProductsId,mp.*');
        $this->CI->db->from('trn_product_categories as tpc');
        $where = "iMstCategoriesId IN (" . $cat_id . ") AND mp.eStatus='Active'";
        if (isset($input_params['filter']) && !empty($input_params['filter'])) {
            $where .= " AND mp.iMstProductsId IN (" . $input_params['pids'] . ")";
        }
        // if (!empty($start_price)) {
        $where .= " AND fSalePrice >= " . $start_price;
        // }
        // if (!empty($end_price)) {
        $where .= " AND fSalePrice <= " . $end_price;
        // }
        $this->CI->db->where($where);
        $this->CI->db->join('mst_products as mp', 'mp.iMstProductsId = tpc.iMstProductsId');
        if ($sort_id > 0 && $price_id > 0) {
            if ($sort_id == 1) {
                if ($price_id == 1) {
                    $this->CI->db->order_by('iSaleState', 'DESC');
                    // $this->CI->db->order_by('fRegularPrice', 'DESC'));
                    $this->CI->db->order_by('fSalePrice', 'DESC');
                } else {
                    $this->CI->db->order_by('iSaleState', 'DESC');
                    // $this->CI->db->order_by('fRegularPrice', 'ASC');
                    $this->CI->db->order_by('fSalePrice', 'ASC');
                }
            } elseif ($sort_id == 2) {
                if ($price_id == 1) {
                    $this->CI->db->order_by('tpc.iMstProductsId', 'DESC');
                    // $this->CI->db->order_by('fRegularPrice','DESC');
                    $this->CI->db->order_by('fSalePrice', 'DESC');
                } else {
                    $this->CI->db->order_by('tpc.iMstProductsId', 'DESC');
                    // $this->CI->db->order_by('fRegularPrice', 'ASC');
                    $this->CI->db->order_by('fSalePrice', 'ASC');
                }
            } elseif ($sort_id == 3) {
                if ($price_id == 1) {
                    $this->CI->db->order_by('iDifferencePer', 'DESC');
                    // $this->CI->db->order_by('fRegularPrice','DESC');
                    $this->CI->db->order_by('fSalePrice', 'DESC');
                } else {
                    $this->CI->db->order_by('iDifferencePer', 'DESC');
                    // $this->CI->db->order_by('fRegularPrice', 'ASC');
                    $this->CI->db->order_by('fSalePrice', 'ASC');
                }
            } else {
                if ($price_id == 1) {
                    $this->CI->db->order_by('iViewState', 'DESC');
                    // $this->CI->db->order_by('fRegularPrice','DESC');
                    $this->CI->db->order_by('fSalePrice', 'DESC');
                } else {
                    $this->CI->db->order_by('iViewState', 'DESC');
                    // $this->CI->db->order_by('fRegularPrice', 'ASC');
                    $this->CI->db->order_by('fSalePrice', 'ASC');
                }
            }
        } elseif ($sort_id > 0 && $price_id == 0) {
            if ($sort_id == 1) {
                $this->CI->db->order_by('iSaleState', 'DESC');
            } elseif ($sort_id == 2) {
                $this->CI->db->order_by('mp.iMstProductsId', 'DESC');
            } elseif ($sort_id == 3) {
                $this->CI->db->order_by('iDifferencePer', 'DESC');
            } else {
                
            }
        } else {
            if ($price_id == 1) {
                // $this->CI->db->order_by('fRegularPrice', 'DESC');
                $this->CI->db->order_by('fSalePrice', 'DESC');
            } else {
                // $this->CI->db->order_by('fRegularPrice', 'ASC');
                $this->CI->db->order_by('fSalePrice', 'ASC');
            }
        }
        //$this->CI->db->order_by('iMstCategoriesId', 'RANDOM');
        $this->CI->db->stop_cache();
        $total_records = $this->CI->db->count_all_results();

        $settings_params['count'] = $total_records;

        $record_limit = $this->CI->config->item("REC_LIMIT_FRONT");
        $record_limit = intval($record_limit);
        $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;

        $total_pages = ceil($total_records / $record_limit);
        $start_index = ($current_page - 1) * $record_limit;

        $settings_params['curr_page'] = $current_page;
        $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
        $settings_params['next_page'] = ($current_page + 1 > $total_pages) ? 0 : 1;
        $this->CI->db->limit($record_limit, $start_index);
        $product_data = $this->CI->db->get()->result_array();
        $this->CI->db->flush_cache();
        // echo $this->CI->db->last_query();
        $return_array[0]['settings'] = $settings_params;
        $return_array[0]['product_data'] = $product_data;
        return $return_array;
    }

    function CategoryWalkup($categoryId = 0) {
        $query = 'SELECT T2.iMstCategoriesId, T2.vTitle, T3.vCleanUrl FROM ( SELECT @r AS _id, (SELECT @r := iParentId FROM mst_categories WHERE iMstCategoriesId= _id) AS parent_id, @l := @l + 1 AS lvl FROM (SELECT @r := ' . $categoryId . ', @l := 0) vars, mst_categories h WHERE @r <> 0) T1 JOIN mst_categories T2 ON T1._id = T2.iMstCategoriesId JOIN trn_clean_url T3 ON T1._id=T3.iResourceId AND eResourceType="C" ORDER BY T1.lvl DESC';
        $retAr = $this->CI->db->query($query)->result_array();
        return $retAr;
    }

    function WSCleanUrl($value = '', $dataArr = array()) {
        $clearnUrl = '';
        if (isset($dataArr['type'])) {
            if (strtolower($dataArr['type']) == 'p') {
                $clearnUrl = $this->cleanurl($dataArr['type'], $dataArr['mp_mst_products_id']);
            } else {
                $clearnUrl = $this->cleanurl($dataArr['type'], $dataArr['thc_mst_categories_id']);
            }
        } elseif (isset($dataArr['type_1'])) {
            $clearnUrl = $this->cleanurl($dataArr['type_1'], $dataArr['mc_mst_categories_id']);
        }
        return $clearnUrl;
    }

    function SellerBlock($StoreId = 0) {
        $return = '';
        if ($StoreId > 0) {
            // Get Seller Information
            $this->CI->db->select('vStoreName,fStoreRateAvg');
            $this->CI->db->from('mst_store_detail');
            $this->CI->db->where('iMstStoreDetailId', $StoreId);
            $result = $this->CI->db->get()->result_array();
            $storename = $result[0]['vStoreName'];
            $storerating = $result[0]['fStoreRateAvg'];
            if (!isset($storerating) || empty($storerating)) {
                $storerating = '0';
            }
            $score = $storerating . '/5';
            //$contact_seller_link = $this->CleanUrl('d','CONTACT_US_STORE');
            $contact_seller_link = $this->CI->config->item('site_url') . $StoreId . '/store_contact.html';
            $registration_link = $this->CleanUrl('d', 'STORE_REGISTRATION');
            //$store_url = $this->CleanUrl('v', $StoreId);
            $store_url = $this->CI->config->item("site_url") . 'store/' . $storename .'.html';
            $store_products = $this->CleanUrl('vp', $StoreId);
            $store_contact = '<a href="' . $contact_seller_link . '">Contact Seller</a><br>';
            //$store_page = '<a href="' . $store_products . '">Other Items sold by Seller</a><br>';
            $become_seller = '<a href="' . $registration_link . '">Become a Seller</a>';

            //$return = '<div class="steve-madden"><h2>' . $storename . '</h2><a href="' . $store_url . '" class="view-store-btn">View Store</a><div class="seller-text"><strong>SELLER SCORE :</strong><span class="text">' . $score . '</span></div><div class="dis"><p>' . $store_contact . ' ' . $store_page . ' ' . $become_seller . '</p></div></div>';
            $return = '<div class="steve-madden"><h2>' . $storename . '</h2><a href="' . $store_url . '" class="view-store-btn">View Store</a><div class="seller-text"><strong>SELLER SCORE :</strong><span class="text">' . $score . '</span></div><div class="dis"><p>' . $store_contact . ' ' . $store_page . ' ' . $become_seller . '</p></div></div>';
        }
        return $return;
    }

    function CleanUrlName($title = '', $resourceType = '') {
        if (!empty($title) && !empty($resourceType)) {
            // clearn url
            $title = strtolower($title);
            $title = preg_replace("![^a-z0-9]+!i", "-", $title);
            // compare in db
            $this->CI->db->select('COUNT(*) as total');
            $this->CI->db->from('trn_clean_url');
            $this->CI->db->where('eResourceType', $resourceType);
            $this->CI->db->where('vCleanUrl', $title);
            $result = $this->CI->db->get()->result_array();
            $counter = $result[0]['total'];
            if ($counter == 0) {
                $title = $title . '.html';
            } else {
                $title = $title + '-' . ($counter + 1) . '.html';
            }
            return $title;
        }
    }

    function insertDataToCleanUrl($mode = '', $id = '', $parID = '') {
        if (!empty($id)) {
            if (isset($_POST['mc_title'])) {
                $title = $_POST['mc_title'];
                $resourcetype = 'c';
            } else
            if (isset($_POST['mp_title']) && isset($_POST['sys_custom_field_1'])) {
                $title = $_POST['mp_title'];
                $resourcetype = 'p';
            } else
            if (isset($_POST['msd_status'])) {
                //if($_POST['mode'] == 'Add'){
                $title = $_POST['msd_store_name'];
                //$id = substr($_POST['msd_store_code'], -2);
                $id = substr($_POST['msd_store_code'], strpos($_POST['msd_store_code'], "e") + 1);
                $resourcetype = 'v';
                /* }else if($_POST['mode'] == 'Update'){
                  $title = $_POST['msd_store_name'];
                  $resourcetype = 'v';
                  // get storeid on update
                  $this->CI->db->select('iMstStoreDetailId');
                  $this->CI->db->from('mst_store_detail');
                  $this->CI->db->where('iAdminId',$id);
                  $returnid = $this->CI->db->get()->row_array();
                  $id = $returnid['iMstStoreDetailId'];

                  } */
            } else {
                $ret_arr['success'] = true;
            }

            $url = $this->CleanUrlName($title, $resourcetype);
            if (strlen($url) > 0) {
                $data = array(
                    'vCleanUrl' => $url,
                    'eResourceType' => $resourcetype,
                    'iResourceId' => $id,
                    'tTime' => date('Y-m-d H:i:s')
                );
                if ($_POST['mode'] == 'Update' && $_POST['tab_code'] == 'sellerregistration') {

                    $this->CI->db->where('iResourceId', $id);
                    $this->CI->db->where('eResourceType', 'V');
                    $this->CI->db->update('trn_clean_url', $data);
                    $ret_arr['success'] = true;
                } else {
                    $this->CI->db->insert('trn_clean_url', $data);
                    $insertId = $this->CI->db->insert_id();
                    if ($insertId > 0) {
                        $ret_arr['success'] = true;
                    } else {
                        $ret_arr['success'] = false;
                        $ret_arr['message'] = $message;
                    }
                }
            }
        } else {
            $ret_arr['success'] = false;
            $ret_arr['message'] = $message;
        }
        return $ret_arr;
    }

    function ProductDefaultImg($mode = '', $id = '', $parID = '') {
        // Product Clean URL
        if ($mode == 'Add') {
            $this->insertDataToCleanUrl('', $id, '');
        }
        // Product Default Image
        $GetDefaultAr = $_REQUEST['child']['productimages']['mpi_default'];
        if (count($GetDefaultAr) > 0) {
            $GetDefaultImgAr = $_REQUEST['child']['productimages']['mpi_img_path'];
            $flag = false;
            foreach ($GetDefaultAr as $key => $value) {
                if ($value == 'Yes') {
                    $flag = true;
                    $DefaultImg = $GetDefaultImgAr[$key];
                }
            }
            if ($flag == false) {
                // default as first image
                $DefaultImg = $GetDefaultImgAr[0];
            }
            // update default image in product table
            $data = array(
                'vDefaultImg' => $DefaultImg,
            );
            $this->CI->db->where('iMstProductsId', $id);
            $this->CI->db->update('mst_products', $data);
        }
        $ret_arr['success'] = true;
        return $ret_arr;
    }

    function delete_store_details($input_params = array()) {
        $id = $input_params['id'];
        $ret_arr = array();
        if ((is_array($id) && count($id) > 0) || (strlen($id) > 1 && $id != '')) {
            if (is_array($id) && count($id) > 0) {
                $ids = implode(",", $id);
                $ids = trim($ids, ",");
                $ids = preg_replace('/,,+/', ',', $ids);
            } else {
                $ids = trim($id, ",");
            }
            $idArr = explode(',', $ids);
            // pr($idArr);exit;
            $field = "COUNT(*) as total_products";
            $this->CI->db->select($field, false);
            $this->CI->db->from("mst_products");
            $this->CI->db->where_in('iAdminId', $idArr);
            $productArr = $this->CI->db->get()->result_array();
            //if (count($productArr[0]['total_products']) < 1 && is_array($productArr)) {
                $update_arr = array();
                $update_arr['eStatus'] = 'Inactive';

                $this->CI->db->where_in('iAdminId', $idArr);
                $res = $this->CI->db->update("mod_admin", $update_arr);
                //echo $this->CI->db->last_query();
                $this->CI->db->where_in('iAdminId', $idArr);
                $res1 = $this->CI->db->update("mst_store_detail", $update_arr);
                // echo $this->CI->db->last_query();exit;
                if ($res && $res1) {
                    $message = "Seller's deleted successfully.";
                    $status = 1;
                } else {
                    $status = 0;
                    $message = "Failure occured in deleteing the seller.";
                }
            /*} else {
                $status = 0;
                $message = "Seller's are associated with 1 or more products we cannot delete them.";
            }*/
        } else {
            $status = 0;
            $message = "Failure occured in deleting the seller's.";
        }
        $ret_arr['success'] = $status;
        $ret_arr['message'] = $message;
        return $ret_arr;
    }

    function ProductPriceDiffrence($mode = '', $id = '', $parID = '') {
        //**************************************************
        // Find Price Diff and store in to database
        //**************************************************
        $RegularPrice = $_POST['mp_regular_price'];
        $SalePrice = $_POST['mp_sale_price'];
        $diff = 0;
        if ($RegularPrice > 0 && $SalePrice > 0) {
            $diff = ROUND(((($RegularPrice - $SalePrice) / ($RegularPrice)) * 100));
        }
        $_POST['mp_difference_per'] = $diff;
        //**************************************************
        // System Search Keyword Generator
        //**************************************************
        $CategoryArray = $this->CategoryWalkup($_POST['tpc_mst_categories_id']);
        $CategoryNames = '';
        $tmp = 1;
        foreach ($CategoryArray as $value) {
            $CategoryNames .= $value['vTitle'];
            if (count($CategoryArray) != $tmp) {
                $CategoryNames .= ', ';
            }
            $tmp++;
        }
        if (!empty($_POST['mp_search_tag'])) {
            $otherkeyword = $_POST['mp_search_tag'] . ', ';
        }
        $keywords = $_POST['mp_title'] . ', ' . $otherkeyword . $CategoryNames;
        $_POST['mp_search_keyword'] = $keywords;
        $ret_arr['success'] = true;
        return $ret_arr;
    }

    function insertTocategoryOptionValue($mode = '', $id = '', $parID = '') {
        if (!empty($id)) {
            $OptionIdAr = $_POST['child']['categoryoptionvalue']['mcov_category_option_id'];
            foreach ($OptionIdAr as $key => $OptionsItem) {
                $_POST['child']['categoryoptionvalue']['mcov_category_option_id'][$key] = $id;
            }
        }
        $ret_arr['success'] = true;
        return $ret_arr;
    }

    function getStoreAveragelRating($store_avg_arr = '') {
        $rating = array(
            'rating5' => 0,
            'rating4' => 0,
            'rating3' => 0,
            'rating2' => 0,
            'rating1' => 0,
        );
        for ($i = 0; $i < count($store_avg_arr['query']); $i++) {
            $rating['rating5'] += $store_avg_arr['query'][$i]['rating5'][0]['msd_trn_product_rating_id_1'];
            $rating['rating4'] += $store_avg_arr['query'][$i]['rating4'][0]['msd_trn_product_rating_id'];
            $rating['rating3'] += $store_avg_arr['query'][$i]['rating3'][0]['msd_trn_product_rating_id_2'];
            $rating['rating2'] += $store_avg_arr['query'][$i]['rating2'][0]['msd_trn_product_rating_id_3'];
            $rating['rating1'] += $store_avg_arr['query'][$i]['rating1'][0]['msd_trn_product_rating_id_4'];
        }

        $rating['total_rating'] = $rating['rating1'] + $rating['rating2'] + $rating['rating3'] + $rating['rating4'] + $rating['rating5'];
        if ($rating['total_rating'] == 0) {
            $rating['one_avg'] = $rating['two_avg'] = $rating['three_avg'] = $rating['four_avg'] = $rating['five_avg'] = 0;
            $rating['tot_positive_avg'] = 0;
        } else {
            $rating['one_avg'] = round(($rating['rating1'] * 100) / $rating['total_rating']);
            $rating['two_avg'] = round(($rating['rating2'] * 100) / $rating['total_rating']);
            $rating['three_avg'] = round(($rating['rating3'] * 100) / $rating['total_rating']);
            $rating['four_avg'] = round(($rating['rating4'] * 100) / $rating['total_rating']);
            $rating['five_avg'] = round(($rating['rating5'] * 100) / $rating['total_rating']);
            $rating['tot_positive_avg'] = round((100 * ($rating['rating5'] + $rating['rating4'])) / ($rating['rating1'] + $rating['rating2'] + $rating['rating3'] + $rating['rating4'] + $rating['rating5']));
        }

        $return_array[0]['rating'] = $rating;
        return $return_array;
    }

    function decode_email_data($input_params = array()) {
        $return_arr = array();
        $emailArr = json_decode($input_params['json_str'], true);
        return $emailArr;
    }

    function getCateogoryDisplayOrder($mode = '', $value = '', $data = array(), $id = '', $field_name = '', $field_id = '') {
        if ($mode == 'Add') {
            $this->CI->db->select("MAX(iDisplayOrder)+1 as max_order", false);
            $this->CI->db->from("mst_categories");
            $max_category_details = $this->CI->db->get()->result_array();
            return $max_category_details[0]['max_order'];
        } else {
            return $value;
        }
    }

    function getCatDropdown($mode = '', $value = '', $data = array(), $id = '', $field_name = '', $field_id = '') {
        $html = '';
        $this->CI->db->select("iMstCategoriesId AS id");
        $this->CI->db->select("vTitle AS val", false);
        $this->CI->db->select("iParentId AS parId", false);
        $this->CI->db->where("eStatus", 'Active');
        $db_data = $this->CI->db->select_assoc("mst_categories", "parId");
        //echo $this->CI->db->last_query();exit;
        $opt_str = $this->getRecursiveTreeDropdownForJerry($db_data, 0, '', '', $value);
        #echo $opt_str;exit;
        $html .= '<select name="' . $field_name . '" id="' . $field_id . '" class="chosen-select frm-size-medium" data-placeholder="Please Select Category">';
        $html .= '<option value=""></option>';
        $html .= $opt_str;
        $html .= '</select>';
        return $html;

        /*
          return '<select name="tpc_mst_categories_id" id="tpc_mst_categories_id" class="chosen-select frm-size-medium" style="display: none;">
          <optgroup label=" Beauty and Personal Care">
          <optgroup label=" &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Makeup">
          <optgroup label=" &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Face">
          <option value="4"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Foundations</option>
          </optgroup>
          </optgroup>
          <optgroup label=" &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Lips">
          <option value="6"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Lipistic</option>
          </optgroup>
          </optgroup>
          <option value="7"> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Clothing</option>
          <optgroup label=" test">
          <option value="9"> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; test1</option>
          </optgroup>
          </select>';
         */
    }

    function getRecursiveTreeDropdownForJerry($data_arr = array(), $id = '', $loop = 1, $old_str = "", $selected = '') {
        $db_data = $data_arr[$id];
        $n = (is_array($db_data)) ? count($db_data) : 0;
        $temp_str = '';
        if ($n > 0) {
            for ($i = 0; $i < $n; $i++) {
                $val_value = $db_data[$i]['val'];
                $val_id = $db_data[$i]["id"];
                if ($loop == 1) {
                    $val_value = " " . $old_str . "" . $val_value;
                } else {
                    $val_value = " " . $old_str . "&nbsp;&nbsp; " . $val_value;
                }
                if (is_array($data_arr[$val_id]) && count($data_arr[$val_id]) > 0) {
                    //$temp_str .= "<optgroup label='".$val_value."'>";
                    $temp_str .= '<option value="' . $val_id . '" style="color:#ff722b!important;" disabled=true>' . $val_value . '</option>';
                    $send_str = $this->getRecursiveTreeDropdownForJerry($data_arr, $val_id, $loop + 1, $old_str . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $selected);
                    $temp_str .= $send_str;
                    //	$temp_str .="</optgroup>";
                } else {
                    if ($selected == $val_id) {
                        $temp_str .= '<option value="' . $val_id . '" selected>' . $val_value . '</option>';
                    } else {
                        $temp_str .= '<option value="' . $val_id . '">' . $val_value . '</option>';
                    }
                }
            }
        }
        return $temp_str;
    }

    protected function getCategoryTree($cat_id = '', $prefix = '') {
        
        $query = "SELECT iMstCategoriesId FROM mst_categories WHERE iParentId = $cat_id";
        $rows = $this->CI->db->query($query)->result();

        $category = '';
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $category .= $prefix . $row->iMstCategoriesId . ",";
                // Append subcategories
                $category .= $this->getCategoryTree($row->iMstCategoriesId, $prefix . '-');
            }
        }
        return $category;
    }


    function sitemapCategoryWalkup($input_params = '') {
        $Categoryid = $input_params['mc_mst_categories_id'];
        $domain = $this->CI->config->item('site_url');
        //$url = $domain . 'c' . DIRECTORY_SEPARATOR;
        //$seprator = DIRECTORY_SEPARATOR;
        $url = $domain . 'c' . '/';
        $seprator = '/';
        $retAr =  rtrim(str_replace(',-', ',', $this->getCategoryTree($Categoryid)), ',');
        if ($retAr > 0) {
            $QueryLinks = 'SELECT iMstCategoriesId,vTitle,iParentId,CONCAT("' . $url . '",iMstCategoriesId,"' . $seprator . '",vCleanUrl) AS vCleanUrl FROM mst_categories LEFT JOIN trn_clean_url  ON iMstCategoriesId=iResourceId  WHERE iMstCategoriesId IN(' . $retAr . ')  AND  eResourceType="C"';
            $retAr = $this->CI->db->query($QueryLinks)->result_array();
        }
        return $retAr;
    }
    /*
    function sitemapCategoryWalkup($input_params = '') {
        $Categoryid = $input_params['mc_mst_categories_id'];
        $domain = $this->CI->config->item('site_url');
        //$url = $domain . 'c' . DIRECTORY_SEPARATOR;
        //$seprator = DIRECTORY_SEPARATOR;
        $url = $domain . 'c' . '/';
        $seprator = '/';
        $query = "SELECT GROUP_CONCAT(lv SEPARATOR ',') as ids FROM (SELECT @pv:=(SELECT GROUP_CONCAT(iMstCategoriesId SEPARATOR ',') FROM mst_categories WHERE iParentId IN (@pv)) AS lv FROM mst_categories JOIN (SELECT @pv:=" . $Categoryid . ")tmp WHERE iParentId IN (@pv)) a";
        $retAr = $this->CI->db->query($query)->result_array();
        if ($retAr[0]['ids'] > 0) {
            $QueryLinks = 'SELECT iMstCategoriesId,vTitle,iParentId,CONCAT("' . $url . '",iMstCategoriesId,"' . $seprator . '",vCleanUrl) AS vCleanUrl FROM mst_categories LEFT JOIN trn_clean_url  ON iMstCategoriesId=iResourceId  WHERE iMstCategoriesId IN(' . $retAr[0]['ids'] . ')  AND  eResourceType="C"';
            $retAr = $this->CI->db->query($QueryLinks)->result_array();
        }
        return $retAr;
    }*/

    function getBuyerShippingAddress($value = '', $id = '', $data = array()) {
        ////Line 1  - Street name , Area
        // Line 2   City , State , zipcode
        // Line 3 , COuntry
        $final_address_arr = '';
        $buyer_area = $data['mo_buyer_area'];
        $buyer_address2 = $data['mo_buyer_address2'];
        $buyer_address1 = $data['mo_buyer_address1'];
        $country = $data['mc1_country'];
        $state = $data['ms_state'];
        $city = $data['mc2_city'];
        $pin_code = $data['mo_buyer_pin_code'];
        if ($buyer_address1 != '' && isset($buyer_address1)) {
            $line1 = $buyer_address1;
        }
        if ($buyer_area != '' && isset($buyer_area)) {
            if (strlen($line1) > 0) {
                $line1 .= ',';
            }
            $line1 .= $buyer_area . "";
        }
        if (strlen($line1) > 0) {
            $line1 .= "<br/>";
        }
        if ($city != '' && isset($city)) {
            $line2 = $city;
        }
        if ($state != '' && isset($state)) {
            if (strlen($line2) > 0) {
                $line2 .= ',';
            }
            $line2 .= $state;
        }
        if ($pin_code != '' && isset($pin_code)) {
            if (strlen($line2) > 0) {
                $line2 .= ',';
            }
            $line2 .= $pin_code . "";
        }
        if (strlen($line2) > 0) {
            $line2 .= "<br/>";
        }
        if ($country != '' && isset($country)) {
            $line3 = $country;
        }
        $final_address_arr = $line1 . $line2 . $line3;
        return $final_address_arr;

        /* $buyer_area = $data['mo_buyer_area'];
          $buyer_address2 = $data['mo_buyer_address2'];
          $buyer_address1 = $data['mo_buyer_address1'];
          $country = $data['mc1_country'];
          $state = $data['ms_state'];
          $city = $data['mc2_city'];
          $pin_code = $data['mo_buyer_pin_code'];
          $final_address_arr = array();
          if ($mo_buyer_address1 != '' && isset($mo_buyer_address1)) {
          array_push($final_address_arr, $mo_buyer_address1);
          }
          if ($buyer_address2 != '' && isset($buyer_address2)) {
          array_push($final_address_arr, $buyer_address2);
          }
          if ($buyer_area != '' && isset($buyer_area)) {
          array_push($final_address_arr, $buyer_area);
          }
          if ($city != '' && isset($city)) {
          array_push($final_address_arr, $city);
          }
          if ($pin_code != '' && intval($pin_code) > 0) {
          array_push($final_address_arr, $pin_code);
          }

          if ($state != '' && isset($state)) {
          array_push($final_address_arr, $state);
          }
          if ($country != '' && isset($country)) {
          array_push($final_address_arr, $country);
          }
          $shipping_address = implode(", ", $final_address_arr);
          return $shipping_address; */
    }

    function checkOrderStatus($input_params = array()) {
        $statusArr = array();
        $statusArr['Pending'] = 0;
        $statusArr['InProcess'] = 0;
        $statusArr['Shipped'] = 0;
        $statusArr['Delivered'] = 0;
        $statusArr['Cancel'] = 0;
        $statusArr['Close'] = 0;
        $Discount = 0;
        foreach ($input_params['main_order'] as $ord) {
            if ($ord['mso_item_status'] == 'Pending') {
                $statusArr['Pending'] += 1;
            } elseif ($ord['mso_item_status'] == 'In-Process') {
                $statusArr['InProcess'] += 1;
            } elseif ($ord['mso_item_status'] == 'Shipped') {
                $statusArr['Shipped'] += 1;
            } elseif ($ord['mso_item_status'] == 'Delivered') {
                $statusArr['Delivered'] += 1;
            } elseif ($ord['mso_item_status'] == 'Cancel') {
                $statusArr['Cancel'] += 1;
            } elseif ($ord['mso_item_status'] == 'Close') {
                $statusArr['Close'] += 1;
            }
            $Discount += ($ord['Discount_price'] * $ord['mso_product_qty']);
        }
        if ($statusArr['InProcess'] > 0) {
            $minstatus = 'In-Process';
        } elseif ($statusArr['Delivered'] > 0) {
            $minstatus = 'Delivered';
        } elseif ($statusArr['Cancel'] > 0) {
            $minstatus = 'Cancel';
        } elseif ($statusArr['Pending'] > 0) {
            $minstatus = 'Pending';
        } elseif ($statusArr['Shipped'] > 0) {
            $minstatus = 'Shipped';
        } elseif ($statusArr['Close'] > 0) {
            $minstatus = 'Close';
        }

        $retunr_arr['minstatus'] = $minstatus;
        $retunr_arr['Discount'] = $Discount;
        return $retunr_arr;
    }

    function validateDate($date = '', $format = 'Y-m-d H:i:s') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    function getFormatedDateTime($input = '') {
        $input_date = $input;
        if ($this->validateDate($input_date, 'Y-m-d H:i:s')) {
            $input_date = date('M j,Y  g:i A', strtotime($input_date));
        } else {
            $input_date = '';
        }
        return $input_date;
    }

    function getReasonComment($reason_id = 0, $reason_status = '') {
        if (intval($reason_id) > 0) {
            $this->CI->db->select("tActionDetail");
            $this->CI->db->from("trn_rma_history");
            $this->CI->db->where('iTrnRmaId', $reason_id);
            $this->CI->db->where('eActionType', $reason_status);
            $rma_history_arr = $this->CI->db->get()->result_array();
            return ucfirst($rma_history_arr[0]['tActionDetail']);
        }
    }

    function subOrder($input_param = array()) {
        $suborder = json_decode($input_param['order_item'], true);
        return $suborder;
    }

    function return_cancel_btn_format($input_params = array()) {
        $returnflag = 0;
        $cancelFlag = 0;
        foreach ($input_params['main_order'] as $subord) {
            $date = date('Y-m-d', strtotime('+' . $subord['mso_return_period'] . ' Days', strtotime($subord['mso_delivered_date'])));
            if (($subord['mso_item_status'] == 'Delivered' || $subord['mso_item_status'] == 'Close') && (date('Y-m-d') < $date)) {
                $returnflag = 1;
            }
            if ($subord['mso_item_status'] == 'Shipped') {
                $cancelFlag = 1;
            }
        }
        $retunr_arr['return_ord'] = $returnflag;
        $retunr_arr['cancel_ord'] = $cancelFlag;
        return $retunr_arr;
    }

    function getOrderTotal($order_details_id = '') {
        $this->CI->db->select("fTotalCost AS total_cost");
        $this->CI->db->from("mst_sub_order");
        $this->CI->db->where("iMstSubOrderId", $order_details_id);
        $db_data = $this->CI->db->get()->result_array();
        return $db_data[0]['total_cost'];
    }

    function getStructureDataFilter($input_params = array()) {
        $finalArr = array();
        foreach ($input_params['query'] as $data) {
            $finalArr[$data['mo_title']][] = $data;
        }

        foreach ($finalArr as $final) {
            foreach ($final as $fin) {
                $tmp[$fin['mo_title']][] = array(
                    'mst_categories_id' => $fin['mco_mst_categories_id'],
                    'order_id' => $fin['mco_order_id'],
                    'mst_options_id' => $fin['mco_mst_options_id'],
                    'value' => $fin['mcov_value'],
                    'title' => $fin['mo_title'],
                );
            }
        }
        return $tmp;
    }

    function searchFormat($input_params = array()) {
        pr($input_params);
    }

    function getSearchURL($input_params = array()) {
        pr($input_params);
    }

    function getOrderDetailsId($mode = '', $value = '', $data = array(), $id = '', $field_name = '', $field_id = '') {
        return $data['iMstSubOrderId'];
    }

    function getInvoiceHTML($value = '', $id = '', $data = array()) {
        $sub_order_id = $data['iMstSubOrderId'];
        if (intval($sub_order_id) > 0) {
            $admin_url = $this->CI->config->item("admin_url");
            $module_url = $admin_url . "#shippedorderlisting/shippedorderlisting/shipping_invoice/add|mode|View|id|" . $sub_order_id;
            $html = "<p style='text-align:center;'><span style='font-family:Trebuchet MS;font-size:13px;font-weight:normal;font-style:normal;text-decoration:underline;color:#006699;'><a class='fancybox-popup' href ='" . $module_url . "'>View Invoice</span></p>";
            return $html;
        }
    }

    function getDealOfTheDayBySort($input_params = array()) {
        $sort_id = $input_params['sort_by'];
        $price_id = $input_params['price_by'];
        $start_price = $input_params['start_price'];
        $end_price = $input_params['end_price'];
        $this->CI->db->start_cache();
        $this->CI->db->select('mp.iMstProductsId AS mp_mst_products_id_1,mp.vTitle AS mp_title_1, mp.fRegularPrice AS mp_regular_price_1,mp.fSalePrice AS mp_sale_price_1, mp.fRatingAvg AS mp_rating_avg_1, mp.iTotalRate AS mp_total_rate_1, mp.dDate AS mp_date_1, mp.iSaleState AS mp_sale_state_1, mp.iViewState AS mp_view_state, mp.vDefaultImg AS mp_default_img_1, mp.iDifferencePer AS mp_difference_per, mp.iWishlistState AS mp_wishlist_state,mp.iStock as mp_stock');
        $this->CI->db->from('trn_dealoftheday AS td');

        $where = "td.eStatus IN('Active') AND mp.eStatus IN('Active')";
        // if($start_price >= 0 && $end_price > 0){
        $where .= "AND IF(mp.fSalePrice > 0, mp.fSalePrice BETWEEN '" . $start_price . "'  AND '" . $end_price . "', mp.fRegularPrice BETWEEN '" . $start_price . "'  AND '" . $end_price . "')";
        // }
        $this->CI->db->where($where);
        $this->CI->db->join('mst_products AS mp', 'td.iMstProductsId = mp.iMstProductsId');
        if ($sort_id > 0 && $price_id > 0) {
            if ($sort_id == 1) {
                if ($price_id == 1) {
                    $this->CI->db->order_by('mp.iSaleState', 'DESC');
                    // $this->CI->db->order_by('mp.fRegularPrice', 'DESC');
                    $this->CI->db->order_by('mp.fSalePrice', 'DESC');
                } else {
                    // $this->CI->db->order_by('mp.fRegularPrice', 'ASC');
                    $this->CI->db->order_by('mp.fSalePrice', 'ASC');
                    $this->CI->db->order_by('mp.iSaleState', 'DESC');
                }
            } elseif ($sort_id == 2) {
                if ($price_id == 1) {
                    // $this->CI->db->order_by('mp.fRegularPrice', 'DESC');
                    $this->CI->db->order_by('mp.fSalePrice', 'DESC');
                    $this->CI->db->order_by('mp.iMstProductsId', 'DESC');
                } else {
                    $this->CI->db->order_by('mp.iMstProductsId', 'DESC');
                    // $this->CI->db->order_by('mp.fRegularPrice', 'ASC');
                    $this->CI->db->order_by('mp.fSalePrice', 'ASC');
                }
            } elseif ($sort_id == 3) {
                if ($price_id == 1) {
                    $this->CI->db->order_by('mp.iDifferencePer', 'DESC');
                    // $this->CI->db->order_by('mp.fRegularPrice', 'DESC');
                    $this->CI->db->order_by('mp.fSalePrice', 'DESC');
                } else {
                    // $this->CI->db->order_by('mp.fRegularPrice', 'ASC');
                    $this->CI->db->order_by('mp.fSalePrice', 'ASC');
                    $this->CI->db->order_by('mp.iDifferencePer', 'DESC');
                }
            } else {
                if ($price_id == 1) {
                    $this->CI->db->order_by('mp.iViewState', 'DESC');
                    // $this->CI->db->order_by('mp.fRegularPrice', 'DESC');
                    $this->CI->db->order_by('mp.fSalePrice', 'DESC');
                } else {
                    // $this->CI->db->order_by('mp.fRegularPrice', 'ASC');
                    $this->CI->db->order_by('mp.fSalePrice', 'ASC');
                    $this->CI->db->order_by('mp.iViewState', 'DESC');
                }
            }
        } elseif ($sort_id > 0 && $price_id == 0) {
            if ($sort_id == 1) {
                $this->CI->db->order_by('mp.iSaleState', 'DESC');
            } elseif ($sort_id == 2) {
                $this->CI->db->order_by('mp.iMstProductsId', 'DESC');
            } elseif ($sort_id == 3) {
                $this->CI->db->order_by('mp.iDifferencePer', 'DESC');
            } else {
                
            }
        } else {
            if ($price_id == 1) {
                // $this->CI->db->order_by('mp.fRegularPrice', 'DESC');
                $this->CI->db->order_by('mp.fSalePrice', 'DESC');
            } else {
                // $this->CI->db->order_by('mp.fRegularPrice', 'ASC');
                $this->CI->db->order_by('mp.fSalePrice', 'ASC');
            }
        }
        $this->CI->db->stop_cache();
        $total_records = $this->CI->db->count_all_results();

        $settings_params['count'] = $total_records;

        $record_limit = $this->CI->config->item("REC_LIMIT_FRONT");
        $record_limit = intval($record_limit);
        $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;

        $total_pages = ceil($total_records / $record_limit);
        $start_index = ($current_page - 1) * $record_limit;
        $settings_params['curr_page'] = $current_page;
        $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
        $settings_params['next_page'] = ($current_page + 1 > $total_pages) ? 0 : 1;

        $this->CI->db->limit($record_limit, $start_index);
        $product_data = $this->CI->db->get()->result_array();
        $this->CI->db->flush_cache();
        // echo $this->CI->db->last_query();
        $return_array[0]['settings'] = $settings_params;
        $return_array[0]['product_data'] = $product_data;
        return $return_array;
    }

    function getInvoiceDownloadHTML($value = '', $id = '', $data = array()) {
        $sub_order_id = $data['iMstSubOrderId'];
        if (intval($sub_order_id) > 0) {
            $admin_url = $this->CI->config->item("admin_url");
            $module_url = $admin_url . "shippedorderlisting/shippedorderlisting/shipping_invoice?id=" . $sub_order_id . "&download=1";

            $html = "<p style='text-align:center;'><span style='font-family:Trebuchet MS;font-size:13px;font-weight:normal;font-style:normal;text-decoration:underline;color:#006699;'><a target='_blank' href ='" . $module_url . "'>Download Invoice</span></p>";
            return $html;
        }
    }

    function getQuickSearchBySort($input_params = array()) {
        $vDetail = $input_params['vDetail'];
        $sort_id = $input_params['sort_by'];
        $price_id = $input_params['price_by'];
        $start_price = $input_params['start_price'];
        $end_price = $input_params['end_price'];
        $this->CI->db->start_cache();
        $this->CI->db->select('mp.iMstProductsId AS mp_mst_products_id_1,mp.vTitle AS mp_title_1, mp.fRegularPrice AS mp_regular_price_1,mp.fSalePrice AS mp_sale_price_1, mp.fRatingAvg AS mp_rating_avg_1, mp.iTotalRate AS mp_total_rate_1, mp.dDate AS mp_date_1, mp.iSaleState AS mp_sale_state_1, mp.iViewState AS mp_view_state, mp.vDefaultImg AS mp_default_img_1, mp.iDifferencePer AS mp_difference_per, mp.iWishlistState AS mp_wishlist_state,mp.iStock as mp_stock');
        $this->CI->db->from('mst_products AS mp');
        $this->CI->db->join('mst_store_detail AS msd', 'mp.iStoreId = msd.iMstStoreDetailId');
        $where = "mp.eStatus IN('Active') AND mp.tSearchKeyword LIKE '%" . $vDetail . "%'";
        // if ($start_price >= 0 && $end_price > 0) {
        $where .= "AND IF(mp.fSalePrice > 0, mp.fSalePrice BETWEEN '" . $start_price . "'  AND '" . $end_price . "', mp.fRegularPrice BETWEEN '" . $start_price . "'  AND '" . $end_price . "')";
        // }
        $this->CI->db->where($where);
        if ($sort_id > 0 && $price_id > 0) {
            if ($sort_id == 1) {
                if ($price_id == 1) {
                    $this->CI->db->order_by('mp.iSaleState', 'DESC');
                    $this->CI->db->order_by('mp.fRegularPrice', 'DESC');
                } else {
                    $this->CI->db->order_by('mp.fRegularPrice', 'ASC');
                    $this->CI->db->order_by('mp.iSaleState', 'DESC');
                }
            } elseif ($sort_id == 2) {
                if ($price_id == 1) {
                    $this->CI->db->order_by('mp.iMstProductsId', 'DESC');
                    $this->CI->db->order_by('mp.fRegularPrice', 'DESC');
                } else {
                    $this->CI->db->order_by('mp.fRegularPrice', 'ASC');
                    $this->CI->db->order_by('mp.iMstProductsId', 'DESC');
                }
            } elseif ($sort_id == 3) {
                if ($price_id == 1) {
                    $this->CI->db->order_by('mp.iDifferencePer', 'DESC');
                    $this->CI->db->order_by('mp.fRegularPrice', 'DESC');
                } else {
                    $this->CI->db->order_by('mp.fRegularPrice', 'ASC');
                    $this->CI->db->order_by('mp.iDifferencePer', 'DESC');
                }
            } else {
                if ($price_id == 1) {
                    $this->CI->db->order_by('mp.iViewState', 'DESC');
                    $this->CI->db->order_by('mp.fRegularPrice', 'DESC');
                } else {
                    $this->CI->db->order_by('mp.fRegularPrice', 'ASC');
                    $this->CI->db->order_by('mp.iViewState', 'DESC');
                }
            }
        } elseif ($sort_id > 0 && $price_id == 0) {
            if ($sort_id == 1) {
                $this->CI->db->order_by('mp.iSaleState', 'DESC');
            } elseif ($sort_id == 2) {
                $this->CI->db->order_by('mp.iMstProductsId', 'DESC');
            } elseif ($sort_id == 3) {
                $this->CI->db->order_by('mp.iDifferencePer', 'DESC');
            } else {
                
            }
        } else {
            if ($price_id == 1) {
                $this->CI->db->order_by('mp.fRegularPrice', 'DESC');
            } else {
                $this->CI->db->order_by('mp.fRegularPrice', 'ASC');
            }
        }
        $this->CI->db->stop_cache();
        $total_records = $this->CI->db->count_all_results();

        $settings_params['count'] = $total_records;

        $record_limit = $this->CI->config->item("REC_LIMIT_FRONT");
        $record_limit = intval($record_limit);
        // $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;
        $current_page = intval($input_params['page']) > 0 ? intval($input_params['page']) : 1;

        $total_pages = ceil($total_records / $record_limit);
        $start_index = ($current_page - 1) * $record_limit;
        $settings_params['curr_page'] = $current_page;
        $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
        $settings_params['next_page'] = ($current_page + 1 > $total_pages) ? 0 : 1;

        $this->CI->db->limit($record_limit, $start_index);
        $product_data = $this->CI->db->get()->result_array();
        $this->CI->db->flush_cache();
        // echo $this->CI->db->last_query();
        $return_array[0]['settings'] = $settings_params;
        $return_array[0]['product_data'] = $product_data;
        return $return_array;
    }

    function CategoryWalkup_ParentNode_Dropdown($value = '', $mode = '', $id = '', $data = array(), $parent_src = '') {
        $dropdownArr = array();
        $product_id = (intval($data['tpo_product_id']) > 0 ? $data['tpo_product_id'] : $_REQUEST['parID']);
        if ($product_id > 0) {
            $this->CI->db->select("iMstCategoriesId");
            $this->CI->db->from("trn_product_categories");
            $this->CI->db->where("iMstProductsId", $product_id);
            //   $this->CI->db->where("eStatus", 'Active');
            $product_categorieArr = $this->CI->db->get()->result_array();
            $categoryId = intval($product_categorieArr[0]['iMstCategoriesId']);
            //$categoryId=10;
            if ($categoryId > 0) {
                $query = 'SELECT MIN(T2.iMstCategoriesId) as desired_cat_id, T2.vTitle  FROM ( SELECT @r AS _id, (SELECT @r := iParentId FROM mst_categories WHERE iMstCategoriesId= _id) AS parent_id, @l := @l + 1 AS lvl FROM (SELECT @r := ' . $categoryId . ', @l := 0) vars, mst_categories h WHERE @r <> 0) T1 JOIN mst_categories T2 ON T1._id = T2.iMstCategoriesId ORDER BY T1.lvl DESC';
                $retAr = $this->CI->db->query($query)->result_array();
                $desired_cat_id = intval($retAr[0]['desired_cat_id']);
                if ($desired_cat_id > 0) {
                    //$desired_cat_id=1;
                    $this->CI->db->select("*");
                    $this->CI->db->from("category_option_view");
                    $this->CI->db->where("cat_id", $desired_cat_id);
                    $retViewArr = $this->CI->db->get()->result_array();
                    if (is_array($retViewArr) && count($retViewArr) > 0) {
                        foreach ($retViewArr as $key => $val) {
                            $dropdownArr[$key]['id'] = $val['view_cateogry_option_id'];
                            $dropdownArr[$key]['val'] = $val['view_title'];
                        }
                    }
                }
            }
        }
        return $dropdownArr;
    }

    function cancelled_user_name($input_params = array()) {
        $return_arr = array();
        $cancelled_by = $input_params['NEW_eCanelBy'];
        if ($cancelled_by == 'B') {
            $cancelled_by = $input_params['buyer_name'];
        } elseif ($cancelled_by == 'S') {
            $cancelled_by = $input_params['seller_name'];
        } else {
            $cancelled_by = 'Admin';
        }
        $return_arr[0]['cancelled_by'] = $cancelled_by;
        return $return_arr;
    }

    function getImageURL($data = array()) {
        $return_arr = array();
        $input_arr = array();
        $image = trim($data['NEW_vRequestImg']);
        if ($image != '' && isset($image)) {
            //fetch from the physical folder
            $input_arr['image_name'] = $image;
            $input_arr['path'] = 'request_image';
            $input_arr['pk'] = '';
            $input_arr['height'] = '';
            $input_arr['width'] = '';
            $c_image = $this->get_image($input_arr);
            if ($c_image == '') {
                $c_image = $this->CI->config->item('images_url') . "noimage.gif";
            }
        } else {
            $c_image = $this->CI->config->item('images_url') . "noimage.gif";
        }
        $return_arr[0]['image_url'] = $c_image;
        return $return_arr;
    }

    function generateOrderNumber($input = 0, $input_param = array()) {
        return $this->CI->config->item('ORDER_NUMBER_START') + $input_param['order_id'];
    }

    function generateSubOrderNumber($input = 0, $input_param = array()) {
        // return $this->CI->config->item('SUB_ORDER_NUMBER_START')+$input_param['sub_order_id'];
        $this->CI->db->select("iMstSubOrderId");
        $this->CI->db->from("mst_sub_order");
        $this->CI->db->order_by("iMstSubOrderId", "DESC");
        $this->CI->db->limit("1");
        $last_record = $this->CI->db->get()->result_array();
        return $this->CI->config->item('SUB_ORDER_NUMBER_START') + ($last_record[0]['iMstSubOrderId'] + 1);
    }

    function update_shipping_status($mode = '', $id = '', $parID = '') {
        $ret_arr = array();
        $data = array();
        $data['eItemStatus'] = "Shipped";
        $this->CI->db->where("iMstSubOrderId", $id);
        $res = $this->CI->db->update("mst_sub_order", $data);
        if ($res) {
            $ret_arr['success'] = true;
            $message = "Shipping details updated successfully.";
            $ret_arr['message'] = $message;
        } else {
            $ret_arr['success'] = false;
            $message = "Failure occured while updating the shipping details.";
            $ret_arr['message'] = $message;
        }
        return $ret_arr;
    }

    function get_suborder_number($input_params = array()) {
        $return_arr = array();
        $sub_order_number = $input_params['NEW_iSubOrderNumber'];
        $sub_order_prefix = $input_params['NEW_vSubOrderNumberPre'];
        $return_arr[0]['combined_sub_order_number'] = $sub_order_prefix . '' . $sub_order_number;
        return $return_arr;
    }

    function getBuyerShippingAddressExpandedView($value = '', $id = '', $data = array()) {
        ////Line 1  - Street name , Area
        // Line 2   City , State , zipcode
        // Line 3 , COuntry
        $final_address_arr = '';
        $buyer_area = $data['mo_buyer_area'];
        $buyer_address2 = $data['mo_buyer_address2'];
        $buyer_address1 = $data['mo_buyer_address1'];
        $country = $data['mc1_country'];
        $state = $data['ms_state'];
        $city = $data['mc2_city'];
        $pin_code = $data['mo_buyer_pin_code'];
        if ($buyer_address1 != '' && isset($buyer_address1)) {
            $line1 = $buyer_address1;
        }
        if ($buyer_area != '' && isset($buyer_area)) {
            if (strlen($line1) > 0) {
                $line1 .= ',';
            }
            $line1 .= $buyer_area . "";
        }
        if (strlen($line1) > 0) {
            $line1 .= "<br/>";
        }
        if ($city != '' && isset($city)) {
            $line2 = $city;
        }
        if ($state != '' && isset($state)) {
            if (strlen($line2) > 0) {
                $line2 .= ',';
            }
            $line2 .= $state;
        }
        if ($pin_code != '' && isset($pin_code)) {
            if (strlen($line2) > 0) {
                $line2 .= ',';
            }
            $line2 .= $pin_code . "";
        }
        if (strlen($line2) > 0) {
            $line2 .= "<br/>";
        }
        if ($country != '' && isset($country)) {
            $line3 = $country;
        }
        $final_address_arr = $line1 . $line2 . $line3;
        return $final_address_arr;
    }

    function getOrderAddedDate($mode = '', $value = '', $data = array(), $id = '', $field_name = '', $field_id = '') {
        $order_id = intval($data['mso_mst_order_id']);
        $order_date = '';
        if ($order_id > 0) {
            //$this->CI->db->select("DATE_FORMAT(dDate,'%M %d,%Y') as order_added_date");
            $this->CI->db->select("DATE(dDate) as order_added_date");
            $this->CI->db->from("mst_order");
            $this->CI->db->where("iMstOrderId", $order_id);
            $db_data = $this->CI->db->get()->result_array();
            $order_date = $this->dateSystemFormat($db_data[0]['order_added_date']);
        }
        return $order_date;
    }

    function Delete_product($mode = '', $id = '', $parID = '') {
        $ret_arr = array();
        if ((is_array($id) && count($id) > 0) || (strlen($id) > 1 && $id != '')) {
            if (is_array($id) && count($id) > 0) {
                $ids = implode(",", $id);
                $ids = trim($ids, ",");
                $ids = preg_replace('/,,+/', ',', $ids);
            } else {
                $ids = trim($id, ",");
            }
            $query = "select * from mst_sub_order where iMstProductsId IN ($ids)";
            $productArr = $this->CI->db->query($query)->result_array();
            if (count($productArr) < 1 && is_array($productArr)) {

                // Delete product Data
                $del_query = "DELETE FROM mst_products WHERE iMstProductsId  IN ($ids)";
                $this->CI->db->query($del_query);

                // Delete product Image
                $del_prd_img = "DELETE FROM mst_product_img WHERE  iMstProductsId IN ($ids)";
                $this->CI->db->query($del_prd_img);

                // Delete product Clean Url
                $del_prd_cln_url = "DELETE FROM trn_clean_url WHERE  iResourceId IN ($ids) AND eResourceType = 'P'";
                $this->CI->db->query($del_prd_cln_url);

                // Delete product Option
                $del_prd_opt = "DELETE FROM trn_product_option WHERE  iProductId IN ($ids)";
                $this->CI->db->query($del_prd_opt);

                // Delete product Category
                $del_prd_cat = "DELETE FROM trn_product_categories WHERE  iMstProductsId IN ($ids)";

                $this->CI->db->query($del_prd_cat);

                // Delete Feature product
                $del_prd_feat = "DELETE FROM trn_featured_products WHERE  iMstProductsId IN ($ids)";
                $this->CI->db->query($del_prd_feat);

                // Delete product Rating Data
                $del_prd_rating = "DELETE FROM trn_product_rating WHERE  iMstProductsId IN ($ids)";
                $this->CI->db->query($del_prd_rating);

                // Delete product Rating Data
                $del_prd_wish = "DELETE FROM trn_product_wishlist WHERE  iMstProductsId IN ($ids)";
                $this->CI->db->query($del_prd_wish);

                // Delete product Wishlist Data
                $del_prd_wish = "DELETE FROM trn_product_wishlist WHERE  iMstProductsId IN ($ids)";
                $this->CI->db->query($del_prd_wish);
                $message = "Product's deleted successfully.";
                $status = 1;
            } else {
                $status = 0;
                $message = "You cannot deleted a product having 1 or more sub order.";
            }
        } else {
            $status = 0;
            $message = "Failure occured in deleteing the product.";
        }
        $ret_arr['success'] = $status;
        $ret_arr['message'] = $message;
        return $ret_arr;
    }

    function get_formated_currency_details($value = '', $id = '', $data = array()) {
        $currency_symbol = $this->CI->config->item("CURRENCY_SYMBOL");
        $decimal_sep = $this->CI->config->item('ADMIN_DECIMAL_SEPARATOR');
        $decimal_plc = $this->CI->config->item('ADMIN_DECIMAL_PLACES');
        $value = floatval($value);
        $ret_arr = array();
        $thousand_seperator = ($thousand_sep == 'space' || $thousand_sep == 'none') ? ($thousand_sep == 'none' ? '' : ' ') : ',';
        $decimal_seperator = $decimal_sep == 'comma' ? ',' : '.';
        $decimal_places = $decimal_plc != '' ? $decimal_plc : '2';
        $value = number_format($value, $decimal_places, $decimal_seperator, $thousand_seperator);
        $final_amount = $currency_symbol . $value;
        return $final_amount;
    }

    function get_formated_phone_details($value = '', $id = '', $data = array()) {
        return $this->getPhoneMaskedView('', $value);
    }

    function getDueDays($value = '', $id = '', $data = array()) {
        return $this->getDaysAgo($data['status_date']);
    }

    function getDaysAgo($input = '') {
        $msg = '';
        $last_update = trim($input);
        $server_date_time = date('Y-m-d H:i:s');
        if ($this->validateDate($last_update, 'Y-m-d H:i:s')) {
            $last_update = date('Y-m-d H:i:s', strtotime($last_update));
            $date1 = date_create($server_date_time);
            $date2 = date_create($last_update);
            $log = date_diff($date1, $date2);
            if (is_object($log)) {
                if ($log->days < 1) {
                    if ($log->h < 1) {
                        if ($log->i < 1) {
                            $msg = $log->s . ' sec' . (($log->s > 1) ? 's' : '') . ' ago';
                        } else {
                            $msg = $log->i . ' min' . (($log->i > 1) ? 's' : '') . ' ago';
                        }
                    } else {
                        $msg = $log->h . ' hour' . (($log->h > 1) ? 's' : '') . ' ago';
                    }
                } elseif ($log->days <= 31 && $log->m == 0) {
                    $msg = $log->days . ' day' . (($log->days > 1) ? 's' : '') . ' ago';
                } elseif ($log->m > 0 && $log->y == 0) {
                    $msg = $log->m . ' month' . (($log->m > 1) ? 's' : '') . ' ago';
                } else {
                    $msg = $log->y . ' year' . (($log->y > 1) ? 's' : '') . ' ago';
                }
            }
            /*
              $log = array();
              $log['days'] = floor($dateDiff / (60 * 60 * 24));
              $log['months'] = floor($dateDiff / 86400 / 30);
              $log['years'] = floor(($dateDiff) / (60 * 60 * 24 * 365));
             */
        }
        return $msg;
    }

    function getAddressDownloadHTML($value = '', $id = '', $data = array()) {
        $sub_order_id = $data['iMstSubOrderId'];
        if (intval($sub_order_id) > 0) {
            $admin_url = $this->CI->config->item("admin_url");
            $module_url = $admin_url . "#shippedorderlisting/shippedorderlisting/shipping_address|mode|View|id|" . $sub_order_id . "|width|400px";

            $html = "<p style='text-align:center;'><span style='font-family:Trebuchet MS;font-size:13px;font-weight:normal;font-style:normal;text-decoration:underline;color:#006699;'><a class='fancybox-popup' href ='" . $module_url . "'>View Address</span></p>";
            return $html;
        }
    }

    function test($mode = '', $value = '', $data = array(), $id = '', $field_name = '', $field_id = '') {
        echo $id;
        exit;
        return 11;
    }

    function update_order_settlement($mode = '', $id = '', $parID = '') {
        $update_data = array();
        $settlement_id = intval($id);
        $sub_order_ids = trim($_POST['id'], ',');
        $sub_order_ids = preg_replace('/,,+/', ',', $sub_order_ids);
        $message = "Failed to update sub order setellment details.";
        $ret_arr['success'] = false;
        $ret_arr['message'] = $message;
        if ($settlement_id > 0 && $sub_order_ids != '' && isset($sub_order_ids)) {
            $update_data['iTrnSettlementId'] = $settlement_id;
            $update_data['eSettlement'] = 'Yes';
            $update_condition = "iMstSubOrderId IN ($sub_order_ids)";
            $this->CI->db->where($update_condition, null, false);
            $res = $this->CI->db->update("mst_sub_order", $update_data);
            if ($res) {
                $message = "Settlement details updated successfully.";
                $ret_arr['success'] = true;
                $ret_arr['message'] = $message;
            }
        }
        return $ret_arr;
    }

    function encrypt($input_params = array()) {
        $return_arr = array();
        $pure_string = (trim($input_params['password']) != '' ? trim($input_params['password']) : trim($input_params['new_password']));
        $encryption_key = md5($this->CI->config->item("ADMIN_ENC_KEY"));
        $iv = str_repeat(" ", mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
        $crypted_text = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $encryption_key, $pure_string, MCRYPT_MODE_ECB, $iv);
        $encrypted_string = bin2hex($crypted_text);
        // $return_arr['encrypted_string'] = $encrypted_string;
        return $encrypted_string;
    }

    function decrypt($encrypted_string = '') {
        $return_arr = array();
        $encryption_key = md5($this->CI->config->item("ADMIN_ENC_KEY"));
        $iv = str_repeat("", mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
        $encrypted_string = $this->hextobin($encrypted_string);
        $decrypted_string = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
        return trim($decrypted_string);
    }

    function hextobin($hexstr = '') {
        $n = strlen($hexstr);
        $sbin = "";
        $i = 0;
        while ($i < $n) {
            $a = substr($hexstr, $i, 2);
            $c = pack("H*", $a);
            if ($i == 0) {
                $sbin = $c;
            } else {
                $sbin .= $c;
            }
            $i += 2;
        }
        return $sbin;
    }

    function getFormatedDate($input = '') {
        $input_date = $input;
        if ($this->validateDate($input_date, 'Y-m-d H:i:s')) {
            $input_date = date('M j,Y', strtotime($input_date));
        } else {
            $input_date = '';
        }
        return $input_date;
    }

    function getTopmostParentCateogry($value = '', $id = '', $data = array()) {
        $top_most_parent_cat_name = '';
        $categoryId = intval($value);
        if ($categoryId > 0) {
            $query = 'SELECT MIN(T2.iMstCategoriesId) as desired_cat_id, T2.vTitle  FROM ( SELECT @r AS _id, (SELECT @r := iParentId FROM mst_categories WHERE iMstCategoriesId= _id) AS parent_id, @l := @l + 1 AS lvl FROM (SELECT @r := ' . $categoryId . ', @l := 0) vars, mst_categories h WHERE @r <> 0) T1 JOIN mst_categories T2 ON T1._id = T2.iMstCategoriesId ORDER BY T1.lvl DESC';
            $retAr = $this->CI->db->query($query)->result_array();
            $desired_cat_id = intval($retAr[0]['desired_cat_id']);
            if ($desired_cat_id > 0) {
                $this->CI->db->select("vTitle");
                $this->CI->db->from("mst_categories");
                $this->CI->db->where("iMstCategoriesId", $desired_cat_id);
                $categoryArr = $this->CI->db->get()->result_array();
                if (is_array($categoryArr) && count($categoryArr) > 0) {
                    $top_most_parent_cat_name = $categoryArr[0]['vTitle'];
                }
            }
        }
        return $top_most_parent_cat_name;
    }

    function getSettelmentInvoiceHTML($value = '', $id = '', $data = array()) {
        $settelment_id = $data['iTrnSettlementId'];
        if (intval($settelment_id) > 0) {
            $admin_url = $this->CI->config->item("admin_url");
            $module_url = $admin_url . "#adminsettledtransactions/adminsettledtransactions/settelment_invoice/add|mode|View|id|" . $settelment_id;
            $html = "<p style='text-align:center;'><span style='font-family:Trebuchet MS;font-size:13px;font-weight:normal;font-style:normal;text-decoration:underline;color:#006699;'><a class='fancybox-popup' href ='" . $module_url . "'>View Invoice</span></p>";
            return $html;
        }
    }

    function getSettelmentInvoiceDownloadHTML($value = '', $id = '', $data = array()) {
        $settelment_id = $data['iTrnSettlementId'];
        if (intval($settelment_id) > 0) {
            $admin_url = $this->CI->config->item("admin_url");
            $module_url = $admin_url . "adminsettledtransactions/adminsettledtransactions/settelment_invoice/?id=" . $settelment_id . "&download=1";

            $html = "<p style='text-align:center;'><span style='font-family:Trebuchet MS;font-size:13px;font-weight:normal;font-style:normal;text-decoration:underline;color:#006699;'><a target='_blank' href ='" . $module_url . "'>Download Invoice</span></p>";
            return $html;
        }
    }

    function dynamic_assignment_sub_order(&$input_params = array()) {
        $input_params['NEW_fProductRegularPrice'] = $this->get_formated_currency_details($input_params['NEW_fProductRegularPrice']);
        $input_params['NEW_fProductSalePrice'] = $this->get_formated_currency_details($input_params['NEW_fProductSalePrice']);
        $input_params['NEW_fProductPrice'] = $this->get_formated_currency_details($input_params['NEW_fProductPrice']);
        $input_params['NEW_fShippingCost'] = $this->get_formated_currency_details($input_params['NEW_fShippingCost']);
        $input_params['NEW_fTotalCost'] = $this->get_formated_currency_details($input_params['NEW_fTotalCost']);
        $input_params['NEW_fGatewayTDR'] = $this->get_formated_currency_details($input_params['NEW_fGatewayTDR']);
        $input_params['NEW_fAdminCommision'] = $this->get_formated_currency_details($input_params['NEW_fAdminCommision']);
        $input_params['NEW_vSellerInvoiceDate'] = $this->dateSystemFormat($input_params['NEW_vSellerInvoiceDate']);
        $input_params['NEW_dShippingDate'] = $this->dateSystemFormat($input_params['NEW_dShippingDate']);
        $input_params['NEW_dCancelDate'] = $this->dateSystemFormat($input_params['NEW_dCancelDate']);
        $input_params['NEW_dDeliveredDate'] = $this->dateSystemFormat($input_params['NEW_dDeliveredDate']);
    }

    function dynamic_assignment_rma(&$input_params = array()) {
        $input_params['NEW_dRequestDate'] = $this->dateSystemFormat($input_params['NEW_dRequestDate']);
        $input_params['NEW_tRequestDetail'] = ucfirst($input_params['NEW_tRequestDetail']);
    }

    function get_rma_number($input_params = array()) {
        $return_arr = array();
        $rma_id = intval($input_params['NEW_iTrnRmaId']);
        if ($rma_id > 0) {
            $rma_prefix = $this->CI->config->item("RMA_PREFIX");
            $return_arr[0]['rma_number'] = $rma_prefix . $rma_id;
        } else {
            $return_arr[0]['rma_number'] = '';
        }
        return $return_arr;
    }

    function get_admin_rma_number($value = '', $id = '', $data = array()) {
        //   pr($data);exit;
        $value = intval($value);
        $rma_number = '';
        if ($value > 0) {
            $rma_prefix = $this->CI->config->item("RMA_PREFIX");
            $rma_number = $rma_prefix . $value;
        }
        return $rma_number;
    }

    function activate_seller($input_params = array()) {
        $ret_arr = array();
        $id = $input_params['id'];
        if ((is_array($id) && count($id) > 0) || (strlen($id) > 1 && $id != '')) {
            if (is_array($id) && count($id) > 0) {
                $ids = implode(",", $id);
                $ids = trim($ids, ",");
                $ids = preg_replace('/,,+/', ',', $ids);
            } else {
                $ids = trim($id, ",");
            }
            $idArr = explode(',', $ids);
            $update_arr = array();
            $update_arr['eStatus'] = 'Active';

            $this->CI->db->where_in('iAdminId', $idArr);
            $res = $this->CI->db->update("mod_admin", $update_arr);

            $this->CI->db->where_in('iAdminId', $idArr);
            $res1 = $this->CI->db->update("mst_store_detail", $update_arr);
            if ($res && $res1) {
                $message = "Seller's activated successfully.";
                $status = 1;
            } else {
                $status = 0;
                $message = "Failure occured in activating the seller's.";
            }
        } else {
            $status = 0;
            $message = "Failure occured in activating the seller's.";
        }
        $ret_arr['success'] = $status;
        $ret_arr['message'] = $message;
        return $ret_arr;
    }

    function resize_image($params = array()) {
        $enc_image_url = base64_encode($params['default_image']);
        $c_image = $this->CI->config->item('site_url') . 'WS/image_resize/?pic=' . $enc_image_url . '&height=' . $params['height'] . '&width=' . $params['width'] . "&color=" . $color;
        return $c_image;
    }

    function getSellarDate($value = '', $id = '', $data_arr = array()) {
        return date('F, Y', strtotime($value));
    }

    function calculateDueAmount($value = '', $id = '', $data = array()) {
        //pr($data);exit;
        $mso_total_cost = floatval($data['mso_total_cost']);
        $trr_total_amount = floatval($data['trr_total_amount']);
        $mso_gateway_t_d_r = floatval($data['mso_gateway_t_d_r']);
        $mso_admin_commision = floatval($data['mso_admin_commision']);

        if ($trr_total_amount > 0) {
            $ret_val = $mso_total_cost - $trr_total_amount - $mso_gateway_t_d_r - $mso_admin_commision;
        } else {
            $ret_val = $mso_total_cost - $mso_gateway_t_d_r - $mso_admin_commision;
        }
        
        return $ret_val;
        /*
          if ($data['trr_total_amount'] > 0) {
          $ret_val = $data['mso_total_cost'] - $data['trr_total_amount'];
          } else {
          $ret_val = $data['mso_total_cost'] - $data['mso_gateway_t_d_r'] - $data['mso_admin_commision'];
          }
          //IF(trr.fTotalAmount > 0,mso.fTotalCost - trr.fTotalAmount,mso.fTotalCost-fGatewayTDR-fAdminCommision)
          return $ret_val; */
    }

    function getSalesProfit($value = '', $id = '', $data = array()) {
        $total_cost = floatval($data['mso_total_cost']);
        $admin_commision = floatval($data['mso_admin_commision']);
        $gateway_tdr = floatval($data['mso_gateway_t_d_r']);
        $final_profit = $total_cost - ($admin_commision + $gateway_tdr);
        return $final_profit;
        //(mso.fTotalCost - (mso.fAdminCommision + mso.fGatewayTDR))
    }

    function getDueDateNew($value = '', $id = '', $data = array()) {
        $return_period = intval($data['mso_return_period']);
        if ($this->validateDate($value, 'Y-m-d H:i:s')) {
            $date = date('Y-m-d H:i:s', strtotime($value));

            $date = date('Y-m-d H:i:s', strtotime("$date +$return_period day"));
            $final_date = $this->getFormatedDate($date);
        }
        return $final_date;
    }

    function generateActivationCode($input_paerams = array()) {
        $length = 15;
        $final_rand = '';
        $res = array();
        for ($i = 0; $i < $length; $i++) {
            $final_rand .= rand(0, 9);
        }
        $res[0]['activation_code'] = $final_rand;
        return $res;
    }

    function generateUnsubscribeURL($input_params = array()) {
        $return_arr = array();
        $activation_code = $input_params['activation_code'];
        $return_arr[0]['unsubsribe_url'] = $this->CI->config->item("site_url") . "unsubscribe-newsletter.html?e=" . $activation_code;
        return $return_arr;
    }

    function generateSellerStoreLink($seller_id) {
        $query = "SELECT * FROM mst_store_detail WHERE iAdminId=$seller_id";
        $retAr = $this->CI->db->query($query)->result_array();
        //$storeLink = $this->CI->config->item("site_url") . 'store/' . $retAr[0]['iMstStoreDetailId'] . '/' .strtolower($retAr[0]['vStoreName']) . '.html';
        $storeLink = $this->CI->config->item("site_url") . 'store/' .strtolower($retAr[0]['vStoreName']) . '.html';
        return $storeLink;
    }

    function getSellerStoreId($store_data) {
        $store_name = urldecode(html_entity_decode(str_replace('.html','',$store_data)));
        $query = "SELECT iMstStoreDetailId FROM `mst_store_detail` WHERE `vStoreName` LIKE '$store_name'";
        $retAr = $this->CI->db->query($query)->row();
        return $retAr->iMstStoreDetailId;
    }

    #####GENERATED_CUSTOM_FUNCTION_END#####
}

/* End of file General.php */
/* Location: ./application/libraries/General.php */