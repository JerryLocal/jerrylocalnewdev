<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class HB_Form_validation extends CI_Form_validation {

    protected $CI;

    function __construct() {
        parent::__construct();
        $this->CI = &get_instance();
    }

    function recaptcha_matches() {
        $this->CI->load->library('recaptcha');

//        $this->CI->config->load('recaptchaconfig');
//        $public_key = $this->CI->config->item('recaptcha_public_key');
//        $private_key = $this->CI->config->item('recaptcha_private_key');
        $response_field = $this->CI->input->post('recaptcha_response_field');
        $challenge_field = $this->CI->input->post('recaptcha_challenge_field');

        $response = $this->CI->recaptcha->recaptcha_check_answer($_SERVER['REMOTE_ADDR'], $challenge_field, $response_field);

        if ($response['is_valid']) {
            return TRUE;
        } else {
            $this->recaptcha_error = $response['error'];
            $this->set_message('recaptcha_matches', 'The %s is incorrect. Please try again.');
            return FALSE;
        }
    }

    function hbcaptcha_matches() {
        $this->CI->load->library('captcha');


        $hb_captcha_input = $this->CI->input->post('hb_captcha_input');


        $response = $this->CI->captcha->valid($hb_captcha_input);

        if ($response['is_valid']) {
            return TRUE;
        } else {
            $this->captcha_error = $response['error'];
            $this->set_message('hbcaptcha_matches', 'The %s is incorrect. Please try again.');
            return FALSE;
        }
    }
    
    
     public function error_array() {
        return $this->_error_array;
    }
    
    
    
    
    public function set_value($field) {
        return $this->_field_data[$field]['postdata'];;
    }
}
?>