<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Wsresponse
{

    protected $CI;
    public $ws_debug_params;
    public $ws_log_file;

    function __construct()
    {
        $this->CI = & get_instance();
    }

    function outputResponse($output_array = array(), $func_array = array())
    {

        $data_array = $output_array['data'];
        $ouput_fields = $output_array['settings']['fields'];
        $output_keys = $func_array['function']['output_keys'];
        $output_alias = $func_array['function']['output_alias'];
        $inner_keys = $func_array['function']['inner_keys'];
        $single_keys = $func_array['function']['single_keys'];
        $multiple_keys = $func_array['function']['multiple_keys'];
        $custom_keys = $func_array['function']['custom_keys'];

        $this->makeUniqueParams($ouput_fields);
        $this->makeUniqueParams($output_keys);
        $this->makeUniqueParams($single_keys);
        $this->makeUniqueParams($multiple_keys);
        $this->makeUniqueParams($custom_keys);

        $output_keys = is_array($output_keys) ? $output_keys : array();
        $output_alias = is_array($output_alias) ? $output_alias : array();
        $inner_keys = is_array($inner_keys) ? $inner_keys : array();

        $array_data = $this->getFilteredArray($data_array, $ouput_fields, $output_keys, $inner_keys, $custom_keys);
        $array_data = $this->makeAliasArray($array_data, $output_alias, $custom_keys);
        $output_data = $this->finalResponseArray($array_data, $single_keys, $multiple_keys, $custom_keys);

        $settings_fields = $this->makeFieldsArray($ouput_fields, $output_alias);
        $this->makeUniqueParams($settings_fields);

        $output_array['data'] = $output_data;
        $output_array['settings']['fields'] = $settings_fields;

        $send_msg = $this->getWSLanguageMessage($output_array['settings']["message_code"], $func_array['function']['name'], $func_array['function']['messages'], "Flow", $data_array);
        if ($send_msg != "") {
            $output_array['settings']['message'] = $send_msg;
        }
        unset($output_array['settings']["message_code"]);
        return $output_array;
    }

    function getFilteredArray($data_arr = array(), $output_fields = array(), $output_keys = array(), $inner_keys = array(), $custom_keys = array())
    {
        if (!is_array($output_keys)) {
            return $data_arr;
        }
        $output_array = array();
        for ($i = 0; $i < count($output_keys); $i++) {
            $assoc_key = $output_keys[$i];
            if (is_array($data_arr) && array_key_exists($assoc_key, $data_arr)) {
                $data = $data_arr[$assoc_key];
                if (is_array($custom_keys) && in_array((string) $assoc_key, $custom_keys)) {
                    $output_array[$assoc_key] = $data;
                } else {
                    if (is_array($data) && count($data) > 0) {
                        $filter_arr = $this->getSpecifiedFields($data, $output_fields, $inner_keys);
                        $output_array[$assoc_key] = (is_array($filter_arr) && count($filter_arr) > 0) ? $filter_arr : array();
                    } else {
                        $output_array[$assoc_key] = $data;
                    }
                }
            }
        }
        return $output_array;
    }

    function getSpecifiedFields($data_arr = array(), $output_fields = array(), $inner_keys = array())
    {
        if (is_array($data_arr) && count($data_arr) > 0) {
            foreach ((array) $data_arr as $da_key => $da_val) {
                if (is_array($da_val)) {
                    if (is_array($inner_keys) && in_array((string) $da_key, $inner_keys)) {
                        $temp_arr = $this->getSpecifiedFields($da_val, $output_fields);
                        if (is_array($temp_arr)) {
                            $output_arr[$da_key] = $temp_arr;
                        }
                    } else {
                        if (is_array($output_fields) && in_array((string) $da_key, $output_fields)) {
                            $output_arr[$da_key] = $da_val;
                        } else {
                            $output_arr[$da_key] = $this->getSpecifiedFields($da_val, $output_fields);
                        }
                    }
                } else {
                    if (is_array($output_fields) && in_array((string) $da_key, $output_fields)) {
                        $output_arr[$da_key] = $da_val;
                    }
                }
            }
        }
        return $output_arr;
    }

    function finalResponseArray($data_arr = array(), $single_keys = array(), $multiple_keys = array(), $custom_keys = array())
    {
        if (!is_array($data_arr) || count($data_arr) == 0) {
            return $data_arr;
        }
        //$data_arr = array_filter($data_arr);
        $flag = true;
        $ret_arr = array();
        foreach ((array) $data_arr as $key => $val) {
            if (is_array($val) && count($val) > 1) {
                $val_arr = array_values($val);
                if (is_array($val_arr[1])) {
                    $flag = false;
                    break;
                }
            }
            if (is_array($multiple_keys) && count($multiple_keys) > 1 && in_array((string) $key, $multiple_keys)) {
                $flag = false;
                break;
            }
            if (is_array($multiple_keys) && count($multiple_keys) > 0 && is_array($single_keys) && count($single_keys) > 0 && in_array((string) $key, $multiple_keys)) {
                $flag = false;
                break;
            }
            if (is_array($custom_keys) && in_array((string) $key, $custom_keys)) {
                $flag = false;
                break;
            }
            $val_arr = is_array($val) ? array_values($val) : $val;
            if (is_array($val_arr[0])) {
                $ret_arr = array_merge($ret_arr, $val_arr[0]);
            } else {
                $ret_arr = is_array($val) ? array_merge($ret_arr, $val) : $ret_arr;
            }
        }
        if ($flag) {
            if (is_array($ret_arr) && count($ret_arr) > 0) {
                $send_arr = array($ret_arr);
            } else {
                $send_arr = $ret_arr;
            }
        } else {
            if (count($data_arr) == 1) {
                $data_arr = array_values($data_arr);
                $send_arr = $data_arr[0];
            } else {
                $send_arr = $data_arr;
            }
        }
        return $send_arr;
    }

    function makeAliasArray($data = array(), $alias = array(), $keep_alias = array())
    {
        if (!is_array($data) || count($data) == 0) {
            return $data;
        }
        if (!is_array($alias) || count($alias) == 0) {
            return $data;
        }
        $send = $data;
        foreach ($data as $key => $val) {
            if (is_array($val) && count($val) > 0) {
                if (is_array($keep_alias) && in_array((string) $key, $keep_alias)) {
                    continue;
                }
                $temp_val = $this->makeAliasArray($val, $alias);
            } else {
                $temp_val = $val;
            }
            if (is_array($alias) && $alias[$key]) {
                unset($send[$key]);
                $send[$alias[$key]] = $temp_val;
            } else {
                $send[$key] = $temp_val;
            }
        }
        return $send;
    }

    function makeFieldsArray($fields = array(), $alias = array())
    {
        if (!is_array($alias) || count($alias) == 0) {
            return $fields;
        }
        if (!is_array($fields) || count($fields) == 0) {
            return $fields;
        }
        foreach ($alias as $key => $val) {
            if (is_array($fields) && in_array((string) $key, $fields)) {
                $ind = array_search($key, $fields);
                $fields[$ind] = $val;
            }
        }
        return $fields;
    }

    function assignAppendRecord($input_params = array(), $output_data = array())
    {
        if (!is_array($output_data) || count($output_data) == 0) {
            return $input_params;
        }
        if (!is_array($input_params) || count($input_params) == 0) {
            return $output_data;
        }
        $input_params = $output_data + $input_params;
        return $input_params;
    }

    function assignFunctionResponse($output_data = array())
    {
        $return_data = array();
        if ((is_array($output_data['data']) && count($output_data['data']) > 0) || (is_array($output_data['settings']['fields']) && count($output_data['settings']['fields']) > 0)) {
            $return_data = $output_data['data'];
        } else if (is_array($output_data['settings'])) {
            $return_data = $output_data['settings']['success'];
        }
        return $return_data;
    }

    function unsetAppendRecord($input_params = array(), $output_data = array(), $unset_keys = array())
    {
        if (!is_array($output_data) || count($output_data) == 0) {
            return $input_params;
        }
        if (!is_array($input_params) || count($input_params) == 0) {
            return $output_data;
        }
        if (!is_array($unset_keys) || count($unset_keys) == 0) {
            $input_params = $input_params + $output_data;
            return $input_params;
        }

        foreach ($unset_keys as $key => $val) {
            unset($input_params[$val]);
        }
        if (!is_array($input_params) || count($input_params) == 0) {
            return $output_data;
        }
        $input_params = $input_params + $output_data;
        return $input_params;
    }

    function assignSingleRecord($input_params = array(), $output_data = array())
    {
        if (is_array($output_data)) {
            if (is_array($output_data) && array_key_exists(0, $output_data)) {
                $input_params = array_merge($input_params, $output_data[0]);
            } else {
                $input_params = array_merge($input_params, $output_data);
            }
        }
        return $input_params;
    }

    function assignOtherWSRecord($input_params = array(), $output_data = array())
    {
        if (is_array($output_data)) {
            if (is_array($output_data) && array_key_exists(0, $output_data)) {
                $input_params = array_merge($input_params, $output_data[0]);
            } else {
                $input_params = array_merge($input_params, $output_data);
            }
        }
        return $input_params;
    }

    function filterLoopParams($output_data = array(), $loop_arr = array(), $input_params = array())
    {
        $temp_arr = $output_data;
        $send_arr = $unset_arr = array();
        $extra_keys = array_diff_assoc($output_data, $input_params);

        foreach ($extra_keys as $key => $val) {
            $send_arr[$key] = $val;
            if (is_array($val) && array_key_exists(0, $val)) {
                $unset_arr = is_array($val[0]) ? array_keys($val[0]) : $unset_arr;
            }
        }

        foreach ($unset_arr as $key => $val) {
            if (is_array($send_arr) && array_key_exists($val, $send_arr)) {
                unset($send_arr[$val]);
            }
        }

        if (is_array($loop_arr) && count($loop_arr) > 0) {
            foreach ($loop_arr as $key => $val) {
                if (is_array($temp_arr) && array_key_exists($key, $temp_arr)) {
                    $send_arr[$key] = $temp_arr[$key];
                }
            }
        }
        return $send_arr;
    }

    function grabLoopVariables($loop_vars = array(), $input_params = array())
    {
        if (!is_array($loop_vars) || count($loop_vars) == 0) {
            return $input_params;
        }
        foreach ($loop_vars as $key => $val) {
            if (!is_array($val) || count($val) == 0) {
                continue;
            }
            $input_params = array_merge($input_params, $val);
        }
        return $input_params;
    }

    function makeUniqueParams(&$params = array())
    {
        $params = (is_array($params)) ? array_values(array_unique($params)) : array();
    }

    function makeFilterParams(&$params = array())
    {
        $params = (is_array($params)) ? array_values(array_unique(array_filter($params))) : array();
    }

    function validateInputParams($param_array = array(), $request_arr = array(), $ws_func = '', $message_arr = array())
    {
        if (is_array($request_arr) && array_key_exists("_", $request_arr)) {
            unset($request_arr['_']);
        }
        if (is_array($request_arr) && array_key_exists("ws_debug", $request_arr)) {
            unset($request_arr['ws_debug']);
        }
        if (is_array($request_arr) && array_key_exists("ws_ctrls", $request_arr)) {
            unset($request_arr['ws_ctrls']);
        }
        if (is_array($request_arr) && array_key_exists("ws_log", $request_arr)) {
            unset($request_arr['ws_log']);
        }
        if (is_array($request_arr) && array_key_exists("no_cache", $request_arr)) {
            unset($request_arr['no_cache']);
        }
        $this->CI->load->library('validator');
        try {
            $input_params = array();
            if (is_array($_FILES) && count($_FILES) > 0) {
                foreach ($_FILES as $fKey => $fVal) {
                    if (is_array($request_arr) && !array_key_exists($fKey, $request_arr)) {
                        $request_arr[$fKey] = $_FILES[$fKey]['name'];
                    }
                }
            }
            foreach ((array) $param_array as $prKey => $prVal) {
                if (is_array($prVal) && count($prVal) > 0) {
                    $validRuleArr = $validaData = $ruleArr = $msgArr = array();
                    foreach ((array) $prVal as $ruKey => $ruVal) {
                        if ($ruVal['rule'] != "regex") {
                            $ruleArr[$ruVal['rule']] = $ruVal['value'];
                            $msgArr[$ruVal['rule']] = $ruVal['message_code'];
                        } else {
                            $ruleArr['regex'][] = $ruVal['value'];
                            $msgArr['regex'][] = $ruVal['message_code'];
                        }
                        $validRuleArr['rules'][$prKey] = $ruleArr;
                        $validRuleArr['messages'][$prKey] = $msgArr;
                    }
                    $validator_php = new validator($validRuleArr);
                    $validaData[$prKey] = $request_arr[$prKey];

                    $responseArr = $validator_php->validate($validaData);

                    if (is_array($responseArr) && count($responseArr) > 0) {
                        $msg_code = $responseArr[$prKey];
                        $send_msg = $this->getWSLanguageMessage($msg_code, $ws_func, $message_arr, "WebService", $request_arr);
                        $defaultMsg = ($send_msg != "") ? $send_msg : "Please enter valid data for " . $prKey . "";
                        if (strstr($defaultMsg, '#FIELD#') !== false) {
                            $defaultMsg = str_replace('#FIELD#', $prKey, $defaultMsg);
                        }
                        throw new Exception($defaultMsg);
                    } else {
                        $input_params[$prKey] = $request_arr[$prKey];
                    }
                } else {
                    $input_params[$prKey] = $request_arr[$prKey];
                }
            }
            //get the remaining get vars as well
            $rem_input = $request_arr;
            if (is_array($rem_input)) {
                $remaining_array = array_diff_assoc($rem_input, $input_params);
                $input_params = array_merge($input_params, $remaining_array);
            }
            $returnArr['success'] = 1;
            $returnArr['input_params'] = $input_params;
        } catch (Exception $e) {
            $returnArr['success'] = "-5";
            $returnArr['message'] = $e->getMessage();
        }
        return $returnArr;
    }

    function makeValidationResponse($res_arr = array())
    {
        if ($res_arr['success'] == "-5") {
            $res_arr['success'] = "0";
        }
        $settings_arr['success'] = (string) $res_arr['success'];
        $settings_arr['message'] = $res_arr['message'];
        $settings_arr['fields'] = array();
        $final_arr['settings'] = $settings_arr;
        $final_arr['data'] = array();
        return $final_arr;
    }

    function sendValidationResponse($res_arr = array())
    {
        if ($res_arr['success'] == "-5") {
            $res_arr['success'] = "0";
        }
        $settings_arr['success'] = (string) $res_arr['success'];
        $settings_arr['message'] = $res_arr['message'];
        $settings_arr['fields'] = array();
        $final_arr['settings'] = $settings_arr;
        $final_arr['data'] = array();
        $this->sendWSResponse($final_arr);
    }

    function getWSLanguageMessage($msg_code = "", $ws_func = '', $message_arr = array(), $type = '', $params = array())
    {
        $lang_code = $this->CI->general->getLangRequestValue();
        $send_msg = '';
        if (is_array($message_arr) && count($message_arr) > 0) {
            foreach ($message_arr as $key => $val) {
                if ($val['eMessageType'] == $type && $val['vMessageCode'] == $msg_code && $val['vLangCode'] == $lang_code) {
                    $send_msg = $val['tMessage'];
                    break;
                }
            }
        }
        if ($send_msg == "") {
            $lang_folder = strtolower($lang_code);
            if (file_exists(APPPATH . "language" . DS . $lang_folder . DS . "webservice_lang.php")) {
                $this->CI->lang->load('webservice', $lang_folder);
            } else {
                $this->CI->lang->load('webservice', "en");
            }
            $ws_msg_arr = $this->CI->lang->line($ws_func);
            $send_msg = $ws_msg_arr[$msg_code];
        }
        if (strstr($send_msg, "#")) {
            $send_msg = $this->CI->general->getReplacedInputParams($send_msg, $params);
        }
        if (strstr($send_msg, "{%REQUEST")) {
            $send_msg = $this->CI->general->processRequestPregMatch($send_msg, $params);
        }
        if (strstr($send_msg, "{%SERVER")) {
            $send_msg = $this->CI->general->processServerPregMatch($send_msg, $params);
        }
        if (strstr($send_msg, "{%SYSTEM")) {
            $send_msg = $this->CI->general->processSystemPregMatch($send_msg, $params);
        }
        $send_msg = stripslashes($send_msg);
        return $send_msg;
    }

    function filterNullValues($tmp_arr)
    {
        if (is_array($tmp_arr)) {
            foreach ($tmp_arr as $key => $val) {
                $tmp_arr[$key] = $this->filterNullValues($val);
            }
            return $tmp_arr;
        } else {
            if (is_null($tmp_arr)) {
                return '';
            } else {
                return (string) $tmp_arr;
            }
        }
    }

    function sendWSResponse($arr = array(), $debug = array())
    {
        $ws_debug = $this->CI->input->get_post("ws_debug", true);
        $arr = $this->filterNullValues($arr);
        if ($ws_debug == 1 && $_ENV['debug_action']) {
            $arr['queries'] = $this->CI->general->getDBQueriesList();
            if (!is_null($this->CI->input->get_post("ws_ctrls"))) {
                $arr['debug'] = $debug;
            }
        }
        $response_type = "json";
        switch ($response_type) {
            case 'xml':
                $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><response></response>");
                $this->array_to_xml($arr, $xml);
                $ret = $xml->asXML();
                break;
            case 'json':
            default:
                $ret = $this->json_safe_encode($arr);
                break;
        }
        if ($this->CI->config->item("MULTI_LINGUAL_PROJECT") == "Yes") {
            header('Content-Type: text/html; charset=utf-8');
        } else {
            header('Content-Type: application/json; charset=utf-8');
        }
        if ($this->CI->config->item('WS_RESPONSE_ENCRYPTION') == "Y") {
            $this->CI->load->library('wschecker');
            $ret = $this->CI->wschecker->encryptData($ret);
        }
        echo $ret;
        exit;
    }

    function json_safe_encode($var)
    {
        if ((version_compare(PHP_VERSION, '5.4.0') >= 0) && $this->CI->input->get_post("ws_debug", true) == '1') {
            $encode_str = json_encode($this->json_fix_cyr($var), JSON_PRETTY_PRINT);
        } else {
            $encode_str = json_encode($this->json_fix_cyr($var));
        }
        return $encode_str;
    }

    function json_fix_cyr($var)
    {
        if (is_array($var)) {
            $new = array();
            foreach ($var as $k => $v) {
                $new[$this->json_fix_cyr($k)] = $this->json_fix_cyr($v);
            }
            $var = $new;
        } elseif (is_object($var)) {
            $vars = get_object_vars($var);
            foreach ($vars as $m => $v) {
                $var->$m = $this->json_fix_cyr($v);
            }
        } elseif (is_string($var)) {
            if (preg_match('!!u', $var)) {
                $var = $var;
            } else {
                $var = utf8_encode($var);
            }
        }
        return $var;
    }

    function array_to_xml($student_info = array(), &$xml_student_info)
    {
        foreach ($student_info as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml_student_info->addChild("row");
                    $this->array_to_xml($value, $subnode);
                } else {
                    $this->array_to_xml($value, $xml_student_info);
                }
            } else {
                $xml_student_info->addChild("$key", "$value");
            }
        }
    }

    function pushDebugParams($key = '', $arr = array(), $tot = array(), $next = '', $start_loop = '', $end_loop = '')
    {
        if (is_null($this->CI->input->get_post("ws_debug")) || is_null($this->CI->input->get_post("ws_ctrls"))) {
            return;
        }
        $this->ws_debug_params['ws_ctrls'][] = $key;
        if (!array_key_exists($key, $this->ws_debug_params)) {
            $this->ws_debug_params[$key] = $arr;
        } else if ($start_loop != '') {
            if (is_array($this->ws_debug_params[$key])) {
                $this->ws_debug_params[$key] = array_merge($this->ws_debug_params[$key], $arr);
            } else {
                $this->ws_debug_params[$key] = $arr;
            }
        }
        $ctrls = $this->CI->input->get_post("ws_ctrls");
        $ctrls = ($ctrls) ? explode(",", $ctrls) : $ctrls;
        if (is_array($ctrls) && count($ctrls) > 0) {
            $_log_arr['flow_name'] = $key;
            $_log_arr['next_flow'] = $next;
            $_log_arr['start_loop'] = $start_loop;
            $_log_arr['end_loop'] = $end_loop;
            $_log_arr['params'] = $tot;
            $_log_arr['debug'] = $this->ws_debug_params;
            $fp = @fopen($this->CI->config->item('api_debugger_path') . $this->ws_log_file, 'w+');
            @fwrite($fp, serialize($_log_arr));
            @fclose($fp);
        }
        if (is_array($ctrls) && in_array($key, $ctrls)) {
            $output_debug_params = $this->ws_debug_params;
            unset($output_debug_params['ws_ctrls']);
            $this->ws_debug_params['ws_log'] = $this->ws_log_file;
            $this->sendWSResponse($output_debug_params, $this->ws_debug_params);
        }
    }
}

/* End of file Wsresponse.php */
/* Location: ./application/libraries/Wsresponse.php */