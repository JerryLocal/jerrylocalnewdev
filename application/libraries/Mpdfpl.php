<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpdfpl{
    
    private $CI;
    
    function __construct(){
        $this->CI = & get_instance();
    }
    
    public function download_pdf($htmData, $fileName = "downloadFile", $option = "D"){
        include_once("MPDF44/mpdf.php");
        
        $mpdf=new mPDF();
        $mpdf->AddPage();
        $mpdf->WriteHTML($htmData);
        $mpdf->Output($fileName.".pdf",$option);
	}
        
        function generate_pdf($strContent,$attach_file_name,$id=''){
            
            set_time_limit(0);
            
            ini_set("memory_limit","3000M");
            #ini_set('display_errors',1);
            #ini_set('display_startup_errors',1);
            #error_reporting(-1);
            include_once("MPDF44/mpdf.php");
            $mpdf=new mPDF();
            
            $mpdf->AddPage();
            //$mpdf->debug = true;
            //$mpdf->allow_output_buffering = true;
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->WriteHTML($strContent);
            
            if(is_dir($this->CI->config->item('temppdf_file_path').$id)){
               // $this->CI->general->deleteFolder($this->CI->config->item('temppdf_file_path').$id);
            }
            
            //@mkdir($this->CI->config->item('temppdf_file_path').$id, 0777);
            $this->createUploadFolderIfNotExists('invoice_pdf',true, $id);
            
            @chmod($this->CI->config->item('temppdf_file_path').$id .'/', 0777);
            $pdffile = $mpdf->Output($this->CI->config->item('temppdf_file_path').$id.'/'.$attach_file_name.'.pdf','F');
            @chmod($pdffile, 0777);
            return $pdffile;

    }
    
    function createUploadFolderIfNotExists($folder_name = '', $folderIdWise = false, $id = 0) {
        if ($folder_name == "") {
            return false;
        }
        $upload_folder = $this->CI->config->item('upload_path') . $folder_name . DS;
        if (!is_dir($upload_folder)) {
            $oldUmask = umask(0);
            $res = @mkdir($upload_folder, 0777);
            @chmod($upload_folder, 0777);
            umask($oldUmask);
        }
        if ($folderIdWise && $id > 0) {
            $upload_folder_idWise = $this->CI->config->item('upload_path') . $folder_name . DS . $id . DS;
            if (!is_dir($upload_folder_idWise)) {
                $oldUmask = umask(0);
                $res = @mkdir($upload_folder_idWise, 0777);
                @chmod($upload_folder_idWise, 0777);
                umask($oldUmask);
            }
        }
        return true;
    }

}   

