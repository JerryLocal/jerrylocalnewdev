<?php

defined('BASEPATH') OR exit('No direct script access allowed');

#####GENERATED_CONFIG_SETTINGS_START#####

$config["add_address"] = array(
    "title" => "addAddress",
    "params" => array(
        "vAddress1",
        "vAddress2",
        "vArea",
        "iCountryId",
        "iStateId",
        "iCityId",
        "vPinCode",
        "vPhone",
        "iAdminId",
        "vName"
    )
);
$config["cancel_order"] = array(
    "title" => "cancelOrder",
    "params" => array(
        "oid",
        "tDetail",
        "iCancelId"
    )
);
$config["deal_of_the_day"] = array(
    "title" => "dealOfTheDay",
    "params" => array(
    )
);
$config["deal_of_the_day_by_price"] = array(
    "title" => "dealOfTheDayByPrice",
    "params" => array(
        "start_price",
        "end_price",
        "sort_by",
        "price_by"
    )
);
$config["del_wishlist"] = array(
    "title" => "delWishlist",
    "params" => array(
        "iAdminId",
        "iProductId"
    )
);
$config["email_invoice"] = array(
    "title" => "EmailInvoice",
    "params" => array(
        "oid"
    )
);
$config["get_banners"] = array(
    "title" => "getBanners",
    "params" => array(
    )
);
$config["get_category_block_filter"] = array(
    "title" => "GetCategoryBlock_Filter",
    "params" => array(
        "cid"
    )
);
$config["get_country"] = array(
    "title" => "getCountry",
    "params" => array(
    )
);
$config["get_country_state"] = array(
    "title" => "getCountryState",
    "params" => array(
        "countryid",
        "stateid"
    )
);
$config["get_customer_address"] = array(
    "title" => "getCustomerAddress",
    "params" => array(
        "iCustomerId"
    )
);
$config["get_deal_of_day"] = array(
    "title" => "getDealOfDay",
    "params" => array(
    )
);
$config["get_faq"] = array(
    "title" => "getFaq",
    "params" => array(
        "Type"
    )
);
$config["get_feature_product"] = array(
    "title" => "getFeatureProduct",
    "params" => array(
        "limit"
    )
);
$config["get_filter_data"] = array(
    "title" => "getFilterData",
    "params" => array(
        "catId"
    )
);
$config["get_home_category"] = array(
    "title" => "getHomeCategory",
    "params" => array(
    )
);
$config["get_individual_product"] = array(
    "title" => "getIndividualProduct",
    "params" => array(
        "iProductId"
    )
);
$config["get_jobs"] = array(
    "title" => "getJobs",
    "params" => array(
    )
);
$config["get_left_filters"] = array(
    "title" => "GetLeftFilters",
    "params" => array(
        "catId"
    )
);
$config["get_menu"] = array(
    "title" => "getMenu",
    "params" => array(
    )
);
$config["get_menu1"] = array(
    "title" => "getMenu1",
    "params" => array(
        "MenuName"
    )
);
$config["get_menu1_v1"] = array(
    "title" => "getMenu1_v1",
    "params" => array(
        "MenuName"
    )
);
$config["get_menu_tree"] = array(
    "title" => "getMenuTree",
    "params" => array(
    )
);
$config["get_news"] = array(
    "title" => "getNews",
    "params" => array(
        "dStartDate",
        "dEndDate"
    )
);
$config["get_news_by_date"] = array(
    "title" => "getNewsByDate",
    "params" => array(
        "dStartDate",
        "dEndDate"
    )
);
$config["get_news_detail"] = array(
    "title" => "getNewsDetail",
    "params" => array(
        "iNewsId"
    )
);
$config["get_order_detail"] = array(
    "title" => "getOrderDetail",
    "params" => array(
        "oid"
    )
);
$config["get_order_detail_v1"] = array(
    "title" => "getOrderDetail_v1",
    "params" => array(
        "iBuyerId",
        "oid"
    )
);
$config["get_popular_products"] = array(
    "title" => "getPopularProducts",
    "params" => array(
    )
);
$config["get_product"] = array(
    "title" => "getProduct",
    "params" => array(
        "cid",
        "pids",
        "sort_id",
        "price_id"
    )
);
$config["get_product_by_search"] = array(
    "title" => "getProductBySearch",
    "params" => array(
        "cid",
        "iSearchId",
        "iPriceId",
        "start_price",
        "end_price"
    )
);
$config["get_product_category"] = array(
    "title" => "getProductCategory",
    "params" => array(
    )
);
$config["get_product_images"] = array(
    "title" => "getProductImages",
    "params" => array(
        "pid"
    )
);
$config["get_product_review"] = array(
    "title" => "getProductReview",
    "params" => array(
        "iProductID"
    )
);
$config["get_related_product"] = array(
    "title" => "getRelatedProduct",
    "params" => array(
        "iProductId"
    )
);
$config["get_return_order_detail"] = array(
    "title" => "getReturnOrderDetail",
    "params" => array(
        "iOrderId"
    )
);
$config["get_sales_statistics"] = array(
    "title" => "getSalesStatistics",
    "params" => array(
    )
);
$config["get_sitemap"] = array(
    "title" => "GetSitemap",
    "params" => array(
    )
);
$config["get_state_city"] = array(
    "title" => "getStateCity",
    "params" => array(
        "state_id",
        "city_id"
    )
);
$config["get_store_avg_block"] = array(
    "title" => "getStoreAvgBlock",
    "params" => array(
        "iStoreId"
    )
);
$config["get_store_data"] = array(
    "title" => "getStoreData",
    "params" => array(
        "iStoreId"
    )
);
$config["get_store_product"] = array(
    "title" => "getStoreProduct",
    "params" => array(
        "iStoreId"
    )
);
$config["get_store_rating"] = array(
    "title" => "getStoreRating",
    "params" => array(
        "iStoreId"
    )
);
$config["get_website_statastic"] = array(
    "title" => "getWebsiteStatastic",
    "params" => array(
    )
);
$config["get_wishlist"] = array(
    "title" => "getWishlist",
    "params" => array(
        "iAdminID"
    )
);
$config["ins_product_trend"] = array(
    "title" => "insProductTrend",
    "params" => array(
        "pid",
        "ip"
    )
);
$config["invite_friend"] = array(
    "title" => "InviteFriend",
    "params" => array(
        "ToEmail",
        "FromEmail",
        "Subject",
        "SiteUrl"
    )
);
$config["my_order"] = array(
    "title" => "my_order",
    "params" => array(
        "vBuyerEmail"
    )
);
$config["notification"] = array(
    "title" => "notification",
    "params" => array(
        "iUserId"
    )
);
$config["order_detail"] = array(
    "title" => "orderDetail",
    "params" => array(
        "iMstOrderId"
    )
);
$config["place_order"] = array(
    "title" => "placeOrder",
    "params" => array(
        "iBuyerId",
        "fShippingCost",
        "fSubTotal",
        "fOrderTotal",
        "ePaymentStatus",
        "dDate",
        "vBuyerEmail",
        "vBuyerIp",
        "vBuyerName",
        "vBuyerAddress1",
        "vBuyerAddress2",
        "vBuyerArea",
        "iBuyerCountryId",
        "iBuyerCityId",
        "iBuyerStateId",
        "vBuyerPinCode",
        "vBuyerPhone",
        "order_item",
        "tPaymentDetail",
        "vBuyerCountry",
        "vBuyerState",
        "vBuyerCity"
    )
);
$config["product_invoice"] = array(
    "title" => "product_invoice",
    "params" => array(
        "oid"
    )
);
$config["quicksearch"] = array(
    "title" => "quicksearch",
    "params" => array(
        "vDetail"
    )
);
$config["quicksearch_inner"] = array(
    "title" => "quicksearchInner",
    "params" => array(
        "vDetail"
    )
);
$config["quicksearch_inner_by_sort"] = array(
    "title" => "quicksearchInnerBySort",
    "params" => array(
        "vDetail",
        "start_price",
        "end_price",
        "sort_by",
        "price_by"
    )
);
$config["return_order"] = array(
    "title" => "returnOrder",
    "params" => array(
        "iOrderId",
        "vImgName",
        "tRequestDetail",
        "ePreference"
    )
);
$config["search_products"] = array(
    "title" => "searchProducts",
    "params" => array(
        "pids",
        "startPrice",
        "endPrice",
        "cids",
        "sort_id",
        "price_id"
    )
);
$config["send_email_to_friend"] = array(
    "title" => "sendEmailToFriend",
    "params" => array(
        "ToEmailId",
        "ToName",
        "FromEmail",
        "FromName",
        "Subject",
        "ProductName",
        "productUrl",
        "json_str",
        ""
    )
);
$config["set_news_letter"] = array(
    "title" => "SetNewsLetter",
    "params" => array(
        "iGroupID",
        "eStatus",
        "iUserID",
        "vEmail",
        "vName"
    )
);
$config["set_product_review"] = array(
    "title" => "setProductReview",
    "params" => array(
        "iMstProductsId",
        "iLogedUserId",
        "vName",
        "vEmail",
        "iRate",
        "tReview"
    )
);
$config["set_store_rating"] = array(
    "title" => "setStoreRating",
    "params" => array(
        "storeid",
        "name",
        "description",
        "email",
        "loginuserid",
        "rating",
        "iSellerId"
    )
);
$config["set_wishlist"] = array(
    "title" => "setWishlist",
    "params" => array(
        "iAdminId",
        "iProductId"
    )
);
$config["signin"] = array(
    "title" => "signin",
    "params" => array(
        "email",
        "password"
    )
);
$config["signup"] = array(
    "title" => "signup",
    "params" => array(
        "firstname",
        "lastname",
        "email",
        "password"
    )
);
$config["test"] = array(
    "title" => "test",
    "params" => array(
        "categoryId"
    )
);
$config["test1"] = array(
    "title" => "test1",
    "params" => array(
    )
);
$config["track_order"] = array(
    "title" => "trackOrder",
    "params" => array(
        "iOrderId",
        "vEmailId"
    )
);
$config["validate_product_for_cart"] = array(
    "title" => "validate product for cart",
    "params" => array(
        "iProductId",
        "iQuantity",
        "vOption"
    )
);
$config["view_counter"] = array(
    "title" => "viewCounter",
    "params" => array(
        "pid"
    )
);#####GENERATED_CONFIG_SETTINGS_END#####
/* End of file hb_webservices.php */
/* Location: ./application/config/hb_webservices.php */
    