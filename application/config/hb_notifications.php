<?php

defined('BASEPATH') OR exit('No direct script access allowed');

#####GENERATED_CONFIG_SETTINGS_START#####

$config["contact_us"] = array(
    "title" => "Contact US",
    "type" => "Operation",
    "table" => "trn_messages",
    "operations" => array("Insert")
);
$config["news_letter_for_campaign"] = array(
    "title" => "News Letter For Campaign",
    "type" => "Time",
    "start_date" => "",
    "end_date" => "",
    "cron_format" => "0,30 * * * *",
    "status" => "Active"
);
$config["newsletter_subscription"] = array(
    "title" => "Newsletter Subscription",
    "type" => "Operation",
    "table" => "mst_newsletter_subscription",
    "operations" => array("Insert")
);
$config["return_request_update"] = array(
    "title" => "Return request Update",
    "type" => "Operation",
    "table" => "trn_rma",
    "operations" => array("Update")
);
$config["rma_add"] = array(
    "title" => "Rma Add",
    "type" => "Operation",
    "table" => "trn_rma",
    "operations" => array("Insert")
);
$config["selleraccount"] = array(
    "title" => "Seller Account Registration",
    "type" => "Operation",
    "table" => "mst_store_detail",
    "operations" => array("Insert")
);
$config["selleraccount_v1"] = array(
    "title" => "Seller Approval",
    "type" => "Operation",
    "table" => "mst_store_detail",
    "operations" => array("Update")
);
$config["sub_order_add"] = array(
    "title" => "Sub Order Add",
    "type" => "Operation",
    "table" => "mst_sub_order",
    "operations" => array("Insert")
);
$config["sub_order_updated"] = array(
    "title" => "Sub Order Updated",
    "type" => "Operation",
    "table" => "mst_sub_order",
    "operations" => array("Update")
);#####GENERATED_CONFIG_SETTINGS_END#####

/* End of file hb_notifications.php */
/* Location: ./application/config/hb_notifications.php */
    