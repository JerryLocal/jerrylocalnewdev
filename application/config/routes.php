<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = "home/home/index";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['admin'] = "dashboard/dashboard/sitemap";
$route['admin/(:any)'] = "$1";

$route['user.html'] = "user/user/index";
$route['index.html'] = "content/content/index";
$route['signup.html'] = "user/user/register";
$route['profile.html'] = "user/user/profile";
$route['login.html'] = "user/user/login";
$route['logout.html'] = "user/user/logout";
$route['dashboard.html'] = "user/user/dashboard";
$route['forgot-password.html'] = "user/user/forgotpassword";
$route['forgotme.html'] = "user/user/forgotme";
$route['invite-friend.html'] = "user/user/inviteFriend";
$route['track-orders.html'] = "order/order/TrackOrder";
$route['my-orders.html'] = "order/order/myOrder";
$route['order-details.html'] = "order/order/OrderDetail";
$route['return-order.html'] = "order/order/ReturnOrder";
$route['order-cancel.html'] = "order/order/cancelOrder";
$route['invoice.html'] = "order/order/Invoice";
$route['notifications.html'] = "user/user/notification";

$route['unsubscribe-newsletter.html'] = "user/user/unsubscribe_newsletter";


// dynamic pages
$route['faq.html'] = "faq/faq/index";
$route['seller/faq.html'] = "faq/faq/sellerFAQ";
$route['wishlist.html'] = "wishlist/wishlist/index";
$route['(:any)/mail-to-friend.html'] = "wishlist/wishlist/sendtofriend/$i";
$route['news.html'] = "news/news/index";
$route['news-detail-([0-9]+).html'] = "news/news/newsdetail/$1";
$route['contact.html'] = "contactus/contactus/index";
$route['career.html'] = "career/career/career";
// $route['sitemap.html'] = "sitemap/sitemap/index";
$route['sitemap.html'] = "sitemap/sitemap/sitemaplist";
$route['seller-zone.html'] = "sellerregistration/sellerregistration/index";
$route['seller-zone-2.html'] = "sellerregistration/sellerregistration/setUserData";
$route['seller-zone-3.html'] = "sellerregistration/sellerregistration/setStoreDetail";
$route['seller-zone-4.html'] = "sellerregistration/sellerregistration/setBankDetail";
$route['p/([0-9]+)/(:any).html'] = "productlist/productlist/productDetail/$1/$1/$1";
$route['store/(:any)/about.html'] = "store/store/AboutStore";
$route['store/(:any)/return-policy.html'] = "store/store/ReturnPolicy";
$route['store/(:any)/products.html'] = "store/store/otherproduct";
$route['store/(:any).html'] = "store/store/index/$1";
//$route['store/([0-9]+)/(:any).html'] = "store/store/index/$1";
$route['store-review.html'] = "store/store/StoreReview";
$route['search.html'] = "productlist/productlist/searchResult";
$route['deal-items.html'] = "productlist/productlist/dealOfTheDay";

// Added by Sarvil Ajwaliya
$route['([a-z]+)/([0-9]+)/(:any).html'] = "productlist/productlist/index/$1/$1/$1";
$route['review.html'] = "productlist/productlist/ProductReview";
$route['change-password.html'] = "user/user/change_password";
$route['my-address.html'] = "user/user/my_address";
$route['(:any)/store_contact.html'] = "contactus/contactus/contact_seller/$i";
//$route['([0-9]+)/store_contact.html'] = "contactus/contactus/contact_seller/$i";



// webservices
$route['WS'] = "wsengine/wscontroller/listWSMethods";
$route['WS/(:any)'] = "wsengine/wscontroller/WSExecuter/$1";
$route['WS/execute'] = "rest/restcontroller/execute_notify_schedule";
$route['WS/image_resize'] = "rest/restcontroller/image_resize";
$route['WS/create_token'] = "rest/restcontroller/create_token";
$route['WS/inactive_token'] = "rest/restcontroller/inactive_token";
$route['WS/get_push_notification'] = "rest/restcontroller/get_push_notification";

// notifications
$route['NS'] = "nsengine/notifycontroller/listNSMethods";
$route['NS/(:any)'] = "nsengine/notifycontroller/notifyExecuter/$1";
$route['NS/execute'] = "nsengine/notifycontroller/executeNotifySchedule";

$route['content/(:any)'] = "content/content/staticpage/$1";
$route['content/(:any)/(:any)'] = "content/content/staticpage/$1/$2";

/**
 * Loading Some of the front routes file to specify custom key-values
 * 
 */
require_once 'routes_front.php';

// Database initialization
$db = & HB_DB();
if ($db === FALSE) {
    //redirecting to installation page
    if (is_dir(dirname(BASEPATH) . DS . "installer")) {
        $site_installer_url = (is_https() ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']) . 'installer/';
        header("location:" . $site_installer_url);
        exit;
    } else {
        show_error('No database connection settings were found in the database config file.');
    }
}

$static_pages_obj = $db->select("vPageCode, vUrl, vPageTitle")->where('eStatus', 'Active')->get('mod_page_settings');
$static_pages = is_object($static_pages_obj) ? $static_pages_obj->result_array() : array();
foreach ($static_pages as $i => $route_arr) {
    $route[$route_arr['vUrl']] = "content/content/staticpage/" . $route_arr['vPageCode'];
}
if ($this->config->item('is_admin') == 1) {
    $db->select("vName, vValue");
    $db->where("eStatus", 'Active');
    $db->where_in("vName", array('ADMIN_URL_ENCRYPTION','ADMIN_ENC_KEY'));
    $uri_enc_router = $db->select_assoc("mod_setting", "vName");
    $this->config->set_item("ADMIN_ENC_KEY", $uri_enc_router['ADMIN_ENC_KEY'][0]['vValue']);
    $this->config->set_item("ADMIN_URL_ENCRYPTION", $uri_enc_router['ADMIN_URL_ENCRYPTION'][0]['vValue']);
}

/* End of file routes.php */
/* Location: ./application/config/routes.php */