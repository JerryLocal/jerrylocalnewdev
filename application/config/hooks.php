<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | Hooks
  | -------------------------------------------------------------------------
  | This file lets you define "hooks" to extend CI without hacking the core
  | files.  Please see the user guide for info:
  |
  |	http://codeigniter.com/user_guide/general/hooks.html
  |
 */

if (isset($_ENV['access_log']) && $_ENV['access_log'] == 1) {
    $hook['pre_controller'][] = array(
        'class' => 'AccessLogHook',
        'function' => 'http_request_log',
        'filename' => 'AccessLogHook.php',
        'filepath' => 'hooks',
        'params' => array(
            "admin" => false,
            "front" => false,
            "webservice" => true,
            "notification" => false,
            "admin_system_calls" => array(
                "user" => array(
                    "login" => array("sess_expire", "manifest", "tbcontent", "notify_events")
                )
            ),
            "folder_date_format" => "Y-m-d",
            "file_name" => "log",
            "file_extension" => "txt",
            "log_date_format" => "Y-m-d H:i:s"
        )
    );
}
$hook['pre_controller'][] = array(
    'class'     => 'HBCExtentionHook',
    'function'  => 'init',
    'filename'  => 'HBCExtentionHook.php',
    'filepath'  => 'hooks',
);
$hook['post_controller'][] = array(
    'class' => 'FinalControllerHook',
    'function' => 'final_controller_actions',
    'filename' => 'FinalControllerHook.php',
    'filepath' => 'hooks'
);
$hook['post_system'][] = array(
    'class' => 'FinalSystemHook',
    'function' => 'final_actions',
    'filename' => 'FinalSystemHook.php',
    'filepath' => 'hooks'
);
/* End of file hooks.php */
/* Location: ./application/config/hooks.php */