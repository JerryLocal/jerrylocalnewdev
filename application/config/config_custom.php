<?php
defined('BASEPATH') OR exit('No direct script access allowed');

#####GENERATED_CONFIG_SETTINGS_START#####

//Physical record delete activate flag
$config["PHYSICAL_RECORD_DELETE"] = true;                    
//DB record limit - to avoid hitting the database server from large data sets fetching.
$config['db_max_limit'] = 0;
//CDN server details - For more info, please read this file ../public/cdn/readme.txt.
$config['cdn_activate'] = false;
$config['cdn_http_url'] = '';
#####GENERATED_CONFIG_SETTINGS_END#####

/* End of file config_custom.php */
/* Location: ./application/config/config_custom.php */