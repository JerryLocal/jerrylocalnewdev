<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of AccessLogHook Controller
 *
 * @author Simhachalam G
 */

class AccessLogHook {

    function __construct() {
        $this->CI = & get_instance();
        $this->params = array();
        $this->_data_max_size = 1 * 1024 * 1024 * 5; // 5MB
    }

    function http_request_log($params = array()) {
        
        $this->params = $params;

        if ($this->CI->config->item("is_admin") == 1) {
            if (!$this->params['admin']) {
                return false;
            }
            if ($this->CI->uri->segments[2] == '') {
                return false;
            }
            if (is_array($this->params['admin_system_calls'])) {
                $system_calls = $this->params['admin_system_calls'];
                $ctrl_arr = $system_calls[$this->CI->uri->segments[2]][$this->CI->uri->segments[3]];
                if (is_array($ctrl_arr) && in_array($this->CI->uri->segments[4], $ctrl_arr)) {
                    return false;
                }
            }
            $type = "Admin";
            $request_func = '';
            if ($this->CI->uri->segments[2]) {
                $request_func = $this->CI->uri->segments[2] . "/" . $this->CI->uri->segments[3] . "/" . $this->CI->uri->segments[4];
            }
            $file_suffix = "admin";
        } else if (in_array($this->CI->uri->segments[1], array("WS", 'webservice'))) {
            if (!$this->params['webservice']) {
                return false;
            }
            if ($this->CI->uri->segments[2] == "") {
                return false;
            }
            $type = "Webservice";
            $request_func = $this->CI->uri->segments[2];
            $file_suffix = "ws";
        } else if (in_array($this->CI->uri->segments[1], array("NS", 'notification'))) {
            if (!$this->params['notification']) {
                return false;
            }
            if ($this->CI->uri->segments[2] == "") {
                return false;
            }
            $type = "Notification";
            $request_func = $this->CI->uri->segments[2];
            $file_suffix = "ns";
        } else {
            if (!$this->params['front']) {
                return false;
            }
            $type = "Front";
            $request_func = $this->CI->uri->segments[1];
            $file_suffix = "front";
        }
        $this->params['type'] = $type;
        $this->params['function'] = $request_func;

        $access_log_folder = $this->CI->config->item('admin_access_log_path');
        if (!is_dir($access_log_folder)) {
            $this->CI->general->createFolder($access_log_folder);
        }

        $log_date_format = $this->params['folder_date_format'];
        if (!$log_date_format) {
            $log_date_format = "Y-m-d";
        }
        $log_folder_name = date($log_date_format);
        
        $log_folder_path = $access_log_folder . $log_folder_name . DS;
        if (!is_dir($log_folder_path)) {
            $this->CI->general->createFolder($log_folder_path);
        }

        $log_file_name = $this->params['file_name'];
        if (!$log_file_name) {
            $log_file_name = "log";
        }

        $final_file_name = $log_file_name . "-" . $file_suffix;
        if ($this->params['file_extension']) {
            $final_file_name .= "." . $this->params['file_extension'];
        }

        $log_file_path = $log_folder_path . $final_file_name;

        $request_log_data = $this->get_logging_data();

        $fp = @fopen($log_file_path, 'a+');
        @fwrite($fp, $request_log_data);
        @fclose($fp);
    }

    function get_logging_data() {
        $params_arr = array();
        $date_format = $this->params['log_date_format'];
        $request_type = $this->params['type'];
        $request_func = $this->params['function'];
        if (!$date_format) {
            $date_format = "Y-m-d H:i:s";
        }
        $date_str = date($date_format);
        $url = $this->CI->config->item("site_url") . $this->CI->uri->uri_string . "/";
        $ip_addr = $this->get_http_real_ip_addr();
        $user_agent = $this->get_http_user_agent();
        $plat_form = $this->get_platform($user_agent);
        $bowser = $this->get_browser($user_agent);
        $input_params_arr = $this->get_http_request_params();
        $input_params_str = (is_array($input_params_arr) && count($input_params_arr) > 0) ? serialize($input_params_arr) : "";

        $data_str = <<<EOD
{$ip_addr}~~{$request_func}~~{$request_type}~~{$date_str}~~{$url}~~{$user_agent}~~{$plat_form}~~{$bowser}~~{$input_params_str}

EOD;
        return $data_str;
    }

    public function get_http_real_ip_addr() {
        $ip = $this->CI->general->getHTTPRealIPAddr();
        return $ip;
    }

    public function get_http_user_agent() {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        return $user_agent;
    }

    function get_platform($user_agent = '') {

        $os_platform = "Unknown OS Platform";

        $os_array = array(
            '/windows nt 6.3/i' => 'Windows 8.1',
            '/windows nt 6.2/i' => 'Windows 8',
            '/windows nt 6.1/i' => 'Windows 7',
            '/windows nt 6.0/i' => 'Windows Vista',
            '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
            '/windows nt 5.1/i' => 'Windows XP',
            '/windows xp/i' => 'Windows XP',
            '/windows nt 5.0/i' => 'Windows 2000',
            '/windows me/i' => 'Windows ME',
            '/win98/i' => 'Windows 98',
            '/win95/i' => 'Windows 95',
            '/win16/i' => 'Windows 3.11',
            '/macintosh|mac os x/i' => 'Mac OS X',
            '/mac_powerpc/i' => 'Mac OS 9',
            '/linux/i' => 'Linux',
            '/ubuntu/i' => 'Ubuntu',
            '/iphone/i' => 'iPhone',
            '/ipod/i' => 'iPod',
            '/ipad/i' => 'iPad',
            '/android/i' => 'Android',
            '/blackberry/i' => 'BlackBerry',
            '/webos/i' => 'Mobile'
        );

        foreach ($os_array as $regex => $value) {

            if (preg_match($regex, $user_agent)) {
                $os_platform = $value;
            }
        }
        return $os_platform;
    }

    function get_browser($user_agent = '') {

        $browser = "Unknown Browser";

        $browser_array = array(
            '/msie/i' => 'Internet Explorer',
            '/firefox/i' => 'Firefox',
            '/safari/i' => 'Safari',
            '/chrome/i' => 'Chrome',
            '/opera/i' => 'Opera',
            '/netscape/i' => 'Netscape',
            '/maxthon/i' => 'Maxthon',
            '/konqueror/i' => 'Konqueror',
            '/mobile/i' => 'Handheld Browser'
        );

        foreach ($browser_array as $regex => $value) {

            if (preg_match($regex, $user_agent)) {
                $browser = $value;
            }
        }

        return $browser;
    }

    function get_http_request_params() {
        $get_arr = is_array($this->CI->input->get(null, true)) ? $this->CI->input->get(null, true) : array();
        $post_arr = is_array($this->CI->input->post(null, true)) ? $this->CI->input->post(null, true) : array();
        $post_params = array_merge($get_arr, $post_arr);

        return $post_params;
    }

    function is_data_exceeds_limit($data = '') {
        if (strlen($data) > $this->_data_max_size) {
            return true;
        }
        return false;
    }

}

/* End of file AccessLogHook.php */
/* Location: ./application/hooks/AccessLogHook.php */