<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Final Controller Hook
 *
 * @author Simhachalam G
 */
class FinalControllerHook {

    function __construct() {
        $this->CI = & get_instance();
    }

    function final_controller_actions() {
        if($this->CI->db->_error_found === true && isset($_ENV['debug_action']) && $_ENV['debug_action'] == 1){
            $efile = $this->db_errors();
            $this->CI->output->set_header("Cit-db-error: 1");
            $this->CI->output->set_header("Cit-db-efile: ".$efile);
        }
    }

    function db_errors() {
        $queries = $this->CI->db->_error_messages;
        $output = '';
        $ip = $this->CI->general->getHTTPRealIPAddr();
        if (count($queries) == 0) {
            $output .= "no db errors\n";
        } else {
             for ($i = count($queries) - 1; $i >= 0; $i--) {
                $query = "<div>".@implode("</div><div>", $queries[$i])."</div>";
                $output.= <<<EOD
                        
    <tr class="query-error">
        <td style="display:table-cell">
            {$query}
        </td>
        <td>{$remote_addr}</td>
    </tr>
EOD;
            }
        }
        
        $log_file_name = md5(time());
        $log_folder = $this->CI->config->item('query_error_path');
        if (!is_dir($log_folder)) {
            $this->CI->general->createFolder($log_folder);
        }
        $log_file_path = $log_folder . $log_file_name . ".html";
        
        $file_data = $output;
        if (@file_exists($log_file_path)) {
            $file_data .= @file_get_contents($log_file_path);
        }
        $fp = @fopen($log_file_path, 'w');
        @fwrite($fp, $file_data);
        @fclose($fp);
        
        return $log_file_name;
    }

}

/* End of file FinalControllerHook.php */
/* Location: ./application/hooks/FinalControllerHook.php */