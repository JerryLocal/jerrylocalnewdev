<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Final System Hook
 *
 * @author Simhachalam G
 */
class FinalSystemHook
{

    function __construct()
    {
        $this->CI = & get_instance();
    }

    function final_actions()
    {
        if ($this->CI->config->item('is_admin') == 1) {
            $this->CI->ci_local->complete($this->CI->session->userdata("iAdminId"));
        }
        if (isset($_ENV['debug_action']) && $_ENV['debug_action'] == 1) {
            $this->log_queries();
        }
    }

    function log_queries()
    {
        $times = $this->CI->db->query_times;
        $queries = $this->CI->db->queries;
        $output = '';
        $remote_addr = $this->CI->general->getHTTPRealIPAddr();
        if (count($queries) == 0) {
            $output .= "no queries\n";
        } else {
            $skip_arr = array(
                "SELECT CASE WHEN (@@OPTIONS | 256) = @@OPTIONS THEN 1 ELSE 0 END AS qi",
                "SELECT  TOP " . $this->CI->config->item("db_max_limit") . " " . $this->CI->db->protect("vName") . ", " . $this->CI->db->protect("vValue") . " FROM " . $this->CI->db->protect("mod_setting"),
                "SELECT " . $this->CI->db->protect('vName') . ", " . $this->CI->db->protect('vValue') . " FROM " . $this->CI->db->protect('mod_setting'),
                "SELECT * FROM " . $this->CI->db->protect('mod_language') . " WHERE " . $this->CI->db->protect('eStatus') . " = 'Active'",
                "SELECT " . $this->CI->db->protect("ms.*") . ", IF(" . $this->CI->db->protect("ms.eLang") . " = " . $this->CI->db->escape("Yes") . ", " . $this->CI->db->protect("msl.vValue") . ", " . $this->CI->db->protect("ms.vValue") . ") AS " . $this->CI->db->protect("lang_value") . " FROM " . $this->CI->db->protect("mod_setting") . " AS " . $this->CI->db->protect("ms"),
                "SELECT " . $this->CI->db->protect("vTableName") . ", " . $this->CI->db->protect("eExpireTime") . " FROM " . $this->CI->db->protect("mod_cache_tables"),
                "SELECT * FROM " . $this->CI->db->protect("mod_admin_menu") . " WHERE " . $this->CI->db->protect("iAdminMenuId") . " IN ",
                "SELECT " . $this->CI->db->protect("mgr.*") . " FROM " . $this->CI->db->protect("mod_group_rights") . " " . $this->CI->db->protect("mgr") . " JOIN " . $this->CI->db->protect("mod_admin_menu") . " " . $this->CI->db->protect("mam") . " ON " . $this->CI->db->protect("mam.iAdminMenuId") . " = " . $this->CI->db->protect("mgr.iAdminMenuId"),
                "SELECT " . $this->CI->db->protect("mgr.*") . " FROM " . $this->CI->db->protect("mod_group_rights") . " AS " . $this->CI->db->protect("mgr") . " LEFT JOIN " . $this->CI->db->protect("mod_admin_menu") . " AS " . $this->CI->db->protect("mam") . " ON " . $this->CI->db->protect("mam.iAdminMenuId") . " = " . $this->CI->db->protect("mgr.iAdminMenuId"),
                "SELECT " . $this->CI->db->protect("m.vMenuDisplay") . " AS " . $this->CI->db->protect("subMenu") . ", " . $this->CI->db->protect("m.vURL") . " AS " . $this->CI->db->protect("subURL") . ", " . $this->CI->db->protect("s.vMenuDisplay") . " AS " . $this->CI->db->protect("mainMenu") . ", " . $this->CI->db->protect("s.vURL") . " AS " . $this->CI->db->protect("mainURL") . " FROM " . $this->CI->db->protect("mod_admin_menu") . " AS " . $this->CI->db->protect("m"),
                "INSERT INTO " . $this->CI->db->protect("mod_admin_navigation_log"),
            );
            for ($i = count($queries) - 1; $i >= 0; $i--) {
                $query = $queries[$i];
                $query = str_replace(array("\n", "\r"), " ", $query);
                foreach ($skip_arr as $needle) {
                    if (strpos($query, $needle) === FALSE) {
                        continue 1;
                    } else {
                        continue 2;
                    }
                }

                if ($i % 2 == 0) {
                    $class = 'even';
                } else if (sprintf("%.3f", $times[$i] * 1000) / 1000 >= 2) {
                    $class = 'query-error';
                } else {
                    $class = 'odd';
                }

                $query_time = sprintf("%.3f", $times[$i] * 1000);
                $output.= <<<EOD
                        
    <tr class="{$class}">
        <td style="display:table-cell">
            {$query}
        </td>
        <td>{$query_time}</td>
        <td>{$remote_addr}</td>
    </tr>
EOD;
            }
        }

        if (empty($output)) {
            return;
        }

        $log_file_name = $this->CI->session->userdata('queryLogFile');
        $log_file_name = ($log_file_name != '') ? $log_file_name : md5($_SERVER['REQUEST_URI']);

        if ($this->CI->config->item('is_admin') === true) {
            $log_folder = $this->CI->config->item('admin_query_log_path');
        } else if (in_array($this->uri->segments[1], array("WS", 'webservice'))) {
            $log_folder = $this->CI->config->item('ws_query_log_path');
        } else if (in_array($this->uri->segments[1], array('rest', 'nsengine', 'notification'))) {
            $log_folder = $this->CI->config->item('ns_query_log_path');
        } else {
            $log_folder = $this->CI->config->item('front_query_log_path');
        }

        if (!is_dir($log_folder)) {
            $this->CI->general->createFolder($log_folder);
        }
        $log_file_path = $log_folder . $log_file_name . ".html";

        $file_data = $output;
        if (@file_exists($log_file_path)) {
            $file_data .= @file_get_contents($log_file_path);
        }
        $fp = @fopen($log_file_path, 'w');
        @fwrite($fp, $file_data);
        @fclose($fp);
    }
}

/* End of file FinalSystemHook.php */
/* Location: ./application/hooks/FinalSystemHook.php */