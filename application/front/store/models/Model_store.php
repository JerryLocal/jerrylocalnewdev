<?php
class Model_store extends CI_Model {

    private $primary_key;
    private $main_table;
    public $errorCode;
    public $errorMessage;

    public function __construct() {
        parent::__construct();
        $this->main_table = "mst_store_detail";        
    }
	
    function aboutStore($store_id) {        
        $this->db->select('vAboutStore,vStoreName,iMstStoreDetailId');
        $this->db->from($this->main_table);
        $this->db->where('iMstStoreDetailId',$store_id);
        $about_store= $this->db->get()->result_array();
        return $about_store;
    }
    function returnPolicy($store_id) {        
        $this->db->select('vReturnPolicy,vStoreName,iMstStoreDetailId');
        $this->db->from($this->main_table);
        $this->db->where('iMstStoreDetailId',$store_id);
        $returnpolicy= $this->db->get()->result_array();
        return $returnpolicy;
    }
}
?>