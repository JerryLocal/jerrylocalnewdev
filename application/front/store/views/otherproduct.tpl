<%$this->js->add_js("product-sort.js")%>
<%$this->css->add_css("jquery.mCustomScrollbar.css")%>
<%$this->css->css_src()%>
<%assign var=settings value=$product_arr['settings']%>
<!-- < %assign var=per_page value=$this->config->item('REC_LIMIT_FRONT')%> -->
<%assign var=per_page value=12%>
<%assign var=totalPage value= ceil($settings.count/$per_page)%>
<%assign var=tpages value= ceil($settings.count/$per_page)%>

<!-- < %$product_arr|@pr%> -->
<div class="main-containt">
  <div class="container">
    <div class="row-fluid">
      <%if $product_arr['settings']['success'] eq 1%>
      <div class="breadcrumbs" style="padding: 0 0px;">
        <div class="container">
          <div class="row-fluid col-sm-8 col-xs-2 col-md-6">
                  <%$this->general->Breadcrumb('CLEAN',$product_arr['data'][0].msd_store_name)%>
          </div>
          <div class="row-fluid">
                <span class="btn-back pull-right" style="margin-top:13px;">
                    <a href="<%$this->config->item('site_url')%>store/<%$this->uri->segment(2)%>/seller-page.html" class="your_store" id="your_store" style="cursor:pointer"><< Back To Store</a>
                </span>
          </div>
        </div>
      </div>
      <div class="product-list">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-main">
              <div id="catalog-listing">
                <div class="category-products page-product-list">
                  <div class="row sort_result">                     
                      <%if $product_arr['data']|@count gt 0%>
                      <%assign var=products value=$product_arr['data']%>
                      <%foreach $products as $product%>             
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item">  
                        <%$this->general->ProductBlockGenerator($product.mp_mst_products_id,$product.mp_title,$product.mp_regular_price,$product.mp_sale_price,$product.mp_rating_avg,$product.mp_rating_avg,$product.mp_stock,$product.mp_difference_per,$product.mp_date,$product.mp_wishlist_state,$product.mp_default_img)%>
                        </div>
                        <%/foreach%>                      
                      <%/if%>                    
                  </div>
                </div>
              </div>
              <div class="pagination-box">
                <div class="col-md-7">
                  <%assign var=displayResult value=($settings.curr_page*$per_page gt $settings.count) ? $settings.count : $settings.curr_page*$per_page%>
                  <div class="row">
                    <div class="pagination-list">Items <%(($settings.curr_page-1)*$per_page)+1%> to <%$displayResult%> of <%$settings.count%> total</div>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="row">
                    <ul class="pagination">
                      <%assign var=reload value=$this->config->item('site_url')|cat:"store/"|cat:$product_arr['data'][0].mp_store_id|cat:"/products.html?tpages="|cat:$tpages%>            
                        <%if $totalPage gt 1%>
                        <%$this->generalfront->paginate($reload,$product_arr['settings'].curr_page,$totalPage)%>
                        <%/if%>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <%else%>
        <div class="short-bar">
            <div class="col-md-12" style="text-align:center">                  
              <div class="row">Store is currently unavailable</div>
            </div>
        </div>
      <%/if%>
    </div>
  </div>
</div>