<%$this->js->add_js("review.js")%>
<div id="review"></div>

<div class="main-containt">
    <div class="container">
        <div class="row-fluid">
            <%$this->general->Breadcrumb('CLEAN','Write Review')%>
        </div></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="review-lft-section">
                    <h3><%$Store_arr.Store_name%></h3>
                    <div class="product-details">
                        <%if $Store_arr.Store_logo neq ''%>
                        <img src="<%$this->config->item('store_logo')%><%$Store_arr.Store_logo%>" alt="img" class="img-responsive">
                        <%else%>
                        <img src="<%$this->config->item('images_url')%>seller-logo.png" alt="img" class="img-responsive">
                        <%/if%>      
                        <strong><%$Store_arr.Store_name%></strong>
                        <p><%$Store_arr.Store_description%></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
                <div class="delivery-box">

                    <h3><div class="row"><div class="col-sm-8 col-xs-2 col-md-6">Write Review</div><div class="col-sm-4 col-xs-10 col-md-6"><span class="btn-back pull-right" style="margin-top:9px;"><a href="<%$this->config->item('site_url')%>store/<%$Store_arr.Store_name%>.html">Back to Store</a></span></div></div></h3>
                    <div class="form-add">
                        <form class="form-horizontal" method="POST" action="store/store/addStoreReview" id="frmReview" name="frmReview">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Your Review</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="inputComment" rows="3" name="review"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-3 control-label">Your Rating</label>
                                <div class="col-sm-9">
                                    <div class="col-md-3">
                                        <ul class="rating-star">
                                            <li><span class="review-star" id="review-star-1" data-rel="1"></span></li>
                                            <li><span class="review-star" id="review-star-2" data-rel="2"></span></li>
                                            <li><span class="review-star" id="review-star-3" data-rel="3" ></span></li>
                                            <li><span class="review-star" id="review-star-4" data-rel="4"></span></li>
                                            <li><span class="review-star" id="review-star-5" data-rel="5"></span></li>
                                        </ul>
                                        <input type="hidden" value="1" name="hidden-star" id="hidden_star"> 
                                        <input type="hidden" value="<%$Store_arr.Store_id%>" name="store_id" id="hidden_id"> 
                                        <input type="hidden" value="<%$Store_arr.Store_name%>" name="store_name" id="hidden_store_name"> 
                                    </div>
                                    <span id="starEr"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <input type="submit" class="btn btn-common" id="postReview" value="Submit">
                                    <a class="btn btn-common" href="<%$this->config->item('site_url')%>store/<%$Store_arr.Store_name%>.html">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>