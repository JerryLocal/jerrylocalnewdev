<%$this->js->add_js("review.js")%>
<%assign var=settings value=$store_rating_arr['settings']%>
<%assign var=per_page value=$this->config->item('REC_LIMIT_FRONT')%>
<%assign var=totalPage value= ceil($settings.count/$per_page)%>
<%assign var=tpages value= ceil($settings.count/$per_page)%>
<div class="main-containt">
    <div class="container">
        <div class="row">
        </div>
    </div>
    <div class=""> <!-- container -->
        <div class="delivery-box">
            <h3>
                <div class="row">
                    <div class="col-sm-8 col-xs-2 col-md-6"><%$Store_arr.Store_name%></div>
                    <div class="col-sm-4 col-xs-10 col-md-6">
                        <span class="btn-back pull-right" style="margin-top:9px;">
                            <a data-rel="<%$Store_arr.Store_id%>" class="add_your_review" id="add_your_review" style="cursor:pointer">Write a Review</a>
                        </span>
                        <!--<span class="btn-back pull-right" style="margin-top:9px;">
                            <a href="<%$this->config->item('site_url')%>store/<%$Store_arr.Store_id%>/products.html" class="your_products" id="your_products" style="cursor:pointer">Your Products</a>
                        </span>-->
                    </div>
                    <form id="add_review" method="POST" action="<%$this->config->item('site_url')%>store-review.html" target="_blank">
                        <input type="hidden" value="<%$Store_arr.Store_id%>" name="id">
                    </form>
                </div>
            </h3>
            <div class="form-add">
                <h4 class="fl store_logo_fg" style="margin:0px 40px 40px 0px;">
                    <%if $Store_arr.Store_logo neq ''%>
                    <img src="<%$this->config->item('store_logo')%><%$Store_arr.Store_logo%>" width="100" height="100" alt=""> <!-- < %$Store_arr.Store_name%> -->
                    <%else%>
                    <img src="<%$this->config->item('images_url')%>seller-logo.png" width="100" height="100" alt=""> <!-- < %$Store_arr.Store_name%> -->
                    <%/if%>
                    <!-- <div><strong><%$Store_arr.Contact_name%></strong></div> -->
                </h4>
                <div style="text-align:justify;"><%$Store_arr.Store_description%></div>
                <!-- <p class="sellermsg"></p> -->
                <div class="clear"></div>
                <div class="text-right">
                    <%if $return_policy.vReturnPolicy neq ''%>
                    <a href="<%$this->config->item('site_url')%>store/<%$Store_arr.Store_id%>/return-policy.html" class="btn btn-link" target="_blank">Return Policy</a>/
                    <%/if%>
                    <%if $about_store.vAboutStore neq ''%>
                    <a href="<%$this->config->item('site_url')%>store/<%$Store_arr.Store_id%>/about.html" class="btn btn-link">About Store</a>/
                    <%/if%>
                    <a href="<%$this->config->item('site_url')%><%$Store_arr.Store_id%>/store_contact.html" class="btn btn-link">Contact seller</a>
                </div>
                <!--seller Product area start here-->
                <%$this->js->add_js("product-sort.js")%>
                <%$this->css->add_css("jquery.mCustomScrollbar.css")%>
                <%$this->css->css_src()%>
                <%assign var=settings value=$product_arr['settings']%>
                <!-- < %assign var=per_page value=$this->config->item('REC_LIMIT_FRONT')%> -->
                <%assign var=per_page value=12%>
                <%assign var=totalPage value= ceil($settings.count/$per_page)%>
                <%assign var=tpages value= ceil($settings.count/$per_page)%>

                <!-- < %$product_arr|@pr%> -->
                <div class="main-containt" style="margin-left: -20px;">
                    <div class="container">
                        <div class="row-fluid">
                            <%if $product_arr['settings']['success'] eq 1%>
                            <hr>                            
                            <div class="product-list">
                                <div class="container">
                                    <div class="row1">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-main">
                                            <h3>Store Product</h3>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-main">
                                            <div id="catalog-listing">
                                                <div class="category-products page-product-list">
                                                    <div class="row sort_result">                     
                                                        <%if $product_arr['data']|@count gt 0%>
                                                        <%assign var=products value=$product_arr['data']%>
                                                        <%foreach $products as $product%>             
                                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item">  
                                                            <%$this->general->ProductBlockGenerator($product.mp_mst_products_id,$product.mp_title,$product.mp_regular_price,$product.mp_sale_price,$product.mp_rating_avg,$product.mp_rating_avg,$product.mp_stock,$product.mp_difference_per,$product.mp_date,$product.mp_wishlist_state,$product.mp_default_img)%>
                                                        </div>
                                                        <%/foreach%>                      
                                                        <%/if%>                    
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pagination-box">
                                                <div class="col-md-7">
                                                    <%assign var=displayResult value=($settings.curr_page*$per_page gt $settings.count) ? $settings.count : $settings.curr_page*$per_page%>
                                                    <div class="row">
                                                        <div class="pagination-list">Items <%(($settings.curr_page-1)*$per_page)+1%> to <%$displayResult%> of <%$settings.count%> total</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="row">
                                                        <ul class="pagination">
                                                            <%assign var=reload value=$this->config->item('site_url')|cat:"store/"|cat:$product_arr['data'][0].mp_store_id|cat:"/"|cat:$Store_arr.Store_name|cat:"html?tpages="|cat:$tpages%>            
                                                            <%if $totalPage gt 1%>
                                                            <%$this->generalfront->paginate($reload,$product_arr['settings'].curr_page,$totalPage)%>
                                                            <%/if%>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%else%>
                            <div class="short-bar">
                                <div class="col-md-12" style="text-align:center">                  
                                    <div class="row">Store is currently unavailable</div>
                                </div>
                            </div>
                            <%/if%>
                        </div>
                    </div>
                </div>
                <!--seller Product area start here-->
                <div class="reviewnrating">
                    <h4><strong>Reviews & Ratings</strong></h4>
                    <div class="row">
                        <div class="col-md-3"> <strong class="percentage"><%$Store_rating_avg['tot_positive_avg']%> %</strong> Positive
                            <p class="based-text">Based on <%$Store_rating_avg['total_rating']%> Ratings</p>
                        </div>
                        <div>
                            <div class="sellerRatingBreakup fk-float-left"> <ul class="rating-histogram"> 
                                    <li class="fk-font-small tmargin3"> <span>5 star</span> <div class="rating-bars"> <div class="progress " style="width:<%$Store_rating_avg['five_avg']%>%"></div> </div>
                                        &nbsp;<%$Store_rating_avg['rating5']%> </li>
                                    <li class="fk-font-small tmargin3"> <span>4 star</span> <div class="rating-bars"> <div class="progress " style="width:<%$Store_rating_avg['four_avg']%>%"></div> </div>
                                        &nbsp;<%$Store_rating_avg['rating4']%> </li>
                                    <li class="fk-font-small tmargin3"> <span>3 star</span> <div class="rating-bars"> <div class="progress " style="width:<%$Store_rating_avg['three_avg']%>%"></div> </div>
                                        &nbsp;<%$Store_rating_avg['rating3']%> </li>
                                    <li class="fk-font-small tmargin3"> <span>2 star</span> <div class="rating-bars"> <div class="progress " style="width:<%$Store_rating_avg['two_avg']%>%"></div> </div>
                                        &nbsp;<%$Store_rating_avg['rating2']%> </li>
                                    <li class="fk-font-small tmargin3"> <span>1 star</span> <div class="rating-bars"> <div class="progress " style="width:<%$Store_rating_avg['one_avg']%>%"></div> </div>
                                        &nbsp;<%$Store_rating_avg['rating1']%> </li>
                                </ul> </div>
                        </div>
                        <div class="rating-histogram-addon fk-float-left">
                            <div class="histogram-clubbing histogram-positive tmargin5">Positive</div><div class="histogram-clubbing histogram-neutral tmargin3">Neutral</div>
                            <div class="histogram-clubbing histogram-negative tmargin3">Negative</div>
                        </div>

                        <div> <br>
                            <br><br>
                            <br>
                            <%assign var=displayResult value=($settings.curr_page*$per_page gt $settings.count) ? $settings.count : $settings.curr_page*$per_page%>              
                            <!-- < %if $store_rating_arr['settings'].count gt 0%>
                            Showing <%(($settings.curr_page-1)*$per_page)+1%> to <%$displayResult%> of <%$settings.count%> reviews< %/if%> -->
                        </div>
                    </div>
                </div>
                <%if $store_rating_arr['data']|@count gt 0 %>
                <%foreach $store_rating_arr['data'] as $rating%>
                <div class="comment-reveiw-box">
                    <div class="row">
                        <div class="col-md-3">
                            <%$this->generalfront->productRatingFormat($rating['tsr_rate'])%>
                            <p class="review-member-name"><%$rating['tsr_name']%></p>
                            <p class="review-date"><%$rating['tsr_date']%></p>
                        </div>
                        <div class="col-md-9">
                            <p><%$rating['tsr_review']%></p>
                        </div>
                    </div>
                </div>
                <%/foreach%> 
                <%else%>
                <div class="comment-reveiw-box"> 
                    <h1 class="TAC">No Review Available</h1>
                </div>
                <%/if%>
            </div>
            <%if $store_rating_arr['settings'].count gt 0%>
            <div class="col-md-12">
                <div class="row">
                    <div class="pagination-box">
                        <div class="col-md-7">                  
                            <div class="row">
                                <div class="pagination-list" style="margin-left: 15px;">Items <%(($settings.curr_page-1)*$per_page)+1%> to <%$displayResult%> of <%$settings.count%> total</div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="row">
                                <ul class="pagination">
                                    <%assign var=reload value=$this->config->item('site_url')|cat:"store/"|cat:"<%$Store_arr.Store_id%>"|cat:"/seller-page.html?tpages="|cat:$tpages%>
                                    <%if $totalPage gt 1%>
                                    <%$this->generalfront->paginate($reload,$store_rating_arr['settings'].curr_page,$totalPage)%>
                                    <%/if%>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%/if%>

        </div>
    </div>
</div>