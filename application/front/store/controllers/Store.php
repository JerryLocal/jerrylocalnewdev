<?php

/**
 * Description of Store Controller
 * 
 * @module Store
 * 
 * @class Store.php
 * 
 * @path application\front\store\controllers\Store.php
 * 
 * @author Jaydeep Jagani
 * 
 * @date 16.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Store extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');
        $this->load->model('Model_store');
        $this->store_id = '';
        if(is_numeric($this->uri->segment(2))){
            $this->store_id = $this->uri->segment(2);
        } else {
            $this->store_id = $this->general->getSellerStoreId($this->uri->segment(2));
        }
    }

    /**
     * index method is used to initialize index function.
     */
    public function index() {        
        $this->StoreData();
    }

    /**
     * staticpage method is used to display static pages.
     */
    function StoreData() {
        //$store_id = $this->uri->segment(2);
        $store_id = $this->store_id;
        $params = array();
        $params['iStoreId'] = $store_id;
        // Store Data
        $ws_name = 'get_store_data';
        $store_arr = $this->hbmodel->getData($ws_name, $params);
        $store_details = $merchant_details_arr['data'];        
        $this->smarty->assign('Store_arr',$store_arr['data'][0]);
        // pr($store_arr,1);

        $getarr = $this->input->get();
        if(isset($getarr['product_tpages'])){
            $curr_page = $getarr['page'];
        } else {
            $curr_page = '';
        }
        $params['page_index'] = $curr_page;
        $params['iStoreId'] = $store_id;
        $ws_name = 'get_store_product';
        $store_product = $this->hbmodel->getData($ws_name, $params);
        $store_product_details = $merchant_details_arr['data'];     
        // pr($store_product,1);
        $this->smarty->assign('product_arr',$store_product);

        $ws_name = 'get_store_avg_block';
        $Store_rating_avg = $this->hbmodel->getData($ws_name, $params);
        // pr($Store_rating_avg,1);
        $store_details = $merchant_details_arr['data'];
        $this->smarty->assign('Store_rating_avg',$Store_rating_avg['data']['custom_function'][0]['rating']);

        // Store Review
        $getarr = $this->input->get();
        if(isset($getarr['tpages'])){
            $curr_page = $getarr['page'];
        } else {
            $curr_page = '';
        }
        $params = array();
        $params['page_index'] = $curr_page;
        $params ['iStoreId'] = $store_id;
        $ws_name = 'get_store_rating';
        $store_rating_arr = $this->hbmodel->getData($ws_name, $params);
        // pr($store_rating_arr);exit();
        $store_rating_details = $merchant_details_arr['data'];        
        $this->smarty->assign('store_rating_arr',$store_rating_arr);

        // About Store 
        $aboutstore = $this->Model_store->aboutStore($store_id);
        $aboutstore[0]['vAboutStore'] = nl2br($aboutstore[0]['vAboutStore']);
        // pr($aboutstore,1);
        $this->smarty->assign('about_store',$aboutstore[0]);
        // pr($store_rating_arr,1);

        // Return Policy
        $returnpolicy = $this->Model_store->returnPolicy($store_id);
        $returnpolicy[0]['vReturnPolicy'] = nl2br($returnpolicy[0]['vReturnPolicy']);
        $this->smarty->assign('return_policy',$returnpolicy[0]);

    }

    function StoreReview(){
        $iUserId = $this->session->userdata('iUserId'); 
        if(!$iUserId){
               $this->session->set_flashdata('failure', "Please log in to give review");
               // $this->generalfront->redirectLogin(true);
               $ref = base64_encode($_SERVER['HTTP_REFERER']);
               redirect($this->config->item('site_url') . 'login.html?ref='.$ref);
        }
        if(!empty($_POST)){
            $params['iStoreId'] = $_POST['id'];
            $ws_name = 'get_store_data';
            $store_arr = $this->hbmodel->getData($ws_name, $params);
            $store_details = $merchant_details_arr['data'];        
            $this->smarty->assign('Store_arr',$store_arr['data'][0]);
            // pr($store_arr);exit();
            $this->loadview('review');
        }else
        {
            redirect($this->config->item('site_url'));
        }
    }

    function addStoreReview(){
        if(!empty($_POST)){
            $iUserId = $this->session->userdata('iUserId');
            $userdata = $this->generalfront->getRowData('iCustomerId',$iUserId,'mod_customer');
            $storedata = $this->generalfront->getRowData('iMstStoreDetailId',$_POST['store_id'],'mst_store_detail');
            $params['storeid'] = $_POST['store_id'];
            $params['rating'] = $_POST['hidden-star'];
            $params['description'] = $_POST['review'];
            $params['loginuserid'] =$userdata['iCustomerId'] ;
            $params['name'] = $userdata['vFirstName'];
            $params['email'] =$userdata['vEmail'];
            $params['iSellerId'] =$storedata['iAdminId'];
            $ws_name = 'set_store_rating';
            $rating_arr = $this->hbmodel->getData($ws_name, $params);
            if($rating_arr['settings']['success'] == 1){
                redirect($this->config->item('site_url') .'store/'.$_POST['store_name'] .'.html');
            }else
            {
                redirect($this->config->item('site_url') .'store-review.html');   
            }
            
        }else
        {
             $this->session->set_flashdata('failure', "Please Enter Some Data");
                redirect($this->config->item('site_url') .'store-review.html');   
        }
    }

    function AboutStore(){
        $store_id = $this->store_id;
        $aboutstore = $this->Model_store->aboutStore($store_id);
        $aboutstore[0]['vAboutStore'] = nl2br($aboutstore[0]['vAboutStore']);
        $this->smarty->assign('about_store',$aboutstore[0]);
    }

    function ReturnPolicy(){
        $store_id = $this->store_id;
        $returnpolicy = $this->Model_store->returnPolicy($store_id);
        $returnpolicy[0]['vReturnPolicy'] = nl2br($returnpolicy[0]['vReturnPolicy']);
        $this->smarty->assign('return_policy',$returnpolicy[0]);
    }

    function otherproduct(){
        $store_id = $this->store_id;
        $getarr = $this->input->get();
        $curr_page = $getarr['page'];
        $params['page_index'] = $curr_page;
        $params['iStoreId'] = $store_id;
        $ws_name = 'get_store_product';
        $store_product = $this->hbmodel->getData($ws_name, $params);
        $store_product_details = $merchant_details_arr['data'];     
        // pr($store_product,1);   
        $this->smarty->assign('product_arr',$store_product);
        $this->loadview('otherproduct');
    }
}
