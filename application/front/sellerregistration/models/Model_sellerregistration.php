<?php

/**
 * Description of User Model
 * 
 * @module User
 * 
 * @class Model_homemiddle.php
 * 
 * @path application\front\homemiddle\models\Model_homemiddle.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
class Model_sellerregistration extends CI_Model {

    private $primary_key;
    private $main_table;
    public $errorCode;
    public $errorMessage;

    function __construct() {
        parent::__construct();        
       $this->main_table = "app_banner";        
       $this->primary_key = "iAppBannerId";
    }
    function index(){}
    

    function setUserData($userData){
            $id = $this->generalfront->getColumnValue('vEmail',$userData['User_Email'],'iAdminId','mod_admin','');
            if(empty($id)){
                // Below code for the password Encryption - start
                $input_params['password']=$userData['User_Password'];
                $userData['User_Password'] = $this->general->encrypt($input_params);
                // Above code for the password Encryption - end
                $userDetail = array('vName'=>$userData['vName'],'vEmail'=>$userData['User_Email'],'vUserName'=>$userData['User_Email'],'vPassword'=>$userData['User_Password'],'iGroupId'=>'3','eStatus'=>'Inactive');
                $this->db->insert('mod_admin',$userDetail);
                return $this->db->insert_id();
            }else{
                return 0;
            }
    }
    function setStoreDetail($storeDetail,$userid,$bankDetail){
        $this->db->select('iMstStoreDetailId,vStoreCode');
        $this->db->from('mst_store_detail');
        $this->db->order_by("iMstStoreDetailId","desc"); 
        $this->db->limit(1);
        $lastid= $this->db->get()->result_array();
        $newid = $lastid[0]['iMstStoreDetailId']+1; 
        $storecode ='store'.$newid;

        $insertvalue = array('vStoreCode'=>$storecode,
                            'iAdminId'=>$userid,
                            'vStoreName'=>$storeDetail['StoreName'],
                            'vCompanyName'=>$storeDetail['CompanyName'],
                            'vContactName'=>$storeDetail['ContactName'],
                            'vArea'=>$storeDetail['area'],
                            'vAddress1'=>$storeDetail['address1'],
                            'vAddress2'=>$storeDetail['address2'],
                            'iCountryId'=>$storeDetail['vCountry'],
                            'iStateId'=>$storeDetail['vState'],
                            'iCityId'=>$storeDetail['vCity'],
                            'vContactNumber'=>$storeDetail['contact_no'],
                            // 'vContactNumber'=>$storeDetail['contact_no_hidden'],
                            'vPinCode'=>$storeDetail['pincode'],
                            'vWebsiteUrl'=>$storeDetail['website_url'],
                            'iMstCategoriesId'=>$storeDetail['category'],
                            'eHaveOnlineStore'=>$storeDetail['online_store_available'],
                            'vStoreUrl'=>$storeDetail['online_store_url'],
                            'vBankAccountName'=>$bankDetail['Bank_name'],
                            'vBankAccountNo'=>$bankDetail['bank_account_1'].$bankDetail['bank_account_2'].$bankDetail['bank_account_3'].$bankDetail['bank_account_4'],
                            'vGSTNo'=>$bankDetail['gst_1'].$bankDetail['gst_2'].$bankDetail['gst_3'],
                            'dDate' => date("Y-m-d H:i:s"),
                            'eAcceptTerm'=>'Accepted',
                            );
        $this->db->insert('mst_store_detail',$insertvalue);
        return $this->db->insert_id();
    }

    // function setBankDetail($bankDetail){
    //     $updatevalue = array('vBankAccountName' => $bankDetail['Bank_name'],
    //                          'vBankAccountNo' => $bankDetail['bank_account_1'].$bankDetail['bank_account_2'].$bankDetail['bank_account_3'].$bankDetail['bank_account_4'],
    //                          'vGSTNo' => $bankDetail['gst_1'].$bankDetail['gst_2'].$bankDetail['gst_3'],
    //                          // 'vGSTIRDProof' => $vBankAccountProof[''],
    //                          // 'vBankAccountProof' => $bankDetail[''],
    //                          // 'vOtherProof' => $bankDetail[''],
    //                          // 'vOtherProofDetail' => $bankDetail[''],
    //                          'eAcceptTerm' => 'Accepted'
    //                          );
    //     $this->db->where('iMstStoreDetailId',$bankDetail['storeid']);
    //     $this->db->update('mst_store_detail',$updatevalue);
    // }
    
    function ImageFieldUpdate($array,$where){
        $this->db->where('iMstStoreDetailId',$where);
        $this->db->update('mst_store_detail',$array);
    }    


}