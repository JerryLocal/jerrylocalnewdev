<%$this->js->add_js("seller.js")%>
<script type="text/javascript">
var country = "<%$user_detail.vCountry%>";
var state = "<%$user_detail.vState%>";
var selcity = "<%$user_detail.vCity%>";
</script>
<div id="seller"></div>
<div class="main-containt">
<div class="container">
<div class="row-fluid">
<%$this->general->Breadcrumb('CLEAN','Seller Registration')%>
</div>
</div>
<div class=""> <!-- container -->
<div class="delivery-box seller-registration">
<h3><div class="row"><div class="col-sm-8">Seller Registration</div><div class="col-sm-4"><span class="btn-back pull-right"></span></div></div></h3>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
          <div class="stepwizard-step active">
        <a type="button" class="btn btn-primary btn-circle">1</a>
        <p>Seller Registration</p>
      </div>
          <div class="stepwizard-step">
        <a type="button" class="btn btn-default btn-circle" disabled="">2</a>
        <p>Business Account Details</p>
      </div>
          <div class="stepwizard-step">
        <a type="button" class="btn btn-default btn-circle" disabled="">3</a>
        <p>Bank Details</p>
      </div>
      <div class="stepwizard-step">
        <a type="button" class="btn btn-default btn-circle" disabled="">4</a>
        <p>Finished</p>
      </div>
        </div>
</div>
<div class="delivery-form">
<form class="form-horizontal" role="form" id="seller-basic" method="POST" action="<%$this->config->item('site_url')%>seller-zone-2.html">
      <div class="form-group">
      <label class="col-sm-3 control-label">Email ID <span class="required">*</span></label>
      <div class="col-sm-9">
      <input type="text" class="form-control max-width" name="User_Email" placeholder="Email ID" value="<%$user_detail['User_Email']%>">
        <div id="error_email" class="required"></div>
      </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Password <span class="required">*</span></label>
        <div class="col-sm-9">
           <input type="password" class="form-control max-width" name="User_Password" placeholder="Password" value="<%$user_detail['User_Password']%>">
        <div id="error_password" class="required"></div>

        </div>
      </div>
   <div class="form-group">
      <div class="col-sm-offset-3 col-sm-10">
         <input type="submit" id="step1-submit" class="btn btn-common" value="Submit And Continue" >
         <a href="<%$this->config->item('site_url')%>"><button type="button" class="btn btn-blue">Cancel</button></a>
      </div>
   </div>
</form></div></div>
</div>
</div>