<%$this->js->add_js("seller.js","tooltipsy.min.js")%>
<script type="text/javascript">
    var country = "<%$user_detail.vCountry%>";
    var state = "<%$user_detail.vState%>";
    var selcity = "<%$user_detail.vCity%>";
</script>
<div id="seller"></div>
<div class="main-containt">
    <div class="container">
        <div class="row-fluid">
            <%$this->general->Breadcrumb('CLEAN','Seller Registration')%>
        </div>
    </div>
    <div class=""> <!-- container -->
        <div class="delivery-box seller-registration">
            <h3>Bank Details</h3>
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a type="button" href="<%$this->config->item('site_url')%>seller-zone.html" class="btn btn-primary btn-circle">1</a>
                        <p>Seller Registration</p>
                    </div>
                    <div class="stepwizard-step">
                        <a type="button" href="<%$this->config->item('site_url')%>seller-zone-2.html" class="btn btn-default btn-circle">2</a>
                        <p>Business Account Details</p>
                    </div>
                    <div class="stepwizard-step active">
                        <a type="button" class="btn btn-default btn-circle">3</a>
                        <p>Bank Details</p>
                    </div>
                    <div class="stepwizard-step">
                        <a type="button" class="btn btn-default btn-circle" disabled="">4</a>
                        <p>Finished</p>
                    </div>
                </div>
            </div>
            <div class="delivery-form">
                <form class="form-horizontal" role="form" enctype="multipart/form-data" id="seller-bank" method="POST" action="<%$this->config->item('site_url')%>seller-zone-4.html">
                    <input type="hidden" value="<%$storeid%>" class="storeid" name="storeid"/>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Bank account Name (NZ only) <span class="required">*</span></label>
                        <div class="col-sm-8 pr">
                            <input type="text" class="form-control" name="Bank_name">
                            <a title="Enter valid New Zealand Bank detail for getting paid by us for each successful sale" class="hastip file-upload-help pa seller_tips"><i class="fa fa-question-circle fa-lg"></i></a>
                        </div>
                        <div id="error_bankname" class="required"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Bank account Number <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-3">
                                    <input type="text" class="form-control bank_box" id="bank_box_len_2" name="bank_account_1" maxlength="2">
                                    <span style="color:#5f5f5f;">Bank (2)</span>
                                    <span id="bank_account_1Err"></span>
                                </div>
                                <div id="error_bank_ac_1" class="required"></div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control bank_box" id="bank_box_len_4" name="bank_account_2" maxlength="4">
                                    <span style="color:#5f5f5f;">Branch (4)</span>
                                    <span id="bank_account_2Err"></span>
                                </div>
                                <div id="error_bank_ac_2" class="required"></div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control bank_box" id="bank_box_len_7" name="bank_account_3" maxlength="7">
                                    <span style="color:#5f5f5f;">Account (7)</span>
                                    <span id="bank_account_3Err"></span>
                                </div>
                                <div id="error_bank_ac_3" class="required"></div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control bank_box" id="bank_box_len_3" name="bank_account_4" maxlength="3">
                                    <span style="color:#5f5f5f;">Suffix (3)</span>  
                                    <span id="bank_account_4Err"></span>        
                                </div>
                                <div id="error_bank_ac_4" class="required"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group gst_no">
                        <label class="col-sm-4 control-label">GST Number</label>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="text" class="form-control gst_box" name="gst_1" maxlength="3">
                                </div>
                                <div id="error_gst_1" class="required"></div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control gst_box" name="gst_2" maxlength="3">
                                </div>
                                <div id="error_gst_2" class="required"></div>
                                <div class="col-sm-4 pr">
                                    <input type="text" class="form-control gst_box" name="gst_3" maxlength="3">
                                    <a title="Enter valid GST No that your customer will see on online generated order" class="hastip file-upload-help pa seller_tips"><i class="fa fa-question-circle fa-lg"></i></a>
                                </div>
                                <div id="error_gst_3" class="required"></div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="form-group">
                    <label class="col-sm-4 control-label">Upload GST/IRD Detail Proof</label>
                     <div class="col-sm-8">
                                        <input type="file" class="file-upload-btn gst_upload" name="gst_proof" id="gst_proof" onchange="return previewFile('gst_proof_preview_img', 'gst_proof');">
                                        <span class="file-upload-img" style="display:none;">
                                          <img src="" id="gst_proof_preview_img">
                                          <a href="javascript:;" class="file-upload-img-close" onclick="return removeSelectedImage('gst_proof_preview_img', 'gst_proof');" id="close-btn-gst-proof"><i class="fa fa-times-circle"></i></a>
                                        </span>
                                       <a data-toggle="tooltip" title="Upload GST/IRD Detail Proof" class="file-upload-help" data-original-title="Please Upload GST/IRD Detail Proof"><i class="fa fa-question-circle fa-lg"></i></a>
                                        </div>
                    </div> -->
                    <!-- <div class="form-group">
                    <label class="col-sm-4 control-label">Upload Bank Account Proof</label>
                    <div class="col-sm-8">            
                                        <input type="file" class="file-upload-btn gst_upload" name="bank_proof" id="bank_proof" onchange="return previewFile('bank_proof_preview_img', 'bank_proof');">
                                        <span class="file-upload-img" style="display:none;">
                                          <img src="" id="bank_proof_preview_img">                            
                                          <a href="javascript:;" class="file-upload-img-close" onclick="return removeSelectedImage('bank_proof_preview_img', 'bank_proof');" id="close-btn-bank-proof"><i class="fa fa-times-circle"></i></a>
                                        </span>
                                        <a data-toggle="tooltip" title="Upload Bank Account Proof" class="file-upload-help" data-original-title="Please Upload Bank Account Proof"><i class="fa fa-question-circle fa-lg"></i></a>
                                        </div>
                    </div> -->
                    <!-- <div class="form-group">
                    <label class="col-sm-4 control-label">Upload Any Other Proof</label>
                    <div class="col-sm-8">            
                                        <input type="file" class="file-upload-btn gst_upload" name="other_proof" id="other_proof" onchange="return previewFile('other_proof_preview_img', 'other_proof');">
                                        <span class="file-upload-img" style="display:none;">
                                          <img src="" id="other_proof_preview_img">                            
                                          <a href="javascript:;" class="file-upload-img-close" onclick="return removeSelectedImage('other_proof_preview_img', 'other_proof');" id="close-btn-other-proof"><i class="fa fa-times-circle"></i></a>
                                        </span>
                                        <a data-toggle="tooltip" title="Upload Any Other Proof" class="file-upload-help" data-original-title="Plese Upload Any Other Proof"><i class="fa fa-question-circle fa-lg"></i></a>
                                        </div>
                 </div> -->
                    <div class="form-group">
                        <label class="col-sm-4 control-label">&nbsp;</label>
                        <div class="col-sm-8">
                            <input type="checkbox" name="accept_term" >&nbsp;Accept <a href="<%$this->config->item('site_url')%>term-of-sale.html" target="_blank">Terms and Conditions</a> 
                            <span id="accept_termErr"></span>
                        </div>
                        <div id="error_accept" class="required"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-9">
                            <input type="submit" id="step3-submit"class="btn btn-common" value="Submit And Continue" />
                            <button type="submit" class="btn btn-blue">Cancel</button>
                        </div>
                    </div>
                </form></div>
        </div>
    </div>
</div>
