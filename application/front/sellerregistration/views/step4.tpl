<%$this->js->add_js("seller.js")%>
<div id="seller"></div>
<div class="main-containt">
<div class="container">
<div class="row-fluid">
<%$this->general->Breadcrumb('CLEAN','Seller Registration')%>
</div>
</div>
<div class=""> <!-- container -->
<div class="delivery-box seller-registration">
<h3>Finished</h3>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
          <div class="stepwizard-step">
        <a type="button" class="btn btn-primary btn-circle">1</a>
        <p>Seller Registration</p>
      </div>
          <div class="stepwizard-step">
        <a type="button" class="btn btn-default btn-circle">2</a>
        <p>Business Account Details</p>
      </div>
          <div class="stepwizard-step">
        <a type="button" class="btn btn-default btn-circle">3</a>
        <p>Bank Details</p>
      </div>
      <div class="stepwizard-step active">
        <a href="#" type="button" class="btn btn-default btn-circle">4</a>
        <p>Finished</p>
      </div>
        </div>
  </div>
  <div class="container">
    <div id="thankyoumsg">
      <div class="delivery-box">
          <h3>
              <div class="row">
                  <div class="col-sm-12">
                      <center>Thank You..!!</center><br>
                      <center><p> Once we review your application then we will send you confirmation Email.</p></center>
                  </div>
              </div>
          </h3>        
      </div>
      </div>
  </div>

<!-- <div class="contact-box" style="min-height:350px;padding:40px;text-align:center;">
<p>Once we review your application then we will send you confirmation Email.</p>
</div> -->
</div>
</div>
</div>