<%$this->js->add_js("seller.js","tooltipsy.min.js")%>
<%$this->js->add_js("intlTelInput/intlTelInput.min.js")%>
<%$this->css->add_css("intlTelInput/intlTelInput.css")%>
<%$this->css->css_src()%>
<div id="seller"></div>
<script type="text/javascript">
var country = "<%$user_detail.vCountry%>";
var state = "<%$user_detail.vState%>";
var selcity = "<%$user_detail.vCity%>";
</script>
<div class="main-containt">
<div class="container">
<div class="row-fluid">
<%$this->general->Breadcrumb('CLEAN','Seller Registration')%>
</div>
</div>
<div class=""> <!-- container -->
<div class="delivery-box seller-registration">
<h3>Business Account Details</h3>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
          <div class="stepwizard-step">
        <a type="button" href="<%$this->config->item('site_url')%>seller-zone.html" class="btn btn-primary btn-circle">1</a>
        <p>Seller Registration</p>
      </div>
          <div class="stepwizard-step active">
        <a type="button" class="btn btn-default btn-circle">2</a>
        <p>Business Account Details</p>
      </div>
          <div class="stepwizard-step">
        <a type="button" class="btn btn-default btn-circle" disabled="">3</a>
        <p>Bank Details</p>
      </div>
      <div class="stepwizard-step">
        <a type="button" class="btn btn-default btn-circle" disabled="">4</a>
        <p>Finished</p>
      </div>
        </div>
  </div>
<div class="delivery-form">
<form class="form-horizontal" role="form" id="seller-store"   method="POST" action="<%$this->config->item('site_url')%>seller-zone-3.html">
  <input type="hidden" value="<%$user_detail['userid']%>" name="adminid">
      <div class="form-group">
      <label class="col-sm-3 control-label">Store Name <span class="required">*</span></label>
      <div class="col-sm-9 pr">
      <input type="text" class="form-control" value="<%$user_detail['StoreName']%>" placeholder="Store name" name="StoreName">      
      <a title="Enter Unique store name that your customer will see on our website" class="hastip file-upload-help pa seller_tips"><i class="fa fa-question-circle fa-lg"></i></a>
      <div id="error_storename" class="required"></div>
      </div>
      </div>
      <div class="form-group">
      <label class="col-sm-3 control-label">Company Name <span class="required">*</span></label>
      <div class="col-sm-9 pr">
      <input type="text" class="form-control" placeholder="Company name" value="<%$user_detail['CompanyName']%>" name="CompanyName">
      <a title="Enter valid bussiness name that your customer will see on invoice" class="hastip file-upload-help pa seller_tips"><i class="fa fa-question-circle fa-lg"></i></a>
        <div id="error_cmpnyname" class="required"></div>
      </div>
    </div>
    <div class="form-group">
         <label class="col-sm-3 control-label">Contact Name <span class="required">*</span></label>
         <div class="col-sm-9 pr">
          <input type="text" class="form-control" placeholder="Contact name" value="<%$user_detail['ContactName']%>" name="ContactName">
          <a title="Enter contact person details for all communication" class="hastip file-upload-help pa seller_tips"><i class="fa fa-question-circle fa-lg"></i></a>
           <div id="error_cntcname" class="required"></div>
         </div>
       </div>
       <div class="form-group">
                        <label for="inputName" class="col-sm-3 control-label">Street No / Name <span class="required">*</span></label>
                        <div class="col-sm-9 pr">
                          <input type="text" class="form-control" id="inputname" name="address1" placeholder="Street No / name" value="<%$user_detail['address1']%>">
                          <a title="Enter valid bussiness address that your customer will see on invoice" class="hastip file-upload-help pa seller_tips"><i class="fa fa-question-circle fa-lg"></i></a>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputArea" class="col-sm-3 control-label">Area Name</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="inputArea" name="area" placeholder="Area name" value="<%$user_detail['area']%>">
                        </div>
                      </div>
                      <div class="form-group">
                       <label for="inputEmail" class="col-sm-3 control-label">Country <span class="required">*</span></label>
                        <div class="col-sm-9">
                          <%$this->dropdown->display("vCountry","vCountry","class='form-control chosen-select'","|||Select Country","","<%$user_detail['vCountry']%>","")%>
                       </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-3 control-label">State/Region <span class="required">*</span></label>
                        <div class="col-sm-9">
                          <%$this->dropdown->display("","vState","class='form-control chosen-select'","|||Select Region","","<%$user_detail['vState']%>","")%>
                       </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-3 control-label">City <span class="required">*</span></label>
                        <div class="col-sm-9">
                           <%$this->dropdown->display("","vCity","class='form-control chosen-select'","|||Select City","","<%$user_detail['vCity']%>","")%>
                       </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPincode" class="col-sm-3 control-label">Pin Code <span class="required">*</span></label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="inputPincode" name="pincode" placeholder="Pin Code" value="<%$user_detail['pincode']%>">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label for="inputName" class="col-sm-3 control-label" >Contact No. <span class="required">*</span></label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="contact_no" id="contact_no" value="<%$user_detail['contact_no']%>" maxlength="16" placeholder="Contact No.">
                          <!-- <input name="contact_no_hidden" id='contact_no_hidden' type='hidden' /> -->
                        </div>
                      </div><div class="form-group">
      <label class="col-sm-3 control-label">Company Website URL </label>
      <div class="col-sm-9">
      <input type="text" class="form-control" id="website_url" name="website_url" placeholder="www.demo.com" value="<%$user_detail['website_url']%>">
      </div>
      </div>
   <div class="form-group">
      <label class="col-sm-3 control-label">Primary Category</label>
      <div class="col-sm-9">
      <select size="1" class="form-control" name="category">
        <%assign var=primaryoptions value=$this->generalfront->getPrimaryOption()%>        
        <%foreach $primaryoptions as $option%>
          <option value="<%$option.iMstCategoriesId%>"><%$option.vTitle%></option>
        <%/foreach%>      
      </select>
      </div>
   </div>
  <div class="form-group">
      <label class="col-sm-3 control-label">Do you sell product online already on other marketplace?</label>
    <div class="col-sm-9">
      <input type="radio" id="store_url_yes" class="store_url" name="online_store_available" value="yes">&nbsp;Yes &nbsp;&nbsp; 
      <input type="radio" id="store_url_no" class="store_url" name="online_store_available" value="no" checked="checked">&nbsp;No
     </div>
   </div>
<div class="form-group" id="online_store_url_row">
      <label class="col-sm-3 control-label">Provide URL for store</label>
      <div class="col-sm-9">
      <input type="text" class="form-control" name="online_store_url" id="online_store_url" placeholder="www.demo.com" value="<%$user_detail['online_store_url']%>">
      </div>
      </div>     
   <div class="form-group">
      <div class="col-sm-offset-3 col-sm-10">
         <input type="submit" id="step2-submit" class="btn btn-common" value="Submit And Continue">
         <button type="button" class="btn btn-blue">Cancel</button>
      </div>
   </div>
</form></div>
</div>
</div>
</div>