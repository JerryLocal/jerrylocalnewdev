<?php

/**
 * Description of Content Controller
 * 
 * @module Content
 * 
 * @class content.php
 * 
 * @path application\front\content\controllers\content.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Sellerregistration extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');
        $this->load->model('model_sellerregistration');
    }

    /**
     * index method is used to initialize index function.
     */
    public function index() {
        // $this->session->unset_userdata('seller_reg_step-1');
        $seller_reg_step1 = $this->session->userdata('seller_reg_step-1');    
        if(isset($seller_reg_step1) && !empty($seller_reg_step1)){
          // $seller_reg_step1=$seller_reg_step1;
          // pr($seller_reg_step1,1);
          $this->smarty->assign('user_detail',$seller_reg_step1);
         }
         // $this->loadView('content/static/sellerzone');
    }
    function setUserData(){
      if(!empty($_POST)){
        $user_detail = $_POST;
        $userid = $this->generalfront->getColumnValue('vEmail',$_POST['User_Email'],'iAdminId','mod_admin');
        $user_detail=$this->session->userdata('seller_reg_step-2');
        // pr($user_detail,1);
        if(isset($user_detail) && !empty($user_detail)){
          $this->smarty->assign('user_detail',$user_detail);
          $user_detail = $user_detail; 
        }
        $this->session->set_userdata('seller_reg_step-1',$_POST);
      }else
       {
        $_POST=$this->session->userdata('seller_reg_step-2');
        if(isset($_POST) && !empty($_POST)){
          // $this->smarty->assign('user_detail',$_POST);
          $user_detail = $_POST; 
        }
        $userid = '0';
       } 
        if($userid < 1  && isset($userid)){
          $this->generalfront->assignCountryDropDown();
          $this->generalfront->assignStateDataDropDown();
          $this->generalfront->assignCityDataDropDown();
          $this->smarty->assign('user_detail',$user_detail);
          $this->loadView('step2');  
        }else{
          $this->session->set_flashdata('failure', "You already have account with us");
          redirect($this->config->item('site_url') . 'seller-zone.html');
        }
    }
    function setStoreDetail(){
        $StoreName = $this->generalfront->getColumnValue('vStoreName',$_POST['StoreName'],'vStoreName','mst_store_detail');

        if(!empty($StoreName)){
          $this->session->set_flashdata('failure', "Store Name is already there.");
          redirect($this->config->item('site_url') . 'seller-zone-2.html');
        }else
        {
            // $this->session->set_userdata('seller_reg_step-1')['vName'] = $_POST['StoreName'];
        }
        if(isset($_POST) && !empty($_POST)){ 
            // $_POST['vContactName'] = $this->session->userdata('seller_reg_step-1')['User_Email'];
            $this->session->set_userdata('seller_reg_step-2',$_POST);
         }else{
            $seller_reg_step3 = $this->session->userdata('seller_reg_step-3');    
            if(isset($seller_reg_step3) && !empty($seller_reg_step3)){
              // $seller_reg_step1=$seller_reg_step2;
              $this->smarty->assign('user_detail',$seller_reg_step3);
             }
         }
         $this->loadView('step3');
        
    }
    function setBankDetail(){
        $Userdata = $this->session->userdata('seller_reg_step-1');
        $step2data = $this->session->userdata('seller_reg_step-2');
        $Userdata['vName'] = $step2data['ContactName'];
        $userid =$this->model_sellerregistration->setUserData($Userdata);
        $storedata = $this->session->userdata('seller_reg_step-2');
        $storedata['adminid'] =$userid;
        $storeid =$this->model_sellerregistration->setStoreDetail($storedata,$userid,$_POST);
        $storecode = $this->generalfront->getColumnValue('iMstStoreDetailId',$storeid,'vStoreCode','mst_store_detail');
        $_POST = array();
        $_POST['msd_status'] = 'Pending';
        $_POST['msd_store_name'] = $storedata['StoreName'];
        $_POST['msd_store_code'] = $storecode;

        $this->general->insertDataToCleanUrl('',$storeid);
        if($storeid > 0){
            $bankproof = $this->uploadFile($_FILES['bank_proof'],$this->config->item('bank_proof_image_path'),$storeid,'bank_proof','bank_proof_size','1');
            $gstproof = $this->uploadFile($_FILES['gst_proof'],$this->config->item('gst_proof_image_path'),$storeid,'gsts_proof','gst_proof_size','1');
            $otherproof = $this->uploadFile($_FILES['other_proof'],$this->config->item('other_proof_image_path'),$storeid,'other_proof','other_proof_size','1');
            $vBankAccountProof = $vOtherProof =$vGSTIRDProof= '';
            if($bankproof['succ'] == 1){
                $vBankAccountProof = $bankproof['name'];
            }
            if($gstproof['succ'] == 1){
                $vGSTIRDProof = $gstproof['name'];
            }

            if($otherproof['succ'] == 1){
                $vOtherProof = $otherproof['name'];
            }
            $imageArr = array( 
                              'vBankAccountProof'=>$vBankAccountProof,      
                              'vGSTIRDProof'=>$vGSTIRDProof,      
                              'vOtherProof'=>$vOtherProof      
                             );
            $this->model_sellerregistration->ImageFieldUpdate($imageArr,$storeid);
        }
        $this->session->unset_userdata('seller_reg_step-1');
        $this->session->unset_userdata('seller_reg_step-2');
        $this->session->unset_userdata('seller_reg_step-3');
        $this->loadView('step4');
    }
    function uploadFile($Filedata,$targetFolder,$storeid,$path,$filesize,$sizeindex){
        if (!empty($Filedata)) {    
           $tempFile = $Filedata['tmp_name'];
           $targetPath = $targetFolder;
           $t_f = time() . $Filedata['name'];
           $t_f = preg_replace("/[^a-zA-Z0-9.]/", "", $t_f);
           $targetFile = rtrim($targetPath, '/') . '/' . $t_f;
           $fileParts = pathinfo($t_f);
           if (move_uploaded_file($tempFile, $targetFile)) {
               $resizeimg = '';//$this->generalfront->getGeneratedImage($path, $t_f, $storeid, $sizeindex , 'Yes', '', $filesize, '');
               $arr = array("img" => $resizeimg, "succ" => 1, "name" => $t_f);
           } else {
               $arr = array("succ" => 0);
           }
           // pr($targetFile,1);
           return $arr;
       }
    }

    function getStateDataOnCountry(){
      $this->load->library('dropdown');
      $postarr = $this->input->post();              
      $params = array();            
      $this->generalfront->assignStateDataDropDown($postarr['countryId']);
      $dropdown = $this->dropdown->display("vState","vState","onchange='return city(this.value);' title='Select State' aria-chosen-valid='Yes' class='form-control chosen-select' data-placeholder='Select State' ","", "","","");
       $ret_arr['dropdown'] = $dropdown;
      echo json_encode($ret_arr);
      exit();
    }

    function getCityDataOnState(){
      $this->load->library('dropdown');
      $postarr = $this->input->post(); 
      $params = array();        
      $this->generalfront->assignCityDataDropDown($postarr['stateId']);
      $dropdown = $this->dropdown->display("vCity","vCity","title='Select City' aria-chosen-valid='Yes' class='form-control chosen-select' data-placeholder='Select City' '","", "","","");
       $ret_arr['dropdown'] = $dropdown;
      echo json_encode($ret_arr);
      exit();
    }

    function checkStoreNameExist(){
      $user_arr = $this->input->post();
      $storename = $this->generalfront->getColumnValue('vStoreName',$user_arr["StoreName"],'vStoreName','mst_store_detail');
      if(empty($storename) && isset($storename)){
        echo "true";
      }else
      {
        echo "false";
      }
      exit;
    }
}

