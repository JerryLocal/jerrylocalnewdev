<?php
/**
 * Description of Newsletter Subscription Model
 *
 * @module Newsletter Subscription
 *
 * @class model_newsletter_subscription.php
 *
 * @path applicationront
otification\models\model_newsletter_subscription.php
 *
 * @author CIT Dev Team
 *
 * @date 23.02.2016
 */

class Model_newsletter_subscription extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
    }

    /**
     * update_unsubscribe_code method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function update_unsubscribe_code($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $vUnsubscribeCode = $input_params['activation_code'];
            if ($input_params['NEW_iMstNewsletterSubscriptionId'] != "")
            {
                $this->db->where("iMstNewsletterSubscriptionId =", $input_params['NEW_iMstNewsletterSubscriptionId']);
            }
            if (isset($input_params['activation_code']))
            {
                $this->db->set("vUnsubscribeCode", $vUnsubscribeCode);
            }
            $res = $this->db->update("mst_newsletter_subscription");
            $affected_rows = $this->db->affected_rows();
            if (!$res || $affected_rows == -1)
            {
                throw new Exception("Failure in updation.");
            }
            $result_param = "affected_rows";
            $result_arr[$result_param] = $affected_rows;
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->db->flush_cache();
        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * email_notification method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function email_notification($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["NEW_vEmail"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["UNSUBSCRIBE_URL"] = $input_params["unsubsribe_url"];
            $email_arr["MEMBER_NAME"] = $input_params["NEW_vName"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.SITE_URL%}";

            $success = $this->general->sendMail($email_arr, "NEWSLETTER");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_updated method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_updated($input_params = array())
    {
        try
        {

            $cc_lo_0 = (empty($input_params["update_unsubscribe_code"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_subscription_id_exist method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_subscription_id_exist($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_iMstNewsletterSubscriptionId"];
            $cc_ro_0 = "0";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("integer", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("integer", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("gt", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
