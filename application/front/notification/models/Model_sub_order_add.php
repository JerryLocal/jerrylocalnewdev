<?php
/**
 * Description of Sub Order Add Model
 *
 * @module Sub Order Add
 *
 * @class model_sub_order_add.php
 *
 * @path applicationront
otification\models\model_sub_order_add.php
 *
 * @author CIT Dev Team
 *
 * @date 23.02.2016
 */

class Model_sub_order_add extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
    }

    /**
     * get_seller_name method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_seller_name($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("mst_store_detail AS msd");
            $this->db->join("mod_admin AS ma", "msd.iAdminId = ma.iAdminId", "left");

            $select_fields = array();
            $this->db->select("msd.vStoreName AS store_name");
            $this->db->select("ma.vName AS seller_name");
            $this->db->select("ma.vEmail AS seller_email");
            if ($input_params['NEW_iMstStoreDetailId'] != "")
            {
                $this->db->where("msd.iMstStoreDetailId =", $input_params['NEW_iMstStoreDetailId']);
            }
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0)
            {
                $i = 0;
                foreach ((array) $result_arr as $data_key => $data_arr)
                {

                    $data = $data_arr["store_name"];
                    if (function_exists("ucfirst"))
                    {
                        $data = call_user_func("ucfirst", $data);
                    }
                    $result_arr[$data_key]["store_name"] = $data;

                    $data = $data_arr["seller_name"];
                    if (function_exists("ucfirst"))
                    {
                        $data = call_user_func("ucfirst", $data);
                    }
                    $result_arr[$data_key]["seller_name"] = $data;

                    $i++;
                }
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * get_buyer_email method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_buyer_email($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("mst_order AS mo");

            $select_fields = array();
            $this->db->select("mo.vBuyerEmail AS buyer_email");
            $this->db->select("mo.vBuyerName AS buyer_name");
            if ($input_params['NEW_iMstOrderId'] != "")
            {
                $this->db->where("mo.iMstOrderId =", $input_params['NEW_iMstOrderId']);
            }
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0)
            {
                $i = 0;
                foreach ((array) $result_arr as $data_key => $data_arr)
                {

                    $data = $data_arr["buyer_name"];
                    if (function_exists("ucfirst"))
                    {
                        $data = call_user_func("ucfirst", $data);
                    }
                    $result_arr[$data_key]["buyer_name"] = $data;

                    $i++;
                }
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * get_order_number method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_order_number($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("mst_order AS mo");

            $select_fields = array();
            $this->db->select("(CONCAT_WS('',mo.vOrderNumberPre,mo.iOrderNumber)) AS combined_order_number", FALSE);
            if ($input_params['NEW_iMstOrderId'] != "")
            {
                $this->db->where("mo.iMstOrderId =", $input_params['NEW_iMstOrderId']);
            }
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }

            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * email_notification method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function email_notification($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["buyer_email"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["NEW_vProductName"];
            $email_arr["qty"] = $input_params["NEW_iProductQty"];
            $email_arr["price"] = $input_params["NEW_fProductPrice"];
            $email_arr["shipping"] = $input_params["NEW_fShippingCost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["NEW_eItemStatus"];
            $email_arr["total_price"] = $input_params["NEW_fTotalCost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["NEW_vProductSku"];

            $success = $this->general->sendMail($email_arr, "BUYER_SUB_ORDER_PENDING");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * notification_seller_pending method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function notification_seller_pending($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["seller_email"];
            $email_arr["vFromEmail"] = $input_params[""];
            $email_arr["vFromName"] = $input_params[""];
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["NEW_vProductName"];
            $email_arr["qty"] = $input_params["NEW_iProductQty"];
            $email_arr["price"] = $input_params["NEW_fProductPrice"];
            $email_arr["shipping"] = $input_params["NEW_fShippingCost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["NEW_eItemStatus"];
            $email_arr["total_price"] = $input_params["NEW_fTotalCost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.EMAIL_ADMIN%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["NEW_vProductSku"];

            $success = $this->general->sendMail($email_arr, "SELLER_SUB_ORDER_PENDING");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * notification_admin_pending method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function notification_admin_pending($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = "".$this->config->item("EMAIL_ADMIN")."";
            $email_arr["vFromEmail"] = $input_params[""];
            $email_arr["vFromName"] = $input_params[""];
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["NEW_vProductName"];
            $email_arr["qty"] = $input_params["NEW_iProductQty"];
            $email_arr["price"] = $input_params["NEW_fProductPrice"];
            $email_arr["shipping"] = $input_params["NEW_fShippingCost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["NEW_eItemStatus"];
            $email_arr["total_price"] = $input_params["NEW_fTotalCost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.EMAIL_ADMIN%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_admin%}";
            $email_arr["product_sku"] = $input_params["NEW_vProductSku"];

            $success = $this->general->sendMail($email_arr, "ADMIN_SUB_ORDER_PENDING");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_records method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_records($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_iMstStoreDetailId"];
            $cc_ro_0 = "0";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("integer", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("integer", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("gt", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            $cc_lo_1 = $input_params["NEW_iMstOrderId"];
            $cc_ro_1 = "0";

            $cc_lo_1 = is_null($cc_lo_1) ? "" : $cc_lo_1;
            $cc_ro_1 = is_null($cc_ro_1) ? "" : $cc_ro_1;

            $cc_pl_1 = $this->general->getDataTypeWiseResult("integer", $cc_lo_1, TRUE);
            $cc_pr_1 = $this->general->getDataTypeWiseResult("integer", $cc_ro_1, FALSE);
            $cc_fr_1 = $this->general->compareDataValues("gt", $cc_pl_1, $cc_pr_1);
            if (!$cc_fr_1)
            {
                throw new Exception("Some condition does not match.");
            }
            $cc_lo_2 = $input_params["NEW_iMstSubOrderId"];
            $cc_ro_2 = "0";

            $cc_lo_2 = is_null($cc_lo_2) ? "" : $cc_lo_2;
            $cc_ro_2 = is_null($cc_ro_2) ? "" : $cc_ro_2;

            $cc_pl_2 = $this->general->getDataTypeWiseResult("integer", $cc_lo_2, TRUE);
            $cc_pr_2 = $this->general->getDataTypeWiseResult("integer", $cc_ro_2, FALSE);
            $cc_fr_2 = $this->general->compareDataValues("gt", $cc_pl_2, $cc_pr_2);
            if (!$cc_fr_2)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0 && $cc_fr_1 && $cc_fr_2))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
