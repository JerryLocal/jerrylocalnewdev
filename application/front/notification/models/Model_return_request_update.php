<?php
/**
 * Description of Return request Update Model
 *
 * @module Return request Update
 *
 * @class model_return_request_update.php
 *
 * @path applicationront
otification\models\model_return_request_update.php
 *
 * @author CIT Dev Team
 *
 * @date 23.02.2016
 */

class Model_return_request_update extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
    }

    /**
     * get_refund_histry_details method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_refund_histry_details($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("trn_rma_history AS trh");

            $select_fields = array();
            $this->db->select("trh.dActionDate AS action_date");
            $this->db->select("trh.tActionDetail AS action_detail");
            if ((!is_array($input_params['NEW_eRquestType']) && $input_params['NEW_eRquestType'] != "") || (is_array($input_params['NEW_eRquestType']) && !empty($input_params['NEW_eRquestType'])))
            {
                if (!is_array($input_params['NEW_eRquestType']))
                {
                    $input_params['NEW_eRquestType'] = explode(",", $input_params['NEW_eRquestType']);
                }
                $this->db->where_in("trh.eActionType", $input_params['NEW_eRquestType']);
            }
            if ($input_params['NEW_iTrnRmaId'] != "")
            {
                $this->db->where("trh.iTrnRmaId =", $input_params['NEW_iTrnRmaId']);
            }
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0)
            {
                $i = 0;
                foreach ((array) $result_arr as $data_key => $data_arr)
                {

                    $data = $data_arr["action_date"];
                    if (method_exists($this->general, "dateSystemFormat"))
                    {
                        $data = $this->general->dateSystemFormat($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["action_date"] = $data;

                    $data = $data_arr["action_detail"];
                    if (function_exists("ucfirst"))
                    {
                        $data = call_user_func("ucfirst", $data);
                    }
                    $result_arr[$data_key]["action_detail"] = $data;

                    $i++;
                }
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * get_seller_name method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_seller_name($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("mst_store_detail AS msd");
            $this->db->join("mod_admin AS ma", "msd.iAdminId = ma.iAdminId", "left");

            $select_fields = array();
            $this->db->select("msd.vStoreName AS store_name");
            $this->db->select("ma.vName AS seller_name");
            $this->db->select("ma.vEmail AS seller_email");
            if ($input_params['NEW_iMstStoreDetailId'] != "")
            {
                $this->db->where("msd.iMstStoreDetailId =", $input_params['NEW_iMstStoreDetailId']);
            }
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0)
            {
                $i = 0;
                foreach ((array) $result_arr as $data_key => $data_arr)
                {

                    $data = $data_arr["store_name"];
                    if (function_exists("ucfirst"))
                    {
                        $data = call_user_func("ucfirst", $data);
                    }
                    $result_arr[$data_key]["store_name"] = $data;

                    $data = $data_arr["seller_name"];
                    if (function_exists("ucfirst"))
                    {
                        $data = call_user_func("ucfirst", $data);
                    }
                    $result_arr[$data_key]["seller_name"] = $data;

                    $i++;
                }
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * get_buyer_email method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_buyer_email($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("mst_order AS mo");

            $select_fields = array();
            $this->db->select("mo.vBuyerEmail AS buyer_email");
            $this->db->select("mo.vBuyerName AS buyer_name");
            $this->db->select("(CONCAT_WS('',mo.vOrderNumberPre,mo.iOrderNumber)) AS combined_order_number", FALSE);
            if ($input_params['NEW_iOrderId'] != "")
            {
                $this->db->where("mo.iMstOrderId =", $input_params['NEW_iOrderId']);
            }
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0)
            {
                $i = 0;
                foreach ((array) $result_arr as $data_key => $data_arr)
                {

                    $data = $data_arr["buyer_name"];
                    if (function_exists("ucfirst"))
                    {
                        $data = call_user_func("ucfirst", $data);
                    }
                    $result_arr[$data_key]["buyer_name"] = $data;

                    $i++;
                }
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * get_sub_order method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_sub_order($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("mst_sub_order AS mso");

            $select_fields = array();
            $this->db->select("mso.iMstOrderId AS order_id");
            $this->db->select("mso.iMstStoreDetailId AS store_id");
            $this->db->select("mso.vProductName AS product_name");
            $this->db->select("mso.vProductSku AS product_sku");
            $this->db->select("mso.fProductPrice AS product_price");
            $this->db->select("mso.iProductQty AS product_qty");
            $this->db->select("mso.eCanelBy AS canel_by");
            $this->db->select("mso.fShippingCost AS shipping_cost");
            $this->db->select("mso.eItemStatus AS order_item_status");
            $this->db->select("mso.fTotalCost AS total_cost");
            $this->db->select("mso.vSellerInvoiceDate AS seller_invoice_date");
            $this->db->select("mso.vSellerInvoiceNo AS seller_invoice_no");
            $this->db->select("mso.dShippingDate AS shipping_date");
            $this->db->select("mso.vShipperName AS shipper_name");
            $this->db->select("mso.vTrackingNumber AS tracking_number");
            $this->db->select("mso.dDeliveredDate AS delivered_date");
            $this->db->select("mso.fGatewayTDR AS imei_no");
            $this->db->select("(CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber)) AS combined_sub_order_number", FALSE);
            if ($input_params['NEW_iSubOrderId'] != "")
            {
                $this->db->where("mso.iMstSubOrderId =", $input_params['NEW_iSubOrderId']);
            }
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0)
            {
                $i = 0;
                foreach ((array) $result_arr as $data_key => $data_arr)
                {

                    $data = $data_arr["product_name"];
                    if (function_exists("ucfirst"))
                    {
                        $data = call_user_func("ucfirst", $data);
                    }
                    $result_arr[$data_key]["product_name"] = $data;

                    $data = $data_arr["product_price"];
                    if (method_exists($this->general, "get_formated_currency_details"))
                    {
                        $data = $this->general->get_formated_currency_details($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["product_price"] = $data;

                    $data = $data_arr["shipping_cost"];
                    if (method_exists($this->general, "get_formated_currency_details"))
                    {
                        $data = $this->general->get_formated_currency_details($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["shipping_cost"] = $data;

                    $data = $data_arr["total_cost"];
                    if (method_exists($this->general, "get_formated_currency_details"))
                    {
                        $data = $this->general->get_formated_currency_details($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["total_cost"] = $data;

                    $data = $data_arr["seller_invoice_date"];
                    if (method_exists($this->general, "dateSystemFormat"))
                    {
                        $data = $this->general->dateSystemFormat($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["seller_invoice_date"] = $data;

                    $data = $data_arr["shipping_date"];
                    if (method_exists($this->general, "dateSystemFormat"))
                    {
                        $data = $this->general->dateSystemFormat($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["shipping_date"] = $data;

                    $data = $data_arr["shipper_name"];
                    if (function_exists("ucfirst"))
                    {
                        $data = call_user_func("ucfirst", $data);
                    }
                    $result_arr[$data_key]["shipper_name"] = $data;

                    $data = $data_arr["delivered_date"];
                    if (method_exists($this->general, "dateSystemFormat"))
                    {
                        $data = $this->general->dateSystemFormat($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["delivered_date"] = $data;

                    $i++;
                }
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * get_refund_details method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_refund_details($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("trn_rma_refund AS trr");

            $select_fields = array();
            $this->db->select("trr.tReason AS refund_reason");
            $this->db->select("trr.fShipping AS refund_shipping");
            $this->db->select("trr.fRefund AS refund");
            $this->db->select("trr.tCommentToCustomer AS refund_comment_to_customer");
            $this->db->select("trr.dRefundDate AS refund_date");
            $this->db->select("trr.dDate AS refund_added_date");
            $this->db->select("trr.fTotalAmount AS refund_total_amount");
            $this->db->select("trr.eStatus AS refund_status");
            $this->db->select("trr.eType AS refund_type");
            if ($input_params['NEW_iTrnRmaId'] != "")
            {
                $this->db->where("trr.iTrnRmaId =", $input_params['NEW_iTrnRmaId']);
            }
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0)
            {
                $i = 0;
                foreach ((array) $result_arr as $data_key => $data_arr)
                {

                    $data = $data_arr["refund_reason"];
                    if (function_exists("ucfirst"))
                    {
                        $data = call_user_func("ucfirst", $data);
                    }
                    $result_arr[$data_key]["refund_reason"] = $data;

                    $data = $data_arr["refund_shipping"];
                    if (method_exists($this->general, "get_formated_currency_details"))
                    {
                        $data = $this->general->get_formated_currency_details($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["refund_shipping"] = $data;

                    $data = $data_arr["refund"];
                    if (method_exists($this->general, "get_formated_currency_details"))
                    {
                        $data = $this->general->get_formated_currency_details($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["refund"] = $data;

                    $data = $data_arr["refund_comment_to_customer"];
                    if (function_exists("ucfirst"))
                    {
                        $data = call_user_func("ucfirst", $data);
                    }
                    $result_arr[$data_key]["refund_comment_to_customer"] = $data;

                    $data = $data_arr["refund_date"];
                    if (method_exists($this->general, "dateSystemFormat"))
                    {
                        $data = $this->general->dateSystemFormat($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["refund_date"] = $data;

                    $data = $data_arr["refund_added_date"];
                    if (method_exists($this->general, "dateSystemFormat"))
                    {
                        $data = $this->general->dateSystemFormat($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["refund_added_date"] = $data;

                    $data = $data_arr["refund_total_amount"];
                    if (method_exists($this->general, "get_formated_currency_details"))
                    {
                        $data = $this->general->get_formated_currency_details($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["refund_total_amount"] = $data;

                    $i++;
                }
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * return_request_buyer method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function return_request_buyer($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["buyer_email"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_name"];
            $email_arr["reason"] = $input_params["NEW_tRequestDetail"];
            $email_arr["reason_image"] = $input_params["image_url"];
            $email_arr["transaction_no"] = $input_params["rma_number"];

            $success = $this->general->sendMail($email_arr, "RMA_BUYER_PENDING_RETURN");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * return_request_vendor method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function return_request_vendor($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["seller_email"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["reason"] = $input_params["NEW_tRequestDetail"];
            $email_arr["reason_image"] = $input_params["image_url"];
            $email_arr["transaction_no"] = $input_params["rma_number"];

            $success = $this->general->sendMail($email_arr, "RMA_VENDOR_PENDING_RETURN");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * replace_request_buyer method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function replace_request_buyer($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["buyer_email"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["reason"] = $input_params["NEW_tRequestDetail"];
            $email_arr["reason_image"] = $input_params["image_url"];
            $email_arr["transaction_no"] = $input_params["rma_number"];

            $success = $this->general->sendMail($email_arr, "RMA_BUYER_PENDING_REPLACE");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * replace_request_vendor method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function replace_request_vendor($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["seller_email"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["reason"] = $input_params["NEW_tRequestDetail"];
            $email_arr["reason_image"] = $input_params["image_url"];
            $email_arr["transaction_no"] = $input_params["rma_number"];

            $success = $this->general->sendMail($email_arr, "RMA_VENDOR_PENDING_REPLACE");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_preference method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_preference($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_ePreference"];
            $cc_ro_0 = "Return";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * return_request_accepted_buyer method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function return_request_accepted_buyer($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["buyer_email"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["accept_date"] = $input_params["action_date"];
            $email_arr["accept_message"] = $input_params["action_detail"];
            $email_arr["transaction_no"] = $input_params["rma_number"];

            $success = $this->general->sendMail($email_arr, "RMA_RETURN_ACCEPT_USER");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * rma_return_request_reject_user method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function rma_return_request_reject_user($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["buyer_email"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["reject_reason"] = $input_params["action_detail"];
            $email_arr["reject_date"] = $input_params["action_date"];
            $email_arr["transaction_no"] = $input_params["rma_number"];

            $success = $this->general->sendMail($email_arr, "RMA_RETURN_REQUEST_REJECT_USER");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * return_received_user method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function return_received_user($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["buyer_email"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["received_date"] = $input_params["action_date"];

            $success = $this->general->sendMail($email_arr, "RMA_RETURN_RECEIVED_USER");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * rma_replaced_user method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function rma_replaced_user($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["buyer_email"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = "";
            $email_arr["vCCName"] = "";
            $email_arr["vBCCEmail"] = "";
            $email_arr["vBCCName"] = "";
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["ammount_shipping_concession"] = $input_params["refund_shipping"];
            $email_arr["return_amount"] = $input_params["refund"];
            $email_arr["total_amount"] = $input_params["refund_total_amount"];

            $success = $this->general->sendMail($email_arr, "RMA_REPLACED_USER");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * rma_replaced_admin method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function rma_replaced_admin($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = "".$this->config->item("EMAIL_ADMIN")."";
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["ammount_shipping_concession"] = $input_params["refund_shipping"];
            $email_arr["return_amount"] = $input_params["refund"];
            $email_arr["total_amount"] = $input_params["refund_total_amount"];

            $success = $this->general->sendMail($email_arr, "RMA_REPLACED_ADMIN");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * rma_refund_buyer method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function rma_refund_buyer($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["buyer_email"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["ammount_shipping_concession"] = $input_params["refund_shipping"];
            $email_arr["return_amount"] = $input_params["refund"];
            $email_arr["total_amount"] = $input_params["refund_total_amount"];
            $email_arr["transaction_no"] = $input_params["rma_number"];
            $email_arr["comment_to_customer"] = $input_params["refund_comment_to_customer"];

            $success = $this->general->sendMail($email_arr, "RMA_REFUND_BUYER");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * rma_refund_admin method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function rma_refund_admin($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = "".$this->config->item("EMAIL_ADMIN")."";
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["refund_date"] = $input_params["action_date"];
            $email_arr["transaction_no"] = $input_params["rma_number"];

            $success = $this->general->sendMail($email_arr, "RMA_REFUND_ADMIN");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * email_notification_1 method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function email_notification_1($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["buyer_email"];
            $email_arr["vFromEmail"] = $input_params[""];
            $email_arr["vFromName"] = $input_params[""];
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%REQUEST.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.site_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["refund_date"] = $input_params["action_date"];
            $email_arr["transaction_no"] = $input_params["rma_number"];

            $success = $this->general->sendMail($email_arr, "RMA_CLOSED_BUYER");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * rma_closed_admin method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function rma_closed_admin($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = "".$this->config->item("EMAIL_ADMIN")."";
            $email_arr["vFromEmail"] = $input_params[""];
            $email_arr["vFromName"] = $input_params[""];
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["order_id"] = $input_params["combined_order_number"];
            $email_arr["sub_order_item_id"] = $input_params["combined_sub_order_number"];
            $email_arr["product_name"] = $input_params["product_name"];
            $email_arr["qty"] = $input_params["product_qty"];
            $email_arr["price"] = $input_params["product_price"];
            $email_arr["shipping"] = $input_params["shipping_cost"];
            $email_arr["seller_name"] = $input_params["seller_name"];
            $email_arr["customer_name"] = $input_params["buyer_name"];
            $email_arr["order_status"] = $input_params["order_item_status"];
            $email_arr["total_price"] = $input_params["total_cost"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";
            $email_arr["SYSTEM.site_url"] = "{%SYSTEM.admin_url%}";
            $email_arr["product_sku"] = $input_params["product_sku"];
            $email_arr["refund_date"] = $input_params["action_date"];
            $email_arr["transaction_no"] = $input_params["rma_number"];

            $success = $this->general->sendMail($email_arr, "RMA_CLOSE_ADMIN");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_closed method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_closed($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_eRquestType"];
            $cc_ro_0 = "Close";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_refunded method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_refunded($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_eRquestType"];
            $cc_ro_0 = "Refunded";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_replaced method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_replaced($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_eRquestType"];
            $cc_ro_0 = "Replaced";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_received method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_received($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_eRquestType"];
            $cc_ro_0 = "Received";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_rejected method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_rejected($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_eRquestType"];
            $cc_ro_0 = "Rejected";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_accepted method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_accepted($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_eRquestType"];
            $cc_ro_0 = "Accepted";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_pending method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_pending($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_eRquestType"];
            $cc_ro_0 = "Pending";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * is_records method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function is_records($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_iTrnRmaId"];
            $cc_ro_0 = "0";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("integer", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("integer", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("gt", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            $cc_lo_1 = $input_params["NEW_iOrderId"];
            $cc_ro_1 = "0";

            $cc_lo_1 = is_null($cc_lo_1) ? "" : $cc_lo_1;
            $cc_ro_1 = is_null($cc_ro_1) ? "" : $cc_ro_1;

            $cc_pl_1 = $this->general->getDataTypeWiseResult("integer", $cc_lo_1, TRUE);
            $cc_pr_1 = $this->general->getDataTypeWiseResult("integer", $cc_ro_1, FALSE);
            $cc_fr_1 = $this->general->compareDataValues("gt", $cc_pl_1, $cc_pr_1);
            if (!$cc_fr_1)
            {
                throw new Exception("Some condition does not match.");
            }
            $cc_lo_2 = $input_params["NEW_iSubOrderId"];
            $cc_ro_2 = "0";

            $cc_lo_2 = is_null($cc_lo_2) ? "" : $cc_lo_2;
            $cc_ro_2 = is_null($cc_ro_2) ? "" : $cc_ro_2;

            $cc_pl_2 = $this->general->getDataTypeWiseResult("integer", $cc_lo_2, TRUE);
            $cc_pr_2 = $this->general->getDataTypeWiseResult("integer", $cc_ro_2, FALSE);
            $cc_fr_2 = $this->general->compareDataValues("gt", $cc_pl_2, $cc_pr_2);
            if (!$cc_fr_2)
            {
                throw new Exception("Some condition does not match.");
            }
            $cc_lo_3 = $input_params["NEW_eRquestType"];
            $cc_ro_3 = $input_params["OLD_eRquestType"];

            $cc_lo_3 = is_null($cc_lo_3) ? "" : $cc_lo_3;
            $cc_ro_3 = is_null($cc_ro_3) ? "" : $cc_ro_3;

            $cc_pl_3 = $this->general->getDataTypeWiseResult("string", $cc_lo_3, TRUE);
            $cc_pr_3 = $this->general->getDataTypeWiseResult("string", $cc_ro_3, FALSE);
            $cc_fr_3 = $this->general->compareDataValues("ne", $cc_pl_3, $cc_pr_3);
            if (!$cc_fr_3)
            {
                throw new Exception("Some condition does not match.");
            }
            $cc_lo_4 = $input_params["NEW_eRquestType"];
            $cc_ro_4 = "";

            $cc_lo_4 = is_null($cc_lo_4) ? "" : $cc_lo_4;
            $cc_ro_4 = is_null($cc_ro_4) ? "" : $cc_ro_4;

            $cc_pl_4 = $this->general->getDataTypeWiseResult("string", $cc_lo_4, TRUE);
            $cc_pr_4 = $this->general->getDataTypeWiseResult("string", $cc_ro_4, FALSE);
            $cc_fr_4 = $this->general->compareDataValues("ne", $cc_pl_4, $cc_pr_4);
            if (!$cc_fr_4)
            {
                throw new Exception("Some condition does not match.");
            }
            $cc_lo_5 = $input_params["NEW_iMstStoreDetailId"];
            $cc_ro_5 = "0";

            $cc_lo_5 = is_null($cc_lo_5) ? "" : $cc_lo_5;
            $cc_ro_5 = is_null($cc_ro_5) ? "" : $cc_ro_5;

            $cc_pl_5 = $this->general->getDataTypeWiseResult("integer", $cc_lo_5, TRUE);
            $cc_pr_5 = $this->general->getDataTypeWiseResult("integer", $cc_ro_5, FALSE);
            $cc_fr_5 = $this->general->compareDataValues("gt", $cc_pl_5, $cc_pr_5);
            if (!$cc_fr_5)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0 && $cc_fr_1 && $cc_fr_2 && $cc_fr_3 && $cc_fr_4 && $cc_fr_5))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
