<?php
/**
 * Description of News Letter For Campaign Model
 *
 * @module News Letter For Campaign
 *
 * @class model_news_letter_for_campaign.php
 *
 * @path applicationront
otification\models\model_news_letter_for_campaign.php
 *
 * @author CIT Dev Team
 *
 * @date 23.02.2016
 */

class Model_news_letter_for_campaign extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("trn_newsletter_campaign AS tnc");
            $this->db->join("mst_newsletter_subscription AS mns", "tnc.iNsGroupId = mns.iNewsletterGroupId", "left");
            $this->db->join("mst_newsletter_template AS mnt", "tnc.iMstNewsletterTemplateId = mnt.iMstNewsletterTemplateId", "left");

            $select_fields = array();
            $this->db->select("tnc.vTitle AS tnc_title");
            $this->db->select("tnc.dSendDate AS tnc_send_date");
            $this->db->select("tnc.iMstNewsletterTemplateId AS tnc_mst_newsletter_template_id");
            $this->db->select("mns.vName AS mns_name");
            $this->db->select("mns.vEmail AS mns_email");
            $this->db->select("mnt.vSenderName AS mnt_sender_name");
            $this->db->select("mnt.vSenderEmail AS mnt_sender_email");
            $this->db->select("mnt.tMessage AS mnt_message");
            $this->db->select("mnt.vSubject AS mnt_subject");
            $this->db->select("tnc.iTrnNewsletterCampaignId AS tnc_trn_newsletter_campaign_id_1");
            $this->db->where("tnc.dSendDate = (CURDATE())", FALSE, FALSE);
            $this->db->where_in("tnc.eStatus", array('Publish'));
            $this->db->where_in("tnc.eSendStatus", array('Pending'));
            $this->db->where_in("mns.eStatus", array('Subscribe'));

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }

            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * email_notification method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function email_notification($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["mns_email"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["campain_title"] = $input_params["tnc_title"];
            $email_arr["receiver_name "] = $input_params["mns_name"];
            $email_arr["receiver_email"] = $input_params["mns_email"];
            $email_arr["message"] = $input_params["mnt_message"];
            $email_arr["subject"] = $input_params["mnt_subject"];

            $success = $this->general->sendMail($email_arr, "NEWSLETTER_CAMPAIN_TEMPLATE");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * update_sent method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function update_sent($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $eSendStatus = "Sent";
            if ($input_params['tnc_trn_newsletter_campaign_id'] != "")
            {
                $this->db->where("iTrnNewsletterCampaignId =", $input_params['tnc_trn_newsletter_campaign_id']);
            }
            $this->db->set("eSendStatus", $eSendStatus);
            $res = $this->db->update("trn_newsletter_campaign");
            $affected_rows = $this->db->affected_rows();
            if (!$res || $affected_rows == -1)
            {
                throw new Exception("Failure in updation.");
            }
            $result_param = "affected_rows";
            $result_arr[$result_param] = $affected_rows;
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->db->flush_cache();
        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array())
    {
        try
        {

            $cc_lo_0 = (empty($input_params["query"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
