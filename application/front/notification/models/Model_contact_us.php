<?php
/**
 * Description of Contact US Model
 *
 * @module Contact US
 *
 * @class model_contact_us.php
 *
 * @path applicationront
otification\models\model_contact_us.php
 *
 * @author CIT Dev Team
 *
 * @date 23.02.2016
 */

class Model_contact_us extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("trn_messages AS tm");
            $this->db->join("mst_store_detail AS msd", "tm.iSellerId = msd.iMstStoreDetailId", "left");
            $this->db->join("mod_admin AS ma", "msd.iAdminId = ma.iAdminId", "left");

            $select_fields = array();
            $this->db->select("tm.eToWhom AS tm_to_whom");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("ma.vUserName AS ma_user_name");
            if ($input_params['NEW_iTrnMessagesId'] != "")
            {
                $this->db->where("tm.iTrnMessagesId =", $input_params['NEW_iTrnMessagesId']);
            }
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }

            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * email_notification method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function email_notification($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = "support@jerrylocal.co.nz";
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["NAME"] = $input_params["NEW_vSenderName"];
            $email_arr["EMAIL"] = $input_params["NEW_vSenderEmail"];
            $email_arr["COMMENT"] = $input_params["NEW_tDetail"];

            $success = $this->general->sendMail($email_arr, "CONTACT_US");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * email_notification_1 method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function email_notification_1($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["ma_user_name"];
            $email_arr["vFromEmail"] = "";
            $email_arr["vFromName"] = "";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "";

            $email_arr["NAME"] = $input_params["NEW_vSenderName"];
            $email_arr["EMAIL"] = $input_params["NEW_vSenderEmail"];
            $email_arr["COMMENT"] = $input_params["NEW_tDetail"];
            $email_arr["SELLEREMAIL"] = $input_params["ma_user_name"];
            $email_arr["STORENAME"] = $input_params["msd_store_name"];

            $success = $this->general->sendMail($email_arr, "CONTACT_US_SELLER");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * branch_condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function branch_condition($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["NEW_eToWhom"];
            $cc_ro_0 = "Admin";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
