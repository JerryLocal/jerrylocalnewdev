<?php
/**
 * Description of User Registration Model
 *
 * @module User Registration
 *
 * @class model_user_registration.php
 *
 * @path applicationront
otification\models\model_user_registration.php
 *
 * @author CIT Dev Team
 *
 * @date 12.01.2016
 */

class Model_user_registration extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
    }

    /**
     * email_notification method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function email_notification($input_params = array())
    {
        try
        {

            $email_arr["vEmail"] = $input_params["NEW_vEmail"];
            $email_arr["vFromEmail"] = "cit.email003@gmail.com";
            $email_arr["vFromName"] = "Jerry Local Market Place";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "Your Account is Created With US";

            $email_arr["firstname"] = $input_params["NEW_vFirstName"];
            $email_arr["lastname"] = $input_params["NEW_vLastName"];
            $email_arr["vEmail"] = $input_params["NEW_vEmail"];
            $email_arr["password"] = $input_params["NEW_vPassword"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "{%SYSTEM.COMPANY_NAME%}";

            $success = $this->general->sendMail($email_arr, "USER_REG_FRONT");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
