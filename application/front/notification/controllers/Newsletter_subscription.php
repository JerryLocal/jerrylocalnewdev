<?php
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Description of Newsletter Subscription Controller
 *
 * @module Newsletter Subscription
 *
 * @class newsletter_subscription.php
 *
 * @path applicationront
otification\controllers\newsletter_subscription.php
 *
 * @author CIT Dev Team
 *
 * @date 23.02.2016
 */

class Newsletter_subscription extends HB_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('notifyresponse');
        $this->load->model('model_newsletter_subscription');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array())
    {
        try
        {
            $output_response = array();
            $input_params = $request_arr;
            $output_array = array();

            //logging input params
            $this->notifyresponse->pushDebugParams("input_params", $input_params, $input_params, "is_subscription_id_exist");
            $output_response = $this->is_subscription_id_exist($input_params);
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * is_subscription_id_exist method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_subscription_id_exist($input_params = array())
    {

        $output_arr = $this->model_newsletter_subscription->is_subscription_id_exist($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_subscription_id_exist", $output_arr, $input_params, "custom_function");
            return $this->custom_function($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_subscription_id_exist", $output_arr, $input_params, "finish_success");
            return $this->finish_success($input_params);
        }
    }

    /**
     * custom_function method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function custom_function($input_params = array())
    {
        if (method_exists($this->general, "generateActivationCode"))
        {
            $result_arr["data"] = $this->general->generateActivationCode($input_params);
            $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["custom_function"] = $this->notifyresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "custom_function";
            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->notifyresponse->pushDebugParams("custom_function", $result_arr, $input_params, "generateunsubscribeurl");
        return $this->generateunsubscribeurl($input_params);
    }

    /**
     * generateunsubscribeurl method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function generateunsubscribeurl($input_params = array())
    {
        if (method_exists($this->general, "generateUnsubscribeURL"))
        {
            $result_arr["data"] = $this->general->generateUnsubscribeURL($input_params);
            $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["generateunsubscribeurl"] = $this->notifyresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "generateunsubscribeurl";
            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->notifyresponse->pushDebugParams("generateunsubscribeurl", $result_arr, $input_params, "update_unsubscribe_code");
        return $this->update_unsubscribe_code($input_params);
    }

    /**
     * update_unsubscribe_code method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function update_unsubscribe_code($input_params = array())
    {

        $output_arr = $this->model_newsletter_subscription->update_unsubscribe_code($input_params, $this->settings_params);
        $input_params["update_unsubscribe_code"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("update_unsubscribe_code", $output_arr, $input_params, "is_updated");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "update_unsubscribe_code";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->is_updated($input_params);
    }

    /**
     * is_updated method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_updated($input_params = array())
    {

        $output_arr = $this->model_newsletter_subscription->is_updated($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_updated", $output_arr, $input_params, "email_notification");
            return $this->email_notification($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_updated", $output_arr, $input_params, "finish_success");
            return $this->finish_success($input_params);
        }
    }

    /**
     * email_notification method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function email_notification($input_params = array())
    {

        $output_arr = $this->model_newsletter_subscription->email_notification($input_params);
        $input_params["email_notification"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("email_notification", $output_arr, $input_params, "finish_success");

        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "",
            "message" => "Success.",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "newsletter_subscription";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->notifyresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->notifyresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
