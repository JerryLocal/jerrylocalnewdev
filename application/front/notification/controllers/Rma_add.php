<?php
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Description of Rma Add Controller
 *
 * @module Rma Add
 *
 * @class rma_add.php
 *
 * @path applicationront
otification\controllers\rma_add.php
 *
 * @author CIT Dev Team
 *
 * @date 23.02.2016
 */

class Rma_add extends HB_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('notifyresponse');
        $this->load->model('model_rma_add');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array())
    {
        try
        {
            $output_response = array();
            $input_params = $request_arr;
            $output_array = array();

            //logging input params
            $this->notifyresponse->pushDebugParams("input_params", $input_params, $input_params, "dynamic_assignment_rma");
            $output_response = $this->dynamic_assignment_rma($input_params);
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * dynamic_assignment_rma method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function dynamic_assignment_rma($input_params = array())
    {
        if (method_exists($this->general, "dynamic_assignment_rma"))
        {
            $result_arr["data"] = $this->general->dynamic_assignment_rma($input_params);
            $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["dynamic_assignment_rma"] = $this->notifyresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "dynamic_assignment_rma";
            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->notifyresponse->pushDebugParams("dynamic_assignment_rma", $result_arr, $input_params, "reason_image");
        return $this->reason_image($input_params);
    }

    /**
     * reason_image method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function reason_image($input_params = array())
    {
        if (method_exists($this->general, "getImageURL"))
        {
            $result_arr["data"] = $this->general->getImageURL($input_params);
            $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["reason_image"] = $this->notifyresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "reason_image";
            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->notifyresponse->pushDebugParams("reason_image", $result_arr, $input_params, "get_rma_number");
        return $this->get_rma_number($input_params);
    }

    /**
     * get_rma_number method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_rma_number($input_params = array())
    {
        if (method_exists($this->general, "get_rma_number"))
        {
            $result_arr["data"] = $this->general->get_rma_number($input_params);
            $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["get_rma_number"] = $this->notifyresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "get_rma_number";
            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->notifyresponse->pushDebugParams("get_rma_number", $result_arr, $input_params, "get_seller_name");
        return $this->get_seller_name($input_params);
    }

    /**
     * get_seller_name method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_seller_name($input_params = array())
    {

        $output_arr = $this->model_rma_add->get_seller_name($input_params, $this->settings_params);
        $input_params["get_seller_name"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("get_seller_name", $output_arr, $input_params, "get_buyer_email");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_seller_name";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->get_buyer_email($input_params);
    }

    /**
     * get_buyer_email method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_buyer_email($input_params = array())
    {

        $output_arr = $this->model_rma_add->get_buyer_email($input_params, $this->settings_params);
        $input_params["get_buyer_email"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("get_buyer_email", $output_arr, $input_params, "get_sub_order");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_buyer_email";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->get_sub_order($input_params);
    }

    /**
     * get_sub_order method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_sub_order($input_params = array())
    {

        $output_arr = $this->model_rma_add->get_sub_order($input_params, $this->settings_params);
        $input_params["get_sub_order"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("get_sub_order", $output_arr, $input_params, "is_records");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_sub_order";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->is_records($input_params);
    }

    /**
     * is_records method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_records($input_params = array())
    {

        $output_arr = $this->model_rma_add->is_records($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_records", $output_arr, $input_params, "is_pending");
            return $this->is_pending($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_records", $output_arr, $input_params, "mst_store_detail_finish_success");
            return $this->mst_store_detail_finish_success($input_params);
        }
    }

    /**
     * is_pending method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_pending($input_params = array())
    {

        $output_arr = $this->model_rma_add->is_pending($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_pending", $output_arr, $input_params, "is_preference");
            return $this->is_preference($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_pending", $output_arr, $input_params, "mst_store_detail_finish_success");
            return $this->mst_store_detail_finish_success($input_params);
        }
    }

    /**
     * is_preference method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_preference($input_params = array())
    {

        $output_arr = $this->model_rma_add->is_preference($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_preference", $output_arr, $input_params, "return_request_buyer");
            return $this->return_request_buyer($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_preference", $output_arr, $input_params, "replace_request_buyer");
            return $this->replace_request_buyer($input_params);
        }
    }

    /**
     * return_request_buyer method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function return_request_buyer($input_params = array())
    {

        $output_arr = $this->model_rma_add->return_request_buyer($input_params);
        $input_params["return_request_buyer"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("return_request_buyer", $output_arr, $input_params, "return_request_vendor");

        return $this->return_request_vendor($input_params);
    }

    /**
     * return_request_vendor method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function return_request_vendor($input_params = array())
    {

        $output_arr = $this->model_rma_add->return_request_vendor($input_params);
        $input_params["return_request_vendor"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("return_request_vendor", $output_arr, $input_params, "mst_store_detail_finish_success");

        return $this->mst_store_detail_finish_success($input_params);
    }

    /**
     * replace_request_buyer method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function replace_request_buyer($input_params = array())
    {

        $output_arr = $this->model_rma_add->replace_request_buyer($input_params);
        $input_params["replace_request_buyer"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("replace_request_buyer", $output_arr, $input_params, "replace_request_vendor");

        return $this->replace_request_vendor($input_params);
    }

    /**
     * replace_request_vendor method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function replace_request_vendor($input_params = array())
    {

        $output_arr = $this->model_rma_add->replace_request_vendor($input_params);
        $input_params["replace_request_vendor"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("replace_request_vendor", $output_arr, $input_params, "mst_store_detail_finish_success");

        return $this->mst_store_detail_finish_success($input_params);
    }

    /**
     * mst_store_detail_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_store_detail_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "",
            "message" => "Data saved successfully.",
        );
        $output_fields = array(
            'store_name',
            'seller_name',
            'seller_email',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "rma_add";
        $func_array["function"]["output_keys"] = array(
            'get_seller_name',
        );
        $func_array["function"]["output_alias"] = array(
            "store_name" => "store_name",
            "seller_name" => "seller_name",
            "seller_email" => "seller_email",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->notifyresponse->pushDebugParams("mst_store_detail_finish_success", array(), $input_params, "");
        $responce_arr = $this->notifyresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
