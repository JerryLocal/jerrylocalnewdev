<?php
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Description of Sub Order Add Controller
 *
 * @module Sub Order Add
 *
 * @class sub_order_add.php
 *
 * @path applicationront
otification\controllers\sub_order_add.php
 *
 * @author CIT Dev Team
 *
 * @date 23.02.2016
 */

class Sub_order_add extends HB_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('notifyresponse');
        $this->load->model('model_sub_order_add');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array())
    {
        try
        {
            $output_response = array();
            $input_params = $request_arr;
            $output_array = array();

            //logging input params
            $this->notifyresponse->pushDebugParams("input_params", $input_params, $input_params, "is_records");
            $output_response = $this->is_records($input_params);
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * is_records method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_records($input_params = array())
    {

        $output_arr = $this->model_sub_order_add->is_records($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_records", $output_arr, $input_params, "dynamic_assignment_sub_order");
            return $this->dynamic_assignment_sub_order($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_records", $output_arr, $input_params, "mst_store_detail_finish_success");
            return $this->mst_store_detail_finish_success($input_params);
        }
    }

    /**
     * dynamic_assignment_sub_order method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function dynamic_assignment_sub_order($input_params = array())
    {
        if (method_exists($this->general, "dynamic_assignment_sub_order"))
        {
            $result_arr["data"] = $this->general->dynamic_assignment_sub_order($input_params);
            $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["dynamic_assignment_sub_order"] = $this->notifyresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "dynamic_assignment_sub_order";
            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->notifyresponse->pushDebugParams("dynamic_assignment_sub_order", $result_arr, $input_params, "get_seller_name");
        return $this->get_seller_name($input_params);
    }

    /**
     * get_seller_name method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_seller_name($input_params = array())
    {

        $output_arr = $this->model_sub_order_add->get_seller_name($input_params, $this->settings_params);
        $input_params["get_seller_name"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("get_seller_name", $output_arr, $input_params, "get_buyer_email");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_seller_name";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->get_buyer_email($input_params);
    }

    /**
     * get_buyer_email method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_buyer_email($input_params = array())
    {

        $output_arr = $this->model_sub_order_add->get_buyer_email($input_params, $this->settings_params);
        $input_params["get_buyer_email"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("get_buyer_email", $output_arr, $input_params, "get_order_number");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_buyer_email";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->get_order_number($input_params);
    }

    /**
     * get_order_number method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_order_number($input_params = array())
    {

        $output_arr = $this->model_sub_order_add->get_order_number($input_params, $this->settings_params);
        $input_params["get_order_number"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("get_order_number", $output_arr, $input_params, "custom_function");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_order_number";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->custom_function($input_params);
    }

    /**
     * custom_function method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function custom_function($input_params = array())
    {
        if (method_exists($this->general, "get_suborder_number"))
        {
            $result_arr["data"] = $this->general->get_suborder_number($input_params);
            $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["custom_function"] = $this->notifyresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "custom_function";
            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->notifyresponse->pushDebugParams("custom_function", $result_arr, $input_params, "email_notification");
        return $this->email_notification($input_params);
    }

    /**
     * email_notification method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function email_notification($input_params = array())
    {

        $output_arr = $this->model_sub_order_add->email_notification($input_params);
        $input_params["email_notification"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("email_notification", $output_arr, $input_params, "notification_seller_pending");

        return $this->notification_seller_pending($input_params);
    }

    /**
     * notification_seller_pending method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function notification_seller_pending($input_params = array())
    {

        $output_arr = $this->model_sub_order_add->notification_seller_pending($input_params);
        $input_params["notification_seller_pending"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("notification_seller_pending", $output_arr, $input_params, "notification_admin_pending");

        return $this->notification_admin_pending($input_params);
    }

    /**
     * notification_admin_pending method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function notification_admin_pending($input_params = array())
    {

        $output_arr = $this->model_sub_order_add->notification_admin_pending($input_params);
        $input_params["notification_admin_pending"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("notification_admin_pending", $output_arr, $input_params, "mst_store_detail_finish_success");

        return $this->mst_store_detail_finish_success($input_params);
    }

    /**
     * mst_store_detail_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_store_detail_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "",
            "message" => "Data saved successfully.",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "sub_order_add";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array(
            "store_name" => "store_name",
            "seller_name" => "seller_name",
            "seller_email" => "seller_email",
            "buyer_email" => "buyer_email",
            "buyer_name" => "buyer_name",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->notifyresponse->pushDebugParams("mst_store_detail_finish_success", array(), $input_params, "");
        $responce_arr = $this->notifyresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
