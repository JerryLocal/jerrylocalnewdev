<?php
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Description of News Letter For Campaign Controller
 *
 * @module News Letter For Campaign
 *
 * @class news_letter_for_campaign.php
 *
 * @path applicationront
otification\controllers\news_letter_for_campaign.php
 *
 * @author CIT Dev Team
 *
 * @date 29.02.2016
 */

class News_letter_for_campaign extends HB_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('notifyresponse');
        $this->load->model('model_news_letter_for_campaign');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array())
    {
        try
        {
            $output_response = array();
            $input_params = $request_arr;
            $output_array = array();

            //logging input params
            $this->notifyresponse->pushDebugParams("input_params", $input_params, $input_params, "query");
            $output_response = $this->query($input_params);
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array())
    {

        $output_arr = $this->model_news_letter_for_campaign->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("query", $output_arr, $input_params, "condition");
        $this->multiple_keys[] = "query";
        $this->notifyresponse->makeUniqueParams($this->multiple_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array())
    {

        $output_arr = $this->model_news_letter_for_campaign->condition($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("condition", $output_arr, $input_params, "start_loop");
            return $this->start_loop($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("condition", $output_arr, $input_params, "trn_newsletter_campaign_finish_success");
            return $this->trn_newsletter_campaign_finish_success($input_params);
        }
    }

    /**
     * start_loop method is used to process loop flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function start_loop($input_params = array())
    {
        $this->iterate_start_loop($input_params["query"], $input_params);

        $this->notifyresponse->pushDebugParams("end_loop", array(), $input_params, "finish_success", "query");
        return $this->finish_success($input_params);
    }

    /**
     * generateunsubscri method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function generateunsubscri($input_params = array())
    {
        if (method_exists($this->general, "generateUnsubscribeURL"))
        {
            $result_arr["data"] = $this->general->generateUnsubscribeURL($input_params);
            $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["generateunsubscri"] = $this->notifyresponse->assignFunctionResponse($result_arr);
        }
        $this->notifyresponse->pushDebugParams("generateunsubscri", $result_arr, $input_params, "email_notification", "query");
        return $this->email_notification($input_params);
    }

    /**
     * email_notification method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function email_notification($input_params = array())
    {

        $output_arr = $this->model_news_letter_for_campaign->email_notification($input_params);
        $input_params["email_notification"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("email_notification", $output_arr, $input_params, "update_sent", "query");

        return $this->update_sent($input_params);
    }

    /**
     * update_sent method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function update_sent($input_params = array())
    {

        $output_arr = $this->model_news_letter_for_campaign->update_sent($input_params, $this->settings_params);
        $input_params["update_sent"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("update_sent", $output_arr, $input_params, "finish_success", "query", "end_loop");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);

        return $input_params;
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "",
            "message" => "Data saved successfully.",
        );
        $output_fields = array(
            'tnc_title',
            'tnc_send_date',
            'tnc_mst_newsletter_template_id',
            'mns_name',
            'mns_email',
            'mnt_sender_name',
            'mnt_sender_email',
            'mnt_message',
            'mnt_subject',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "news_letter_for_campaign";
        $func_array["function"]["output_keys"] = array(
            'query',
        );
        $func_array["function"]["output_alias"] = array(
            "tnc_title" => "tnc_title",
            "tnc_send_date" => "tnc_send_date",
            "tnc_mst_newsletter_template_id" => "tnc_mst_newsletter_template_id",
            "mns_name" => "mns_name",
            "mns_email" => "mns_email",
            "mnt_sender_name" => "mnt_sender_name",
            "mnt_sender_email" => "mnt_sender_email",
            "mnt_message" => "mnt_message",
            "mnt_subject" => "mnt_subject",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->notifyresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->notifyresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * trn_newsletter_campaign_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function trn_newsletter_campaign_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "",
            "message" => "No data found.",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "news_letter_for_campaign";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array(
            "tnc_title" => "tnc_title",
            "tnc_send_date" => "tnc_send_date",
            "tnc_mst_newsletter_template_id" => "tnc_mst_newsletter_template_id",
            "mns_name" => "mns_name",
            "mns_email" => "mns_email",
            "mnt_sender_name" => "mnt_sender_name",
            "mnt_sender_email" => "mnt_sender_email",
            "mnt_message" => "mnt_message",
            "mnt_subject" => "mnt_subject",
            "tnc_trn_newsletter_campaign_id_1" => "tnc_trn_newsletter_campaign_id_1",
            "update_sent" => "update_sent",
            "affected_rows" => "affected_rows",
        );
        $func_array["function"]["inner_keys"] = array(
            'update_sent',
        );
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->notifyresponse->pushDebugParams("trn_newsletter_campaign_finish_success", array(), $input_params, "");
        $responce_arr = $this->notifyresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * iterate_start_loop method is used to iterate loop.
     *
     * @param array $query_lp_arr query_lp_arr array to iterate loop.
     * @param array $input_params_addr $input_params_addr array to address original input params.
     */
    public function iterate_start_loop(&$query_lp_arr = array(), &$input_params_addr = array())
    {

        $_loop_params_tmp = $_loop_tmp_dict = array();
        $input_params_loc = $input_params_addr;
        $_loop_params_loc = $query_lp_arr;
        $_lp_ini = 0;
        $_lp_end = count($_loop_params_loc);
        $_lp_cnf = (is_array($_loop_params_loc[0])) ? TRUE : FALSE;
        $_loop_debug_tmp = array(
            "start_point" => $_lp_ini,
            "end_point" => $_lp_end,
            "step" => 1,
            "loop" => "query",
        );
        $this->notifyresponse->pushDebugParams("start_loop", $_loop_debug_tmp, $input_params, "generateunsubscri");
        for ($i = $_lp_ini; $i < $_lp_end; $i += 1)
        {
            $query_lp_pms = $input_params_loc;
            if ($_lp_cnf)
            {
                if (is_array($_loop_params_loc[$i]))
                {
                    $query_lp_pms = $_loop_params_loc[$i]+$input_params_loc;
                }
                unset($query_lp_pms["query"]);
            }
            else
            {
                $query_lp_pms["query"] = $_loop_params_loc[$i];
            }
            $query_lp_pms["i"] = $i;
            $query_lp_pms["__dictionaries"] = $_loop_tmp_dict;
            $input_params = $query_lp_pms;
            $input_params = $this->generateunsubscri($input_params);
            if (is_array($input_params["__dictionaries"]))
            {
                $_loop_tmp_dict = $input_params["__dictionaries"];
                unset($input_params["__dictionaries"]);
            }
            if (is_array($input_params["__variables"]))
            {
                $input_params = $this->notifyresponse->grabLoopVariables($input_params["__variables"], $input_params);
                unset($input_params["__variables"]);
            }
            if ($this->break_continue == 1)
            {
                $this->break_continue = NULL;
                break;
            }
            elseif ($this->break_continue == 2)
            {
                $this->break_continue = NULL;
                continue;
            }
            if ($_lp_cnf)
            {
                $query_lp_arr[$i] = $this->notifyresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $query_lp_pms);
            }
            else
            {
                $_loop_params_tmp[$i] = $this->notifyresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $query_lp_pms);
            }
        }
        if (!$_lp_cnf)
        {
            $input_params_addr["query"] = $_loop_params_tmp;
        }
        if (is_array($_loop_tmp_dict))
        {
            $input_params_addr = array_merge($input_params_addr, $_loop_tmp_dict);
        }
    }
}
