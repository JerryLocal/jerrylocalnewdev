<?php
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Description of Return request Update Controller
 *
 * @module Return request Update
 *
 * @class return_request_update.php
 *
 * @path applicationront
otification\controllers\return_request_update.php
 *
 * @author CIT Dev Team
 *
 * @date 23.02.2016
 */

class Return_request_update extends HB_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('notifyresponse');
        $this->load->model('model_return_request_update');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array())
    {
        try
        {
            $output_response = array();
            $input_params = $request_arr;
            $output_array = array();

            //logging input params
            $this->notifyresponse->pushDebugParams("input_params", $input_params, $input_params, "dynamic_assignment_rma");
            $output_response = $this->dynamic_assignment_rma($input_params);
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * dynamic_assignment_rma method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function dynamic_assignment_rma($input_params = array())
    {
        if (method_exists($this->general, "dynamic_assignment_rma"))
        {
            $result_arr["data"] = $this->general->dynamic_assignment_rma($input_params);
            $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["dynamic_assignment_rma"] = $this->notifyresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "dynamic_assignment_rma";
            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->notifyresponse->pushDebugParams("dynamic_assignment_rma", $result_arr, $input_params, "reason_image");
        return $this->reason_image($input_params);
    }

    /**
     * reason_image method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function reason_image($input_params = array())
    {
        if (method_exists($this->general, "getImageURL"))
        {
            $result_arr["data"] = $this->general->getImageURL($input_params);
            $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["reason_image"] = $this->notifyresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "reason_image";
            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->notifyresponse->pushDebugParams("reason_image", $result_arr, $input_params, "get_rma_number");
        return $this->get_rma_number($input_params);
    }

    /**
     * get_rma_number method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_rma_number($input_params = array())
    {
        if (method_exists($this->general, "get_rma_number"))
        {
            $result_arr["data"] = $this->general->get_rma_number($input_params);
            $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["get_rma_number"] = $this->notifyresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "get_rma_number";
            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->notifyresponse->pushDebugParams("get_rma_number", $result_arr, $input_params, "get_refund_histry_details");
        return $this->get_refund_histry_details($input_params);
    }

    /**
     * get_refund_histry_details method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_refund_histry_details($input_params = array())
    {

        $output_arr = $this->model_return_request_update->get_refund_histry_details($input_params, $this->settings_params);
        $input_params["get_refund_histry_details"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("get_refund_histry_details", $output_arr, $input_params, "get_seller_name");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_refund_histry_details";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->get_seller_name($input_params);
    }

    /**
     * get_seller_name method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_seller_name($input_params = array())
    {

        $output_arr = $this->model_return_request_update->get_seller_name($input_params, $this->settings_params);
        $input_params["get_seller_name"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("get_seller_name", $output_arr, $input_params, "get_buyer_email");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_seller_name";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->get_buyer_email($input_params);
    }

    /**
     * get_buyer_email method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_buyer_email($input_params = array())
    {

        $output_arr = $this->model_return_request_update->get_buyer_email($input_params, $this->settings_params);
        $input_params["get_buyer_email"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("get_buyer_email", $output_arr, $input_params, "get_sub_order");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_buyer_email";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->get_sub_order($input_params);
    }

    /**
     * get_sub_order method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_sub_order($input_params = array())
    {

        $output_arr = $this->model_return_request_update->get_sub_order($input_params, $this->settings_params);
        $input_params["get_sub_order"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("get_sub_order", $output_arr, $input_params, "get_refund_details");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_sub_order";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->get_refund_details($input_params);
    }

    /**
     * get_refund_details method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_refund_details($input_params = array())
    {

        $output_arr = $this->model_return_request_update->get_refund_details($input_params, $this->settings_params);
        $input_params["get_refund_details"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("get_refund_details", $output_arr, $input_params, "is_records");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_refund_details";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->is_records($input_params);
    }

    /**
     * is_records method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_records($input_params = array())
    {

        $output_arr = $this->model_return_request_update->is_records($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_records", $output_arr, $input_params, "is_pending");
            return $this->is_pending($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_records", $output_arr, $input_params, "mst_store_detail_finish_success");
            return $this->mst_store_detail_finish_success($input_params);
        }
    }

    /**
     * is_pending method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_pending($input_params = array())
    {

        $output_arr = $this->model_return_request_update->is_pending($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_pending", $output_arr, $input_params, "is_preference");
            return $this->is_preference($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_pending", $output_arr, $input_params, "is_accepted");
            return $this->is_accepted($input_params);
        }
    }

    /**
     * is_preference method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_preference($input_params = array())
    {

        $output_arr = $this->model_return_request_update->is_preference($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_preference", $output_arr, $input_params, "return_request_buyer");
            return $this->return_request_buyer($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_preference", $output_arr, $input_params, "replace_request_buyer");
            return $this->replace_request_buyer($input_params);
        }
    }

    /**
     * return_request_buyer method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function return_request_buyer($input_params = array())
    {

        $output_arr = $this->model_return_request_update->return_request_buyer($input_params);
        $input_params["return_request_buyer"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("return_request_buyer", $output_arr, $input_params, "return_request_vendor");

        return $this->return_request_vendor($input_params);
    }

    /**
     * return_request_vendor method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function return_request_vendor($input_params = array())
    {

        $output_arr = $this->model_return_request_update->return_request_vendor($input_params);
        $input_params["return_request_vendor"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("return_request_vendor", $output_arr, $input_params, "finish_success");

        return $this->finish_success($input_params);
    }

    /**
     * replace_request_buyer method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function replace_request_buyer($input_params = array())
    {

        $output_arr = $this->model_return_request_update->replace_request_buyer($input_params);
        $input_params["replace_request_buyer"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("replace_request_buyer", $output_arr, $input_params, "replace_request_vendor");

        return $this->replace_request_vendor($input_params);
    }

    /**
     * replace_request_vendor method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function replace_request_vendor($input_params = array())
    {

        $output_arr = $this->model_return_request_update->replace_request_vendor($input_params);
        $input_params["replace_request_vendor"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("replace_request_vendor", $output_arr, $input_params, "finish_success");

        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "",
            "message" => "Data saved successfully.",
        );
        $output_fields = array(
            'store_name',
            'seller_name',
            'seller_email',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "return_request_update";
        $func_array["function"]["output_keys"] = array(
            'get_seller_name',
        );
        $func_array["function"]["output_alias"] = array(
            "store_name" => "store_name",
            "seller_name" => "seller_name",
            "seller_email" => "seller_email",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->notifyresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->notifyresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * is_accepted method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_accepted($input_params = array())
    {

        $output_arr = $this->model_return_request_update->is_accepted($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_accepted", $output_arr, $input_params, "return_request_accepted_buyer");
            return $this->return_request_accepted_buyer($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_accepted", $output_arr, $input_params, "is_rejected");
            return $this->is_rejected($input_params);
        }
    }

    /**
     * return_request_accepted_buyer method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function return_request_accepted_buyer($input_params = array())
    {

        $output_arr = $this->model_return_request_update->return_request_accepted_buyer($input_params);
        $input_params["return_request_accepted_buyer"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("return_request_accepted_buyer", $output_arr, $input_params, "finish_success");

        return $this->finish_success($input_params);
    }

    /**
     * is_rejected method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_rejected($input_params = array())
    {

        $output_arr = $this->model_return_request_update->is_rejected($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_rejected", $output_arr, $input_params, "rma_return_request_reject_user");
            return $this->rma_return_request_reject_user($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_rejected", $output_arr, $input_params, "is_received");
            return $this->is_received($input_params);
        }
    }

    /**
     * rma_return_request_reject_user method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rma_return_request_reject_user($input_params = array())
    {

        $output_arr = $this->model_return_request_update->rma_return_request_reject_user($input_params);
        $input_params["rma_return_request_reject_user"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("rma_return_request_reject_user", $output_arr, $input_params, "finish_success");

        return $this->finish_success($input_params);
    }

    /**
     * is_received method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_received($input_params = array())
    {

        $output_arr = $this->model_return_request_update->is_received($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_received", $output_arr, $input_params, "return_received_user");
            return $this->return_received_user($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_received", $output_arr, $input_params, "is_replaced");
            return $this->is_replaced($input_params);
        }
    }

    /**
     * return_received_user method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function return_received_user($input_params = array())
    {

        $output_arr = $this->model_return_request_update->return_received_user($input_params);
        $input_params["return_received_user"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("return_received_user", $output_arr, $input_params, "finish_success");

        return $this->finish_success($input_params);
    }

    /**
     * is_replaced method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_replaced($input_params = array())
    {

        $output_arr = $this->model_return_request_update->is_replaced($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_replaced", $output_arr, $input_params, "rma_replaced_user");
            return $this->rma_replaced_user($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_replaced", $output_arr, $input_params, "is_refunded");
            return $this->is_refunded($input_params);
        }
    }

    /**
     * rma_replaced_user method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rma_replaced_user($input_params = array())
    {

        $output_arr = $this->model_return_request_update->rma_replaced_user($input_params);
        $input_params["rma_replaced_user"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("rma_replaced_user", $output_arr, $input_params, "rma_replaced_admin");

        return $this->rma_replaced_admin($input_params);
    }

    /**
     * rma_replaced_admin method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rma_replaced_admin($input_params = array())
    {

        $output_arr = $this->model_return_request_update->rma_replaced_admin($input_params);
        $input_params["rma_replaced_admin"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("rma_replaced_admin", $output_arr, $input_params, "finish_success");

        return $this->finish_success($input_params);
    }

    /**
     * is_refunded method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_refunded($input_params = array())
    {

        $output_arr = $this->model_return_request_update->is_refunded($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_refunded", $output_arr, $input_params, "rma_refund_buyer");
            return $this->rma_refund_buyer($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_refunded", $output_arr, $input_params, "is_closed");
            return $this->is_closed($input_params);
        }
    }

    /**
     * rma_refund_buyer method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rma_refund_buyer($input_params = array())
    {

        $output_arr = $this->model_return_request_update->rma_refund_buyer($input_params);
        $input_params["rma_refund_buyer"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("rma_refund_buyer", $output_arr, $input_params, "rma_refund_admin");

        return $this->rma_refund_admin($input_params);
    }

    /**
     * rma_refund_admin method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rma_refund_admin($input_params = array())
    {

        $output_arr = $this->model_return_request_update->rma_refund_admin($input_params);
        $input_params["rma_refund_admin"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("rma_refund_admin", $output_arr, $input_params, "finish_success");

        return $this->finish_success($input_params);
    }

    /**
     * is_closed method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function is_closed($input_params = array())
    {

        $output_arr = $this->model_return_request_update->is_closed($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("is_closed", $output_arr, $input_params, "email_notification_1");
            return $this->email_notification_1($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("is_closed", $output_arr, $input_params, "finish_success");
            return $this->finish_success($input_params);
        }
    }

    /**
     * email_notification_1 method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function email_notification_1($input_params = array())
    {

        $output_arr = $this->model_return_request_update->email_notification_1($input_params);
        $input_params["email_notification_1"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("email_notification_1", $output_arr, $input_params, "rma_closed_admin");

        return $this->rma_closed_admin($input_params);
    }

    /**
     * rma_closed_admin method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rma_closed_admin($input_params = array())
    {

        $output_arr = $this->model_return_request_update->rma_closed_admin($input_params);
        $input_params["rma_closed_admin"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("rma_closed_admin", $output_arr, $input_params, "finish_success");

        return $this->finish_success($input_params);
    }

    /**
     * mst_store_detail_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_store_detail_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "",
            "message" => "Error.",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "return_request_update";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array(
            "image_url" => "image_url",
            "store_name" => "store_name",
            "seller_name" => "seller_name",
            "seller_email" => "seller_email",
            "buyer_email" => "buyer_email",
            "buyer_name" => "buyer_name",
            "combined_order_number" => "combined_order_number",
            "order_id" => "order_id",
            "store_id" => "store_id",
            "product_name" => "product_name",
            "product_sku" => "product_sku",
            "product_price" => "product_price",
            "product_qty" => "product_qty",
            "canel_by" => "canel_by",
            "shipping_cost" => "shipping_cost",
            "order_item_status" => "order_item_status",
            "total_cost" => "total_cost",
            "seller_invoice_date" => "seller_invoice_date",
            "seller_invoice_no" => "seller_invoice_no",
            "shipping_date" => "shipping_date",
            "shipper_name" => "shipper_name",
            "tracking_number" => "tracking_number",
            "delivered_date" => "delivered_date",
            "imei_no" => "imei_no",
            "combined_sub_order_number" => "combined_sub_order_number",
            "refund_reason" => "refund_reason",
            "refund_shipping" => "refund_shipping",
            "refund" => "refund",
            "refund_comment_to_customer" => "refund_comment_to_customer",
            "refund_date" => "refund_date",
            "refund_added_date" => "refund_added_date",
            "refund_total_amount" => "refund_total_amount",
            "refund_status" => "refund_status",
            "refund_type" => "refund_type",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->notifyresponse->pushDebugParams("mst_store_detail_finish_success", array(), $input_params, "");
        $responce_arr = $this->notifyresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
