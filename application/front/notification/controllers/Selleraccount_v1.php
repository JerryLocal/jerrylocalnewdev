<?php
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Description of Seller Approval Controller
 *
 * @module Seller Approval
 *
 * @class selleraccount_v1.php
 *
 * @path applicationront
otification\controllers\selleraccount_v1.php
 *
 * @author CIT Dev Team
 *
 * @date 23.02.2016
 */

class Selleraccount_v1 extends HB_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('notifyresponse');
        $this->load->model('model_selleraccount_v1');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array())
    {
        try
        {
            $output_response = array();
            $input_params = $request_arr;
            $output_array = array();

            //logging input params
            $this->notifyresponse->pushDebugParams("input_params", $input_params, $input_params, "chk_record");
            $output_response = $this->chk_record($input_params);
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * chk_record method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function chk_record($input_params = array())
    {

        $output_arr = $this->model_selleraccount_v1->chk_record($input_params);
        if ($output_arr["success"])
        {
            $this->notifyresponse->pushDebugParams("chk_record", $output_arr, $input_params, "query");
            return $this->query($input_params);
        }
        else
        {
            $this->notifyresponse->pushDebugParams("chk_record", $output_arr, $input_params, "mod_admin_finish_failure");
            return $this->mod_admin_finish_failure($input_params);
        }
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array())
    {

        $output_arr = $this->model_selleraccount_v1->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->notifyresponse->pushDebugParams("query", $output_arr, $input_params, "email_notification");
        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query";
        $this->notifyresponse->makeUniqueParams($this->single_keys);

        return $this->email_notification($input_params);
    }

    /**
     * email_notification method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function email_notification($input_params = array())
    {

        $output_arr = $this->model_selleraccount_v1->email_notification($input_params);
        $input_params["email_notification"] = $output_arr["success"];
        $this->notifyresponse->pushDebugParams("email_notification", $output_arr, $input_params, "finish_success");

        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "",
            "message" => "Success",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "selleraccount_v1";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array(
            "ma_email" => "ma_email",
            "ma_name" => "ma_name",
            "ma_user_name" => "ma_user_name",
            "ma_password" => "ma_password",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->notifyresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->notifyresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mod_admin_finish_failure method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mod_admin_finish_failure($input_params = array())
    {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "",
            "message" => "Failure.",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "selleraccount_v1";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array(
            "ma_email" => "ma_email",
            "ma_name" => "ma_name",
            "ma_user_name" => "ma_user_name",
            "ma_password" => "ma_password",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->notifyresponse->pushDebugParams("mod_admin_finish_failure", array(), $input_params, "");
        $responce_arr = $this->notifyresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
