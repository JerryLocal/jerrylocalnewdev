<?php

/**
 * Description of Content Controller
 * 
 * @module Content
 * 
 * @class content.php
 * 
 * @path application\front\content\controllers\content.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Content extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * index method is used to initialize index function.
     */
    public function index() {
        redirect($this->config->item('site_url'));
    }

    /**
     * staticpage method is used to display static pages.
     */
    public function staticpage($page = '', $lang = 'en') {
        $this->load->model('tools/staticpages');
        $page_code = $page;
        $fields = array("vPageTitle", "vPageCode", "vContent", "tMetaTitle", "tMetaKeyword", "tMetaDesc");
        $render_arr = array();
        if (empty($lang) || $lang == "en" || $this->config->item('MULTI_LINGUAL_PROJECT') != "Yes") {
            $page_details = $this->staticpages->getStaticPageData($page_code, $fields);
            $lang = 'en';
        } else {
            $lang_fields = $this->staticpages->getLangTableFields();
            if (is_array($lang_fields) && count($lang_fields) > 0) {
                $lang_arr = array();
                foreach ($fields as $key => $val) {
                    if (in_array($val, $lang_fields)) {
                        $lang_arr[] = "mps_lang." . $val;
                    } else {
                        $lang_arr[] = "mps." . $val;
                    }
                }
                $page_details = $this->staticpages->getStaticPageLangData($lang, $page_code, $lang_arr);
            }
            if (!is_array($page_details) || count($page_details) == 0) {
                $page_details = $this->staticpages->getStaticPageData($page_code, $fields);
            }
        }
        $render_arr['display_lang'] = $lang;
        $render_arr['page_code'] = $page_code;
        $render_arr['page_title'] = $page_details[0]["vPageTitle"];
        $render_arr['page_content'] = $page_details[0]["vContent"];
        $render_arr['meta_title'] = $page_details[0]["tMetaTitle"];
        $render_arr['meta_keyword'] = $page_details[0]["tMetaKeyword"];
        $render_arr['meta_description'] = $page_details[0]["tMetaDesc"];
        $this->smarty->assign($render_arr);
    }

}
