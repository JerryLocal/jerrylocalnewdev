<div class="breadcrumbs">
<ul>
<li><a href="/JerryLocal/index.html">Home</a>&nbsp;&nbsp; &gt; &nbsp;</li>
<li>Copyright Policy</li>
</ul>
</div>
<p><span style="font-size: 10pt; font-family: verdana, geneva;"><strong>Copy Right Policy</strong></span></p>
<p><span style="font-size: 10pt; font-family: verdana, geneva;">1 &nbsp;<strong>General</strong></span></p>
<p><span style="font-size: 10pt; font-family: verdana, geneva;">1 &nbsp;All information, content, services and software displayed on, transmitted through, or used in connection with the Website/Application or Platform, including for example news articles, reviews, directories, guides, text, photographs, images, illustrations, audio clips, video, html, source and object code, trademarks, logos, and the like (collectively and hereinafter referred to as the "Content"), as well as its selection and arrangement, is owned by Us. You may use the Content only through the Website/Application or Platform, and solely for your personal, non-commercial use.</span></p>
<p><span style="font-size: 10pt; font-family: verdana, geneva;">2 You may not, republish any portion of the Content on any Internet, Intranet or extranet site or incorporate the Content in any database, compilation, archive or cache. You may not distribute any Content to others, whether or not for payment or other consideration, and you may not modify, copy, frame, cache, reproduce, sell, publish, transmit, display or otherwise use any portion of the Content. You may not scrape or otherwise copy our Content without permission. You agree not to decompile, reverse engineer or disassemble any software or other products or processes accessible through the Website/Application or&nbsp; Platform, not to insert any code or product or manipulate the content of the Website/Application/ Platform in any way that affects the user's experience, and not to use any data mining, data gathering or extraction method</span></p>
<p><span style="font-family: verdana, geneva; font-size: 10pt;">3 &nbsp;We respect the intellectual property of others. If You believe your work has been copied in a way that constitutes infringement or are aware of any infringing material on the Application, you can report the infringement by clicking the Report option on the Application and by emailing us at &nbsp;</span><span style="color: #0000ff;"><a href="mailto:support@jerrylocal.co.nz"><span style="color: #0000ff;">support@jerrylocal.co.nz</span></a> </span><span style="font-family: verdana, geneva; font-size: 10pt;">and by providing the following:</span></p>
<ul>
<li><span style="font-size: 10pt; font-family: verdana, geneva;">A statement that you have identified Content on the Platform that infringes your copyright or the copyright of a third party on whose behalf you are entitled to act;</span></li>
<li><span style="font-size: 10pt; font-family: verdana, geneva;">A description of the copyright work that you claim has been infringed;</span></li>
<li><span style="font-size: 10pt; font-family: verdana, geneva;">A description of the Content that you claim is infringing and details of where on the Platform the Content that you claim is infringing may be found;</span></li>
<li><span style="font-size: 10pt; font-family: verdana, geneva;">Your contact information including your full name, address and telephone number and a valid email address on which you can be contacted;</span></li>
<li><span style="font-size: 10pt; font-family: verdana, geneva;">A statement by you that you have a good faith belief that the disputed use of the material is not authorized by the copyright owner, its agent, or the law;</span></li>
<li><span style="font-size: 10pt; font-family: verdana, geneva;">A statement by you that the information in your notice is accurate and that you are authorized to act on behalf of the owner of the exclusive right that is allegedly infringed.</span></li>
<li><span style="font-size: 10pt; font-family: verdana, geneva;">your electronic or physical signature (which may be a scanned copy).</span></li>
</ul>
<p><span style="font-size: 10pt; font-family: verdana, geneva;">4 &nbsp;By providing information to, communicating with, and/or placing material on, the Application, including for example but not limited to, communication during any registration and communication on the bulletin board, message or chat area, You represent and warrant:</span></p>
<ol type="a">
<li>
<p><span style="font-size: 10pt; font-family: verdana, geneva;">You own or otherwise have all necessary rights to the content you provide and the rights to use it as provided in this Terms of Service;</span></p>
</li>
</ol><ol start="2" type="a">
<li>
<p><span style="font-size: 10pt; font-family: verdana, geneva;">all information You provide is true, accurate, current and complete, and does not violate these Terms of Service; and,</span></p>
</li>
</ol><ol start="3" type="a">
<li>
<p><span style="font-size: 10pt; font-family: verdana, geneva;">the information and Content shall not cause injury to any person or entity. Using a name other than your own legal name is prohibited (except in those specific areas of the Application that specifically ask for unique, fictitious names such as,&nbsp;<em>inter alia&nbsp;</em>certain message boards and chat rooms).</span></p>
<p><span style="font-size: 10pt; font-family: verdana, geneva;">For all such information and material, you grant us, a royalty-free, perpetual, irrevocable, non-exclusive right and license to use, copy, modify, display, archive, store, distribute, reproduce and create derivative works from such information, in any form, media, software or technology of any kind now existing or developed in the future. Without limiting the generality of the previous sentence, you authorize us to share the information across all our affiliated Applications, to include the information in a searchable format accessible by users of the Application and other affiliated Applications, and to use your name and any other information in connection with its use of the material you provide. You also grant the right to use any material, information, ideas, concepts, know-how or techniques contained in any communication you send to us for any purpose whatsoever, including but not limited to developing, manufacturing and marketing products using such information. All rights in this paragraph are granted without the need for additional compensation of any sort to you.</span></p>
</li>
</ol>