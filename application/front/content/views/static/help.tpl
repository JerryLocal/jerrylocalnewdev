<div class="listing-middbg">
	<p>
		A Bauxite Angola, S.A. (BxA) &eacute; uma empresa mista de direito angolano, criada para realizar opera&ccedil;&otilde;es de minera&ccedil;&atilde;o e investimento em diversos dom&iacute;nios, sendo, desde Setembro de 2007, titular de direitos mineiros sobre os jazigos de bauxite da Guin&eacute;-Bissau, bem como de concess&otilde;es para a prospec&ccedil;&atilde;o de bauxite em Angola e na Guin&eacute; Conakry.</p>
	<p>
		Empresa pioneira em Angola na prospec&ccedil;&atilde;o e extrac&ccedil;&atilde;o de bauxite, a BxA foi estabelecida como uma parceria constitu&iacute;da por capitais p&uacute;blicos do Governo Angolano, e do Governo Guineense bem como por privados, nacionais estrangeiros.</p>
	<img alt="" class="img-left" src="images/home-img1.gif" />
	<p>
		Com uma sede em Luanda e uma delega&ccedil;&atilde;o regional em Bissau, a BxA possui um quadro de pessoal t&eacute;cnico e administrativo constitu&iacute;do por 44 trabalhadores.</p>
	<p>
		<strong>Sede da Bauxite Angola, S.A. em Luanda.</strong></p>
	<p>
		A Bauxite Angola possui um bra&ccedil;o empresarial de direito guinense, denominado Sociedade Mineira do Bo&eacute; (SMB), atrav&eacute;s do qual levar&aacute; a cabo um conjunto de opera&ccedil;&otilde;es previstas no seu objecto social. Instala&ccedil;&otilde;es da delega&ccedil;&atilde;o da empresa em Bissau.</p>
	<img alt="" class="img-right" src="images/home-img2.gif" />
	<p>
		A Bauxite Angola possui um bra&ccedil;o empresarial de direito guinense, denominado Sociedade Mineira do Bo&eacute; (SMB), atrav&eacute;s do qual levar&aacute; a cabo um conjunto de opera&ccedil;&otilde;es previstas no seu objecto social.</p>
	<p>
		<strong>Instala&ccedil;&otilde;es da delega&ccedil;&atilde;o da empresa em Bissau.<br />
		<br />
		Test Content ..</strong></p>
</div>
