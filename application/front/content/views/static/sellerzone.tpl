<div class="header-fg">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><a href="/"> <!--- img-responsive --> <img src="<%$this->config->item('images_url')%>logo_sellerzone.png" alt="" /></a></div>
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                <div class="signup_but fr"><a href="<%$this->config->item('site_url')%>seller-zone.html"><button class="btn btn-primary" type="button">Sign up</button></a></div>
                <div class="login_but fr"><a href="<%$this->config->item('site_url')%>admin/"><button class="btn btn-primary" type="button">Login</button></a></div>
                <div class="clear">&nbsp;</div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="clear"></div> -->
<div class="slider"><img class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" src="<%$this->config->item('images_url')%>size 02.jpg" alt="" /></div>
<div class="clear white">&nbsp;</div>
<div class="sell_online">
    <div class="blocks">
        <div class="container">
            <div class="row">
                <h2 align="center"><strong>How to sell</strong></h2>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="indi_block"><img class="img-responsive" src="<%$this->config->item('images_url')%>best-seller-s.png" alt="" /></div>
                    <div class="indi_block_name" style="text-align: center;"><span style="font-size: 12pt;"><strong>Register &amp; Sell Products</strong></span></div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="indi_block_receve"><img class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" src="<%$this->config->item('images_url')%>cart-s.png" alt="" /></div>
                    <div class="indi_block_name" style="text-align: center;"><span style="font-size: 12pt;"><strong>Get Orders</strong></span></div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="indi_block_service"><img class="img-responsive" src="<%$this->config->item('images_url')%>delivery-s.png" alt="" /></div>
                    <div class="indi_block_name" style="text-align: center;"><strong><span style="font-size: 12pt;">Fulfill orders</span></strong></div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="indi_block_payment"><img class="img-responsive" src="<%$this->config->item('images_url')%>cc-s.png" alt="" /></div>
                    <div class="indi_block_name" style="text-align: center;"><strong><span style="font-size: 12pt;">Receive Payments</span></strong></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="clear">&nbsp;</div>
    <div class="blocks">
        <iframe width="420" height="315" src="https://www.youtube.com/embed/gTFPepTWHBQ" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="risk">
        <div class="container">
            <h2 align="center">Why sellers love us</h2>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="risk_img fl"><img class="img-responsive" src="<%$this->config->item('images_url')%>money-bag-48.png" alt="" /></div>
                    <div class="risk_content fl">
                        <div class="risk_heading"><span style="color: #222222; font-family: Verdana;"><strong>No Fees unless you generate revenue</strong></span></div>
                        <div class="risk_text"><span style="color: #010101; font-family: Verdana;">Our business model is simple, we make our dream come &nbsp;true only when you make first. You will not get charged unless you make your sales from our marketplace. We take only&nbsp;a small % &nbsp;of your each successful&nbsp;sale &nbsp;and we work on a revenue sharing model.</span> &nbsp;</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="risk_img fl"><img class="img-responsive" src="<%$this->config->item('images_url')%>positive-dynamic-48.png" alt="" /></div>
                    <div class="risk_content fl">
                        <div class="risk_heading"><span style="color: #010101; font-family: Verdana;"><strong>Maximize your online sales</strong></span></div>
                        <div class="risk_text"><span style="color: #010101; font-family: Verdana;">We will show your products to more than 4 million consumers across New Zealand.</span> &nbsp;</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="risk_img fl"><img class="img-responsive" src="<%$this->config->item('images_url')%>self-service-kiosk-48.png" alt="" /></div>
                    <div class="risk_content fl">
                        <div class="risk_heading"><span style="color: #010101; font-family: Verdana;"><strong>Easy to use &amp; self service</strong></span></div>
                        <div class="risk_text"><span style="color: #010101; font-family: Verdana;">We provide easy to use &amp; do-it-yourself&nbsp;sign&nbsp;up&nbsp;process. Simple products upload in no time so you can start selling and generate revenue. Best support in the industry (<span style="color: #0000ff;"><a href="mailto:seller@jerrylocal.co.nz?Subject=Hello%20again" target="_top"><span style="color: #0000ff;">support@jerrylocal.co.nz</span></a></span></span><span style="color: #010101; font-family: Verdana;">) and fastest payment for fulfilled orders.</span> &nbsp;</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="risk_img fl"><img class="img-responsive" src="<%$this->config->item('images_url')%>cart-58-48.png" alt="" /></div>
                    <div class="risk_content fl">
                        <div class="risk_heading"><span style="color: #010101; font-family: Verdana;"><strong>Fair Market Place</strong></span></div>
                        <div class="risk_text"><span style="color: #010101; font-family: Verdana;">Best seller provides the best values of service can get more orders.</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear">&nbsp;</div>
</div>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="question">
                <div class="heading">have a question?</div>
                <div class="mail">
                    <div class="foot_menu">Email: <a href="mailto:seller@jerrylocal.co.nz?Subject=Hello%20again" target="_top">support@jerrylocal.co.nz</a></div>
                </div>
            </div>
            <div class="question">
                <div class="heading">Site Map</div>
                <div class="foot_menu"><a href="/">Home</a><a href="/seller/faq.html">FAQ's</a></div>
            </div>
        </div>
    </div>
</div>