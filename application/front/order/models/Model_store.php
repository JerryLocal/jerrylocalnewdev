<?php
class Model_store extends CI_Model {

    private $primary_key;
    private $main_table;
    public $errorCode;
    public $errorMessage;

    public function __construct() {
        parent::__construct();
        $this->main_table = "mst_store_pages";        
    }
	
    function aboutStore($store_id) {        
        $this->db->select('vAboutStore');
        $this->db->from($this->main_table);
        $this->db->where('iMstStoreDetailId',$store_id);
        $about_store= $this->db->get()->result_array();
        return $about_store;
    }

    function returnPolicy($store_id) {        
        $this->db->select('vReturnPolicy');
        $this->db->from($this->main_table);
        $this->db->where('iMstStoreDetailId',$store_id);
        $returnpolicy= $this->db->get()->result_array();
        return $returnpolicy;
    }

    function toCheckReturnReqestSent($oid){
        $this->db->select('iTrnRmaId,eRquestType');
        $this->db->from('trn_rma');
        $this->db->where('iSubOrderId',$oid);
        $result = $this->db->get()->row_array();        
        if(count($result) > 0 && !empty($result)){
            return $result;
        }else{
            $result['iTrnRmaId'] = 0;
            $result['eRquestType'] = '';
            return $result;
        }
    }

    function getAllOrderStatusHistory($suboid){        
        $this->db->select("DATE_FORMAT(dDate,'%b %e,%Y') as dDate,tDetail,eOrderStatus");
        $this->db->from('trn_order_status_history');
        $this->db->where('iMstSubOrderId',$suboid);
        $this->db->order_by('iTrnOrderStatusHistoryId',DESC);
        $result = $this->db->get()->result_array();   
        $pendingflag = false;
        if(count($result) > 0 && !empty($result)){
            foreach($result as $key=>$myvalue){                
                $myvalue = $myvalue['eOrderStatus'];                
                if($pendingflag){                  
                   for ($i=0; $i < $key; $i++) {                    
                       if($result[$i]['eOrderStatus'] == 'Pending'){
                        unset($result[$i]); 
                       }
                   }
                   $pendingflag = false;
                }
                if($myvalue == 'Pending'){
                    $result[$key]['status_msg']= 'Your Order is in Pending Status';
                    $pendingflag = true;
                }
                elseif ($myvalue =='In-Process') {
                    $result[$key]['status_msg'] = 'Your Order is in process';
                }elseif ($myvalue =='Shipped') {
                    $result[$key]['status_msg'] = 'Your Order has shipped';
                }elseif ($myvalue =='Delivered') {
                   $result[$key]['status_msg'] = 'Your Order has been delivered';
                }elseif ($myvalue =='Cancel') {
                    $result[$key]['status_msg'] = 'Your Order is cancelled';
                }
            }            
            return $result;
        }
    }
}
?>