<?php

/**
 * Description of Order Controller
 * 
 * @module Order
 * 
 * @class Order.php
 * 
 * @path application\front\order\controllers\Order.php
 * 
 * @author Jaydeep Jagani
 * 
 * @date 20.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Order extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');
        $this->load->model('model_store');
    }

    /**
     * index method is used to initialize index function.
     */
    public function index() {  
    }

    /**
     * staticpage method is used to display static pages.
     */
    function TrackOrder(){
        $iAdminId = $this->session->userdata('iUserId');
        $vEmail = $this->generalfront->getColumnValue('iCustomerId',$iAdminId,'vEmail','mod_customer');
        // $vEmail = $this->session->userdata('vEmail');
        if(isset($vEmail) && !empty($vEmail)){
            $this->smarty->assign('vEmail',$vEmail);
        }
        if(isset($_POST))
        {
            $params = array();
            $params['iOrderId'] = str_replace(' ','',$_POST['orderName']);
            $params['vEmailId'] = str_replace(' ','',$_POST['orderEmail']);
            // Store Data
            $ws_name = 'track_order';
            $order_arr = $this->hbmodel->getData($ws_name, $params);
            // pr($order_arr,1);
            if($order_arr['settings']['success'] == 1){
                $order_details = $merchant_details_arr['data'];
                foreach ($order_arr['data'] as $key => $value) {
                    $ordername =$this->generalfront->getOrderNumber($value['mso_mst_sub_order_id'],'Suborder');
                    $order_arr['data'][$key]['vSubOrderName'] = $ordername[0]['vSubOrderNumberPre'].$ordername[0]['iSubOrderNumber'];    
                }
                // pr($order_arr,1);
            }else
            {
                $this->smarty->assign('orderid',$_POST['orderName']);
            }
            $this->smarty->assign('order_arr',$order_arr['data']);    
        }
    }

    function myOrder(){
        $iAdminId = $this->session->userdata('iUserId');
        $Email = $this->generalfront->getColumnValue('iCustomerId',$iAdminId,'vEmail','mod_customer','');
        $vEmail =$Email[0];
        // Added by SA on 03-02-2016 to change the order listing with buyerId instead of there emailId
        $iBuyerId =$iAdminId;        
        if(isset($vEmail)){
            $getarr = $this->input->get();
            $curr_page = $getarr['page'];
            $params = array();
            $params['page_index'] = $curr_page;
            // $params['vBuyerEmail'] = $vEmail;
            // Added by SA on 03-02-2016 to change the order listing with buyerId instead of there emailId
            $params['vBuyerEmail'] = $iBuyerId;                        
            // Order Data
            $ws_name = 'my_order';
            $myorder_arr = $this->hbmodel->getData($ws_name, $params);
            // pr($myorder_arr,1);
            foreach ($myorder_arr['data'] as $key => $value) {
                $ordername =$this->generalfront->getOrderNumber($value['mo_mst_order_id'],'Order');
                $myorder_arr['data'][$key]['vOrderName'] = $ordername[0]['vOrderNumberPre'].$ordername[0]['iOrderNumber'];
            }
            // below code for the status history - start
            foreach ($myorder_arr['data'] as $key => $value) {                
                foreach($value['get_sub_order_detail'] as $key1 => $suborder){
                    $historyData = $this->model_store->getAllOrderStatusHistory($suborder['mso_mst_sub_order_id']);
                    $myorder_arr['data'][$key]['get_sub_order_detail'][$key1]['order_history'] = $historyData;
                }
            }            
            // above code for the status history - end
            // pr($myorder_arr,1);
            $order_details = $merchant_details_arr['data'];       
            $this->smarty->assign('myorder_arr',$myorder_arr); 
        }else
        {
            $this->session->set_flashdata('failure', "Please login to see your order list");
            $ref = base64_encode($this->config->item('site_url').$_SERVER['PATH_INFO']);
               redirect($this->config->item('site_url') . 'login.html?ref='.$ref);    
        }
    }

    function OrderDetail(){
        $vEmail = $this->session->userdata('vEmail');
        $iBuyerId = $this->session->userdata('iUserId');
        if(isset($vEmail) && !empty($vEmail)){
            if(isset($_GET) && !empty($_GET)){
                $params = array();
                $params['oid'] = $_GET['oid'];
                $params['iBuyerId'] = $iBuyerId;
                // Order Data
                $ws_name = 'get_order_detail_v1';
                $order_detail_arr = $this->hbmodel->getData($ws_name, $params);
                // pr($order_detail_arr['data'],1);
                if($order_detail_arr['settings']['success'] == 1){
                    $order_detail_arr = $this->generalfront->return_cancel_order_status($order_detail_arr);
                    $pendingflag = false;
                    foreach ($order_detail_arr['data']['main_order'] as $key => $value) {
                        $ReturnOrderStatus = $this->model_store->toCheckReturnReqestSent($value['mso_mst_sub_order_id']);
                        // To get RMA Request ID with Prefix - start
                        
                        $RMA_ID_WITH_PREFIX = $this->general->get_admin_rma_number($ReturnOrderStatus['iTrnRmaId']);
                        $order_detail_arr['data']['main_order'][$key]['ReturnOrderStatus'] = $ReturnOrderStatus['iTrnRmaId'];
                        $order_detail_arr['data']['main_order'][$key]['ReturnOrderStatusPrefix'] = $RMA_ID_WITH_PREFIX;
                        $order_detail_arr['data']['main_order'][$key]['eRquestType'] = $ReturnOrderStatus['eRquestType'];

                        // To get RMA Request ID with Prefix - End

                        // pr($order_detail_arr['data']['main_order'][$key]['order_history'],1);
                        foreach($order_detail_arr['data']['main_order'][$key]['order_history'] as $mykey=>$myvalue){
                            $myvalue = $myvalue['tosh_order_status'];
                            if($pendingflag){
                               //  pr($order_detail_arr['data']['main_order'][$key]['order_history'],1);
                               // unset($order_detail_arr['data']['main_order'][$key]['order_history'][$mykey]); 
                               for ($i=0; $i < $mykey; $i++) {
                                   if($order_detail_arr['data']['main_order'][$key]['order_history'][$i]['tosh_order_status'] == 'Pending'){
                                    unset($order_detail_arr['data']['main_order'][$key]['order_history'][$i]); 
                                   }
                               }
                               $pendingflag = false;
                            }
                            if($myvalue == 'Pending'){
                                //$order_detail_arr['data']['main_order'][$key]['status_msg'] = 'Your Order is in Pending Status';
                                $order_detail_arr['data']['main_order'][$key]['order_history'][$mykey]['status_msg']= 'Your Order is in Pending Status';
                                $pendingflag = true;
                            }
                            elseif ($myvalue =='In-Process') {
                               //$order_detail_arr['data']['main_order'][$key]['status_msg'] = 'Your Order is in process';
                                $order_detail_arr['data']['main_order'][$key]['order_history'][$mykey]['status_msg'] = 'Your Order is in process';
                            }elseif ($myvalue =='Shipped') {
                               // $order_detail_arr['data']['main_order'][$key]['status_msg'] = 'Your Order has shipped ';
                                $order_detail_arr['data']['main_order'][$key]['order_history'][$mykey]['status_msg'] = 'Your Order has shipped';
                            }elseif ($myvalue =='Delivered') {
                                //$order_detail_arr['data']['main_order'][$key]['status_msg'] = 'Your Order has been delivered ';
                                $order_detail_arr['data']['main_order'][$key]['order_history'][$mykey]['status_msg'] = 'Your Order has been delivered';
                            }elseif ($myvalue =='Cancel') {
                               //$order_detail_arr['data']['main_order'][$key]['status_msg'] = 'Your Order is cancelled ';
                                $order_detail_arr['data']['main_order'][$key]['order_history'][$mykey]['status_msg'] = 'Your Order is cancelled';
                            }
                        }
                    }
                    
                    foreach ($order_detail_arr['data']['main_order'] as $key => $value) {
                        $ordername =$this->generalfront->getOrderNumber($value['mso_mst_sub_order_id'],'Suborder');
                        $order_detail_arr['data']['main_order'][$key]['vSubOrderName'] = $ordername[0]['vSubOrderNumberPre'].$ordername[0]['iSubOrderNumber'];
                    }
                    $ordername =$this->generalfront->getOrderNumber($order_detail_arr['data']['main_order'][0]['mo_mst_order_id'],'Order');
                        $order_detail_arr['data']['main_order'][0]['vOrderName'] = $ordername[0]['vOrderNumberPre'].$ordername[0]['iOrderNumber'];
                    $order_details = $merchant_details_arr['data'];
                    // pr($order_detail_arr['data'],1);
                    $this->smarty->assign('order_detail',$order_detail_arr['data']); 
                }else{
                    $this->session->set_flashdata('failure', "Invalid Data");
                    redirect($this->config->item('site_url') .'my-orders.html');
                }
            }else
            {
                $this->session->set_flashdata('failure', "Invalid Data");
                redirect($this->config->item('site_url') .'my-orders.html');
            }
        }else
        {
            $this->session->set_flashdata('failure', "Please login to see your order list");
            $ref = base64_encode($this->config->item('site_url').$_SERVER['PATH_INFO']);
               redirect($this->config->item('site_url') . 'login.html?ref='.$ref);    
        }
    }

    function ReturnOrder(){
        $params = array();
        $params['iOrderId'] = $_POST['order_id'];
        $ws_name = 'get_return_order_detail';
        $return_order_arr = $this->hbmodel->getData($ws_name, $params);
        if($return_order_arr['settings']['success'] == 1){
            // $order_details = $merchant_details_arr['data'];
            foreach ($return_order_arr['data'] as $key => $value) {
                    $ordername =$this->generalfront->getOrderNumber($value['mso_mst_sub_order_id'],'Suborder');
                    $return_order_arr['data'][$key]['vSubOrderName'] = $ordername[0]['vSubOrderNumberPre'].$ordername[0]['iSubOrderNumber'];
                }
            $this->smarty->assign('page_url',$_SERVER['HTTP_REFERER']);    
            $this->smarty->assign('order_detail',$return_order_arr['data']);
        }else
        {
            $this->session->set_flashdata('failure', "Sorry!! Your return date is expired.");
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function setReturnOrder(){
        $params = array();
        $params['iOrderId'] = $_POST['suborder'];
        $return_image = $this->uploadFile($_FILES['return_image'],$this->config->item('order_return_image_path'),'request_image','order_return_size','1');
        
        $params['vImgName'] = $return_image['name'];
        $params['tRequestDetail'] = $_POST['comment'];
        $params['ePreference'] = $_POST['preference'];
        // Order Data
        $ws_name = 'return_order';
        $return_arr = $this->hbmodel->getData($ws_name, $params);
        if($return_arr['settings']['success'] == 1){
            $this->session->set_flashdata('success', "Your Request is added Successfully");
            // redirect($this->config->item('site_url'));     
            redirect($_POST['page_url']);
        }else
        {
            $this->session->set_flashdata('success', "Error in returning your Order");
            // redirect($this->config->item('site_url').'my-orders.html');     
            redirect($_POST['page_url']);
        }
    }    

    function cancelOrder(){
        $iUserId = $this->session->userdata('iUserId');
        if(isset($iUserId) && !empty($iUserId)){
            $params =array();
            $params['oid']  =$_POST['order_id'];
            $params['tDetail']  =$_POST['cancel_detail'];
            $params['iCancelId']  =$iUserId;
            $ws_name = 'cancel_order';
            $cancel_arr = $this->hbmodel->getData($ws_name, $params);
            if($cancel_arr['settings']['success'] == '1'){
                $this->session->set_flashdata('success', "Your Order is cancelled Successfully");
                redirect($_SERVER['HTTP_REFERER']);  
            }else
            {
                $this->session->set_flashdata('failure', "Error While Cancelling your order");
                redirect($this->config->item('site_url').'my-orders.html');  
            }
        }else
        {
            $this->session->set_flashdata('failure', "Please Login to cancel Your Order");
                redirect($this->config->item('site_url').'login.html');  
        }
    }

    function Invoice(){
        $post_arr= $this->input->post();
       if(isset($post_arr['id']) && !empty($post_arr['id'])){
            $params = array();
            $params['oid'] = $post_arr['id'];
            if(isset($post_arr['section'])){
                $ws_name = 'email_invoice';
                $emailstatus= $this->hbmodel->getData($ws_name, $params);  
                echo $emailstatus['settings']['success'];exit();
            }else
            {
                $ws_name = 'product_invoice';
                $invoice_arr = $this->hbmodel->getData($ws_name, $params);

                $data = array();
                $data['mo_buyer_address1'] = $invoice_arr['data'][0]['msd_address_seller'];
                $data['mc1_country'] = $invoice_arr['data'][0]['mc_country_seller'];
                $data['ms_state'] = $invoice_arr['data'][0]['ms_state_seller'];
                $data['mc2_city'] = $invoice_arr['data'][0]['mc_city_seller'];
                $data['mo_buyer_pin_code'] = $invoice_arr['data'][0]['msd_pin_code_seller'];
                $buyer_address = $this->general->getBuyerShippingAddressExpandedView('', '', $data);
                $invoice_arr['data'][0]['msd_address_seller'] = $buyer_address;

                $data = array();
                $data['mo_buyer_area'] = $invoice_arr['data'][0]['mo_buyer_area'];
                $data['mo_buyer_address1'] = $invoice_arr['data'][0]['mo_buyer_address1'];
                $data['mc1_country'] = $invoice_arr['data'][0]['mc_country_buyer'];
                $data['ms_state'] = $invoice_arr['data'][0]['ms_state_buyer'];
                $data['mc2_city'] = $invoice_arr['data'][0]['mc1_city_buyer'];
                $data['mo_buyer_pin_code'] = $invoice_arr['data'][0]['mo_buyer_pin_code'];
                $buyer_address = $this->general->getBuyerShippingAddressExpandedView('', '', $data);
                $invoice_arr['data'][0]['mo_buyer_address1'] = $buyer_address;                 
                
                require_once APPPATH . 'third_party/dompdf/dompdf_config.inc.php';
                $html = $this->parser->parse("Invoice.tpl", $invoice_arr['data'][0], true);
                
                $dompdf = new DOMPDF();
                $dompdf->load_html($html);
                $dompdf->render();
                $file_name = time() . "_buyer_invoice.pdf";
                $pdffile = "public/upload/pdf/" . $file_name;

                $this->general->custom_file_put_contents($pdffile, $dompdf->output());

                $dir_path = $this->config->item("upload_path") . "pdf/";
                echo $pdf_url = $this->config->item("upload_url") . 'pdf/' . $file_name;
                //$this->general->force_download($file_name, $dir_path);
                exit;                
                //$this->smarty->assign('invoice_arr',$invoice_arr['data'][0]);
                // redirect($_SERVER['HTTP_REFERER']); 
            }
       }else
       {
            $this->session->set_flashdata('failure', "Please Login to print Your Invoice");
            redirect($this->config->item('site_url').'login.html'); 
       }
        
    }

    function uploadFile($Filedata,$targetFolder,$path,$filesize,$sizeindex){
        //$pathtoupload = $this->config->item('school_photos_path') . $iSchoolMasterId . '/';  
        if (!empty($Filedata)) { 
           $tempFile = $Filedata['tmp_name'];
           $targetPath = $targetFolder;
           $t_f = time() . $Filedata['name'];
           $t_f = preg_replace("/[^a-zA-Z0-9.]/", "", $t_f);
           $targetFile = rtrim($targetPath, '/') . '/' . $t_f;
           $fileParts = pathinfo($t_f);
           if (move_uploaded_file($tempFile, $targetFile)) {
               @chmod($targetFile, 0777);
               $resizeimg = '';//$this->generalfront->getGeneratedImage($path, $t_f, $storeid, $sizeindex , 'Yes', '', $filesize, '');
               $arr = array("img" => $resizeimg, "succ" => 1, "name" => $t_f, "id" => $id);
               
           } else {
               $arr = array("succ" => 0);
           }
           return $arr;
       }
    }
}
