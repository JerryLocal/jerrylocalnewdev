<%$this->js->add_js("order.js")%>
<%assign var=settings value=$myorder_arr['settings']%>
<%assign var=per_page value=$myorder_arr['settings']['per_page']%>
<%assign var=totalPage value= ceil($settings.count/$per_page)%>
<%assign var=tpages value= ceil($settings.count/$per_page)%>
<div class="main-containt">
  <div class="container">
    <div class="row-fluid">
      <%$this->general->Breadcrumb('CLEAN','My Order')%>
    </div></div>
    <div class=""> <!-- container -->
      <div class="row">
        <%include file = "left.tpl"%>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
          <div class="delivery-box">
            <h3><div class="row"><div class="col-sm-12">My Orders</div></div></h3> <!-- < %$myorder_arr|@pr%> -->
            <%if $myorder_arr['settings'].count gt 0%>
            <div class="table-box">
              <form class="form-horizontal" method="post" action='<%$this->config->item('site_url')%>order-details.html' id="order_list_form">
                <input type="hidden" id="ord_id" name="order_id" value="">
                  <table class="table table-bordered">
                    <%if $myorder_arr['settings'].count gt 0%>
                    <tr>
                      <th width="20%" scope="col" align="left">Order Number</th>
                      <th width="55%" scope="col" align="left">Products</th>
                      <th width="14%" scope="col" align="left">Amount</th>
                      <th width="11%" scope="col" align="left">Status</th>
                    </tr>
                    <%/if%>
                  </table> 

                  <%foreach $myorder_arr['data'] as $order%>
                  <div class="overflow_auto">
                  <table class="table table-bordered">
                    <!-- < %if $order['get_sub_order_detail']|@count gt 0%>
                    <tr>
                      <th scope="col" align="left">Order Number</th>
                      <th scope="col" align="left">Products</th>
                      <th scope="col" align="left">Amount</th>
                      <th scope="col" align="left">Status</th>
                    </tr>
                    < %/if%> -->
                    <%for $i=0 to $order['get_sub_order_detail']|@count-1%>
                    <%assign var=P_URL value=$this->general->CleanUrl('P',$order['get_sub_order_detail'][$i].mso_mst_products_id)%> 
                    <tr>
                      <td width="20%"><a class="order_id1" href="<%$this->config->item('site_url')%>order-details.html?oid=<%$order.mo_mst_order_id%>" id="order_id" data-id="<%$order.mo_mst_order_id%>">
                        <%($i eq '0') ? $order.vOrderName : ''%></td>
                      <td width="55%"><a href="<%$P_URL%>"><%$order['get_sub_order_detail'][$i].mso_product_name%></a></td>
                      <td width="14%" align="right"><%$this->config->item('CURRENCY_SYMBOL')%><%number_format($order['get_sub_order_detail'][$i].mso_total_cost,2)%></td>
                      <td width="11%" class="text-center">
                        <a href="#" class="<%($order['get_sub_order_detail'][$i].mso_item_status eq 'Delivered') ? 'mrgn-rgt-10' : '' %> btn-<%$order['get_sub_order_detail'][$i].mso_item_status%>" data-toggle="modal" data-target="#myModal<%$order['get_sub_order_detail'][$i].mso_mst_sub_order_id%>">
                          <%if $order['get_sub_order_detail'][$i].mso_item_status eq 'Close'%>Closed
                          <%elseif $order['get_sub_order_detail'][$i].mso_item_status eq 'Cancel'%>Cancelled
                          <%else%><%$order['get_sub_order_detail'][$i].mso_item_status%><%/if%>
                        </a>   
                        <%assign var=vSubOrderName value=$order['get_sub_order_detail'][$i].mso_sub_order_number_pre|cat:$order['get_sub_order_detail'][$i].mso_sub_order_number%>
                        <div class="modal jarry-popup fade" id="myModal<%$order['get_sub_order_detail'][$i].mso_mst_sub_order_id%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Sub Order ID: <%$vSubOrderName%><br><strong><%$order['get_sub_order_detail'][$i].mso_product_name%></strong></h4>
                              </div>
                              
                              <div class="modal-body">
                                <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                 <tr align="left">
                                  <th width="9%">Status</th>
                                  <th width="27%">Status Date</th>
                                  <th width="64%">Comments</th>
                                 </tr>
                                  <%foreach $order['get_sub_order_detail'][$i].order_history as $history_data%>
                                    <%if $history_data.eOrderStatus neq ''%>
                                    <tr>
                                      <td>
                                        <%if $history_data.eOrderStatus eq 'Close'%>
                                        Closed
                                        <%elseif $history_data.eOrderStatus eq 'Cancel'%>
                                        Cancelled
                                        <%else%>
                                        <%$history_data.eOrderStatus%>
                                        <%/if%>
                                      </td>
                                      <td><%$history_data.dDate%></td>
                                      <td align="left" style="padding-bottom:5px"><span><%$history_data.tDetail%></span>
                                        <%if $history_data.tDetail neq ''%>
                                        <br/>
                                        <%/if%>
                                        <span><i><%$history_data.status_msg%></i></span></td>
                                    </tr>
                                    <%/if%>
                                  <%/foreach%>
                                </table>
                              </div>                              
                            </div>
                          </div>
                        </div>                     
                      </td>
                    </tr>   
                    <!-- Below Code for the Popup of status history - start -->
                    <!-- <tr><td>
                    
                    </td></tr> -->
                    <!-- Above Code for the Popup of status history - End -->
                    <%/for%>
                  </table>                  
                  </div>
                  <%/foreach%>
              </form>
              <%if $myorder_arr['settings'].count gt 0%>
              <!-- <div class="pagination-box">
               <div class="col-md-7">
                 <%assign var=displayResult value=($settings.curr_page*$per_page gt $settings.count) ? $settings.count : $settings.curr_page*$per_page%>
                 <div class="row">
                   <div class="pagination-list">Items <%(($settings.curr_page-1)*$per_page)+1%> to <%$displayResult%> of <%$settings.count%> total</div>
                 </div>
               </div>
               <div class="col-md-5">
                 <div class="row">
                   <ul class="pagination">
                     <%assign var=reload value=$this->config->item('site_url')|cat:"my-orders.html?tpages="|cat:$tpages%>            
                     <%if $totalPage gt 1%>
                     <%$this->generalfront->paginate($reload,$myorder_arr['settings'].curr_page,$totalPage)%>
                     <%/if%>
                   </ul>
                 </div>
               </div>
              </div> -->
             <%/if%>
            </div>
            <%else%>
            <div class="row">
                <div class="col-sm-12">
                  <center><h1 style="border-bottom: none;"> You haven't made any orders.</h1></center>
                </div>
            </div>
            <%/if%>
         </div>
        </div>
     </div>
   </div>
 </div>
</div>
</div>
      