<%$this->js->add_js("order.js")%>
<%assign var=order_detail_arr value=$order_detail['main_order']%>
<div class="main-containt">
  <div class="container">
    <div class="row-fluid">
      <div class="breadcrumbs">
         <%$this->general->Breadcrumb('D','ORDER_HISTORY','Order Detail')%>
      </div>
    </div>
  </div>
  <div class=""> <!-- container -->
    <div class="row">
      <%include file = "left.tpl"%>
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-main">
      <div class="delivery-box"><h3><div class="row"><div class="col-sm-12">Order Details</div></div></h3>
      <div class="contact-box">
        <div class="row">
          <div class="col-md-8 overflow_auto">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <th>Name</th>
                  <td><%$order_detail_arr[0].mo_buyer_name%></td>
                  <th>Order Number</th>
                  <td><%$order_detail_arr[0].vOrderName%></td>
                </tr>
                <tr>
                  <th>Customer Email</th>
                  <td><%$order_detail_arr[0].mo_buyer_email%></td>
                  <th>Order Date</th>
                  <td><%$order_detail_arr[0].mo_date%></td>
                </tr>
                <!-- <tr>
                  <th>From Ip</th>
                  <td><%$order_detail_arr[0].mo_buyer_ip%></td>
                  <th>Order Status</th>
                  <td><%$order_detail['custom_function'].minstatus%></td>
                </tr> -->
                <tr>
                  <th>Total Paid</th>
                  <td><%$this->config->item('CURRENCY_SYMBOL')%><%number_format($order_detail_arr[0].mo_order_total,2)%> </td>
                  <!-- <th>Payment Mode</th>
                  <td>Paypal</td> -->
                  <th></th>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- <div class="col-md-4">
            <div class="panel panel-blue">
              <div class="panel-heading"><h3 class="panel-title text-center">Billing Address</h3></div>
              <div class="panel-body">
                <strong><%$order_detail_arr[0].mo_buyer_name%></strong><br>
                   <%if $order_detail_arr[0].mo_buyer_address1 neq ''%>
                   <%$order_detail_arr[0].mo_buyer_address1%> ,
                   <%/if%>
                   <%if $order_detail_arr[0].mo_buyer_area neq ''%>
                   <%$order_detail_arr[0].mo_buyer_area%> 
                   <%/if%>
                   <br>
                 <%if $order_detail_arr[0].mc1_city neq ''%>
                   <%$order_detail_arr[0].mc1_city%>, 
                 <%/if%>
                 <%if $order_detail_arr[0].ms1_state neq ''%>
                   <%$order_detail_arr[0].ms1_state%>, 
                 <%/if%> 
                 <%if $order_detail_arr[0].ms1_state neq ''%>
                   <%$order_detail_arr[0].ms1_state%>, 
                 <%/if%>
                 <%if $order_detail_arr[0].mo_buyer_pin_code neq ''%>
                   <%$order_detail_arr[0].mo_buyer_pin_code%>, 
                 <%/if%>  
                 <%$order_detail_arr[0].mc_country%>
                 <%if $order_detail_arr[0].mo_buyer_phone neq ''%>
                   <br>T: <%$order_detail_arr[0].mo_buyer_phone%> 
                 <%/if%>

              </div>
            </div>
          </div> -->
          <div class="col-md-4">
            <div class="panel panel-blue">
              <div class="panel-heading shipping_addr"><h3 class="panel-title text-center">Shipping Address</h3></div>
              <div class="panel-body">
               <strong><%$order_detail_arr[0].mo_buyer_name%></strong><br>
                  <%if $order_detail_arr[0].mo_buyer_address1 neq ''%>
                  <%$order_detail_arr[0].mo_buyer_address1%> ,
                  <%/if%>
                  <%if $order_detail_arr[0].mo_buyer_area neq ''%>
                  <%$order_detail_arr[0].mo_buyer_area%> 
                  <%/if%>
                  <br>
                <%if $order_detail_arr[0].mo_buyer_city_id neq ''%>                  
                  <%$this->generalfront->getcolumnValue('iCityId',$order_detail_arr[0].mo_buyer_city_id,'vCity','mod_city')%>, 
                <%/if%>                
                <%if $order_detail_arr[0].mo_buyer_state_id neq ''%>
                  <%$this->generalfront->getcolumnValue('iStateId',$order_detail_arr[0].mo_buyer_state_id,'vState','mod_state')%>, 
                <%/if%>
                <%if $order_detail_arr[0].mo_buyer_pin_code neq ''%>
                  <%$order_detail_arr[0].mo_buyer_pin_code%> 
                <%/if%> 
                <br>
                <%if $order_detail_arr[0].mo_buyer_country_id neq ''%>
                   <%$this->generalfront->getcolumnValue('iCountryId',$order_detail_arr[0].mo_buyer_country_id,'vCountry','mod_country')%>.
                <%/if%>
                <%if $order_detail_arr[0].mo_buyer_phone neq ''%>
                  <br>Phone: <%$order_detail_arr[0].mo_buyer_phone%> 
                <%/if%>
              </div>
            </div>
          </div>
        </div>
        <form class="form-horizontal" method="post" target="_blank" action='<%$this->config->item('site_url')%>review.html' id="review_list_form">
          <input type="hidden" name="id" id="pId" value="" />
        </form>
        <form class="form-horizontal" method="post" action='<%$this->config->item('site_url')%>track-orders.html' id="trackorder_list_form">
          <input type="hidden" name="orderName" id="oId" value="" />
        </form>
        <!-- < %for $i=0 to $order_detail_arr|@count%> -->
        <!-- < %assign var=store_id value=$order_detail_arr[$i].mso_mst_store_detail_id %> -->
        <%foreach $order_detail_arr as $order%>
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-my-default">
              <div class="panel-heading">
                <div class="row order-detail-box-h">
                  <div class="col-sm-4">
                    <!-- strong-txt --><!--<p><strong class="">Sold By : </strong><a href="<%$this->general->CleanUrl('v', $order.mso_mst_store_detail_id)%>" target="_blank"><%$order.msd_store_name%></a> </p>-->
                    <!-- strong-txt --><p><strong class="">Sold By : </strong><a href="<%$this->config->item('site_url')%>store/<%$order.msd_store_name%>.html" target="_blank"><%$order.msd_store_name%></a> </p>
                    <!-- strong-txt --><p><strong class="">Sub Order No : </strong><%$order.vSubOrderName%> </p>
                  </div>
                  <div class="col-sm-3 text-left-m-mb">
                    <%if $order.ReturnOrderStatus gt 0%>
                    <strong>Return Id : </strong> <%$order.ReturnOrderStatusPrefix%><br>
                    <strong>RMA Status :</strong> <%$order.eRquestType%><br>
                    <%/if%>
                    <%if $order.mso_tracking_number neq ''%>
                    <strong>Tracking Code :</strong> 
                    <%$order.mso_tracking_number%>
                    <%/if%>
                  </div>
                  <div class="col-sm-5 text-right">
                    <form class="form-horizontal" method="post" action='<%$this->config->item('site_url')%>review.html' id="return_order_form" role="form">
                      <input type="hidden" name="order_id" class="order_id" value="">
                    </form>
                    <div>
                    <%if $order.return_ord eq '1'%>   
                      <%if $order.ReturnOrderStatus eq 0%>                                       
                      <a class="return_comman_btn btn btn-default" data-id="<%$order.mso_mst_sub_order_id%>" data-rel="<%$this->config->item('site_url')%>return-order.html">Return Order</a>&nbsp;
                      <%/if%>
                    <%/if%>
                    <!-- < %if $order.cancel_ord eq '1'%>
                    <a class="btn btn-default"  data-id="<%$order.mso_mst_sub_order_id%>" data-toggle="modal" data-target='#my_cncl_model'>Cancel Order</a>&nbsp;
                    <div class="modal jarry-popup fade" id="my_cncl_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header text-left">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Sub Order ID: <%$order.vSubOrderName%> <br><strong><%$order.mso_product_name%></strong></h4>
                                </div>
                                <form class="form-horizontal" method="post" action='<%$this->config->item('site_url')%>order-cancel.html' name="cancel_order_form" id="cancel_order_form">
                                <div class="modal-body" style="padding-bottom:10px;">
                                  
                                      <input type="hidden" name="order_id" class="order_id" value="<%$order.mso_mst_sub_order_id%>">
                                       <label for="inputComment" class="col-sm-3 control-label"> Why You want to cancel this product ?</label>
                                       <div class="col-sm-9">
                                        <textarea class="form-control" id="inputComment" rows="3" name="cancel_detail" style="max-width:350px;"></textarea>
                                      </div>
                              <div class="clear"></div>
                              </div>
                              <div class="clear"></div>
                              <div class="modal-footer">
                                <input type="button" id="cancel_btn" class="btn btn-default" name="cancel_btn" value="Cancel Order">
                              </div>
                               </form>
                            </div>
                          </div>
                        </div>
                     < %/if%> -->

                    <%if $order.invoice_ord eq '1'%>
                      <a data-rel="<%$this->config->item('site_url')%>invoice.html" class="btn btn-default btn_invoice" data-id="<%$order.mso_mst_sub_order_id%>">Print Invoice</a>&nbsp;
                    <%/if%>
                    <%if $order.invoice_ord eq '1'%>
                      <a data-rel="<%$this->config->item('site_url')%>invoice.html" class="btn btn-default btn_email" data-id="<%$order.mso_mst_sub_order_id%>">Email</a>
                    <%/if%>
                    <%if $order.mso_item_status eq 'Shipped' || $order.invoice_ord eq '1'%>
                      <div style="margin-top:10px;"><a href="<%$order.ms1_url%>" target="_blank" class="btn btn-Pending-SA" style="padding:6px 12px!important;">Track Order</a></div>
                    <%/if%>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-mrgn-btm">
                    <thead>
                      <tr>
                        <th width="8%" style="text-align: center;">#</th>
                        <th width="42%">Product Name</th>
                        <th width="5%" class="text-center">Unit</th>
                        <th width="12%" class="text-right">Unit Price</th>
                        <th width="10%" class="text-right">Sub Total</th>                        
                        <th width="2%" class="text-right">Shipping</th>                        
                        <th width="9%" style="text-align: center;">Status</th>
                        <!-- <th width="15%">Order Details Date</th> -->
                        <%if $order.mso_item_status eq 'Delivered'%>
                        <th width="11%">Action</th>
                        <%/if%>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center"><a href="<%$this->general->cleanUrl('p',$order['mso_mst_products_id'])%>"><img src="<%$order.mp_default_img%>" class="img-responsive" alt="<%$order.mso_product_name%>"></a></td>
                        <td><a href="<%$this->general->cleanUrl('p',$order['mso_mst_products_id'])%>"> <%$order.mso_product_name%></a> <br/>
                          <%if $order.mso_product_option neq ''%>Size : <%$order.mso_product_option%><%/if%>
                        </td>
                        <td class="text-center"><%$order.mso_product_qty%></td>
                        <td class="text-right"><%$this->config->item('CURRENCY_SYMBOL')%><%number_format($order.mso_product_sale_price,2)%></td>
                        
                        <%assign var=total_price  value=$total_price + ($order.mso_product_qty * $order.mso_product_sale_price)%>
                        <td class="text-right"><%$this->config->item('CURRENCY_SYMBOL')%><%number_format($order.mso_product_qty * $order.mso_product_sale_price,2)%></td>    
                        <td class="text-right">
                          <%if $order.mso_shipping_cost gt 0%>
                            <%$this->config->item('CURRENCY_SYMBOL')%><%number_format($order.mso_shipping_cost,2)%>
                          <%else%>
                          Free
                          <%/if%>
                        </td>                        
                        <td align="center"><a href="#" class="<%($order.mso_item_status eq 'Delivered') ? 'mrgn-rgt-10' : '' %> btn-<%$order.mso_item_status%>" data-toggle="modal" data-target="#myModal<%$order.mso_mst_sub_order_id%>">
                          <%if $order.mso_item_status eq 'Close'%>Closed<%elseif $order.mso_item_status eq 'Cancel'%>Cancelled<%else%><%$order.mso_item_status%><%/if%></a></td>
                        <!-- <td><%$order.tosh_date%></td> -->
                        <%if $order.mso_item_status eq 'Delivered'%>
                        <td>
                          <a class="product_id review_order_detail btn btn-default" data-id="<%$order.mso_mst_products_id%>">Review </a></td>
                        <%/if%>
                        </tr>
                      </tbody>
                    </table>
                    <div class="modal jarry-popup fade" id="myModal<%$order.mso_mst_sub_order_id%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Sub Order ID: <%$order.vSubOrderName%><br><strong><%$order.mso_product_name%></strong></h4>
                                </div>
                                <div class="modal-body">
                                  <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                   <tr align="left">
                                    <th width="9%">Status</th>
                                    <th width="27%">Status Date</th>
                                    <th width="64%">Comments</th>
                                  </tr>
                                  <%foreach $order.order_history as $history_data%>
                                  <%if $history_data.tosh_order_status neq ''%>
                                  <tr>
                                    <td>
                                      <%if $history_data.tosh_order_status eq 'Close'%>
                                      Closed
                                      <%elseif $history_data.tosh_order_status eq 'Cancel'%>
                                      Cancelled
                                      <%else%>
                                      <%$history_data.tosh_order_status%>
                                      <%/if%>
                                    </td>
                                    <td><%$history_data.tosh_date%></td>
                                    <td align="left" style="padding-bottom:5px"><span><%$history_data.tosh_detail%></span>
                                      <%if $history_data.tosh_detail neq ''%>
                                      <br/>
                                      <%/if%>
                                      <span><i><%$history_data.status_msg%></i></span></td>
                                  </tr>
                                  <%/if%>
                                  <%/foreach%>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                  </div>
                </div>
                <!-- <div class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4"><strong>Courier Name :</strong>
                      <%if $order.mso_shipper_name neq ''%>
                        <%$order.mso_shipper_name%>
                     <%else%>
                      #
                      <%/if%>
                    </div>
                    <div class="col-sm-4 text-center text-left-m-mb"><strong>Tracking Code :</strong> 
                      <%if $order.mso_tracking_number neq ''%>
                      <%$order.mso_tracking_number%>
                      <%else%>
                      #
                      <%/if%>
                    </div>
                    <div class="col-sm-4 text-right text-left-m-mb"><strong>Courier Website : </strong>
                      <%if $order.ms1_url neq ''%>
                      <a href="<%$order.ms1_url%>" target="_blank"><%$order.ms1_url%></a>
                      <%else%>
                      #
                      <%/if%>

                    </div>
                  </div>
                </div> -->
              </div>
            </div>
          </div>
          <%/foreach%>
          
          <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6">
              <div class="well order-total">
                <div class="row static-info align-reverse">
                  <div class="col-md-7 name">
                   <!-- Original -->Sub Total
                 </div>
                 <div class="col-md-5 value">
                  <!-- < %$this->config->item('CURRENCY_SYMBOL')%> <%$order_detail_arr[0].mso_product_regular_price%>  -->
                  <%$this->config->item('CURRENCY_SYMBOL')%><%number_format($total_price,2)%>
                 </div>
               </div>
               <!-- <div class="row static-info align-reverse">
                <div class="col-md-7 name">
                 Discount
               </div>
               <div class="col-md-5 value">
                 - <%$this->config->item('CURRENCY_SYMBOL')%><%number_format($total_price - $order_detail_arr[0].mo_sub_total,2)%>
               </div>
             </div> -->
             <%if $order_detail_arr[0].mo_shipping_cost gt 0%>
             <div class="row static-info align-reverse">
              <div class="col-md-7 name">
               Shipping
             </div>
             <div class="col-md-5 value">
               + <%$this->config->item('CURRENCY_SYMBOL')%><%number_format($order_detail_arr[0].mo_shipping_cost,2)%>
             </div>
           </div>
           <%/if%>
           <div class="row static-info align-reverse">
            <div class="col-md-7 name-total">
             Grand Total<br><small>(Include GST)</small>
           </div>
           <div class="col-md-5 value-total">
             <%$this->config->item('CURRENCY_SYMBOL')%><%number_format($order_detail_arr[0].mo_sub_total+$order_detail_arr[0].mo_shipping_cost,2)%>
           </div>
         </div>
       </div>
     </div>
   </div>



 </div></div>
</div></div></div>
</div>