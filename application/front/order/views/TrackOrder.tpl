<div class="main-containt">
  <div class="container">
    <div class="row-fluid">
      <div class="breadcrumbs">
        <ul>
          <li><a href="#">Home</a><span>|</span></li>
          <li><strong>Track Orders</strong></li>
        </ul>
      </div>
    </div></div>
    <div class="container">
      <div class="delivery-box">
        <h3>Know your order status</h3>
        <form class="form-horizontal" method="post" action='<%$this->config->item('site_url')%>order-details.html' id="order_list_form">
              <input type="hidden" id="ord_id" name="order_id" value="">
        </form>
        <div class="contact-box">
          <%if $orderid neq ''%>
          <form class="form-horizontal" method="post" action='<%$this->config->item('site_url')%>/track-orders.html' id="track-order-form-dr">

              <div class="form-group">
                <label for="inputName" class="col-sm-4 control-label">Order ID</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" name="orderName" id="inputname" placeholder="Order ID">
                  <div class="err orderid" style="display:none;">Please Enter Order ID</div>
                </div>
              </div>
              <%if $vEmail neq ''%>
              <div class="form-group" style="display:none">
                <label for="inputEmail" class="col-sm-4 control-label">Email ID</label>
                <div class="col-sm-5">
                  <input type="email" class="form-control" name="orderEmail" id="inputEmail" placeholder="Email Address" value="<%$vEmail%>">
                </div>
              </div>
              <%else%>
                <div class="form-group">
                <label for="inputEmail" class="col-sm-4 control-label">Email ID</label>
                <div class="col-sm-5">
                  <input type="email" class="form-control" name="orderEmail" id="inputEmail" placeholder="Email Address">
                  <div class="err emailid" style="display:none;">Please Enter Email ID</div>
                </div>
              </div>
              <%/if%>
              <div class="form-group"> <!-- align="center" -->
                <div class="col-sm-4"></div>
                <div class="col-sm-5">
                    <input type="submit" class="btn btn-primary btn-primary-j" value="Submit" />
                    <button type="button" class="btn btn-default btn-default-j" data-dismiss="modal" onclick="return cancleTrackOrder();">Cancel</button>
                </div>
                </div>
            </form>
            <script type="text/javascript">
              function cancleTrackOrder() {
                document.location.href = site_url+"my-orders.html";
              }
            </script>
            <div class="table-box">
             <table class="data-table new">
               <tbody>
                 <tr>
                  <td width="100%" ><center>System could not find your Order Number: <%$orderid%></center></td>
                 </tr>
               </tbody>
             </table>
           </div>
            <%else%>
         <div class="table-box"> <!-- < %$order_arr|@pr%> -->
          <table class="data-table new">
            <tbody>
              <tr>
                <th width="15%" align="left">Sub Order Id</th>
                <th width="56%" align="left">Product Name</th>
                <th width="12%" style="text-align:center">Status</th>
                 
                <th width="17%" id="status_th" style="text-align:center;display:none">Track Order</th>
                
              </tr>
              <%foreach $order_arr as $order%>
              <tr>
                <td><%$order.vSubOrderName%></td>
                <td><%$order.mso_product_name%></td>
                <td align="center"><span class="btn-<%$order.mso_item_status%>">
                  <%if $order.mso_item_status eq 'Close'%>Closed<%elseif $order.mso_item_status eq 'Cancel'%>Cancelled<%else%><%$order.mso_item_status%><%/if%>
                  </span></td>
                <%if $order.mso_item_status eq 'Shipped' || $order.mso_item_status eq 'Delivered' || $order.mso_item_status eq 'Close' %>
                <script type="text/javascript">
                    document.getElementById('status_th').style.display = 'table-cell';
                </script>
                <td align="center"><a href="#" data-toggle="modal" data-target="#myModal-<%$order.mso_mst_sub_order_id%>" class="btn btn-track-order" id="track_order" >Track Order</a>
                  <div class="modal jarry-popup fade" id="myModal-<%$order.mso_mst_sub_order_id%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Order Tracking Detail</h4>
                    </div>
                    <div class="modal-body">
                       <p>Shipped by <b><%$order.mso_shipper_name%> </b> on <%$order.mso_shipping_date%></p>
                       <p>here is your tracking number <%$order.mso_tracking_number%> <br/>you can track your order
from this link <a href="<%$order.ms_url%>" target ="_blank">TrackNow</a></p> 
                    </div>
                  </div>
                </div>
              </div>
                </td>
                <%/if%>
              </tr>
              <%/foreach%>
            </tbody></table>
          </div>
          <%/if%>
        </div>
      </div>
    </div>
  </div>