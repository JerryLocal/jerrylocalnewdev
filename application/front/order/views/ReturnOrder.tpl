<%$this->js->add_js("return_order.js")%>
<div class="returnOrder"></div>
<div class="main-containt">
	<div class="container">
		<div class="row-fluid">
      		<%$this->general->Breadcrumb('CLEAN','Return Request')%>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-main">
				<div class="common-back">
					<h3>Return Request</h3>
					<div class="contact-box">
						<div class="return-t-order-main">
							<div class="col-md-4 return-t-order"><strong>Order ID</strong> <%$order_detail[0].vSubOrderName%></div>
							<div class="col-md-4 return-t-order"><strong>Amount Paid</strong><%$this->config->item('CURRENCY_SYMBOL')%> <%number_format($order_detail[0].mso_total_cost, 2)%></div>
							<div class="col-md-4 return-t-order"><strong>Order Status</strong> <span class="btn-Delivered"><%$order_detail[0].mso_item_status%></span></div>
							<div class="col-md-4 return-t-order"><strong>Order Date</strong><%$order_detail[0].mso_delivered_date%></div>
						</div>
						<form class="form-horizontal return-order-form" enctype="multipart/form-data" method="post" action='<%$this->config->item('site_url')%>order/order/setReturnOrder' id="return_order_form"> 
							<input type="hidden" name="page_url" value="<%$page_url%>">
							<div class="form-group">
								<label for="inputqty1" class="col-xs-12 col-sm-3 control-label">Product name </label>
								<div class="col-xs-6 col-sm-9">
									<input type="hidden" class="order_id" name="suborder" value="<%$order_detail[0].mso_mst_sub_order_id%>">
									<input type="hidden" class="form-control" name="productname_<%$order_detail[0].mso_mst_sub_order_id%>" value="<%$order_detail[0].mp_title%>" readonly>
									<label class="control-label max350"><%$order_detail[0].mp_title%></label>
								</div>
							</div>

							<div class="form-group">
								<label for="inputEmail" class="col-sm-3 control-label">Reason for Replace or Refund <span class="required">*</span></label>
								<div class="col-sm-9">
									<select class="form-control max350" name="reason">
										<option value="" disabled selected>Select Reason</option>
										<option>I have not recevied my item</option>
										<option>Issues with seller</option>
										<option>My refund status</option>
										<option>Payment issue</option>
										<option>Services issues</option>
										<option>Feed back</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-sm-3 control-label">Preference <span class="required">*</span></label>
								<div class="col-sm-9">
									<select class="form-control max350" name="preference">
										<option value="" disabled selected>Select Preference</option>
										<option>Replace</option>
										<option>Return</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="inputComment" class="col-sm-3 control-label">Comment <span class="required">*</span></label>
								<div class="col-sm-9">
									<textarea class="form-control max350" id="inputComment" rows="3" name="comment"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="inputCaptcha" class="col-sm-3 control-label">Attachment</label>
								<div class="col-sm-9">
									<input type="file" class="file-upload-btn" name="return_image" id="return_image" onchange="return previewFile();">
									
									<span class="file-upload-img" style="display:none;">
										<img src="" id="preview-img" />
										<!-- <img src="<%$this->config->item('images_url')%>noimage_1.gif" height="35" id="preview-img-noimage" /> -->
										<!-- <img src="<%$order_detail.mp_default_img%>"> -->
										<!-- style="display:none;" -->
										<a href="javascript:;" onclick="return removeSelectedImage();" class="file-upload-img-close" id="close-btn"><i class="fa fa-times-circle"></i></a>
									</span>
									<a title="you can upload return product image with size of 2 MB and in format of JPG/PNG" class="file-upload-help"><i class="fa fa-question-circle fa-lg"></i></a>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-default-site" id="return_btn">Confirm Return</button>
									<button type="button" class="btn btn-default-cancel" onClick="history.go(-1)">Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
