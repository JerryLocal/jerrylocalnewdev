<?php

/**
 * Description of Content Controller
 * 
 * @module Content
 * 
 * @class content.php
 * 
 * @path application\front\content\controllers\content.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Newslatter extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();        
        $this->load->model('hbmodel');       
    }

    
    function subscribeNewsLatter(){
        $postarr = $this->input->post(); 
        $this->load->library('fancybox');
        $vEmail = trim($postarr['vEmail']);
        // $emailexist = $this->generalfront->checkemailExist('vEmail',$vEmail,'mst_newsletter_subscription');
        $emailexist = $this->generalfront->checkSubcriber($vEmail);
        
        if(!$emailexist){
            $ret = array('success'=>0,'msg'=>'You are already Subscribed.');
            echo json_encode($ret);
            exit();
        }else{
            $params = array();
            $ws_name = 'set_news_letter';
            $iUserID = $this->session->userdata('iUserID');
            if($iUserID != '' && $iUserID > 0){
                $params['vEmail'] = $vEmail;
                $params['iUserID'] = $iUserID;
                $params['vName'] = 'User';
                $params['iNewsletterGroupId'] = '2';
                $params['eStatus'] = 'Subscribe';

            }else{
                $params['vEmail'] = $vEmail;
                $params['iUserID'] = 0;
                $params['vName'] = 'Guest User'; 
                $params['eStatus'] = 'Subscribe';                   
            }
            $subscriber_arr = $this->hbmodel->getData($ws_name, $params);     
            
            if(count($subscriber_arr['data']) > 0){
                $ret = array('success'=>1,'msg'=>'You are now Subscribed to our newslatter.');
            }else{
                $ret = array('success'=>0,'msg'=>$subscriber_arr['message']);
            }
            echo json_encode($ret);
            exit();            
        }
    }
}
