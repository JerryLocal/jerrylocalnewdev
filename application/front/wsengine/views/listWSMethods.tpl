<%$this->css->add_css("ws_validate.css")%>
<%$this->css->css_src()%>
<%$this->js->add_js("jquery.validate.min.js","crypto-md5.js","application_ws.js")%>

<script type="text/javascript">
    var api_key = '<%$this->config->item("API_KEY")%>';
    var api_secret = '<%$this->config->item("API_SECRET")%>';
    var ws_base = "";
</script>

<h1>All Available APIs</h1>
<%foreach name="j" from=$all_methods key=key item=item%>    
    <div class="method-name">
        <%$smarty.foreach.j.iteration%>. <%$item.title%>
    </div>
    <div class="inputparams">
        <form action="<%$this->config->item('site_url')%>WS/<%$key%>?ws_debug=1" method="post" class="ws">
            <%assign var="ws_params" value=$item.params%>
            <%section name="i" loop=$ws_params%>
                <%if $ws_params[i] neq ''%>
                    <p>
                        <label for="<%$ws_params[i]%>"><%$ws_params[i]%></label>
                        <input id="<%$ws_params[i]%>" name="<%$ws_params[i]%>" value="">          
                    </p>
                <%/if%>
            <%/section%>  
            <p>
                <button>Submit</button>
            </p>
        </form>
        <pre class="code"></pre>
    </div>
<%/foreach%>