<?php

/**
 * Description of WS Controller
 * 
 * @module WS Controller
 * 
 * @class Wscontroller.php
 * 
 * @path application\front\wsengine\controllers\Wscontroller.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Wscontroller extends HB_Controller {

    protected $_debug_log = FALSE;
    protected $_debug_called = FALSE;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('wschecker');
        $this->load->library('wsresponse');
    }

    /**
     * listWSMethods method is used to get all webservices list.
     */
    public function listWSMethods() {
        $redirect_url = '';
        if ($this->session->userdata('iAdminId') > 0) {
            $redirect_url = $this->config->item('site_url');
        } else {
            $redirect_url = $this->config->item('admin_url');
        }
        redirect($redirect_url);
        /* $this->set_template('ws_template.tpl');
          if ($_ENV['debug_action']) {
          $this->config->load('hb_webservices', TRUE);
          $all_methods = $this->config->item('hb_webservices');
          }
          $all_methods = empty($all_methods) ? array() : $all_methods;
          $render_arr = array(
          'all_methods' => $all_methods
          );
          $this->smarty->assign($render_arr); */
    }

    /**
     * WSExecuter method is used to get individual webservice list.
     * @param string $func_name func_name is the webservice name.
     */
    public function WSExecuter($func_name = '') {
        $this->config->load('hb_webservices', TRUE);
        $all_methods = $this->config->item('hb_webservices');
        if (empty($all_methods[$func_name])) {
            show_error('API code not found. Please save settings or update code.', 400);
        }
        $this->load->module("webservice/" . $func_name);
        //request params
        $get_arr = is_array($this->input->get(NULL, TRUE)) ? $this->input->get(NULL, TRUE) : array();
        $post_arr = is_array($this->input->post(NULL, TRUE)) ? $this->input->post(NULL, TRUE) : array();
        $request_arr = array_merge($get_arr, $post_arr);
        //data encryption process
        if ($this->config->item('WS_RESPONSE_ENCRYPTION') == "Y") {
            $request_arr = $this->wschecker->decrypt_params($request_arr);
        }
        //token and checksum validation
        $verify_res = $this->wschecker->verify_webservice($request_arr);
        if ($verify_res['success'] != "1") {
            $this->wschecker->show_error_code($verify_res);
        }
        //checking for webservice controller
        if (!is_object($this->$func_name)) {
            show_error('API code not found. Please save settings or update code.', 400);
        }
        //setup for debugger
        if (!is_null($this->input->get_post("ws_debug")) || !is_null($this->input->get_post("ws_ctrls"))) {
            $this->wsresponse->ws_log_file = $this->input->get_post("ws_log");
            $debug_cache_dir = $this->config->item('api_debugger_path');
            if (!is_dir($debug_cache_dir)) {
                $this->general->createFolder($debug_cache_dir);
            }
            if (!empty($this->wsresponse->ws_log_file) && is_file($debug_cache_dir . $this->wsresponse->ws_log_file)) {
                $_log_params = @file_get_contents($debug_cache_dir . $this->wsresponse->ws_log_file);
                $_log_params = unserialize($_log_params);
                if (is_array($_log_params) && count($_log_params) > 0) {
                    $this->wsresponse->ws_debug_params = $_log_params['debug'];
                    $next_flow = $_log_params['next_flow'];
                    if (method_exists($this->$func_name, $next_flow)) {
                        if (!empty($_log_params['end_loop'])) {
                            $this->wsresponse->pushDebugParams($_log_params['end_loop'], array(), $_log_params['params'], $next_flow, $_log_params['start_loop'], $_log_params['end_loop']);
                        }
                        $responce_arr = $this->$func_name->$next_flow($_log_params['params']);
                        $this->wsresponse->sendWSResponse($responce_arr, $this->wsresponse->ws_debug_params);
                    } else {
                        show_error('API debugger having some problem to detect next flow. Please try again.', 400);
                    }
                }
            }
            if (!$this->_debug_log) {
                $this->wsresponse->ws_log_file = md5("debug_" . date("YmdHis") . "_" . rand(1000, 9999));
            }
            $this->_debug_called = TRUE;
        }
        //initiate webservice
        $output_arr = $this->$func_name->handler($request_arr);
        //print output response
        if ($this->_debug_called == TRUE) {
            $this->wsresponse->sendWSResponse($output_arr, $this->wsresponse->ws_debug_params);
        } else {
            $this->wsresponse->sendWSResponse($output_arr);
        }
    }

}
