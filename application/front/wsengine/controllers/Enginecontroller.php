<?php
/**
 * Description of WS Engine Controller
 * 
 * @module WS Engine
 * 
 * @class enginecontroller.php
 * 
 * @path application\front\wsengine\controllers\enginecontroller.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class EngineController extends HB_Controller
{

    public $ws_details;
    public $debug_params;
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $inner_loop;
    public $expression_eval;
    public $break_continue;
    protected $_logfile;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->ws_details = array();
        $this->debug_params = array();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->inner_loop = array();
        $this->expression_eval = null;
        $this->break_continue = null;

        $this->load->model('engine');
        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
    }

    /**
     * listWSMethods method is used to get all webservices list.
     */
    function listWSMethods()
    {
        $this->set_template('ws_template.tpl');
        if ($_ENV['debug_action']) {
            $all_methods = $this->engine->getAllWSMethods();
        } else {
            $all_methods = array();
        }
        $render_arr = array(
            'all_methods' => $all_methods
        );
        $this->smarty->assign($render_arr);
    }

    /**
     * engineHandler method is used to get individual webservice list.
     * @param string $func_name func_name is the webservice name.
     * @param array $params_fwd params_fwd is the array of params to process.
     * @param boolean $return_op return_op is the boolean flag for returning data array.
     * @return array $responce_arr returns webservice response. (either json or array)
     */
    function engineHandler($func_name = '', $params_fwd = array(), $return_op = false)
    {
        $ws_details = $this->engine->loadWSDetails($func_name);
        if (!is_array($ws_details) || !is_array($ws_details['ws_master']) || count($ws_details['ws_master']) == 0) {
            show_error('Oh god you should not try to check the site!', 404);
        }
        $ws_details['ws_flow'] = $this->jsonDecodeWSSettings($ws_details['ws_flow']);

        $params_arr = $this->unSerializeWSParams($ws_details['ws_params']);

        $ws_details['ws_params'] = $params_arr['ws_params'];
        $this->ws_details[$func_name] = $ws_details;

        if ($return_op !== true && !is_null($this->input->get_post("ws_debug")) && !is_null($this->input->get_post("ws_ctrls"))) {
            $this->_logfile = $this->input->get_post("ws_log");
            $debug_cache_dir = $this->config->item('api_debugger_path');
            if (!is_dir($debug_cache_dir)) {
                $this->general->createFolder($debug_cache_dir);
            }
            $_log_exists = false;
            if (!empty($this->_logfile) && file_exists($debug_cache_dir . $this->_logfile)) {
                $_log_params = @file_get_contents($debug_cache_dir . $this->_logfile);
                $_log_params = unserialize($_log_params);
                if (is_array($_log_params) && count($_log_params) > 0) {
                    $this->debug_params = $_log_params['debug'];
                    $responce_arr = $this->handleFlowRecursively($func_name, $_log_params['flow_id'], $_log_params['params'], $_log_params['loop_name'], $_log_params['loop_keys']);
                    $this->wsresponse->sendWSResponse($responce_arr, $this->debug_params);
                }
            }
            if (!$_log_exists) {
                $this->_logfile = md5("debug_" . date("YmdHis") . "_" . rand(1000, 9999));
            }
        }

        //validate all the input params
        $get_arr = is_array($this->input->get(null)) ? $this->input->get(null) : array();
        $post_arr = is_array($this->input->post(null)) ? $this->input->post(null) : array();
        $post_params = array_merge($get_arr, $post_arr);
        $post_params = is_array($params_fwd) ? array_merge($post_params, $params_fwd) : array();

        if ($return_op !== true) {
            if ($this->config->item('WS_RESPONSE_ENCRYPTION') == "Y") {
                $post_params = $this->wschecker->decrypt_params($post_params);
            }
        }

        $validation_res = $this->wsresponse->validateInputParams($params_arr['valid_arr'], $post_params, $func_name, $this->ws_details[$func_name]['ws_messages']);
        if ($validation_res['success'] == "-5") {
            if ($return_op === true) {
                return $validation_res;
            } else {
                $validation_final_res = $this->wsresponse->makeValidationResponse($validation_res);
                $this->wsresponse->sendWSResponse($validation_final_res);
            }
        }
        if ($return_op !== true) {
            $verify_res = $this->wschecker->verify_webservice($post_params);
            if ($verify_res['success'] != "1") {
                $this->wschecker->show_error_code($verify_res);
            }
        }

        $input_params = $validation_res['input_params'];

        if (is_array($params_arr['comma_arr']) && count($params_arr['comma_arr']) > 0) {
            foreach ($params_arr['comma_arr'] as $key => $val) {
                if (!is_array($input_params[$val['value']])) {
                    $split = (trim($val['split']) != '') ? trim($val['split']) : ",";
                    $input_params[$val['value']] = (trim($input_params[$val['value']]) != '') ? explode($split, trim($input_params[$val['value']])) : array();
                }
            }
        }
        if (is_array($params_arr['json_arr']) && count($params_arr['json_arr']) > 0) {
            foreach ($params_arr['json_arr'] as $key => $val) {
                if (!is_array($input_params[$val['value']])) {
                    $input_params[$val['value']] = (trim($input_params[$val['value']]) != '') ? json_decode(trim($input_params[$val['value']]), true) : array();
                }
                if ($val['type'] == "JSONDictionary" && is_array($input_params[$val['value']])) {
                    $input_params = array_merge($input_params, $input_params[$val['value']]);
                }
            }
        }

        $this->pushDebugParams("input_params", 0, $input_params, $input_params);

        //get the first flow details
        $this->engine->_default_lang = $this->wsresponse->getLangRequestValue();
        $ws_flow_id = $ws_details['ws_master']['iWSFlowId'];
        $responce_arr = $this->handleFlowRecursively($func_name, $ws_flow_id, $input_params);
        if ($return_op === true) {
            return $responce_arr;
        } else {
            $this->wsresponse->sendWSResponse($responce_arr, $this->debug_params);
        }
    }

    /**
     * handleFlowRecursively method is used to process different flows in webservices.
     * @param string $func_name func_name is the webservice name.
     * @param integer $ws_flow_id ws_flow_id is the current flow id.
     * @param array $input_params input_params are the array of input to webservice flow.
     * @return array $responce_arr returns webservice response array records.
     */
    function handleFlowRecursively($func_name = '', $ws_flow_id = '', $input_params = array(), $ws_loop_name = '', $ws_loop_keys = array())
    {
        $flow_details = $this->getFlowDetails($ws_flow_id, $this->ws_details[$func_name]['ws_flow']);
        if (!$flow_details) {
            show_error('Some configuration error for flow direction.!', 404);
        }
        $_SESSION['__ci_exec_api_flow'] = $ws_flow_id;
        $this->engine->ws_details = $this->ws_details[$func_name];

        $has_next_flow = false;
        if ($flow_details) {
            $has_next_flow = true;
        }

        if ($has_next_flow) {
            $flow_id = $flow_details['iWSFlowId'];
            $flow_type = $flow_details['eFlowType'];
            $flow_output_param = $flow_details['vFlowLabel'];
            $flow_query_details = $flow_details['tSettingsJSON'];
            $next_flow_key = 'iChildTrueFlowId';
            switch ($flow_type) {
                case 'Query':
                    $output_arr = $this->engine->execute_query_by_settings($flow_query_details, $input_params, $this->settings_params);
                    $input_params[$flow_output_param] = $output_arr["data"];
                    $this->pushDebugParams($flow_output_param, $flow_id, $output_arr, $input_params, $ws_loop_name, $ws_loop_keys);
                    $query_type = $flow_query_details['query']['query_type'];
                    $max_rec = $flow_query_details['query']['max_rec'];
                    if (in_array($query_type, array("Insert", "Update", "Delete")) || ($query_type == "Select" && $max_rec == 1)) {
                        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
                    }
                    if ($flow_query_details['query']['max_rec'] == 1) {
                        if (count($this->inner_loop) == 0) {
                            $this->single_keys[] = $flow_output_param;
                            $this->wsresponse->makeUniqueParams($this->single_keys);
                        }
                    } else {
                        if (count($this->inner_loop) == 0) {
                            $this->multiple_keys[] = $flow_output_param;
                            $this->wsresponse->makeUniqueParams($this->multiple_keys);
                        }
                    }
                    break;
                case 'Condition':
                    $condition_result = $this->engine->execute_condition_flow($flow_query_details, $input_params);
                    $this->pushDebugParams($flow_output_param, $flow_id, $condition_result, $input_params, $ws_loop_name, $ws_loop_keys);
                    if (!$condition_result['success']) {
                        $next_flow_key = 'iChildFalseFlowId';
                    }
                    break;
                case 'Variable':
                    list($input_params, $var_vars) = $this->assignSingleVariables($flow_query_details, $input_params);
                    $this->pushDebugParams($flow_output_param, $flow_id, $var_vars, $input_params, $ws_loop_name, $ws_loop_keys);
                    if (count($this->inner_loop) > 0) {
                        if (is_array($var_vars) && count($var_vars) > 0) {
                            $input_params['__variables'][] = $var_vars;
                        }
                    }
                    break;
                case 'Dictionary':
                    $dictionary_result = $this->assignMultiVariables($flow_query_details, $input_params);
                    #$input_params = $this->wsresponse->assignSingleRecord($input_params, $dictionary_result);
                    $this->pushDebugParams($flow_output_param, $flow_id, $dictionary_result, $input_params, $ws_loop_name, $ws_loop_keys);
                    if (count($this->inner_loop) == 0) {
                        if (is_array($dictionary_result[0]) && count($dictionary_result[0]) > 0) {
                            $input_params[$flow_output_param][] = $dictionary_result[0];
                        }
                        $this->single_keys[] = $flow_output_param;
                        $this->wsresponse->makeUniqueParams($this->single_keys);
                    } else {
                        $this->multiple_keys[] = $flow_output_param;
                        $this->wsresponse->makeUniqueParams($this->multiple_keys);
                        if (is_array($dictionary_result[0]) && count($dictionary_result[0]) > 0) {
                            $input_params['__dictionaries'][$flow_output_param][] = $dictionary_result[0];
                        }
                    }
                    break;
                case 'StartLoop':
                    $loop_type = $flow_query_details['loop']['iterate_type'];
                    $loop_name = $flow_query_details['loop']['loop_name'];
                    $number_arr = $flow_query_details['loop']['number'];
                    $custom_arr = $flow_query_details['loop']['custom'];
                    $loop_tmp_array = $loop_tmp_dict = $loop_org_array = $loop_loc_array = array();
                    if ($loop_name != '' && array_key_exists($loop_name, $input_params)) {
                        $this->inner_loop[] = $loop_name;
                        $loop_org_array = $input_params[$loop_name];
                        $loop_loc_array = &$input_params[$loop_name];
                    } else {
                        $this->inner_loop[] = $flow_output_param;
                    }
                    if ($loop_type == "custom") {
                        if ($custom_arr['start_point']['type'] == "param") {
                            $custom_start = $input_params[$custom_arr['start_point']['value']];
                        } else if ($custom_arr['start_point']['type'] == "static") {
                            $custom_start = $custom_arr['start_point']['value'];
                        } else {
                            $custom_start = $custom_arr['start_point']['value'];
                            if (strstr($custom_start, $this->engine->requestVariable())) {
                                $custom_start = $this->engine->processRequestPregMatch($custom_start, $input_params);
                            }
                        }
                        if ($custom_arr['end_point']['type'] == "param") {
                            $custom_end = $input_params[$custom_arr['end_point']['value']];
                        } else if ($custom_arr['end_point']['type'] == "static") {
                            $custom_end = $custom_arr['end_point']['value'];
                        } else {
                            $custom_end = $custom_arr['end_point']['value'];
                            if (strstr($custom_end, $this->engine->requestVariable())) {
                                $custom_end = $this->engine->processRequestPregMatch($custom_end, $input_params);
                            }
                        }
                        $_loop_init = (is_array($custom_start)) ? count($custom_start) : intval($custom_start);
                        $_loop_oper = $custom_arr['operator'];
                        $_loop_step = intval($custom_arr['counter'] != 0) ? intval($custom_arr['counter']) : 1;
                        $_loop_end = (is_array($custom_end)) ? count($custom_end) : intval($custom_end);
                    } else {
                        $_loop_init = 0;
                        $_loop_oper = 'lt';
                        $_loop_step = 1;
                        if ($loop_type == "number") {
                            if ($number_arr['end_point']['type'] == "param") {
                                $number_end = $number_arr['end_point']['value'];
                            } else {
                                $number_end = $number_arr['end_point']['value'];
                                if (strstr($number_end, $this->engine->requestVariable())) {
                                    $number_end = $this->engine->processRequestPregMatch($number_end, $input_params);
                                }
                            }
                            $_loop_end = intval($number_end);
                        } else {
                            $_loop_end = count($loop_org_array);
                        }
                    }
                    $_loop_cond = $this->checkLoopCondition($_loop_oper, $_loop_init, $_loop_end);
                    $loop_tmp_flag = (is_array($loop_org_array[0])) ? true : false;
                    $loop_debug_arr = array("start_point" => $_loop_init, "end_point" => $_loop_end, "step" => $_loop_step, "loop" => $loop_name);
                    $this->pushDebugParams($flow_output_param, $flow_id, $loop_debug_arr, $input_params, $ws_loop_name, $ws_loop_keys);
                    while ($_loop_cond) {
                        $loop_input_params = $input_params;
                        if ($loop_tmp_flag) {
                            if (is_array($loop_org_array[$_loop_init])) {
                                $loop_input_params = $loop_org_array[$_loop_init] + $input_params;
                            }
                            unset($loop_input_params[$loop_name]);
                        } else if ($loop_name != '') {
                            $loop_input_params[$loop_name] = $loop_org_array[$_loop_init];
                        }
                        $loop_input_params['i'] = $_loop_init;
                        $loop_input_params['__dictionaries'] = $loop_tmp_dict;
                        $loop_keys = (is_array($loop_org_array[$_loop_init])) ? array_keys($loop_org_array[$_loop_init]) : array();
                        $response = $this->handleFlowRecursively($func_name, $flow_details[$next_flow_key], $loop_input_params, $loop_name, $loop_keys);
                        if (is_array($response['__dictionaries'])) {
                            $loop_tmp_dict = $response['__dictionaries'];
                            unset($response['__dictionaries']);
                        }
                        if (is_array($response['__variables'])) {
                            $input_params = $this->wsresponse->grabLoopVariables($response['__variables'], $input_params);
                            unset($response['__variables']);
                        }
                        if ($loop_tmp_flag) {
                            $loop_loc_array[$_loop_init] = $this->wsresponse->filterLoopParams($response, $loop_org_array[$_loop_init], $loop_input_params);
                        } else {
                            $loop_tmp_array[$_loop_init] = $this->wsresponse->filterLoopParams($response, $loop_org_array[$_loop_init], $loop_input_params);
                        }
                        if ($this->break_continue === 1) {
                            $this->break_continue = null;
                            break;
                        } else if ($this->break_continue === 2) {
                            $this->break_continue = null;
                            $_loop_init = $_loop_init + ($_loop_step);
                            $_loop_cond = $this->checkLoopCondition($_loop_oper, $_loop_init, $_loop_end);
                            continue;
                        }
                        $_loop_init = $_loop_init + ($_loop_step);
                        $_loop_cond = $this->checkLoopCondition($_loop_oper, $_loop_init, $_loop_end);
                    }
                    if ($loop_name != '') {
                        $loop_key = array_search($loop_name, $this->inner_loop);
                        unset($this->inner_loop[$loop_key]);
                        $this->inner_loop = array_values($this->inner_loop);
                    } else {
                        $loop_key = array_search($flow_output_param, $this->inner_loop);
                        unset($this->inner_loop[$loop_key]);
                        $this->inner_loop = array_values($this->inner_loop);
                    }
                    if ($loop_name == '') {
                        $input_params[$flow_output_param] = $loop_tmp_array;
                    } else if (!is_array($loop_org_array[0])) {
                        $input_params[$loop_name] = $loop_tmp_array;
                    }
                    if (is_array($loop_tmp_dict)) {
                        $input_params = array_merge($input_params, $loop_tmp_dict);
                    }
                    $flow_details = $this->getFlowDetails($flow_query_details['loop']['end_id'], $this->ws_details[$func_name]['ws_flow']);
                    if (!$flow_details) {
                        show_error('Some configuration error for ending one of the loop.!', 404);
                    }
                    break;
                case 'Break':
                    if (count($this->inner_loop) > 0) {
                        $condition_result = $this->engine->execute_break_continue_flow($flow_query_details, $input_params);
                        $this->pushDebugParams($flow_output_param, $flow_id, $condition_result, $input_params, $ws_loop_name, $ws_loop_keys);
                        if ($condition_result['success']) {
                            $this->break_continue = 1;
                            return $input_params;
                        }
                    }
                    break;
                case 'Continue':
                    if (count($this->inner_loop) > 0) {
                        $condition_result = $this->engine->execute_break_continue_flow($flow_query_details, $input_params);
                        $this->pushDebugParams($flow_output_param, $flow_id, $condition_result, $input_params, $ws_loop_name, $ws_loop_keys);
                        if ($condition_result['success']) {
                            $this->break_continue = 2;
                            return $input_params;
                        }
                    }
                    break;
                case 'EndLoop':
                    $this->pushDebugParams($flow_output_param, $flow_id, array(), $input_params, $ws_loop_name, $ws_loop_keys);
                    return $input_params;
                    break;
                case 'Function':
                    $function_type = $flow_query_details['function']['function_type'];
                    $function_name = $flow_query_details['function']['function_name'];
                    $return_type = $flow_query_details['function']['return_type'];
                    $response_type = $flow_query_details['function']['response_type'];
                    $overwrite_flow = $flow_query_details['function']['overwrite_flow'];
                    if ($function_type == 'webservice') {
                        $result_arr = $this->callInnerWebservcie($flow_query_details, $input_params);
                        $result_keys = is_array($result_arr) ? array_keys($result_arr) : array();
                        if ($result_arr["success"] == "-5") {
                            $final_result_arr = $this->wsresponse->makeValidationResponse($result_arr);
                            $this->wsresponse->sendWSResponse($final_result_arr, $this->debug_params);
                        } else if (in_array("settings", $result_keys) || in_array("data", $result_keys)) {
                            $custom_flag = true;
                            if ($return_type == "single") {
                                if (count($this->inner_loop) > 0) {
                                    if ($response_type == "mergeparams" || $response_type == "replaceparams") {
                                        $custom_flag = false;
                                    }
                                }
                            }
                            $this->output_params = is_array($result_arr["settings"]["fields"]) ? array_merge($this->output_params, $result_arr["settings"]["fields"]) : $this->output_params;
                            $this->wsresponse->makeUniqueParams($this->output_params);
                            if ($custom_flag) {
                                $this->custom_keys[] = $flow_output_param;
                                $this->wsresponse->makeUniqueParams($this->custom_keys);
                            }
                        }
                    } else {
                        if (method_exists($this->general, $function_name)) {
                            $result_arr['data'] = $this->general->$function_name($input_params);
                        }
                    }
                    if ($return_type == "single") {
                        $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);
                        if (count($this->inner_loop) > 0) {
                            if ($response_type == "addparams") {
                                $input_params[$flow_output_param] = $this->wsresponse->assignFunctionResponse($result_arr);
                            } elseif ($response_type == "mergeparams") {
                                if ($overwrite_flow == $ws_loop_name) {
                                    $input_params = $this->wsresponse->assignAppendRecord($input_params, $result_arr['data'][0]);
                                } else {
                                    $input_params[$overwrite_flow] = $this->wsresponse->assignAppendRecord($input_params[$overwrite_flow][0], $result_arr['data'][0]);
                                }
                            } elseif ($response_type == "replaceparams") {
                                if ($overwrite_flow == $ws_loop_name) {
                                    $input_params = $this->wsresponse->unsetAppendRecord($input_params, $result_arr['data'][0], $ws_loop_keys);
                                } else {
                                    $input_params[$overwrite_flow] = $this->wsresponse->assignFunctionResponse($result_arr);
                                }
                            }
                        } else {
                            if ($response_type == "addparams") {
                                $input_params[$flow_output_param] = $this->wsresponse->assignFunctionResponse($result_arr);
                                $this->single_keys[] = $flow_output_param;
                                $this->wsresponse->makeUniqueParams($this->single_keys);
                            } elseif ($response_type == "mergeparams") {
                                $input_params[$overwrite_flow] = $this->wsresponse->assignAppendRecord($input_params[$overwrite_flow][0], $result_arr['data'][0]);
                            } elseif ($response_type == "replaceparams") {
                                $input_params[$overwrite_flow] = $this->wsresponse->assignFunctionResponse($result_arr);
                            }
                        }
                    } else {
                        $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);
                        $input_params[$flow_output_param] = $this->wsresponse->assignFunctionResponse($result_arr);
                        if (count($this->inner_loop) == 0) {
                            $this->multiple_keys[] = $flow_output_param;
                            $this->wsresponse->makeUniqueParams($this->multiple_keys);
                        }
                    }
                    $this->pushDebugParams($flow_output_param, $flow_id, $result_arr, $input_params, $ws_loop_name, $ws_loop_keys);
                    break;
                case 'ExternalApi':
                    if (count($this->inner_loop) == 0) {
                        $this->multiple_keys[] = $flow_output_param;
                        $this->wsresponse->makeUniqueParams($this->multiple_keys);
                    }
                    $ws_url_input_arr = $ws_header_input_arr = array();
                    $ws_url_input_arr = $this->engine->execute_external_api_url($flow_query_details, $input_params);
                    $ws_header_input_arr = $this->engine->execute_external_api_header($flow_query_details, $input_params);
                    $request_static_params = $flow_query_details['request_fields'];

                    if (is_array($request_static_params) && count($request_static_params) > 0) {
                        foreach ($request_static_params as $r_key => $r_val) {
                            if (!array_key_exists($r_key, $input_params)) {
                                $temp_val = $this->engine->processRequestPregMatch($r_val, $input_params);
                                if ($temp_val == "") {
                                    $temp_val = $flow_query_details['static_values']['input_api_params'][$r_key];
                                }
                                $input_params[$r_key] = $temp_val;
                            }
                        }
                    }
                    $output_arr = $this->wsexternal->process_external_api($flow_query_details, $input_params, $ws_header_input_arr, $ws_url_input_arr, $ws_flow_id);
                    $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr['data']);
                    $input_params[$flow_output_param] = ($output_arr["success"]) ? $output_arr["data"] : array();
                    if (is_array($output_arr['status']) && count($output_arr['status']) > 0) {
                        foreach ($output_arr['status'] as $key => $val) {
                            $input_params[$flow_output_param . "_" . $key] = $val;
                        }
                    }
                    $input_params = $this->wsresponse->assignSingleRecord($input_params, array("http_response_code" => $output_arr['code']));
                    $this->pushDebugParams($flow_output_param, $flow_id, $output_arr["data"], $input_params, $ws_loop_name, $ws_loop_keys);
                    break;
                case 'NotifyEmail':
                    $output_arr = $this->engine->send_mail_notification($flow_query_details, $input_params);
                    $input_params[$flow_output_param] = $output_arr['success'];
                    $this->pushDebugParams($flow_output_param, $flow_id, $output_arr, $input_params, $ws_loop_name, $ws_loop_keys);
                    break;
                case 'PushNotify':
                    $output_arr = $this->engine->send_push_notification($flow_query_details, $input_params);
                    $input_params[$flow_output_param] = $output_arr['success'];
                    $this->pushDebugParams($flow_output_param, $flow_id, $output_arr, $input_params, $ws_loop_name, $ws_loop_keys);
                    break;
                case 'SMS':
                    $output_arr = $this->engine->send_sms_notification($flow_query_details, $input_params);
                    $input_params[$flow_output_param] = $output_arr['success'];
                    $this->pushDebugParams($flow_output_param, $flow_id, $output_arr, $input_params, $ws_loop_name, $ws_loop_keys);
                    break;
                case 'Finish':
                    $output_array = $func_array = array();
                    $output_array['settings'] = (is_array($flow_query_details['setting_fields'])) ? array_merge($this->settings_params, $flow_query_details['setting_fields']) : $this->settings_params;
                    $output_array['settings']['fields'] = (is_array($flow_query_details['output_fields'])) ? array_merge($this->output_params, $flow_query_details['output_fields']) : $this->output_params;
                    $output_array['data'] = $input_params;

                    $func_array['function']['name'] = $func_name;
                    $func_array['function']['messages'] = $this->ws_details[$func_name]['ws_messages'];
                    $func_array['function']['output_keys'] = $flow_query_details['output_keys'];
                    $func_array['function']['output_alias'] = $flow_query_details['output_alias'];
                    $func_array['function']['inner_keys'] = $flow_query_details['inner_keys'];
                    $func_array['function']['single_keys'] = $this->single_keys;
                    $func_array['function']['multiple_keys'] = $this->multiple_keys;
                    $func_array['function']['custom_keys'] = $this->custom_keys;
                    $this->pushDebugParams($flow_output_param, $flow_id, array(), $input_params, $ws_loop_name, $ws_loop_keys);
                    $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);
                    $has_next_flow = FALSE;
                    break;
            }
            if ($has_next_flow) {
                return $this->handleFlowRecursively($func_name, $flow_details[$next_flow_key], $input_params, $ws_loop_name, $ws_loop_keys);
            } else {
                return $responce_arr;
            }
        }
    }

    /**
     * getFlowDetails method is used to process different flows in webservices.
     * @param integer $flow_id flow_id is the specific flow id.
     * @param array $flow_array flow_array is the total flows for webservice.
     * @return array $ret_arr returns specific flow array data.
     */
    function getFlowDetails($flow_id = 0, $flow_array = array())
    {
        for ($i = 0; $i < count($flow_array); $i++) {
            if ($flow_array[$i]['iWSFlowId'] == $flow_id) {
                $ret_arr = $flow_array[$i];
                return $ret_arr;
            }
        }
        return false;
    }

    function assignSingleVariables($flow_details = array(), $input_params = array())
    {
        $new_vars = array();
        if (!is_array($flow_details['variables']) || count($flow_details['variables']) == 0) {
            return array($input_params, $new_vars);
        }
        $variables = $flow_details['variables'];
        foreach ($variables as $key => $val) {
            $var_type = $val['variable']['type'];
            $var_value = $val['variable']['value'];
            $data_type = $val['datatype'];
            $agn_type = $val['assignee']['type'];
            $agn_value = $val['assignee']['value'];
            if ($data_type == "array_single" || $data_type == "array_multi") {
                if ($agn_type == "param") {
                    $agn_value = $input_params[$agn_value];
                } else if ($agn_type == "function") {
                    if (method_exists($this->general, $agn_value)) {
                        $agn_value = $this->general->$agn_value($input_params);
                    }
                } else {
                    if (strstr($agn_value, $this->engine->requestVariable())) {
                        $agn_value = $this->engine->processRequestPregMatch($agn_value, $input_params, true);
                    }
                }
                $input_params[$var_value] = $agn_value;
                $new_vars[$var_value] = $agn_value;
            } else if ($data_type == "array_comma") {
                if ($agn_type == "param") {
                    $agn_value = $input_params[$agn_value];
                } else if ($agn_type == "function") {
                    if (method_exists($this->general, $agn_value)) {
                        $agn_value = $this->general->$agn_value($input_params);
                    }
                } else {
                    if (strstr($agn_value, $this->engine->requestVariable())) {
                        $agn_value = $this->engine->processRequestPregMatch($agn_value, $input_params, true);
                    }
                    if (strstr($agn_value, $this->engine->systemVariable())) {
                        $agn_value = $this->engine->processSystemPregMatch($agn_value, $input_params, true);
                    }
                    if (strstr($agn_value, $this->engine->sessionVariable())) {
                        $agn_value = $this->engine->processSessionPregMatch($agn_value, $input_params, true);
                    }
                }
                if (!is_array($agn_value)) {
                    $separator = (trim($val['separator']) != '') ? $val['separator'] : ",";
                    $agn_value = (trim($agn_value) != '') ? explode($separator, $agn_value) : array();
                }
                $input_params[$var_value] = $agn_value;
                $new_vars[$var_value] = $agn_value;
            } else {
                if ($agn_type == "param") {
                    $agn_value = $input_params[$agn_value];
                } else if ($agn_type == "function") {
                    if (method_exists($this->general, $agn_value)) {
                        $agn_value = $this->general->$agn_value($input_params);
                    }
                } else {
                    if (strstr($agn_value, $this->engine->requestVariable())) {
                        $agn_value = $this->engine->processRequestPregMatch($agn_value, $input_params);
                    }
                    if (strstr($agn_value, $this->engine->serverVariable())) {
                        $agn_value = $this->engine->processServerPregMatch($agn_value, $input_params);
                    }
                    if (strstr($agn_value, $this->engine->systemVariable())) {
                        $agn_value = $this->engine->processSystemPregMatch($agn_value, $input_params);
                    }
                    if (strstr($agn_value, $this->engine->sessionVariable())) {
                        $agn_value = $this->engine->processSessionPregMatch($agn_value, $input_params);
                    }
                    if ($agn_type == "expression") {
                        $agn_value = $this->general->evaluateMathExpression($agn_value);
                    }
                }
                $input_params[$var_value] = $agn_value;
                $new_vars[$var_value] = $agn_value;
            }
        }
        return array($input_params, $new_vars);
    }

    function assignMultiVariables($flow_details = array(), $input_params = array())
    {
        if (!is_array($flow_details['variables']) || count($flow_details['variables']) == 0) {
            return $input_params;
        }
        $eos = $this->general->getExpressionEvalObject();
        $variables = $flow_details['variables'];
//        $index_type = $variables[0]['index']['type'];
//        $index_value = $variables[0]['index']['value'];
//        $index_key = (is_int($index_value)) ? intval($index_value) : intval($input_params[$index_value]);
        $keyval_pairs = $variables[0]['assignee']['keyval'];
        if (!is_array($keyval_pairs) || count($keyval_pairs) == 0) {
            return $input_params;
        }
        $make_arr = $send_arr = array();
        foreach ($keyval_pairs as $key => $val) {
            $key_value = $val['keyname']['value'];
            $val_type = $val['valname']['type'];
            $val_value = $val['valname']['value'];
            $final_val = $val_value;
            if ($val_type == "param") {
                $final_val = $input_params[$val_value];
            } else if ($val_type == "function") {
                if (method_exists($this->general, $val_value)) {
                    $final_val = $this->general->$val_value($input_params);
                }
            } else {
                if (strstr($final_val, $this->engine->requestVariable())) {
                    $final_val = $this->engine->processRequestPregMatch($final_val, $input_params);
                }
                if (strstr($final_val, $this->engine->serverVariable())) {
                    $final_val = $this->engine->processServerPregMatch($final_val, $input_params);
                }
                if (strstr($final_val, $this->engine->systemVariable())) {
                    $final_val = $this->engine->processSystemPregMatch($final_val, $input_params);
                }
                if (strstr($final_val, $this->engine->sessionVariable())) {
                    $final_val = $this->engine->processSessionPregMatch($final_val, $input_params);
                }
                if ($val_type == "expression") {
                    $final_val = $this->general->evaluateMathExpression($final_val);
                }
            }
            $make_arr[$key_value] = $final_val;
        }
        $send_arr[0] = $make_arr;
        return $send_arr;
    }

    function checkLoopCondition($operator = '', $operand_1 = '', $operand_2 = '')
    {
        $operator = (in_array($operator, array("lt", "le", "gt", "ge"))) ? $operator : "lt";
        $flag = $this->general->compareDataValues($operator, $operand_1, $operand_2);
        return $flag;
    }

    /**
     * callInnerWebservcie method is used to call one webservices witin another webservice.
     * @param array $flow_output_param flow_output_param is the webservice flow name.
     * @param array $flow_details flow_details is the webservice flow details.
     * @param array $input_params input_params are the array of input to webservice flow.
     * @return array $result_arr returns specific webservice responce array.
     */
    function callInnerWebservcie($flow_details = array(), $input_params = array())
    {
        $function_name = $flow_details['function']['function_name'];
        $map_params = $flow_details['function']['map_params'];
        #$this->load->module("wsengine/enginecontroller");
        #$this->load->library('../front/wsengine/controllers/enginecontroller', NULL, $function_name);
        $sub_engine_obj = new EngineController();
        if (is_array($map_params) && count($map_params) > 0) {
            foreach ($map_params as $key => $val) {
                $temp_val = $val['value'];
                if ($val['type'] == "param") {
                    if (!array_key_exists($temp_val, $input_params) || $temp_val == "") {
                        continue;
                    }
                    $req_val = $input_params[$temp_val];
                } else {
                    $temp_val = $this->engine->processRequestPregMatch($temp_val, $input_params);
                    $temp_val = $this->engine->processServerPregMatch($temp_val, $input_params);
                    $temp_val = $this->engine->processSystemPregMatch($temp_val, $input_params);
                    $temp_val = $this->engine->processSessionPregMatch($temp_val, $input_params);
                    $req_val = $temp_val;
                }
                $input_params[$val['code']] = $req_val;
            }
        }
        $sub_engine_obj->unsetObjectVars();
        $result_arr = $sub_engine_obj->engineHandler($function_name, $input_params, true);
        return $result_arr;
    }

    function unsetObjectVars()
    {
        $this->ws_details = array();
        $this->debug_params = array();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->inner_loop = array();
        $this->expression_eval = null;
        $this->break_continue = null;
    }

    /**
     * jsonDecodeWSSettings method is used to decode webservice settings.
     * @param array $ws_params ws_params is the webservice settings details.
     * @param array $flag flag is to differ assoc array of normal array.
     * @return array $ret_arr returns webservice settings array.
     */
    function jsonDecodeWSSettings($ws_params = array(), $flag = false)
    {
        $ret_arr = array();
        if (is_array($ws_params) && count(v) > 0) {
            foreach ((array) $ws_params as $fd_key => $fd_val) {
                $tSettingsJSON = ($flag == true) ? json_decode($fd_val[0]['tSettingsJSON'], true) : json_decode($fd_val['tSettingsJSON'], true);
                $tSettingsArr = $this->recursiveStripSlashesArr($tSettingsJSON);
                if ($flag == true) {
                    $ws_params[$fd_key][0]['tSettingsJSON'] = $tSettingsArr;
                } else {
                    $ws_params[$fd_key]['tSettingsJSON'] = $tSettingsArr;
                }
            }
            $ret_arr = $ws_params;
        }
        return $ret_arr;
    }

    /**
     * unSerializeWSParams method is used to unserialize webservice validation settings.
     * @param array $ws_params ws_params is the webservice validation settings details.
     * @param array $flag flag is to differ assoc array of normal array.
     * @return array $ret_arr returns webservice settings array.
     */
    function unSerializeWSParams($ws_params = array(), $flag = false)
    {
        $ret_arr = $valid_arr = $comma_arr = $json_arr = array();
        if (is_array($ws_params) && count($ws_params) > 0) {
            foreach ((array) $ws_params as $fd_key => $fd_val) {
                $vValidation = ($flag == true) ? unserialize($fd_val[0]['vValidation']) : unserialize($fd_val['vValidation']);
                $vValidationArr = $this->recursiveStripSlashesArr($vValidation);
                $parameter = $fd_val['vParameter'];
                if (is_array($vValidationArr['rules']) && count($vValidationArr['rules']) > 0) {
                    $each_arr = array();
                    foreach ($vValidationArr['rules'] as $vKey => $vVal) {
                        $rule = $vVal['rule'];
                        $message = $vVal['message'];
                        if (strstr($vVal['message'], '#FIELD#') !== false) {
                            $message = str_replace('#FIELD#', $fd_val['vParameter'], $vVal['message']);
                        }
                        if (!in_array($rule, array("range", "rangelength", "min", "max", "minlength", "maxlength", "regex"))) {
                            $value = true;
                        } else {
                            $value = $vVal['value'];
                        }
                        $each_arr[] = array(
                            "rule" => $rule,
                            "value" => $value,
                            "message_code" => $vVal['message_code'] . "_" . $rule,
                            "message" => $message
                        );
                    }
                    $valid_arr[$parameter] = $each_arr;
                }
                if ($flag == true) {
                    $ws_params[$fd_key][0]['vValidation'] = $vValidationArr;
                } else {
                    $ws_params[$fd_key]['vValidation'] = $vValidationArr;
                }
                if ($fd_val['eParamMode'] == "ArrayComma") {
                    $comma_arr[] = array("value" => $fd_val['vParameter'], "split" => $fd_val['vParamString']);
                } else if ($fd_val['eParamMode'] == "JSONArray" || $fd_val['eParamMode'] == "JSONDictionary") {
                    $json_arr[] = array("value" => $fd_val['vParameter'], "type" => $fd_val['eParamMode']);
                }
            }
        }

        $ret_arr['ws_params'] = $ws_params;
        $ret_arr['valid_arr'] = $valid_arr;
        $ret_arr['comma_arr'] = $comma_arr;
        $ret_arr['json_arr'] = $json_arr;

        return $ret_arr;
    }

    /**
     * recursiveStripSlashesArr method is used to recursively strip the slashes after decoding.
     * @param array $ws_params ws_params is the webservice settings details.
     * @return array $ret_arr returns webservice settings array.
     */
    function recursiveStripSlashesArr($ws_params = array())
    {
        $ret_arr = array();
        foreach ((array) $ws_params as $key => $val) {
            if (is_array($val)) {
                $ret_arr[$key] = $this->recursiveStripSlashesArr($val);
            } else {
                $ret_arr[$key] = stripslashes($val);
            }
        }
        return $ret_arr;
    }

    function pushDebugParams($key = '', $id = '', $arr = array(), $tot = array(), $loop_name = '', $loop_keys = '')
    {
        if (is_null($this->input->get_post("ws_debug")) || is_null($this->input->get_post("ws_ctrls"))) {
            return;
        }
        $this->debug_params['ws_ctrls'][] = $id;
        if (!array_key_exists($key, $this->debug_params)) {
            $this->debug_params[$key] = $arr;
        } else if (count($this->inner_loop) > 0) {
            if (is_array($this->debug_params[$key])) {
                $this->debug_params[$key] = array_merge($this->debug_params[$key], $arr);
            } else {
                $this->debug_params[$key] = $arr;
            }
        }
        $ctrls = $this->input->get_post("ws_ctrls");
        $ctrls = ($ctrls) ? explode(",", $ctrls) : $ctrls;
        if (is_array($ctrls) && count($ctrls) > 0) {
            $_log_arr['flow_id'] = $id;
            $_log_arr['flow_name'] = $key;
            $_log_arr['loop_name'] = $loop_name;
            $_log_arr['loop_keys'] = $loop_keys;
            $_log_arr['params'] = $tot;
            $_log_arr['debug'] = $this->debug_params;
            $fp = @fopen($this->config->item('api_debugger_path') . $this->_logfile, 'w+');
            @fwrite($fp, serialize($_log_arr));
            @fclose($fp);
        }
        if (is_array($ctrls) && in_array($id, $ctrls)) {
            $output_debug_params = $this->debug_params;
            unset($output_debug_params['ws_ctrls']);
            $this->debug_params['ws_log'] = $this->_logfile;
            $this->wsresponse->sendWSResponse($output_debug_params, $this->debug_params);
        }
    }
}
