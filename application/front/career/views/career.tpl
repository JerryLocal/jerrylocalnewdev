<%assign var=settings value=$job_arr['settings']%>
<%assign var=totalPage value= ceil($settings.count/$settings.per_page)%>
<%assign var=tpages value= ceil($settings.count/$settings.per_page)%>
<%assign var=per_page value=$this->config->item('REC_LIMIT_FRONT')%>
<div class="main-containt">
  <div class="">
    <div class="row-fluid">
      <%$this->general->Breadcrumb('CLEAN','Careers')%> 
      <div class="">
        <div class="delivery-box">
          <h3>Jobs Available on Jerry</h3>
          <%if $job_arr['settings'].count gt 0%>
              <%assign var=jobs value=$job_arr['data']%>
          <div class="career-page">
            <%foreach $jobs as $job%>
            <div class="col-md-12">
              <div class="panel panel-my-default">
                <div class="panel-heading">
                  <div class="row order-detail-box-h">
                    <div class="col-sm-6">
                      <p><strong class="strong-txt">Job Title</strong> <%$job.mj_job_title%></p>
                      <p><strong class="strong-txt">Job Code</strong> <%$job.mj_job_code%></p>
                    </div>
                    <div class="col-sm-6 text-right">
                      <p><strong class="strong-txt">Sallery :</strong> <%$this->config->item('CURRENCY_SYMBOL')%> <%number_format($job.mj_from_salary,2)%>  - <%$this->config->item('CURRENCY_SYMBOL')%> <%number_format($job.mj_to_salary,2)%> </p>
                    </div>
                    <%if $job.mj_contact_email neq ''%>
                    <div class="col-sm-6 text-right"><p><strong class="strong-txt">Contact :</strong> <a href="#"><%$job.mj_contact_email%></a></p></div>
                    <%/if%>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="table-responsive">
                    <%$job.mj_job_description%>
                  </div>
                  <br/>
                  <div class="row">
                    <%if $job.mj_notes neq ''%>
                    <div class="col-sm-12"><strong>Notes :</strong> <i><%$job.mj_notes%></i></div>
                    <%/if%>
                    <div class="text-center text-left-m-mb"></div>
                  </div>
                </div>
                <!-- <div class="panel-footer">
                  
                </div> -->
              </div>
            </div>
            <%/foreach%>
            <div class="clear"></div>
          </div>
          <%/if%>
        </div>
        <%if $job_arr['settings'].count gt 0%>
          <div class="pagination-box">
            <div class="col-md-7">
              <div class="row">
                <div class="pagination-list">
                  <%assign var=displayResult value=($settings.curr_page*$per_page gt $settings.count) ? $settings.count : $settings.curr_page*$per_page%>

                  Displaying items <%(($settings.curr_page-1)*$per_page)+1%> to <%$displayResult%> of <%$settings.count%> total items              
                </div>
              </div>
            </div>
            <div class="col-md-5">
              <div class="row"> 
                <ul class="pagination">              
                  <%assign var=reload value=$this->config->item('site_url')|cat:"career.html?tpages="|cat:$tpages%>            
                    <%if $totalPage gt 1%>
                    <%$this->generalfront->paginate($reload,$job_arr['settings'].curr_page,$totalPage)%>
                    <%/if%>
                </ul>
              </div>
            </div>
          </div>
          <%/if%>
    </div>
  </div>
</div>
</div>