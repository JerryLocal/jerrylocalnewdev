<?php

/**
 * Description of Career Controller
 * 
 * @module Career
 * 
 * @class career.php
 * 
 * @path application\front\content\controllers\career.php
 * 
 * @author Sarvil Ajwaliya
 * 
 * @date 06.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Career extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();        
        $this->load->model('hbmodel');        
    }

    /**
     * index method is used to initialize index function.
     */
    public function career() {
        $getarr = $this->input->get();        
        $curr_page = $getarr['page'];
        $params = array();
        $params['page_index'] = $curr_page;
        $ws_name = 'get_jobs';
        $job_arr = $this->hbmodel->getData($ws_name, $params);
        // pr($job_arr,1);
        $job_detail = $merchant_details_arr['data'];
        $this->smarty->assign('job_arr',$job_arr);
    }
}