<%if $this->cart->total_items() gt 0 %>
<p>
    <span>
        (You have <%count($this->cart->contents(true))%> items in your cart)
    </span>
</p>
<%assign var=totalshipping value=0%>
<div class="overflow_auto">
<table class="data-table  cart-table">
    <tr>
        <th width="5%">
            &nbsp;
        </th>
        <th width="44%"  colspan="2" class="TAC">
            Product Details
        </th>
        <!-- <th width="30%" align="left">
            Product Name
        </th> -->
        <th width="15%" class="TAC">
            Price <!-- < %$this->config->item('CURRENCY_SYMBOL')%> -->
        </th>
        <th width="11%" class="TAC">
            Qty
        </th>        
        <th width="13%" class="TAC">
            Sub Total 
        </th>
        <th width="12%" class="TAC">
            Shipping
        </th>
    </tr>
    <%foreach $cartitemlist as $store_id => $itesm%> 
    <tr>
        <td>
            <%if $paymentview neq 'paymentview'%>
                <a class="removeitemfromcart" href="javascript:void(0);" data-pname='<%$itesm['name']%>' data-rowid='<%$itesm['rowid']%>'>
                    <i class="fa fa-trash-o text-danger-j">
                    </i>
                </a>
            <%/if%>    
        </td>
        <td>
            <a href="<%$this->general->CleanUrl('P', $itesm['id'])%>">
                <img src="<%$itesm['image']%>" class="img-responsive" alt="<%$itesm['name']%>"/>
            </a>
        </td>
        <td>
            <a target="_blank" style="color:#337ab7;" href="<%$this->general->CleanUrl('P', $itesm['id'])%>"><%$itesm['name']%></a>
            <%if isset($itesm['options']['size'])%>
            <br>
            <i class='text-primary help-text'>Size:  <%$itesm['options']['size']%></i>
            <%/if%>
            <%if isset($itesm['store_name'])%>
            <br/>
            <em class='text-info'>Seller: <b class='text-primary'><a style="color:#337ab7;" href="<%$this->config->item('site_url')%>store/<%$itesm['store_name']%>.html" target="_blank"><%$itesm['store_name']%></a></b></em>
            <!--<em class='text-info'>Seller: <b class='text-primary'><a style="color:#337ab7;" href="<%$this->general->CleanUrl('v', $itesm.store_id)%>" target="_blank"><%$itesm['store_name']%></a></b></em>-->
            <%/if%>
            <%if $paymentview neq 'paymentview'%>
            <!-- <a class="remove-link removeitemfromcart" href="javascript:void(0);" data-pname='<%$itesm['name']%>' data-rowid='<%$itesm['rowid']%>'>
                <span class="fa fa-remove">
                </span>
                &nbsp;Remove
            </a> -->
            <%/if%>
        </td>
        <td align="right">
            <%$this->config->item('CURRENCY_SYMBOL')%> <%number_format($itesm['price'],2)%>
        </td>
        <td align="center">
                <%if $paymentview neq 'paymentview'%>
                <div class="product-quantity-spinner">
                  <div class="input-group input-small">
                    <input id='<%$itesm['rowid']%>' type="text" class="spinner-input product-qty-control form-control" maxlength="3" readonly  value='<%$itesm['qty']%>'>
                    <div class="spinner-buttons input-group-btn btn-group-vertical">
                      <button type="button" class="btn spinner-up btn-xs blue">
                      <i class="fa fa-angle-up"></i>
                      </button>
                      <button type="button" class="btn spinner-down btn-xs blue">
                      <i class="fa fa-angle-down"></i>
                      </button>
                    </div>
                  </div>
                  <a href='javascript:void()' data-pid='<%$itesm['id']%>' data-rowid='<%$itesm['rowid']%>' class='hidden updateqtylink' >update</a>
                </div>
                <%else%>
                <%$itesm['qty']%>
                <%/if%>
                <!-- <input value="1" size="4" title="Qty" type="number" class="input-text qty" maxlength="12" id="flycart_cart_item_1933"/> -->
        </td>
        <td align="right">
            <%$this->config->item('CURRENCY_SYMBOL')%> <%number_format($itesm['subtotal'],2)%>                
        </td>
        <%if $itesm['free_shipping'] eq 'Yes' OR $itesm['shipping_charges'] eq 0%>
        <td align="center">
            <span class='text-success'>Free</span>                
            <%else%>
            <td align="right">
            <span class=''><!-- text-primary -->
                <%$this->config->item('CURRENCY_SYMBOL')%> <%number_format($itesm['shipping_charges'],2)%></span>
            <%assign var=totalshipping value=$totalshipping+$itesm['shipping_charges']%>
        </td>
        <%/if%>
    </tr>
    <%/foreach%>       
</table>
</div>

<!-- Below table for the order Total - start -->    
<table class="data-table" style="margin-top: -21px;">
    <tr>
        <td style="width:64.2%;" class="web-display" align="left"></td>
        <td   style="width:36%;">
            <div class="totals">
                <table>
                    <tr>
                        <td>
                            Total :
                        </td>
                        <td>
                            <span class="price">
                               <%$this->config->item('CURRENCY_SYMBOL')%> <%number_format($this->cart->total(),2)%>
                            </span>
                        </td>
                    </tr>
                    <tr style="background:#FBFBFB;">
                        <td>
                            Shipping :
                        </td>
                        <td>
                            <span class="price">
                                <%if $totalshipping gt 0%>
                                <%$this->config->item('CURRENCY_SYMBOL')%> <%number_format($totalshipping,2)%>
                                <%else%>
                                <span class='text-success'>Free <!-- Shipping --></span>                
                                <%/if%>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>
                                You Pay :
                            </strong>
                        </td>
                        <td>
                            <strong>
                                <span class="price">
                                    <%$this->config->item('CURRENCY_SYMBOL')%> <%number_format($this->cart->total()+$totalshipping,2)%>
                                </span>
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td  style="background:#FBFBFB;" colspan="2">
                        </td>
                    </tr>
                     <%if $paymentview neq 'paymentview'%>
                    <%if $this->
                        session->
                        userdata('iUserId') gt 0%>
                        <tr>
                            <td  style="background:#FBFBFB;" colspan="2">
                           
                                <a href='<%base_url('checkout/deliveryaddress')%>' title="Select Delivery Address" class="btn btn-common">
                                    Select Delivery Address
                                </a>
                            
                            </td>
                        </tr>
                        <%else%>
                        <tr>
                            <!-- <td  style="background:#FBFBFB;" colspan="2">
                                <button type="button" class="btn btn-common" data-dismiss="modal" data-toggle="modal" data-target="#myModal3">Select Delivery Address</button>
                            </td> -->

                            <td  style="background:#FBFBFB;" colspan="2">
                                <a href='javascript:void(0);' onclick='$("#loginbox").slideToggle("slow",function(e){$("#loginbox #vEmail").focus();});' type="button" title="Select Delivery Address" class="btn btn-common" >
                                    Select Delivery Address
                                </a>
                            </td>
                        </tr>
                        <%/if%>
                        <%/if%>
                        
                            <tr>
                            </tr>
                        </table>
                    </div>
                </td>
    </tr>
</table>
<!-- Above table for the order Total - End -->    
<%else%>

<div class="alert alert-warning fade in">
<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
<!-- <strong>Warning!</strong> --> Your shopping cart is empty..!!
</div>

<%/if%>