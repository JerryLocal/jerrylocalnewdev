<div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step <%$cartactive%>">
                    <a href="<%if $cartactive eq ''%><%base_url('checkout/myshoppingcart')%><%else%>javascript:void(0);<%/if%>" type="button" class="btn btn-primary btn-circle">
                        1
                    </a>
                    <p>
                        Cart
                    </p>
                </div>
                <div class="stepwizard-step <%$deliveryactive%>">
                    <a href="<%if $deliveryaddresslink neq ''%><%$deliveryaddresslink%><%else%>javascript:void();<%/if%>" type="button" class="btn btn-default btn-circle">
                        2
                    </a>
                    <p>
                        Delivery
                    </p>
                </div>
                <div class="stepwizard-step <%$paymentactive%>">
                    <a href="javascript:void();" type="button" class="btn btn-default btn-circle">
                        3
                    </a>
                    <p>
                        Payment
                    </p>
                </div>
            </div>
        </div>
