<div class=""> <!-- container -->
    <div class="delivery-box">
        <h3>
            <div class="row">
                <div class="col-sm-12">
                    My Cart
                </div>
            </div>
        </h3>        
        <%if $this->cart->total_items() gt 0 %>
        <!-- to show: you are  in which stpe  -->
        <%include file='steps.tpl'%>        
        
        <!-- login section for non loged in users   if logged in then not required  -->
        <%if $this->session->userdata('iUserId') eq 0%>
        <%include file='login_required.tpl'%>
        <%/if%>
        <%/if%>
        
        
        <!-- cart item listing -->
       
        <div class="table-box" id='cart_item_box'>
        <%include file='cart_items.tpl'%>
        </div>
        

        </div>
    </div>
