<div style='display:none;' class="payment-process" id='loginbox'>
    <div class="col-sm-6">
        <div class="payment-box new">
            <div class="col-lg-12">
                <form class="form-horizontal" name="frmSignin" id="frmSignin" action="<%base_url('user/user/login_action')%>" autocomplete="off" method="post">
                    <input type='hidden' name='ref' value='<%base64_encode(base_url('checkout/myshoppingcart'))%>'/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <!-- Email ID -->
                        </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control dev_SR" id="vEmail" name="vEmail" placeholder="Email ID"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <!-- Password -->
                        </label>
                        <div class="col-sm-7">
                            <input type="password" class="form-control" id="Password" name="Password" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class='col-sm-3'></div>
                        <div  class='col-sm-7'>

                            <div class="checkbox-list-remember">
                                <input type="checkbox" id="checkbox-1-3" class="checkbox-list" name="remember_me" value="Yes"/>
                                <label for="checkbox-1-3">
                                    Remember me
                                </label>
                            </div>
                            <a href="#" class="btn btn-link pad-w-0" data-dismiss="modal" data-toggle="modal" data-target="#myModal1">
                                Forgot Password
                            </a>
                        </div>    
                    </div>
                    <div class="col-sm-offset-3 col-sm-10"> <!-- bottom-mrgn -->
                        <button type="submit" class="btn btn-common">
                            Continue
                        </button>
                        <br/>
                    </div>
                    <div class="clear"></div>
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="payment-box new">
            <div class="col-lg-12">
                <div class="social-box-new">
                    <a href="<%$this->config->item('site_url')%>user/user/googlePlusLogin">
                        <img src="<%base_url("public/images/gplus-icon.png")%>" class="img-responsive" alt="Google+ Login"/>
                    </a>
                    <a href="<%$this->config->item('site_url')%>user/user/facebookLogin">
                        <img src="<%base_url("public/images/f-plus.png")%>" class="img-responsive" alt="Facebook Login"/>
                    </a>
                    <a data-target="#myModal2" data-toggle="modal" data-dismiss="modal">
                        <img src="<%base_url("public/images/signup-as-email-icon.png")%>" class="img-responsive" alt="" style="cursor:pointer;"/>
                    </a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
