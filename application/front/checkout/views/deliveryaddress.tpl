<%$this->js->add_js("delivery.js")%>
<%$this->js->add_js("intlTelInput/intlTelInput.min.js")%>
<%$this->css->add_css("intlTelInput/intlTelInput.css")%>
<%$this->css->css_src()%>
<div class="delivery"></div>
<div class="">  <!-- container -->
    <div class="delivery-box">
        <h3>
            <div class="row">
                <div class="col-sm-9">
                    Add a New Address
                </div>
                <div class="col-sm-3">
                    <span class="btn-back pull-right web-display">
                        <a href="<%base_url('checkout/myshoppingcart')%>">
                            Back to Cart
                        </a>
                    </span>
                    <span class="pull-right mob-display" style="width: 82px; position: absolute; bottom: 0px; right: -33px;">
                        <a href="<%base_url('checkout/myshoppingcart')%>">
                            <img src="<%$this->config->item('images_url')%>back_jerry.png" width="30%">
                        </a>
                    </span>
                </div>
            </div>
        </h3>
        <!-- to show: you are  in which stpe -->
        <%include file='steps.tpl'%>

        <div class="delivery-form">
            <form class="form-horizontal" role="form" method="post" action="<%base_url('checkout/Deliveryaddress/saveDeliveryAddress')%>" id="delivery-address-form">
                <div class="form-group">
                    <label for="inputName" class="col-sm-3 control-label">
                       Receiver Name <span class="required">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input name='mba_name' value='<%$customer_address['mba_name']%>' type="text" class="form-control"  placeholder="Receiver Name"/>
                        <div id="vFirstNameErr" class="errormsg text-danger"><%form_error('mba_name')%></div>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-3 control-label">
                        Street No / Name <span class="required">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input name='mba_address1'  value='<%$customer_address['mba_address1']%>' type="text" class="form-control"  placeholder="Street No / name"/>
                        <div id="mba_address1" class="errormsg text-danger"><%form_error('mba_address1')%></div>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label for="inputArea" class="col-sm-3 control-label">
                        Area Name <span class="required">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input name='mba_area'  value='<%$customer_address['mba_area']%>'  type="text" class="form-control" id="inputArea" placeholder="Area name"/>
                        <div id="mba_area" class="errormsg text-danger"><%form_error('mba_area')%></div>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-sm-3 control-label">
                        Country <span class="required">*</span>
                    </label>
                    <div class="col-sm-9">
                        <%$countrydropdown%>
                        <div id="mba_country_id" class="errormsg text-danger"><%form_error('mba_country_id')%></div>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-sm-3 control-label">
                        State/Region <span class="required">*</span>
                    </label>
                    <div class="col-sm-9">
                        <!-- <select name='mba_state_id' size="1" class="form-control">
                            <option value='2345'>Auckland</option>                            
                        </select> -->
                        <div id='statedropdowncon'>
                        <%$statedropdown%>
                        </div>
                        <div id="mba_state_id" class="errormsg text-danger"><%form_error('mba_state_id')%></div>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-sm-3 control-label">
                        City <span class="required">*</span>
                    </label>
                    <div class="col-sm-9">
                        <!-- <select name='mba_city_id' size="1" class="form-control">
                            <option value='1'>Ahipara</option>                            
                        </select> -->
                        <div id='citydropdowncon'>
                         <%$citydropdown%>
                         </div>
                        <div id="mba_city_id" class="errormsg text-danger"><%form_error('mba_city_id')%></div>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label for="inputPincode" class="col-sm-3 control-label">
                        Pin Code<span class="required">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input name='mba_pin_code' value='<%$customer_address['mba_pin_code']%>'  type="text" class="form-control" id="inputPincode" placeholder="Pin Code"/>
                        <div id="mba_pin_code" class="errormsg text-danger"><%form_error('mba_pin_code')%></div>
                    </div>
                    
                </div>
               
                <div class="form-group">
                    <label for="inputName" class="col-sm-3 control-label">
                        Contact No.<span class="required">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input name='mba_phone' id='mba_phone' value='<%$customer_address['mba_phone']%>' type="text" class="form-control" maxlength="16" />
                        <!-- <input name="mba_phone_hidden" id='mba_phone_hidden' type='hidden' /> -->
                        <div id="mba_phone_error" class="errormsg text-danger"><%form_error('mba_phone')%></div>
                    </div>
                    
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-10">
                        <button type="submit" class="btn btn-common" id="delivery_add_btn">
                            Proceed to Payment
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
