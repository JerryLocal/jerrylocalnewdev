<div class="container">
<div class="delivery-box">
<h3><div class="row"><div class="col-sm-12">My Cart</div></div></h3>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
          <div class="stepwizard-step active">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Cart</p>
      </div>
          <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle">2</a>
        <p>Delivery</p>
      </div>
          <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle">3</a>
        <p>Payment</p>
      </div>
        </div>
  </div>
<div class="payment-process">
<div class="col-sm-6">
<div class="payment-box new">
<div class="col-lg-12"><form class="form-horizontal" role="form">
 <div class="form-group">
  <label class="col-sm-3 control-label">Email ID</label>
  <div class="col-sm-5">
   <input type="text" class="form-control" placeholder="Email ID">
   </div>
  </div>
 <!--<div class="radio new">
  <label class="col-sm-3 control-label">&nbsp;</label>
  <div class="col-sm-9"><input type="radio">Continue without password<br>
   <span class="small-txt">(Don't have an account yet)</span></div>
 </div>
 <div class="radio">
  <label class="col-sm-3 control-label">&nbsp;</label>
  <div class="col-sm-9"><input type="radio">I have a Jerry account & password<br>
   <span class="small-txt">(Sign in to your account to get benifits)</span><br><br>
</div>
 </div>-->
 <div class="form-group">
  <label class="col-sm-3 control-label">Password</label>
  <div class="col-sm-5">
   <input type="password" class="form-control" placeholder="Password">
   </div>
  </div>
  <div class="form-group">
  <label class="col-sm-3 control-label">&nbsp;</label>
  <div class="col-sm-8">
   <input type="checkbox"> Remember me <a class="pull-right" href="#">Forgot your Password?</a>
   </div>
  </div>
   <div class="form-group">
  <div class="col-sm-offset-3 col-sm-10 bottom-mrgn">
   <button type="submit" class="btn btn-common">Continue</button><br>
   </div>
  </div>
</form></div>
</div>
</div>
<div class="col-sm-6">
<div class="payment-box new"><div class="col-lg-12"><div class="social-box-new">
<a href="#"><img src="images/gplus-icon.png" class="img-responsive" alt=""></a>
<a href="#"><img src="images/f-plus.png" class="img-responsive" alt=""></a>
<a href="#"><img src="images/signup-as-email-icon.png" class="img-responsive" alt=""></a>
</div></div></div>
</div>
</div>
<div class="table-box">
<p><span>(You have 4 items in your cart)</span></p>
<table class="data-table">
<tr><th colspan="7" align="left" class="store-name">ABC store (abc@store.com)</th></tr>
<tr>
                        <th width="5%">&nbsp;</th>
                        <th width="14%" align="left">Product Photo</th>
                        <th width="30%" align="left">Product Name</th>
                        <th width="15%">Price in $</th>
                        <th width="11%">Quantity</th>
                        <th width="12%">Shipping</th>
                        <th width="13%" align="center">Amount in $</th>
                        </tr>
<tr>
                        <td><a href="#"><i class="fa fa-trash-o text-danger-j"></i></a></td>
                        <td><a href="#"><img src="images/17_1.jpg" class="img-responsive" alt=""></a></td>
                        <td>Product Name<br>
<a class="remove-link" href="#"><span class="fa fa-remove"></span>&nbsp;Remove</a></td>
                        <td align="center">50</td>
                        <td align="center"><input value="1" size="4" title="Qty" type="number" class="input-text qty" maxlength="12" id="flycart_cart_item_1933"></td>
                        <td align="center">5</td>
                        <td align="center">60</td>
                        </tr>
                        <tr>
                        <td><a href="#"><i class="fa fa-trash-o text-danger-j"></i></a></td>
                        <td><a href="#"><img src="images/17_1.jpg" class="img-responsive" alt=""></a></td>
                        <td>Product Name<br>
<a class="remove-link" href="#"><span class="fa fa-remove"></span>&nbsp;Remove</a></td>
                        <td align="center">50</td>
                        <td align="center"><input value="1" size="4" title="Qty" type="number" class="input-text qty" maxlength="12" id="flycart_cart_item_1933"></td>
                        <td align="center">5</td>
                        <td align="center">60</td>
                        </tr>
                        
</table>
<table class="data-table cart-table">
<tr><th colspan="7" align="left" class="store-name">XYZ store (xyz@store.com)</th></tr>
<tr>
                        <th width="5%">&nbsp;</th>
                        <th width="14%" align="left">Product Photo</th>
                        <th width="30%" align="left">Product Name</th>
                        <th width="15%">Price in $</th>
                        <th width="11%">Quantity</th>
                        <th width="12%">Shipping</th>
                        <th width="13%" align="center">Amount in $</th>
                        </tr>
<tr>
                        <td><a href="#"><i class="fa fa-trash-o text-danger-j"></i></a></td>
                        <td><a href="#"><img src="images/17_1.jpg" class="img-responsive" alt=""></a></td>
                        <td>Product Name</td>
                        <td align="center">50</td>
                        <td align="center"><input value="1" size="4" title="Qty" type="number" class="input-text qty" maxlength="12" id="flycart_cart_item_1933"></td>
                        <td align="center">5</td>
                        <td align="center">60</td>
                        </tr>
                        <tr>
                        <td><a href="#"><i class="fa fa-trash-o text-danger-j"></i></a></td>
                        <td><a href="#"><img src="images/17_1.jpg" class="img-responsive" alt=""></a></td>
                        <td>Product Name</td>
                        <td align="center">50</td>
                        <td align="center"><input value="1" size="4" title="Qty" type="number" class="input-text qty" maxlength="12" id="flycart_cart_item_1933"></td>
                        <td align="center">5</td>
                        <td align="center">60</td>
                        </tr>
                        <tr>
                        <td colspan="4" align="left">
                       
                        </td>
                        <td colspan="3"><div class="totals">
                        <table>
                        <tr><td>Total :</td>
                        <td><span class="price">$2,000.00</span></td>
                        </tr>
                        <tr style="background:#FBFBFB;"><td>Shipping :</td>
                        <td><span class="price">$2,0.00</span></td>
                        </tr>
                        <tr>
                        <td><strong>You Pay :</strong></td>
                        <td><strong><span class="price">$2,020.00</span></strong></td>
                        </tr>
                        <tr>
                        <td  style="background:#FBFBFB;" colspan="2"> </td>
                        </tr>
                        <tr>
                        <td  style="background:#FBFBFB;" colspan="2"><button type="button" title="Select Delivery Address" class="btn btn-common">Select Delivery Address</button></td>
                        </tr>
                        <tr>
                        </tr>
                        </table>
                        </div></td>
                        </tr>
                        
</table>
</div>
</div>
</div>