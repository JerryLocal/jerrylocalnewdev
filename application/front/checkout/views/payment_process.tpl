<%$this->js->add_js("payment.js")%>
<div class=""> <!-- container -->
    <div class="delivery-box">
        <h3>
            <div class="row">
                <div class="col-sm-10">
                    Payment Process
                </div>
            </div>
        </h3>
        <!-- to show: you are  in which stpe -->
        <%include file='steps.tpl'%>
        <div class="table-box" id='cart_item_box'>
            <%include file='cart_items.tpl'%>
        </div>
        <div class="payment-process">
            <div class="col-sm-6">
                <div class="payment-box">
                    <div class="total-payment">
                        Delivery Address
                        <span class="pull-right edit-button">
                            <a href="<%base_url('checkout/deliveryaddress')%>">
                                <span class="fa fa-edit" style="margin-top:8px">
                                </span>
                            </a>
                        </span>
                    </div>
                    <div class="payment-through">
                        <div class="payment-address">
                            <p>
                                <%$deliveryaddress->
                                vName%>
                                <br/>
                                <%$deliveryaddress->
                                vAddress1%>
                                <%$deliveryaddress->
                                vAddress2%>,
                                <%$deliveryaddress->
                                vArea%>
                                <br/>
                                <%$this->generalfront->getcolumnValue('iCityId',$deliveryaddress->
                                iCityId,'vCity','mod_city')%>,
                                <%$this->generalfront->getcolumnValue('iStateId',$deliveryaddress->
                                iStateId,'vState','mod_state')%>,
                                <%$deliveryaddress->
                                vPinCode%>
                                <br/>
                                <%$this->generalfront->getcolumnValue('iCountryId',$deliveryaddress->
                                iCountryId,'vCountry','mod_country')%>.
                                <br/>
                                Phone:
                                <%$deliveryaddress->
                                vPhone%>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="payment-box">
                    <div class="payment-through">
                        <form method="post" action='<%base_url('checkout/placeorder')%>' class="form-horizontal" role="form" id="payment_form">
                            <fieldset>                                
                                <legend style="position:relative;">
                                    <div class="paypal_paynow">
                                        <!-- <img src="<%$this->config->item('images_url')%>paypay-icon.png"> -->
                                    </div>
                                    <span>Payment</span>
                                </legend>
                                <!-- <div class="form-group">
                                    <label class="col-sm-3 control-label" for="card-holder-name">
                                        Card Type
                                    </label>
                                    <div class="col-sm-9 err-left">
                                        <select class="select form-control required" id="user_credit_card_type" name="card-type">
                                            <option value="visa" selected>
                                                Visa
                                            </option>
                                            <option value="mastercard">
                                                Mastercard
                                            </option>
                                            <option value="discover">
                                                Discover
                                            </option>
                                            <option value="amex">
                                                Amex
                                            </option>
                                        </select>
                                    </div>
                                </div> -->
            <!--  <div class="form-group">
            <label class="col-sm-3 control-label" for="card-holder-name">
            Name on Card
            </label>
            <div class="col-sm-9">
            <input type="text" class="form-control" name="card-holder-name" id="card-holder-name" placeholder="Card Holder's Name" />
            </div>
        </div> -->
        <div class="form-group">
            <label class="col-sm-3 control-label" for="card-number">
                Card Number
            </label>
            <div class="col-sm-9 err-left">
                <!-- 4417119669820331 -->
                <input value='' type="text" class="form-control" name="card-number" id="card-number" placeholder="Debit/Credit Card Number"  maxlength="16"/>
                <div id="vFirstNameErr" class="errormsg text-danger"><%form_error('card-number')%></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="expiry-month">
                Expiration Date
            </label>
            <div class="col-sm-9 err-left">
                <div class="row">
                    <div class="col-xs-6">
                        <select class="form-control col-sm-2" name="expiry-month" id="expiry-month">
                            <option value=''>Month</option>
                            <%for $foo=1 to 12%>
                            <option value="<%$foo%>" >
                                <%sprintf('%02d', $foo)%>
                            </option>
                            <%/for%>
                        </select>
                        <div id="vFirstNameErr" class="errormsg text-danger">
                            <%form_error('expiry-month')%></div>
                        </div>
                        <div class="col-xs-6">
                            <select class="form-control" name="expiry-year">
                                <option value=''>Year</option>
                                <%for $ct=0 to 11%>
                                <option value="<%date('y')+$ct%>" >
                                    <%date('Y')+$ct%>
                                </option>
                                <%/for%>
                            </select>
                            <div id="vFirstNameErr" class="errormsg text-danger"><%form_error('expiry-year')%></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="cvv">
                    Card CVV
                </label>
                <div class="col-sm-9 err-left">
                    <!-- 012 -->
                    <input value='' type="text" class="form-control text-center" name="cvv" id="cvv" placeholder="CVV Code" maxlength="3" minlength='3' style="width:167px"/>
                    <div id="vFirstNameErr" class="errormsg text-danger">
                        <%form_error('cvv')%></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10 text-right">
                        <button type="submit" class="btn btn-success" id="pay_btn">
                            Pay Now
                        </button>                        
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <a href="https://www.paymentexpress.com">
                        <img src="<%base_url("public/images/paymentexpress.PNG")%>" alt="Payment Processor" width="200" height="">
                        </a>
                        <img src="<%base_url("public/images/mastercardvisa.png")%>" alt="Visa & MasterCard" height="" width='143'>
                        <br>
                        <small>Note: All transactions are made by <a href='http://www.paymentexpress.com/about/about_paymentexpress/privacy_policy.htmlle' style='color:#CF151D;'>payment express</a> and its secure and encrypted and credit card information is never stored by us</small>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
</div>                    
</div>
</div>
</div>
