<?php

/**
 * Description of Career Controller
 *
 * @module Career
 *
 * @class cart.php
 *
 * @path application\front\content\controllers\cart.php
 *
 * @author Dhananjay singh
 *
 * @date 20.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Myshoppingcart extends HB_Controller
{
    
    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    private $response = array();
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');
        $this->smarty->assign('cartactive','active');

    }

    public function index() {        
        $cartcontent   =    $this->cart->contents(true);
        
        $cartitemlist  =    array();
        /*foreach ($cartcontent as $key => $value) {            
            $cartitemlist[$value['store_id']][]=$value;
        }*/
        // Size Fix
        foreach($cartcontent as $key=>$value){
            if(is_array($value)){
            $cartcontent[$key]['shipping_charges'] = $value['shipping_charges']*$value['qty'];
            $cartcontent[$key]['shipping_chargesPerQty'] = $value['shipping_charges'];
                foreach($value as $mykey=>$myvalue){
                    if($mykey == 'options'){
                        foreach($myvalue as $otpionkey=>$otionvalue){
                             if(strtolower($otpionkey) == 'size'){
                                $newoption = $otionvalue;
                                unset($cartcontent[$key][$mykey][$otpionkey]);
                                $cartcontent[$key][$mykey]['size']=$otionvalue;
                             }
                        }
                    }
                }
            }
        }
        // End size fix
        $this->smarty->assign('cartitemlist',$cartcontent);
        $this->loadView('myshoppingcart');
    }


    public function cartItem(){
        $cartcontent   =    $this->cart->contents(true);
        $cartitemlist  =    array();
        /*foreach ($cartcontent as $key => $value) {            
            $cartitemlist[$value['store_id']][]=$value;
        }*/
        foreach($cartcontent as $key=>$value){
            if(is_array($value)){
                $cartcontent[$key]['shipping_charges'] = $value['shipping_charges']*$value['qty'];
                $cartcontent[$key]['shipping_chargesPerQty'] = $value['shipping_charges'];
            }
        }
        $this->smarty->assign('cartitemlist',$cartcontent);
        echo $this->parser->parse('cart_items.tpl','',true);
        exit;
    }
}
