<?php

/**
 * Description of Career Controller
 *
 * @module Career
 *
 * @class cart.php
 *
 * @path application\front\content\controllers\cart.php
 *
 * @author Dhananjay singh
 *
 * @date 20.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Placeorder extends HB_Controller
{
    
    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    private $response = array();
    private $fshippingcost = 0;
    private $fsubtotal = 0;
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');
        $this->fshippingcost = 0;
        $this->fsubtotal = 0;
        $this->load->library('PaymentExpress');
    }
    
    public function index() {
        if (!$this->validate_ccdetail()) {
            return true;
        }
        
        $post = $this->input->post();
        $order = array();
        $deliveryaddress = $this->session->userdata('deliveryaddress');
        $address = json_decode($deliveryaddress, true);
        $order['vBuyerName'] = $address['vName'];
        $order['vBuyerAddress1'] = $address['vAddress1'];
        $order['vBuyerAddress2'] = $address['vAddress2'];
        $order['vBuyerArea'] = $address['vArea'];
        $order['iBuyerCountryId'] = $address['iCountryId'];
        $order['iBuyerCityId'] = $address['iCityId'];
        $order['iBuyerStateId'] =$address['iStateId'];
        $order['vBuyerPinCode'] = $address['vPinCode'];
        $order['vBuyerPhone'] = $address['vPhone'];
        $order['vBuyerCountry'] = $address['vBuyerCountry'];
        $order['vBuyerState'] = $address['vBuyerState'];
        $order['vBuyerCity'] = $address['vBuyerCity'];

        $suborder = $this->validate_items();

        $order['iBuyerId'] = $this->session->userdata('iUserId');
        $order['ePaymentStatus'] = 'Unpaid';
        $order['dDate'] = 'NOW()';
        $order['vBuyerEmail'] = $this->session->userdata('vEmail');
        $order['vBuyerIp'] = $this->ip_address();
        $order['order_item'] = $suborder;
        $order['fShippingCost'] = $this->fshippingcost;
        $order['fSubTotal'] = $this->fsubtotal - $order['fShippingCost'];
        $order['fOrderTotal'] = $order['fSubTotal'] + $order['fShippingCost'];

        $card_detail = array();
        $card_detail['type'] = $post['card-type'];
        $card_detail['number'] = $post['card-number'];
        $card_detail['expire_month'] = $post['expiry-month'];
        $card_detail['expire_year'] = $post['expiry-year'];
        $card_detail['cvv2'] = $post['cvv'];

        $params=array();
        $params['amount']=$order['fOrderTotal'];
        $params['merchantReference']=$order['vBuyerName'] . '-' . $order['iBuyerId']. date('Y-m-d h:i:s');
        $params['returnUrl']='';
        $params['txnRef']=$order['iBuyerId']. date('Ymdhis');
        $params['CardHolderName']=$order['vBuyerName'];
        $params['CardNumber']=$post['card-number'];
        $params['Cvc2']=$post['cvv'];
        $params['ExpiryMonth']=$post['expiry-month'];
        $params['ExpiryYear']=$post['expiry-year'];//date('y',strtotime($params['ExpiryYear'].'-2-2'));
        $params['returnUrl']=base_url('checkout/placeorder');

       

       

        $response=$this->paymentexpress->processPayment($params);
        
        if($response['success']==1){
            $order['tPaymentDetail'] = $response['transactionId'];
        }else{
            $this->session->set_flashdata('failure', $response['message']);
            redirect(base_url('checkout/payment'));           
        }
        
        //if ($response['status'] == 'completed' || $payment->getState() == 'approved') {
        $order['ePaymentStatus'] = 'Paid';
        $this->session->set_flashdata('success', 'Your order has been placed successfully.');
        /*Below Code for the update placed order QTY - start*/
        $placedorderqty = json_decode($suborder);
        $decQty = array();
        foreach ($placedorderqty as $key => $value) {
            $decQty['iStoreId']=$value->Item_iMstStoreDetailId;
            $decQty['iMstProductsId']=$value->Item_iMstProductsId;
            $decQty['iStock']=$value->Item_iProductQty;
            $this->generalfront->updatePlacedOrderQTY($decQty);            
        }
            /*above Code for the update placed order QTY - End*/
        /*} 
        else {
            $this->session->set_flashdata('success', 'PENDING');
        }*/
        $order_placed = $this->hbmodel->getData('place_order', $order);
        // store entry to address table
        $this->generalfront->insertaddress($address,$this->session->userdata('iUserId'));
        //pr($payment);die;
        if ($order_placed['settings']['success'] == 0) {
            $this->session->set_flashdata('failure', $order_placed['settings']['message']);
            redirect('checkout/myshoppingcart');
        } 
        else {
            $this->cart->destroy();
            redirect('checkout/thankyou');
            //redirect('my-orders.html');
        }
    }
    
    private function ip_address() {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } 
        else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }
        return $ip_address;
    }
    
    private function validate_ccdetail() {
        $this->load->library('form_validation');
        #$this->form_validation->set_rules('card-type', 'Cart Type', 'required');
        $this->form_validation->set_rules('card-number', 'Card Number',array('required','numeric'));
        $this->form_validation->set_rules('expiry-month', 'Expiry month', 'required');
        $this->form_validation->set_rules('expiry-year', 'Expiry year', 'required');
        $this->form_validation->set_rules('cvv', 'CVV number', array('required','numeric'));
        if ($this->form_validation->run() != FALSE) {
            return true;
        } 
        else {
            $this->load->module('checkout/payment');
            $this->payment->index();
            $this->loadView('payment_process');
            return false;
        }
    }
    
    /**
     *   validate_product private method
     */
    private function validate_items() {
        $cartcontent = $this->cart->contents(true);
        $suborder = array();
        foreach ($cartcontent as $key => $value) {
            $params = array();
            $ws_name = 'Validate_product_for_cart';
            $params['iProductId'] = $value['id'];
            $params['iQuantity'] = $value['qty'];
            $product_detail_arr = $this->hbmodel->getData($ws_name, $params);

            if ($product_detail_arr['settings']['success'] == 0) {
                $this->session->set_flashdata('failure', '<b>' . $value['name'] . '</b> is not available. please remove this item.');
                redirect('checkout/myshoppingcart');
                break;
            }
            $pdata = $product_detail_arr['data'][0];
            $item = array();
            $item['Item_iMstStoreDetailId'] = $pdata['mp_store_id'];
            $item['Item_iMstProductsId'] = $pdata['mp_mst_products_id'];
            $item['Item_vProductName'] = $pdata['mp_title'];
            $item['Item_vProductSku'] = $pdata['mp_sku'];
            $item['Item_fProductRegularPrice'] = $pdata['mp_regular_price'];
            $item['Item_fProductSalePrice'] = $pdata['mp_sale_price'];
            $item['Item_fProductPrice'] = $pdata['mp_sale_price'] > 0 ? $pdata['mp_sale_price'] : $pdata['mp_regular_price'];
            $item['Item_iProductQty'] = $value['qty'];
            $item['Item_tProductOption'] = '';
            if(is_array($value['options'])){
            foreach($value['options'] as $otpionkey=>$otionvalue){
                 if(strtolower($otpionkey) == 'size'){
                     $item['Item_tProductOption'] = isset($value['options'][$otpionkey]) ? $value['options'][$otpionkey] : '';
                 }
            }
            }
           // $item['Item_tProductOption'] = isset($value['options']['SIZE']) ? $value['options']['SIZE'] : '';
           // if(empty($item['Item_tProductOption'])){
           //     $item['Item_tProductOption'] = isset($value['options']['size']) ? $value['options']['size'] : '';
           // }
            $item['Item_tExtraInfo'] = $pdata['mp_description'];
            $item['Item_iReturnPeriod'] = $pdata['mp_return_days'];

            if($pdata['mp_free_shipping'] == 'Yes'){
                $item['Item_fTotalCost'] = (($item['Item_iProductQty'] * $item['Item_fProductPrice']));
            }else{
                $item['Item_fTotalCost'] = (($item['Item_iProductQty'] * $item['Item_fProductPrice'])+($pdata['mp_shipping_charge']*$value['qty']));    
            }
            
            $item['Item_fAdminCommisionPer'] =$pdata['mc_commission'];
            $item['Item_fGatewayTDRPer'] =$this->config->item('TDR_RATE');
            $item['Item_fAdminCommision'] =($item['Item_fTotalCost']*$pdata['mc_commission'])/100;
            if($item['Item_fGatewayTDRPer']>0)
            $item['Item_fGatewayTDR'] = ($item['Item_fTotalCost']*$item['Item_fGatewayTDRPer'])/100;
            else
            $item['Item_fGatewayTDR'] = 0;

            







            $item['Item_fShippingCost'] = $pdata['mp_free_shipping'] == 'Yes' ? 0 : $pdata['mp_shipping_charge']*$value['qty'];
            $suborder[] = $item;
            $this->fshippingcost = $this->fshippingcost + $item['Item_fShippingCost'];
            $this->fsubtotal = $this->fsubtotal + $item['Item_fTotalCost'];
        }
        return json_encode($suborder);
    }







 

       

       
    
    public function parse_xml($data)
    {
         
        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser, $data, $vals);
        xml_parser_free($xml_parser);
            
        $params = array();
        $level = array();
        foreach ($vals as $xml_elem) {
            if ($xml_elem['type'] == 'open') {
                if (array_key_exists('attributes',$xml_elem)) {
                list($level[$xml_elem['level']],$extra) = array_values($xml_elem['attributes']);
            } 
            else {
                $level[$xml_elem['level']] = $xml_elem['tag'];
                }
            }
            if ($xml_elem['type'] == 'complete') {
                $start_level = 1;
                $php_stmt = '$params';
                            
                while($start_level < $xml_elem['level']) {
                    $php_stmt .= '[$level['.$start_level.']]';
                    $start_level++;
                }
                    
                        
                 
                $php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';
                if (isset($xml_elem['value']))      
                    eval($php_stmt);
            }

        }
            
        return $params;
    }
}
