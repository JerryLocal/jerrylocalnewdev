<?php

/**
 * Description of Career Controller
 *
 * @module Career
 *
 * @class cart.php
 *
 * @path application\front\content\controllers\cart.php
 *
 * @author Dhananjay singh
 *
 * @date 20.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Deliveryaddress extends HB_Controller
{
    
    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    private $response = array();
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');  
       
         $this->load->library('form_validation');      
         $this->smarty->assign('deliveryactive','active');
    }

    public function index() {
        $params=array();
        $params['iCustomerId']=$this->session->userdata('iUserId');
        $customer_address     = $this->hbmodel->getData('get_customer_address', $params);
        $this->getCountry($customer_address['data'][0]['mba_country_id']);
        $this->getState($customer_address['data'][0]['mba_country_id'],$customer_address['data'][0]['mba_state_id']);
        $this->getCity($customer_address['data'][0]['mba_state_id'],$customer_address['data'][0]['mba_city_id']);
        if($customer_address['settings']['success']==1){
            $this->smarty->assign('customer_address',$customer_address['data'][0]);
        }
        else{
            $address=array();
            foreach ($customer_address['settings']['fields'] as $key => $value) {
                $address[$value]='';
            }
            $this->smarty->assign('customer_address',$address);
        }
        $this->loadView('deliveryaddress');        
    }


    public function saveDeliveryAddress(){
        $this->validate_address();
        $post       = $this->input->post();        
        $address    = array();
        // pr($post,1);
        $address['iAdminId']    = $this->session->userdata('iUserId');
        $address['vName']     = $post['mba_name'];
        $address['vAddress1'] = $post['mba_address1'];
        $address['vAddress2'] = '';
        $address['vArea']     = $post['mba_area'];
        $address['iCountryId'] = $post['mba_country_id'];
        $address['iStateId']  = $post['mba_state_id'];
        $address['iCityId']   = $post['mba_city_id'];
        $address['vPinCode']  = $post['mba_pin_code'];
        $address['vPhone']    = $post['mba_phone'];        
        // $address['vPhone']    = $post['mba_phone_hidden'];        
        $params = array();
        $params['iCustomerId'] = $this->session->userdata('iUserId');
        $customer_address = $this->hbmodel->getData('get_customer_address', $params);
        if ($customer_address['settings']['success'] == 0) {
            $this->hbmodel->getData('add_address', $address);
        }
        $vBuyerCountry = $this->generalfront->getColumnValue('iCountryId',$_POST['mba_country_id'],'vCountry','mod_country');
        $vBuyerState = $this->generalfront->getColumnValue('iStateId',$_POST['mba_state_id'],'vState','mod_state');
        $vBuyerCity = $this->generalfront->getColumnValue('iCityId',$_POST['mba_city_id'],'vCity','mod_city');
        $address['vBuyerCountry'] = $vBuyerCountry;
        $address['vBuyerState']  = $vBuyerState;           
        $address['vBuyerCity']  = $vBuyerCity; 
        $this->session->set_userdata('deliveryaddress',json_encode($address));

        $this->session->set_flashdata('success',$order_placed['settings']['message']);
        redirect(base_url('checkout/payment'));
    }




    private function validate_address() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mba_name', 'Buyer Name', 'required');
        $this->form_validation->set_rules('mba_address1', 'Address', 'required');
        $this->form_validation->set_rules('mba_area', 'Area', 'required');
        $this->form_validation->set_rules('mba_country_id', 'Country', 'required');
        $this->form_validation->set_rules('mba_city_id', 'City', 'required');
        $this->form_validation->set_rules('mba_pin_code', 'Pin Code', 'required');
        $this->form_validation->set_rules('mba_state_id', 'State', 'required');
        $this->form_validation->set_rules('mba_phone', 'Phone Number', 'required|numeric');
        if ($this->form_validation->run() != FALSE) {
            return true;
        } 
        else {
            $deliveryaddress = $this->load->module('checkout/deliveryaddress');
            $this->deliveryaddress->index();
            $this->loadView('deliveryaddress');
        }
    }



    function getCountry($iCountryId=0){
        $country     = $this->hbmodel->getData('get_country');       
        $country_array=array();
        if(count($country['data'])>0){        
            $country_array['']= "Please Select Country";    
            foreach ($country['data'] as $key => $value) {
                $country_array[$value['mc_country_id']]=$value['country'];
            }
        }   
        $countrydropdown=form_dropdown('mba_country_id', $country_array,$iCountryId,'size="1" class="form-control" id="mba_country_id_dd"');        
        $this->smarty->assign('countrydropdown',$countrydropdown);
    }



    function getState($iCountryId=0,$iStateId=0){        
        $params=array();
        if($iCountryId>0)
        $params['countryid']=$iCountryId;

        $state     = $this->hbmodel->getData('get_country_state',$params);
        $state_array=array();
        if(count($state['data'])>0){            
            $state_array[]='Please Select State';
            foreach ($state['data'] as $key => $value) {
                $state_array[$value['state_id']]=$value['state'];
            }
        }else{
            $state_array[]='No State found';
        }   
        $statedropdown=form_dropdown('mba_state_id', $state_array,$iStateId,'size="1" class="form-control" id="mba_state_id_dd"');        
        if($_REQUEST['ajax']==1){
            echo $statedropdown;
            exit;
        }
        $this->smarty->assign('statedropdown',$statedropdown);
    }


    function getCity($iStateId=0,$iCityId=0){
        $params=array();
        if($iStateId>0)
        $params['state_id']=$iStateId;

        $city     = $this->hbmodel->getData('get_state_city',$params);
        $city_array=array();
        if(count($city['data'])>0){            
            $city_array[]='Please Select City';
            foreach ($city['data'] as $key => $value) {
                $city_array[$value['cityid']]=$value['city'];
            }
        }   

        $citydropdown=form_dropdown('mba_city_id', $city_array,$iCityId,'size="1" class="form-control" id="mba_city_id_dd"');        
        if($_REQUEST['ajax']==1){
            echo $citydropdown;
            exit;
        }
        $this->smarty->assign('citydropdown',$citydropdown);
    }
}
