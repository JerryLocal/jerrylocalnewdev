<?php

/**
 * Description of Career Controller
 *
 * @module Career
 *
 * @class cart.php
 *
 * @path application\front\content\controllers\cart.php
 *
 * @author Dhananjay singh
 *
 * @date 20.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Thankyou extends HB_Controller
{
    
    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */  
    public function __construct() {
        parent::__construct();       
    }
    
    public function index() {
        $this->loadView('thankyou');
    }
}