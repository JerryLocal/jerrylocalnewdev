<?php

/**
 * Description of User Model
 * 
 * @module User
 * 
 * @class Model_user.php
 * 
 * @path application\front\user\models\Model_user.php
 * 
 * @author Steve Smith
 * 
 * @date 03.06.2015
 */
class Model_user extends CI_Model {

    private $primary_key;
    private $main_table;
    private $table_alias;
    public $errorCode;
    public $errorMessage;

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->main_table = "mod_customer";
        $this->table_alias = "mc";
        $this->load->helper('date');
        $this->primary_key = "iCustomerId";
    }

    function getUserValidation($usertype = '', $user_arr = array()) {
        if ($usertype != '' && isset($user_arr[$usertype]) && trim($user_arr[$usertype]) != '') {
            $value = trim($user_arr[$usertype]);
            $this->db->select($usertype);
            $this->db->from($this->main_table);
            $this->db->where($usertype, $value);
            if (isset($user_arr['userId'])) {
                $this->db->where($this->primary_key . " <>", $user_arr['userId']);
            }
            $user_obj = $this->db->get();
            $user_data = is_object($user_obj) ? $user_obj->result_array() : array();
            if (is_array($user_data) && count($user_data) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        } else {
            return 'false';
        }
    }

    /**
     * insert method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    function insert($data = array()) {

        $this->db->insert($this->main_table, $data);
        $insertId = $this->db->insert_id();
        return $insertId;
    }

    /**
     * update method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @return boolean $res returns TRUE or FALSE.
     */
    function update($data = array(), $where) {
        if (is_numeric($where)) {
            $this->db->where($this->primary_key, $where);
        } else if ($where) {
            $this->db->where($where, FALSE, FALSE);
        } else {
            return false;
        }
        $res = $this->db->update($this->main_table, $data);
        return $res;
    }

    function confirm($email = '') {
        $confirm = array();
        $this->db->select('*');
        $this->db->where('vEmail', $email);
        $result_obj = $this->db->get($this->main_table);
        $result = is_object($result_obj) ? $result_obj->result_array() : array();
        if (count($result) > 0) {
            if ($result[0]['eStatus'] == "Active") {
                $this->session->set_flashdata('success', "Your are already active");
                $this->authenticate($result[0]['vUserName'], $result[0]['vPassword']);
                $confirm['confirmCode'] = 2;
                $confirm['confirmMessage'] = 'Your account already activated.';
            } else {
                $data = array(
                    "eStatus" => "active"
                );
                $this->db->where('vEmail', $email);
                $this->db->update($this->main_table, $data);
                $this->authenticate($result[0]['vUserName'], $result[0]['vPassword']);
                $confirm['confirmCode'] = 1;
                $confirm['confirmMessage'] = 'Your account activated successfully.';
            }
        } else {

            $confirm['confirmCode'] = 0;
            $confirm['confirmMessage'] = 'Sorry activation link is expired.';
        }

        return $confirm;
    }

    /**
     * getData method is used to get data records for user.
     * @param string $extra_cond $email is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @return array $data_arr returns data records array.
     */
    public function getData($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "") {        
        if (is_array($fields)) {
            $this->db->select($fields);
        } else if ($fields != "") {
            $this->db->select($fields);
        } else {
            $this->db->select("*");
        }
        $this->db->from($this->main_table . " AS " . $this->table_alias);

        if (is_array($extra_cond) && count($extra_cond) > 0) {
            foreach ($extra_cond as $key => $val) {
                $this->db->where($val['field'], $val['value']);
            }
        } else if (is_numeric($extra_cond)) {
            $this->db->where($this->table_alias . "." . $this->primary_key, intval($extra_cond));
        } else if ($extra_cond) {
            $this->db->where($extra_cond, FALSE, FALSE);
        }

        $this->general->getPhysicalRecordWhere($this->main_table, $this->table_alias, "AR");
        if ($group_by != "") {
            $this->db->group_by($group_by);
        }
        if ($order_by != "") {
            $this->db->order_by($order_by);
        }
        if ($limit != "") {
            if (is_numeric($limit)) {
                $this->db->limit($limit);
            } else {
                list($offset, $limit) = @explode(",", $limit);
                $this->db->limit($offset, $limit);
            }
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }

    /**
     * authenticate method is used to authenticate user.
     * @param string $user_name user_name is name passed for authentification.
     * @param string $password password is name passed for authentification.
     * @return array $data_arr returns success/failre status and related message array.
     */
    function authenticate($user_name = '', $password) {
        $this->db->select('iCustomerId,vFirstName,vLastName,vEmail,vUserName,eStatus');
        $this->db->from($this->main_table);

        $where = "(`vUserName` = '" . $user_name . "' OR `vEmail` = '" . $user_name . "')";
        $this->db->where($where, NULL, FALSE);
        $this->db->where('vPassword', $password);

        $result = $this->db->get();
        $record = is_object($result) ? $result->result_array() : array();
        
        if (is_array($record) && count($record) > 0) {
            if ($record[0]['eStatus'] == 'Inactive') {
                $this->errorCode = 2;
                $this->errorMessage = 'You have not activated your account.';
            } else {
                $this->_id = $record[0]["iCustomerId"];
                $this->session->set_userdata("iUserId", $record[0]["iCustomerId"]);
                $this->session->set_userdata("vFirstName", $record[0]["vFirstName"]);
                $this->session->set_userdata("vLastName", $record[0]["vLastName"]);
                $this->session->set_userdata("vEmail", $record[0]["vEmail"]);
                $this->session->set_userdata("vUserName", $record[0]["vUserName"]);
                $this->session->set_userdata("eStatus", $record[0]["eStatus"]);
                $this->session->set_userdata("eLoginType", "Member");

                $sessionLoginData['iUserId'] = $record[0]['iCustomerId'];
                $sessionLoginData['eUserType'] = 'Member';
                $sessionLoginData['vIP'] = $this->input->ip_address();
                $sessionLoginData['dLoginDate'] = date('Y-m-d H:i:s', now());

                $this->load->model('tools/loghistory');
                $iLogId = $this->loghistory->insert($sessionLoginData);

                $this->session->set_userdata("iLogId", $iLogId);
                $this->errorCode = 1;
            }
        } else {
            $this->errorCode = 0;
            $this->errorMessage = 'Incorrect username/email or password. Please try again.';
        }

        $data_arr['errorCode'] = $this->errorCode;
        $data_arr['errorMessage'] = $this->errorMessage;

        return $data_arr;
    }

    function insertAddress($data = array()) {
        $this->db->select('iAdminId');
        $this->db->from('mst_buyer_address');
        $this->db->where('iAdminId',$data['iAdminId']);
        $result = $this->db->get()->result_array();   

        if(count($result) > 0 && !empty($result)){    
            $this->db->where('iAdminId',$data['iAdminId']);      
            $this->db->update('mst_buyer_address', $data);            
            return $data['iAdminId'];
        }else{
            $this->db->insert('mst_buyer_address', $data);
            $insertId = $this->db->insert_id();   
            return $insertId; 
        }        
        
    }

    function facebook_login($user_name) {
        $this->db->select('iCustomerId,vFirstName,vLastName,vEmail,vUserName,eStatus');
        $this->db->from($this->main_table);

        $where = "(`vUserName` = '" . $user_name . "' OR `vEmail` = '" . $user_name . "')";
        $this->db->where($where, NULL, FALSE);

        $result = $this->db->get();
        $record = is_object($result) ? $result->result_array() : array();

        if (is_array($record) && count($record) > 0) {
            if ($record[0]['eStatus'] == 'Inactive') {
                $this->errorCode = 2;
                $this->errorMessage = 'You have not activated your account.';
            } else {
                $this->_id = $record[0]["iCustomerId"];
                $this->session->set_userdata("iUserId", $record[0]["iCustomerId"]);
                $this->session->set_userdata("vFirstName", $record[0]["vFirstName"]);
                $this->session->set_userdata("vLastName", $record[0]["vLastName"]);
                $this->session->set_userdata("vEmail", $record[0]["vEmail"]);
                $this->session->set_userdata("vUserName", $record[0]["vUserName"]);
                $this->session->set_userdata("eStatus", $record[0]["eStatus"]);
                $this->session->set_userdata("eLoginType", "Member");

                $sessionLoginData['iUserId'] = $record[0]['iCustomerId'];
                $sessionLoginData['eUserType'] = 'Member';
                $sessionLoginData['vIP'] = $this->input->ip_address();
                $sessionLoginData['dLoginDate'] = date('Y-m-d H:i:s', now());

                $this->load->model('tools/loghistory');
                $iLogId = $this->loghistory->insert($sessionLoginData);

                $this->session->set_userdata("iLogId", $iLogId);
                $this->errorCode = 1;
            }
        } else {
            $this->errorCode = 0;
            $this->errorMessage = 'Incorrect username/email or password. Please try again.';
        }

        $data_arr['errorCode'] = $this->errorCode;
        $data_arr['errorMessage'] = $this->errorMessage;
        
        return $data_arr;
    }

    function googleplus_login($user_name) {
        $this->db->select('iCustomerId,vFirstName,vLastName,vEmail,vUserName,eStatus');
        $this->db->from($this->main_table);

        $where = "(`vUserName` = '" . $user_name . "' OR `vEmail` = '" . $user_name . "')";
        $this->db->where($where, NULL, FALSE);

        $result = $this->db->get();
        $record = is_object($result) ? $result->result_array() : array();

        if (is_array($record) && count($record) > 0) {
            if ($record[0]['eStatus'] == 'Inactive') {
                $this->errorCode = 2;
                $this->errorMessage = 'You have not activated your account.';
            } else {
                $this->_id = $record[0]["iCustomerId"];
                $this->session->set_userdata("iUserId", $record[0]["iCustomerId"]);
                $this->session->set_userdata("vFirstName", $record[0]["vFirstName"]);
                $this->session->set_userdata("vLastName", $record[0]["vLastName"]);
                $this->session->set_userdata("vEmail", $record[0]["vEmail"]);
                $this->session->set_userdata("vUserName", $record[0]["vUserName"]);
                $this->session->set_userdata("eStatus", $record[0]["eStatus"]);
                $this->session->set_userdata("eLoginType", "Member");

                $sessionLoginData['iUserId'] = $record[0]['iCustomerId'];
                $sessionLoginData['eUserType'] = 'Member';
                $sessionLoginData['vIP'] = $this->input->ip_address();
                $sessionLoginData['dLoginDate'] = date('Y-m-d H:i:s', now());

                $this->load->model('tools/loghistory');
                $iLogId = $this->loghistory->insert($sessionLoginData);

                $this->session->set_userdata("iLogId", $iLogId);
                $this->errorCode = 1;
            }
        } else {
            $this->errorCode = 0;
            $this->errorMessage = 'Incorrect username/email or password. Please try again.';
        }

        $data_arr['errorCode'] = $this->errorCode;
        $data_arr['errorMessage'] = $this->errorMessage;
        
        return $data_arr;
    }
    
    
    function update_newsletter_subscription($data = array(), $where) {
        if (is_numeric($where)) {
            $this->db->where($this->primary_key, $where);
        } else if ($where) {
            $this->db->where($where, null, FALSE);
        } else {
            return false;
        }
        $res = $this->db->update('mst_newsletter_subscription', $data);
        return $res;
    }

    public function getNewsletterSubscriptionDetails($extra_cond = "", $fields = "*") {
        $this->db->select($fields, false);
        $this->db->from("mst_newsletter_subscription");
        $this->db->where($extra_cond, null, false);
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        return $data_arr;
    }

}
