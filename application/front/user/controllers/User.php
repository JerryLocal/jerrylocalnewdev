<?php

/**
 * Description of User Controller
 * 
 * @module User
 * 
 * @class User.php
 * 
 * @path application\front\user\controllers\User.php
 * 
 * @author Stive Smith
 * 
 * @date 03.06.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');
        $this->load->model('model_user');
    }

    /**
     * index method is used to define Home page content.
     */
    public function index() {
        $view_file = "welcome_message";
        $this->loadView($view_file);
    }

    /**
     * dashboard method is used to define dashboard data after user logged in.
     */
    public function dashboard() {
        
    }

    /**
     * login method is used to display login page.
     */
    public function login() {
        if ($this->session->userdata('iUserId')) {
            // redirect($this->config->item("site_url") . 'dashboard.html');
            redirect($this->config->item("site_url"));
        }
        if (isset($_GET['ref']) && !empty($_GET['ref'])) {
            $this->smarty->assign('ref', $_GET['ref']);
        }
        $refr = $this->session->userdata('ref');
        if (isset($refr) && !empty($refr)) {
            $this->smarty->assign('ref', $refr);
            $this->session->unset_userdata('ref');
        }
        $view_file = "login";
        $this->loadView($view_file);
    }

    /**
     * login_action method is used to process login page for authentification.
     */
    public function login_action() {
        // $user = $this->input->get_post('User');
        $postArr = $this->input->post();
        $vEmail = trim($postArr['vEmail']);
        if (!isset($postArr['Password'])) {
            $password = trim($postArr['password']);
        } else {
            $password = trim($postArr['Password']);
        }

        // Below code for the password Encryption - start
        $input_params['password'] = $password;
        $password = $this->general->encrypt($input_params);
        // Above code for the password Encryption - end
        // $stay_signed = $this->input->get_post('remember_me');
        // pr($user);exit();
        $user_name = $vEmail; #$user['vUserName'];
        $password = $password; #$user['vPassword'];

        $cookiearr = $this->cookie->read('userarray');

        if (is_array($cookiearr) && $cookiearr['username'] != '') {
            $user_name = $cookiearr['username'];
            $password = $cookiearr['password'];
        }
        $stay_signed = $this->input->get_post('remember_me');
        $ajaxcall = $this->input->get_post('ajaxcall');

        $identity = $this->model_user->authenticate($user_name, $password);
        if ($identity['errorCode'] == 1) {

            $sess_prefix = $this->config->item("sess_prefix");
            if ($stay_signed == 'Yes') {
                $cookiedata = array(
                    $sess_prefix . 'username' => $user_name,
                    $sess_prefix . 'password' => $password
                );
                $this->cookie->write('userarray', $cookiedata);
            } else {
                $cookiedata = array(
                    $sess_prefix . 'username' => '',
                    $sess_prefix . 'password' => ''
                );
                $this->cookie->write('userarray', $cookiedata);
            }
            $this->session->set_flashdata('success', 'Logged in successfully');
            $this->smarty->assign('alldata', $this->session->all_userdata());
            $url = base64_decode($_POST['ref']);

            if (isset($url) && !empty($url)) {
                redirect($url);
            } else {
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            if (!empty($_POST)) {
                $this->session->set_userdata("ref", $_POST['ref']);
            }
            $this->session->set_flashdata('failure', $identity['errorMessage']);
            redirect($this->config->item("site_url") . 'login.html');
            // redirect($this->config->item("site_url"));
        }
    }

    /**
     * logout method is used to log out the current login user.
     */
    public function logout() {
        $this->load->model('tools/loghistory');
        $iLogId = $this->session->userdata('iLogId');
        $this->loghistory->updateLogoutUser($iLogId);
        $sess_prefix = $this->config->item("sess_prefix");
        $cookiedata = array(
            $sess_prefix . 'username' => '',
            $sess_prefix . 'password' => ''
        );
        $this->cookie->write('userarray', $cookiedata);
        /* $user_data = $this->session->all_userdata();
          foreach ($user_data as $key => $value) {
          if ($key != 'cart_contents') {
          $this->session->unset_userdata($key);
          }
          } */
        $this->session->sess_destroy();
        redirect($this->config->item("site_url"));
    }

    /**
     * register method is used to display register page.
     */
    function register() {
        if ($this->session->userdata('iUserId')) {
            // redirect($this->config->item("site_url") . 'dashboard.html');
            redirect($this->config->item("site_url"));
        }
        $data['heading'] = "Register";
        $data['type'] = "register";
        $data['user'] = array('firstname' => '', 'lastname' => '', 'email' => '');
        $this->loadView('register', $data);
    }

    /**
     * register_action method is used to process register page for adding customer record.
     */
    function register_action() {
        $postArr = $this->input->get_post('User');
        $user_arr = array();
        foreach ($postArr as $key => $val) {
            $user_arr[$key] = $val;
        }
        $user_arr['dtRegisteredDate'] = date('Y-m-d H:i:s');
        $user_id = $this->model_user->insert($user_arr);

        if (!$user_id) {
            $this->session->set_flashdata('failure', "Error occured during registering your profile.");
        } else {
            $this->session->set_flashdata('success', "You have successfully registered.");
        }

        redirect($this->config->item("site_url") . 'index.html');
    }

    function signup_action() {
        $postArr = $this->input->post();
        $fname = trim($postArr['fname']);
        $lname = trim($postArr['lname']);
        $vEmail = trim($postArr['vEmail']);
        $password = trim($postArr['password']);

        // Below code for the password Encryption - start
        $input_params['password'] = $password;
        $password = $this->general->encrypt($input_params);
        // Above code for the password Encryption - End
        // pr($postArr);exit();
        if (!empty($postArr)) {
            if (count($postArr) > 0) {
                $params = array();
                $params['firstname'] = $fname;
                $params['lastname'] = $lname;
                $params['email'] = $vEmail;
                $params['password'] = $password;
                $ws_name = 'signup';
                $signup_arr = $this->hbmodel->getData($ws_name, $params);
                $signup_details = $merchant_details_arr['data'];
                $insert_id = $signup_arr['data'][0]['insert_id'];
                $msg = $signup_arr['settings']['message'];
                if ($insert_id > 0) {
                    // $this->smarty->assign('signup_arr',$signup_arr);
                    $this->session->set_flashdata('success', $msg);
                    // Below function for the login autometically after signup
                    $this->login_action();
                } else {
                    $this->session->set_flashdata('failure', $msg);
                }
                redirect($this->config->item('site_url'));
            } else {
                $this->session->set_flashdata('failure', 'Error occured during registering your profile.');
                redirect($this->config->item('site_url') . 'login.html');
            }
        } else {
            $this->session->set_flashdata('failure', 'Error occured during registering your profile.');
            redirect($this->config->item('site_url') . 'login.html');
        }
    }

    /**
     * check_user_email method is used to check wether username or email already exist in data base.
     */
    function check_user_email() {
        $user_arr = $this->input->get_post('User');

        if (isset($user_arr["vEmail"])) {
            echo $this->model_user->getUserValidation('vEmail', $user_arr);
            exit;
        }
        if (isset($user_arr["vUserName"])) {
            echo $this->model_user->getUserValidation('vUserName', $user_arr);
            exit;
        }
        exit;
    }

    /**
     * check_customer_email method is used to check wether email already exist in data base.
     */
    function check_customer_email() {
        $user_arr = $this->input->post();
        if (isset($user_arr["vEmail"])) {
            // Last argument for return string or boolean
            echo $this->generalfront->checkemailExist('vEmail', $user_arr["vEmail"], "mod_customer", true);
            exit;
        }
        exit;
    }

    /**
     * check_customer_email method is used to check wether email already exist in data base.
     */
    public function check_customer_email_for_forgotpassword() {
        $user_arr = $this->input->post();
        if (isset($user_arr["email"])) {
            // Last argument for return string or boolean
            $result = $this->generalfront->checkemailExist('vEmail', $user_arr["email"], "mod_customer");
            if ($result) {
                echo "false";
            } else {
                echo "true";
            }
        }
        exit;
    }

    /**
     * forgotpassword method is used to display forgot password page.
     */
    public function forgotpassword() {
        $view_file = "forgotpassword";
        $this->loadView($view_file);
    }

    /**
     * forgotpassword_action method is used to send forgot password action.
     */
    function forgotpassword_action() {
        // $user_arr = $this->input->get_post('User');
        $user_arr = $this->input->post();
        // $user_name = addslashes($user_arr['vUserName']);
        $user_name = addslashes($user_arr['email']);

        $where = "(" . $this->db->protect("vUserName") . " = " . $this->db->escape($user_name) . " OR " . $this->db->protect("vEmail") . " = " . $this->db->escape($user_name) . ")";
        $user_details = $this->model_user->getData($where);
        $user_details[0]['vName'] = $user_details[0]['vFirstName']." ".$user_details[0]['vLastName'];

        // Below code for the password Decryption - start
        $user_details[0]['vPassword'] = $this->general->decrypt($user_details[0]['vPassword']);
        // Above code for the password Decryption - end

        $this->load->model('tools/emailer');
        $success = 0;
        if (is_array($user_details) && count($user_details) > 0) {
            $success = $this->emailer->send_mail($user_details[0], 'FRONT_FORGOT_PASSWORD');
        } else {
            $success = 2;
        }

        if ($success == 1) {
            $this->session->set_flashdata('success', "Forgot password email sent sucessfully. Please check your email.");
        } else if ($success == 2) {
            $this->session->set_flashdata('failure', "We are unable to find your username or email. Please try again.");
        } else {
            $this->session->set_flashdata('failure', "Error in sending mail. Please contact adminstrator.");
        }
        // redirect($this->config->item("site_url") . 'forgot-password.html');
        redirect($this->config->item("site_url"));
    }

    /**
     * profile method is used to display and  update customer page.
     */
    public function profile() {
        $userId = $this->session->userdata('iUserId');
        if (!$userId) {
            $this->session->set_flashdata('failure', "Please log in first to update profile.");
            redirect($this->config->item('site_url') . 'login.html');
        } else {
            if ($this->input->post()) {
                $postArr = $this->input->get_post();
                $fname = trim($postArr['fname']);
                $lname = trim($postArr['lname']);
                $vEmail = trim($postArr['vEmail']);
                $vPhoneNo = $postArr['vPhoneNo'];
                // $vPhoneNo = $postArr['vPhoneNo_hidden'];
                $dDOB = $postArr['DOBYear'] . '-' . $postArr['DOBMonth'] . '-' . $postArr['DOBDay'];
                $eGender = $postArr['eGender'];
                $vBankName = trim($postArr['vBankName']);
                $iBankAccountNumber = $postArr['acnt1'] . '-' . $postArr['acnt2'] . '-' . $postArr['acnt3'] . '-' . $postArr['acnt4'];
                $chkEmail = $this->model_user->getUserValidation('vEmail', $postArr);
                $user_arr = array();
                if ($chkEmail == "true") {
                    $user_arr['vEmail'] = $vEmail;
                    $user_arr['vUserName'] = $vEmail;
                }/* else{
                  $this->session->set_flashdata('failure', "Email Already Exist");
                  } */
                $user_arr['vFirstName'] = $fname;
                $user_arr['vLastName'] = $lname;
                $user_arr['vPhoneNo'] = $vPhoneNo;
                $user_arr['dDOB'] = $dDOB;
                $user_arr['eGender'] = $eGender;
                $user_arr['vBankName'] = $vBankName;
                $user_arr['iBankAccountNumber'] = $iBankAccountNumber;
                /* $user_arr = array('vFirstName'=>$fname,
                  'vLastName'=>$lname,
                  'vEmail'=>$vEmail,
                  'vUserName'=>$vEmail,
                  'vPhoneNo'=>$vPhoneNo,
                  'dDOB'=>$dDOB,
                  'eGender'=>$eGender,
                  'vBankName'=>$vBankName,
                  'iBankAccountNumber'=>$iBankAccountNumber
                  ); */

                // foreach ($postArr as $key => $val) {
                //     $user_arr[$key] = ($val);
                // }
                $isUpdated = $this->model_user->update($user_arr, $this->input->post('iUserId'));

                if (!$isUpdated) {
                    $this->session->set_flashdata('failure', "Error occured during updating user profile.");
                } else {
                    $this->session->set_flashdata('success', "User profile updated successfully.");
                }
                redirect($this->config->item("site_url") . 'profile.html');
            }
            $where = $this->db->protect("iCustomerId") . " = " . $this->db->escape($userId);
            $user = $this->model_user->getData($where);
            if (empty($user)) {
                $this->session->set_flashdata('failure', "User profile not found.");
                redirect($this->config->item("site_url") . 'logout.html');
            }
            $data['user'] = array(
                'id' => $userId,
                'firstname' => $user[0]['vFirstName'],
                'lastname' => $user[0]['vLastName'],
                'email' => $user[0]['vEmail'],
                'username' => $user[0]['vUserName'],
                'phoneno' => $user[0]['vPhoneNo'],
                'dob' => $user[0]['dDOB'],
                'gender' => $user[0]['eGender'],
                'bankname' => $user[0]['vBankName'],
                'bankaccountno' => $user[0]['iBankAccountNumber']
            );
            $data['heading'] = "User Profile";
            $data['type'] = "profile";
            $this->smarty->assign('user', $data['user']);
        }
        // $this->loadView("register", $data);
    }

    function change_password() {
        $this->generalfront->redirectLogin(true);
    }

    function change_password_action() {
        $userArr = $this->input->post();
        $iUserId = $this->session->userdata('iUserId');
        $eLoginType = $this->session->userdata('eLoginType');
        $condition = '';
        if ($eLoginType == 'Member') {
            $condition = "iCustomerId = $iUserId";
        }
        $db_user = $this->model_user->getData($condition, "vPassword");

        $dbPassword = $db_user[0]['vPassword'];
        // Below code for the password Decryption - start
        $dbPassword = $this->general->decrypt($dbPassword);
        // Above code for the password Decryption - end

        $oldPassword = trim($userArr['old_psw']);
        $vPassword = trim($userArr['new_psw']);
        $vConfirmPassword = trim($userArr['confirm_new_psw']);
        $input_params['password'] = $vPassword;

        //check for old password validation
        if ($oldPassword != '' && $dbPassword != '') {

            // CHECKING DB PASSWORD AND OLD PASSWORD

            if ($oldPassword == $dbPassword) {

                // CHECKING NEW PASSWORD AND CONFIRM PASSWORD
                if ($vPassword == $vConfirmPassword) {

                    // Below code for the password Encryption - start
                    $input_params['password'] = $vPassword;
                    $data['vPassword'] = $this->general->encrypt($input_params);
                    // Above code for the password Encryption - End
                    //$data['vConfirmPassword'] = $vPassword;
                    $this->model_user->update($data, $condition);
                    $message_tmp = "Password updated successfully";
                    $this->session->set_flashdata('success', $message_tmp);
                    $url = 'change-password.html';
                } else {
                    //$message_tmp = "Error in updating password.";
                    $message_tmp = "Please enter same value for password & confirm password";
                    $this->session->set_flashdata('failure', $message_tmp);
                    $url = 'change-password.html';
                }
            } else {
                //$message_tmp = "Old password is incorrect.";
                $message_tmp = "Please enter correct old password";
                $this->session->set_flashdata('failure', $message_tmp);
                $url = 'change-password.html';
            }
        } else {
            //$message_tmp = "User details not found.";
            $message_tmp = "Please enter correct old password";
            $this->session->set_flashdata('failure', $message_tmp);
            $url = 'change-password.html';
        }
        redirect($this->config->item('site_url') . $url);
    }

    function my_address() {
        $postArr = $this->input->post();
        $this->generalfront->assignCountryDropDown();
        $this->generalfront->assignStateDataDropDown();
        $this->generalfront->assignCityDataDropDown();
        $url = 'my-address.html';
        $this->generalfront->redirectLogin(true);
        $address = $this->generalfront->getRowData('iAdminId', $this->session->userdata('iUserId'), 'mst_buyer_address');
        $this->smarty->assign('address', $address);
        if (!empty($postArr) && count($postArr) > 0) {
            if (count($postArr) > 0) {
                $iUserId = $this->session->userdata('iUserId');
                if ($iUserId > 0) {
                    $UserName = $this->session->userdata('vFirstName') . " " . $this->session->userdata('vLastName');
                    $data['iAdminId'] = $iUserId;
                    $data['vName'] = $UserName;
                    $data['vAddress1'] = $postArr['vStreet'];
                    $data['vAddress2'] = '';
                    $data['vArea'] = $postArr['vArea'];
                    $data['iCountryId'] = $postArr['vCountry'];
                    $data['iStateId'] = $postArr['vRegion'];
                    $data['iCityId'] = $postArr['vCity'];
                    $data['vPinCode'] = $postArr['iPincode'];
                    $data['vPhone'] = $postArr['vPhoneNo'];
                    $insert_id = $this->model_user->insertAddress($data);
                    if ($insert_id > 0) {
                        $this->session->set_flashdata('success', 'Address Updated successfully.');
                        // $url = '';
                    } else {
                        $this->session->set_flashdata('failure', 'Problem while adding address.');
                        $url = '';
                    }
                } else {
                    $this->session->set_flashdata('failure', 'Please Login and Try again.');
                    $url = '';
                }
            } else {
                $this->session->set_flashdata('failure', 'Invalid Data.');
                // $url = '';
            }

            redirect($this->config->item('site_url') . $url);
        }
    }

    function facebookLogin() {
        $this->load->library('facebook'); // Automatically picks appId and secret from config
        // $this->facebook->destroySession();exit;
        $user = $this->facebook->getUser();

        if ($user) {
            try {
                $data['user_profile'] = $this->facebook->api('/me?fields=id,first_name,last_name,email,gender');
            } catch (FacebookApiException $e) {
                $user = null;
            }
        } else {
            // Solves first time login issue. (Issue: #10)
            //$this->facebook->destroySession();
        }

        if ($user) {
            $wsql = "vEmail='" . $data['user_profile']['email'] . "'";

            $existuser = $this->model_user->getData($wsql);
            // Randon Passoword Generator - Start
            $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
            $password = '';
            for ($i = 0; $i < 6; $i++) {
                $password .= $characters[rand(0, strlen($characters))];
            }

            // Below code for thge password Encryption - start
            $input_params['password'] = $password;
            $password = $this->general->encrypt($input_params);
            // Above code for thge password Encryption - End
            // Randon Passoword Generator - End
            if (empty($existuser)) {
                $insertData = array("vFirstName" => $data['user_profile']['first_name'], "vLastName" => $data['user_profile']['last_name'], "vEmail" => $data['user_profile']['email'], "vUserName" => $data['user_profile']['email'], "vPassword" => $password, "eGender" => $data['user_profile']['gender'], "dtRegisteredDate" => date('Y-m-d H:i:s'), "eStatus" => "Active");
                $user_id = $this->model_user->insert($insertData);
                $user_name = $data['user_profile']['email'];
                $data_arr = $this->model_user->facebook_login($user_name);
            } else {
                $user_name = $data['user_profile']['email'];
                $data_arr = $this->model_user->facebook_login($user_name);
            }
            if ($data_arr['errorCode'] == 1) {
                $this->session->set_flashdata('success', "You have successfully logged in with Facebook.");
                redirect($this->config->item("site_url") . 'index.html');
            }
        } else {
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('user/facebookLogin'),
                'scope' => array("email") // permissions here
            ));
            redirect($data['login_url']);
        }
    }

    function googlePlusLogin() {

        $this->load->library('googleplus');
        if (isset($_GET['code'])) {
            $this->googleplus->client->authenticate($_GET['code']);
        }
        if ($this->googleplus->client->getAccessToken()) {
            $user = $this->googleplus->auth2->userinfo->get();

            $wsql = "vEmail='" . $user['email'] . "'";

            $existuser = $this->model_user->getData($wsql);
            // Randon Passoword Generator - Start
            $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
            $password = '';
            for ($i = 0; $i < 6; $i++) {
                $password .= $characters[rand(0, strlen($characters))];
            }

            // Below code for thge password Encryption - start
            $input_params['password'] = $password;
            $password = $this->general->encrypt($input_params);
            // Above code for thge password Encryption - End
            // Randon Passoword Generator - End
            if (empty($existuser)) {
                $insertData = array("vFirstName" => $user['given_name'], "vLastName" => $user['family_name'], "vEmail" => $user['email'], "vUserName" => $user['email'], "vPassword" => $password, "dtRegisteredDate" => date('Y-m-d H:i:s'), "eStatus" => "Active");
                $user_id = $this->model_user->insert($insertData);
                $user_name = $user['email'];
                $data_arr = $this->model_user->facebook_login($user_name);
            } else {
                $user_name = $user['email'];
                $data_arr = $this->model_user->googleplus_login($user_name);
            }
            if ($data_arr['errorCode'] == 1) {
                $this->session->set_flashdata('success', "You have successfully logged in with Facebook.");
                redirect($this->config->item("site_url") . 'index.html');
            }
            exit;
        } else {
            $authUrl = $this->googleplus->client->createAuthUrl();
            redirect($authUrl);
        }
    }

    function inviteFriend() {
        if (empty($_POST)) {
            $email = $this->session->userdata('vEmail');
            if (isset($email)) {
                $this->smarty->assign('user_email', $email);
            } else {
                $this->session->set_flashdata('failure', 'Please Login to Invite your friend');
                redirect($this->config->item('site_url'));
            }
        } else {
            $email = explode(',', $_POST['toEmail']);
            foreach ($email as $key => $value) {
                $params['Subject'] = $_POST['Message'];
                $params['FromEmail'] = $_POST['fromEmail'];
                $params['SiteUrl'] = $this->config->item('site_url');
                $params['ToEmail'] = $value;
                $ws_name = 'invite_friend';
                $result = $this->hbmodel->getData($ws_name, $params);
            }

            // $ws_name = 'invite_friend';
            // $result = $this->hbmodel->getData($ws_name, $params);
            if ($result['settings']['success'] == 1) {
                $this->session->set_flashdata('success', 'Invitation sent to your friend');
            } else {
                $this->session->set_flashdata('failure', 'Error while inviting to your Friends');
            }
            redirect($this->config->item('site_url') . "invite-friend.html");
        }
    }

    function notification() {

        $iUserId = $this->session->userdata('iUserId');
        // pr($iUserId);
        if (isset($iUserId) && !empty($iUserId)) {
            $getarr = $this->input->get();
            $curr_page = $getarr['page'];
            $params = array();
            $params['iUserId'] = $iUserId;
            $params['page_index'] = $curr_page;
            $ws_name = 'notification';
            $result = $this->hbmodel->getData($ws_name, $params);
            $notiArr = array();
            foreach ($result['data'] as $key => $value) {
                if ($value['tosh_order_status'] == 'Pending') {
                    $notiArr['data'][$key]['message'] = 'Your Order ' . $value['mso_sub_order_number_pre'] . $value['mso_sub_order_number'] . ', is successfully added in system';
                } elseif ($value['tosh_order_status'] == 'In-Process') {
                    $notiArr['data'][$key]['message'] = 'Your Order ' . $value['mso_sub_order_number_pre'] . $value['mso_sub_order_number'] . ', is in process';
                } elseif ($value['tosh_order_status'] == 'Shipped') {
                    $notiArr['data'][$key]['message'] = 'Your Order ' . $value['mso_sub_order_number_pre'] . $value['mso_sub_order_number'] . ', is shipped and you will get delivery soon';
                } elseif ($value['tosh_order_status'] == 'Delivered') {
                    $notiArr['data'][$key]['message'] = 'Your Order ' . $value['mso_sub_order_number_pre'] . $value['mso_sub_order_number'] . ', is delivered ';
                } elseif ($value['tosh_order_status'] == 'Cancel') {
                    $notiArr['data'][$key]['message'] = 'Your Order ' . $value['mso_sub_order_number_pre'] . $value['mso_sub_order_number'] . ', is cancelled ';
                }
                $notiArr['data'][$key]['extraMsg'] = $value['tosh_detail'];
                $notiArr['data'][$key]['days'] = $value['tosh_date_1'];
                $notiArr['data'][$key]['orderid'] = $value['tosh_mst_sub_order_id'];
                $notiArr['data'][$key]['orderid_new'] = $value['mso_sub_order_number_pre'] . $value['mso_sub_order_number'];
                $statusDT = $this->generalfront->getColumnValue('iMstSubOrderId', $value['tosh_mst_sub_order_id'], 'dDate', 'trn_order_status_history');
                $notiArr['data'][$key]['statusDT'] = date("d-m-Y", strtotime($statusDT));
            }
            // pr($notiArr,1);
            $notiArr['settings'] = $result['settings'];
            $this->smarty->assign('noti_arr', $notiArr);
        } else {
            $this->session->set_flashdata('failure', 'Please Login to see your notification');
            $ref = base64_encode($this->config->item('site_url') . $_SERVER['PATH_INFO']);
            redirect($this->config->item('site_url') . 'login.html?ref=' . $ref);
        }
    }

    function getStateDataOnCountry() {
        $this->load->library('dropdown');
        $postarr = $this->input->post();
        $params = array();
        $this->generalfront->assignStateDataDropDown($postarr['countryId']);
        $dropdown = $this->dropdown->display("vState", "vRegion", "onchange='return city(this.value);' title='Select Region' aria-chosen-valid='Yes' class='form-control chosen-select' data-placeholder='Select Region' ", "", "", "", "");
        $ret_arr['dropdown'] = $dropdown;
        echo json_encode($ret_arr);
        exit();
    }

    function getCityDataOnState() {
        $this->load->library('dropdown');
        $postarr = $this->input->post();
        $params = array();
        $this->generalfront->assignCityDataDropDown($postarr['stateId']);
        $dropdown = $this->dropdown->display("vCity", "vCity", "title='Select City' aria-chosen-valid='Yes' class='form-control chosen-select' data-placeholder='Select City' '", "", "", "", "");
        $ret_arr['dropdown'] = $dropdown;
        echo json_encode($ret_arr);
        exit();
    }

    // Below code to validate user while  login 
    function validateUserDetail() {
        $postArr = $this->input->post();
        $Passoword = trim($postArr['Password']);
        $vEmail = trim($postArr['vEmail']);
        // Below code for the password Encryption - start
        $input_params['password'] = $Passoword;
        $Passoword = $this->general->encrypt($input_params);
        // Above code for the password Encryption - End
        $where = "(" . $this->db->protect("vEmail") . " = " . $this->db->escape($vEmail) . " AND " . $this->db->protect("vPassword") . " = " . $this->db->escape($Passoword) . ")";
        $result = $this->model_user->getData($where, 'iCustomerId');
        if (!empty($result) && $result > 0) {
            echo "true";
        } else {
            echo "false";
        }
    }

    function unsubscribe_newsletter() {

        $unsubscribe_code = $this->input->get('e');
        if ($unsubscribe_code != '' && isset($unsubscribe_code)) {
            $field = "vEmail";
            $condition = "vUnsubscribeCode  = '" . $unsubscribe_code . "'";
            $subscriptionDetailsArr = $this->model_user->getNewsletterSubscriptionDetails($condition, $field);
            if (is_array($subscriptionDetailsArr) && count($subscriptionDetailsArr) > 0 && $subscriptionDetailsArr[0]['vEmail'] != '') {
                $data = array();
                $vEmail = $subscriptionDetailsArr[0]['vEmail'];
                $data['eStatus'] = "Unsubscribe";
                $condition = "vEmail  = '" . $vEmail . "'";
                $res = $this->model_user->update_newsletter_subscription($data, $condition);
                if ($res) {
                    $this->session->set_flashdata('success', "User unsubscribed successfully.");
                } else {
                    $this->session->set_flashdata('failure', "Failure occured while unsubscribing the user.");
                }
            } else {
                $this->session->set_flashdata('failure', "Failure occured while unsubscribing the user.");
            }
        } else {
            $this->session->set_flashdata('failure', "Failure occured while unsubscribing the user.");
        }
        redirect($this->config->item("site_url"));
    }

}
