<%$this->js->add_js("change_password.js")%>
<div id="change_pass"></div>
<div class="container">
  <div class="row-fluid">
    <div class="breadcrumbs">
      <ul>
          <li><a href="#">Home</a><span>|</span></li>
          <li><strong>Change Password</strong></li>
      </ul>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="product-list">      
      <%include file = "left.tpl"%>
      <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
        <div class="delivery-box">
          <h3>
            <div class="row">
              <div class="col-sm-9">CHANGE PASSWORD</div>
              <div class="col-sm-3"><span class="btn-back pull-right"></span>
              </div>
            </div>
          </h3>
          <div class="inner-form">
            <form class="form-horizontal" name="frmChangepassword" id="frmChangepassword" action="<%$this->config->item('site_url')%>user/user/change_password_action" autocomplete="off" method="post">
            <div class="form-group">
              <label for="inputName" class="col-sm-3 control-label">Old Password</label>
              <div class="col-sm-9">
                <input type="password" class="form-control max-width" name="old_psw" id="old_psw" placeholder="Old Password">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-sm-3 control-label">New Password</label>
              <div class="col-sm-9">
                <input type="password" class="form-control max-width" name="new_psw" id="new_psw" placeholder="New Password">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-sm-3 control-label">Confirm New Password</label>
              <div class="col-sm-9">
                <input type="password" class="form-control max-width" name="confirm_new_psw" id="confirm_new_psw" placeholder="Confirm New Password">
              </div>
            </div>


            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-default-site" name="save_chng_btn" id="save_chng_btn">Save Changes</button>
                <!-- <button type="button" class="btn btn-default-cancel" name="cancel_btn" id="cancel_btn">Cancel</button> -->
              </div>
            </div>
          </form>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
