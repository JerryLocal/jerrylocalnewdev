<%$this->js->add_js("invitefriend.js")%>
<div class="main-containt">
  <div class="container"><div class="row-fluid"><div class="breadcrumbs">
    <%$this->general->Breadcrumb('CLEAN','Invite Friend')%>
  </div></div></div>
  <div class=""> <!-- container -->
    <div class="row"><div class="product-list">
      <%include file = 'left.tpl'%>
     <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
      <div class="delivery-box">
        <h3>Invite Your Friends to JerryLocal via Email</h3>
        <div class="inner-form">
          <form id="invite_friend" class="form-horizontal" method="post" action="<%$this->config->item('site_url')%>user/user/inviteFriend">
            <div class="form-group">
              <label for="inputName" class="col-sm-3 control-label">From:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="fromEmail" id="inputname" value="<%$user_email%>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="inputArea" class="col-sm-3 control-label">To:</label>
              <div class="col-sm-9">
                <textarea class="form-control" id="inputEmail" name="toEmail" rows="3" placeholder="Enter comma separated values.."></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="inputArea" name="subject" class="col-sm-3 control-label">Message:</label>
              <div class="col-sm-9">
                <textarea class="form-control" id="inputComment" name="Message" rows="3"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <input type="button" id="send_friend" class="btn btn-default-site" value='Invite'>
                <!-- <button type="button" class="btn btn-default-cancel">Cancel</button> -->
              </div>
            </div>
          </form>
        </div></div>
      </div>
    </div></div>
  </div>
</div>
