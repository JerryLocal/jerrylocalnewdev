<%assign var=settings value=$noti_arr['settings']%>
<!-- < %assign var=per_page value=$settings['per_page']%> -->
<%assign var=per_page value=$this->config->item('REC_LIMIT_FRONT')%>
<%assign var=totalPage value= ceil($settings.count/$per_page)%>
<%assign var=tpages value= ceil($settings.count/$per_page)%>
<div class="main-containt">
  <div class="container">
    <div class="row-fluid">
      <div class="breadcrumbs">
        <%$this->general->Breadcrumb('CLEAN','Notification')%>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="product-list">
        <%include file = "left.tpl"%>
        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
          <div class="delivery-box">
            <h3 style="margin-bottom:0px;">Notifications</h3>
            <div class="noti-list">
             <%if $noti_arr['data']|@count gt 0%>
              <%foreach $noti_arr['data'] as $result%>
                 <table class="data-table notification-table">
                   <th colspan="2"> Posted before <strong><%$result['days']%> Days ago</strong>
                    <span style="float:right;"><%$result['statusDT']%></span></th> 
                  <tr>
                    <td width="15%" align="center"><%$result['orderid_new']%></td>
                    <td><p><i><%$result['message']%></i></p>
                        <%if $result['extraMsg'] neq ''%>
                      <p><b>Additional note : </b> <%$result['extraMsg']%></p>
                      <%/if%>
                    </td>
                  </tr>
                </table>
              <%/foreach%>
            <%else%>
            <div class="short-bar">
              <div class="col-md-12" style="text-align:center"> No Notification Available </div>
            </div>
            <%/if%>
          </div>
        </div>
        <%if $noti_arr['data']|@count gt 0%>
        <div class="pagination-box">
         <div class="col-md-7">
           <%assign var=displayResult value=($settings.curr_page*$per_page gt $settings.count) ? $settings.count : $settings.curr_page*$per_page%>
           <div class="row">
             <div class="pagination-list">Items <%(($settings.curr_page-1)*$per_page)+1%> to <%$displayResult%> of <%$settings.count%> total</div>
           </div>
         </div>
         <div class="col-md-5">
           <div class="row">
             <ul class="pagination">
               <%assign var=reload value=$this->config->item('site_url')|cat:"notifications.html?tpages="|cat:$tpages%>            
               <%if $totalPage gt 1%>
               <%$this->generalfront->paginate($reload,$noti_arr['settings'].curr_page,$totalPage)%>
               <%/if%>
             </ul>
           </div>
         </div>
       </div>
       <%/if%>
     </div>
   </div>
 </div></div>                         
</div>