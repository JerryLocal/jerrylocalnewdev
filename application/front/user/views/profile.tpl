<%$this->js->add_js("jquery-ui-1.9.2.min.js","profile.js")%>
<%$this->js->add_js("intlTelInput/intlTelInput.min.js")%>
<%$this->css->add_css("intlTelInput/intlTelInput.css")%>
<%$this->css->css_src()%>
<div id="profiles"></div>
<div class="container">
  <div class="row-fluid">
    <div class="breadcrumbs">
      <%$this->general->Breadcrumb('CLEAN','Personal Information')%>
    </div>
  </div>
</div>
<div class=""> <!-- container -->
  <div class="row">
    <div class="product-list">      
      <%include file = "left.tpl"%>
      <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
        <div class="delivery-box">
          <h3>
            <div class="row">
              <div class="col-sm-9">Personal Information</div>
              <!-- <div class="col-sm-3"><span class="btn-back pull-right"><a href="<%$this->config->item('site_url')%>contact.html">Contact Admin</a></span></div> -->
            </div>
          </h3>
          <div class="inner-form">
            <form class="form-horizontal" name="frmProfile" id="frmProfile" action="<%$this->config->item('site_url')%>profile.html" autocomplete="off" method="post">
              <input type="hidden" name="iUserId" id="iUserId" value="<%$user['id']%>"/>
              <div class="form-group">
                <label for="inputName" class="col-sm-3 control-label">First Name <span class="required">*</span></label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" id="fname" name="fname" value="<%$user['firstname']%>">
                </div>
              </div>
              <div class="form-group">
                <label for="inputArea" class="col-sm-3 control-label">Last Name <span class="required">*</span></label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" id="lname" name="lname" value="<%$user['lastname']%>">
                </div>
              </div>
              <div class="form-group">
                <label for="inputArea" class="col-sm-3 control-label">Contact Number</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" id="vPhoneNo" name="vPhoneNo" placeholder="Contact Number" maxlength="16" value="<%$user['phoneno']%>">
                  <!-- <input type="hidden" class="form-control" id="vPhoneNo_hidden" name="vPhoneNo_hidden" value=""> -->
                </div>
              </div>              
              <%assign var=date value=explode('-',$user.dob)%>              
              <div class="form-group">                
                <label for="inputArea" class="col-sm-3 control-label">Date of Birth</label>
                <div class="col-sm-6">
                  <div class="row">
                    <div class="col-xs-4">
                      <select class="form-control col-sm-2" name="DOBDay" id="DOBDay">
                        <option value=''>Day</option>
                        <%for $foo=1 to 31%>                          
                          <option value="<%$foo%>" <%($foo eq $date[2]) ? 'selected' : ''%>><%sprintf('%02d', $foo)%></option>
                        <%/for%>
                      </select>                      
                    </div>
                    
                    <div class="col-xs-4">
                      <select class="form-control col-sm-2" name="DOBMonth" id="DOBMonth">
                          <option value=''>Month</option>
                          <%for $foo=1 to 12%>
                              <option value="<%$foo%>" <%($foo eq $date[1]) ? 'selected' : ''%>><%sprintf('%02d', $foo)%></option>
                          <%/for%>
                      </select>                      
                    </div>                                          
                    <div class="col-xs-4">
                    <select class="form-control" name="DOBYear" id="DOBYear">
                    <option value=''>Year</option>
                        <%for $ct=date('Y') to 1950 step -1%>
                            <option value="<%$ct%>" <%($ct eq $date[0]) ? 'selected' : ''%>><%$ct%></option>
                        <%/for%>
                    </select>      
                    </div>
                  </div>
                  </div>
              </div>

              <div class="form-group">
                <label for="inputArea" class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-6">
                  <select class="form-control" style="width:160px;" id="eGender" name="eGender" value="<%$user['gender']%>">
                    <option value="Male" <%($user['gender'] eq 'Male') ? 'selected' : ''%>>Male</option>
                    <option value="Female" <%($user['gender'] eq 'Female') ? 'selected' : ''%>>Female</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputArea" class="col-sm-3 control-label">Email Address <span class="required">*</span></label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" id="vEmail" name="vEmail" value="<%$user['email']%>">
                </div>
              </div>
              <!-- <div class="form-group">
                <label for="inputArea" class="col-sm-3 control-label">Bank Account Name</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" id="vBankName" name="vBankName" value="<%$user['bankname']%>">
                </div>
              </div> -->
              <%assign var=acnt value=explode('-',$user['bankaccountno'])%>
              
            <!--   <div class="form-group">
                <label for="inputArea" class="col-sm-3 control-label">Bank Account Number</label>
                <div class="col-sm-6">
                  <div class="row">
                    <div class="col-sm-3">
                      <input type="text" class="form-control" name="acnt1" id="acnt1" value="<%$acnt[0]%>">
                    </div>
                    <div class="col-sm-3">
                      <input type="text" class="form-control" name="acnt2" id="acnt2" value="<%$acnt[1]%>">
                    </div>
                    <div class="col-sm-3">
                      <input type="text" class="form-control" name="acnt3" id="acnt3" value="<%$acnt[2]%>">
                    </div>
                    <div class="col-sm-3">
                      <input type="text" class="form-control" name="acnt4" id="acnt4" value="<%$acnt[3]%>">
                    </div>
                  </div>
                  <span id="acntErr"></span>
                </div>
              </div> -->
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                  <button type="submit" class="btn btn-default-site" id="saveChangesbtn">Save Changes</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
