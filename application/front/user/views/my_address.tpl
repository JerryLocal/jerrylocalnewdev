<%$this->js->add_js("address.js")%>
<%$this->js->add_js("intlTelInput/intlTelInput.min.js")%>
<%$this->css->add_css("intlTelInput/intlTelInput.css")%>
<%$this->css->css_src()%>
<div id="my_address"></div>
<div class="container">
  <div class="row-fluid">
    <%$this->general->Breadcrumb('CLEAN','My Address')%>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="product-list">      
      <%include file = "left.tpl"%>
      <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
        <div class="delivery-box">
          <h3>My Address</h3>
          <div class="inner-form">
            <form class="form-horizontal" name="frmAddress" id="frmAddress" action="<%$this->config->item('site_url')%>user/user/my_address" autocomplete="off" method="post">
              <div class="form-group">
                <label for="inputName" class="col-sm-3 control-label">Street No / Name <span class="required">*</span></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name ="vStreet" id="vStreet" placeholder="Street No / name" value="<%$address.vAddress1%>">
                </div>
              </div>
              <div class="form-group">
                <label for="inputArea" class="col-sm-3 control-label">Area Name <span class="required">*</span></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="vArea" id="vArea" placeholder="Area name" value="<%$address.vArea%>">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEmail" class="col-sm-3 control-label">Country <span class="required">*</span></label>
                <div class="col-sm-9">
                  <%$this->dropdown->display("vCountry","vCountry","class='form-control chosen-select'","|||Select Country","","<%$address.iCountryId%>","")%>                  
                </div>
              </div>

              <div class="form-group">
                <label for="inputEmail" class="col-sm-3 control-label">State/Region <span class="required">*</span></label>
                <div class="col-sm-9">                  
                  <%$this->dropdown->display("vState","vRegion","class='form-control chosen-select'","|||Select Region","","<%$address.iStateId%>","")%>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail" class="col-sm-3 control-label">City <span class="required">*</span></label>
                <div class="col-sm-9">
                  <%$this->dropdown->display("vCity","vCity","class='form-control chosen-select'","|||Select City","","<%$address.iCityId%>","")%>
                </div>
              </div>
              <div class="form-group">
                <label for="inputPincode" class="col-sm-3 control-label">Pin Code <span class="required">*</span></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="iPincode" id="iPincode" placeholder="Pin Code" value="<%$address.vPinCode%>">
                </div>
              </div>
              
              <div class="form-group">
                <label for="inputName" class="col-sm-3 control-label">Contact No. <span class="required">*</span></label>
                <div class="col-sm-9">
                  <input name="vPhoneNo_hidden" id='vPhoneNo_hidden' type='hidden' />
                  <input type="text" class="form-control" name="vPhoneNo" id="vPhoneNo" maxlength="16" value="<%$address.vPhone%>">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                  <button type="submit" class="btn btn-default-site" id="submitAddr">Submit</button>
                  <!-- <button type="button" class="btn btn-default-cancel">Cancel</button> -->
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
