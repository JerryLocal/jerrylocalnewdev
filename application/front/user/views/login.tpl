<%$this->js->add_js("signup.js")%>
<div id="signup"></div>
<div class="main-containt">
  <div class="container">
    <div class="row-fluid">
      <div class="breadcrumbs" style="padding: 0 0px;">
        <div class="container">
          <div class="row-fluid">
           <%$this->general->Breadcrumb('CLEAN','Sign In')%>
          </div>
        </div>
      </div>
      <div class="product-list common-back">
       <h3>Sign In</h3>
        <div class="container padding-20">
          <div class="row-fluid">
            <div class="col-sm-5">
              <form class="form-horizontal" name="frmSignin" id="frmSignin" action="<%$this->config->item('site_url')%>user/user/login_action" autocomplete="off" method="post">
                
                <input type="hidden" value="<%$ref%>" name="ref">
                <div class="form-group">
                  <input type="email" class="form-control dev_SR" id="vEmail" name="vEmail" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" id="Password" name="Password" placeholder="Password">
                </div>
                <div class="form-group">
                  <div class="checkbox-list-remember">
                    <input type="checkbox" id="remember_me" class="checkbox-list" name="remember_me" value="Yes">
                    <label for="remember_me">Remember me</label>
                  </div>
                  <a href="#" class="btn btn-link pad-w-0" data-dismiss="modal" data-toggle="modal" data-target="#myModal1">Forgot Password</a>
                </div>
                <div class="modal-footer">
                  <input type="submit" class="btn btn-primary btn-primary-j" id="signinbtn" name="signinbtn" value="Sign In">        
                  <button type="button" class="btn btn-default btn-default-j" data-dismiss="modal">Cancel</button>
                </div>
              </form>
            </div>
            <div class="col-sm-2 padd-0">
              <div class="or-txt">
                --Or--
              </div>
            </div>
            <div class="col-sm-5 padd-0">              
              <a href="<%$this->config->item('site_url')%>user/user/facebookLogin" class=""><img src="<%$this->config->item('images_url')%>f-plus.png" alt="facebook" class="img-responsive accordion"></a>
              <a href="<%$this->config->item('site_url')%>user/user/googlePlusLogin" ><img src="<%$this->config->item('images_url')%>gplus-icon.png" alt="Google" class="img-responsive accordion"></a>
              <a data-target="#myModal2" data-toggle="modal" data-dismiss="modal">
                  <img src="<%base_url("public/images/signup-as-email-icon.png")%>" class="img-responsive" alt="" style="cursor:pointer;"/>
              </a>
            </div>  
          </div>          
        </div>
      </div>
    </div>
  </div>
</div>
<!-- </div> -->