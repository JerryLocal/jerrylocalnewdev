<div class="main-containt">
  <div class="container"><div class="row"><div class="breadcrumbs">
    <ul>
      <li><a href="#">Home</a><span>|</span></li>
      <li><a href="#">My Account</a><span>|</span></li>
      <li><strong>Invite Friends</strong></li>
    </ul>
  </div></div></div>
  <div class="container">
    <div class="row"><div class="product-list">
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <div class="notifications-left">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">My Account</h3></div>
            <div class="panel-body">
              <h3 class="panel-title">ORDERS</h3>
              <ul class="order-list">
               <li><a href="#">My Orders <i class="fa fa-angle-right pull-right"></i></a></li>
               <li><a href="#">My Wishlist <i class="fa fa-angle-right pull-right"></i></a></li>
               <li><a href="#">Invite Friends <i class="fa fa-angle-right pull-right"></i></a></li>
             </ul>
             
             <h3 class="panel-title">SETTINGS</h3>
             <ul class="order-list">
               <li><a href="#">Personal Information <i class="fa fa-angle-right pull-right"></i></a></li>
               <li><a href="#">My Address <i class="fa fa-angle-right pull-right"></i></a></li>
               <li><a href="#">Change Password <i class="fa fa-angle-right pull-right"></i></a></li>
               <li><a href="#">Notifications <i class="fa fa-angle-right pull-right"></i></a></li>
             </ul>
           </div>
         </div>
       </div>
     </div>
     <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
      <div class="common-back">
        <h3>Invite Your Friends to Jerry via Email</h3>
        <div class="inner-form">
          <form class="form-horizontal">
            <div class="form-group">
              <label for="inputName" class="col-sm-3 control-label">From:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="inputname" value="<%$user_email%>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="inputArea" class="col-sm-3 control-label">To:</label>
              <div class="col-sm-9">
                <textarea class="form-control" id="inputComment" rows="3" placeholder="Enter comma separated values.."></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="inputArea" class="col-sm-3 control-label">Message:</label>
              <div class="col-sm-9">
                <textarea class="form-control" id="inputComment" rows="3"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-default-site">Invite</button>
                <button type="button" class="btn btn-default-cancel">Cancel</button>
              </div>
            </div>
          </form>
        </div></div>
      </div>
    </div></div>
  </div>
</div>
<footer>
  <div class="social-box">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="box-newletter">
              <div class="row">
                <div class="col-md-5">
                  <h2>get in touch with us</h2>
                </div>
                <div class="col-md-7">
                  <div class="social-box-icon"><a href="#"><span class="fa fa-facebook"></span></a> <a href="#"><span class="fa fa-google-plus"></span></a> <a href="#"><span class="fa fa-twitter"></span></a> <a href="#"><span class="fa fa-youtube"></span></a> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="box-newletter">
              <div class="col-md-5">
                <h2>join our mailing list</h2>
              </div>
              <div class="col-md-7">
                <div class="input-box">
                  <input type="text" title="Enter your email address" placeholder="Enter your email address" class="input-text required-entry validate-email">
                  <div class="actions pull-right">
                    <button type="submit" title="Subscribe" class="button"><span class="fa fa-angle-right"></span></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-link">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4">
                  <h3>Company</h3>
                  <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">News</a></li>
                    <li><a href="#">Sitemap</a></li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h3>policy info</h3>
                  <ul>
                    <li><a href="#">Privacy Notice</a></li>
                    <li><a href="#">Terms of Use</a></li>
                    <li><a href="#">Terms of Sale</a></li>
                    <li><a href="#">Copyright Policy</a></li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h3>need help?</h3>
                  <ul>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Contact Us</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-6">
              <h3>payment method</h3>
              <div class="qr-code"><a href="#"><span class="payment-cards discover">&nbsp;</span></a><a href="#"><span class="payment-cards paypal">&nbsp;</span></a><a href="#"><span class="payment-cards american">&nbsp;</span></a><a href="#"><span class="payment-cards maestro">&nbsp;</span></a><a href="#"><span class="payment-cards visa">&nbsp;</span></a></div>
            </div>
            <div class="col-md-6">
              <h3>QR CODE</h3>
              <div class="qr-code"><a href="#"><img src="images/qr-code.png" class="img-responsive" alt=""></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright">
    <div class="container">&copy; Copyright 2015 Jerry Local. All rights reserved.</div>
  </div>