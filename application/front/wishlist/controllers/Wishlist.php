<?php

/**
 * Description of Content Controller
 * 
 * @module Wishlist
 * 
 * @class Wishlist.php
 * 
 * @path application\front\wishlist\controllers\Wishlist.php
 * 
 * @author Jaydeep Jagani
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Wishlist extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');
    }

    /**
     * index method is used to initialize index function.
     */
    public function index() {
        
        $params = array();
        $iUserId = $this->session->userdata('iUserId');
        if(!$iUserId ){
               $this->session->set_flashdata('failure', "Please log in to see your wishlist");
               $ref = base64_encode($this->config->item('site_url').$_SERVER['PATH_INFO']);
               redirect($this->config->item('site_url') . 'login.html?ref='.$ref);
        }
        else{
         if($iUserId > 0){
                $params['iAdminID'] =$iUserId;
                $ws_name = 'get_wishlist';
                $wishlist_arr = $this->hbmodel->getData($ws_name, $params);
                $wishlist_detail = $merchant_details_arr['data'];
                $this->smarty->assign('wishlist_arr',$wishlist_arr);
            }
         }
    }

    function removeWishlist(){
        $params = array();
        $iUserId = $this->session->userdata('iUserId');
        $iProductId = $this->input->post('id');
        $params['iAdminId'] = $iUserId;
        $params['iProductId'] = $iProductId;
        $ws_name = 'del_wishlist';
        $del_arr = $this->hbmodel->getData($ws_name, $params);
        $this->generalfront->updateWishlistCount($iProductId,'-');
        $wishcount = $this->generalfront->getColumnValue('iMstProductsId',$iProductId,'iWishlistState','mst_products');
            $ret_arr = array('msg'=>$del_arr['settings']['success'],'count'=>$wishcount);
         echo json_encode($ret_arr);exit();

    }
    function addToWishlist(){
        $iUserId = $this->session->userdata('iUserId');
        if(!$iUserId ){
            echo $wishlist_arr['settings']['success'] = 2; exit();
        }
        else
        {
            $params = array();
            $iUserId = $this->session->userdata('iUserId');
            $iProductId = $this->input->post('id');
            $params['iAdminId'] = $iUserId;
            $params['iProductId'] = $iProductId;
            $ws_name = 'set_wishlist';
            $wishlist_arr = $this->hbmodel->getData($ws_name, $params);
            if($wishlist_arr['settings']['success'] == 1){
                $this->generalfront->updateWishlistCount($iProductId,'+');
            }
            $wishcount = $this->generalfront->getColumnValue('iMstProductsId',$iProductId,'iWishlistState','mst_products');
            $ret_arr = array('msg'=>$wishlist_arr['settings']['success'],'count'=>$wishcount);
            echo json_encode($ret_arr);exit();
        }
    }

    // Below function is to send mail to the friend about perticular product.
    function sendtofriend(){
        $pid = $this->uri->segment(1);
        $this->smarty->assign('productId',$pid);
        $this->load->library('captcha');
         $param = array(
             "type" => "png",
             "length" => 5,
             "height" => 37,
             "case" => false,
             "filters" => array(
                 "noise" => 0,
                 "blur" => 0
             ),
             "bgColor" => array(25, 51, 148),
             "textColor" => array(255, 255, 255)
         );
         $this->captcha->setParameter($param);
         $this->loadview('sendtofriend');
    }

    function sendMailToFriend(){
        $params['iProductId'] = $_POST['productId'];
        $ws_name = 'get_individual_product';
        $product_detail_arr = $this->hbmodel->getData($ws_name, $params);
        $ProductUrl = $this->general->cleanUrl("P", $_POST['productId']);
        $params['ProductName'] = $product_detail_arr['data']['query'][0]['mp_title'];
        $params['productUrl'] = $ProductUrl;
        $params['FromName'] = $_POST['sendername'];
        $params['FromEmail'] = $_POST['sender']['email'];
        $params['ToName'] = $_POST['recipients']['name'];
        $params['ToEmailId'] = $_POST['recipients']['email'];
        $params['Subject'] = $_POST['sender']['message'];
        // $params['json_str'] = $_POST['recipients'];
        for ($i=0; $i <count($_POST['recipients']) ; $i++) { 
                $input_params[$i]['to_email_id'] =  $_POST['recipients']['email'][$i];
                $input_params[$i]['to_name'] =  $_POST['recipients']['name'][$i];
        }
        $params['json_str'] = $input_params;
        $ws_name = 'send_email_to_friend';
        $result = $this->hbmodel->getData($ws_name, $params);
        if($result['settings']['success'] == 1){
            $this->session->set_flashdata('success', "Invitation sent Successfully.");
        }else
        {
            $this->session->set_flashdata('failure', "Error while sending invitation");
        }
            // redirect($this->config->item('site_url'));
        redirect($ProductUrl);
    }
}
