<%$this->js->add_js("productlist.js")%>
<div class='productlist'></div>
<div class="main show-bg"> <!-- container -->
  <div class="preface grid-full in-col1"></div>
  <div class="col-main grid4-3 grid-col2-main in-col2">
    <div class="send-friend common-back">
      <div class="page-title">
        <h3>Email to a Friend</h3>
      </div>
      <ul class="messages">
      </ul>
      <form id="product_sendtofriend_form" role="form" method="post" action="<%$this->config->item('site_url')%>wishlist/wishlist/sendMailToFriend">
        <input type="hidden" name="productId" value="<%$productId%>">
        <div class="fieldset send_to_friend">
          <%assign var=fullname value=$this->session->userdata('vFirstName')|cat: " "|cat: $this->session->userdata('vLastName')%>
          <h2 class="legend"><span>Sender:</span></h2>
          <ul id="sender_options" class="form-list">
            <li class="fields">
              <div class="field">
                <label class="required" for="sender_name"><em>*</em>Name:</label>
                <div class="input-box">
                  <input type="text" class="input-text required-entry" id="sender_name" title="Name" value="<%$fullname%>" name="sendername">
                </div>
              </div>
              <div class="field">
                <label class="required" for="sender_email"><em>*</em>Email:</label>
                <div class="input-box">
                  <input type="text" class="input-text required-entry validate-email" id="sender_email" title="Email Address" value="<%$this->session->userdata('vEmail')%>" name="sender[email]">
                </div>
              </div>
            </li>
            <li class="wide">
              <label class="required" for="sender_message"><em>*</em>Message:</label>
              <div class="input-box">
                <textarea rows="3" cols="3" id="sender_message" class="input-text required-entry" name="sender[message]" style="margin-bottom:10px"></textarea>
              </div>
            </li>
            <li>
                <div class="form-group" style="margin-bottom: 25px;">
                  <!-- <label for="Captcha" class="col-sm-4 control-label">Enter Captcha</label> -->
                  <!-- <div class="col-sm-8">                -->
                    <img src="<%$this->captcha->show()%>" id="capchareset" style="cursor:pointer;position: relative;width:165px;height:34px;vertical-align:top; float:left;" />
                    <input class="captcha-input" type="text" id="Captcha" name="Captcha" value="" maxlength="6" style="float:left; margin-left:4px;">          
                    <img src="<%$this->config->item('images_url')%>refresh.png" id="resetcaptcha" style="float:left; width:43px; cursor:pointer;">
                    <!-- <button class="btn btn-default" id="resetcaptcha"><i class="fa fa-refresh"></i></button>               -->
                    <div class="clear"></div>
                    <span id="CaptchaErr"></span>
                  <!-- </div> -->
                </div>
            </li>
          </ul>
          
          <div class="buttons-set">
          
          <input type="button" id="send_mail" class="button" value="Send Email" >
          <div style="display:none;" id="max_recipient_message">
            <p class="limit">Maximum 10 email addresses allowed.</p>
          </div>
          
        </div>
        </div>
        <div class="fieldset send_to_friend">
          <h2 class="legend"><span>Recipient:</span></h2>
          <ul id="recipients_options" class="form-list">
            <li class="fields">
              <div class="field">
                <label class="required" for="recipients_name"><em>*</em>Name:</label>
                <div class="input-box">
                  <input type="text" id="recipients_name" class="input-text required-entry" name="recipients[name][0]">
                </div>
              </div>
              <div class="field">
                <label class="required" for="recipients_email"><em>*</em>Email Address:</label>
                <div class="input-box">
                  <input type="text" class="input-text required-entry validate-email" id="recipients_email" title="Email Address" value="" name="recipients[email][0]">
                </div>
              </div>
            </li>
          </ul>
          <p id="add_recipient_button">
            <button class="button" onClick="add_recipient();" type="button"><span><span>Add Recipient</span></span></button>
          </p>
          
        </div>
        
      </form>
       </div>
  </div>
  <div class="col-right sidebar grid4-1 grid-col2-sidebar in-sidebar">
    <div style="display:none" class="block block-wishlist"></div>
  </div>
  <div class="postscript grid-full in-col1"></div>
</div>