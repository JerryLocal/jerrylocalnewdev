<%$this->js->add_js("wishlist.js")%>
<!-- <div id="wishlist"></div> -->
<div class="main-containt">
<div class=""> <!-- container -->
<div class="row-fluid">
<%$this->general->Breadcrumb('CLEAN','Wishlist')%>
</div>
<div class=""> <!-- container -->
<div class="row">
<div class="product-list">
<%include file = 'left.tpl'%>
<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
<div class="delivery-box">
<h3><div class="row"><div class="col-sm-9">MY WISHLIST (<span class="wishlist_total" data-rel="<%$wishlist_arr['data']|@count%>"><%$wishlist_arr['data']|@count %></span> ITEMS)</div></div></h3>
<div class="wishlist-list">
    <%if $wishlist_arr['data']|@count gt '0' %>
        <ol class="products-list wish_list">
          <%foreach $wishlist_arr['data'] as $product%> 
          <li class="item" id="<%$product['tpw_mst_products_id']%>">
            <%assign var=pUrl value=$this->general->cleanUrl('p',$product['tpw_mst_products_id'])%> 
            <div class="inner-item ">
              <div class="item-image">
                    <div class="inner">
                      <a class="product-image no-touch" title="" href="<%$pUrl%>">
                        <img class="first_image" src="<%$product['query'][0]['mp_default_img']%>" alt="<%$product['query'][0]['mp_title']%>">
                      </a> 
                    </div>
              </div>
              <div class="product-shop">
                    <div class="inner">
                  <h2 class="product-name">
                    <a class="product-image" href="<%$pUrl%>" title="<%$product['query'][0]['mp_title']%>"><%$product['query'][0]['mp_title']%></a></h2>
                  <%$this->general->ProductPriceFormat('2',$product['query'][0]['mp_regular_price'],$product['query'][0]['mp_sale_price'],$product['query'][0]['mp_difference_per'])%>
                 </div>
                  <div class="desc std"> 
                    <%if $product['query'][0]['mp_short_description'] neq ''%>
                    <p class="margin_space"><%$product['query'][0]['mp_short_description']%> </p>
                    <%/if%>

                  <a class="btn-block btn-link remove_wishlist" data-rel="<%$product['tpw_mst_products_id']%>">Remove from Wishlist</a>
                  </div>
                  <div class="wrap-btn-prolist"> 
                  <div class="item-btn">
                      <div class="box-inner"><a title="Add to wishlist" class="link-wishlist"></a><%$product['query'][0]['mp_date']%></div>
                    </div>
                     <%$product['orderbtn_function'][0]['OrderBtn']%>
                  </div>
              </div>
            </div>
          </li>   
          <%/foreach%> 
        </ol>
        <%else%>
          <div class="no-wishlist">
              <p>No product available</p>
          </div>
          <%/if%>
  </div>
</div>
</div>
</div>
</div></div>                         
</div>
</div>