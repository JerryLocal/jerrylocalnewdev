<?php

/**
 * Description of Career Controller
 * 
 * @module Career
 * 
 * @class career.php
 * 
 * @path application\front\content\controllers\career.php
 * 
 * @author Sarvil Ajwaliya
 * 
 * @date 06.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Sitemap extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();        
        $this->load->model('hbmodel');        
    }

    /**
     * index method is used to initialize index function.
     */
    public function index() {
        $params = array();
        $ws_name = 'get_sitemap';
        $data_arr = $this->hbmodel->getData($ws_name, $params);
        $data = $data_arr['data'];
        $this->smarty->assign('sitemap_data',$data);
    }

    public function sitemaplist() {
        // Get categories whose parent id 0
        $categories = $this->generalfront->getPrimaryOption();
        foreach($categories as $key=>$category) {
            if(count($category) > 0) {
                $categories[$key]['vCleanUrl'] = $this->general->CleanUrl("c", $category['iMstCategoriesId']);
                $categories[$key]['child'] = $this->generalfront->getCategoriesAsPerIds($category['iMstCategoriesId']);
                if(count($categories[$key]['child']) > 0) {
                    foreach($categories[$key]['child'] as $childkey=>$childcat) {
                        $categories[$key]['child'][$childkey]['vCleanUrl'] = $this->general->CleanUrl("c", $childcat['iMstCategoriesId']);
                        $nlevelchildids = implode(",", $this->generalfront->categorywalkup_nlevel_list_by_id($childcat['iMstCategoriesId']));
                        if(!empty($nlevelchildids)) {
                            $subcategories = $this->generalfront->categorydetails($nlevelchildids);
                            $categories[$key]['child'][$childkey]['child'] = $subcategories;
                        }
                    }
                }
            }
        }
        $this->smarty->assign('sitemap_data',$categories);
    }
}