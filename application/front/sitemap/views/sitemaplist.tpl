<div class="main-containt">
	<div class="container">
		<div class="row-fluid">
			<%$this->general->Breadcrumb('CLEAN','Sitemap')%>
		</div>
	</div>
	<div class="container">
		<div class="delivery-box"><h3>Sitemap</h3>
			<div class="form-add">
				<%foreach $sitemap_data  as $sitemap_item %>
				<div class="sitemap-blog">
					<h3><a href="<%$sitemap_item['vCleanUrl']%>"><%$sitemap_item['vTitle']%></a></h3>
					<%assign var=citem value=$sitemap_item['child']%>
					<%assign var=totalPage value=$citem|@count%>
					<%if $totalPage gt 0%>
					<%foreach $citem as $child_item%>
						<h4><a href="<%$child_item['vCleanUrl']%>"><%$child_item['vTitle']%></a></h4>
						<%assign var=scitem value=$child_item['child']%>
						<%assign var=subchildtotal value=$scitem|@count%>
						<%if $subchildtotal gt 0%>
							<ul>
							<%foreach $scitem as $subchilditem%>
							<li><a href="<%$subchilditem['vCleanUrl']%>"><%$subchilditem['vTitle']%></a></li>
							<%/foreach%>
							</ul>
						<%/if%>
					<%/foreach%>
					<%/if%>
				</div>
				<%/foreach%>
			</div>
		</div>
	</div>
</div>