  <div class="main-containt">
    <div class="container">
      <div class="row-fluid">
        <%$this->general->Breadcrumb('CLEAN','Sitemap')%> 
        </div></div>       
        <div class="container">
          <div class="delivery-box"><h3>Sitemap</h3>
          <div class="form-add">
          <%foreach $sitemap_data  as $sitemap_item %>
                  <div class="sitemap-blog">
                    <h3><%$sitemap_item['mc_title']%></h3>
                    <%assign var=citem value=$sitemap_item['custom_function']%>
                    <%assign var=totalPage value=$citem|@count%>
                    <%foreach $citem  as $child_item %>
                    <%if $child_item['iParentId'] eq $sitemap_item['mc_mst_categories_id']%>
                      <h4><a href="<%$child_item['vCleanUrl']%>"><%$child_item['vTitle']%></a></h4><ul>
                    <%/if%>
                      <%if $child_item['iParentId'] neq $sitemap_item['mc_mst_categories_id']%>
                        <li><a href="<%$child_item['vCleanUrl']%>"><%$child_item['vTitle']%></a></li>
                      <%/if%>
                      <%if $child_item@index eq ($totalPage-1)%>
                        </ul>
                      <%/if%>
                    <%/foreach%> 
                  </div>
           <%/foreach%>  
           </div>
           </div>
        </div>
      </div>