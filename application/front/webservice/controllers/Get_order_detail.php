<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getOrderDetail Controller
 *
 * @module getOrderDetail
 *
 * @class get_order_detail.php
 *
 * @path applicationront\webservice\controllers\get_order_detail.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Get_order_detail extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_order_detail');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "oid" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "oid_required",
                    "message" => "Please enter a value for the oid field.",
                ),
                array(
                    "rule" => "number",
                    "value" => TRUE,
                    "message_code" => "oid_number",
                    "message" => "Please enter valid number for the oid field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_order_detail");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_order_detail->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "main_order");
            $output_response = $this->main_order($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * main_order method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function main_order($input_params = array()) {

        $output_arr = $this->model_get_order_detail->main_order($input_params, $this->settings_params);
        $input_params["main_order"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("main_order", $output_arr, $input_params, "condition");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "main_order";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_get_order_detail->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "sub_order_sellers");
            return $this->sub_order_sellers($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "finish_success");
            return $this->finish_success($input_params);
        }
    }

    /**
     * sub_order_sellers method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function sub_order_sellers($input_params = array()) {

        $output_arr = $this->model_get_order_detail->sub_order_sellers($input_params, $this->settings_params);
        $input_params["sub_order_sellers"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("sub_order_sellers", $output_arr, $input_params, "start_loop");
        $this->multiple_keys[] = "sub_order_sellers";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->start_loop($input_params);
    }

    /**
     * start_loop method is used to process loop flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function start_loop($input_params = array()) {
        $this->iterate_start_loop($input_params["sub_order_sellers"], $input_params);

        $this->wsresponse->pushDebugParams("end_loop", array(), $input_params, "custom_function", "sub_order_sellers");
        return $this->custom_function($input_params);
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array()) {

        $output_arr = $this->model_get_order_detail->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "rtn_cncl_status", "sub_order_sellers");

        return $this->rtn_cncl_status($input_params);
    }

    /**
     * rtn_cncl_status method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rtn_cncl_status($input_params = array()) {
        if (method_exists($this->general, "return_cancel_btn_format")) {
            $result_arr["data"] = $this->general->return_cancel_btn_format($input_params);
            $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["rtn_cncl_status"] = $this->wsresponse->assignFunctionResponse($result_arr);
        }
        $this->wsresponse->pushDebugParams("rtn_cncl_status", $result_arr, $input_params, "custom_function", "sub_order_sellers", "end_loop");
        return $input_params;
    }

    /**
     * custom_function method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function custom_function($input_params = array()) {
        if (method_exists($this->general, "checkOrderStatus")) {
            $result_arr["data"] = $this->general->checkOrderStatus($input_params);
            $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["custom_function"] = $this->wsresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "custom_function";
            $this->wsresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->wsresponse->pushDebugParams("custom_function", $result_arr, $input_params, "finish_success_1");
        return $this->finish_success_1($input_params);
    }

    /**
     * finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success_1",
            "message" => "",
        );
        $output_fields = array(
            'mo_buyer_phone',
            'mo_buyer_pin_code',
            'mo_buyer_area',
            'mo_buyer_address2',
            'mo_buyer_address1',
            'mo_buyer_name',
            'mo_shipping_cost',
            'mo_sub_total',
            'mo_order_total',
            'mo_buyer_id',
            'mo_buyer_email',
            'mo_buyer_ip',
            'mo_payment_status',
            'mo_date',
            'mc_country',
            'ms_state',
            'mc1_city',
            'mso_mst_store_detail_id',
            'mso_store_name',
            'mso1_mst_sub_order_id',
            'mso1_mst_order_id',
            'query',
            'mso_product_name',
            'mso_mst_order_id',
            'mso_mst_sub_order_id',
            'mso_total_cost',
            'mso_item_status',
            'mso_tracking_number',
            'mso_shipper_name',
            'mp_default_img',
            'mp_difference_per',
            'mso_mst_products_id',
            'mso_product_regular_price',
            'mso_product_sale_price',
            'mso_product_qty',
            'mso_product_price',
            'ms_url',
            'tosh_date',
            'tosh_order_status',
            'mso_shipping_remark',
            'Discount_price',
            'mso_delivered_date_1',
            'mso_return_period',
            'rtn_cncl_status',
            'minstatus',
            'Discount',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_order_detail";
        $func_array["function"]["output_keys"] = array(
            'main_order',
            'sub_order_sellers',
            'custom_function',
        );
        $func_array["function"]["output_alias"] = array(
            "mo_buyer_phone" => "mo_buyer_phone",
            "mo_buyer_pin_code" => "mo_buyer_pin_code",
            "mo_buyer_area" => "mo_buyer_area",
            "mo_buyer_address2" => "mo_buyer_address2",
            "mo_buyer_address1" => "mo_buyer_address1",
            "mo_buyer_name" => "mo_buyer_name",
            "mo_shipping_cost" => "mo_shipping_cost",
            "mo_sub_total" => "mo_sub_total",
            "mo_order_total" => "mo_order_total",
            "mo_buyer_id" => "mo_buyer_id",
            "mo_buyer_email" => "mo_buyer_email",
            "mo_buyer_ip" => "mo_buyer_ip",
            "mo_payment_status" => "mo_payment_status",
            "mo_date" => "mo_date",
            "mc_country" => "mc_country",
            "ms_state" => "ms_state",
            "mc1_city" => "mc1_city",
            "mso_mst_store_detail_id" => "mso_mst_store_detail_id",
            "mso_store_name" => "mso_store_name",
            "mso1_mst_sub_order_id" => "mso1_mst_sub_order_id",
            "mso1_mst_order_id" => "mso1_mst_order_id",
            "query" => "query",
            "mso_product_name" => "mso_product_name",
            "mso_mst_order_id" => "mso_mst_order_id",
            "mso_mst_sub_order_id" => "mso_mst_sub_order_id",
            "mso_total_cost" => "mso_total_cost",
            "mso_item_status" => "mso_item_status",
            "mso_tracking_number" => "mso_tracking_number",
            "mso_shipper_name" => "mso_shipper_name",
            "mp_default_img" => "mp_default_img",
            "mp_difference_per" => "mp_difference_per",
            "mso_mst_products_id" => "mso_mst_products_id",
            "mso_product_regular_price" => "mso_product_regular_price",
            "mso_product_sale_price" => "mso_product_sale_price",
            "mso_product_qty" => "mso_product_qty",
            "mso_product_price" => "mso_product_price",
            "ms_url" => "ms_url",
            "tosh_date" => "tosh_date",
            "tosh_order_status" => "tosh_order_status",
            "mso_shipping_remark" => "mso_shipping_remark",
            "Discount_price" => "Discount_price",
            "mso_delivered_date_1" => "mso_delivered_date_1",
            "mso_return_period" => "mso_return_period",
            "rtn_cncl_status" => "rtn_cncl_status",
            "minstatus" => "minstatus",
            "Discount" => "Discount",
        );
        $func_array["function"]["inner_keys"] = array(
            'query',
            'rtn_cncl_status',
        );
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "",
            "message_code" => "finish_success",
            "message" => "",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_order_detail";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * iterate_start_loop method is used to iterate loop.
     *
     * @param array $sub_order_sellers_lp_arr sub_order_sellers_lp_arr array to iterate loop.
     * @param array $input_params_addr $input_params_addr array to address original input params.
     */
    public function iterate_start_loop(&$sub_order_sellers_lp_arr = array(), &$input_params_addr = array()) {

        $_loop_params_tmp = $_loop_tmp_dict = array();
        $input_params_loc = $input_params_addr;
        $_loop_params_loc = $sub_order_sellers_lp_arr;
        $_lp_ini = 0;
        $_lp_end = count($_loop_params_loc);
        $_lp_cnf = (is_array($_loop_params_loc[0])) ? TRUE : FALSE;
        $_loop_debug_tmp = array(
            "start_point" => $_lp_ini,
            "end_point" => $_lp_end,
            "step" => 1,
            "loop" => "sub_order_sellers",
        );
        $this->wsresponse->pushDebugParams("start_loop", $_loop_debug_tmp, $input_params, "query");
        for ($i = $_lp_ini; $i < $_lp_end; $i += 1) {
            $sub_order_sellers_lp_pms = $input_params_loc;
            if ($_lp_cnf) {
                if (is_array($_loop_params_loc[$i])) {
                    $sub_order_sellers_lp_pms = $_loop_params_loc[$i]+$input_params_loc;
                }
                unset($sub_order_sellers_lp_pms["sub_order_sellers"]);
            } else {
                $sub_order_sellers_lp_pms["sub_order_sellers"] = $_loop_params_loc[$i];
            }
            $sub_order_sellers_lp_pms["i"] = $i;
            $sub_order_sellers_lp_pms["__dictionaries"] = $_loop_tmp_dict;
            $input_params = $sub_order_sellers_lp_pms;
            $input_params = $this->query($input_params);
            if (is_array($input_params["__dictionaries"])) {
                $_loop_tmp_dict = $input_params["__dictionaries"];
                unset($input_params["__dictionaries"]);
            }
            if (is_array($input_params["__variables"])) {
                $input_params = $this->wsresponse->grabLoopVariables($input_params["__variables"], $input_params);
                unset($input_params["__variables"]);
            }
            if ($this->break_continue == 1) {
                $this->break_continue = NULL;
                break;
            } elseif ($this->break_continue == 2) {
                $this->break_continue = NULL;
                continue;
            }
            if ($_lp_cnf) {
                $sub_order_sellers_lp_arr[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $sub_order_sellers_lp_pms);
            } else {
                $_loop_params_tmp[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $sub_order_sellers_lp_pms);
            }
        }
        if (!$_lp_cnf) {
            $input_params_addr["sub_order_sellers"] = $_loop_params_tmp;
        }
        if (is_array($_loop_tmp_dict)) {
            $input_params_addr = array_merge($input_params_addr, $_loop_tmp_dict);
        }
    }
}
