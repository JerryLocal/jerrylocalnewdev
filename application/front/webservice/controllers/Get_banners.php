<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getBanners Controller
 *
 * @module getBanners
 *
 * @class get_banners.php
 *
 * @path applicationront\webservice\controllers\get_banners.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Get_banners extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_banners');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array();
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_banners");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_banners->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "get_slider");
            $output_response = $this->get_slider($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * get_slider method is used to process query block.
     * To get all the slider banner
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_slider($input_params = array()) {

        $output_arr = $this->model_get_banners->get_slider($input_params, $this->settings_params);
        $input_params["get_slider"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_slider", $output_arr, $input_params, "get_fix_banner");
        $this->multiple_keys[] = "get_slider";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->get_fix_banner($input_params);
    }

    /**
     * get_fix_banner method is used to process query block.
     * get all the banner with Fix banner
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_fix_banner($input_params = array()) {

        $output_arr = $this->model_get_banners->get_fix_banner($input_params, $this->settings_params);
        $input_params["get_fix_banner"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_fix_banner", $output_arr, $input_params, "finish_success");
        $this->multiple_keys[] = "get_fix_banner";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "Record Found.",
        );
        $output_fields = array(
            'mb_title',
            'mb_img',
            'mb_link',
            'mb_img_1',
            'mb_order',
            'mb_link_1',
            'mb_position',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_banners";
        $func_array["function"]["output_keys"] = array(
            'get_slider',
            'get_fix_banner',
        );
        $func_array["function"]["output_alias"] = array(
            "mb_title" => "mb_title",
            "mb_img" => "mb_img",
            "mb_link" => "mb_link",
            "mb_img_1" => "mb_img_1",
            "mb_order" => "mb_order",
            "mb_link_1" => "mb_link_1",
            "mb_position" => "mb_position",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
