<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of product_invoice Controller
 *
 * @module product_invoice
 *
 * @class product_invoice.php
 *
 * @path applicationront\webservice\controllers\product_invoice.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Product_invoice extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_product_invoice');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array();
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "product_invoice");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_product_invoice->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "invoice_data");
            $output_response = $this->invoice_data($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * invoice_data method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function invoice_data($input_params = array()) {

        $output_arr = $this->model_product_invoice->invoice_data($input_params, $this->settings_params);
        $input_params["invoice_data"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("invoice_data", $output_arr, $input_params, "condition");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "invoice_data";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_product_invoice->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mst_sub_order_finish_success_1");
            return $this->mst_sub_order_finish_success_1($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mst_sub_order_finish_success");
            return $this->mst_sub_order_finish_success($input_params);
        }
    }

    /**
     * mst_sub_order_finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_sub_order_finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "mst_sub_order_finish_success_1",
            "message" => "",
        );
        $output_fields = array(
            'mso_mst_sub_order_id',
            'mso_mst_order_id',
            'mso_product_name',
            'mso_product_qty',
            'mso_seller_invoice_date',
            'mso_seller_invoice_no',
            'mso_shipper_name',
            'mp_title',
            'mo_buyer_phone',
            'mo_buyer_pin_code',
            'mo_buyer_address1',
            'mo_buyer_area',
            'mo_buyer_name',
            'mo_buyer_email',
            'mc_country',
            'ms_state',
            'mc1_city',
            'msd_contact_name',
            'msd_contact_number',
            'msd_store_name',
            'msd_address1',
            'msd_pin_code',
            'msd_g_s_t_no',
            'msd_store_url',
            'mc2_country_seller',
            'ms1_state_seller',
            'mc3_city_seller',
            'mp_short_description',
            'ma_email',
            'mso_shipping_remark',
            'mso_sub_order_number_pre',
            'mso_sub_order_number',
            'mo_order_number_pre',
            'mo_order_number',
            'mp_sku',
            'mo_buyer_address2',
            'msd_address2',
            'msd_area',
            'mso_shipping_cost',
            'mso_total_cost',
            'mso_product_sale_price',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "product_invoice";
        $func_array["function"]["output_keys"] = array(
            'invoice_data',
        );
        $func_array["function"]["output_alias"] = array(
            "mso_mst_sub_order_id" => "mso_mst_sub_order_id",
            "mso_mst_order_id" => "mso_mst_order_id",
            "mso_product_name" => "mso_product_name",
            "mso_product_qty" => "mso_product_qty",
            "mso_seller_invoice_date" => "mso_seller_invoice_date",
            "mso_seller_invoice_no" => "mso_seller_invoice_no",
            "mso_shipper_name" => "mso_shipper_name",
            "mp_title" => "mp_title",
            "mo_buyer_phone" => "mo_buyer_phone",
            "mo_buyer_pin_code" => "mo_buyer_pin_code",
            "mo_buyer_address1" => "mo_buyer_address1",
            "mo_buyer_area" => "mo_buyer_area",
            "mo_buyer_name" => "mo_buyer_name",
            "mo_buyer_email" => "mo_buyer_email",
            "mc_country" => "mc_country_buyer",
            "ms_state" => "ms_state_buyer",
            "mc1_city" => "mc1_city_buyer",
            "msd_contact_name" => "msd_contact_name",
            "msd_contact_number" => "msd_contact_number",
            "msd_store_name" => "msd_store_name",
            "msd_address1" => "msd_address_seller",
            "msd_pin_code" => "msd_pin_code_seller",
            "msd_g_s_t_no" => "msd_g_s_t_no",
            "msd_store_url" => "msd_store_url",
            "mc2_country_seller" => "mc_country_seller",
            "ms1_state_seller" => "ms_state_seller",
            "mc3_city_seller" => "mc_city_seller",
            "mp_short_description" => "mp_short_description",
            "ma_email" => "ma_seller_email",
            "mso_shipping_remark" => "mso_shipping_remark",
            "mso_sub_order_number_pre" => "mso_sub_order_number_pre",
            "mso_sub_order_number" => "mso_sub_order_number",
            "mo_order_number_pre" => "mo_order_number_pre",
            "mo_order_number" => "mo_order_number",
            "mp_sku" => "mp_sku",
            "mo_buyer_address2" => "mo_buyer_address2",
            "msd_address2" => "msd_address2",
            "msd_area" => "msd_area",
            "mso_shipping_cost" => "mso_shipping_cost",
            "mso_total_cost" => "mso_total_cost",
            "mso_product_sale_price" => "mso_product_sale_price",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_sub_order_finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_sub_order_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_sub_order_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_sub_order_finish_success",
            "message" => "Error While Generating Your Invoice",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "product_invoice";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_sub_order_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
