<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of returnOrder Controller
 *
 * @module returnOrder
 *
 * @class return_order.php
 *
 * @path applicationront\webservice\controllers\return_order.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Return_order extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_return_order');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "iOrderId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iOrderId_required",
                    "message" => "Please enter a value for the iOrderId field.",
                )
            ),
            "ePreference" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "ePreference_required",
                    "message" => "Please enter a value for the ePreference field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "return_order");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_return_order->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "get_order_other_detail");
            $output_response = $this->get_order_other_detail($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * get_order_other_detail method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_order_other_detail($input_params = array()) {

        $output_arr = $this->model_return_order->get_order_other_detail($input_params, $this->settings_params);
        $input_params["get_order_other_detail"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_order_other_detail", $output_arr, $input_params, "check_value");
        $this->multiple_keys[] = "get_order_other_detail";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->check_value($input_params);
    }

    /**
     * check_value method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function check_value($input_params = array()) {

        $output_arr = $this->model_return_order->check_value($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("check_value", $output_arr, $input_params, "query");
            return $this->query($input_params);
        } else {
            $this->wsresponse->pushDebugParams("check_value", $output_arr, $input_params, "mst_sub_order_finish_success_1");
            return $this->mst_sub_order_finish_success_1($input_params);
        }
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array()) {

        $output_arr = $this->model_return_order->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "mst_sub_order_finish_success");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->mst_sub_order_finish_success($input_params);
    }

    /**
     * mst_sub_order_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_sub_order_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "mst_sub_order_finish_success",
            "message" => "Return Request is successfully Added",
        );
        $output_fields = array(
            'mso_mst_order_id',
            'mso_mst_sub_order_id',
            'mso_mst_store_detail_id',
            'mo_buyer_id',
            'msd_admin_id',
            'insert_id',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "return_order";
        $func_array["function"]["output_keys"] = array(
            'get_order_other_detail',
        );
        $func_array["function"]["output_alias"] = array(
            "mso_mst_order_id" => "mso_mst_order_id",
            "mso_mst_sub_order_id" => "mso_mst_sub_order_id",
            "mso_mst_store_detail_id" => "mso_mst_store_detail_id",
            "mo_buyer_id" => "mo_buyer_id",
            "msd_admin_id" => "msd_admin_id",
            "insert_id" => "insert_id",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_sub_order_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_sub_order_finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_sub_order_finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_sub_order_finish_success_1",
            "message" => "No Order available",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "return_order";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_sub_order_finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
