<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of sendEmailToFriend Controller
 *
 * @module sendEmailToFriend
 *
 * @class send_email_to_friend.php
 *
 * @path applicationront\webservice\controllers\send_email_to_friend.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Send_email_to_friend extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_send_email_to_friend');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "FromEmail" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "FromEmail_required",
                    "message" => "Please enter a value for the FromEmail field.",
                ),
                array(
                    "rule" => "email",
                    "value" => TRUE,
                    "message_code" => "FromEmail_email",
                    "message" => "Please enter valid email address for the FromEmail field.",
                )
            ),
            "FromName" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "FromName_required",
                    "message" => "Please enter a value for the FromName field.",
                )
            ),
            "Subject" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "Subject_required",
                    "message" => "Please enter a value for the Subject field.",
                )
            ),
            "ProductName" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "ProductName_required",
                    "message" => "Please enter a value for the ProductName field.",
                )
            ),
            "productUrl" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "productUrl_required",
                    "message" => "Please enter a value for the productUrl field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "send_email_to_friend");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_send_email_to_friend->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];
            if (!is_array($input_params["json_str"])) {
                $input_params["json_str"] = json_decode($input_params["json_str"], TRUE);
            }
            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "decode_email_data");
            $output_response = $this->decode_email_data($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * decode_email_data method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function decode_email_data($input_params = array()) {
        if (method_exists($this->general, "decode_email_data")) {
            $result_arr["data"] = $this->general->decode_email_data($input_params);
            $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["decode_email_data"] = $this->wsresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "decode_email_data";
            $this->wsresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->wsresponse->pushDebugParams("decode_email_data", $result_arr, $input_params, "start_loop_emails");
        return $this->start_loop_emails($input_params);
    }

    /**
     * start_loop_emails method is used to process loop flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function start_loop_emails($input_params = array()) {
        $this->iterate_start_loop_emails($input_params["json_str"], $input_params);

        $this->wsresponse->pushDebugParams("end_loop_emails", array(), $input_params, "finish_success", "json_str");
        return $this->finish_success($input_params);
    }

    /**
     * email_notification method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function email_notification($input_params = array()) {

        $output_arr = $this->model_send_email_to_friend->email_notification($input_params);
        $input_params["email_notification"] = $output_arr["success"];
        $this->wsresponse->pushDebugParams("email_notification", $output_arr, $input_params, "finish_success", "json_str", "end_loop_emails");

        return $input_params;
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "Success",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "send_email_to_friend";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * iterate_start_loop_emails method is used to iterate loop.
     *
     * @param array $json_str_lp_arr json_str_lp_arr array to iterate loop.
     * @param array $input_params_addr $input_params_addr array to address original input params.
     */
    public function iterate_start_loop_emails(&$json_str_lp_arr = array(), &$input_params_addr = array()) {

        $_loop_params_tmp = $_loop_tmp_dict = array();
        $input_params_loc = $input_params_addr;
        $_loop_params_loc = $json_str_lp_arr;
        $_lp_ini = 0;
        $_lp_end = count($_loop_params_loc);
        $_lp_cnf = (is_array($_loop_params_loc[0])) ? TRUE : FALSE;
        $_loop_debug_tmp = array(
            "start_point" => $_lp_ini,
            "end_point" => $_lp_end,
            "step" => 1,
            "loop" => "json_str",
        );
        $this->wsresponse->pushDebugParams("start_loop_emails", $_loop_debug_tmp, $input_params, "email_notification");
        for ($i = $_lp_ini; $i < $_lp_end; $i += 1) {
            $json_str_lp_pms = $input_params_loc;
            if ($_lp_cnf) {
                if (is_array($_loop_params_loc[$i])) {
                    $json_str_lp_pms = $_loop_params_loc[$i]+$input_params_loc;
                }
                unset($json_str_lp_pms["json_str"]);
            } else {
                $json_str_lp_pms["json_str"] = $_loop_params_loc[$i];
            }
            $json_str_lp_pms["i"] = $i;
            $json_str_lp_pms["__dictionaries"] = $_loop_tmp_dict;
            $input_params = $json_str_lp_pms;
            $input_params = $this->email_notification($input_params);
            if (is_array($input_params["__dictionaries"])) {
                $_loop_tmp_dict = $input_params["__dictionaries"];
                unset($input_params["__dictionaries"]);
            }
            if (is_array($input_params["__variables"])) {
                $input_params = $this->wsresponse->grabLoopVariables($input_params["__variables"], $input_params);
                unset($input_params["__variables"]);
            }
            if ($this->break_continue == 1) {
                $this->break_continue = NULL;
                break;
            } elseif ($this->break_continue == 2) {
                $this->break_continue = NULL;
                continue;
            }
            if ($_lp_cnf) {
                $json_str_lp_arr[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $json_str_lp_pms);
            } else {
                $_loop_params_tmp[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $json_str_lp_pms);
            }
        }
        if (!$_lp_cnf) {
            $input_params_addr["json_str"] = $_loop_params_tmp;
        }
        if (is_array($_loop_tmp_dict)) {
            $input_params_addr = array_merge($input_params_addr, $_loop_tmp_dict);
        }
    }
}
