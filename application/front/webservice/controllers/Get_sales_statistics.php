<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getSalesStatistics Controller
 *
 * @module getSalesStatistics
 *
 * @class get_sales_statistics.php
 *
 * @path applicationront\webservice\controllers\get_sales_statistics.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Get_sales_statistics extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_sales_statistics');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array();
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_sales_statistics");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_sales_statistics->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "total_user_query");
            $output_response = $this->total_user_query($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * total_user_query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function total_user_query($input_params = array()) {

        $output_arr = $this->model_get_sales_statistics->total_user_query($input_params, $this->settings_params);
        $input_params["total_user_query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("total_user_query", $output_arr, $input_params, "live_user_query");
        $this->multiple_keys[] = "total_user_query";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->live_user_query($input_params);
    }

    /**
     * live_user_query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function live_user_query($input_params = array()) {

        $output_arr = $this->model_get_sales_statistics->live_user_query($input_params, $this->settings_params);
        $input_params["live_user_query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("live_user_query", $output_arr, $input_params, "finish_success");
        $this->multiple_keys[] = "live_user_query";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "",
        );
        $output_fields = array(
            'Total_user',
            'live_user',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_sales_statistics";
        $func_array["function"]["output_keys"] = array(
            'total_user_query',
            'live_user_query',
        );
        $func_array["function"]["output_alias"] = array(
            "Total_user" => "Total_user",
            "live_user" => "live_user",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
