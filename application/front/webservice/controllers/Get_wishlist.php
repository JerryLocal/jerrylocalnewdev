<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getWishlist Controller
 *
 * @module getWishlist
 *
 * @class get_wishlist.php
 *
 * @path applicationront\webservice\controllers\get_wishlist.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Get_wishlist extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_wishlist');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "iAdminID" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iAdminID_required",
                    "message" => "Please enter a value for the iAdminID field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_wishlist");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_wishlist->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "select_product_id");
            $output_response = $this->select_product_id($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * select_product_id method is used to process query block.
     * Get the all product id based on iAdminID
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function select_product_id($input_params = array()) {

        $output_arr = $this->model_get_wishlist->select_product_id($input_params, $this->settings_params);
        $input_params["select_product_id"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("select_product_id", $output_arr, $input_params, "check_wishlist_product");
        $this->multiple_keys[] = "select_product_id";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->check_wishlist_product($input_params);
    }

    /**
     * check_wishlist_product method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function check_wishlist_product($input_params = array()) {

        $output_arr = $this->model_get_wishlist->check_wishlist_product($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("check_wishlist_product", $output_arr, $input_params, "start_loop");
            return $this->start_loop($input_params);
        } else {
            $this->wsresponse->pushDebugParams("check_wishlist_product", $output_arr, $input_params, "finish_success");
            return $this->finish_success($input_params);
        }
    }

    /**
     * start_loop method is used to process loop flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function start_loop($input_params = array()) {
        $this->iterate_start_loop($input_params["select_product_id"], $input_params);

        $this->wsresponse->pushDebugParams("end_loop", array(), $input_params, "finish_success_1", "select_product_id");
        return $this->finish_success_1($input_params);
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array()) {

        $output_arr = $this->model_get_wishlist->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "orderbtn_function", "select_product_id");

        return $this->orderbtn_function($input_params);
    }

    /**
     * orderbtn_function method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function orderbtn_function($input_params = array()) {
        if (method_exists($this->general, "OrderBtn")) {
            $result_arr["data"] = $this->general->OrderBtn($input_params);
            $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["orderbtn_function"] = $this->wsresponse->assignFunctionResponse($result_arr);
        }
        $this->wsresponse->pushDebugParams("orderbtn_function", $result_arr, $input_params, "finish_success_1", "select_product_id", "end_loop");
        return $input_params;
    }

    /**
     * finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success_1",
            "message" => "",
        );
        $output_fields = array(
            'tpw_mst_products_id',
            'mp_title',
            'mp_regular_price',
            'mp_sale_price',
            'mp_difference_per',
            'mp_default_img',
            'mp_date',
            'mp_rating_avg',
            'mp_total_rate',
            'mp_stock',
            'mp_status',
            'orderbtn_function',
            'OrderBtn',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_wishlist";
        $func_array["function"]["output_keys"] = array(
            'select_product_id',
        );
        $func_array["function"]["output_alias"] = array(
            "tpw_mst_products_id" => "tpw_mst_products_id",
            "mp_title" => "mp_title",
            "mp_regular_price" => "mp_regular_price",
            "mp_sale_price" => "mp_sale_price",
            "mp_difference_per" => "mp_difference_per",
            "mp_default_img" => "mp_default_img",
            "mp_date" => "mp_date",
            "mp_rating_avg" => "mp_rating_avg",
            "mp_total_rate" => "mp_total_rate",
            "mp_stock" => "mp_stock",
            "mp_status" => "mp_status",
            "orderbtn_function" => "orderbtn_function",
            "OrderBtn" => "OrderBtn",
        );
        $func_array["function"]["inner_keys"] = array(
            'orderbtn_function',
        );
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "finish_success",
            "message" => "No Record Found",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_wishlist";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * iterate_start_loop method is used to iterate loop.
     *
     * @param array $select_product_id_lp_arr select_product_id_lp_arr array to iterate loop.
     * @param array $input_params_addr $input_params_addr array to address original input params.
     */
    public function iterate_start_loop(&$select_product_id_lp_arr = array(), &$input_params_addr = array()) {

        $_loop_params_tmp = $_loop_tmp_dict = array();
        $input_params_loc = $input_params_addr;
        $_loop_params_loc = $select_product_id_lp_arr;
        $_lp_ini = 0;
        $_lp_end = count($_loop_params_loc);
        $_lp_cnf = (is_array($_loop_params_loc[0])) ? TRUE : FALSE;
        $_loop_debug_tmp = array(
            "start_point" => $_lp_ini,
            "end_point" => $_lp_end,
            "step" => 1,
            "loop" => "select_product_id",
        );
        $this->wsresponse->pushDebugParams("start_loop", $_loop_debug_tmp, $input_params, "query");
        for ($i = $_lp_ini; $i < $_lp_end; $i += 1) {
            $select_product_id_lp_pms = $input_params_loc;
            if ($_lp_cnf) {
                if (is_array($_loop_params_loc[$i])) {
                    $select_product_id_lp_pms = $_loop_params_loc[$i]+$input_params_loc;
                }
                unset($select_product_id_lp_pms["select_product_id"]);
            } else {
                $select_product_id_lp_pms["select_product_id"] = $_loop_params_loc[$i];
            }
            $select_product_id_lp_pms["i"] = $i;
            $select_product_id_lp_pms["__dictionaries"] = $_loop_tmp_dict;
            $input_params = $select_product_id_lp_pms;
            $input_params = $this->query($input_params);
            if (is_array($input_params["__dictionaries"])) {
                $_loop_tmp_dict = $input_params["__dictionaries"];
                unset($input_params["__dictionaries"]);
            }
            if (is_array($input_params["__variables"])) {
                $input_params = $this->wsresponse->grabLoopVariables($input_params["__variables"], $input_params);
                unset($input_params["__variables"]);
            }
            if ($this->break_continue == 1) {
                $this->break_continue = NULL;
                break;
            } elseif ($this->break_continue == 2) {
                $this->break_continue = NULL;
                continue;
            }
            if ($_lp_cnf) {
                $select_product_id_lp_arr[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $select_product_id_lp_pms);
            } else {
                $_loop_params_tmp[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $select_product_id_lp_pms);
            }
        }
        if (!$_lp_cnf) {
            $input_params_addr["select_product_id"] = $_loop_params_tmp;
        }
        if (is_array($_loop_tmp_dict)) {
            $input_params_addr = array_merge($input_params_addr, $_loop_tmp_dict);
        }
    }
}
