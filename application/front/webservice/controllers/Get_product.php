<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getProduct Controller
 *
 * @module getProduct
 *
 * @class get_product.php
 *
 * @path applicationront\webservice\controllers\get_product.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Get_product extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_product');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "cid" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "cid_required",
                    "message" => "Please enter a value for the iProdcutId field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_product");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_product->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "get_product_data");
            $output_response = $this->get_product_data($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * get_product_data method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_product_data($input_params = array()) {

        $output_arr = $this->model_get_product->get_product_data($input_params, $this->settings_params);
        $input_params["get_product_data"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_product_data", $output_arr, $input_params, "condition");
        $this->multiple_keys[] = "get_product_data";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_get_product->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "finish_success");
            return $this->finish_success($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "finish_success_1");
            return $this->finish_success_1($input_params);
        }
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "Record Found",
        );
        $output_fields = array(
            'mp1_mst_products_id_1',
            'mp1_mst_products_id',
            'mp1_admin_id',
            'mp1_title',
            'mp1_sku',
            'mp1_regular_price',
            'mp1_sale_price',
            'mp1_rating_avg',
            'mp1_allow_max_purchase',
            'mp1_low_avl_limit_notifcation',
            'mp1_stock',
            'mp1_difference_per',
            'mp1_total_rate',
            'mp1_date',
            'mp1_modify_date',
            'mp1_default_img',
            'mp1_description',
            'mp1_sale_state',
            'mp1_wishlist_state',
            'mp1_free_shipping',
            'mp1_view_state',
            'mp1_return_days',
            'mp1_do_return',
            'mp1_status',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_product";
        $func_array["function"]["output_keys"] = array(
            'product_count',
            'get_product_data',
        );
        $func_array["function"]["output_alias"] = array(
            "mp1_mst_products_id_1" => "total_product",
            "mp1_mst_products_id" => "mp1_mst_products_id",
            "mp1_admin_id" => "mp1_admin_id",
            "mp1_title" => "mp1_title",
            "mp1_sku" => "mp1_sku",
            "mp1_regular_price" => "mp1_regular_price",
            "mp1_sale_price" => "mp1_sale_price",
            "mp1_rating_avg" => "mp1_rating_avg",
            "mp1_allow_max_purchase" => "mp1_allow_max_purchase",
            "mp1_low_avl_limit_notifcation" => "mp1_low_avl_limit_notifcation",
            "mp1_stock" => "mp1_stock",
            "mp1_difference_per" => "mp1_difference_per",
            "mp1_total_rate" => "mp1_total_rate",
            "mp1_date" => "mp1_date",
            "mp1_modify_date" => "mp1_modify_date",
            "mp1_default_img" => "mp1_default_img",
            "mp1_description" => "mp1_description",
            "mp1_sale_state" => "mp1_sale_state",
            "mp1_wishlist_state" => "mp1_wishlist_state",
            "mp1_free_shipping" => "mp1_free_shipping",
            "mp1_view_state" => "mp1_view_state",
            "mp1_return_days" => "mp1_return_days",
            "mp1_do_return" => "mp1_do_return",
            "mp1_status" => "mp1_status",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "finish_success_1",
            "message" => "No Record Found",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_product";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
