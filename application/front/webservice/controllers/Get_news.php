<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getNews Controller
 *
 * @module getNews
 *
 * @class get_news.php
 *
 * @path applicationront\webservice\controllers\get_news.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Get_news extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_news');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array();
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_news");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_news->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "condition");
            $output_response = $this->condition($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_get_news->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "data_based_on_both_date");
            return $this->data_based_on_both_date($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "condition_1");
            return $this->condition_1($input_params);
        }
    }

    /**
     * data_based_on_both_date method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function data_based_on_both_date($input_params = array()) {

        $output_arr = $this->model_get_news->data_based_on_both_date($input_params, $this->settings_params);
        $input_params["data_based_on_both_date"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("data_based_on_both_date", $output_arr, $input_params, "mst_news_finish_success");
        $this->multiple_keys[] = "data_based_on_both_date";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->mst_news_finish_success($input_params);
    }

    /**
     * mst_news_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_news_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "mst_news_finish_success",
            "message" => "",
        );
        $output_fields = array(
            'mn_mst_news_id_1',
            'mn_title_1',
            'mn_description_1',
            'mn_date_1',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_news";
        $func_array["function"]["output_keys"] = array(
            'data_based_on_both_date',
        );
        $func_array["function"]["output_alias"] = array(
            "mn_mst_news_id_1" => "mn_mst_news_id",
            "mn_title_1" => "mn_title",
            "mn_description_1" => "mn_description",
            "mn_date_1" => "mn_date",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_news_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * condition_1 method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition_1($input_params = array()) {

        $output_arr = $this->model_get_news->condition_1($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition_1", $output_arr, $input_params, "get_news_data_by_start_date");
            return $this->get_news_data_by_start_date($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition_1", $output_arr, $input_params, "condition_2");
            return $this->condition_2($input_params);
        }
    }

    /**
     * get_news_data_by_start_date method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_news_data_by_start_date($input_params = array()) {

        $output_arr = $this->model_get_news->get_news_data_by_start_date($input_params, $this->settings_params);
        $input_params["get_news_data_by_start_date"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_news_data_by_start_date", $output_arr, $input_params, "finish_success");
        $this->multiple_keys[] = "get_news_data_by_start_date";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "News Data get Successfully",
        );
        $output_fields = array(
            'mn_mst_news_id',
            'mn_title',
            'mn_description',
            'mn_date',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_news";
        $func_array["function"]["output_keys"] = array(
            'get_news_data_by_start_date',
        );
        $func_array["function"]["output_alias"] = array(
            "mn_mst_news_id" => "mn_mst_news_id",
            "mn_title" => "mn_title",
            "mn_description" => "mn_description",
            "mn_date" => "mn_date",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * condition_2 method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition_2($input_params = array()) {

        $output_arr = $this->model_get_news->condition_2($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition_2", $output_arr, $input_params, "data_based_on_end_date");
            return $this->data_based_on_end_date($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition_2", $output_arr, $input_params, "all_data");
            return $this->all_data($input_params);
        }
    }

    /**
     * data_based_on_end_date method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function data_based_on_end_date($input_params = array()) {

        $output_arr = $this->model_get_news->data_based_on_end_date($input_params, $this->settings_params);
        $input_params["data_based_on_end_date"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("data_based_on_end_date", $output_arr, $input_params, "finish_success_1");
        $this->multiple_keys[] = "data_based_on_end_date";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->finish_success_1($input_params);
    }

    /**
     * finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success_1",
            "message" => "",
        );
        $output_fields = array(
            'mn_mst_news_id_2',
            'mn_title_2',
            'mn_description_2',
            'mn_date_2',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_news";
        $func_array["function"]["output_keys"] = array(
            'data_based_on_end_date',
        );
        $func_array["function"]["output_alias"] = array(
            "mn_mst_news_id_2" => "mn_mst_news_id",
            "mn_title_2" => "mn_title",
            "mn_description_2" => "mn_description",
            "mn_date_2" => "mn_date",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * all_data method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function all_data($input_params = array()) {

        $output_arr = $this->model_get_news->all_data($input_params, $this->settings_params);
        $input_params["all_data"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("all_data", $output_arr, $input_params, "finish_success_2");
        $this->multiple_keys[] = "all_data";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->finish_success_2($input_params);
    }

    /**
     * finish_success_2 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success_2($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success_2",
            "message" => "",
        );
        $output_fields = array(
            'mn_mst_news_id_3',
            'mn_title_3',
            'mn_description_3',
            'mn_date_3',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_news";
        $func_array["function"]["output_keys"] = array(
            'all_data',
        );
        $func_array["function"]["output_alias"] = array(
            "mn_mst_news_id_3" => "mn_mst_news_id",
            "mn_title_3" => "mn_title",
            "mn_description_3" => "mn_description",
            "mn_date_3" => "mn_date",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success_2", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
