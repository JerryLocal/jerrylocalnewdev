<?php
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Description of EmailInvoice Controller
 *
 * @module EmailInvoice
 *
 * @class email_invoice.php
 *
 * @path applicationront\webservice\controllers\email_invoice.php
 *
 * @author CIT Dev Team
 *
 * @date 24.02.2016
 */

class Email_invoice extends HB_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_email_invoice');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE)
    {
        $validation_arr = array(
            "oid" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "oid_required",
                    "message" => "Please enter a value for the oid field.",
                )
            )
        );
        try
        {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "email_invoice");
            if ($validation_res["success"] == "-5")
            {
                if ($inner_api === TRUE)
                {
                    return $validation_res;
                }
                else
                {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_email_invoice->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "main_order");
            $output_response = $this->main_order($input_params);
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * main_order method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function main_order($input_params = array())
    {

        $output_arr = $this->model_email_invoice->main_order($input_params, $this->settings_params);
        $input_params["main_order"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("main_order", $output_arr, $input_params, "condition");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "main_order";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array())
    {

        $output_arr = $this->model_email_invoice->condition($input_params);
        if ($output_arr["success"])
        {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "email_notification");
            return $this->email_notification($input_params);
        }
        else
        {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mst_order_finish_success_1");
            return $this->mst_order_finish_success_1($input_params);
        }
    }

    /**
     * email_notification method is used to process email notification.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function email_notification($input_params = array())
    {

        $output_arr = $this->model_email_invoice->email_notification($input_params);
        $input_params["email_notification"] = $output_arr["success"];
        $this->wsresponse->pushDebugParams("email_notification", $output_arr, $input_params, "mst_order_finish_success");

        return $this->mst_order_finish_success($input_params);
    }

    /**
     * mst_order_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_order_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "mst_order_finish_success",
            "message" => "Your Mail Send Successfully",
        );
        $output_fields = array(
            'mo_payment_status',
            'mso_mst_sub_order_id',
            'mso_product_name',
            'mso_product_qty',
            'mso_shipper_name',
            'mso_tracking_number',
            'mso_delivered_date',
            'msd_store_name',
            'ms_state',
            'ms1_url',
            'Total_price',
            'mso_product_regular_price_1',
            'mso_item_status_1',
            'mo_buyer_email',
            'mo_buyer_name',
            'full_sub_ord_id',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "email_invoice";
        $func_array["function"]["output_keys"] = array(
            'main_order',
        );
        $func_array["function"]["output_alias"] = array(
            "mo_payment_status" => "mo_payment_status",
            "mso_mst_sub_order_id" => "mso_mst_sub_order_id",
            "mso_product_name" => "mso_product_name",
            "mso_product_qty" => "mso_product_qty",
            "mso_shipper_name" => "mso_shipper_name",
            "mso_tracking_number" => "mso_tracking_number",
            "mso_delivered_date" => "mso_delivered_date",
            "msd_store_name" => "msd_store_name",
            "ms_state" => "ms_state",
            "ms1_url" => "ms1_url",
            "Total_price" => "Total_price",
            "mso_product_regular_price_1" => "mso_product_regular_price_1",
            "mso_item_status_1" => "mso_item_status_1",
            "mo_buyer_email" => "mo_buyer_email",
            "mo_buyer_name" => "mo_buyer_name",
            "full_sub_ord_id" => "full_sub_ord_id",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_order_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_order_finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_order_finish_success_1($input_params = array())
    {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_order_finish_success_1",
            "message" => "Error while Email you",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "email_invoice";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_order_finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
