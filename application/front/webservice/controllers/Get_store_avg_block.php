<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getStoreAvgBlock Controller
 *
 * @module getStoreAvgBlock
 *
 * @class get_store_avg_block.php
 *
 * @path applicationront\webservice\controllers\get_store_avg_block.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Get_store_avg_block extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_store_avg_block');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "iStoreId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iStoreId_required",
                    "message" => "Please enter a value for the iStoreId field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_store_avg_block");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_store_avg_block->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "query");
            $output_response = $this->query($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array()) {

        $output_arr = $this->model_get_store_avg_block->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "start_loop");
        $this->multiple_keys[] = "query";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->start_loop($input_params);
    }

    /**
     * start_loop method is used to process loop flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function start_loop($input_params = array()) {
        $this->iterate_start_loop($input_params["query"], $input_params);

        $this->wsresponse->pushDebugParams("end_loop", array(), $input_params, "custom_function", "query");
        return $this->custom_function($input_params);
    }

    /**
     * rating5 method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rating5($input_params = array()) {

        $output_arr = $this->model_get_store_avg_block->rating5($input_params, $this->settings_params);
        $input_params["rating5"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("rating5", $output_arr, $input_params, "rating4", "query");

        return $this->rating4($input_params);
    }

    /**
     * rating4 method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rating4($input_params = array()) {

        $output_arr = $this->model_get_store_avg_block->rating4($input_params, $this->settings_params);
        $input_params["rating4"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("rating4", $output_arr, $input_params, "rating3", "query");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);

        return $this->rating3($input_params);
    }

    /**
     * rating3 method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rating3($input_params = array()) {

        $output_arr = $this->model_get_store_avg_block->rating3($input_params, $this->settings_params);
        $input_params["rating3"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("rating3", $output_arr, $input_params, "rating2", "query");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);

        return $this->rating2($input_params);
    }

    /**
     * rating2 method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rating2($input_params = array()) {

        $output_arr = $this->model_get_store_avg_block->rating2($input_params, $this->settings_params);
        $input_params["rating2"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("rating2", $output_arr, $input_params, "rating1", "query");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);

        return $this->rating1($input_params);
    }

    /**
     * rating1 method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function rating1($input_params = array()) {

        $output_arr = $this->model_get_store_avg_block->rating1($input_params, $this->settings_params);
        $input_params["rating1"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("rating1", $output_arr, $input_params, "custom_function", "query", "end_loop");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);

        return $input_params;
    }

    /**
     * custom_function method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function custom_function($input_params = array()) {
        if (method_exists($this->general, "getStoreAveragelRating")) {
            $result_arr["data"] = $this->general->getStoreAveragelRating($input_params);
            $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["custom_function"] = $this->wsresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "custom_function";
            $this->wsresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->wsresponse->pushDebugParams("custom_function", $result_arr, $input_params, "finish_success");
        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "",
        );
        $output_fields = array(
            'mp_mst_products_id',
            'rating5',
            'msd_trn_product_rating_id_1',
            'rating4',
            'msd_trn_product_rating_id',
            'rating3',
            'msd_trn_product_rating_id_2',
            'rating2',
            'msd_trn_product_rating_id_3',
            'rating1',
            'msd_trn_product_rating_id_4',
            'rating',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_store_avg_block";
        $func_array["function"]["output_keys"] = array(
            'query',
            'custom_function',
        );
        $func_array["function"]["output_alias"] = array(
            "mp_mst_products_id" => "mp_mst_products_id",
            "rating5" => "rating5",
            "msd_trn_product_rating_id_1" => "msd_trn_product_rating_5",
            "rating4" => "rating4",
            "msd_trn_product_rating_id" => "msd_trn_product_rating_4",
            "rating3" => "rating3",
            "msd_trn_product_rating_id_2" => "msd_trn_product_rating_3",
            "rating2" => "rating2",
            "msd_trn_product_rating_id_3" => "msd_trn_product_rating_2",
            "rating1" => "rating1",
            "msd_trn_product_rating_id_4" => "msd_trn_product_rating_1",
            "rating" => "rating",
        );
        $func_array["function"]["inner_keys"] = array(
            'rating5',
            'rating4',
            'rating3',
            'rating2',
            'rating1',
        );
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * iterate_start_loop method is used to iterate loop.
     *
     * @param array $query_lp_arr query_lp_arr array to iterate loop.
     * @param array $input_params_addr $input_params_addr array to address original input params.
     */
    public function iterate_start_loop(&$query_lp_arr = array(), &$input_params_addr = array()) {

        $_loop_params_tmp = $_loop_tmp_dict = array();
        $input_params_loc = $input_params_addr;
        $_loop_params_loc = $query_lp_arr;
        $_lp_ini = 0;
        $_lp_end = count($_loop_params_loc);
        $_lp_cnf = (is_array($_loop_params_loc[0])) ? TRUE : FALSE;
        $_loop_debug_tmp = array(
            "start_point" => $_lp_ini,
            "end_point" => $_lp_end,
            "step" => 1,
            "loop" => "query",
        );
        $this->wsresponse->pushDebugParams("start_loop", $_loop_debug_tmp, $input_params, "rating5");
        for ($i = $_lp_ini; $i < $_lp_end; $i += 1) {
            $query_lp_pms = $input_params_loc;
            if ($_lp_cnf) {
                if (is_array($_loop_params_loc[$i])) {
                    $query_lp_pms = $_loop_params_loc[$i]+$input_params_loc;
                }
                unset($query_lp_pms["query"]);
            } else {
                $query_lp_pms["query"] = $_loop_params_loc[$i];
            }
            $query_lp_pms["i"] = $i;
            $query_lp_pms["__dictionaries"] = $_loop_tmp_dict;
            $input_params = $query_lp_pms;
            $input_params = $this->rating5($input_params);
            if (is_array($input_params["__dictionaries"])) {
                $_loop_tmp_dict = $input_params["__dictionaries"];
                unset($input_params["__dictionaries"]);
            }
            if (is_array($input_params["__variables"])) {
                $input_params = $this->wsresponse->grabLoopVariables($input_params["__variables"], $input_params);
                unset($input_params["__variables"]);
            }
            if ($this->break_continue == 1) {
                $this->break_continue = NULL;
                break;
            } elseif ($this->break_continue == 2) {
                $this->break_continue = NULL;
                continue;
            }
            if ($_lp_cnf) {
                $query_lp_arr[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $query_lp_pms);
            } else {
                $_loop_params_tmp[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $query_lp_pms);
            }
        }
        if (!$_lp_cnf) {
            $input_params_addr["query"] = $_loop_params_tmp;
        }
        if (is_array($_loop_tmp_dict)) {
            $input_params_addr = array_merge($input_params_addr, $_loop_tmp_dict);
        }
    }
}
