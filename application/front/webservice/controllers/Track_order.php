<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of trackOrder Controller
 *
 * @module trackOrder
 *
 * @class track_order.php
 *
 * @path applicationront\webservice\controllers\track_order.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Track_order extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_track_order');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "iOrderId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iOrderId_required",
                    "message" => "Please enter a value for the iOrderId field.",
                )
            ),
            "vEmailId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vEmailId_required",
                    "message" => "Please enter a value for the vEmailId field.",
                ),
                array(
                    "rule" => "email",
                    "value" => TRUE,
                    "message_code" => "vEmailId_email",
                    "message" => "Please enter valid email address for the vEmailId field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "track_order");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_track_order->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "query");
            $output_response = $this->query($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array()) {

        $output_arr = $this->model_track_order->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "condition");
        $this->multiple_keys[] = "query";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_track_order->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "finish_success");
            return $this->finish_success($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "sub_order_data");
            return $this->sub_order_data($input_params);
        }
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "",
        );
        $output_fields = array(
            'mso_product_name',
            'mso_item_status',
            'mso_mst_sub_order_id',
            'mo_mst_order_id',
            'mo_buyer_email',
            'mso_tracking_number',
            'ms_url',
            'mso_shipper_name',
            'mso_shipping_date',
            'mo_order_number',
            'mo_order_number_pre',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "track_order";
        $func_array["function"]["output_keys"] = array(
            'query',
        );
        $func_array["function"]["output_alias"] = array(
            "mso_product_name" => "mso_product_name",
            "mso_item_status" => "mso_item_status",
            "mso_mst_sub_order_id" => "mso_mst_sub_order_id",
            "mo_mst_order_id" => "mo_mst_order_id",
            "mo_buyer_email" => "mo_buyer_email",
            "mso_tracking_number" => "mso_tracking_number",
            "ms_url" => "ms_url",
            "mso_shipper_name" => "mso_shipper_name",
            "mso_shipping_date" => "mso_shipping_date",
            "mo_order_number" => "mo_order_number",
            "mo_order_number_pre" => "mo_order_number_pre",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * sub_order_data method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function sub_order_data($input_params = array()) {

        $output_arr = $this->model_track_order->sub_order_data($input_params, $this->settings_params);
        $input_params["sub_order_data"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("sub_order_data", $output_arr, $input_params, "condition_1");
        $this->multiple_keys[] = "sub_order_data";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->condition_1($input_params);
    }

    /**
     * condition_1 method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition_1($input_params = array()) {

        $output_arr = $this->model_track_order->condition_1($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition_1", $output_arr, $input_params, "mst_order_finish_success");
            return $this->mst_order_finish_success($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition_1", $output_arr, $input_params, "mst_order_finish_success_1");
            return $this->mst_order_finish_success_1($input_params);
        }
    }

    /**
     * mst_order_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_order_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "mst_order_finish_success",
            "message" => "Sub Order Found",
        );
        $output_fields = array(
            'mso_mst_sub_order_id_1',
            'mso_mst_order_id',
            'mso_product_name_1',
            'mso_item_status_1',
            'mso_shipper_name_1',
            'mso_shipping_date_1',
            'mso_tracking_number_1',
            'mso_sub_order_number_pre',
            'mso_sub_order_number',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "track_order";
        $func_array["function"]["output_keys"] = array(
            'sub_order_data',
        );
        $func_array["function"]["output_alias"] = array(
            "mso_mst_sub_order_id_1" => "mso_mst_sub_order_id",
            "mso_mst_order_id" => "mso_mst_order_id",
            "mso_product_name_1" => "mso_product_name",
            "mso_item_status_1" => "mso_item_status",
            "mso_shipper_name_1" => "mso_shipper_name",
            "mso_shipping_date_1" => "mso_shipping_date",
            "mso_tracking_number_1" => "mso_tracking_number",
            "mso_sub_order_number_pre" => "mso_sub_order_number_pre",
            "mso_sub_order_number" => "mso_sub_order_number",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_order_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_order_finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_order_finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_order_finish_success_1",
            "message" => "",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "track_order";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_order_finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
