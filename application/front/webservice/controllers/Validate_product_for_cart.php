<?php
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Description of validate product for cart Controller
 *
 * @module validate product for cart
 *
 * @class validate_product_for_cart.php
 *
 * @path applicationront\webservice\controllers\validate_product_for_cart.php
 *
 * @author CIT Dev Team
 *
 * @date 24.12.2015
 */

class Validate_product_for_cart extends HB_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_validate_product_for_cart');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE)
    {
        $validation_arr = array(
            "iProductId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iProductId_required",
                    "message" => "Please enter a value for the iProductId field.",
                )
            ),
            "iQuantity" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iQuantity_required",
                    "message" => "Please enter a value for the iQuantity field.",
                )
            )
        );
        try
        {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "validate_product_for_cart");
            if ($validation_res["success"] == "-5")
            {
                if ($inner_api === TRUE)
                {
                    return $validation_res;
                }
                else
                {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_validate_product_for_cart->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "query");
            $output_response = $this->query($input_params);
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array())
    {

        $output_arr = $this->model_validate_product_for_cart->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "condition");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array())
    {

        $output_arr = $this->model_validate_product_for_cart->condition($input_params);
        if ($output_arr["success"])
        {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "admincommission");
            return $this->admincommission($input_params);
        }
        else
        {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mst_products_finish_success");
            return $this->mst_products_finish_success($input_params);
        }
    }

    /**
     * admincommission method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function admincommission($input_params = array())
    {

        $output_arr = $this->model_validate_product_for_cart->admincommission($input_params, $this->settings_params);
        $input_params["admincommission"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("admincommission", $output_arr, $input_params, "query_1");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "admincommission";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->query_1($input_params);
    }

    /**
     * query_1 method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query_1($input_params = array())
    {

        $output_arr = $this->model_validate_product_for_cart->query_1($input_params, $this->settings_params);
        $input_params["query_1"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query_1", $output_arr, $input_params, "condition_1");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query_1";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->condition_1($input_params);
    }

    /**
     * condition_1 method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition_1($input_params = array())
    {

        $output_arr = $this->model_validate_product_for_cart->condition_1($input_params);
        if ($output_arr["success"])
        {
            $this->wsresponse->pushDebugParams("condition_1", $output_arr, $input_params, "condition_2");
            return $this->condition_2($input_params);
        }
        else
        {
            $this->wsresponse->pushDebugParams("condition_1", $output_arr, $input_params, "mst_products_finish_success_2");
            return $this->mst_products_finish_success_2($input_params);
        }
    }

    /**
     * condition_2 method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition_2($input_params = array())
    {

        $output_arr = $this->model_validate_product_for_cart->condition_2($input_params);
        if ($output_arr["success"])
        {
            $this->wsresponse->pushDebugParams("condition_2", $output_arr, $input_params, "mst_products_finish_success_1");
            return $this->mst_products_finish_success_1($input_params);
        }
        else
        {
            $this->wsresponse->pushDebugParams("condition_2", $output_arr, $input_params, "mst_products_finish_success_3");
            return $this->mst_products_finish_success_3($input_params);
        }
    }

    /**
     * mst_products_finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_products_finish_success_1($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "mst_products_finish_success_1",
            "message" => "Product detial",
        );
        $output_fields = array(
            'mp_mst_products_id',
            'mp_store_id',
            'mp_title',
            'mp_sku',
            'mp_regular_price',
            'mp_sale_price',
            'mp_rating_avg',
            'mp_allow_max_purchase',
            'mp_low_avl_limit_notifcation',
            'mp_stock',
            'mp_difference_per',
            'mp_total_rate',
            'mp_description',
            'mp_short_description',
            'mp_default_img',
            'tpo_option_value',
            'tpo_category_option_value',
            'msd_store_name',
            'msd_website_url',
            'mp_shipping_charge',
            'mp_free_shipping',
            'mp_return_days',
            'mc_commission',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "validate_product_for_cart";
        $func_array["function"]["output_keys"] = array(
            'query',
            'query_1',
        );
        $func_array["function"]["output_alias"] = array(
            "mp_mst_products_id" => "mp_mst_products_id",
            "mp_store_id" => "mp_store_id",
            "mp_title" => "mp_title",
            "mp_sku" => "mp_sku",
            "mp_regular_price" => "mp_regular_price",
            "mp_sale_price" => "mp_sale_price",
            "mp_rating_avg" => "mp_rating_avg",
            "mp_allow_max_purchase" => "mp_allow_max_purchase",
            "mp_low_avl_limit_notifcation" => "mp_low_avl_limit_notifcation",
            "mp_stock" => "mp_stock",
            "mp_difference_per" => "mp_difference_per",
            "mp_total_rate" => "mp_total_rate",
            "mp_description" => "mp_description",
            "mp_short_description" => "mp_short_description",
            "mp_default_img" => "mp_default_img",
            "tpo_option_value" => "tpo_option_value",
            "tpo_category_option_value" => "tpo_category_option_value",
            "msd_store_name" => "msd_store_name",
            "msd_website_url" => "msd_website_url",
            "mp_shipping_charge" => "mp_shipping_charge",
            "mp_free_shipping" => "mp_free_shipping",
            "mp_return_days" => "mp_return_days",
            "mc_commission" => "mc_commission",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_products_finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_products_finish_success_3 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_products_finish_success_3($input_params = array())
    {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_products_finish_success_3",
            "message" => "Store didn't allow to buy  #iQuantity#  quantity at a time.",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "validate_product_for_cart";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_products_finish_success_3", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_products_finish_success_2 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_products_finish_success_2($input_params = array())
    {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_products_finish_success_2",
            "message" => "Requested quantity or item not in stock!",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "validate_product_for_cart";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_products_finish_success_2", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_products_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_products_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_products_finish_success",
            "message" => "Requested quantity or item not in stock!",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "validate_product_for_cart";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_products_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
