<?php
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Description of insProductTrend Controller
 *
 * @module insProductTrend
 *
 * @class ins_product_trend.php
 *
 * @path applicationront\webservice\controllers\ins_product_trend.php
 *
 * @author CIT Dev Team
 *
 * @date 11.01.2016
 */

class Ins_product_trend extends HB_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_ins_product_trend');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE)
    {
        $validation_arr = array(
            "pid" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "pid_required",
                    "message" => "Please enter a value for the pid field.",
                )
            )
        );
        try
        {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "ins_product_trend");
            if ($validation_res["success"] == "-5")
            {
                if ($inner_api === TRUE)
                {
                    return $validation_res;
                }
                else
                {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_ins_product_trend->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "checkproductinviewcount");
            $output_response = $this->checkproductinviewcount($input_params);
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * checkproductinviewcount method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function checkproductinviewcount($input_params = array())
    {

        $output_arr = $this->model_ins_product_trend->checkproductinviewcount($input_params, $this->settings_params);
        $input_params["checkproductinviewcount"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("checkproductinviewcount", $output_arr, $input_params, "chk_count");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "checkproductinviewcount";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->chk_count($input_params);
    }

    /**
     * chk_count method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function chk_count($input_params = array())
    {

        $output_arr = $this->model_ins_product_trend->chk_count($input_params);
        if ($output_arr["success"])
        {
            $this->wsresponse->pushDebugParams("chk_count", $output_arr, $input_params, "query");
            return $this->query($input_params);
        }
        else
        {
            $this->wsresponse->pushDebugParams("chk_count", $output_arr, $input_params, "trn_view_count_finish_success");
            return $this->trn_view_count_finish_success($input_params);
        }
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array())
    {

        $output_arr = $this->model_ins_product_trend->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "update_view");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->update_view($input_params);
    }

    /**
     * update_view method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function update_view($input_params = array())
    {

        $output_arr = $this->model_ins_product_trend->update_view($input_params, $this->settings_params);
        $input_params["update_view"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("update_view", $output_arr, $input_params, "finish_success");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "update_view";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "View Updated",
        );
        $output_fields = array(
            'tvc_trn_view_count_id',
            'insert_id',
            'affected_rows1',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "ins_product_trend";
        $func_array["function"]["output_keys"] = array(
            'checkproductinviewcount',
            'query',
            'update_view',
        );
        $func_array["function"]["output_alias"] = array(
            "tvc_trn_view_count_id" => "tvc_trn_view_count_id",
            "insert_id" => "insert_id",
            "affected_rows1" => "affected_rows1",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * trn_view_count_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function trn_view_count_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "trn_view_count_finish_success",
            "message" => "Already Exist.",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "ins_product_trend";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("trn_view_count_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
