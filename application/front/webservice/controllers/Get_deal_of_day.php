<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getDealOfDay Controller
 *
 * @module getDealOfDay
 *
 * @class get_deal_of_day.php
 *
 * @path applicationront\webservice\controllers\get_deal_of_day.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Get_deal_of_day extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_deal_of_day');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array();
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_deal_of_day");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_deal_of_day->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "deal_of_the_day");
            $output_response = $this->deal_of_the_day($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * deal_of_the_day method is used to process query block.
     * Get Daily deals
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function deal_of_the_day($input_params = array()) {

        $output_arr = $this->model_get_deal_of_day->deal_of_the_day($input_params, $this->settings_params);
        $input_params["deal_of_the_day"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("deal_of_the_day", $output_arr, $input_params, "chk_has_deal");
        $this->multiple_keys[] = "deal_of_the_day";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->chk_has_deal($input_params);
    }

    /**
     * chk_has_deal method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function chk_has_deal($input_params = array()) {

        $output_arr = $this->model_get_deal_of_day->chk_has_deal($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("chk_has_deal", $output_arr, $input_params, "start_loop");
            return $this->start_loop($input_params);
        } else {
            $this->wsresponse->pushDebugParams("chk_has_deal", $output_arr, $input_params, "no_deals");
            return $this->no_deals($input_params);
        }
    }

    /**
     * start_loop method is used to process loop flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function start_loop($input_params = array()) {
        $this->iterate_start_loop($input_params["deal_of_the_day"], $input_params);

        $this->wsresponse->pushDebugParams("end_loop", array(), $input_params, "has_deals", "deal_of_the_day");
        return $this->has_deals($input_params);
    }

    /**
     * get_product_details method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_product_details($input_params = array()) {

        $output_arr = $this->model_get_deal_of_day->get_product_details($input_params, $this->settings_params);
        $input_params["get_product_details"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_product_details", $output_arr, $input_params, "has_deals", "deal_of_the_day", "end_loop");

        return $input_params;
    }

    /**
     * has_deals method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function has_deals($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "has_deals",
            "message" => "Product Found.",
        );
        $output_fields = array(
            'td_date',
            'td_mst_products_id',
            'get_product_details',
            'mp_title',
            'mp_sale_price',
            'mp_regular_price',
            'mp_rating_avg',
            'mp_difference_per',
            'mp_total_rate',
            'mp_date',
            'mp_default_img',
            'mp_wishlist_state',
            'mp_sale_state',
            'mp_stock',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_deal_of_day";
        $func_array["function"]["output_keys"] = array(
            'deal_of_the_day',
        );
        $func_array["function"]["output_alias"] = array(
            "td_date" => "td_date",
            "td_mst_products_id" => "td_mst_products_id",
            "get_product_details" => "get_product_details",
            "mp_title" => "mp_title",
            "mp_sale_price" => "mp_sale_price",
            "mp_regular_price" => "mp_regular_price",
            "mp_rating_avg" => "mp_rating_avg",
            "mp_difference_per" => "mp_difference_per",
            "mp_total_rate" => "mp_total_rate",
            "mp_date" => "mp_date",
            "mp_default_img" => "mp_default_img",
            "mp_wishlist_state" => "mp_wishlist_state",
            "mp_sale_state" => "mp_sale_state",
            "mp_stock" => "mp_stock",
        );
        $func_array["function"]["inner_keys"] = array(
            'get_product_details',
        );
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("has_deals", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * no_deals method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function no_deals($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "no_deals",
            "message" => "Oops",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_deal_of_day";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("no_deals", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * iterate_start_loop method is used to iterate loop.
     *
     * @param array $deal_of_the_day_lp_arr deal_of_the_day_lp_arr array to iterate loop.
     * @param array $input_params_addr $input_params_addr array to address original input params.
     */
    public function iterate_start_loop(&$deal_of_the_day_lp_arr = array(), &$input_params_addr = array()) {

        $_loop_params_tmp = $_loop_tmp_dict = array();
        $input_params_loc = $input_params_addr;
        $_loop_params_loc = $deal_of_the_day_lp_arr;
        $_lp_ini = 0;
        $_lp_end = count($_loop_params_loc);
        $_lp_cnf = (is_array($_loop_params_loc[0])) ? TRUE : FALSE;
        $_loop_debug_tmp = array(
            "start_point" => $_lp_ini,
            "end_point" => $_lp_end,
            "step" => 1,
            "loop" => "deal_of_the_day",
        );
        $this->wsresponse->pushDebugParams("start_loop", $_loop_debug_tmp, $input_params, "get_product_details");
        for ($i = $_lp_ini; $i < $_lp_end; $i += 1) {
            $deal_of_the_day_lp_pms = $input_params_loc;
            if ($_lp_cnf) {
                if (is_array($_loop_params_loc[$i])) {
                    $deal_of_the_day_lp_pms = $_loop_params_loc[$i]+$input_params_loc;
                }
                unset($deal_of_the_day_lp_pms["deal_of_the_day"]);
            } else {
                $deal_of_the_day_lp_pms["deal_of_the_day"] = $_loop_params_loc[$i];
            }
            $deal_of_the_day_lp_pms["i"] = $i;
            $deal_of_the_day_lp_pms["__dictionaries"] = $_loop_tmp_dict;
            $input_params = $deal_of_the_day_lp_pms;
            $input_params = $this->get_product_details($input_params);
            if (is_array($input_params["__dictionaries"])) {
                $_loop_tmp_dict = $input_params["__dictionaries"];
                unset($input_params["__dictionaries"]);
            }
            if (is_array($input_params["__variables"])) {
                $input_params = $this->wsresponse->grabLoopVariables($input_params["__variables"], $input_params);
                unset($input_params["__variables"]);
            }
            if ($this->break_continue == 1) {
                $this->break_continue = NULL;
                break;
            } elseif ($this->break_continue == 2) {
                $this->break_continue = NULL;
                continue;
            }
            if ($_lp_cnf) {
                $deal_of_the_day_lp_arr[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $deal_of_the_day_lp_pms);
            } else {
                $_loop_params_tmp[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $deal_of_the_day_lp_pms);
            }
        }
        if (!$_lp_cnf) {
            $input_params_addr["deal_of_the_day"] = $_loop_params_tmp;
        }
        if (is_array($_loop_tmp_dict)) {
            $input_params_addr = array_merge($input_params_addr, $_loop_tmp_dict);
        }
    }
}
