<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getOrderDetail_v1 Controller
 *
 * @module getOrderDetail_v1
 *
 * @class get_order_detail_v1.php
 *
 * @path applicationront\webservice\controllers\get_order_detail_v1.php
 *
 * @author Steve Smith
 *
 * @date 07.12.2015
 */

class Get_order_detail_v1 extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_order_detail_v1');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "oid" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "oid_required",
                    "message" => "Please enter a value for the oid field.",
                )
            ),
            "iBuyerId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iBuyerId_required",
                    "message" => "Please enter a value for the iBuyerId field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_order_detail_v1");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_order_detail_v1->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "main_order");
            $output_response = $this->main_order($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * main_order method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function main_order($input_params = array()) {

        $output_arr = $this->model_get_order_detail_v1->main_order($input_params, $this->settings_params);
        $input_params["main_order"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("main_order", $output_arr, $input_params, "condition");
        $this->multiple_keys[] = "main_order";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_get_order_detail_v1->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "start_loop");
            return $this->start_loop($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mst_order_finish_success");
            return $this->mst_order_finish_success($input_params);
        }
    }

    /**
     * start_loop method is used to process loop flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function start_loop($input_params = array()) {
        $this->iterate_start_loop($input_params["main_order"], $input_params);

        $this->wsresponse->pushDebugParams("end_loop", array(), $input_params, "custom_function", "main_order");
        return $this->custom_function($input_params);
    }

    /**
     * order_history method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function order_history($input_params = array()) {

        $output_arr = $this->model_get_order_detail_v1->order_history($input_params, $this->settings_params);
        $input_params["order_history"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("order_history", $output_arr, $input_params, "custom_function", "main_order", "end_loop");

        return $input_params;
    }

    /**
     * custom_function method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function custom_function($input_params = array()) {
        if (method_exists($this->general, "checkOrderStatus")) {
            $result_arr["data"] = $this->general->checkOrderStatus($input_params);
            $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["custom_function"] = $this->wsresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "custom_function";
            $this->wsresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->wsresponse->pushDebugParams("custom_function", $result_arr, $input_params, "finish_success");
        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "",
        );
        $output_fields = array(
            'mo_mst_order_id',
            'mo_buyer_pin_code',
            'mo_buyer_area',
            'mo_buyer_address1',
            'mo_buyer_name',
            'mo_date',
            'mo_payment_status',
            'mo_shipping_cost',
            'mo_sub_total',
            'mo_order_total',
            'mo_buyer_id',
            'mo_buyer_email',
            'mo_buyer_ip',
            'mso_mst_sub_order_id',
            'mso_mst_store_detail_id',
            'mso_product_name',
            'mso_product_regular_price',
            'mso_product_sale_price',
            'mso_product_qty',
            'mso_item_status',
            'mso_shipping_cost',
            'mso_total_cost',
            'mso_shipper_name',
            'mso_tracking_number',
            'mso_delivered_date',
            'mc_country',
            'mp_difference_per',
            'mp_default_img',
            'msd_store_name',
            'ms_state',
            'mc1_city',
            'ms1_url',
            'return_ord',
            'cancel_ord',
            'mso_return_period',
            'Discount_price',
            'mso_mst_products_id',
            'mso_product_option',
            'invoice_ord',
            'mo_buyer_address2',
            'mo_buyer_phone',
            'mo_buyer_country_id',
            'mo_buyer_state_id',
            'mo_buyer_city_id',
            'mp_do_return',
            'order_history',
            'tosh_date',
            'tosh_detail',
            'tosh_order_status',
            'minstatus',
            'Discount',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_order_detail_v1";
        $func_array["function"]["output_keys"] = array(
            'main_order',
            'custom_function',
        );
        $func_array["function"]["output_alias"] = array(
            "mo_mst_order_id" => "mo_mst_order_id",
            "mo_buyer_pin_code" => "mo_buyer_pin_code",
            "mo_buyer_area" => "mo_buyer_area",
            "mo_buyer_address1" => "mo_buyer_address1",
            "mo_buyer_name" => "mo_buyer_name",
            "mo_date" => "mo_date",
            "mo_payment_status" => "mo_payment_status",
            "mo_shipping_cost" => "mo_shipping_cost",
            "mo_sub_total" => "mo_sub_total",
            "mo_order_total" => "mo_order_total",
            "mo_buyer_id" => "mo_buyer_id",
            "mo_buyer_email" => "mo_buyer_email",
            "mo_buyer_ip" => "mo_buyer_ip",
            "mso_mst_sub_order_id" => "mso_mst_sub_order_id",
            "mso_mst_store_detail_id" => "mso_mst_store_detail_id",
            "mso_product_name" => "mso_product_name",
            "mso_product_regular_price" => "mso_product_regular_price",
            "mso_product_sale_price" => "mso_product_sale_price",
            "mso_product_qty" => "mso_product_qty",
            "mso_item_status" => "mso_item_status",
            "mso_shipping_cost" => "mso_shipping_cost",
            "mso_total_cost" => "mso_total_cost",
            "mso_shipper_name" => "mso_shipper_name",
            "mso_tracking_number" => "mso_tracking_number",
            "mso_delivered_date" => "mso_delivered_date",
            "mc_country" => "mc_country",
            "mp_difference_per" => "mp_difference_per",
            "mp_default_img" => "mp_default_img",
            "msd_store_name" => "msd_store_name",
            "ms_state" => "ms_state",
            "mc1_city" => "mc1_city",
            "ms1_url" => "ms1_url",
            "return_ord" => "return_ord",
            "cancel_ord" => "cancel_ord",
            "mso_return_period" => "mso_return_period",
            "Discount_price" => "Discount_price",
            "mso_mst_products_id" => "mso_mst_products_id",
            "mso_product_option" => "mso_product_option",
            "invoice_ord" => "invoice_ord",
            "mo_buyer_address2" => "mo_buyer_address2",
            "mo_buyer_phone" => "mo_buyer_phone",
            "mo_buyer_country_id" => "mo_buyer_country_id",
            "mo_buyer_state_id" => "mo_buyer_state_id",
            "mo_buyer_city_id" => "mo_buyer_city_id",
            "mp_do_return" => "mp_do_return",
            "order_history" => "order_history",
            "tosh_date" => "tosh_date",
            "tosh_detail" => "tosh_detail",
            "tosh_order_status" => "tosh_order_status",
            "minstatus" => "minstatus",
            "Discount" => "Discount",
        );
        $func_array["function"]["inner_keys"] = array(
            'order_history',
        );
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_order_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_order_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_order_finish_success",
            "message" => "No Order Available",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_order_detail_v1";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_order_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * iterate_start_loop method is used to iterate loop.
     *
     * @param array $main_order_lp_arr main_order_lp_arr array to iterate loop.
     * @param array $input_params_addr $input_params_addr array to address original input params.
     */
    public function iterate_start_loop(&$main_order_lp_arr = array(), &$input_params_addr = array()) {

        $_loop_params_tmp = $_loop_tmp_dict = array();
        $input_params_loc = $input_params_addr;
        $_loop_params_loc = $main_order_lp_arr;
        $_lp_ini = 0;
        $_lp_end = count($_loop_params_loc);
        $_lp_cnf = (is_array($_loop_params_loc[0])) ? TRUE : FALSE;
        $_loop_debug_tmp = array(
            "start_point" => $_lp_ini,
            "end_point" => $_lp_end,
            "step" => 1,
            "loop" => "main_order",
        );
        $this->wsresponse->pushDebugParams("start_loop", $_loop_debug_tmp, $input_params, "order_history");
        for ($i = $_lp_ini; $i < $_lp_end; $i += 1) {
            $main_order_lp_pms = $input_params_loc;
            if ($_lp_cnf) {
                if (is_array($_loop_params_loc[$i])) {
                    $main_order_lp_pms = $_loop_params_loc[$i]+$input_params_loc;
                }
                unset($main_order_lp_pms["main_order"]);
            } else {
                $main_order_lp_pms["main_order"] = $_loop_params_loc[$i];
            }
            $main_order_lp_pms["i"] = $i;
            $main_order_lp_pms["__dictionaries"] = $_loop_tmp_dict;
            $input_params = $main_order_lp_pms;
            $input_params = $this->order_history($input_params);
            if (is_array($input_params["__dictionaries"])) {
                $_loop_tmp_dict = $input_params["__dictionaries"];
                unset($input_params["__dictionaries"]);
            }
            if (is_array($input_params["__variables"])) {
                $input_params = $this->wsresponse->grabLoopVariables($input_params["__variables"], $input_params);
                unset($input_params["__variables"]);
            }
            if ($this->break_continue == 1) {
                $this->break_continue = NULL;
                break;
            } elseif ($this->break_continue == 2) {
                $this->break_continue = NULL;
                continue;
            }
            if ($_lp_cnf) {
                $main_order_lp_arr[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $main_order_lp_pms);
            } else {
                $_loop_params_tmp[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $main_order_lp_pms);
            }
        }
        if (!$_lp_cnf) {
            $input_params_addr["main_order"] = $_loop_params_tmp;
        }
        if (is_array($_loop_tmp_dict)) {
            $input_params_addr = array_merge($input_params_addr, $_loop_tmp_dict);
        }
    }
}
