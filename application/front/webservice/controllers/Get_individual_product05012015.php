<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getIndividualProduct Controller
 *
 * @module getIndividualProduct
 *
 * @class get_individual_product.php
 *
 * @path applicationront\webservice\controllers\get_individual_product.php
 *
 * @author Steve Smith
 *
 * @date 03.12.2015
 */

class Get_individual_product extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_individual_product');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "iProductId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iProductId_required",
                    "message" => "Please enter a value for the iProductId field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_individual_product");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_individual_product->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "query");
            $output_response = $this->query($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array()) {

        $output_arr = $this->model_get_individual_product->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "product_option");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->product_option($input_params);
    }

    /**
     * product_option method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function product_option($input_params = array()) {

        $output_arr = $this->model_get_individual_product->product_option($input_params, $this->settings_params);
        $input_params["product_option"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("product_option", $output_arr, $input_params, "custom_function");
        $this->multiple_keys[] = "product_option";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->custom_function($input_params);
    }

    /**
     * custom_function method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function custom_function($input_params = array()) {
        if (method_exists($this->general, "OrderBtn")) {
            $result_arr["data"] = $this->general->OrderBtn($input_params);
            $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["custom_function"] = $this->wsresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "custom_function";
            $this->wsresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->wsresponse->pushDebugParams("custom_function", $result_arr, $input_params, "finish_success");
        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "",
        );
        $output_fields = array(
            'mp_title',
            'mp_sku',
            'mp_regular_price',
            'mp_sale_price',
            'mp_rating_avg',
            'mp_stock',
            'mp_difference_per',
            'mp_description',
            'mp_short_description',
            'mp_default_img',
            'mpi_img_path',
            'mp_mst_products_id',
            'mp_mst_store_id',
            'tpo_option_value',
            'tpo_category_option_value',
            'tpo_category_option_value_other',
            'tpo_option_value_other',
            'mp_low_avl_limit_notification',
            'mp_status',
            'fShippingCharge',
            'iReturnDays',
            'iWishlistState',
            'eFreeShipping',
            'tpo_category_option_value_1',
            'tpo_option_value_1',
            'OrderBtn',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_individual_product";
        $func_array["function"]["output_keys"] = array(
            'query',
            'product_option',
            'custom_function',
        );
        $func_array["function"]["output_alias"] = array(
            "mp_title" => "mp_title",
            "mp_sku" => "mp_sku",
            "mp_regular_price" => "mp_regular_price",
            "mp_sale_price" => "mp_sale_price",
            "mp_rating_avg" => "mp_rating_avg",
            "mp_stock" => "mp_stock",
            "mp_difference_per" => "mp_difference_per",
            "mp_description" => "mp_description",
            "mp_short_description" => "mp_short_description",
            "mp_default_img" => "mp_default_img",
            "mpi_img_path" => "mpi_img_path",
            "mp_mst_products_id" => "mp_mst_products_id",
            "mp_mst_store_id" => "mp_mst_store_id",
            "tpo_option_value" => "tpo_option_value",
            "tpo_category_option_value" => "tpo_category_option_value",
            "tpo_category_option_value_other" => "tpo_category_option_value_other",
            "tpo_option_value_other" => "tpo_option_value_other",
            "mp_low_avl_limit_notification" => "mp_low_avl_limit_notification",
            "mp_status" => "mp_status",
            "fShippingCharge" => "fShippingCharge",
            "iReturnDays" => "iReturnDays",
            "iWishlistState" => "iWishlistState",
            "eFreeShipping" => "eFreeShipping",
            "tpo_category_option_value_1" => "tpo_category_option_value_1",
            "tpo_option_value_1" => "tpo_option_value_1",
            "OrderBtn" => "OrderBtn",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
