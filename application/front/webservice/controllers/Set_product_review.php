<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of setProductReview Controller
 *
 * @module setProductReview
 *
 * @class set_product_review.php
 *
 * @path applicationront\webservice\controllers\set_product_review.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Set_product_review extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_set_product_review');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "iMstProductsId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iMstProductsId_required",
                    "message" => "Please enter a value for the iMstProductsId field.",
                )
            ),
            "iLogedUserId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iLogedUserId_required",
                    "message" => "Please enter a value for the iLogedUserId field.",
                )
            ),
            "vName" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vName_required",
                    "message" => "Please enter a value for the vName field.",
                )
            ),
            "vEmail" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vEmail_required",
                    "message" => "Please enter a value for the vEmail field.",
                )
            ),
            "iRate" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iRate_required",
                    "message" => "Please enter a value for the iRate field.",
                )
            ),
            "tReview" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "tReview_required",
                    "message" => "Please enter a value for the tReview field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "set_product_review");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_set_product_review->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "insert_query_rating");
            $output_response = $this->insert_query_rating($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * insert_query_rating method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function insert_query_rating($input_params = array()) {

        $output_arr = $this->model_set_product_review->insert_query_rating($input_params, $this->settings_params);
        $input_params["insert_query_rating"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("insert_query_rating", $output_arr, $input_params, "condition");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "insert_query_rating";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_set_product_review->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "query");
            return $this->query($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "finish_success_1");
            return $this->finish_success_1($input_params);
        }
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array()) {

        $output_arr = $this->model_set_product_review->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "query_1");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->query_1($input_params);
    }

    /**
     * query_1 method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query_1($input_params = array()) {

        $output_arr = $this->model_set_product_review->query_1($input_params, $this->settings_params);
        $input_params["query_1"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query_1", $output_arr, $input_params, "update_fratingavg");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query_1";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->update_fratingavg($input_params);
    }

    /**
     * update_fratingavg method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function update_fratingavg($input_params = array()) {

        $output_arr = $this->model_set_product_review->update_fratingavg($input_params, $this->settings_params);
        $input_params["update_fratingavg"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("update_fratingavg", $output_arr, $input_params, "finish_success");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "update_fratingavg";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "Thank You for Rating Out Product",
        );
        $output_fields = array(
            'insert_id',
            'affected_rows',
            'tpr_rate',
            'tpr_trn_product_rating_id',
            'avg',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "set_product_review";
        $func_array["function"]["output_keys"] = array(
            'insert_query_rating',
            'query',
            'query_1',
        );
        $func_array["function"]["output_alias"] = array(
            "insert_id" => "insert_id",
            "affected_rows" => "affected_rows",
            "tpr_rate" => "tpr_rate",
            "tpr_trn_product_rating_id" => "tpr_trn_product_rating_id",
            "avg" => "avg",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "finish_success_1",
            "message" => "Error While inserting the Rating ",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "set_product_review";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
