<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of quicksearchInner Controller
 *
 * @module quicksearchInner
 *
 * @class quicksearch_inner.php
 *
 * @path applicationront\webservice\controllers\quicksearch_inner.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Quicksearch_inner extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_quicksearch_inner');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array();
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "quicksearch_inner");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_quicksearch_inner->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "product_max_price_query");
            $output_response = $this->product_max_price_query($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * product_max_price_query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function product_max_price_query($input_params = array()) {

        $output_arr = $this->model_quicksearch_inner->product_max_price_query($input_params, $this->settings_params);
        $input_params["product_max_price_query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("product_max_price_query", $output_arr, $input_params, "product_search");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "product_max_price_query";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->product_search($input_params);
    }

    /**
     * product_search method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function product_search($input_params = array()) {

        $output_arr = $this->model_quicksearch_inner->product_search($input_params, $this->settings_params);
        $input_params["product_search"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("product_search", $output_arr, $input_params, "condition");
        $this->multiple_keys[] = "product_search";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_quicksearch_inner->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mst_products_finish_success_1");
            return $this->mst_products_finish_success_1($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mst_products_finish_success");
            return $this->mst_products_finish_success($input_params);
        }
    }

    /**
     * mst_products_finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_products_finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "mst_products_finish_success_1",
            "message" => "",
        );
        $output_fields = array(
            'p_sale_price',
            'mp_title',
            'mp_regular_price',
            'mp_default_img',
            'mp_mst_products_id',
            'type',
            'cleanurl_product',
            'mp_store_id',
            'mp_sale_price',
            'mp_rating_avg',
            'mp_stock',
            'mp_difference_per',
            'mp_total_rate',
            'mp_date',
            'mp_wishlist_state',
            'mp_sale_state',
            'mp_view_state',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "quicksearch_inner";
        $func_array["function"]["output_keys"] = array(
            'product_max_price_query',
            'product_search',
        );
        $func_array["function"]["output_alias"] = array(
            "p_sale_price" => "p_sale_price",
            "mp_title" => "mp_title",
            "mp_regular_price" => "mp_regular_price",
            "mp_default_img" => "mp_default_img",
            "mp_mst_products_id" => "mp_mst_products_id",
            "type" => "type",
            "cleanurl_product" => "cleanurl_product",
            "mp_store_id" => "mp_store_id",
            "mp_sale_price" => "mp_sale_price",
            "mp_rating_avg" => "mp_rating_avg",
            "mp_stock" => "mp_stock",
            "mp_difference_per" => "mp_difference_per",
            "mp_total_rate" => "mp_total_rate",
            "mp_date" => "mp_date",
            "mp_wishlist_state" => "mp_wishlist_state",
            "mp_sale_state" => "mp_sale_state",
            "mp_view_state" => "mp_view_state",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_products_finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_products_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_products_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_products_finish_success",
            "message" => "",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "quicksearch_inner";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_products_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
