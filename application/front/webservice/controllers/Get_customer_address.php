<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getCustomerAddress Controller
 *
 * @module getCustomerAddress
 *
 * @class get_customer_address.php
 *
 * @path applicationront\webservice\controllers\get_customer_address.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Get_customer_address extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_customer_address');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "iCustomerId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iCustomerId_required",
                    "message" => "Please enter a value for the iCustomerId field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_customer_address");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_customer_address->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "query");
            $output_response = $this->query($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array()) {

        $output_arr = $this->model_get_customer_address->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "condition");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_get_customer_address->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mst_buyer_address_finish_success_1");
            return $this->mst_buyer_address_finish_success_1($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mst_buyer_address_finish_success");
            return $this->mst_buyer_address_finish_success($input_params);
        }
    }

    /**
     * mst_buyer_address_finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_buyer_address_finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "mst_buyer_address_finish_success_1",
            "message" => "Default address",
        );
        $output_fields = array(
            'mba_name',
            'mba_address1',
            'mba_address2',
            'mba_area',
            'mba_country_id',
            'mba_state_id',
            'mba_city_id',
            'mba_pin_code',
            'mba_phone',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_customer_address";
        $func_array["function"]["output_keys"] = array(
            'query',
        );
        $func_array["function"]["output_alias"] = array(
            "mba_name" => "mba_name",
            "mba_address1" => "mba_address1",
            "mba_address2" => "mba_address2",
            "mba_area" => "mba_area",
            "mba_country_id" => "mba_country_id",
            "mba_state_id" => "mba_state_id",
            "mba_city_id" => "mba_city_id",
            "mba_pin_code" => "mba_pin_code",
            "mba_phone" => "mba_phone",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_buyer_address_finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_buyer_address_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_buyer_address_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_buyer_address_finish_success",
            "message" => "Please fill your delivery address.",
        );
        $output_fields = array(
            'mba_name',
            'mba_address1',
            'mba_address2',
            'mba_area',
            'mba_country_id',
            'mba_state_id',
            'mba_city_id',
            'mba_pin_code',
            'mba_phone',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_customer_address";
        $func_array["function"]["output_keys"] = array(
            'query',
        );
        $func_array["function"]["output_alias"] = array(
            "mba_name" => "mba_name",
            "mba_address1" => "mba_address1",
            "mba_address2" => "mba_address2",
            "mba_area" => "mba_area",
            "mba_country_id" => "mba_country_id",
            "mba_state_id" => "mba_state_id",
            "mba_city_id" => "mba_city_id",
            "mba_pin_code" => "mba_pin_code",
            "mba_phone" => "mba_phone",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_buyer_address_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
