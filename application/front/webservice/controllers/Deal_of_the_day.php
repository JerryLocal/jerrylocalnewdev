<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of dealOfTheDay Controller
 *
 * @module dealOfTheDay
 *
 * @class deal_of_the_day.php
 *
 * @path applicationront\webservice\controllers\deal_of_the_day.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Deal_of_the_day extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_deal_of_the_day');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array();
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "deal_of_the_day");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_deal_of_the_day->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "deal_of_the_day");
            $output_response = $this->deal_of_the_day($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * deal_of_the_day method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function deal_of_the_day($input_params = array()) {

        $output_arr = $this->model_deal_of_the_day->deal_of_the_day($input_params, $this->settings_params);
        $input_params["deal_of_the_day"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("deal_of_the_day", $output_arr, $input_params, "chk_has_deal");
        $this->multiple_keys[] = "deal_of_the_day";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->chk_has_deal($input_params);
    }

    /**
     * chk_has_deal method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function chk_has_deal($input_params = array()) {

        $output_arr = $this->model_deal_of_the_day->chk_has_deal($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("chk_has_deal", $output_arr, $input_params, "trn_dealoftheday_finish_success_2");
            return $this->trn_dealoftheday_finish_success_2($input_params);
        } else {
            $this->wsresponse->pushDebugParams("chk_has_deal", $output_arr, $input_params, "trn_dealoftheday_finish_success");
            return $this->trn_dealoftheday_finish_success($input_params);
        }
    }

    /**
     * trn_dealoftheday_finish_success_2 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function trn_dealoftheday_finish_success_2($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "trn_dealoftheday_finish_success_2",
            "message" => "",
        );
        $output_fields = array(
            'td_mst_products_id',
            'td_date',
            'mp_mst_products_id_1',
            'mp_store_id_1',
            'mp_title_1',
            'mp_sku',
            'mp_regular_price_1',
            'mp_sale_price_1',
            'mp_rating_avg_1',
            'mp_stock',
            'mp_difference_per_1',
            'mp_total_rate_1',
            'mp_date_1',
            'mp_wishlist_state_1',
            'mp_sale_state_1',
            'mp_view_state',
            'mp_default_img_1',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "deal_of_the_day";
        $func_array["function"]["output_keys"] = array(
            'deal_of_the_day',
        );
        $func_array["function"]["output_alias"] = array(
            "td_mst_products_id" => "td_mst_products_id",
            "td_date" => "td_date",
            "mp_mst_products_id_1" => "mp_mst_products_id_1",
            "mp_store_id_1" => "mp_store_id_1",
            "mp_title_1" => "mp_title_1",
            "mp_sku" => "mp_sku",
            "mp_regular_price_1" => "mp_regular_price_1",
            "mp_sale_price_1" => "mp_sale_price_1",
            "mp_rating_avg_1" => "mp_rating_avg_1",
            "mp_stock" => "mp_stock",
            "mp_difference_per_1" => "mp_difference_per_1",
            "mp_total_rate_1" => "mp_total_rate_1",
            "mp_date_1" => "mp_date_1",
            "mp_wishlist_state_1" => "mp_wishlist_state_1",
            "mp_sale_state_1" => "mp_sale_state_1",
            "mp_view_state" => "mp_view_state",
            "mp_default_img_1" => "mp_default_img_1",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("trn_dealoftheday_finish_success_2", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * trn_dealoftheday_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function trn_dealoftheday_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "trn_dealoftheday_finish_success",
            "message" => "No Deal Available",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "deal_of_the_day";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("trn_dealoftheday_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
