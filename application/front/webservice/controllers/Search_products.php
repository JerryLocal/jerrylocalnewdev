<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of searchProducts Controller
 *
 * @module searchProducts
 *
 * @class search_products.php
 *
 * @path applicationront\webservice\controllers\search_products.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Search_products extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_search_products');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array();
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "search_products");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_search_products->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "search_query");
            $output_response = $this->search_query($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * search_query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function search_query($input_params = array()) {

        $output_arr = $this->model_search_products->search_query($input_params, $this->settings_params);
        $input_params["search_query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("search_query", $output_arr, $input_params, "finish_success_search_query");
        $this->multiple_keys[] = "search_query";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->finish_success_search_query($input_params);
    }

    /**
     * finish_success_search_query method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success_search_query($input_params = array()) {

        $setting_fields = array(
            "success" => "",
            "message_code" => "finish_success_search_query",
            "message" => "",
        );
        $output_fields = array(
            'mp_mst_products_id',
            'mp_admin_id',
            'mp_store_id',
            'mp_title',
            'mp_sku',
            'mp_regular_price',
            'mp_sale_price',
            'mp_rating_avg',
            'mp_allow_max_purchase',
            'mp_low_avl_limit_notifcation',
            'mp_stock',
            'mp_difference_per',
            'mp_total_rate',
            'mp_date',
            'mp_modify_date',
            'mp_free_shipping',
            'mp_shipping_charge',
            'mp_wishlist_state',
            'mp_sale_state',
            'mp_view_state',
            'mp_return_days',
            'mp_do_return',
            'mp_description',
            'mp_short_description',
            'mp_default_img',
            'mp_search_tag',
            'mp_search_keyword',
            'mp_status',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "search_products";
        $func_array["function"]["output_keys"] = array(
            'search_query',
        );
        $func_array["function"]["output_alias"] = array(
            "mp_mst_products_id" => "mp_mst_products_id",
            "mp_admin_id" => "mp_admin_id",
            "mp_store_id" => "mp_store_id",
            "mp_title" => "mp_title",
            "mp_sku" => "mp_sku",
            "mp_regular_price" => "mp_regular_price",
            "mp_sale_price" => "mp_sale_price",
            "mp_rating_avg" => "mp_rating_avg",
            "mp_allow_max_purchase" => "mp_allow_max_purchase",
            "mp_low_avl_limit_notifcation" => "mp_low_avl_limit_notifcation",
            "mp_stock" => "mp_stock",
            "mp_difference_per" => "mp_difference_per",
            "mp_total_rate" => "mp_total_rate",
            "mp_date" => "mp_date",
            "mp_modify_date" => "mp_modify_date",
            "mp_free_shipping" => "mp_free_shipping",
            "mp_shipping_charge" => "mp_shipping_charge",
            "mp_wishlist_state" => "mp_wishlist_state",
            "mp_sale_state" => "mp_sale_state",
            "mp_view_state" => "mp_view_state",
            "mp_return_days" => "mp_return_days",
            "mp_do_return" => "mp_do_return",
            "mp_description" => "mp_description",
            "mp_short_description" => "mp_short_description",
            "mp_default_img" => "mp_default_img",
            "mp_search_tag" => "mp_search_tag",
            "mp_search_keyword" => "mp_search_keyword",
            "mp_status" => "mp_status",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success_search_query", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
