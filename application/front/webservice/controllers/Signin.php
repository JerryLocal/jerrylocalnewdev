<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of signin Controller
 *
 * @module signin
 *
 * @class signin.php
 *
 * @path applicationront\webservice\controllers\signin.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Signin extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_signin');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "email" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "email_required",
                    "message" => "Please enter a value for the email field.",
                ),
                array(
                    "rule" => "email",
                    "value" => TRUE,
                    "message_code" => "email_email",
                    "message" => "Please enter valid email address for the email field.",
                )
            ),
            "password" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "password_required",
                    "message" => "Please enter a value for the password field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "signin");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_signin->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "get_user_login_detail");
            $output_response = $this->get_user_login_detail($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * get_user_login_detail method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_user_login_detail($input_params = array()) {

        $output_arr = $this->model_signin->get_user_login_detail($input_params, $this->settings_params);
        $input_params["get_user_login_detail"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_user_login_detail", $output_arr, $input_params, "condition");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "get_user_login_detail";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_signin->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "finish_success");
            return $this->finish_success($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mod_customer_finish_success");
            return $this->mod_customer_finish_success($input_params);
        }
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "Record Found",
        );
        $output_fields = array(
            'mc_email',
            'mc_password',
            'mc_first_name',
            'mc_last_name',
            'mc_status',
            'mc_customer_id',
            'mc_user_name',
            'mc_t_registered_date',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "signin";
        $func_array["function"]["output_keys"] = array(
            'get_user_login_detail',
        );
        $func_array["function"]["output_alias"] = array(
            "mc_email" => "mc_email",
            "mc_password" => "mc_password",
            "mc_first_name" => "mc_first_name",
            "mc_last_name" => "mc_last_name",
            "mc_status" => "mc_status",
            "mc_customer_id" => "mc_customer_id",
            "mc_user_name" => "mc_user_name",
            "mc_t_registered_date" => "mc_t_registered_date",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mod_customer_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mod_customer_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mod_customer_finish_success",
            "message" => "No user Found",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "signin";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mod_customer_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
