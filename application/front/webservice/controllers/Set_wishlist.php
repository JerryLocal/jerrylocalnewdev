<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of setWishlist Controller
 *
 * @module setWishlist
 *
 * @class set_wishlist.php
 *
 * @path applicationront\webservice\controllers\set_wishlist.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Set_wishlist extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_set_wishlist');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "iAdminId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iAdminId_required",
                    "message" => "Please enter a value for the iAdminId field.",
                )
            ),
            "iProductId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iProductId_required",
                    "message" => "Please enter a value for the iProductId field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "set_wishlist");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_set_wishlist->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "check_wishlist");
            $output_response = $this->check_wishlist($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * check_wishlist method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function check_wishlist($input_params = array()) {

        $output_arr = $this->model_set_wishlist->check_wishlist($input_params, $this->settings_params);
        $input_params["check_wishlist"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("check_wishlist", $output_arr, $input_params, "wishlistcheck");
        $this->multiple_keys[] = "check_wishlist";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->wishlistcheck($input_params);
    }

    /**
     * wishlistcheck method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function wishlistcheck($input_params = array()) {

        $output_arr = $this->model_set_wishlist->wishlistcheck($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("wishlistcheck", $output_arr, $input_params, "insert_query_wishlist");
            return $this->insert_query_wishlist($input_params);
        } else {
            $this->wsresponse->pushDebugParams("wishlistcheck", $output_arr, $input_params, "trn_product_wishlist_finish_success");
            return $this->trn_product_wishlist_finish_success($input_params);
        }
    }

    /**
     * insert_query_wishlist method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function insert_query_wishlist($input_params = array()) {

        $output_arr = $this->model_set_wishlist->insert_query_wishlist($input_params, $this->settings_params);
        $input_params["insert_query_wishlist"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("insert_query_wishlist", $output_arr, $input_params, "wishlistinserted");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "insert_query_wishlist";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->wishlistinserted($input_params);
    }

    /**
     * wishlistinserted method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function wishlistinserted($input_params = array()) {

        $output_arr = $this->model_set_wishlist->wishlistinserted($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("wishlistinserted", $output_arr, $input_params, "finish_success");
            return $this->finish_success($input_params);
        } else {
            $this->wsresponse->pushDebugParams("wishlistinserted", $output_arr, $input_params, "finish_success_1");
            return $this->finish_success_1($input_params);
        }
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "Your Product is been added in wishlist ",
        );
        $output_fields = array(
            'tpw_mst_products_id',
            'insert_id',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "set_wishlist";
        $func_array["function"]["output_keys"] = array(
            'check_wishlist',
            'insert_query_wishlist',
        );
        $func_array["function"]["output_alias"] = array(
            "tpw_mst_products_id" => "tpw_mst_products_id",
            "insert_id" => "insert_id",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "finish_success_1",
            "message" => "Error in Inserting Wishlist",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "set_wishlist";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * trn_product_wishlist_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function trn_product_wishlist_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "trn_product_wishlist_finish_success",
            "message" => "Alread Product available in your wishlist",
        );
        $output_fields = array(
            'tpw_admin_id',
            'tpw_mst_products_id',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "set_wishlist";
        $func_array["function"]["output_keys"] = array(
            'check_wishlist',
        );
        $func_array["function"]["output_alias"] = array(
            "tpw_admin_id" => "tpw_admin_id",
            "tpw_mst_products_id" => "tpw_mst_products_id",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("trn_product_wishlist_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
