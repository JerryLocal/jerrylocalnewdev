<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of addAddress Controller
 *
 * @module addAddress
 *
 * @class add_address.php
 *
 * @path applicationront\webservice\controllers\add_address.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Add_address extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_add_address');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "iAdminId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iAdminId_required",
                    "message" => "Please enter a value for the iAdminId field.",
                )
            ),
            "vName" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vName_required",
                    "message" => "Please enter a value for the vName field.",
                )
            ),
            "vAddress1" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vAddress1_required",
                    "message" => "Please enter a value for the vAddress1 field.",
                )
            ),
            "vArea" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vArea_required",
                    "message" => "Please enter a value for the vArea field.",
                )
            ),
            "iCountryId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iCountryId_required",
                    "message" => "Please enter a value for the iCountryId field.",
                )
            ),
            "iStateId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iStateId_required",
                    "message" => "Please enter a value for the iStateId field.",
                )
            ),
            "iCityId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iCityId_required",
                    "message" => "Please enter a value for the iCityId field.",
                )
            ),
            "vPinCode" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vPinCode_required",
                    "message" => "Please enter a value for the vPinCode field.",
                )
            ),
            "vPhone" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vPhone_required",
                    "message" => "Please enter a value for the vPhone field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "add_address");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_add_address->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "query");
            $output_response = $this->query($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array()) {

        $output_arr = $this->model_add_address->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "mst_buyer_address_finish_success");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->mst_buyer_address_finish_success($input_params);
    }

    /**
     * mst_buyer_address_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_buyer_address_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "mst_buyer_address_finish_success",
            "message" => "Added successfully.",
        );
        $output_fields = array(
            'insert_id',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "add_address";
        $func_array["function"]["output_keys"] = array(
            'query',
        );
        $func_array["function"]["output_alias"] = array(
            "insert_id" => "insert_id",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_buyer_address_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
