<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of placeOrder Controller
 *
 * @module placeOrder
 *
 * @class place_order.php
 *
 * @path applicationront\webservice\controllers\place_order.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Place_order extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_place_order');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "iBuyerId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iBuyerId_required",
                    "message" => "Please enter a value for the iBuyerId field.",
                )
            ),
            "fShippingCost" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "fShippingCost_required",
                    "message" => "Please enter a value for the fShippingCost field.",
                )
            ),
            "fSubTotal" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "fSubTotal_required",
                    "message" => "Please enter a value for the fSubTotal field.",
                )
            ),
            "fOrderTotal" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "fOrderTotal_required",
                    "message" => "Please enter a value for the fOrderTotal field.",
                )
            ),
            "ePaymentStatus" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "ePaymentStatus_required",
                    "message" => "Please enter a value for the ePaymentStatus field.",
                )
            ),
            "dDate" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "dDate_required",
                    "message" => "Please enter a value for the dDate field.",
                )
            ),
            "vBuyerEmail" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vBuyerEmail_required",
                    "message" => "Please enter a value for the vBuyerEmail field.",
                )
            ),
            "vBuyerIp" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vBuyerIp_required",
                    "message" => "Please enter a value for the vBuyerIp field.",
                )
            ),
            "vBuyerName" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vBuyerName_required",
                    "message" => "Please enter a value for the vBuyerName field.",
                )
            ),
            "vBuyerAddress1" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vBuyerAddress1_required",
                    "message" => "Please enter a value for the vBuyerAddress1 field.",
                )
            ),
            "vBuyerArea" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vBuyerArea_required",
                    "message" => "Please enter a value for the vBuyerArea field.",
                )
            ),
            "iBuyerCountryId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iBuyerCountryId_required",
                    "message" => "Please enter a value for the iBuyerCountryId field.",
                )
            ),
            "iBuyerCityId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iBuyerCityId_required",
                    "message" => "Please enter a value for the iBuyerCityId field.",
                )
            ),
            "iBuyerStateId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iBuyerStateId_required",
                    "message" => "Please enter a value for the iBuyerStateId field.",
                )
            ),
            "vBuyerPinCode" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vBuyerPinCode_required",
                    "message" => "Please enter a value for the vBuyerPinCode field.",
                )
            ),
            "vBuyerPhone" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "vBuyerPhone_required",
                    "message" => "Please enter a value for the vBuyerPhone field.",
                )
            ),
            "order_item" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "order_item_required",
                    "message" => "Please enter a value for the order_item field.",
                )
            ),
            "tPaymentDetail" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "tPaymentDetail_required",
                    "message" => "Please enter a value for the tPaymentDetail field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "place_order");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_place_order->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "query");
            $output_response = $this->query($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array()) {

        $output_arr = $this->model_place_order->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "condition");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_place_order->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "query_2");
            return $this->query_2($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mst_order_finish_success");
            return $this->mst_order_finish_success($input_params);
        }
    }

    /**
     * query_2 method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query_2($input_params = array()) {

        $output_arr = $this->model_place_order->query_2($input_params, $this->settings_params);
        $input_params["query_2"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query_2", $output_arr, $input_params, "custom_function");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);
        $this->single_keys[] = "query_2";
        $this->wsresponse->makeUniqueParams($this->single_keys);

        return $this->custom_function($input_params);
    }

    /**
     * custom_function method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function custom_function($input_params = array()) {
        if (method_exists($this->general, "subOrder")) {
            $result_arr["data"] = $this->general->subOrder($input_params);
            $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["custom_function"] = $this->wsresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "custom_function";
            $this->wsresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->wsresponse->pushDebugParams("custom_function", $result_arr, $input_params, "start_loop");
        return $this->start_loop($input_params);
    }

    /**
     * start_loop method is used to process loop flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function start_loop($input_params = array()) {
        $this->iterate_start_loop($input_params["custom_function"], $input_params);

        $this->wsresponse->pushDebugParams("end_loop", array(), $input_params, "mst_order_finish_success_1", "custom_function");
        return $this->mst_order_finish_success_1($input_params);
    }

    /**
     * query_1 method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query_1($input_params = array()) {

        $output_arr = $this->model_place_order->query_1($input_params, $this->settings_params);
        $input_params["query_1"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query_1", $output_arr, $input_params, "mst_order_finish_success_1", "custom_function", "end_loop");
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $output_arr["data"]);

        return $input_params;
    }

    /**
     * mst_order_finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_order_finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "mst_order_finish_success_1",
            "message" => "Order Placed Successfully",
        );
        $output_fields = array(
            'order_id',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "place_order";
        $func_array["function"]["output_keys"] = array(
            'query',
        );
        $func_array["function"]["output_alias"] = array(
            "order_id" => "order_id",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_order_finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_order_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_order_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_order_finish_success",
            "message" => "Order Failed.",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "place_order";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_order_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * iterate_start_loop method is used to iterate loop.
     *
     * @param array $custom_function_lp_arr custom_function_lp_arr array to iterate loop.
     * @param array $input_params_addr $input_params_addr array to address original input params.
     */
    public function iterate_start_loop(&$custom_function_lp_arr = array(), &$input_params_addr = array()) {

        $_loop_params_tmp = $_loop_tmp_dict = array();
        $input_params_loc = $input_params_addr;
        $_loop_params_loc = $custom_function_lp_arr;
        $_lp_ini = 0;
        $_lp_end = count($_loop_params_loc);
        $_lp_cnf = (is_array($_loop_params_loc[0])) ? TRUE : FALSE;
        $_loop_debug_tmp = array(
            "start_point" => $_lp_ini,
            "end_point" => $_lp_end,
            "step" => 1,
            "loop" => "custom_function",
        );
        $this->wsresponse->pushDebugParams("start_loop", $_loop_debug_tmp, $input_params, "query_1");
        for ($i = $_lp_ini; $i < $_lp_end; $i += 1) {
            $custom_function_lp_pms = $input_params_loc;
            if ($_lp_cnf) {
                if (is_array($_loop_params_loc[$i])) {
                    $custom_function_lp_pms = $_loop_params_loc[$i]+$input_params_loc;
                }
                unset($custom_function_lp_pms["custom_function"]);
            } else {
                $custom_function_lp_pms["custom_function"] = $_loop_params_loc[$i];
            }
            $custom_function_lp_pms["i"] = $i;
            $custom_function_lp_pms["__dictionaries"] = $_loop_tmp_dict;
            $input_params = $custom_function_lp_pms;
            $input_params = $this->query_1($input_params);
            if (is_array($input_params["__dictionaries"])) {
                $_loop_tmp_dict = $input_params["__dictionaries"];
                unset($input_params["__dictionaries"]);
            }
            if (is_array($input_params["__variables"])) {
                $input_params = $this->wsresponse->grabLoopVariables($input_params["__variables"], $input_params);
                unset($input_params["__variables"]);
            }
            if ($this->break_continue == 1) {
                $this->break_continue = NULL;
                break;
            } elseif ($this->break_continue == 2) {
                $this->break_continue = NULL;
                continue;
            }
            if ($_lp_cnf) {
                $custom_function_lp_arr[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $custom_function_lp_pms);
            } else {
                $_loop_params_tmp[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $custom_function_lp_pms);
            }
        }
        if (!$_lp_cnf) {
            $input_params_addr["custom_function"] = $_loop_params_tmp;
        }
        if (is_array($_loop_tmp_dict)) {
            $input_params_addr = array_merge($input_params_addr, $_loop_tmp_dict);
        }
    }
}
