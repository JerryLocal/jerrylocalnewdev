<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getPopularProducts Controller
 *
 * @module getPopularProducts
 *
 * @class get_popular_products.php
 *
 * @path applicationront\webservice\controllers\get_popular_products.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Get_popular_products extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_popular_products');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array();
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_popular_products");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_popular_products->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "get_product_details");
            $output_response = $this->get_product_details($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * get_product_details method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_product_details($input_params = array()) {

        $output_arr = $this->model_get_popular_products->get_product_details($input_params, $this->settings_params);
        $input_params["get_product_details"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_product_details", $output_arr, $input_params, "condition");
        $this->multiple_keys[] = "get_product_details";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_get_popular_products->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "finish_success");
            return $this->finish_success($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "get_product_details_v1");
            return $this->get_product_details_v1($input_params);
        }
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "Product Found.",
        );
        $output_fields = array(
            'mp_title',
            'mp_sale_price',
            'mp_regular_price',
            'mp_rating_avg',
            'mp_difference_per',
            'mp_total_rate',
            'mp_date',
            'mp_default_img',
            'mp_sale_state',
            'mp_mst_products_id',
            'mp_wishlist_state',
            'mp_stock',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_popular_products";
        $func_array["function"]["output_keys"] = array(
            'get_product_details',
        );
        $func_array["function"]["output_alias"] = array(
            "mp_title" => "mp_title",
            "mp_sale_price" => "mp_sale_price",
            "mp_regular_price" => "mp_regular_price",
            "mp_rating_avg" => "mp_rating_avg",
            "mp_difference_per" => "mp_difference_per",
            "mp_total_rate" => "mp_total_rate",
            "mp_date" => "mp_date",
            "mp_default_img" => "mp_default_img",
            "mp_sale_state" => "mp_sale_state",
            "mp_mst_products_id" => "mp_mst_products_id",
            "mp_wishlist_state" => "mp_wishlist_state",
            "mp_stock" => "mp_stock",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * get_product_details_v1 method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_product_details_v1($input_params = array()) {

        $output_arr = $this->model_get_popular_products->get_product_details_v1($input_params, $this->settings_params);
        $input_params["get_product_details_v1"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_product_details_v1", $output_arr, $input_params, "finish_success_1");
        $this->multiple_keys[] = "get_product_details_v1";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->finish_success_1($input_params);
    }

    /**
     * finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success_1($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success_1",
            "message" => "Random Records Found",
        );
        $output_fields = array(
            'mp_title_1',
            'mp_sale_price_1',
            'mp_regular_price_1',
            'mp_rating_avg_1',
            'mp_difference_per_1',
            'mp_total_rate_1',
            'mp_date_1',
            'mp_default_img_1',
            'mp_sale_state_1',
            'mp_mst_products_id_1',
            'mp_wishlist_state_1',
            'mp_stock_1',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_popular_products";
        $func_array["function"]["output_keys"] = array(
            'get_product_details_v1',
        );
        $func_array["function"]["output_alias"] = array(
            "mp_title_1" => "mp_title_1",
            "mp_sale_price_1" => "mp_sale_price_1",
            "mp_regular_price_1" => "mp_regular_price_1",
            "mp_rating_avg_1" => "mp_rating_avg_1",
            "mp_difference_per_1" => "mp_difference_per_1",
            "mp_total_rate_1" => "mp_total_rate_1",
            "mp_date_1" => "mp_date_1",
            "mp_default_img_1" => "mp_default_img_1",
            "mp_sale_state_1" => "mp_sale_state_1",
            "mp_mst_products_id_1" => "mp_mst_products_id_1",
            "mp_wishlist_state_1" => "mp_wishlist_state_1",
            "mp_stock_1" => "mp_stock_1",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
