<?php
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Description of getMenuTree Controller
 *
 * @module getMenuTree
 *
 * @class get_menu_tree.php
 *
 * @path applicationront\webservice\controllers\get_menu_tree.php
 *
 * @author CIT Dev Team
 *
 * @date 29.12.2015
 */

class Get_menu_tree extends HB_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_menu_tree');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE)
    {
        $validation_arr = array();
        try
        {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_menu_tree");
            if ($validation_res["success"] == "-5")
            {
                if ($inner_api === TRUE)
                {
                    return $validation_res;
                }
                else
                {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_menu_tree->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "menu");
            $output_response = $this->menu($input_params);
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * menu method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function menu($input_params = array())
    {

        $output_arr = $this->model_get_menu_tree->menu($input_params, $this->settings_params);
        $input_params["menu"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("menu", $output_arr, $input_params, "condition");
        $this->multiple_keys[] = "menu";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array())
    {

        $output_arr = $this->model_get_menu_tree->condition($input_params);
        if ($output_arr["success"])
        {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "query");
            return $this->query($input_params);
        }
        else
        {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "finish_success");
            return $this->finish_success($input_params);
        }
    }

    /**
     * query method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array())
    {

        $output_arr = $this->model_get_menu_tree->query($input_params, $this->settings_params);
        $input_params["query"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("query", $output_arr, $input_params, "get_tree_structure");
        $this->multiple_keys[] = "query";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->get_tree_structure($input_params);
    }

    /**
     * get_tree_structure method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_tree_structure($input_params = array())
    {
        if (method_exists($this->general, "Menu"))
        {
            $result_arr["data"] = $this->general->Menu($input_params);
            $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["get_tree_structure"] = $this->wsresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "get_tree_structure";
            $this->wsresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->wsresponse->pushDebugParams("get_tree_structure", $result_arr, $input_params, "finish_success_1");
        return $this->finish_success_1($input_params);
    }

    /**
     * finish_success_1 method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success_1($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success_1",
            "message" => "",
        );
        $output_fields = array(
            'mm_mst_menu_id',
            'mm_title',
            'mm_menu_key',
            'mmi_mst_menu_id',
            'mmi_title',
            'mmi_parent_id',
            'mmi_product_id',
            'mmi_category_id',
            'mmi_static_page_id',
            'mmi_external_link',
            'mmi_resource_type',
            'mmi_login_req',
            'mmi_mst_menu_items_id',
            'mmi_status',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_menu_tree";
        $func_array["function"]["output_keys"] = array(
            'menu',
            'query',
        );
        $func_array["function"]["output_alias"] = array(
            "mm_mst_menu_id" => "mm_mst_menu_id",
            "mm_title" => "mm_title",
            "mm_menu_key" => "mm_menu_key",
            "mmi_mst_menu_id" => "mmi_mst_menu_id",
            "mmi_title" => "mmi_title",
            "mmi_parent_id" => "mmi_parent_id",
            "mmi_product_id" => "mmi_product_id",
            "mmi_category_id" => "mmi_category_id",
            "mmi_static_page_id" => "mmi_static_page_id",
            "mmi_external_link" => "mmi_external_link",
            "mmi_resource_type" => "mmi_resource_type",
            "mmi_login_req" => "mmi_login_req",
            "mmi_mst_menu_items_id" => "mmi_mst_menu_items_id",
            "mmi_status" => "mmi_status",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success_1", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "finish_success",
            "message" => "no menu entry found",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_menu_tree";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
