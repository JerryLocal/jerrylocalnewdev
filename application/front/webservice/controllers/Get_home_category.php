<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of getHomeCategory Controller
 *
 * @module getHomeCategory
 *
 * @class get_home_category.php
 *
 * @path applicationront\webservice\controllers\get_home_category.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Get_home_category extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_get_home_category');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array();
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "get_home_category");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_get_home_category->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "get_category_name");
            $output_response = $this->get_category_name($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * get_category_name method is used to process query block.
     * get image and main category name
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_category_name($input_params = array()) {

        $output_arr = $this->model_get_home_category->get_category_name($input_params, $this->settings_params);
        $input_params["get_category_name"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_category_name", $output_arr, $input_params, "chk_has_main_cat");
        $this->multiple_keys[] = "get_category_name";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->chk_has_main_cat($input_params);
    }

    /**
     * chk_has_main_cat method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function chk_has_main_cat($input_params = array()) {

        $output_arr = $this->model_get_home_category->chk_has_main_cat($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("chk_has_main_cat", $output_arr, $input_params, "get_child_cat");
            return $this->get_child_cat($input_params);
        } else {
            $this->wsresponse->pushDebugParams("chk_has_main_cat", $output_arr, $input_params, "no_main_category_found");
            return $this->no_main_category_found($input_params);
        }
    }

    /**
     * get_child_cat method is used to process query block.
     * To get Child Category name
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_child_cat($input_params = array()) {

        $output_arr = $this->model_get_home_category->get_child_cat($input_params, $this->settings_params);
        $input_params["get_child_cat"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_child_cat", $output_arr, $input_params, "chk_has_child");
        $this->multiple_keys[] = "get_child_cat";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->chk_has_child($input_params);
    }

    /**
     * chk_has_child method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function chk_has_child($input_params = array()) {

        $output_arr = $this->model_get_home_category->chk_has_child($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("chk_has_child", $output_arr, $input_params, "child_categories_found");
            return $this->child_categories_found($input_params);
        } else {
            $this->wsresponse->pushDebugParams("chk_has_child", $output_arr, $input_params, "no_child_found");
            return $this->no_child_found($input_params);
        }
    }

    /**
     * child_categories_found method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function child_categories_found($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "child_categories_found",
            "message" => "Child Categories Found",
        );
        $output_fields = array(
            'thc_mst_categories_id',
            'thc_img',
            'mc_title',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_home_category";
        $func_array["function"]["output_keys"] = array(
            'get_category_name',
            'get_child_cat',
        );
        $func_array["function"]["output_alias"] = array(
            "thc_mst_categories_id" => "thc_mst_categories_id",
            "thc_img" => "thc_img",
            "mc_title" => "mc_title",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("child_categories_found", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * no_child_found method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function no_child_found($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "no_child_found",
            "message" => "No Child Category Found.",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_home_category";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("no_child_found", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * no_main_category_found method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function no_main_category_found($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "no_main_category_found",
            "message" => "No Record Found",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_home_category";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("no_main_category_found", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
