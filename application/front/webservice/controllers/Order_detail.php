<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of orderDetail Controller
 *
 * @module orderDetail
 *
 * @class order_detail.php
 *
 * @path applicationront\webservice\controllers\order_detail.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Order_detail extends HB_Controller {
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $break_continue;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->break_continue = NULL;

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_order_detail');
    }

    /**
     * handler method is used to initiate api execution flow.
     *
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function handler($request_arr = array(), $inner_api = FALSE) {
        $validation_arr = array(
            "iMstOrderId" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message_code" => "iMstOrderId_required",
                    "message" => "Please enter a value for the iMstOrderId field.",
                )
            )
        );
        try {
            $output_response = array();
            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "order_detail");
            if ($validation_res["success"] == "-5") {
                if ($inner_api === TRUE) {
                    return $validation_res;
                } else {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $this->model_order_detail->_default_lang = $this->general->getLangRequestValue();
            $input_params = $validation_res['input_params'];

            $output_array = $func_array = array();

            //logging input params
            $this->wsresponse->pushDebugParams("input_params", $input_params, $input_params, "get_ordr_data_with_suborder");
            $output_response = $this->get_ordr_data_with_suborder($input_params);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * get_ordr_data_with_suborder method is used to process query block.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_ordr_data_with_suborder($input_params = array()) {

        $output_arr = $this->model_order_detail->get_ordr_data_with_suborder($input_params, $this->settings_params);
        $input_params["get_ordr_data_with_suborder"] = $output_arr["data"];
        $this->wsresponse->pushDebugParams("get_ordr_data_with_suborder", $output_arr, $input_params, "condition");
        $this->multiple_keys[] = "get_ordr_data_with_suborder";
        $this->wsresponse->makeUniqueParams($this->multiple_keys);

        return $this->condition($input_params);
    }

    /**
     * condition method is used to process conditions.
     *
     * @param array $input_params input_params array to process condition flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function condition($input_params = array()) {

        $output_arr = $this->model_order_detail->condition($input_params);
        if ($output_arr["success"]) {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "custom_function");
            return $this->custom_function($input_params);
        } else {
            $this->wsresponse->pushDebugParams("condition", $output_arr, $input_params, "mst_order_finish_success");
            return $this->mst_order_finish_success($input_params);
        }
    }

    /**
     * custom_function method is used to process custom function.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function custom_function($input_params = array()) {
        if (method_exists($this->general, "checkOrderStatus")) {
            $result_arr["data"] = $this->general->checkOrderStatus($input_params);
            $input_params = $this->wsresponse->assignSingleRecord($input_params, $result_arr["data"]);

            $input_params["custom_function"] = $this->wsresponse->assignFunctionResponse($result_arr);
            $this->multiple_keys[] = "custom_function";
            $this->wsresponse->makeUniqueParams($this->multiple_keys);
        }
        $this->wsresponse->pushDebugParams("custom_function", $result_arr, $input_params, "finish_success");
        return $this->finish_success($input_params);
    }

    /**
     * finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "1",
            "message_code" => "finish_success",
            "message" => "",
        );
        $output_fields = array(
            'mo_mst_order_id',
            'mo_buyer_phone',
            'mo_buyer_pin_code',
            'mo_buyer_area',
            'mo_buyer_address1',
            'mo_order_total',
            'mo_date',
            'mo_buyer_email',
            'mso_mst_sub_order_id',
            'mso_product_name',
            'mso_total_cost',
            'mso_item_status',
            'mso_tracking_number',
            'mso_shipper_name',
            'mp_default_img',
            'msd_store_name',
            'ms_url',
            'mc_country',
            'ms1_state',
            'mc1_city',
            'mo_payment_status',
            'mo_buyer_name',
            'mo_buyer_ip',
            'tosh_date',
            'tosh_order_status',
            'mso_shipping_remark',
            'mso_mst_products_id',
            'mp_difference_per',
            'mo_shipping_cost',
            'Discount_price',
            'mso_product_sale_price',
            'mso_product_regular_price',
            'mso_product_price',
            'mso_product_qty',
            'mso_shipping_cost',
            'mso_mst_store_detail_id',
            'minstatus',
            'Discount',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "order_detail";
        $func_array["function"]["output_keys"] = array(
            'get_ordr_data_with_suborder',
            'custom_function',
        );
        $func_array["function"]["output_alias"] = array(
            "mo_mst_order_id" => "mo_mst_order_id",
            "mo_buyer_phone" => "mo_buyer_phone",
            "mo_buyer_pin_code" => "mo_buyer_pin_code",
            "mo_buyer_area" => "mo_buyer_area",
            "mo_buyer_address1" => "mo_buyer_address1",
            "mo_order_total" => "mo_order_total",
            "mo_date" => "mo_date",
            "mo_buyer_email" => "mo_buyer_email",
            "mso_mst_sub_order_id" => "mso_mst_sub_order_id",
            "mso_product_name" => "mso_product_name",
            "mso_total_cost" => "mso_total_cost",
            "mso_item_status" => "mso_item_status",
            "mso_tracking_number" => "mso_tracking_number",
            "mso_shipper_name" => "mso_shipper_name",
            "mp_default_img" => "mp_default_img",
            "msd_store_name" => "msd_store_name",
            "ms_url" => "ms_url",
            "mc_country" => "mc_country",
            "ms1_state" => "ms1_state",
            "mc1_city" => "mc1_city",
            "mo_payment_status" => "mo_payment_status",
            "mo_buyer_name" => "mo_buyer_name",
            "mo_buyer_ip" => "mo_buyer_ip",
            "tosh_date" => "tosh_date",
            "tosh_order_status" => "tosh_order_status",
            "mso_shipping_remark" => "mso_shipping_remark",
            "mso_mst_products_id" => "mso_mst_products_id",
            "mp_difference_per" => "mp_difference_per",
            "mo_shipping_cost" => "mo_shipping_cost",
            "Discount_price" => "Discount_price",
            "mso_product_sale_price" => "mso_product_sale_price",
            "mso_product_regular_price" => "mso_product_regular_price",
            "mso_product_price" => "mso_product_price",
            "mso_product_qty" => "mso_product_qty",
            "mso_shipping_cost" => "mso_shipping_cost",
            "mso_mst_store_detail_id" => "mso_mst_store_detail_id",
            "minstatus" => "minstatus",
            "Discount" => "Discount",
        );
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mst_order_finish_success method is used to process finish flow.
     *
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mst_order_finish_success($input_params = array()) {

        $setting_fields = array(
            "success" => "0",
            "message_code" => "mst_order_finish_success",
            "message" => "No Order available",
        );
        $output_fields = array();

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "order_detail";
        $func_array["function"]["output_keys"] = array();
        $func_array["function"]["output_alias"] = array();
        $func_array["function"]["inner_keys"] = array();
        $func_array["function"]["single_keys"] = $this->single_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;
        $func_array["function"]["custom_keys"] = $this->custom_keys;

        $this->wsresponse->pushDebugParams("mst_order_finish_success", array(), $input_params, "");
        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
