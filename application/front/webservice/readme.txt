Please copy the below file(s) from downloaded zip folder to your project folder


Source and Destination Path(s)
------------------------------

Downloaded Zip --> webservice/controllers/*
Project Folder --> application/front/webservice/controllers/*

Downloaded Zip --> webservice/models/*
Project Folder --> application/front/webservice/models/*

[English - Language]
Downloaded Zip --> webservice/webservice_lang.php
Project Folder --> application/language/en/webservice_lang.php

[Config (>= 3.0)]
Downloaded Zip --> webservice/hb_webservices.php
Project Folder --> application/config/hb_webservices.php