<?php
/**
 * Description of getIndividualProduct Model
 *
 * @module getIndividualProduct
 *
 * @class model_get_individual_product.php
 *
 * @path applicationront\webservice\models\model_get_individual_product.php
 *
 * @author CIT Dev Team
 *
 * @date 05.01.2016
 */

class Model_get_individual_product extends CI_Model
{
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $sql_query = "SELECT `mp`.`fShippingCharge`,`mp`.`eFreeShipping`,`mp`.`iReturnDays`,`mp`.`iMstProductsId` AS `mp_mst_products_id`,`mp`.`iWishlistState`,`mp`.`iStoreId` AS `mp_mst_store_id`, `mp`.`vTitle` AS `mp_title`,
`mp`.`vSku` AS `mp_sku`, `mp`.`fRegularPrice` AS `mp_regular_price`, `mp`.`fSalePrice` AS `mp_sale_price`,
`mp`.`fRatingAvg` AS `mp_rating_avg`, `mp`.`iStock` AS `mp_stock`, `mp`.`iDifferencePer` AS `mp_difference_per`,
`mp`.`tDescription` AS `mp_description`, `mp`.`vShortDescription` AS `mp_short_description`, `mp`.`iLowAvlLimitNotifcation` AS `mp_low_avl_limit_notification`, `mp`.`eStatus` AS `mp_status`,
`mp`.`vDefaultImg` AS `mp_default_img`, `mpi`.`vImgPath` AS `mpi_img_path` ,
(SELECT GROUP_CONCAT(tpo.vOptionValue)  FROM trn_product_option as tpo WHERE tpo.iProductId=".$input_params["iProductId"]." AND LOWER(tpo.vCategoryOptionValue) LIKE '%size%') AS tpo_option_value,
(SELECT tpo.vCategoryOptionValue  FROM trn_product_option as tpo WHERE tpo.iProductId=".$input_params["iProductId"]." AND LOWER(tpo.vCategoryOptionValue) LIKE '%size%' LIMIT 1) AS tpo_category_option_value
FROM `mst_products` AS `mp`
LEFT JOIN `mst_product_img` AS `mpi` ON `mp`.`iMstProductsId` = `mpi`.`iMstProductsId`
WHERE `mp`.`iMstProductsId` = ".$input_params["iProductId"]." LIMIT 1";
            $result_obj = $this->db->query($sql_query);
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }

            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * product_option method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function product_option($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("trn_product_option AS tpo");

            $select_fields = array();
            $this->db->select("tpo.vCategoryOptionValue AS tpo_category_option_value_1");
            $this->db->select("tpo.vOptionValue AS tpo_option_value_1");
            if ($input_params['iProductId'] != "")
            {
                $this->db->where("tpo.iProductId =", $input_params['iProductId']);
            }
            $this->db->not_like("tpo.vCategoryOptionValue", "Size", "both");

            $this->db->order_by("tpo.vCategoryOptionValue", "asc");

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }

            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array())
    {
        try
        {

            $cc_lo_0 = (empty($input_params["query"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
