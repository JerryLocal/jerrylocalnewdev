<?php
/**
 * Description of validate product for cart Model
 *
 * @module validate product for cart
 *
 * @class model_validate_product_for_cart.php
 *
 * @path applicationront\webservice\models\model_validate_product_for_cart.php
 *
 * @author CIT Dev Team
 *
 * @date 24.12.2015
 */

class Model_validate_product_for_cart extends CI_Model
{
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("mst_products AS mp");
            $join_condition = $this->db->protect("mp.iMstProductsId")." = ".$this->db->protect("tpo.iProductId")."  AND LOWER(tpo.vCategoryOptionValue)='size'";
            $this->db->join("trn_product_option AS tpo", $join_condition, "left", FALSE);
            $this->db->join("mst_store_detail AS msd", "mp.iStoreId = msd.iMstStoreDetailId", "left");
            $this->db->join("trn_product_categories AS tpc", "mp.iMstProductsId = tpc.iMstProductsId", "left");

            $select_fields = array();
            $this->db->select("mp.iMstProductsId AS mp_mst_products_id");
            $this->db->select("mp.iStoreId AS mp_store_id");
            $this->db->select("mp.vTitle AS mp_title");
            $this->db->select("mp.vSku AS mp_sku");
            $this->db->select("mp.fRegularPrice AS mp_regular_price");
            $this->db->select("mp.fSalePrice AS mp_sale_price");
            $this->db->select("mp.fRatingAvg AS mp_rating_avg");
            $this->db->select("mp.iAllowMaxPurchase AS mp_allow_max_purchase");
            $this->db->select("mp.iLowAvlLimitNotifcation AS mp_low_avl_limit_notifcation");
            $this->db->select("mp.iStock AS mp_stock");
            $this->db->select("mp.iDifferencePer AS mp_difference_per");
            $this->db->select("mp.iTotalRate AS mp_total_rate");
            $this->db->select("mp.tDescription AS mp_description");
            $this->db->select("mp.vShortDescription AS mp_short_description");
            $this->db->select("mp.vDefaultImg AS mp_default_img");
            $this->db->select("GROUP_CONCAT(tpo.vOptionValue) AS tpo_option_value");
            $this->db->select("tpo.vCategoryOptionValue AS tpo_category_option_value");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("msd.vWebsiteUrl AS msd_website_url");
            $this->db->select("mp.fShippingCharge AS mp_shipping_charge");
            $this->db->select("mp.eFreeShipping AS mp_free_shipping");
            $this->db->select("tpc.iMstCategoriesId AS product_categories_id");
            $this->db->select("mp.iReturnDays AS mp_return_days");
            if ($input_params['iProductId'] != "")
            {
                $this->db->where("mp.iMstProductsId =", $input_params['iProductId']);
            }
            $this->db->where_in("mp.eStatus", array('Active'));
            $this->db->where("(mp.fRegularPrice > 0 OR mp.fSalePrice  > 0)", FALSE, FALSE);

            $this->db->group_by(array("mp.iMstProductsId"));
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0)
            {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr)
                {

                    $data = $data_arr["mp_default_img"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = @implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["height"] = "50";
                    $image_arr["width"] = "50";
                    $image_arr["color"] = "FFFFFF";
                    $image_arr["path"] = $this->general->getImageNestedFolders("p_img");
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["mp_default_img"] = $data;

                    $i++;
                }
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * admincommission method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function admincommission($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $sql_query = "SELECT MIN(T2.iMstCategoriesId) as desired_cat_id, T2.vTitle,T2.fCommission, T3.vCleanUrl FROM ( SELECT @r AS _id, (SELECT @r := iParentId FROM mst_categories WHERE iMstCategoriesId= _id) AS parent_id, @l := @l + 1 AS lvl FROM (SELECT @r := ".$input_params["product_categories_id"].", @l := 0) vars, mst_categories h WHERE @r <> 0) T1 JOIN mst_categories T2 ON T1._id = T2.iMstCategoriesId JOIN trn_clean_url T3 ON T1._id=T3.iResourceId AND eResourceType=\"C\" ORDER BY T1.lvl DESC LIMIT 1";
            $result_obj = $this->db->query($sql_query);
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }

            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_1 method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query_1($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("mst_categories AS mc");

            $select_fields = array();
            $this->db->select("mc.fCommission AS mc_commission");
            if ($input_params['desired_cat_id'] != "")
            {
                $this->db->where("mc.iMstCategoriesId =", $input_params['desired_cat_id']);
            }
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }

            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition_2 method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition_2($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["mp_allow_max_purchase"];
            $cc_ro_0 = $input_params["iQuantity"];

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("integer", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("integer", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("ge", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * condition_1 method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition_1($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["mp_stock"];
            $cc_ro_0 = $input_params["iQuantity"];

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("integer", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("integer", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("ge", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array())
    {
        try
        {

            $cc_lo_0 = (empty($input_params["query"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
