<?php
/**
 * Description of setProductReview Model
 *
 * @module setProductReview
 *
 * @class model_set_product_review.php
 *
 * @path applicationront\webservice\models\model_set_product_review.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_set_product_review extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * insert_query_rating method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function insert_query_rating($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $iMstProductsId = $input_params['iMstProductsId'];
            $iLogedUserId = $input_params['iLogedUserId'];
            $vName = $input_params['vName'];
            $vEmail = $input_params['vEmail'];
            $iRate = $input_params['iRate'];
            $tReview = $input_params['tReview'];
            $dDate = "NOW()";
            $eStatus = "Active";
            if (isset($input_params['iMstProductsId'])) {
                $this->db->set("iMstProductsId", $iMstProductsId);
            }
            if (isset($input_params['iLogedUserId'])) {
                $this->db->set("iLogedUserId", $iLogedUserId);
            }
            if (isset($input_params['vName'])) {
                $this->db->set("vName", $vName);
            }
            if (isset($input_params['vEmail'])) {
                $this->db->set("vEmail", $vEmail);
            }
            if (isset($input_params['iRate'])) {
                $this->db->set("iRate", $iRate);
            }
            if (isset($input_params['tReview'])) {
                $this->db->set("tReview", $tReview);
            }
            $this->db->set("dDate", $dDate, FALSE);
            $this->db->set("eStatus", $eStatus);
            $this->db->insert("trn_product_rating");
            $insert_id = $this->db->insert_id();
            if (!$insert_id) {
                throw new Exception("Failure in insertion.");
            }
            $result_param = "insert_id";
            $result_arr[$result_param] = $insert_id;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $iTotalRate = "iTotalRate + 1";
            if ($input_params['iMstProductsId'] != "") {
                $this->db->where("iMstProductsId =", $input_params['iMstProductsId']);
            }

            $this->db->set("iTotalRate", $iTotalRate, FALSE);
            $res = $this->db->update("mst_products");
            $affected_rows = $this->db->affected_rows();
            if (!$res || $affected_rows == -1) {
                throw new Exception("Failure in updation.");
            }
            $result_param = "affected_rows";
            $result_arr[$result_param] = $affected_rows;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->db->flush_cache();
        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_1 method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query_1($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("trn_product_rating AS tpr");

            $select_fields = array();
            $this->db->select("SUM(tpr.iRate) AS tpr_rate");
            $this->db->select("COUNT(tpr.iTrnProductRatingId) AS tpr_trn_product_rating_id");
            $this->db->select("(SUM(tpr.iRate) /COUNT(tpr.iTrnProductRatingId)) AS avg", FALSE);
            if ($input_params['iMstProductsId'] != "") {
                $this->db->where("tpr.iMstProductsId =", $input_params['iMstProductsId']);
            }

            $this->db->group_by(array("tpr.iMstProductsId"));
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * update_fratingavg method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function update_fratingavg($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $fRatingAvg = $input_params['avg'];
            if ($input_params['iMstProductsId'] != "") {
                $this->db->where("iMstProductsId =", $input_params['iMstProductsId']);
            }
            if (isset($input_params['avg'])) {
                $this->db->set("fRatingAvg", $fRatingAvg);
            }
            $res = $this->db->update("mst_products");
            $affected_rows = $this->db->affected_rows();
            if (!$res || $affected_rows == -1) {
                throw new Exception("Failure in updation.");
            }
            $result_param = "affected_rows1";
            $result_arr[$result_param] = $affected_rows;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->db->flush_cache();
        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["insert_query_rating"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
