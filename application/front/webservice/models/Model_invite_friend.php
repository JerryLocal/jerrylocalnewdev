<?php
/**
 * Description of InviteFriend Model
 *
 * @module InviteFriend
 *
 * @class model_invite_friend.php
 *
 * @path applicationront\webservice\models\model_invite_friend.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_invite_friend extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * email_notification method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function email_notification($input_params = array()) {
        try {

            $email_arr["vEmail"] = $input_params["ToEmail"];
            $email_arr["vFromEmail"] = $input_params["FromEmail"];
            $email_arr["vFromName"] = $input_params["FromEmail"];
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "Invitation for the Jerry Local";

            $email_arr["vContent"] = "Hello Friends ,
<br/>
Message : #Subject# <br/>
<a href='#SiteUrl#' /> Jerry Local </a>";
            $email_arr["vContent"] = $this->general->getReplacedInputParams($email_arr["vContent"], $input_params);

            $success = $this->general->CISendMail($email_arr["vEmail"], $email_arr["vSubject"], $email_arr["vContent"], $email_arr["vFromEmail"], $email_arr["vFromName"], $email_arr["vCCEmail"], $email_arr["vBCCEmail"]);

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success) {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success) {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
