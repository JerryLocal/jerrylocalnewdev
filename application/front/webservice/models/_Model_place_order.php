<?php
/**
 * Description of placeOrder Model
 *
 * @module placeOrder
 *
 * @class model_place_order.php
 *
 * @path applicationront\webservice\models\model_place_order.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_place_order extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $vBuyerPhone = $input_params['vBuyerPhone'];
            $vBuyerPinCode = $input_params['vBuyerPinCode'];
            $iBuyerCountryId = $input_params['iBuyerCountryId'];
            $iBuyerStateId = $input_params['iBuyerStateId'];
            $iBuyerCityId = $input_params['iBuyerCityId'];
            $vBuyerArea = $input_params['vBuyerArea'];
            $vBuyerAddress2 = $input_params['vBuyerAddress2'];
            $vBuyerAddress1 = $input_params['vBuyerAddress1'];
            $vBuyerName = $input_params['vBuyerName'];
            $dDate = "NOW()";
            $ePaymentStatus = $input_params['ePaymentStatus'];
            $fShippingCost = $input_params['fShippingCost'];
            $fSubTotal = $input_params['fSubTotal'];
            $fOrderTotal = $input_params['fOrderTotal'];
            $iBuyerId = $input_params['iBuyerId'];
            $vBuyerEmail = $input_params['vBuyerEmail'];
            $vBuyerIp = $input_params['vBuyerIp'];
            $tPaymentDetail = $input_params['tPaymentDetail'];
            $vOrderNumberPre = $this->config->item('ORDER_NUMBER_PREFIX');
            $vBuyerCountry = $input_params['vBuyerCountry'];
            $vBuyerState = $input_params['vBuyerState'];
            $vBuyerCity = $input_params['vBuyerCity'];
            if (isset($input_params['vBuyerPhone'])) {
                $this->db->set("vBuyerPhone", $vBuyerPhone);
            }
            if (isset($input_params['vBuyerPinCode'])) {
                $this->db->set("vBuyerPinCode", $vBuyerPinCode);
            }
            if (isset($input_params['iBuyerCountryId'])) {
                $this->db->set("iBuyerCountryId", $iBuyerCountryId);
            }
            if (isset($input_params['iBuyerStateId'])) {
                $this->db->set("iBuyerStateId", $iBuyerStateId);
            }
            if (isset($input_params['iBuyerCityId'])) {
                $this->db->set("iBuyerCityId", $iBuyerCityId);
            }
            if (isset($input_params['vBuyerArea'])) {
                $this->db->set("vBuyerArea", $vBuyerArea);
            }
            if (isset($input_params['vBuyerAddress2'])) {
                $this->db->set("vBuyerAddress2", $vBuyerAddress2);
            }
            if (isset($input_params['vBuyerAddress1'])) {
                $this->db->set("vBuyerAddress1", $vBuyerAddress1);
            }
            if (isset($input_params['vBuyerName'])) {
                $this->db->set("vBuyerName", $vBuyerName);
            }
            $this->db->set("dDate", $dDate, FALSE);
            if (isset($input_params['ePaymentStatus'])) {
                $this->db->set("ePaymentStatus", $ePaymentStatus);
            }
            if (isset($input_params['fShippingCost'])) {
                $this->db->set("fShippingCost", $fShippingCost);
            }
            if (isset($input_params['fSubTotal'])) {
                $this->db->set("fSubTotal", $fSubTotal);
            }
            if (isset($input_params['fOrderTotal'])) {
                $this->db->set("fOrderTotal", $fOrderTotal);
            }
            if (isset($input_params['iBuyerId'])) {
                $this->db->set("iBuyerId", $iBuyerId);
            }
            if (isset($input_params['vBuyerEmail'])) {
                $this->db->set("vBuyerEmail", $vBuyerEmail);
            }
            if (isset($input_params['vBuyerIp'])) {
                $this->db->set("vBuyerIp", $vBuyerIp);
            }
            if (isset($input_params['tPaymentDetail'])) {
                $this->db->set("tPaymentDetail", $tPaymentDetail);
            }
            $this->db->set("vOrderNumberPre", $vOrderNumberPre);
            if (isset($input_params['vBuyerCountry'])) {
                $this->db->set("vBuyerCountry", $vBuyerCountry);
            }
            if (isset($input_params['vBuyerState'])) {
                $this->db->set("vBuyerState", $vBuyerState);
            }
            if (isset($input_params['vBuyerCity'])) {
                $this->db->set("vBuyerCity", $vBuyerCity);
            }
            $this->db->insert("mst_order");
            $insert_id = $this->db->insert_id();
            if (!$insert_id) {
                throw new Exception("Failure in insertion.");
            }
            $result_param = "order_id";
            $result_arr[$result_param] = $insert_id;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_2 method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query_2($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $iOrderNumber = "";
            if (method_exists($this->general, "generateOrderNumber")) {
                $iOrderNumber = $this->general->generateOrderNumber($iOrderNumber, $input_params);
            }
            if ($input_params['order_id'] != "") {
                $this->db->where("iMstOrderId =", $input_params['order_id']);
            }

            $this->db->set("iOrderNumber", $iOrderNumber);
            $res = $this->db->update("mst_order");
            $affected_rows = $this->db->affected_rows();
            if (!$res || $affected_rows == -1) {
                throw new Exception("Failure in updation.");
            }
            $result_param = "affected_rows";
            $result_arr[$result_param] = $affected_rows;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->db->flush_cache();
        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_1 method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query_1($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $iMstOrderId = $input_params['order_id'];
            $iMstStoreDetailId = $input_params['Item_iMstStoreDetailId'];
            $iMstProductsId = $input_params['Item_iMstProductsId'];
            $vProductName = $input_params['Item_vProductName'];
            $vProductSku = $input_params['Item_vProductSku'];
            $fProductRegularPrice = $input_params['Item_fProductRegularPrice'];
            $fProductSalePrice = $input_params['Item_fProductSalePrice'];
            $fProductPrice = $input_params['Item_fProductPrice'];
            $iProductQty = $input_params['Item_iProductQty'];
            $tProductOption = $input_params['Item_tProductOption'];
            $tExtraInfo = $input_params['Item_tExtraInfo'];
            $fTotalCost = $input_params['Item_fTotalCost'];
            $fShippingCost = $input_params['Item_fShippingCost'];
            $fGatewayTDRPer = $input_params['Item_fGatewayTDRPer'];
            $fAdminCommision = $input_params['Item_fAdminCommision'];
            $fAdminCommisionPer = $input_params['Item_fAdminCommisionPer'];
            $fGatewayTDR = $input_params['Item_fGatewayTDR'];
            $vSubOrderNumberPre = $this->config->item('SUB_ORDER_NUMBER_PREFIX');
            $eActionTakenBy = "Buyer";
            $iSubOrderNumber = "";
            if (method_exists($this->general, "generateSubOrderNumber")) {
                $iSubOrderNumber = $this->general->generateSubOrderNumber($iSubOrderNumber, $input_params);
            }
            if (isset($input_params['order_id'])) {
                $this->db->set("iMstOrderId", $iMstOrderId);
            }
            if (isset($input_params['Item_iMstStoreDetailId'])) {
                $this->db->set("iMstStoreDetailId", $iMstStoreDetailId);
            }
            if (isset($input_params['Item_iMstProductsId'])) {
                $this->db->set("iMstProductsId", $iMstProductsId);
            }
            if (isset($input_params['Item_vProductName'])) {
                $this->db->set("vProductName", $vProductName);
            }
            if (isset($input_params['Item_vProductSku'])) {
                $this->db->set("vProductSku", $vProductSku);
            }
            if (isset($input_params['Item_fProductRegularPrice'])) {
                $this->db->set("fProductRegularPrice", $fProductRegularPrice);
            }
            if (isset($input_params['Item_fProductSalePrice'])) {
                $this->db->set("fProductSalePrice", $fProductSalePrice);
            }
            if (isset($input_params['Item_fProductPrice'])) {
                $this->db->set("fProductPrice", $fProductPrice);
            }
            if (isset($input_params['Item_iProductQty'])) {
                $this->db->set("iProductQty", $iProductQty);
            }
            if (isset($input_params['Item_tProductOption'])) {
                $this->db->set("tProductOption", $tProductOption);
            }
            if (isset($input_params['Item_tExtraInfo'])) {
                $this->db->set("tExtraInfo", $tExtraInfo);
            }
            if (isset($input_params['Item_fTotalCost'])) {
                $this->db->set("fTotalCost", $fTotalCost);
            }
            if (isset($input_params['Item_fShippingCost'])) {
                $this->db->set("fShippingCost", $fShippingCost);
            }
            if (isset($input_params['Item_fGatewayTDRPer'])) {
                $this->db->set("fGatewayTDRPer", $fGatewayTDRPer);
            }
            if (isset($input_params['Item_fAdminCommision'])) {
                $this->db->set("fAdminCommision", $fAdminCommision);
            }
            if (isset($input_params['Item_fAdminCommisionPer'])) {
                $this->db->set("fAdminCommisionPer", $fAdminCommisionPer);
            }
            if (isset($input_params['Item_fGatewayTDR'])) {
                $this->db->set("fGatewayTDR", $fGatewayTDR);
            }
            $this->db->set("vSubOrderNumberPre", $vSubOrderNumberPre);
            $this->db->set("eActionTakenBy", $eActionTakenBy);
            $this->db->set("iSubOrderNumber", $iSubOrderNumber);
            $this->db->insert("mst_sub_order");
            $insert_id = $this->db->insert_id();
            if (!$insert_id) {
                throw new Exception("Failure in insertion.");
            }
            $result_param = "sub_order_id";
            $result_arr[$result_param] = $insert_id;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["query"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
