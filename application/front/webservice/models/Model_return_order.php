<?php
/**
 * Description of returnOrder Model
 *
 * @module returnOrder
 *
 * @class model_return_order.php
 *
 * @path applicationront\webservice\models\model_return_order.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_return_order extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * get_order_other_detail method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_order_other_detail($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_sub_order AS mso");
            $this->db->join("mst_order AS mo", "mso.iMstOrderId = mo.iMstOrderId", "left");
            $this->db->join("mst_store_detail AS msd", "mso.iMstStoreDetailId = msd.iMstStoreDetailId", "left");

            $select_fields = array();
            $this->db->select("mso.iMstOrderId AS mso_mst_order_id");
            $this->db->select("mso.iMstSubOrderId AS mso_mst_sub_order_id");
            $this->db->select("mso.iMstStoreDetailId AS mso_mst_store_detail_id");
            $this->db->select("mo.iBuyerId AS mo_buyer_id");
            $this->db->select("msd.iAdminId AS msd_admin_id");
            if ($input_params['iOrderId'] != "") {
                $this->db->where("mso.iMstSubOrderId =", $input_params['iOrderId']);
            }

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $batch_loop_arr = $input_params["get_order_other_detail"];
            if (!is_array($batch_loop_arr) || count($batch_loop_arr) == 0) {
                throw new Exception("Batch insertion data not found.");
            }
            $org_input_params = $input_params;
            $batch_ins_arr = array();
            $batch_loop_count = count($batch_loop_arr);
            for ($i = 0; $i < $batch_loop_count; $i++) {
                $temp_ins_arr = array();
                if (is_array($batch_loop_arr[$i])) {
                    $input_params = $batch_loop_arr[$i]+$org_input_params;
                } else {
                    $input_params = $org_input_params;
                }
                $input_params['i'] = $i;

                $vRequestImg = $input_params['vImgName'];
                $tRequestDetail = $input_params['tRequestDetail'];
                $dRequestDate = "NOW()";
                $iCustomerId = $input_params['mo_buyer_id'];
                $iSellerId = $input_params['msd_admin_id'];
                $iMstStoreDetailId = $input_params['mso_mst_store_detail_id'];
                $iSubOrderId = $input_params['mso_mst_sub_order_id'];
                $iOrderId = $input_params['mso_mst_order_id'];
                $ePreference = $input_params['ePreference'];
                $eRquestType = "Pending";
                $temp_ins_arr[$this->db->protect("vRequestImg")] = $this->db->escape($vRequestImg);
                $temp_ins_arr[$this->db->protect("tRequestDetail")] = $this->db->escape($tRequestDetail);
                $temp_ins_arr[$this->db->protect("dRequestDate")] = $dRequestDate;
                $temp_ins_arr[$this->db->protect("iCustomerId")] = $this->db->escape($iCustomerId);
                $temp_ins_arr[$this->db->protect("iSellerId")] = $this->db->escape($iSellerId);
                $temp_ins_arr[$this->db->protect("iMstStoreDetailId")] = $this->db->escape($iMstStoreDetailId);
                $temp_ins_arr[$this->db->protect("iSubOrderId")] = $this->db->escape($iSubOrderId);
                $temp_ins_arr[$this->db->protect("iOrderId")] = $this->db->escape($iOrderId);
                $temp_ins_arr[$this->db->protect("ePreference")] = $this->db->escape($ePreference);
                $assign_arr[$i]["eRquestType"] = $eRquestType;
                $temp_ins_arr[$this->db->protect("eRquestType")] = $this->db->escape($eRquestType);

                $batch_ins_arr[] = $temp_ins_arr;
            }

            $affected_rows = $this->db->insert_batch($this->db->protect("trn_rma"), $batch_ins_arr, FALSE);
            if (!$affected_rows) {
                throw new Exception("Failure in insertion.");
            }

            $result_param = "insert_id";
            $result_arr[$result_param] = $affected_rows;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * check_value method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function check_value($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["get_order_other_detail"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
