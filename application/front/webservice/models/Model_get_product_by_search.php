<?php
/**
 * Description of getProductBySearch Model
 *
 * @module getProductBySearch
 *
 * @class model_get_product_by_search.php
 *
 * @path applicationront\webservice\models\model_get_product_by_search.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_product_by_search extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }
}
