<?php
/**
 * Description of orderDetail Model
 *
 * @module orderDetail
 *
 * @class model_order_detail.php
 *
 * @path applicationront\webservice\models\model_order_detail.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_order_detail extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * get_ordr_data_with_suborder method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_ordr_data_with_suborder($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_order AS mo");
            $this->db->join("mst_sub_order AS mso", "mo.iMstOrderId = mso.iMstOrderId", "left");
            $this->db->join("mst_products AS mp", "mso.iMstProductsId = mp.iMstProductsId", "left");
            $this->db->join("mst_store_detail AS msd", "mso.iMstStoreDetailId = msd.iMstStoreDetailId", "left");
            $this->db->join("mst_shipper AS ms", "mso.iMstShipperId = ms.iMstShipperId", "left");
            $this->db->join("mod_country AS mc", "mo.iBuyerCountryId = mc.iCountryId", "left");
            $this->db->join("mod_state AS ms1", "mo.iBuyerStateId = ms1.iStateId", "left");
            $this->db->join("mod_city AS mc1", "mo.iBuyerCityId = mc1.iCityId", "left");
            $this->db->join("trn_order_status_history AS tosh", "mso.iMstSubOrderId = tosh.iMstSubOrderId", "left");

            $select_fields = array();
            $this->db->select("mo.iMstOrderId AS mo_mst_order_id");
            $this->db->select("mo.vBuyerPhone AS mo_buyer_phone");
            $this->db->select("mo.vBuyerPinCode AS mo_buyer_pin_code");
            $this->db->select("mo.vBuyerArea AS mo_buyer_area");
            $this->db->select("mo.vBuyerAddress1 AS mo_buyer_address1");
            $this->db->select("mo.fOrderTotal AS mo_order_total");
            $this->db->select("mo.dDate AS mo_date");
            $this->db->select("mo.vBuyerEmail AS mo_buyer_email");
            $this->db->select("mso.iMstSubOrderId AS mso_mst_sub_order_id");
            $this->db->select("mso.vProductName AS mso_product_name");
            $this->db->select("mso.fTotalCost AS mso_total_cost");
            $this->db->select("mso.eItemStatus AS mso_item_status");
            $this->db->select("mso.vTrackingNumber AS mso_tracking_number");
            $this->db->select("mso.vShipperName AS mso_shipper_name");
            $this->db->select("mp.vDefaultImg AS mp_default_img");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("ms.vUrl AS ms_url");
            $this->db->select("mc.vCountry AS mc_country");
            $this->db->select("ms1.vState AS ms1_state");
            $this->db->select("mc1.vCity AS mc1_city");
            $this->db->select("mo.ePaymentStatus AS mo_payment_status");
            $this->db->select("mo.vBuyerName AS mo_buyer_name");
            $this->db->select("mo.vBuyerIp AS mo_buyer_ip");
            $this->db->select("tosh.dDate AS tosh_date");
            $this->db->select("tosh.eOrderStatus AS tosh_order_status");
            $this->db->select("mso.tShippingRemark AS mso_shipping_remark");
            $this->db->select("mso.iMstProductsId AS mso_mst_products_id");
            $this->db->select("mp.iDifferencePer AS mp_difference_per");
            $this->db->select("mo.fShippingCost AS mo_shipping_cost");
            $this->db->select("(mso.fProductRegularPrice - mso.fProductSalePrice ) AS Discount_price", FALSE);
            $this->db->select("mso.fProductSalePrice AS mso_product_sale_price");
            $this->db->select("mso.fProductRegularPrice AS mso_product_regular_price");
            $this->db->select("mso.fProductPrice AS mso_product_price");
            $this->db->select("mso.iProductQty AS mso_product_qty");
            $this->db->select("mso.fShippingCost AS mso_shipping_cost");
            $this->db->select("mso.iMstStoreDetailId AS mso_mst_store_detail_id");
            if ($input_params['iMstOrderId'] != "") {
                $this->db->where("mo.iMstOrderId =", $input_params['iMstOrderId']);
            }

            $this->db->order_by("mo.dDate", "asc");
            $this->db->order_by("mso.iMstStoreDetailId", "asc");

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["mo_date"];
                    if (method_exists($this->general, "dateSystemFormat")) {
                        $data = $this->general->dateSystemFormat($data, $data_arr, $i);
                    }
                    $result_arr[$data_key]["mo_date"] = $data;

                    $data = $data_arr["mp_default_img"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = @implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["height"] = "50";
                    $image_arr["width"] = "50";
                    $image_arr["color"] = "FFFFFF";
                    $image_arr["path"] = $this->general->getImageNestedFolders("p_img");
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["mp_default_img"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["get_ordr_data_with_suborder"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
