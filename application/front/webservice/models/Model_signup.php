<?php
/**
 * Description of signup Model
 *
 * @module signup
 *
 * @class model_signup.php
 *
 * @path applicationront\webservice\models\model_signup.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_signup extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mod_customer AS mc");

            $select_fields = array();
            $this->db->select("COUNT(mc.vEmail) AS mc_email");
            if ($input_params['email'] != "") {
                $this->db->where("mc.vEmail =", $input_params['email']);
            }
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * create_customer method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function create_customer($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $vFirstName = $input_params['firstname'];
            $vLastName = $input_params['lastname'];
            $vEmail = $input_params['email'];
            $vUserName = $input_params['email'];
            $vPassword = $input_params['password'];
            $dtRegisteredDate = "NOW()";
            $eStatus = "Active";
            if (isset($input_params['firstname'])) {
                $this->db->set("vFirstName", $vFirstName);
            }
            if (isset($input_params['lastname'])) {
                $this->db->set("vLastName", $vLastName);
            }
            if (isset($input_params['email'])) {
                $this->db->set("vEmail", $vEmail);
            }
            if (isset($input_params['email'])) {
                $this->db->set("vUserName", $vUserName);
            }
            if (isset($input_params['password'])) {
                $this->db->set("vPassword", $vPassword);
            }
            $this->db->set("dtRegisteredDate", $dtRegisteredDate, FALSE);
            $this->db->set("eStatus", $eStatus);
            $this->db->insert("mod_customer");
            $insert_id = $this->db->insert_id();
            if (!$insert_id) {
                throw new Exception("Failure in insertion.");
            }
            $result_param = "insert_id";
            $result_arr[$result_param] = $insert_id;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * user_reg_email method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function user_reg_email($input_params = array()) {
        try {

            $email_arr["vEmail"] = $input_params["email"];
            $email_arr["vFromEmail"] = "cit.email003@gmail.com";
            $email_arr["vFromName"] = "Jerry Local Market Place";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "Your Account is Created With US";

            $email_arr["firstname"] = $input_params["firstname"];
            $email_arr["lastname"] = $input_params["lastname"];
            $email_arr["vEmail"] = $input_params["email"];
            $email_arr["password"] = $input_params["password"];
            $email_arr["SYSTEM.COMPANY_NAME"] = "'{%SYSTEM.COMPANY_NAME%}' ";

            $success = $this->general->sendMail($email_arr, "USER_REG_FRONT");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success) {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success) {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * chk_insert_rcd method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function chk_insert_rcd($input_params = array()) {
        try {

            $cc_lo_0 = $input_params["insert_id"];
            $cc_ro_0 = "0";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("integer", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("integer", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("gt", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = $input_params["mc_email"];
            $cc_ro_0 = "0";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("integer", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("integer", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
