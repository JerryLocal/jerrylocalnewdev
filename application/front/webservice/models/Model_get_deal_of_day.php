<?php
/**
 * Description of getDealOfDay Model
 *
 * @module getDealOfDay
 *
 * @class model_get_deal_of_day.php
 *
 * @path applicationront\webservice\models\model_get_deal_of_day.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_deal_of_day extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * deal_of_the_day method is used to execute database queries.
     * Get Daily deals
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function deal_of_the_day($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("trn_dealoftheday AS td");

            $select_fields = array();
            $this->db->select("td.dDate AS td_date");
            $this->db->select("td.iMstProductsId AS td_mst_products_id");
            $this->db->where("td.eStatus = 'Active'", FALSE, FALSE);

            $this->db->order_by("td.iMstProductsId", "desc");
            if (intval("10") > 0) {
                $this->db->limit("10");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * get_product_details method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_product_details($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_products AS mp");
            $this->db->join("mst_store_detail AS msd", "mp.iStoreId = msd.iMstStoreDetailId", "left");

            $select_fields = array();
            $this->db->select("mp.vTitle AS mp_title");
            $this->db->select("mp.fSalePrice AS mp_sale_price");
            $this->db->select("mp.fRegularPrice AS mp_regular_price");
            $this->db->select("mp.fRatingAvg AS mp_rating_avg");
            $this->db->select("mp.iDifferencePer AS mp_difference_per");
            $this->db->select("mp.iTotalRate AS mp_total_rate");
            $this->db->select("mp.dDate AS mp_date");
            $this->db->select("mp.vDefaultImg AS mp_default_img");
            $this->db->select("mp.iWishlistState AS mp_wishlist_state");
            $this->db->select("mp.iSaleState AS mp_sale_state");
            $this->db->select("mp.iStock AS mp_stock");
            if ($input_params['td_mst_products_id'] != "") {
                $this->db->where("mp.iMstProductsId =", $input_params['td_mst_products_id']);
            }
            $this->db->where_in("msd.eStatus", array('Active'));
            $this->db->where_in("mp.eStatus", array('Active'));

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["mp_default_img"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = @implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["height"] = "150";
                    $image_arr["width"] = "150";
                    $image_arr["color"] = "FFFFFF";
                    $image_arr["path"] = $this->general->getImageNestedFolders("p_img");
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["mp_default_img"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * chk_has_deal method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function chk_has_deal($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["deal_of_the_day"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
