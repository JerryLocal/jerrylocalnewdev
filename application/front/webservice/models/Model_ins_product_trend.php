<?php
/**
 * Description of insProductTrend Model
 *
 * @module insProductTrend
 *
 * @class model_ins_product_trend.php
 *
 * @path applicationront\webservice\models\model_ins_product_trend.php
 *
 * @author CIT Dev Team
 *
 * @date 11.01.2016
 */

class Model_ins_product_trend extends CI_Model
{
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * checkproductinviewcount method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function checkproductinviewcount($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("trn_view_count AS tvc");

            $select_fields = array();
            $this->db->select("COUNT(tvc.iTrnViewCountId) AS tvc_trn_view_count_id");
            $this->db->select("tvc.vIp AS tvc_ip");
            $this->db->select("tvc.iProductId AS tvc_product_id");
            if ($input_params['ip'] != "")
            {
                $this->db->where("tvc.vIp =", $input_params['ip']);
            }
            if ($input_params['pid'] != "")
            {
                $this->db->where("tvc.iProductId =", $input_params['pid']);
            }
            if (intval("1") > 0)
            {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }

            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $vIp = $input_params['ip'];
            $iProductId = $input_params['pid'];
            if (isset($input_params['ip']))
            {
                $this->db->set("vIp", $vIp);
            }
            if (isset($input_params['pid']))
            {
                $this->db->set("iProductId", $iProductId);
            }
            $this->db->insert("trn_view_count");
            $insert_id = $this->db->insert_id();
            if (!$insert_id)
            {
                throw new Exception("Failure in insertion.");
            }
            $result_param = "insert_id";
            $result_arr[$result_param] = $insert_id;
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * update_view method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function update_view($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $iViewState = "iViewState+1";
            if ($input_params['pid'] != "")
            {
                $this->db->where("iMstProductsId =", $input_params['pid']);
            }

            $this->db->set("iViewState", $iViewState, FALSE);
            $res = $this->db->update("mst_products");
            $affected_rows = $this->db->affected_rows();
            if (!$res || $affected_rows == -1)
            {
                throw new Exception("Failure in updation.");
            }
            $result_param = "affected_rows1";
            $result_arr[$result_param] = $affected_rows;
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->db->flush_cache();
        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * chk_count method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function chk_count($input_params = array())
    {
        try
        {

            $cc_lo_0 = $input_params["tvc_trn_view_count_id"];
            $cc_ro_0 = "0";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("integer", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("integer", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
