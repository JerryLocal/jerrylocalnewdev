<?php
/**
 * Description of setWishlist Model
 *
 * @module setWishlist
 *
 * @class model_set_wishlist.php
 *
 * @path applicationront\webservice\models\model_set_wishlist.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_set_wishlist extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * check_wishlist method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function check_wishlist($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("trn_product_wishlist AS tpw");

            $select_fields = array();
            $this->db->select("tpw.iAdminId AS tpw_admin_id");
            $this->db->select("tpw.iMstProductsId AS tpw_mst_products_id");
            if ($input_params['iProductId'] != "") {
                $this->db->where("tpw.iMstProductsId =", $input_params['iProductId']);
            }
            if ($input_params['iAdminId'] != "") {
                $this->db->where("tpw.iAdminId =", $input_params['iAdminId']);
            }

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * insert_query_wishlist method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function insert_query_wishlist($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $iMstProductsId = $input_params['iProductId'];
            $iAdminId = $input_params['iAdminId'];
            $dDate = "NOW()";
            if (isset($input_params['iProductId'])) {
                $this->db->set("iMstProductsId", $iMstProductsId);
            }
            if (isset($input_params['iAdminId'])) {
                $this->db->set("iAdminId", $iAdminId);
            }
            $this->db->set("dDate", $dDate, FALSE);
            $this->db->insert("trn_product_wishlist");
            $insert_id = $this->db->insert_id();
            if (!$insert_id) {
                throw new Exception("Failure in insertion.");
            }
            $result_param = "insert_id";
            $result_arr[$result_param] = $insert_id;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * wishlistinserted method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function wishlistinserted($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["insert_query_wishlist"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * wishlistcheck method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function wishlistcheck($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["check_wishlist"]) ? 0 : 1);
            $cc_ro_0 = "0";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
