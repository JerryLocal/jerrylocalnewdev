<?php
/**
 * Description of getNewsDetail Model
 *
 * @module getNewsDetail
 *
 * @class model_get_news_detail.php
 *
 * @path applicationront\webservice\models\model_get_news_detail.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_news_detail extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_news AS mn");

            $select_fields = array();
            $this->db->select("mn.iMstNewsId AS mn_mst_news_id");
            $this->db->select("mn.vTitle AS mn_title");
            $this->db->select("mn.tDescription AS mn_description");
            $this->db->select("DATE_FORMAT(mn.dDate,'%m/%d/%Y') AS mn_date");
            if ($input_params['iNewsId'] != "") {
                $this->db->where("mn.iMstNewsId =", $input_params['iNewsId']);
            }
            $this->db->where("mn.eStatus='Active'", FALSE, FALSE);
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }
}
