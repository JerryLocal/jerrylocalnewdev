<?php
/**
 * Description of getSalesStatistics Model
 *
 * @module getSalesStatistics
 *
 * @class model_get_sales_statistics.php
 *
 * @path applicationront\webservice\models\model_get_sales_statistics.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_sales_statistics extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * total_user_query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function total_user_query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mod_admin AS ma");

            $select_fields = array();
            $this->db->select("COUNT(ma.iAdminId) AS Total_user");

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * live_user_query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function live_user_query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mod_admin AS ma");

            $select_fields = array();
            $this->db->select("COUNT(ma.iAdminId) AS live_user");
            $this->db->where("ma.eStatus = 'Active'", FALSE, FALSE);

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }
}
