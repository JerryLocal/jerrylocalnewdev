<?php
/**
 * Description of getFaq Model
 *
 * @module getFaq
 *
 * @class model_get_faq.php
 *
 * @path applicationront\webservice\models\model_get_faq.php
 *
 * @author CIT Dev Team
 *
 * @date 24.12.2015
 */

class Model_get_faq extends CI_Model
{
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * faq_category method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function faq_category($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("mst_faq_category AS mfc");

            $select_fields = array();
            $this->db->select("mfc.vTitle AS mfc_title");
            $this->db->select("mfc.iMstFaqCategoryId AS mfc_mst_faq_category_id");
            $this->db->select("mfc.eType AS mfc_type");
            $this->db->where_in("mfc.eStatus", array('Active'));
            if ((!is_array($input_params['Type']) && $input_params['Type'] != "") || (is_array($input_params['Type']) && !empty($input_params['Type'])))
            {
                if (!is_array($input_params['Type']))
                {
                    $input_params['Type'] = explode(",", $input_params['Type']);
                }
                $this->db->where_in("mfc.eType", $input_params['Type']);
            }

            $this->db->order_by("mfc.iOrder", "asc");

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }

            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * item_qry method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function item_qry($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("mst_faq AS mf");

            $select_fields = array();
            $this->db->select("mf.vTitle AS mf_title");
            $this->db->select("mf.tDescription AS mf_description");
            $this->db->select("mf.iMstFaqId AS mf_mst_faq_id");
            $this->db->where("mf.eStatus = ('Active')", FALSE, FALSE);
            if ($input_params['mfc_mst_faq_category_id'] != "")
            {
                $this->db->where("mf.iMstFaqCategoryId =", $input_params['mfc_mst_faq_category_id']);
            }

            $this->db->order_by("mf.iOrderId", "asc");

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }

            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array())
    {
        try
        {

            $cc_lo_0 = (empty($input_params["faq_category"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
