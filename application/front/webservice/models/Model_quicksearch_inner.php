<?php
/**
 * Description of quicksearchInner Model
 *
 * @module quicksearchInner
 *
 * @class model_quicksearch_inner.php
 *
 * @path applicationront\webservice\models\model_quicksearch_inner.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_quicksearch_inner extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * product_max_price_query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function product_max_price_query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_products AS p");

            $select_fields = array();
            $this->db->select("MAX(p.fSalePrice) AS p_sale_price");
            if ($input_params['vDetail'] != "") {
                $this->db->like("p.vTitle", $input_params['vDetail'], "both");
            }

            $this->db->order_by("p.fSalePrice", "desc");
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * product_search method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function product_search($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->start_cache();
            $this->db->from("mst_products AS mp");
            $this->db->join("mst_store_detail AS msd", "mp.iStoreId = msd.iMstStoreDetailId", "left");

            $select_fields = array();
            $this->db->select("mp.vTitle AS mp_title");
            $this->db->select("mp.fRegularPrice AS mp_regular_price");
            $this->db->select("mp.vDefaultImg AS mp_default_img");
            $this->db->select("mp.iMstProductsId AS mp_mst_products_id");
            $this->db->select("(".$this->db->escape("p").") AS type", FALSE);
            $this->db->select("(".$this->db->escape("cleanurl").") AS cleanurl_product", FALSE);
            $this->db->select("mp.iStoreId AS mp_store_id");
            $this->db->select("mp.fSalePrice AS mp_sale_price");
            $this->db->select("mp.fRatingAvg AS mp_rating_avg");
            $this->db->select("mp.iStock AS mp_stock");
            $this->db->select("mp.iDifferencePer AS mp_difference_per");
            $this->db->select("mp.iTotalRate AS mp_total_rate");
            $this->db->select("mp.dDate AS mp_date");
            $this->db->select("mp.iWishlistState AS mp_wishlist_state");
            $this->db->select("mp.iSaleState AS mp_sale_state");
            $this->db->select("mp.iViewState AS mp_view_state");
            $this->db->where_in("mp.eStatus", array('Active'));
            $this->db->where_in("msd.eStatus", array('Active'));
            $this->db->where("mp.tSearchKeyword LIKE '%".$input_params["vDetail"]."%' ", FALSE, FALSE);

            $this->db->stop_cache();
            $total_records = $this->db->count_all_results();

            $settings_params['count'] = $total_records;

            $record_limit = "9";
            $record_limit = intval($record_limit);
            $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;
            $total_pages = getTotalPages($total_records, $record_limit);
            $start_index = getStartIndex($total_records, $current_page, $record_limit);
            $settings_params['curr_page'] = $current_page;
            $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
            $settings_params['next_page'] = ($current_page+1 > $total_pages) ? 0 : 1;

            $this->db->limit($record_limit, $start_index);
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            $this->db->flush_cache();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["cleanurl_product"];
                    if (method_exists($this->general, "WSCleanUrl")) {
                        $data = $this->general->WSCleanUrl($data, $data_arr, $i);
                    }
                    $result_arr[$data_key]["cleanurl_product"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["product_search"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);

            $cc_lo_1 = (empty($input_params["category_search"]) ? 0 : 1);
            $cc_ro_1 = "1";

            $cc_lo_1 = is_null($cc_lo_1) ? "" : $cc_lo_1;
            $cc_ro_1 = is_null($cc_ro_1) ? "" : $cc_ro_1;

            $cc_pl_1 = $this->general->getDataTypeWiseResult("string", $cc_lo_1, TRUE);
            $cc_pr_1 = $this->general->getDataTypeWiseResult("string", $cc_ro_1, FALSE);
            $cc_fr_1 = $this->general->compareDataValues("eq", $cc_pl_1, $cc_pr_1);
            if (!($cc_fr_0 || $cc_fr_1)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
