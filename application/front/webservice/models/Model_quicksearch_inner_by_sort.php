<?php
/**
 * Description of quicksearchInnerBySort Model
 *
 * @module quicksearchInnerBySort
 *
 * @class model_quicksearch_inner_by_sort.php
 *
 * @path applicationront\webservice\models\model_quicksearch_inner_by_sort.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_quicksearch_inner_by_sort extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }
}
