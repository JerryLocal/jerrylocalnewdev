<?php
/**
 * Description of getProduct Model
 *
 * @module getProduct
 *
 * @class model_get_product.php
 *
 * @path applicationront\webservice\models\model_get_product.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_product extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * get_product_data method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_product_data($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->start_cache();
            $this->db->from("trn_product_categories AS mp");
            $this->db->join("mst_products AS mp1", "mp.iMstProductsId = mp1.iMstProductsId", "left");

            $select_fields = array();
            $this->db->select("mp1.iMstProductsId AS mp1_mst_products_id");
            $this->db->select("mp1.iAdminId AS mp1_admin_id");
            $this->db->select("mp1.vTitle AS mp1_title");
            $this->db->select("mp1.vSku AS mp1_sku");
            $this->db->select("mp1.fRegularPrice AS mp1_regular_price");
            $this->db->select("mp1.fSalePrice AS mp1_sale_price");
            $this->db->select("mp1.fRatingAvg AS mp1_rating_avg");
            $this->db->select("mp1.iAllowMaxPurchase AS mp1_allow_max_purchase");
            $this->db->select("mp1.iLowAvlLimitNotifcation AS mp1_low_avl_limit_notifcation");
            $this->db->select("mp1.iStock AS mp1_stock");
            $this->db->select("mp1.iDifferencePer AS mp1_difference_per");
            $this->db->select("mp1.iTotalRate AS mp1_total_rate");
            $this->db->select("mp1.dDate AS mp1_date");
            $this->db->select("mp1.dModifyDate AS mp1_modify_date");
            $this->db->select("mp1.vDefaultImg AS mp1_default_img");
            $this->db->select("mp1.tDescription AS mp1_description");
            $this->db->select("mp1.iSaleState AS mp1_sale_state");
            $this->db->select("mp1.iWishlistState AS mp1_wishlist_state");
            $this->db->select("mp1.eFreeShipping AS mp1_free_shipping");
            $this->db->select("mp1.iViewState AS mp1_view_state");
            $this->db->select("mp1.iReturnDays AS mp1_return_days");
            $this->db->select("mp1.eDoReturn AS mp1_do_return");
            $this->db->select("mp1.eStatus AS mp1_status");
            $this->db->where_in("mp1.eStatus", array('Active'));
            $this->db->where("mp.iMstCategoriesId IN (".$input_params["cid"].")", FALSE, FALSE);

            $this->db->stop_cache();
            $total_records = $this->db->count_all_results();

            $settings_params['count'] = $total_records;

            $record_limit = "".$this->config->item("REC_LIMIT_FRONT")."";
            $record_limit = intval($record_limit);
            $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;
            $total_pages = getTotalPages($total_records, $record_limit);
            $start_index = getStartIndex($total_records, $current_page, $record_limit);
            $settings_params['curr_page'] = $current_page;
            $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
            $settings_params['next_page'] = ($current_page+1 > $total_pages) ? 0 : 1;

            $this->db->order_by("CASE  \"".$input_params["sort_id"]."\"
WHEN  \"1\" THEN mp1.iSaleState
WHEN  \"2\" THEN mp1.iMstProductsId
WHEN  \"3\" THEN mp1.iDifferencePer
WHEN  \"4\" THEN mp1.iViewState
END DESC,
CASE  \"".$input_params["price_id"]."\" WHEN '1' THEN  mp1.fSalePrice END DESC,
CASE  \"".$input_params["price_id"]."\" WHEN '2' THEN  mp1.fSalePrice END ASC", FALSE, FALSE);
            $this->db->limit($record_limit, $start_index);
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            $this->db->flush_cache();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["get_product_data"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
