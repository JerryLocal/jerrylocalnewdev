<?php
/**
 * Description of getFeatureProduct Model
 *
 * @module getFeatureProduct
 *
 * @class model_get_feature_product.php
 *
 * @path applicationront\webservice\models\model_get_feature_product.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_feature_product extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->start_cache();
            $this->db->from("trn_featured_products AS tfp");
            $this->db->join("mst_products AS mp", "tfp.iMstProductsId = mp.iMstProductsId", "left");

            $select_fields = array();
            $this->db->select("mp.vTitle AS mp_title");
            $this->db->select("mp.fRegularPrice AS mp_regular_price");
            $this->db->select("mp.fSalePrice AS mp_sale_price");
            $this->db->select("mp.fRatingAvg AS mp_rating_avg");
            $this->db->select("mp.iStock AS mp_stock");
            $this->db->select("mp.iDifferencePer AS mp_difference_per");
            $this->db->select("mp.iTotalRate AS mp_total_rate");
            $this->db->select("mp.dDate AS mp_date");
            $this->db->select("mp.vDefaultImg AS mp_default_img");
            $this->db->where("mp.iStock > (0)", FALSE, FALSE);
            $this->db->where("tfp.eStatus='Active'  AND  mp.eStatus='Active'", FALSE, FALSE);

            $this->db->stop_cache();
            $total_records = $this->db->count_all_results();

            $settings_params['count'] = $total_records;

            $max_rec = "".$input_params["imit"]."";
            $max_rec = intval($max_rec);
            $record_limit = "9";
            $record_limit = intval($record_limit);
            $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;
            $total_pages = getTotalPages($total_records, $record_limit);
            $start_index = getStartIndex($total_records, $current_page, $record_limit);
            $tot_count = $start_index+$record_limit;
            if (($start_index >= $max_rec) || ($start_index >= $total_records)) {
                throw new Exception('No records found.');
            }
            $record_limit = ($tot_count > $max_rec) ? $max_rec-$start_index : $record_limit;
            $settings_params['curr_page'] = $current_page;
            $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
            if ($total_records > $tot_count) {
                $settings_params['next_page'] = ($tot_count > $max_rec) ? 0 : 1;
            } else {
                $settings_params['next_page'] = 0;
            }

            $this->db->order_by("tfp.iOrderId", "asc");
            $this->db->limit($record_limit, $start_index);
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            $this->db->flush_cache();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params[""]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
