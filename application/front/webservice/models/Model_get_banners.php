<?php
/**
 * Description of getBanners Model
 *
 * @module getBanners
 *
 * @class model_get_banners.php
 *
 * @path applicationront\webservice\models\model_get_banners.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_banners extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * get_slider method is used to execute database queries.
     * To get all the slider banner
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_slider($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_banner AS mb");

            $select_fields = array();
            $this->db->select("mb.vImg AS mb_img");
            $this->db->select("mb.vLink AS mb_link");
            $this->db->where("mb.eType = 'Slider' AND mb.eStatus = 'Active' ", FALSE, FALSE);

            $this->db->order_by("mb.iOrder", "asc");

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["mb_img"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = @implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["height"] = "404";
                    $image_arr["width"] = "859";
                    $image_arr["color"] = "FFFFFF";
                    $image_arr["path"] = $this->general->getImageNestedFolders("slider_img");
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["mb_img"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * get_fix_banner method is used to execute database queries.
     * get all the banner with Fix banner
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_fix_banner($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_banner AS mb");

            $select_fields = array();
            $this->db->select("mb.vImg AS mb_img_1");
            $this->db->select("mb.iOrder AS mb_order");
            $this->db->select("mb.vLink AS mb_link_1");
            $this->db->select("mb.ePosition AS mb_position");
            $this->db->where("mb.eType = 'Fix_Banner' AND mb.eStatus = 'Active'", FALSE, FALSE);

            $this->db->order_by("mb.ePosition", "asc");

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["mb_img_1"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = @implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["path"] = $this->general->getImageNestedFolders("banner_img");
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["mb_img_1"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }
}
