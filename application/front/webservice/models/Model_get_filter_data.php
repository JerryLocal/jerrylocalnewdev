<?php
/**
 * Description of getFilterData Model
 *
 * @module getFilterData
 *
 * @class model_get_filter_data.php
 *
 * @path applicationront\webservice\models\model_get_filter_data.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_filter_data extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_category_options AS mco");
            $this->db->join("mst_category_option_values AS mcov", "mco.iMstCategoryOptionsId = mcov.iMstCategoryOptionsId", "left");
            $this->db->join("mst_options AS mo", "mco.iMstOptionsId = mo.iMstOptionsId", "left");

            $select_fields = array();
            $this->db->select("mco.iMstCategoriesId AS mco_mst_categories_id");
            $this->db->select("mco.iOrderId AS mco_order_id");
            $this->db->select("mco.iMstOptionsId AS mco_mst_options_id");
            $this->db->select("mcov.vValue AS mcov_value");
            $this->db->select("mo.vTitle AS mo_title");
            $this->db->where_in("mco.eFilter", array('Yes'));
            if ($input_params['catId'] != "") {
                $this->db->where("mco.iMstCategoriesId =", $input_params['catId']);
            }

            $this->db->order_by("mco.iOrderId", "asc");

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["query"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
