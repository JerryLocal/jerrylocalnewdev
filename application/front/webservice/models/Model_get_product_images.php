<?php
/**
 * Description of getProductImages Model
 *
 * @module getProductImages
 *
 * @class model_get_product_images.php
 *
 * @path applicationront\webservice\models\model_get_product_images.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_product_images extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query_small method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query_small($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_product_img AS mpi");

            $select_fields = array();
            $this->db->select("mpi.vImgPath AS mpi_img_path");
            $this->db->select("mpi.eDefault AS mpi_default");
            if ($input_params['pid'] != "") {
                $this->db->where("mpi.iMstProductsId =", $input_params['pid']);
            }

            $this->db->order_by("mpi.eDefault", "asc");
            $this->db->order_by("mpi.iMstProductImgId", "asc");

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["mpi_img_path"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = @implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["height"] = "110";
                    $image_arr["width"] = "110";
                    $image_arr["color"] = "FFFFFF";
                    $image_arr["path"] = $this->general->getImageNestedFolders("p_img");
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["mpi_img_path"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_mid method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query_mid($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_product_img AS mpi");

            $select_fields = array();
            $this->db->select("mpi.vImgPath AS mpi_img_path_1");
            $this->db->select("mpi.eDefault AS mpi_default_1");
            if ($input_params['pid'] != "") {
                $this->db->where("mpi.iMstProductsId =", $input_params['pid']);
            }

            $this->db->order_by("mpi.eDefault", "asc");
            $this->db->order_by("mpi.iMstProductImgId", "asc");

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["mpi_img_path_1"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = @implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["height"] = "370";
                    $image_arr["width"] = "370";
                    $image_arr["path"] = $this->general->getImageNestedFolders("p_img");
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["mpi_img_path_1"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_large method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query_large($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_product_img AS mpi");

            $select_fields = array();
            $this->db->select("mpi.vImgPath AS mpi_img_path_2");
            $this->db->select("mpi.eDefault AS mpi_default_2");
            if ($input_params['pid'] != "") {
                $this->db->where("mpi.iMstProductsId =", $input_params['pid']);
            }

            $this->db->order_by("mpi.eDefault", "asc");
            $this->db->order_by("mpi.iMstProductImgId", "asc");

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["mpi_img_path_2"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = @implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["height"] = "800";
                    $image_arr["width"] = "800";
                    $image_arr["path"] = $this->general->getImageNestedFolders("p_img");
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["mpi_img_path_2"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["query_small"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
