<?php
/**
 * Description of product_invoice Model
 *
 * @module product_invoice
 *
 * @class model_product_invoice.php
 *
 * @path applicationront\webservice\models\model_product_invoice.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_product_invoice extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * invoice_data method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function invoice_data($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_sub_order AS mso");
            $this->db->join("mst_products AS mp", "mso.iMstProductsId = mp.iMstProductsId", "left");
            $this->db->join("mst_order AS mo", "mso.iMstOrderId = mo.iMstOrderId", "left");
            $this->db->join("mod_country AS mc", "mo.iBuyerCountryId = mc.iCountryId", "left");
            $this->db->join("mod_state AS ms", "mo.iBuyerStateId = ms.iStateId", "left");
            $this->db->join("mod_city AS mc1", "mo.iBuyerCityId = mc1.iCityId", "left");
            $this->db->join("mst_store_detail AS msd", "mso.iMstStoreDetailId = msd.iMstStoreDetailId", "left");
            $this->db->join("mod_country AS mc2", "msd.iCountryId = mc2.iCountryId", "left");
            $this->db->join("mod_state AS ms1", "msd.iStateId = ms1.iStateId", "left");
            $this->db->join("mod_city AS mc3", "msd.iCityId = mc3.iCityId", "left");
            $this->db->join("mod_admin AS ma", "msd.iAdminId = ma.iAdminId", "left");

            $select_fields = array();
            $this->db->select("mso.iMstSubOrderId AS mso_mst_sub_order_id");
            $this->db->select("mso.iMstOrderId AS mso_mst_order_id");
            $this->db->select("mso.vProductName AS mso_product_name");
            $this->db->select("mso.iProductQty AS mso_product_qty");
            $this->db->select("mso.vSellerInvoiceDate AS mso_seller_invoice_date");
            $this->db->select("mso.vSellerInvoiceNo AS mso_seller_invoice_no");
            $this->db->select("mso.vShipperName AS mso_shipper_name");
            $this->db->select("mp.vTitle AS mp_title");
            $this->db->select("mo.vBuyerPhone AS mo_buyer_phone");
            $this->db->select("mo.vBuyerPinCode AS mo_buyer_pin_code");
            $this->db->select("mo.vBuyerAddress1 AS mo_buyer_address1");
            $this->db->select("mo.vBuyerArea AS mo_buyer_area");
            $this->db->select("mo.vBuyerName AS mo_buyer_name");
            $this->db->select("mo.vBuyerEmail AS mo_buyer_email");
            $this->db->select("mc.vCountry AS mc_country");
            $this->db->select("ms.vState AS ms_state");
            $this->db->select("mc1.vCity AS mc1_city");
            $this->db->select("msd.vContactName AS msd_contact_name");
            $this->db->select("msd.vContactNumber AS msd_contact_number");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("msd.vAddress1 AS msd_address1");
            $this->db->select("msd.vPinCode AS msd_pin_code");
            $this->db->select("msd.vGSTNo AS msd_g_s_t_no");
            $this->db->select("msd.vStoreUrl AS msd_store_url");
            $this->db->select("mc2.vCountry AS mc2_country_seller");
            $this->db->select("ms1.vState AS ms1_state_seller");
            $this->db->select("mc3.vCity AS mc3_city_seller");
            $this->db->select("mp.vShortDescription AS mp_short_description");
            $this->db->select("ma.vEmail AS ma_email");
            $this->db->select("mso.tShippingRemark AS mso_shipping_remark");
            $this->db->select("mso.vSubOrderNumberPre AS mso_sub_order_number_pre");
            $this->db->select("mso.iSubOrderNumber AS mso_sub_order_number");
            $this->db->select("mo.vOrderNumberPre AS mo_order_number_pre");
            $this->db->select("mo.iOrderNumber AS mo_order_number");
            $this->db->select("mp.vSku AS mp_sku");
            $this->db->select("mo.vBuyerAddress2 AS mo_buyer_address2");
            $this->db->select("msd.vAddress2 AS msd_address2");
            $this->db->select("msd.vArea AS msd_area");
            $this->db->select("mso.fShippingCost AS mso_shipping_cost");
            $this->db->select("mso.fTotalCost AS mso_total_cost");
            $this->db->select("mso.fProductSalePrice AS mso_product_sale_price");
            if ($input_params['oid'] != "") {
                $this->db->where("mso.iMstSubOrderId =", $input_params['oid']);
            }
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["invoice_data"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
