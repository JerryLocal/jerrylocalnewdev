<?php
/**
 * Description of getJobs Model
 *
 * @module getJobs
 *
 * @class model_get_jobs.php
 *
 * @path applicationront\webservice\models\model_get_jobs.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_jobs extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->start_cache();
            $this->db->from("mst_jobs AS mj");

            $select_fields = array();
            $this->db->select("mj.vJobCode AS mj_job_code");
            $this->db->select("mj.vJobTitle AS mj_job_title");
            $this->db->select("mj.tJobDescription AS mj_job_description");
            $this->db->select("mj.tNotes AS mj_notes");
            $this->db->select("mj.fFromSalary AS mj_from_salary");
            $this->db->select("mj.fToSalary AS mj_to_salary");
            $this->db->select("mj.vContactEmail AS mj_contact_email");
            $this->db->where_in("mj.eStatus", array('Active'));

            $this->db->stop_cache();
            $total_records = $this->db->count_all_results();

            $settings_params['count'] = $total_records;

            $record_limit = "9";
            $record_limit = intval($record_limit);
            $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;
            $total_pages = getTotalPages($total_records, $record_limit);
            $start_index = getStartIndex($total_records, $current_page, $record_limit);
            $settings_params['curr_page'] = $current_page;
            $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
            $settings_params['next_page'] = ($current_page+1 > $total_pages) ? 0 : 1;

            $this->db->limit($record_limit, $start_index);
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            $this->db->flush_cache();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["query"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
