<?php
/**
 * Description of trackOrder Model
 *
 * @module trackOrder
 *
 * @class model_track_order.php
 *
 * @path applicationront\webservice\models\model_track_order.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_track_order extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_order AS mo");
            $this->db->join("mst_sub_order AS mso", "mo.iMstOrderId = mso.iMstOrderId", "inner");
            $this->db->join("mst_shipper AS ms", "mso.iMstShipperId = ms.iMstShipperId", "left");

            $select_fields = array();
            $this->db->select("mso.vProductName AS mso_product_name");
            $this->db->select("mso.eItemStatus AS mso_item_status");
            $this->db->select("mso.iMstSubOrderId AS mso_mst_sub_order_id");
            $this->db->select("mo.iMstOrderId AS mo_mst_order_id");
            $this->db->select("mo.vBuyerEmail AS mo_buyer_email");
            $this->db->select("mso.vTrackingNumber AS mso_tracking_number");
            $this->db->select("ms.vUrl AS ms_url");
            $this->db->select("mso.vShipperName AS mso_shipper_name");
            $this->db->select("mso.dShippingDate AS mso_shipping_date");
            $this->db->select("mo.iOrderNumber AS mo_order_number");
            $this->db->select("mo.vOrderNumberPre AS mo_order_number_pre");
            if ($input_params['vEmailId'] != "") {
                $this->db->where("mo.vBuyerEmail =", $input_params['vEmailId']);
            }
            $this->db->where("CONCAT(`mo`.`vOrderNumberPre`,`mo`.`iOrderNumber`) ='".$input_params["iOrderId"]."'  ", FALSE, FALSE);

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["mso_shipping_date"];
                    if (method_exists($this->general, "dateSystemFormat")) {
                        $data = $this->general->dateSystemFormat($data, $data_arr, $i);
                    }
                    $result_arr[$data_key]["mso_shipping_date"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * sub_order_data method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function sub_order_data($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_sub_order AS mso");
            $this->db->join("mst_order AS mo", "mso.iMstOrderId = mo.iMstOrderId", "left");

            $select_fields = array();
            $this->db->select("mso.iMstSubOrderId AS mso_mst_sub_order_id_1");
            $this->db->select("mso.iMstOrderId AS mso_mst_order_id");
            $this->db->select("mso.vProductName AS mso_product_name_1");
            $this->db->select("mso.eItemStatus AS mso_item_status_1");
            $this->db->select("mso.vShipperName AS mso_shipper_name_1");
            $this->db->select("mso.dShippingDate AS mso_shipping_date_1");
            $this->db->select("mso.vTrackingNumber AS mso_tracking_number_1");
            $this->db->select("mso.vSubOrderNumberPre AS mso_sub_order_number_pre");
            $this->db->select("mso.iSubOrderNumber AS mso_sub_order_number");
            if ($input_params['vEmailId'] != "") {
                $this->db->where("mo.vBuyerEmail =", $input_params['vEmailId']);
            }
            $this->db->where("CONCAT(`mso`.`vSubOrderNumberPre`,`mso`.`iSubOrderNumber`) ='".$input_params["iOrderId"]."'  ", FALSE, FALSE);

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition_1 method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition_1($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["sub_order_data"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["query"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
