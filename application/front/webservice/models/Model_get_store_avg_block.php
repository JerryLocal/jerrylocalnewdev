<?php
/**
 * Description of getStoreAvgBlock Model
 *
 * @module getStoreAvgBlock
 *
 * @class model_get_store_avg_block.php
 *
 * @path applicationront\webservice\models\model_get_store_avg_block.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_store_avg_block extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_products AS mp");

            $select_fields = array();
            $this->db->select("mp.iMstProductsId AS mp_mst_products_id");
            if ($input_params['iStoreId'] != "") {
                $this->db->where("mp.iStoreId =", $input_params['iStoreId']);
            }

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * rating5 method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function rating5($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("trn_product_rating AS msd");

            $select_fields = array();
            $this->db->select("count(msd.iTrnProductRatingId) AS msd_trn_product_rating_id_1");
            $this->db->where("msd.iRate = ('5')", FALSE, FALSE);
            if ($input_params['mp_mst_products_id'] != "") {
                $this->db->where("msd.iMstProductsId =", $input_params['mp_mst_products_id']);
            }

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * rating4 method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function rating4($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("trn_product_rating AS msd");

            $select_fields = array();
            $this->db->select("count(msd.iTrnProductRatingId) AS msd_trn_product_rating_id");
            if ($input_params['mp_mst_products_id'] != "") {
                $this->db->where("msd.iMstProductsId =", $input_params['mp_mst_products_id']);
            }
            $this->db->where("msd.iRate = ('4')", FALSE, FALSE);
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * rating3 method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function rating3($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("trn_product_rating AS msd");

            $select_fields = array();
            $this->db->select("count(msd.iTrnProductRatingId) AS msd_trn_product_rating_id_2");
            if ($input_params['mp_mst_products_id'] != "") {
                $this->db->where("msd.iMstProductsId =", $input_params['mp_mst_products_id']);
            }
            $this->db->where("msd.iRate = ('3')", FALSE, FALSE);
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * rating2 method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function rating2($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("trn_product_rating AS msd");

            $select_fields = array();
            $this->db->select("count(msd.iTrnProductRatingId) AS msd_trn_product_rating_id_3");
            if ($input_params['mp_mst_products_id'] != "") {
                $this->db->where("msd.iMstProductsId =", $input_params['mp_mst_products_id']);
            }
            $this->db->where("msd.iRate = ('2')", FALSE, FALSE);
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * rating1 method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function rating1($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("trn_product_rating AS msd");

            $select_fields = array();
            $this->db->select("count(msd.iTrnProductRatingId) AS msd_trn_product_rating_id_4");
            if ($input_params['mp_mst_products_id'] != "") {
                $this->db->where("msd.iMstProductsId =", $input_params['mp_mst_products_id']);
            }
            $this->db->where("msd.iRate = ('1')", FALSE, FALSE);
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }
}
