<?php
/**
 * Description of EmailInvoice Model
 *
 * @module EmailInvoice
 *
 * @class model_email_invoice.php
 *
 * @path applicationront\webservice\models\model_email_invoice.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_email_invoice extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * main_order method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function main_order($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_order AS mo");
            $this->db->join("mst_sub_order AS mso", "mo.iMstOrderId = mso.iMstOrderId", "left");
            $this->db->join("mod_country AS mc", "mo.iBuyerCountryId = mc.iCountryId", "left");
            $this->db->join("mst_products AS mp", "mc.iCountryId = mp.iMstProductsId", "left");
            $this->db->join("mst_store_detail AS msd", "mc.iCountryId = msd.iMstStoreDetailId", "left");
            $this->db->join("mod_state AS ms", "mo.iBuyerStateId = ms.iStateId", "left");
            $this->db->join("mod_city AS mc1", "mo.iBuyerCityId = mc1.iCityId", "left");
            $this->db->join("mst_shipper AS ms1", "mo.iMstOrderId = ms1.iMstShipperId", "left");
            $this->db->join("trn_order_status_history AS tosh", "mc1.iCityId = tosh.iMstSubOrderId", "left");

            $select_fields = array();
            $this->db->select("mo.ePaymentStatus AS mo_payment_status");
            $this->db->select("mso.iMstSubOrderId AS mso_mst_sub_order_id");
            $this->db->select("mso.vProductName AS mso_product_name");
            $this->db->select("mso.iProductQty AS mso_product_qty");
            $this->db->select("mso.vShipperName AS mso_shipper_name");
            $this->db->select("mso.vTrackingNumber AS mso_tracking_number");
            $this->db->select("mso.dDeliveredDate AS mso_delivered_date");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("ms.vState AS ms_state");
            $this->db->select("ms1.vUrl AS ms1_url");
            $this->db->select("(mso.fProductSalePrice  * mso.iProductQty) AS Total_price", FALSE);
            $this->db->select("mso.fProductRegularPrice AS mso_product_regular_price_1");
            $this->db->select("mso.eItemStatus AS mso_item_status_1");
            $this->db->select("mo.vBuyerEmail AS mo_buyer_email");
            $this->db->select("mo.vBuyerName AS mo_buyer_name");
            if ($input_params['oid'] != "") {
                $this->db->where("mso.iMstSubOrderId =", $input_params['oid']);
            }

            $this->db->order_by("msd.vStoreName", "desc");
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * email_notification method is used to send email notification.
     *
     * @param array $input_params input_params array to process email notification.
     * @return array $return_arr returns response of email notification.
     */
    public function email_notification($input_params = array()) {
        try {

            $email_arr["vEmail"] = $input_params["mo_buyer_email"];
            $email_arr["vFromEmail"] = "admin@website.com";
            $email_arr["vFromName"] = "Administrator";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "Your Order Invoice Detail";

            $email_arr["mo_buyer_name"] = $input_params[""];
            $email_arr["mso_mst_sub_order_id"] = $input_params[""];
            $email_arr["mso_product_name"] = $input_params[""];
            $email_arr["mso_product_qty"] = $input_params[""];
            $email_arr["mso_product_regular_price_1"] = $input_params[""];
            $email_arr["mso_item_status_1"] = $input_params[""];
            $email_arr["mso_delivered_date"] = $input_params[""];
            $email_arr["msd_store_name"] = $input_params[""];
            $email_arr["Total_price"] = $input_params[""];
            $email_arr["mso_tracking_number"] = $input_params[""];
            $email_arr["mso_shipper_name"] = $input_params[""];
            $email_arr["SYSTEM.COMPANY_NAME"] = " '{%SYSTEM.COMPANY_NAME%}' ";
            $email_arr["SYSTEM.site_url"] = "'{%SYSTEM.SITE_URL%}'";

            $success = $this->general->sendMail($email_arr, "BUYER_INVOICE_MAIL");

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? @implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success) {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success) {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["main_order"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
