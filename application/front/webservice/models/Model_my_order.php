<?php
/**
 * Description of my_order Model
 *
 * @module my_order
 *
 * @class model_my_order.php
 *
 * @path applicationront\webservice\models\model_my_order.php
 *
 * @author CIT Dev Team
 *
 * @date 03.02.2016
 */

class Model_my_order extends CI_Model
{
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->start_cache();
            $this->db->from("mst_order AS mo");
            $this->db->join("mst_sub_order AS mso", "mo.iMstOrderId = mso.iMstOrderId", "left");

            $select_fields = array();
            $this->db->select("mo.iMstOrderId AS mo_mst_order_id");
            $this->db->select("mo.dDate AS mo_date");
            $this->db->select("mo.fOrderTotal AS mo_order_total");
            $this->db->select("mo.ePaymentStatus AS mo_payment_status");
            if ($input_params['vBuyerEmail'] != "")
            {
                $this->db->where("mo.iBuyerId =", $input_params['vBuyerEmail']);
            }

            $this->db->group_by(array("mo.iMstOrderId"));
            $this->db->stop_cache();
            $paging_data = $this->db->get();
            $total_records = is_object($paging_data) ? $paging_data->num_rows() : 0;

            $settings_params['count'] = $total_records;

            $record_limit = "25";
            $record_limit = intval($record_limit);
            $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;
            $total_pages = getTotalPages($total_records, $record_limit);
            $start_index = getStartIndex($total_records, $current_page, $record_limit);
            $settings_params['curr_page'] = $current_page;
            $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
            $settings_params['next_page'] = ($current_page+1 > $total_pages) ? 0 : 1;

            $this->db->order_by("mo.iMstOrderId", "desc");
            $this->db->limit($record_limit, $start_index);
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            $this->db->flush_cache();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0)
            {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr)
                {

                    $data = $data_arr["mo_date"];
                    if (method_exists($this->general, "dateSystemFormat"))
                    {
                        $data = $this->general->dateSystemFormat($data, $result_arr[$data_key], $i, $input_params);
                    }
                    $result_arr[$data_key]["mo_date"] = $data;

                    $i++;
                }
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * get_sub_order_detail method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_sub_order_detail($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $this->db->from("mst_sub_order AS mso");

            $select_fields = array();
            $this->db->select("mso.vProductName AS mso_product_name");
            $this->db->select("mso.iMstSubOrderId AS mso_mst_sub_order_id");
            $this->db->select("mso.fTotalCost AS mso_total_cost");
            $this->db->select("mso.eItemStatus AS mso_item_status");
            $this->db->select("mso.vShipperName AS mso_shipper_name");
            $this->db->select("mso.iMstProductsId AS mso_mst_products_id");
            $this->db->select("mso.fProductRegularPrice AS mso_product_regular_price");
            $this->db->select("mso.fProductSalePrice AS mso_product_sale_price");
            $this->db->select("mso.iProductQty AS mso_product_qty");
            $this->db->select("mso.fProductPrice AS mso_product_price");
            $this->db->select("mso.vSubOrderNumberPre AS mso_sub_order_number_pre");
            $this->db->select("mso.iSubOrderNumber AS mso_sub_order_number");
            if ($input_params['mo_mst_order_id'] != "")
            {
                $this->db->where("mso.iMstOrderId =", $input_params['mo_mst_order_id']);
            }

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }

            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array())
    {
        try
        {

            $cc_lo_0 = (empty($input_params["query"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
