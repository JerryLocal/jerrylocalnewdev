<?php
/**
 * Description of setStoreRating Model
 *
 * @module setStoreRating
 *
 * @class model_set_store_rating.php
 *
 * @path applicationront\webservice\models\model_set_store_rating.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_set_store_rating extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $iMstStoreDetailId = $input_params['storeid'];
            $vName = $input_params['name'];
            $vEmail = $input_params['email'];
            $iAdminId = $input_params['loginuserid'];
            $iRate = $input_params['rating'];
            $tReview = $input_params['description'];
            $dDate = "NOW()";
            $eStatus = "Active";
            $iSellerId = $input_params['iSellerId'];
            if (isset($input_params['storeid'])) {
                $this->db->set("iMstStoreDetailId", $iMstStoreDetailId);
            }
            if (isset($input_params['name'])) {
                $this->db->set("vName", $vName);
            }
            if (isset($input_params['email'])) {
                $this->db->set("vEmail", $vEmail);
            }
            if (isset($input_params['loginuserid'])) {
                $this->db->set("iAdminId", $iAdminId);
            }
            if (isset($input_params['rating'])) {
                $this->db->set("iRate", $iRate);
            }
            if (isset($input_params['description'])) {
                $this->db->set("tReview", $tReview);
            }
            $this->db->set("dDate", $dDate, FALSE);
            $this->db->set("eStatus", $eStatus);
            if (isset($input_params['iSellerId'])) {
                $this->db->set("iSellerId", $iSellerId);
            }
            $this->db->insert("trn_store_rating");
            $insert_id = $this->db->insert_id();
            if (!$insert_id) {
                throw new Exception("Failure in insertion.");
            }
            $result_param = "insert_id";
            $result_arr[$result_param] = $insert_id;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_1 method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query_1($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $sql_query = "SELECT COUNT(msd.iTrnStoreRatingId) AS totalRating, FORMAT((SUM(msd.iRate)/COUNT(msd.iTrnStoreRatingId)), 2)  AS `avgRate` FROM `trn_store_rating` AS `msd` WHERE `msd`.`iMstStoreDetailId` = '".$input_params["storeid"]."' LIMIT 1";
            $result_obj = $this->db->query($sql_query);
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_2 method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query_2($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $iStoreTotalReview = $input_params['totalRating'];
            $fStoreRateAvg = $input_params['avgRate'];
            if ($input_params['storeid'] != "") {
                $this->db->where("iMstStoreDetailId =", $input_params['storeid']);
            }
            if (isset($input_params['totalRating'])) {
                $this->db->set("iStoreTotalReview", $iStoreTotalReview);
            }
            if (isset($input_params['avgRate'])) {
                $this->db->set("fStoreRateAvg", $fStoreRateAvg);
            }
            $res = $this->db->update("mst_store_detail");
            $affected_rows = $this->db->affected_rows();
            if (!$res || $affected_rows == -1) {
                throw new Exception("Failure in updation.");
            }
            $result_param = "affected_rows";
            $result_arr[$result_param] = $affected_rows;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->db->flush_cache();
        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = $input_params["insert_id"];
            $cc_ro_0 = "0";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("integer", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("integer", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("gt", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
