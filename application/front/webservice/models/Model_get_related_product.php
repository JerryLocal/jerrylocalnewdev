<?php
/**
 * Description of getRelatedProduct Model
 *
 * @module getRelatedProduct
 *
 * @class model_get_related_product.php
 *
 * @path applicationront\webservice\models\model_get_related_product.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_related_product extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("trn_product_categories AS tpc");

            $select_fields = array();
            $this->db->select("tpc.iMstCategoriesId AS tpc_mst_categories_id");
            if ($input_params['iProductId'] != "") {
                $this->db->where("tpc.iMstProductsId =", $input_params['iProductId']);
            }
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * get_related_product method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_related_product($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_products AS mp");
            $this->db->join("trn_product_categories AS tpc", "mp.iMstProductsId = tpc.iMstProductsId", "left");

            $select_fields = array();
            $this->db->select("mp.iMstProductsId AS mp_mst_products_id");
            $this->db->select("mp.vTitle AS mp_title");
            $this->db->select("mp.vSku AS mp_sku");
            $this->db->select("mp.fRegularPrice AS mp_regular_price");
            $this->db->select("mp.fSalePrice AS mp_sale_price");
            $this->db->select("mp.fRatingAvg AS mp_rating_avg");
            $this->db->select("mp.iStock AS mp_stock");
            $this->db->select("mp.iDifferencePer AS mp_difference_per");
            $this->db->select("mp.iTotalRate AS mp_total_rate");
            $this->db->select("mp.dDate AS mp_date");
            $this->db->select("mp.iWishlistState AS mp_wishlist_state");
            $this->db->select("mp.vShortDescription AS mp_short_description");
            $this->db->select("mp.vDefaultImg AS mp_default_img");
            if ($input_params['tpc_mst_categories_id'] != "") {
                $this->db->where("tpc.iMstCategoriesId =", $input_params['tpc_mst_categories_id']);
            }
            if ($input_params['iProductId'] != "") {
                $this->db->where("mp.iMstProductsId <>", $input_params['iProductId']);
            }

            $this->db->order_by("RAND()", FALSE, FALSE);
            if (intval("10") > 0) {
                $this->db->limit("10");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }
}
