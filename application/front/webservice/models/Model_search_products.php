<?php
/**
 * Description of searchProducts Model
 *
 * @module searchProducts
 *
 * @class model_search_products.php
 *
 * @path applicationront\webservice\models\model_search_products.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_search_products extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * search_query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function search_query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->start_cache();
            $this->db->from("mst_products AS mp");
            $this->db->join("trn_product_categories AS tpc", "mp.iMstProductsId = tpc.iMstProductsId", "left");

            $select_fields = array();
            $this->db->select("mp.iMstProductsId AS mp_mst_products_id");
            $this->db->select("mp.iAdminId AS mp_admin_id");
            $this->db->select("mp.iStoreId AS mp_store_id");
            $this->db->select("mp.vTitle AS mp_title");
            $this->db->select("mp.vSku AS mp_sku");
            $this->db->select("mp.fRegularPrice AS mp_regular_price");
            $this->db->select("mp.fSalePrice AS mp_sale_price");
            $this->db->select("mp.fRatingAvg AS mp_rating_avg");
            $this->db->select("mp.iAllowMaxPurchase AS mp_allow_max_purchase");
            $this->db->select("mp.iLowAvlLimitNotifcation AS mp_low_avl_limit_notifcation");
            $this->db->select("mp.iStock AS mp_stock");
            $this->db->select("mp.iDifferencePer AS mp_difference_per");
            $this->db->select("mp.iTotalRate AS mp_total_rate");
            $this->db->select("mp.dDate AS mp_date");
            $this->db->select("mp.dModifyDate AS mp_modify_date");
            $this->db->select("mp.eFreeShipping AS mp_free_shipping");
            $this->db->select("mp.fShippingCharge AS mp_shipping_charge");
            $this->db->select("mp.iWishlistState AS mp_wishlist_state");
            $this->db->select("mp.iSaleState AS mp_sale_state");
            $this->db->select("mp.iViewState AS mp_view_state");
            $this->db->select("mp.iReturnDays AS mp_return_days");
            $this->db->select("mp.eDoReturn AS mp_do_return");
            $this->db->select("mp.tDescription AS mp_description");
            $this->db->select("mp.vShortDescription AS mp_short_description");
            $this->db->select("mp.vDefaultImg AS mp_default_img");
            $this->db->select("mp.vSearchTag AS mp_search_tag");
            $this->db->select("mp.tSearchKeyword AS mp_search_keyword");
            $this->db->select("mp.eStatus AS mp_status");
            $this->db->where_in("mp.eStatus", array('Active'));
            $this->db->where("IF('".$input_params["pids"]."'<>'0' , FIND_IN_SET(mp.iMstProductsId, '".$input_params["pids"]."'), '1=1') AND IF('".$input_params["cids"]."'<>'0', FIND_IN_SET(tpc.iMstCategoriesId, '".$input_params["cids"]."'), '1=1') AND mp.fSalePrice >= '".$input_params["startPrice"]."' AND mp.fSalePrice <= '".$input_params["endPrice"]."'", FALSE, FALSE);

            $this->db->stop_cache();
            $total_records = $this->db->count_all_results();

            $settings_params['count'] = $total_records;

            $record_limit = "9";
            $record_limit = intval($record_limit);
            $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;
            $total_pages = getTotalPages($total_records, $record_limit);
            $start_index = getStartIndex($total_records, $current_page, $record_limit);
            $settings_params['curr_page'] = $current_page;
            $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
            $settings_params['next_page'] = ($current_page+1 > $total_pages) ? 0 : 1;

            $this->db->order_by("CASE  \"".$input_params["sort_id"]."\"
WHEN  \"1\" THEN mp.iSaleState
WHEN  \"2\" THEN mp.iMstProductsId
WHEN  \"3\" THEN mp.iDifferencePer
WHEN  \"4\" THEN mp.iViewState
END DESC,
CASE  \"".$input_params["price_id"]."\" WHEN '1' THEN  mp.fSalePrice END DESC,
CASE  \"".$input_params["price_id"]."\" WHEN '2' THEN  mp.fSalePrice END ASC", FALSE, FALSE);
            $this->db->limit($record_limit, $start_index);
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            $this->db->flush_cache();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["mp_default_img"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = @implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["height"] = "170";
                    $image_arr["width"] = "170";
                    $image_arr["color"] = "FFFFFF";
                    $image_arr["path"] = $this->general->getImageNestedFolders("p_img");
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["mp_default_img"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }
}
