<?php
/**
 * Description of getStoreData Model
 *
 * @module getStoreData
 *
 * @class model_get_store_data.php
 *
 * @path applicationront\webservice\models\model_get_store_data.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_store_data extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_store_detail AS msd");

            $select_fields = array();
            $this->db->select("msd.iMstStoreDetailId AS msd_mst_store_detail_id");
            $this->db->select("msd.iAdminId AS msd_admin_id");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("msd.vCompanyName AS msd_company_name");
            $this->db->select("msd.vContactName AS msd_contact_name");
            $this->db->select("msd.vStoreLogo AS msd_store_logo");
            $this->db->select("msd.tStoreDescription AS msd_store_description");
            if ($input_params['iStoreId'] != "") {
                $this->db->where("msd.iMstStoreDetailId =", $input_params['iStoreId']);
            }
            if (intval("1") > 0) {
                $this->db->limit("1");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }

            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }
}
