<?php
/**
 * Description of test1 Model
 *
 * @module test1
 *
 * @class model_test1.php
 *
 * @path applicationront\webservice\models\model_test1.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_test1 extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }
}
