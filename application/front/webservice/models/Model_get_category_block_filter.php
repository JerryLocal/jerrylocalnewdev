<?php
/**
 * Description of GetCategoryBlock_Filter Model
 *
 * @module GetCategoryBlock_Filter
 *
 * @class model_get_category_block_filter.php
 *
 * @path applicationront\webservice\models\model_get_category_block_filter.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_category_block_filter extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }
}
