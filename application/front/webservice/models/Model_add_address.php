<?php
/**
 * Description of addAddress Model
 *
 * @module addAddress
 *
 * @class model_add_address.php
 *
 * @path applicationront\webservice\models\model_add_address.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_add_address extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $iAdminId = $input_params['iAdminId'];
            $vName = $input_params['vName'];
            $vAddress1 = $input_params['vAddress1'];
            $vAddress2 = $input_params['vAddress2'];
            $vArea = $input_params['vArea'];
            $iCountryId = $input_params['iCountryId'];
            $iStateId = $input_params['iStateId'];
            $iCityId = $input_params['iCityId'];
            $vPinCode = $input_params['vPinCode'];
            $vPhone = $input_params['vPhone'];
            if (isset($input_params['iAdminId'])) {
                $this->db->set("iAdminId", $iAdminId);
            }
            if (isset($input_params['vName'])) {
                $this->db->set("vName", $vName);
            }
            if (isset($input_params['vAddress1'])) {
                $this->db->set("vAddress1", $vAddress1);
            }
            if (isset($input_params['vAddress2'])) {
                $this->db->set("vAddress2", $vAddress2);
            }
            if (isset($input_params['vArea'])) {
                $this->db->set("vArea", $vArea);
            }
            if (isset($input_params['iCountryId'])) {
                $this->db->set("iCountryId", $iCountryId);
            }
            if (isset($input_params['iStateId'])) {
                $this->db->set("iStateId", $iStateId);
            }
            if (isset($input_params['iCityId'])) {
                $this->db->set("iCityId", $iCityId);
            }
            if (isset($input_params['vPinCode'])) {
                $this->db->set("vPinCode", $vPinCode);
            }
            if (isset($input_params['vPhone'])) {
                $this->db->set("vPhone", $vPhone);
            }
            $this->db->insert("mst_buyer_address");
            $insert_id = $this->db->insert_id();
            if (!$insert_id) {
                throw new Exception("Failure in insertion.");
            }
            $result_param = "insert_id";
            $result_arr[$result_param] = $insert_id;
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }
}
