<?php
/**
 * Description of dealOfTheDayByPrice Model
 *
 * @module dealOfTheDayByPrice
 *
 * @class model_deal_of_the_day_by_price.php
 *
 * @path applicationront\webservice\models\model_deal_of_the_day_by_price.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_deal_of_the_day_by_price extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }
}
