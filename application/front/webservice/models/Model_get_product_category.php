<?php
/**
 * Description of getProductCategory Model
 *
 * @module getProductCategory
 *
 * @class model_get_product_category.php
 *
 * @path applicationront\webservice\models\model_get_product_category.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_product_category extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * get_category_product method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_category_product($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("trn_homepage_category AS thc");

            $select_fields = array();
            $this->db->select("thc.iMstCategoriesId AS thc_mst_categories_id");
            $this->db->select("thc.vTitle AS thc_title");
            $this->db->select("thc.vImg AS thc_img");
            $this->db->select("thc.ePosition AS thc_position");
            $this->db->select("thc.vSubcategory AS thc_subcategory");
            $this->db->select("(".$this->db->escape("C").") AS type", FALSE);
            $this->db->select("(".$this->db->escape("").") AS url", FALSE);
            $this->db->where_in("thc.eStatus", array('Active'));

            $this->db->order_by("thc.iOrderId", "asc");
            if (intval("10") > 0) {
                $this->db->limit("10");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["thc_img"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = @implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["height"] = "327";
                    $image_arr["width"] = "267";
                    $image_arr["path"] = $this->general->getImageNestedFolders("c_img");
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["thc_img"] = $data;

                    $data = $data_arr["url"];
                    if (method_exists($this->general, "WSCleanUrl")) {
                        $data = $this->general->WSCleanUrl($data, $data_arr, $i);
                    }
                    $result_arr[$data_key]["url"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * get_sub_cat method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function get_sub_cat($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_categories AS mc");

            $select_fields = array();
            $this->db->select("mc.iMstCategoriesId AS mc_mst_categories_id");
            $this->db->select("mc.vTitle AS mc_title");
            $this->db->select("(".$this->db->escape("C").") AS type_1", FALSE);
            $this->db->select("(".$this->db->escape("").") AS url_1", FALSE);
            $this->db->where_in("mc.eStatus", array('Active'));
            $this->db->where("mc.iMstCategoriesId IN (".$input_params["thc_subcategory"].")", FALSE, FALSE);
            if (intval("7") > 0) {
                $this->db->limit("7");
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["url_1"];
                    if (method_exists($this->general, "WSCleanUrl")) {
                        $data = $this->general->WSCleanUrl($data, $data_arr, $i);
                    }
                    $result_arr[$data_key]["url_1"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["get_category_product"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
