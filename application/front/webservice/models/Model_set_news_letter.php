<?php
/**
 * Description of SetNewsLetter Model
 *
 * @module SetNewsLetter
 *
 * @class model_set_news_letter.php
 *
 * @path applicationront\webservice\models\model_set_news_letter.php
 *
 * @author CIT Dev Team
 *
 * @date 27.02.2016
 */

class Model_set_news_letter extends CI_Model
{
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * insert_newsletter_sunscription method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function insert_newsletter_sunscription($input_params = array(), &$settings_params)
    {
        try
        {
            $result_arr = array();

            $iLoginUserId = $input_params['iUserID'];
            $vName = $input_params['vName'];
            $vEmail = $input_params['vEmail'];
            $dDate = "NOW()";
            $iNewsletterGroupId = "2";
            $eStatus = $input_params['eStatus'];
            if (isset($input_params['iUserID']))
            {
                $this->db->set("iLoginUserId", $iLoginUserId);
            }
            if (isset($input_params['vName']))
            {
                $this->db->set("vName", $vName);
            }
            if (isset($input_params['vEmail']))
            {
                $this->db->set("vEmail", $vEmail);
            }
            $this->db->set("dDate", $dDate, FALSE);
            $this->db->set("iNewsletterGroupId", $iNewsletterGroupId, FALSE);
            if (isset($input_params['eStatus']))
            {
                $this->db->set("eStatus", $eStatus);
            }
            $this->db->insert("mst_newsletter_subscription");
            $insert_id = $this->db->insert_id();
            if (!$insert_id)
            {
                throw new Exception("Failure in insertion.");
            }
            $result_param = "insert_id";
            $result_arr[$result_param] = $insert_id;
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array())
    {
        try
        {

            $cc_lo_0 = (empty($input_params["insert_newsletter_sunscription"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0)
            {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0))
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
