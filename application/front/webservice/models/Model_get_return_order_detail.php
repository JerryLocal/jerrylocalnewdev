<?php
/**
 * Description of getReturnOrderDetail Model
 *
 * @module getReturnOrderDetail
 *
 * @class model_get_return_order_detail.php
 *
 * @path applicationront\webservice\models\model_get_return_order_detail.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_get_return_order_detail extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    /**
     * query method is used to execute database queries.
     *
     * @param array $input_params input_params array to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($input_params = array(), &$settings_params) {
        try {
            $result_arr = array();

            $this->db->from("mst_sub_order AS mso");
            $this->db->join("mst_products AS mp", "mso.iMstProductsId = mp.iMstProductsId", "left");

            $select_fields = array();
            $this->db->select("mso.iMstSubOrderId AS mso_mst_sub_order_id");
            $this->db->select("mso.iMstOrderId AS mso_mst_order_id");
            $this->db->select("mso.vProductName AS mso_product_name");
            $this->db->select("mso.iMstProductsId AS mso_mst_products_id");
            $this->db->select("mso.fTotalCost AS mso_total_cost");
            $this->db->select("mso.iProductQty AS mso_product_qty");
            $this->db->select("mp.vTitle AS mp_title");
            $this->db->select("mso.dDeliveredDate AS mso_delivered_date");
            $this->db->select("mso.eItemStatus AS mso_item_status");
            $this->db->select("mp.vDefaultImg AS mp_default_img");
            $this->db->where_in("mso.eItemStatus", array('Delivered'));
            if ($input_params['iOrderId'] != "") {
                $this->db->where("mso.iMstSubOrderId =", $input_params['iOrderId']);
            }
            $this->db->where("NOW() < DATE_ADD( mso.dDeliveredDate , INTERVAL mso.iReturnPeriod  DAY) ", FALSE, FALSE);

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found.');
            }
            if (is_array($result_arr) && count($result_arr) > 0) {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr) {

                    $data = $data_arr["mso_delivered_date"];
                    if (method_exists($this->general, "dateSystemFormat")) {
                        $data = $this->general->dateSystemFormat($data, $data_arr, $i);
                    }
                    $result_arr[$data_key]["mso_delivered_date"] = $data;

                    $data = $data_arr["mp_default_img"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = @implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["height"] = "50";
                    $image_arr["width"] = "50";
                    $image_arr["color"] = "FFFFFF";
                    $image_arr["path"] = $this->general->getImageNestedFolders("p_img");
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["mp_default_img"] = $data;

                    $i++;
                }
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * condition method is used to execute flow conditions.
     *
     * @param array $input_params input_params array to process flow conditions.
     * @return array $return_arr returns response of conditions.
     */
    public function condition($input_params = array()) {
        try {

            $cc_lo_0 = (empty($input_params["query"]) ? 0 : 1);
            $cc_ro_0 = "1";

            $cc_lo_0 = is_null($cc_lo_0) ? "" : $cc_lo_0;
            $cc_ro_0 = is_null($cc_ro_0) ? "" : $cc_ro_0;

            $cc_pl_0 = $this->general->getDataTypeWiseResult("string", $cc_lo_0, TRUE);
            $cc_pr_0 = $this->general->getDataTypeWiseResult("string", $cc_ro_0, FALSE);
            $cc_fr_0 = $this->general->compareDataValues("eq", $cc_pl_0, $cc_pr_0);
            if (!$cc_fr_0) {
                throw new Exception("Some condition does not match.");
            }
            if (!($cc_fr_0)) {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
