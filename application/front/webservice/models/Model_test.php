<?php
/**
 * Description of test Model
 *
 * @module test
 *
 * @class model_test.php
 *
 * @path applicationront\webservice\models\model_test.php
 *
 * @author Steve Smith
 *
 * @date 01.12.2015
 */

class Model_test extends CI_Model {
    public $_default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }
}
