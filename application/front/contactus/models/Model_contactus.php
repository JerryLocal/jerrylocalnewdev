<?php
class Model_contactus extends CI_Model {

    private $primary_key;
    private $main_table;
    public $errorCode;
    public $errorMessage;

    public function __construct() {
        parent::__construct();
        $this->main_table = "trn_messages";        
    }
	
    function insert($data = array()) {        
        $this->db->insert($this->main_table, $data);
        $insertId = $this->db->insert_id();
        return $insertId;
    }
}
?>