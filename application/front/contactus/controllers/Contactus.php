<?php

/**
 * Description of Content Controller
 * 
 * @module Content
 * 
 * @class content.php
 * 
 * @path application\front\content\controllers\content.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Contactus extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();        
        // $this->load->model('hbmodel');        
        $this->load->model('Model_contactus');
        $this->store_id = '';
        if(is_numeric($this->uri->segment(1))){
            $this->store_id = $this->uri->segment(1);
        } else {
            $this->store_id = $this->general->getSellerStoreId($this->uri->segment(1));
        }
    }

    /**
     * index method is used to initialize index function.
     */
    public function index() {        
      $this->load->library('captcha');
         $param = array(
             "type" => "png",
             "length" => 5,
             "height" => 37,
             "case" => false,
             "filters" => array(
                 "noise" => 0,
                 "blur" => 0
             ),
             "bgColor" => array(25, 51, 148),
             "textColor" => array(255, 255, 255)
         );
         $this->captcha->setParameter($param);
    }
    function contact_seller(){
        $sid = $this->store_id;
        $StoreName = '';
        if(!empty($sid)){  
            $StoreName = $this->generalfront->getColumnValue('iMstStoreDetailId',$sid,'vStoreName','mst_store_detail');
        }
        $this->smarty->assign('storeid',$sid);
        $this->smarty->assign('store_name',$StoreName);
        $this->index();
    }
    public function contact(){
        $postarr = $this->input->post();        
        $this->load->library('captcha');
        if(!empty($postarr) && count($postarr) > 0){            
            $fullname =  $postarr['fullname'];
            $Email = $postarr['Email'];
            $EmailSubject = $postarr['EmailSubject'];
            $Comment = $postarr['Comment'];
            $captcha_input = $postarr['Captcha'];
            if(empty($captcha_input)){
                $response['is_valid'] = true;
            }else{
                $response = $this->captcha->valid($captcha_input);
            }            
            if ($response['is_valid']) {
                $iUserId = $this->session->userdata('iUserId'); 
                if($iUserId == ''){
                    $iUserId = 0;
                }
                $data = array();
                $data['vSenderName'] = $fullname;
                $data['vSenderEmail'] = $Email;
                $data['iLogedUserId'] = $iUserId;                        
                $data['dDate'] = date('Y-m-d h:i:s');
                $data['vSubject'] = $EmailSubject;
                $data['tDetail'] = $Comment;
                $data['eToWhom'] = 'Admin'; 
                if(isset($postarr['ToWhom']))
                {
                    $data['eToWhom'] = 'Seller';
                    $data['iSellerId'] = $_POST['storeid'];
                    $Store = $this->generalfront->getColumnValue('iMstStoreDetailId',$_POST['storeid'],'vStoreName','mst_store_detail');
                }
                $insertid = $this->Model_contactus->insert($data);
                if($insertid > 0 && !empty($insertid)){  
                    if(isset($postarr['ToWhom'])){
                        $this->session->set_flashdata('success',"Your enquiry has been successfully sent to ".$Store.".The Seller will endeavour to answer your query to your email as soon as possible."); 
                    }else{
                        $this->session->set_flashdata('success','Your enquiry has been successfully sent to JerryLocal. We will endeavour to answer your query as soon as possible.');
                    }                  
                                      
                }else{                    
                    $this->session->set_flashdata('failure','There is some error plese try agian later.');
                }   
                redirect($_SERVER['HTTP_REFERER']);
                
            }else{
                $msg = "Invalid Captcha.";
                $this->session->set_flashdata('failure', $msg);
                redirect($this->config->item('site_url') . "contact.html");
                exit();
            }            
        }else{
            $this->session->set_flashdata('failure','There is some error plese try agian later.');
            redirect($this->config->item('site_url') . 'contact.html');
        }     
    }
    
    function resetCaptcha() {
       $this->load->library('captcha');
       $param = array(
           "type" => "png",
           "length" => 5,
           "height" => 37,
           "case" => false,
           "filters" => array(
               "noise" => 0,
               "blur" => 0
           ),
           "bgColor" => array(25, 51, 148),
           "textColor" => array(255, 255, 255)
       );
       $this->captcha->setParameter($param);
       $html_capcha = $this->captcha->show();
       echo $html_capcha;
       exit();
    }

    /* 0651
     * Fuction to check captcha
     */

    function check_captcha() {

        $captcha_text = $this->input->post('Captcha');
        $checkCaptcha = $this->generalfront->captchaIsValid($captcha_text);
        echo $checkCaptcha;
        exit;
    }
}
