<%$this->js->add_js("contactus.js")%>
<div id="contactus"></div>
<div class="main-containt">
  <div class=""> <!-- container -->
    <div class="row-fluid">
      <div class="breadcrumbs">
        <%$this->general->Breadcrumb('CLEAN','Contact Us')%>  
      </div>
    </div>
    <div class="common-back">
      <h3>Contact Us</h3>
      <div class="col-sm-6">
        <form action="<%$this->config->item('site_url')%>contactus/contactus/contact" class="form-horizontal" id="frmContactus" name="frmContactus" method="POST">
          <%assign var=fullname value=$this->session->userdata('vFirstName')|cat: " "|cat: $this->session->userdata('vLastName')%>
          <div class="form-group">
            <label for="fullname" class="col-sm-4 control-label">Full Name <span class="required">*</span></label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Full Name" value="<%$fullname%>">                            
            </div>
          </div>
          <div class="form-group">
            <label for="Email" class="col-sm-4 control-label">Email Address <span class="required">*</span></label>
            <div class="col-sm-8">
              <input type="email" class="form-control" id="Email" name="Email" placeholder="Email Address" value="<%$this->session->userdata('vEmail')%>">
            </div>
          </div>
          <%assign var=subject value=explode(',',$this->config->item('CONTACT_US_SUBJECT'))%>
          <div class="form-group">
            <label for="Email" class="col-sm-4 control-label">Subject <span class="required">*</span></label>
            <div class="col-sm-8">
              <select class="form-control" id="EmailSubject" name="EmailSubject">
                <%foreach $subject as $sub key=key%>
                  <%if $key eq 0%>
                    <option value=""><%$sub%></option>
                  <%else%>
                    <option value="<%$sub%>"><%$sub%></option>
                  <%/if%>
                <%/foreach%>                
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="Comment" class="col-sm-4 control-label">Comment <span class="required">*</span></label>
            <div class="col-sm-8">
              <textarea class="form-control" id="Comment" name="Comment" rows="3"></textarea>
            </div>
          </div>
          <!-- <div class="form-group">
            <label for="Captcha" class="col-sm-4 control-label">Enter Captcha</label>
            <div class="col-sm-8">               
              <img src="<%$this->captcha->show()%>" id="capchareset" style="cursor:pointer;position: relative;width:165px;height:34px;vertical-align:top;" />
              <input class="captcha-input" type="text" id="Captcha" name="Captcha" value="" maxlength="6">              
              <button class="btn btn-default" id="resetcaptcha"><i class="fa fa-refresh"></i></button>              
              <span id="CaptchaErr"></span>
            </div>
          </div> -->
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
              <button type="submit" class="btn btn-default-site" id="contactusSubmit">Send</button>
            </div>
          </div>
        </form>
      </div>
      <div class="or-txt fl" style="text-align:left; width:auto; margin-top:50px;">--Or--</div>
      <div class="col-sm-5">
        <div class="contact-info" style="border:none;">
          <!-- <h4>Email US </h4> -->
          <!-- <p><span class="fa fa-phone-square"></span>Call : <%$this->config->item('COMPANY_TOLL_FREE')%></p> -->
          <p><span class="fa fa-support"></span>Email Us: <%$this->config->item('COMPANY_SUPPORT_EMAIL')%></p>
        </div> 
      </div>
    </div>
  </div>
</div>
</div>
</div>