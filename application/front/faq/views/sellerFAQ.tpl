<div class="container">
<div class="row-fluid">
<div class="breadcrumbs">    
	<%$this->general->Breadcrumb('CLEAN','FAQ\'s')%>  
</div>
</div></div>
 <div class="delivery-box">
    <h3>FAQ's</h3>
    <div class="form-add">
        <div class="faq-content">
	        <%foreach $faq_arr['data'] as $faq%> 
	        <h3><%$faq['mfc_title']%></h3>
	        <%for $i = 0 to $faq['item_qry']|@count-1%>
	            <div class="panel-group" id="accordion">
	               	<div class="panel panel-default">
			            <div class="panel-heading">
			                <h4 class="panel-title">
			                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#<%$faq['item_qry'][$i]['mf_mst_faq_id']%>"><%$faq['item_qry'][$i]['mf_title']%></a>
			                </h4>
			            </div>
			            <div id="<%$faq['item_qry'][$i]['mf_mst_faq_id']%>" class="panel-collapse collapse">
			                <div class="panel-body"><%$faq['item_qry'][$i]['mf_description']%>
			                </div>
			            </div>
	        		</div>
			        
	        	</div>
	        	<%/for%>
	        <%/foreach%> 
	    </div> 
	</div>
</div>