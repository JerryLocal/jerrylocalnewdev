<%$this->js->add_js("faq.js")%>  
<style type="text/css">
.faq-wrap { padding:0px 20px; overflow:hidden; padding-bottom:20px;}

.faqs-label { float:left; width:320px; text-align:right; margin-right:0px;}
.all-faq { float:left; width:60%; margin-right:-20px;}
.faqs-label h4 {font-size: 18px;  font-family: 'latoregular'; color: #5f5f5f; padding:10px 10px; border-right: 5px solid #fff; }
.faqs-label h4:hover {border-right: 5px solid #dd3d0b;}
.individual-faq h4 { margin-top:20px; margin-bottom:20px;}
.faqs-content h2 { font-size: 28px;  font-family: 'latoregular'; color: #5f5f5f; border-left: 5px solid #dd3d0b; padding-left:20px; margin-top:15px;}
.g-hr {position: relative; clear: both; margin: 40px 0; width: 100%;  height: 10px;}
.g-hr-h {position: absolute; top: 50%;  left: 0;  height: 0;  width: 100%; border-top: 1px solid #e8e8e8;}
.g-hr-h i { position: absolute; top: 0;  left: 50%;  font-size: 16px; height: 16px;  width: 50px;   margin-top: -8px;  margin-left: -25px;  text-align: center;
    background-color: #fff;  color: #e8e8e8;}
.faqs-label.fixed {
    position: fixed;
}
.faqs-label {
    position: absolute;
}
#faq-wrap .faqs-label.fixed.bottom {
    position: absolute;
}
.all-faq {
  margin-left:30%
}
.faqs-label h4.f-active {
    border-right: 5px solid #dd3d0b;
}
</style>
         
<div class=""> <!-- container -->
<div class="row-fluid">
<div class="breadcrumbs">    
	<%$this->general->Breadcrumb('CLEAN','FAQ\'s')%>  
</div>
</div>
<div class="row">
  <div class="product-list">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-main">
      <div class="delivery-box">
        <h3>FAQ's</h3>
        <div class="faq-wrap" id="faq-wrap">
              <div class="faqs-label">
              	<%foreach $faq_arr['data'] as $faq%> 
              	<%if $faq['item_qry']|@count gt '0'%>
                <h4><a href="faq.html#f-<%$faq['mfc_mst_faq_category_id']%>"><%$faq['mfc_title']%></a></h4>
                <%/if%>
                <%/foreach%>
              </div>
              
            <div class="all-faq">
              <%foreach $faq_arr['data'] as $faq%> 
              <%if $faq['item_qry']|@count gt '0'%>
              <div class="faqs-content" id="f-<%$faq['mfc_mst_faq_category_id']%>">
                <h2><%$faq['mfc_title']%></h2>
                <%for $i = 0 to $faq['item_qry']|@count-1%>
                <div class="individual-faq">
                  <h4><%$faq['item_qry'][$i]['mf_title']%></h4>
                  <div><%$faq['item_qry'][$i]['mf_description']%></div>
                </div>
	        	<%/for%>
              </div>
              <div class="g-hr"><span class="g-hr-h"><i class="fa fa-star"></i></span></div>
              <%/if%>
              <%/foreach%>
            </div>
      	</div>
    </div>
  </div>
</div>
</div>
</div>