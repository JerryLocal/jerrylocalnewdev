<?php

/**
 * Description of Content Controller
 * 
 * @module Content
 * 
 * @class content.php
 * 
 * @path application\front\content\controllers\content.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Faq extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');
    }

    /**
     * index method is used to initialize index function.
     */
    public function index() {
        $params = array();
        $params['Type'] = "Buyer";
        $ws_name = 'get_faq';
        $faq_arr = $this->hbmodel->getData($ws_name, $params);
        $faq_detail = $merchant_details_arr['data'];
        $this->smarty->assign('faq_arr',$faq_arr);
    }

    public function sellerFAQ() {
        $params = array();
        $params['Type'] = "Seller";
        $ws_name = 'get_faq';        
        $faq_arr = $this->hbmodel->getData($ws_name, $params);
        $faq_detail = $merchant_details_arr['data'];
        $this->smarty->assign('faq_arr',$faq_arr);
    }
}
