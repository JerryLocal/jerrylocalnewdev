<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<TITLE><%$this->systemsettings->getSettings('SITE_TITLE')%></TITLE>-->
        <link rel="shortcut icon" href="<%$this->config->item('site_url')%>images/favicon.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%if $meta_title neq ''%><%$meta_title%><%else%><%$this->systemsettings->getSettings('META_TITLE')%><%/if%></title>
        <meta name="description" content="<%if $meta_description neq ''%><%$meta_description%><%else%><%$this->systemsettings->getSettings('META_DESCRIPTION')%><%/if%>" />
        <meta name="keywords" content="<%if $meta_keyword neq ''%><%$meta_keyword%><%else%><%$this->systemsettings->getSettings('META_KEYWORD')%><%/if%>" />

        <%if $this->router->fetch_method() eq 'productDetail'%>
        <meta property="og:title" content="<%$Meta_Product_Name%>" />
        <meta property="og:description" content="<%$Meta_Product_Short_Description%>" />  
        <meta property="og:url" content="<%$Meta_Product_Page_URL%>"/>
        <meta property="og:image" content="<%$Meta_Product_IMG_URL%>"/> 

        <meta itemprop="name" content="<%$Meta_Product_Name%>" /> 
        <meta itemprop="description" content="<%$Meta_Product_Short_Description%>" />
        <%else%>
        <meta property="og:url"                content="https://www.jerrylocal.co.nz/" />
        <meta property="og:type"               content="E-commerce" />
        <meta property="og:title"              content="<%if $meta_title neq ''%><%$meta_title%><%else%><%$this->systemsettings->getSettings('META_TITLE')%><%/if%>" />
        <meta property="og:description"        content="<%if $meta_description neq ''%><%$meta_description%><%else%><%$this->systemsettings->getSettings('META_DESCRIPTION')%><%/if%>" />
        <!--<meta property="og:image"              content="http://static01.nyt.com/images/2015/02/19/arts/international/19iht-btnumbers19A/19iht-btnumbers19A-facebookJumbo-v2.jpg" />-->
        <!--<meta property="og:title" content="<%if $meta_title neq ''%><%$meta_title%><%else%><%$this->systemsettings->getSettings('META_TITLE')%><%/if%>" />
        <meta property="og:description" content="<%if $meta_description neq ''%><%$meta_description%><%else%><%$this->systemsettings->getSettings('META_DESCRIPTION')%><%/if%>" />  -->
        <%/if%>
        
        <link rel="shortcut icon" href="<%$this->config->item('images_url')%>favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" media="print" href="<%$this->config->item('site_url')%>public/styles/print_css.css">
        <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
        
        <%if $this->systemsettings->getSettings('META_OTHER') neq ''%>
            <%$this->systemsettings->getSettings('META_OTHER')%>
        <%/if%>    
        <%$this->css->add_css("bootstrap3/bootstrap.css")%>
        <%$this->css->add_css("bootstrap3/icons.css")%>
        <%$this->css->add_css("bootstrap3/font-awesome.min.css")%>
        <%$this->css->add_css("style.css","style2.css","owl.carousel.css","media.css","developer-SA.css","jquery.fancybox.css")%>
        <%$this->css->add_css('fancybox/jquery.fancybox.css','fancybox/jquery.fancybox-buttons.css','fancybox/jquery.fancybox-thumbs.css')%>
        <%$this->css->add_css('datepicker/daterangepicker.css','datepicker/jquery.ui.datepicker.css','datepicker/jquery-ui-timepicker-addon.css')%>
        <%$this->css->add_css("multizoom.css","lightview.css","jquery.bxslider.css")%>
        <%$this->css->css_src()%>
        <%$this->js->add_js("common.js")%>        
        <%$this->js->add_js("libraries/jquery.validate.js")%>        
        
        <%$this->js->add_js("jquery-1.11.3.min.js","jquery.validate.js","bootstrap3/bootstrap.min.js","general.js","owl.carousel.min.js","modernizr.custom.29473.js","jquery.mousewheel-3.0.6.pack.js")%>
        <%$this->js->add_js("libraries/fancybox/jquery.fancybox.js","libraries/fancybox/jquery.fancybox.pack.js")%>
        <%$this->js->add_js("jquery.mCustomScrollbar.concat.min.js")%>
        <%$this->js->add_js("storagelib.js","jquery-ui.js")%>
        <%$this->js->add_js("libraries/fuelux/js/spinner.js")%>
        <%$this->js->add_js("add-to-cart.js","multizoom.js")%>
        <%$this->js->add_js("jquery.bxslider.min.js","lightview.js","multizoom.js")%>

        <base href="<%$this->config->item('site_url')%>" />
        <script>
            var site_url = '<%$this->config->item("site_url")%>';
            var userid = '<%$this->session->userdata("iUserId")%>';            
        </script>
        <script>
                 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                 ga('create', 'UA-71858212-1', 'auto');
                 ga('send', 'pageview');

               </script>
    </head>
    <body>
        <a href="#" id="back-to-top" title="Back to top"></a>
        <%assign var="error_class" value=""%>
        <%assign var="style_val" value="display:none;"%>
        <%if $this->session->flashdata('success') neq ''%>
            <%assign var="error_class" value="alert-success"%>
            <%assign var="style_val" value=""%>
        <%elseif $this->session->flashdata('failure') neq ''%>   
            <%assign var="error_class" value="alert-danger"%>
            <%assign var="style_val" value=""%>
        <%/if%>    
        <div class="errorbox-position" id="var_msg_cnt" style="<%$style_val%>">
            <div class="closebtn-errorbox">
                <a href="javascript:void(0);" onClick="Project.closeMessage();return false;"><button class="close" type="button">×</button></a>
            </div>
            <div class="content-errorbox alert <%$error_class%>" id="err_msg_cnt">
                <%if $this->session->flashdata('success') neq ''%>
                    <%$this->session->flashdata('success')%>
                <%/if%>   
                <%if $this->session->flashdata('failure') neq ''%>   
                    <%$this->session->flashdata('failure')%>
                <%/if%>
            </div>

        </div>        
        <%assign var=sellerZone value=$this->uri->segment(1)|cat:"/"|cat: $this->uri->segment(2)%>
        <%assign var=menu value=$this->generalfront->FrontMenu()%>    
        <%if $sellerZone neq 'seller/zone.html'%>
        <div class="jl-header">
        <div id="top-container" class="">
                <!--top-part start here-->
                <%include file="top/top.tpl"%>
                <!--top-part End here-->
            </div></div>
        <%/if%>
        <%if $sellerZone neq 'seller/zone.html'%>
            <div id="midd-container" class="container <%$this->router->fetch_class()%>Page">
        <%else%>
            <div id="midd-container" class="<%$this->router->fetch_class()%>Page">
        <%/if%>
                <!-- middle part start here-->
                <%include file=$include_script_template%>
                <!-- middle part end here-->
            </div>
        <%if $sellerZone neq 'seller/zone.html'%>
        <div id="bott-container">
                <!--bottom link start here-->
                <%include file="bottom/footer.tpl"%>
                <!--bottom part End here-->
            </div>
        <%/if%>

            <div style="display: none;">
                <div id="test_confirm" style="width:100%;">
                    <!-- <div class="popuptitle">Confirm</div> -->
                    <div class="popup-cnt">
                        <img src="<%$this->config->item('images_url')%>alert.png">
                        <div class="clear">&nbsp;</div>
                        <h4 id="test_confirm_message" class='test_confirm_message'></h4>
                        <div class="clear">&nbsp;</div>
                        <div align="center" class="btn-align">
                            <div class="btn_ok">
                            <a class="common-btn" href="javascript:" title="" id="inlineok">Ok</a>
                            </div>
                            <div class="btn_cancel">
                            <a class="common-btn" href="javascript:" title="" id="inlinecancel">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <%if $this->systemsettings->getSettings('GOOGLE_ANALYTICS')|@trim neq ''%>
            <%$this->systemsettings->getSettings('GOOGLE_ANALYTICS')%>
        <%/if%>
        <%$this->js->js_src()%>
        <script type="text/javascript">
            $(document).ready(function() {
                //Sign up popup init
                if(($('#user_id').val() == 0) && ($("#homepage-flag").length > 0)){
                    $("#myModal2").modal('show');
                }
                //End of code
                Project.init();
                Project.checkmsg();
            });
        </script>
        <script>
            if ($('#back-to-top').length) {
            var scrollTrigger = 100, // px
                backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('#back-to-top').addClass('show');
                    } else {
                        $('#back-to-top').removeClass('show');
                    }
                };
            backToTop();
            $(window).on('scroll', function () {
                backToTop();
            });
            $('#back-to-top').on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });
        }
        </script>
    </body>
</html>