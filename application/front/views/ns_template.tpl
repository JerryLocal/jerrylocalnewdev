<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <TITLE>NS Test Console</TITLE>
        <link rel="shortcut icon" href="<%$this->config->item('site_url')%>images/favicon.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="<%if $meta_description neq ''%><%$meta_description%><%else%><% $this->systemsettings->getSettings('META_DESCRIPTION')%><%/if%>" />
        <meta name="keywords" content="<%if $meta_keyword neq ''%><%$meta_keyword%><%else%> <% $this->systemsettings->getSettings('META_KEYWORD')%><%/if%>" />

        <%if $this->systemsettings->getSettings('META_OTHER') neq ''%>
            <%$this->systemsettings->getSettings('META_OTHER')%>
        <%/if%>    

        <%$this->css->add_css("style.css")%>
        <%$this->css->css_src()%>
        <base href="<%$this->config->item('site_url')%>" />
        <script>
            var site_url = '<%$this->config->item("site_url")%>';
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div id="midd-container" style="border:1px solid #000000;">
                <!-- middle part start here-->
                <%include file=$include_script_template%>
                <!-- middle part end here-->
            </div>
        </div>

        <%$this->js->js_src()%>
    </body>
</html>