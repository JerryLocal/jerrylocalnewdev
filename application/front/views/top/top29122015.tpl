<div class="header">
<div class="top-header">
  <div class="container">
       <div class="row">
        <div class="user block block-cart box-right"><a href="<%$this->config->item('site_url')%>checkout/myshoppingcart"><i class="fa fa-shopping-cart"></i></a>
          <!-- <div class="block-content">
            <div class="inner">
              <p class="block-subtitle">Recently added item(s)</p>
              <ol id="cart-sidebar" class="mini-products-list">
                <li class="item last odd"> <a href="#" title="Electronics Product 02" class="product-image"><img src="<%$this->config->item('images_url')%>cosmetic02.png" alt="Electronics Product 02"></a> <a href="#" title="Remove This Item" class="btn-remove">Remove This Item</a> 
                  <div class="product-details">
                    <p class="product-name"><a title="Electronics Product 02" href="#">Electronics Product 02</a></p>
                    <span class="price">$900.00</span> <strong> <a title="Decrement" class="flycart-qty-btn flycart-qty-change flycart-qty-change-left" href="#"><i class="fa fa-minus"></i></a>
                    <input class="input-text qty flycart-qty" type="text" id="flycart_sidebar_1894" value="1">
                    <a title="Increment" class="flycart-qty-btn flycart-qty-change flycart-qty-change-right" href="#"><i class="fa fa-plus"></i></a></strong> </div>
                  </li>
                </ol>
                <div class="summary">
                  <p class="subtotal"> <span class="label">Subtotal:</span> <span class="price">$900.00</span> </p>
                </div>
                <div class="actions">
                  <div class="a-inner"> <a class="btn-mycart" href="#" title="View my cart"> view my cart </a><a href="#" title="Checkout" class="btn-checkout"> Checkout </a> </div>
                </div>
              </div>
            </div> -->
          </div>
    <div class="box-right"><a href="#" class="top-h-wish-list" data-toggle="modal" data-target="#myModal"><i class="fa fa-map-marker"></i>&nbsp; Track Order</a></div>
    <!--<div class=" box-right"><a href="#" class="top-h-login" data-toggle="modal" data-target="#myModal3"><i class="fa fa-lock"></i>&nbsp; Login</a></div>-->
   <div class="box-right box-my-account block block-cart">
    <a class="top-h-help" href="#"><%$menu['HELP'].mm_title%> &nbsp;<i class="fa fa-angle-down"></i></a>
      <div class="block-content top-nav-h-sub">
        <div class="inner">
          <%assign var=items value=$menu['HELP'].items%>
          <ul>
            <%foreach $items as $item%>
              <%if ($this->session->userdata('iUserId') gt '0' || $this->session->userdata('iUserId') eq '') && $item.mmi_login_req eq 'No'%>
                <li><a href="<%$item.mmi_external_link%>"><%$item.mmi_title%></a></li>
              <%/if%>
            <%/foreach%>
          </ul>
        </div>
      </div>
    </div>
    <%assign var=items value=$menu['MYACCOUNT'].mm_title%>
    <%assign var=menulist value=$menu['MYACCOUNT'].item%>    
    <div class="box-right border-l-none box-my-account block block-cart"><a class="top-h-my-account" href="#"><%$items%> &nbsp;<i class="fa fa-angle-down"></i></a>
    
    <div class="block-content top-nav-h-sub new">
        <div class="inner">
            <%if $this->session->userdata['iUserId'] gt 0%>

            <%else%>
            <div class="user-inner-btn">
                <div class="user-icon-bg">                  
                    <i class="fa fa-user"></i>
                </div>
                <button type="button" class="btn btn-default-j-b" data-dismiss="modal" data-toggle="modal" data-target="#myModal3">Login</button>
                <button type="button" class="btn btn-default-j-b" data-dismiss="modal" data-toggle="modal" data-target="#myModal2">Sign Up</button>
            </div>
            <%/if%>
            <%if $this->session->userdata['iUserId'] gt 0%>
            <!-- <ul>              
              < %foreach $menulist as $menu%>
                <li><a href="#"><%$menu%></a></li>
              < %/foreach%>                
            </ul> -->
            <ul>
              <li><a href="<%$this->config->item('site_url')%>my-orders.html">My Orders</a></li>
              <li><a href="<%$this->config->item('site_url')%>wishlist.html">My Wishlist</a></li>
              <li><a href="<%$this->config->item('site_url')%>profile.html">Profile</a></li>
              <li><a href="<%$this->config->item('site_url')%>invite-friend.html">Invite friends</a></li>
              <li><a href="<%$this->config->item('site_url')%>logout.html">Logout</a></li>
            </ul>
            <%/if%>
         </div>
         </div>
        </div>
        <%if $this->session->userdata('iUserId') neq ''%>
        <div class="header_user_wlcm"> Welcome <span style="color: #f1592a;"> <%$this->session->userdata('vFirstName')%></span> !</div>
        <%/if%>
         <ul class="logo-user">
        <!--  <li class="user"><a href="#"><i class="fa fa-user"></i></a>
      <ul>
           <li><a href="#">My Account</a></li>
           <li><a href="#">Wish List</a></li>
           <li><a href="#">Help</a></li>
           <li><a href="#">Login</a></li>
          </ul>
     </li> -->
         <li class="user block block-cart">
         <a href="#"><i class="fa fa-shopping-cart"></i>
         </a>
          <!-- <div class="block-content">
               <div class="inner">
            <p class="block-subtitle">Recently added item(s)</p>
            <ol id="cart-sidebar" class="mini-products-list">
                 <li class="item last odd"> <a href="#" title="Electronics Product 02" class="product-image"><img src="<%$this->config->item('images_url')%>cosmetic02.png" alt="Electronics Product 02"></a> <a href="#" title="Remove This Item" class="btn-remove">Remove This Item</a> <a href="#" title="Edit item" class="btn-edit">Edit item</a>
              <div class="product-details">
                   <p class="product-name"><a title="Electronics Product 02" href="#">Electronics Product 02</a></p>
                   <span class="price">$900.00</span> <strong> <a title="Decrement" class="flycart-qty-btn flycart-qty-change flycart-qty-change-left" href="#"><i class="fa fa-minus"></i></a>
               <input class="input-text qty flycart-qty" type="text" id="flycart_sidebar_1894" value="1">
               <a title="Increment" class="flycart-qty-btn flycart-qty-change flycart-qty-change-right" href="#"><i class="fa fa-plus"></i></a></strong> </div>
             </li>
                </ol>
            
            <div class="summary">
                 <p class="subtotal"> <span class="label">Subtotal:</span> <span class="price">$900.00</span> </p>
            </div>
            <div class="actions">
                 <div class="a-inner"> <a class="btn-mycart" href="#" title="View my cart"> view my cart </a><a href="#" title="Checkout" class="btn-checkout"> Checkout </a> </div>
            </div>
           </div>
          </div> -->
     </li>
        </ul>
   </div>
      </div>
</div>
<div class="logo-area">
  <div class="container">
    <div class="">
      <div class="main-logo fl">
          <a href="<%$this->config->item('site_url')%>" class="logo"> 
              <img src="<%$this->config->item('images_url')%>logo.png" alt="<%$this->config->item('COMPANY_NAME')%>" class="img-responsive"> 
          </a>
      </div>      
      <div class="right-of-logo fl">
        <form class="form-horizontal" method="GET" action="<%$this->config->item('site_url')%>search.html" >
          <div class="advanced-search">
              <div class="form-search">
               <input type="text" id="seach_text" class="input-text search_box" placeholder="I am looking for..." value="" name="search" autocomplete="off" tabindex="0">
               <button type="submit" title="Search" class="button form-button"><span><span>Search</span></span></button>
              </div>
              <div id="quicksearch"></div>
          </div>
        </form>
      </div>
      <div class="cart-right fr">
         <div class="block block-cart" id='cart-information-block'>
                  <!-- cart info load hear important place -->
        </div>    
      </div>
      <div class="clear"></div>
      </div>      
      <div class="clear"></div>
    </div>
  </div>
</div>
<div class="nav-header">

<div class="container">
    <button class="navbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    <ul id="mainnav" class="mainnav">
      <%assign var=items value =$menu['HEADERMENU'].items%>
      <!-- < %$items|@pr%> -->
      <%foreach $items as $item%>
       <%if ($this->session->userdata('iUserId') gt '0' || $this->session->userdata('iUserId') eq '') && $item.mmi_login_req eq 'No'%>   
             <!-- toggleMenu(this,0) toggleMenu(this,<%$item.mmi_mst_menu_items_id%>)-->
         <li class="level0 nav-1 parent" onmouseover="" onmouseout=""> <a href="<%$item.mmi_external_link%>" class=""><span><%$item.mmi_title%></span> </a>
          <ul class="level0">
            <li>
              <div>
                <div class="nav-column">
                  <%if $item['children']|@count ge 4%>
                    <%foreach $item['children'] as $subitem%>   
                      <h3><%$subitem.mmi_title%></h3>
                      <ul>
                        <li><a href="<%$subitem.mmi_external_link%>"><%$subitem.mmi_title%></a></li>
                      </ul>
                    <%/foreach%>
                  <%else%>                
                    <%foreach $item['children'] as $subitem%>
                      <!-- <h3 class="orange"><%$subitem.mmi_title%></h3> -->
                      <a href="<%$subitem.mmi_external_link%>"><h3 class="orange"><%$subitem.mmi_title%></h3></a>
                      <ul>                
                          <!-- <li><a href="<%$subitem.mmi_external_link%>"><%$subitem.mmi_title%></a></li> -->
                          <%if $subitem['children']|@count gt 0%>
                          <ul>
                            <%foreach $subitem['children'] as $subitem2%>
                              <li><a href="<%$subitem2.mmi_external_link%>"><%$subitem2.mmi_title%></a></li>
                            <%/foreach%>
                          </ul>
                          <%/if%>
                      </ul>
                    <%/foreach%>            
                  <%/if%>
                </div>
              </div>
            </li>
          </ul>
        </li>
       <%/if%>
      <%/foreach%>

      <!-- <li class="level0 nav-1 parent" onmouseover="toggleMenu(this,1)" onmouseout=""> <a href="#" class="">
       <span>Fashion</span> </a>
        <ul class="level0">
            <li>
              <div>
                <div class="nav-column">
                  <h3 class="orange">Related Categories</h3>
                  <ul>
                    <li><a href="#">Pampers Diapers</a></li>
                    <li><a href="#">Huggies Diapers</a></li>
                    <li><a href="#">Diapers</a></li>
                  </ul>

                  <h3 class="orange">Brands</h3>
                  <ul>
                    <li><a href="#">Driving shoes</a></li>
                    <li><a href="#">Espadrilles</a></li>
                  </ul>
                </div>

                <div class="nav-column">
                  <h3>Home</h3>
                  <ul>
                    <li><a href="#">Pampers Diapers</a></li>
                    <li><a href="#">Huggies Diapers</a></li>
                    <li><a href="#">Seventh Generation</a></li>
                    <li><a href="#">Diapers</a></li>
                    <li><a href="#">Derbies</a></li>
                    <li><a href="#">Driving shoes</a></li>
                    <li><a href="#">Espadrilles</a></li>
                    <li><a href="#">Loafers</a></li>
                  </ul>
                </div>

                <div class="nav-column">
                  <h3>Home</h3>
                  <ul>
                    <li><a href="#">Driving shoes</a></li>
                    <li><a href="#">Espadrilles</a></li>
                    <li><a href="#">Loafers</a></li>
                  </ul>

                  <h3>Home</h3>
                  <ul>
                    <li><a href="#">Driving shoes</a></li>
                    <li><a href="#">Espadrilles</a></li>
                    <li><a href="#">Loafers</a></li>
                  </ul>
                </div>

                <div class="nav-column">
                  <h3>Home</h3>
                  <ul>
                    <li><a href="#">Pampers Diapers</a></li>
                    <li><a href="#">Huggies Diapers</a></li>
                    <li><a href="#">Seventh Generation</a></li>
                    <li><a href="#">Diapers</a></li>
                    <li><a href="#">Derbies</a></li>
                    <li><a href="#">Driving shoes</a></li>
                    <li><a href="#">Espadrilles</a></li>
                    <li><a href="#">Loafers</a></li>
                  </ul>
                </div>
                <div class="nav-column">
                  <h3>Home</h3>
                  <ul>
                    <li><a href="#">Pampers Diapers</a></li>
                    <li><a href="#">Huggies Diapers</a></li>
                    <li><a href="#">Seventh Generation</a></li>
                    <li><a href="#">Diapers</a></li>
                    <li><a href="#">Derbies</a></li>
                    <li><a href="#">Driving shoes</a></li>
                    <li><a href="#">Espadrilles</a></li>
                    <li><a href="#">Loafers</a></li>
                  </ul>
                </div>
              </div>
            </li>
        </ul>
      </li> -->
    </ul>
</div>
</div>
</div>
