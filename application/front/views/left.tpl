<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
  <div class="notifications-left">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">My Account</h3>
      </div>
      <div class="panel-body">
        <h3 class="panel-title">ORDERS</h3>
        <ul class="order-list">
          <li><a href="<%$this->config->item('site_url')%>my-orders.html">My Orders <i class="fa fa-angle-right pull-right"></i></a></li>
          <li><a href="<%$this->config->item('site_url')%>wishlist.html">My Wishlist <i class="fa fa-angle-right pull-right"></i></a></li>
          <li><a href="<%$this->config->item('site_url')%>invite-friend.html">Invite Friends <i class="fa fa-angle-right pull-right"></i></a></li>
        </ul>
        <h3 class="panel-title">SETTINGS</h3>
        <ul class="order-list">
          <li><a href="<%$this->config->item('site_url')%>profile.html">Personal Information <i class="fa fa-angle-right pull-right"></i></a></li>
          <li><a href="<%$this->config->item('site_url')%>my-address.html">My Address <i class="fa fa-angle-right pull-right"></i></a></li>
          <li><a href="<%$this->config->item('site_url')%>change-password.html">Change Password <i class="fa fa-angle-right pull-right"></i></a></li>
          <li><a href="<%$this->config->item('site_url')%>notifications.html">Notifications <i class="fa fa-angle-right pull-right"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>