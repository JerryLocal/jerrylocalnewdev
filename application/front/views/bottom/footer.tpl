<%$this->js->add_js("signup.js")%>
<footer>
    <div class="social-box">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="row"><div class="box-newletter">
                            <!-- row --><div class=""><div class="col-md-5">
                                    <h2>get in touch with us</h2>
                                </div>  <div class="col-md-7">
                                    <div class="social-box-icon">
                                        <a href="<%($this->config->item('FB_LINK') neq '') ? $this->config->item('FB_LINK') : '#'%>" target="_blank"><span class="fa fa-facebook"></span></a> 
                                        <a href="<%($this->config->item('GOOGLE_PLUS') neq '') ? $this->config->item('GOOGLE_PLUS') : '#'%>" target="_blank"><span class="fa fa-google-plus"></span></a> 
                                        <a href="<%($this->config->item('TWTTER') neq '') ? $this->config->item('TWTTER') : '#'%>" target="_blank"><span class="fa fa-twitter"></span></a> 
                                        <a href="<%($this->config->item('YOUTUBE') neq '') ? $this->config->item('YOUTUBE') : '#'%>" target="_blank"><span class="fa fa-youtube"></span></a> </div>
                                </div></div>

                        </div></div>
                </div>
                <div class="col-md-6">
                    <div class="row"><div class="box-newletter">
                            <div class="col-md-5">
                                <h2>join our mailing list</h2>
                            </div>
                            <div class="col-md-7">
                                <div class="input-box">
                                    <input type="text" id="subscriberEmail" placeholder="Enter your email address" class="input-text required-entry validate-email">
                                    <div class="actions pull-right">
                                        <button type="submit" id="subscribe" class="button"><span class="fa fa-angle-right"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="border-bottom: 1px solid #5d5d5d; padding: 25px 0px;">
        <div class="container">
            <div class="row">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td class="rborder" style="text-align: center; color: #fff; width: 25%;"><img src="<%$this->config->item('images_url')%>Great-Value.png" alt=""><br><br> Great Value</td>
                            <td class="rborder" style="text-align: center; color: #fff; width: 25%;"><img src="<%$this->config->item('images_url')%>Safe-Payment.png" alt=""><br><br>Safe Payment</td>
                            <td class="rborder" style="text-align: center; color: #fff; width: 25%;"><img src="<%$this->config->item('images_url')%>Shop-with-confidence.png" alt=""><br><br>Shop With Confidence</td>
                            <td class="rborder" style="text-align: center; color: #fff; width: 25%;"><img src="<%$this->config->item('images_url')%>Help-Center.png" alt=""><br><br>24/7 Help Center</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>    
    <div class="footer-link">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <h3><%$menu['FOOTERMENU-COMPANY'].mm_title%></h3> 
                                    <%assign var=items value=$menu['FOOTERMENU-COMPANY'].items%>
                                    <ul>
                                        <%foreach $items as $item%>
                                        <%if ($this->session->userdata('iUserId') gt '0' || $this->session->userdata('iUserId') eq '') && $item.mmi_login_req eq 'No'%>
                                        <li><a href="<%$item.mmi_external_link%>"><%$item.mmi_title%></a></li>
                                        <%/if%>
                                        <%/foreach%>
                                    </ul>
                                </div>
                                <div class="col-md-4">            


                                    <h3><%$menu['FOOTERMENU-POLICY'].mm_title%></h3> 
                                    <%assign var=items value=$menu['FOOTERMENU-POLICY'].items%>
                                    <ul>
                                        <%foreach $items as $item%>
                                        <%if ($this->session->userdata('iUserId') gt '0' || $this->session->userdata('iUserId') eq '') && $item.mmi_login_req eq 'No'%>
                                        <li><a href="<%$item.mmi_external_link%>"><%$item.mmi_title%></a></li>
                                        <%/if%>
                                        <%/foreach%>
                                    </ul>


                                </div>
                                <div class="col-md-4">            
                                    <h3><%$menu['FOOTERMENU-HELP'].mm_title%></h3> 
                                    <%assign var=items value=$menu['FOOTERMENU-HELP'].items%>
                                    <ul>
                                        <%foreach $items as $item%>
                                        <%if ($this->session->userdata('iUserId') gt '0' || $this->session->userdata('iUserId') eq '') && $item.mmi_login_req eq 'No'%>
                                        <li><a href="<%$item.mmi_external_link%>"><%$item.mmi_title%></a></li>
                                        <%/if%>
                                        <%/foreach%>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-8">
                            <%assign var=payment value=explode(',',$this->config->item('PAYMENT_METHOD_ICON'))%>
                            <h3>payment method</h3>
                            <div class="qr-code" style="    background: #fff;    padding: 0 9px;">
                                <a href="https://www.paymentexpress.com">
                                    <img src="<%base_url("public/images/payment-express.png")%>" alt="Payment Processor" width="200" height="">
                                </a>
                                <img src="<%base_url("public/images/mastercardvisa.png")%>" alt="Visa & MasterCard" height="" width='143'>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h3>QR CODE</h3>
                            <div class="qr-code">
                                <a>
                                    <img src="<%$this->config->item('settings_files_url')%><%$this->config->item('QR_CODE')%>"  class="img-responsive" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="copyright"><%$this->general->getcopyrighttext()%></div>
    </div>
</footer>

<!-- Modal -->
<div class="modal jarry-popup fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Know your order status</h4>
            </div>
            <div class="modal-body">
                <p>Please provide following details to track your order.</p>
                <div class="contact-box">
                    <div class="row">
                        <form class="form-horizontal" method="post" action='<%$this->config->item('site_url')%>track-orders.html' id="track-order-form-dr2">
                              <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label TAL">Order ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="orderName" id="inputname" placeholder="Order ID">
                                    <div class="err orderid" style="display:none;">Please Enter Order ID</div>
                                </div>
                            </div>

                            <%assign var=vEmail value=$this->generalfront->getColumnValue('iCustomerId',$this->session->userdata('iUserId'),'vEmail','mod_customer')%>
                            <%if $vEmail neq ''%>
                            <div class="form-group" style="display:none">
                                <label for="inputEmail" class="col-sm-2 control-label">Email ID</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" name="orderEmail" id="inputEmail" placeholder="Email Address" value="<%$vEmail%>">
                                </div>
                            </div>
                            <%else%>
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label TAL">Email ID</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" name="orderEmail" id="inputEmail" placeholder="Email Address">
                                    <div class="err emailid" style="display:none;">Please Enter Email ID</div>
                                </div>
                            </div>
                            <%/if%>
                            <div class="form-group">              
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <input type="submit" class="btn btn-primary btn-primary-j" value="Submit" />
                                    <button type="button" class="btn btn-default btn-default-j" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>        
            </div>
        </div>
    </div>
</div>
<!-- Modal forgot -->
<div class="modal jarry-popup fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
            </div>
            <div class="modal-body">
                <p>Enter your email address and we will send you a link to reset you password.</p>
                <div class="contact-box">
                    <form class="form-horizontal" name="frmforgotpsw" id="frmforgotpsw" action="<%$this->config->item('site_url')%>user/user/forgotpassword_action" autocomplete="off" method="post">
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
                        </div>
                    </form>
                </div>        
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-primary-j" id="forgotbtn" name="forgotbtn">Send</button>
                <button type="button" class="btn btn-default btn-default-j" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal sign up -->
<div id="signup"></div>
<div class="modal jarry-popup fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">SIGN UP</h4>
            </div>
            <div class="modal-body">
                <div class="contact-box">
                    <div class="row">
                        <div class="col-sm-5">
                            <form class="form-horizontal" name="frmSignup" id="frmSignup" action="<%$this->config->item('site_url')%>user/user/signup_action" autocomplete="off" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="" id="fname" name="fname" placeholder="First name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" value="" id="lname" name="lname" placeholder="Last name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" value="" id="vEmail" name="vEmail" placeholder="Email Address">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" value="" id="password" name="password" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <div class="checkbox-list-remember">
                                        <%assign var=privacypage value=$this->generalfront->getColumnValue('vPageCode','privacypolicy','vUrl','mod_page_settings')%>
                                        <input type="checkbox" id="termscondition" class="checkbox-list" name="termscondition" value="Yes">
                                        <label for="termscondition">By Creating an Account, you agree to our <a href="<%$this->config->item('site_url')%>terms-of-use.html" target="_blank" class="termsofuse">Terms of Use</a> and <a href="<%$this->config->item('site_url')|cat:$privacypage%>" target="_blank" class="termsofuse">Privacy Policy</a></label>
                                    </div>
                                    <span id="termsconditionErr"></span>
                                </div>


                            </form>
                        </div>
                        <div class="col-sm-2 padd-0">
                            <div class="or-txt">
                                --Or--
                            </div>
                        </div>
                        <div class="col-sm-5 padd-0">
                            <a href="<%$this->config->item('site_url')%>user/user/facebookLogin" class="fb-link-btn"><img src="<%$this->config->item('images_url')%>f-plus.png" alt="facebook" class="img-responsive"></a>
                            <a href="<%$this->config->item('site_url')%>user/user/googlePlusLogin"><img src="<%$this->config->item('images_url')%>gplus-icon.png" alt="facebook" class="img-responsive" data-target="#myModal24"></a>
                            <div class="popup-login-content">
                                <button type="button" class="btn btn-primary btn-primary-j content-signin-btn" data-dismiss="modal" data-toggle="modal" data-target="#myModal3">Sign In</button>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-primary-j" id="signupbtn">Sign Up</button>
                <button type="button" class="btn btn-default btn-default-j" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal sign in -->
<div class="modal jarry-popup fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">SIGN IN</h4>
            </div>
            <form class="form-horizontal" name="frmSignin" id="frmSignin" action="<%$this->config->item('site_url')%>user/user/login_action" autocomplete="off" method="post">
                <div class="modal-body">
                    <div class="contact-box">
                        <div class="row">
                            <div class="col-sm-5">

                                <div class="form-group">
                                    <input type="email" class="form-control dev_SR" id="vEmail" name="vEmail" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="Password" name="Password" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <div class="checkbox-list-remember">
                                        <input type="checkbox" id="remember_me" class="checkbox-list" name="remember_me" value="Yes">
                                        <label for="remember_me">Remember me</label>
                                    </div>
                                    <a href="#" class="btn btn-link pad-w-0" data-dismiss="modal" data-toggle="modal" data-target="#myModal1">Forgot Password</a>
                                </div>

                            </div>
                            <div class="col-sm-2 padd-0">
                                <div class="or-txt">
                                    --Or--
                                </div>
                            </div>
                            <div class="col-sm-5 padd-0">
                                <a href="<%$this->config->item('site_url')%>user/user/facebookLogin" class="fb-link-btn"><img src="<%$this->config->item('images_url')%>f-plus.png" alt="facebook" class="img-responsive"></a>
                                <a href="<%$this->config->item('site_url')%>user/user/googlePlusLogin"><img src="<%$this->config->item('images_url')%>gplus-icon.png" alt="Google" class="img-responsive"></a>
                                <div class="popup-signup-content">
                                    <button type="button" class="btn btn-primary btn-primary-j content-signup-btn" data-dismiss="modal" data-toggle="modal" data-target="#myModal2">Sign Up</button>
                                </div>
                            </div>
                        </div>        
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-primary-j" id="signinbtn" name="signinbtn" value="Sign In"/>
                    <!-- <button type="button" class="btn btn-info btn-info-j" data-dismiss="modal" data-toggle="modal" data-target="#myModal2">Sign Up</button> -->
                    <button type="button" class="btn btn-default btn-default-j" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<%if $this->session->userdata['iUserId'] gt 0%>
    <input type="hidden" name="user_id" id="user_id" value="<%$this->session->userdata['iUserId']%>"/>
<%else%>
    <input type="hidden" name="user_id" id="user_id" value="0"/>
<%/if%>
<!-- Modal sign in with social-->
<!-- <div class="modal jarry-popup fade" id="myModal24" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">SIGN IN</h4>
      </div>
      <div class="modal-body">
        <div class="contact-box">
          <div class="row">
            <div class="col-sm-5">      

            </div>
          </div>
        </div>        
      </div>
      
    </div>
  </div>
</div> -->