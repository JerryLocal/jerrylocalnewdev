<?php

/**
 * Description of Productlist Controller
 * 
 * @module Productlist
 * 
 * @class Productlist.php
 * 
 * @path application\front\content\controllers\content.php
 * 
 * @author Sarvil Ajwaliya
 * 
 * @date 09.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Productlist extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');

    }

    /**
     * index method is used to initialize index function.
     */
    public function index() {        
        $this->productList();
        $this->getLeftPanelSearch();
        $this->getLeftPanelFilter();
    }

    /**
     * staticpage method is used to display static pages.
     */
    function productList() {
        $postarr = $this->input->post();
        $getarr = $this->input->get();
        $curr_page = $getarr['page'];
        $cid = $this->uri->segment(2);
        $this->smarty->assign('catId', $cid);
        // $page = base_url(uri_string());
        $page = $this->config->item("site_url")."productlist/productlist/searchProducts";
        // $left_search = $this->general->sitemapCategoryWalkup(array('mc_mst_categories_id'=>$cid));  
        // $cids[] = $cid;
        // foreach ($left_search as $key => $value) {
        //     $cids[] = $value['iMstCategoriesId'];
        // }
        // $cids = array_filter($cids);
        // $cid = implode(',',$cids);
        $mycids = $this->generalfront->categorywalkup_nlevel_list_by_id($cid);
        $cid = $cid.((!empty($mycids)) ? ",".implode(',',$mycids) : "");
        if(isset($postarr['filter']) && !empty($postarr['filter'])) {
            $pids = $this->generalfront->getProductByFilter($postarr);
            $p2cids = $this->generalfront->productid_to_categoryid($pids);
            $ids = array();
            foreach($p2cids as $k=>$v) {
                $ids[] = $v['iMstCategoriesId'];
            }
            // $cid = implode(",", $ids);
            // $params['cid'] = $cid;
        // } else {
            
        }
        // pr($mycids);
        $params = array();
        $params['filter'] = $postarr['filter'];
        $params['pids'] = $pids;
        $params['cid'] = $cid;
        $params['page_index'] = $curr_page;
        $params['price_id'] = $postarr['price_id'];
        $params['sort_id'] = $postarr['sort_id'];
        $ws_name = 'get_product';
        $product_arr = $this->hbmodel->getData($ws_name, $params);
        $product_details = $merchant_details_arr['data'];
        $this->smarty->assign('product_arr',$product_arr);
        $this->smarty->assign('page',$page);
        $this->smarty->assign('cid',$cid);
    }
    function productDetail(){
        $pid = $this->uri->segment(2);             
        $params['iProductId'] = $pid;
        $ws_name = 'get_individual_product';
        $product_detail_arr = $this->hbmodel->getData($ws_name, $params);
        $product_detail_arr['data']['query'][0]['mp_description'] = nl2br($product_detail_arr['data']['query'][0]['mp_description']);

        if ($product_detail_arr['data']['query'][0]['tpo_option_value'] != '') $product_detail_arr['data']['query'][0]['tpo_option_value'] = explode(",", $product_detail_arr['data']['query'][0]['tpo_option_value']);
        // pr($product_detail_arr,1);
        $this->smarty->assign('product_detail_arr',$product_detail_arr['data']);
        if($this->session->userdata("review") =="Y"){
            $this->smarty->assign('review',$this->session->userdata("review"));
            $this->session->set_userdata("review", "N");
        }
        // Assign Meta Tags - Start
        $this->smarty->assign("Meta_Product_Name", $product_detail_arr['data']['query'][0]['mp_title']);
        $this->smarty->assign("Meta_Product_Short_Description", $product_detail_arr['data']['query'][0]['mp_short_description']);
        $this->smarty->assign("Meta_Product_Page_URL", $this->general->CleanUrl("p", $product_detail_arr['data']['query'][0]['mp_mst_products_id']));
        $this->smarty->assign("Meta_Product_IMG_URL", $this->config->item("product_img").$product_detail_arr['data']['query'][0]['mp_default_img']);
        // Assign Meta Tags - End

        
        // Get detail of product review 
        $params['iProductID'] = $pid;
        $ws_name = 'get_product_review';
        $product_review_arr = $this->hbmodel->getData($ws_name, $params);
        // pr($product_review_arr);exit();
        $this->smarty->assign('product_review_arr',$product_review_arr['data']);
        // Get Product Images
        $params['pid'] = $pid;
        $ws_name = 'get_product_images';
        $product_img_arr = $this->hbmodel->getData($ws_name, $params);
        // pr($product_img_arr,1);
        $this->smarty->assign('product_img_arr',$product_img_arr['data']);
        // get Related product 
        $params['iProductId'] = $pid;
        $ws_name = 'get_related_product';
        $related_product_arr = $this->hbmodel->getData($ws_name, $params);
        $this->smarty->assign('related_product_arr',$related_product_arr['data']['get_related_product']);
        // pr($related_product_arr);exit();
        // Update View Trigger in database
        $params['pid'] = $pid;
        $ws_name = 'ins_product_trend';
        $InsertTrend = $this->hbmodel->getData($ws_name, $params);
        $this->loadview('productdetail');

    }

    // To load the product review page and load related product data

    function ProductReview(){
        $iUserId = $this->session->userdata('iUserId'); 
        if(!$iUserId){
            $this->session->set_flashdata('failure', "Please log in to give review");
            $ref =base64_encode($_SERVER['HTTP_REFERER']);
            redirect($this->config->item('site_url') . 'login.html?ref='.$ref);
        }
        if(isset($_GET['pid']) && !isset($_POST['id'])){
            $_POST['id'] = $_GET['pid'];
            $this->smarty->assign('c',$_GET['c']);
        }
        if(!empty($_POST)){
            $params['iProductId'] = $_POST['id'];
            $ws_name = 'get_individual_product';
            $product_detail_arr = $this->hbmodel->getData($ws_name, $params);
            // pr($product_detail_arr);exit();
            $this->smarty->assign('product_detail_arr',$product_detail_arr['data']);
            $this->loadview('review');
        }else
        {
            redirect($this->config->item('site_url'));
        }
    }

    function addProductreview(){
        $iUserId = $this->session->userdata('iUserId');
        if(!empty($_POST)){
            $userdata = $this->generalfront->getRowData('iCustomerId',$iUserId,'mod_customer');            
            $params['iMstProductsId'] = $_POST['product-id'];
            $params['iRate'] = $_POST['hidden-star'];
            $params['tReview'] = $_POST['review'];
            $params['iLogedUserId'] =$userdata['iCustomerId'];
            $params['vName'] = $userdata['vFirstName']." ".$userdata['vLastName'];
            $params['vEmail'] =$userdata['vEmail'];
            
            $ws_name = 'set_product_review';
            $rating_arr = $this->hbmodel->getData($ws_name, $params);
            // pr($rating_arr);
            if($rating_arr['settings']['success'] == 1){
                //$this->session->set_flashdata('success','Review Added successfully.');                                    
                $this->session->set_userdata("review", "Y");
                // $ProductUrl = $this->general->cleanUrl("P", $_POST['product-id']);                 
                redirect($this->config->item('site_url') .'review.html?c=1&pid='.$_POST['product-id']);    
                /*redirect($ProductUrl);*/
            }else{
                redirect($this->config->item('site_url') .'review.html');    
            }
            
        }
        
    }

    function productListBySort(){
        $postArr = $this->input->post();
        $getarr = $this->input->get();
        $curr_page = $postArr['page'];
        $params = array();
        if(isset($postArr['filter']) && !empty($postArr['filter'])) {
            $pids = $this->generalfront->getProductByFilter($postArr);
            $p2cids = $this->generalfront->productid_to_categoryid($pids);
            $ids = array();
            foreach($p2cids as $k=>$v) {
                $ids[] = $v['iMstCategoriesId'];
            }
            $cid = implode(",", $ids);
            $params['cid'] = $cid;
        } else {
            if(empty($postArr['cid'])) {
                $mycids = $this->generalfront->categorywalkup_nlevel_list_by_id($getarr['catId']);
                $postArr['cid'] = $getarr['catId'].((!empty($mycids)) ? ",".implode(",", $mycids) : "");
            }
            $params['cid'] = $postArr['cid'];
        }
        $params['iSearchId'] = $postArr['sort_id'];
        $params['iPriceId'] = $postArr['price_id'];
        $params['start_price'] = $postArr['start_price'];
        $params['end_price'] = $postArr['end_price'];
        $params['filter'] = $postArr['filter'];
        $params['pids'] = $pids;
        $params['sort_by'] = $postArr['sort_id'];
        $params['price_by'] = $postArr['price_id'];
        $params['page_index'] = $curr_page;

        $ws_name = 'get_product_by_search';
        $product_arr = $this->hbmodel->getData($ws_name, $params);
        if(!isset($product_arr['settings']['count'])) {
            $product_arr['settings']['count'] = $product_arr['data'][0]['settings']['count'];
        }
        if(!isset($product_arr['settings']['curr_page'])) {
            $product_arr['settings']['curr_page'] = $product_arr['data'][0]['settings']['curr_page'];
        }
        $product_details = $merchant_details_arr['data']; 
        $product = $product_arr['data'][0]['product_data'];
        $return_value = '';
        if(count($product) > 0) {
            for ($i=0; $i <count($product) ; $i++) { 
                $return_value .= '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">' ;
                $return_value .= $this->general->ProductBlockGenerator(
                                $product[$i]['iMstProductsId'],
                                $product[$i]['vTitle'],
                                $product[$i]['fRegularPrice'],
                                $product[$i]['fSalePrice'],
                                $product[$i]['fRatingAvg'],
                                $product[$i]['fRatingAvg'],
                                $product[$i]['iStock'],
                                $product[$i]['iDifferencePer'],
                                $product[$i]['dDate'],
                                $product[$i]['iWishlistState'],
                                $product[$i]['vDefaultImg'],
                                'atc-btn-handler1');    
                $return_value .='</div>';
            }
            $page = base_url(uri_string());

            $totalRecordFound = $product_arr['settings']['count'];
            $per_page = $this->config->item('REC_LIMIT_FRONT');
            $totalpages = ceil($totalRecordFound/$per_page);
            $paging = $this->generalfront->paginateWithAjaxByFilterSearch($page, $product_arr['settings']['curr_page'], $totalpages);
            $displayResult = (($product_arr['settings']['curr_page'] * $per_page) > $product_arr['settings']['count']) ? $product_arr['settings']['count'] : $product_arr['settings']['curr_page'] * $per_page;
            $pagingstring = "Items ".((($product_arr['settings']['curr_page']-1)*$per_page)+1)." to ".$displayResult." of ".$product_arr['settings']['count']." total";

            $productCounter = $this->filterKeywordListProductCounter($params);

            $return = array("status"=>1, "content"=>$return_value, "paging"=>$paging, "pagingstring"=>$pagingstring, "totalRecordFound"=>$totalRecordFound, "productCounter"=>$productCounter);
        } else {
            $return_value .='<center><h1 style="border-bottom:none">Oops..No Records Found..!!</h1></center>';
            $return = array("status"=>0, "content"=>$return_value);
        }
        $data = json_encode($return);
        echo $data;
        exit();
    }


    // Search Functionality for the Home page search.

    function quicksearch(){
        $getarr = $this->input->post();
        $curr_page = $getarr['page'];
        $params =array();
        $params['vDetail'] = $getarr['vDetail'];
        $params['page_index'] = $curr_page;
        $ws_name = 'quicksearch';
        $search_arr = $this->hbmodel->getData($ws_name, $params);
            
        // echo json_encode($search_arr['data']);exit();
        echo json_encode($search_arr);exit();
    }


    
    function getLeftPanelSearch(){
        $cid = $this->uri->segment(2);        
        // $left_search = $this->general->sitemapCategoryWalkup($cid);
        // pr($left_search);
        // $this->smarty->assign('left_search',$left_search);

        $params = array();
        $ws_name = 'get_left_filters';
        $params['catId'] = $cid;        
        $data_arr = $this->hbmodel->getData($ws_name, $params);
        $data = $data_arr['data'];                  
        $title = $data[0]['mc_title'];
        $this->smarty->assign('list_title',$title);
        if (strlen($title) > 18) {
            $title = substr($title, 0, 18).'...';
        }
        /*$total = 0;
        foreach ($data[0]['custom_function'] as $key => $value) {            
            $total += $this->generalfront->getProductCount($value['iMstCategoriesId']);
        }*/  
        $data = $this->generalfront->BuildTree($data[0]['custom_function'],$data[0]['mc_mst_categories_id']);
        // pr($data);           

              
        // pr($total);
        // $this->smarty->assign('total',$total);
        $this->smarty->assign('title',$title);
        $this->smarty->assign('sitemap_data',$data);
    }

    function getLeftPanelFilter(){
        $cid = $this->uri->segment(2);
        $childToParentCat = $this->general->CategoryWalkup($cid);
        $cid = $childToParentCat[0]['iMstCategoriesId'];
        $params = array();
        $params['catId'] = $cid;
        $ws_name = 'get_filter_data';
        $filter_arr = $this->hbmodel->getData($ws_name, $params);
        $filter_data = $filter_arr['data'];   
        $this->smarty->assign('filter_data',$filter_data);
    }    

    // Function for Deal Of the Day Page. (deal-items.html)
    function dealOfTheDay(){
        $getarr = $this->input->get();
        $params = array();
        $curr_page = $getarr['page'];
        $params['page_index'] = $curr_page;
        $ws_name = 'deal_of_the_day';
        $deal_arr = $this->hbmodel->getData($ws_name, $params);
        // pr($deal_arr,1);
        $this->smarty->assign('product_arr',$deal_arr);
        // $this->getLeftPanelSearch();
        // $this->getLeftPanelFilter();
    }

    function dealOfDayBySort(){
        $postArr = $this->input->post();
        $getarr = $this->input->get();
        if(empty($postArr) && (isset($getarr['start_price']) || isset($getarr['end_price']) || isset($getarr['sort_id']) || isset($getarr['price_id']))) {
            $postArr['start_price'] = $getarr['start_price'];
            $postArr['end_price'] = $getarr['end_price'];
            $postArr['sort_by'] = $getarr['sort_id'];
            $postArr['price_by'] = $getarr['price_id'];
        }
        $curr_page = $postArr['page'];
        $params = array();
        $params['start_price'] = $postArr['start_price'];
        $params['end_price'] = $postArr['end_price'];
        $params['sort_by'] = $postArr['sort_by'];
        $params['price_by'] = $postArr['price_by'];
        $params['page_index'] = $curr_page;
        $ws_name = 'deal_of_the_day_by_price';
        $product_arr = $this->hbmodel->getData($ws_name, $params);
        if(!isset($product_arr['settings']['count'])) {
            $product_arr['settings']['count'] = $product_arr['data'][0]['settings']['count'];
        }
        if(!isset($product_arr['settings']['curr_page'])) {
            $product_arr['settings']['curr_page'] = $product_arr['data'][0]['settings']['curr_page'];
        }

        $product_details = $merchant_details_arr['data']; 
        $product = $product_arr['data'][0]['product_data'];
        // pr($product,1);
        $return_value = '';
        if(count($product) > 0){
            for ($i=0; $i <count($product) ; $i++) { 
               $return_value .= '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">' ;
               $return_value .= $this->general->ProductBlockGenerator(
                                $product[$i]['mp_mst_products_id_1'],
                                $product[$i]['mp_title_1'],
                                $product[$i]['mp_regular_price_1'],
                                $product[$i]['mp_sale_price_1'],
                                $product[$i]['mp_rating_avg_1'],
                                $product[$i]['mp_rating_avg_1'],
                                $product[$i]['mp_stock'],
                                $product[$i]['mp_difference_per'],
                                $product[$i]['mp_date_1'],
                                $product[$i]['mp_wishlist_state_1'],
                                $product[$i]['mp_default_img_1'],
                                'atc-btn-handler1');    
               $return_value .='</div>';
            }
            $page = base_url(uri_string());

            $per_page = $this->config->item('REC_LIMIT_FRONT');
            $totalpages = ceil($product_arr['settings']['count']/$per_page);
            $paging = $this->generalfront->paginateWithAjaxByFilterSearch($page, $product_arr['settings']['curr_page'], $totalpages);
            $displayResult = (($product_arr['settings']['curr_page'] * $per_page) > $product_arr['settings']['count']) ? $product_arr['settings']['count'] : $product_arr['settings']['curr_page'] * $per_page;
            $pagingstring = "Items ".((($product_arr['settings']['curr_page']-1)*$per_page)+1)." to ".$displayResult." of ".$product_arr['settings']['count']." total";
            $retAr = array("status"=>1, "content"=>$return_value, "paging"=>$paging, "pagingstring"=>$pagingstring);
        }else{
            $return_value .='<center><h1 style="border-bottom:none">Oops..No Deal Found..!!</h1></center>';
            $retAr = array("status"=>0, "content"=>$return_value);
        }  
        echo json_encode($retAr);      
        // echo $return_value;
        exit();    
    }


    function searchResult(){
        $getarr = $this->input->get();
        $params = array();
        $curr_page = $getarr['page'];
        $params['page_index'] = $curr_page;
        $params['vDetail'] = addslashes($getarr['search']);
        $ws_name = 'quicksearch_inner';
        $search_arr = $this->hbmodel->getData($ws_name, $params);
        // pr($search_arr);
        $this->smarty->assign('product_arr',$search_arr);
        $this->smarty->assign('vDetail',$getarr['search']);
        // $this->getLeftPanelSearch();
        // $this->getLeftPanelFilter();
    }

    function searchResultBySort(){
        $postArr = $this->input->post();
        $params = array();
        $params['start_price'] = $postArr['start_price'];
        $params['end_price'] = $postArr['end_price'];
        $params['sort_by'] = $postArr['sort_by'];
        $params['price_by'] = $postArr['price_by'];
        $params['vDetail'] = $postArr['vDetail'];
        $params['page'] = $postArr['page'];
        $ws_name = 'quicksearch_inner_by_sort';
        $product_arr = $this->hbmodel->getData($ws_name, $params);
        if(!isset($product_arr['settings']['count'])) {
            $product_arr['settings']['count'] = $product_arr['data'][0]['settings']['count'];
        }
        if(!isset($product_arr['settings']['curr_page'])) {
            $product_arr['settings']['curr_page'] = $product_arr['data'][0]['settings']['curr_page'];
        }
        $product_details = $merchant_details_arr['data']; 
        $product = $product_arr['data'][0]['product_data'];        
        if(count($product) > 0){
            $return_value = '';
            for ($i=0; $i <count($product) ; $i++) { 
               $return_value .= '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">' ;
               $return_value .= $this->general->ProductBlockGenerator(
                                $product[$i]['mp_mst_products_id_1'],
                                $product[$i]['mp_title_1'],
                                $product[$i]['mp_regular_price_1'],
                                $product[$i]['mp_sale_price_1'],
                                $product[$i]['mp_rating_avg_1'],
                                $product[$i]['mp_rating_avg_1'],
                                $product[$i]['mp_stock'],
                                $product[$i]['mp_difference_per'],
                                $product[$i]['mp_date_1'],
                                $product[$i]['mp_wishlist_state_1'],
                                $product[$i]['mp_default_img_1'],
                                'atc-btn-handler1');    
               $return_value .='</div>';

            }
            $page = base_url(uri_string());

            $per_page = $this->config->item('REC_LIMIT_FRONT');
            $totalpages = ceil($product_arr['settings']['count']/$per_page);
            $paging = $this->generalfront->paginateWithAjaxByFilterSearch($page, $product_arr['settings']['curr_page'], $totalpages);
            $displayResult = (($product_arr['settings']['curr_page'] * $per_page) > $product_arr['settings']['count']) ? $product_arr['settings']['count'] : $product_arr['settings']['curr_page'] * $per_page;
            $pagingstring = "Items ".((($product_arr['settings']['curr_page']-1)*$per_page)+1)." to ".$displayResult." of ".$product_arr['settings']['count']." total";
            $retAr = array("status"=>1, "content"=>$return_value, "paging"=>$paging, "pagingstring"=>$pagingstring);
        }else
        {
            $return_value .='<center><h1 style="border-bottom:none">Oops..No Records Found..!!</h1></center>';
            $retAr = array("status"=>0, "content"=>$return_value);
        }
        // echo $return_value;
        echo json_encode($retAr);
        exit();    
    }

    function searchProducts(){
        $postArr = $this->input->post();
        $getarr = $this->input->get();
        if(empty($postArr) && (isset($getarr['filter']) || isset($getarr['start_price']) || isset($getarr['end_price']) || isset($getarr['catId']) || isset($getarr['sort_id']) || isset($getarr['price_id']))) {echo 1;
            $postArr['filter'] = $getarr['filter'];
            $postArr['startPrice'] = $getarr['start_price'];
            $postArr['endPrice'] = $getarr['end_price'];
            $postArr['catId'] = $getarr['catId'];
            $postArr['sort_id'] = $getarr['sort_id'];
            $postArr['price_id'] = $getarr['price_id'];
        }
        $curr_page = $postArr['page'];
        if(!empty($postArr)){
            $pids = $this->generalfront->getProductByFilter($postArr);
            // $ids = $this->general->sitemapCategoryWalkup(array('mc_mst_categories_id'=>$postArr['catId']));

            // $mycids = $this->generalfront->categorywalkup_nlevel_list_by_id($postArr['catId']);
            // $cids = $postArr['catId'].((!empty($mycids)) ? ",".implode(",", $mycids) : "");

            // $ids = $postArr['ids'];
            // if(!empty($ids)) {
            //     $brand = "'".implode("','",$ids)."'";    
            //     $pids = $this->generalfront->getProductByBrand($brand);
            // } else {
            //     $pids = $this->generalfront->getProductByBrand('');
            // }
            // if(strlen($pids)){
                // $retArr = $this->generalfront->getProductsBasedOnPids($pids);
            // }
            $params['filter'] = $postArr['filter'];
            $params['pids'] = ($pids) ? $pids : 0;
            $params['cids'] = $postArr['cid'];
            $params['startPrice'] = $postArr['start_price'];
            $params['endPrice'] = $postArr['end_price'];
            $params['page_index'] = $curr_page;
            $params['sort_id'] = $postArr['sort_id'];
            $params['price_id'] = $postArr['price_id'];
            
            $ws_name = "search_products";
            $product_arr = $this->hbmodel->getData($ws_name, $params);
            $products = $product_arr['data'];
            $str = '';            
            if(count($products) > 0){
                foreach ($products as $key => $product) {
                    $str .= '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">';
                    $str .= $this->general->ProductBlockGenerator(
                            $product['mp_mst_products_id'],
                            $product['mp_title'],
                            $product['mp_regular_price'],
                            $product['mp_sale_price'],
                            $product['mp_rating_avg'],
                            $product['mp_rating_avg'],
                            $product['mp_stock'],
                            $product['mp_difference_per'],
                            $product['mp_date'],
                            $product['mp_wishlist_state'],
                            $product['mp_default_img'],
                                'atc-btn-handler1');
                    $str .= "</div>";
                }
                $page = base_url(uri_string());
                $per_page = $this->config->item('REC_LIMIT_FRONT');
                $totalRecordFound = $product_arr['settings']['count'];
                $totalpages = ceil($product_arr['settings']['count']/$per_page);
                $paging = $this->generalfront->paginateWithAjaxByFilterSearch($page, $product_arr['settings']['curr_page'], $totalpages);
                $displayResult = (($product_arr['settings']['curr_page'] * $per_page) > $product_arr['settings']['count']) ? $product_arr['settings']['count'] : $product_arr['settings']['curr_page'] * $per_page;
                $pagingstring = "Items ".((($product_arr['settings']['curr_page']-1)*$per_page)+1)." to ".$displayResult." of ".$product_arr['settings']['count']." total";
                $retAr = array("status"=>1, "content"=>$str, "paging"=>$paging, "pagingstring"=>$pagingstring, "totalRecordFound"=>$totalRecordFound);
            }else{
                $str .='<center><h1 style="border-bottom:none">Oops..No Records Found..!!</h1></center>';
                $retAr = array("status"=>0,"content"=>$str);
            }
            echo json_encode($retAr);
        }
        exit();
    }

    function checkCategoryOpton() {
        // $postarr = $this->input->post();
        // $result = $this->generalfront->getCategoryOptionsAvailable($postarr);
        // echo json_encode($result);
        // exit();

        $postarr = $this->input->post();
        $mycids = $this->generalfront->categorywalkup_nlevel_list_by_id($postarr['catid']);
        $postarr['cid'] = $postarr['catid'].((!empty($mycids)) ? ",".implode(",", $mycids) : "");
        // $result = $this->generalfront->getCategoryOptionsAvailable($postarr);
        $result = $this->generalfront->getDisableFilterValue($postarr);
        echo json_encode($result);
        exit();
    }

    function loadfilterlist() {
        $postarr = $this->input->post();
        $checkfilterdisable = $this->checkDisableFilterValueByLoadFilterList($postarr['catid']);
        $c2pcategory = $this->general->CategoryWalkup($postarr['catid']);
        $postarr['catid'] = $c2pcategory[0]['iMstCategoriesId'];  // overwrite post catid for get child to parent
        $result = $this->generalfront->getCategoryOptionsAvailable($postarr, true);
        $i = 0;
        $html = "";
        foreach($result as $key=>$value) {
            // if(in_array($postarr['filter']."|".$value['vValue'], $checkfilterdisable['filter'])) {
            //     $disable = "";
            //     $style = "";
            //     $productcount = $checkfilterdisable['totalProduct'];
            // } else {
            //     $disable = "disabled='disabled'";
            //     $style = "style='color:#A1A1A1;'";
            //     $productcount = 0;
            // }
            // <span id='tot_count'>(".$this->generalfront->getBrandCount($value['vValue'], true).")</span>
            // $html .= "<li><div class='checkbox'><input id='".$postarr['filter']."checkbox".($i+1)."' type='checkbox' class='chk' name='".$postarr['filter']."checkbox".($i+1)."' value='".$postarr['filter']."|".$value['vValue']."' onclick='return filterlistcheckbox();' ".$disable."><label for='".$postarr['filter']."checkbox".($i+1)."' ".$style.">".$value['vValue']." <span id='tot_count'>(".$productcount.")</span><span id='zero_count' style='display:none'>(0)</span> </label></div>";
            // $html .= "</li>";
            if(in_array($postarr['filter']."|".$value['vValue'], $checkfilterdisable['filter'])) {
                $productcount = $checkfilterdisable['totalProduct'];
                $html .= "<li><div class='checkbox'><input id='".$postarr['filter']."checkbox".($i+1)."' type='checkbox' class='chk' name='".$postarr['filter']."checkbox".($i+1)."' value='".$postarr['filter']."|".$value['vValue']."' onclick='return filterlistcheckbox();'><label for='".$postarr['filter']."checkbox".($i+1)."'>".$value['vValue']." <span id='tot_count'>(".$productcount.")</span><span id='zero_count' style='display:none'>(0)</span> </label></div>";
                $html .= "</li>";
            }
            $i++;
        }
        if(!isset($postarr['key'])) {
            $html .= "<li><div class='checkbox'><input id='".$postarr['filter']."checkbox".($i)."' type='checkbox' class='chk' name='".$postarr['filter']."checkbox".($i)."' value='".$postarr['filter']."|other' onclick='return filterlistcheckbox();'><label for='".$postarr['filter']."checkbox".($i)."'>Other</label></div></li>";
        }
        $response['filterchklist'] = $html;
        $mycids = $this->generalfront->categorywalkup_nlevel_list_by_id($postarr['catid']);
        $params['cid'] = $postarr['catid'].((!empty($mycids)) ? ",".implode(",", $mycids) : "");
        $productCounter = $this->filterKeywordListProductCounter($params);
        $response['productCounter'] = $productCounter;
        echo json_encode($response);
        exit();
    }

    function p2ccategory() {
        $postarr = $this->input->post();
        $catid = $postarr['catid'];
        $p2ccategoryids = $mycids = $this->generalfront->categorywalkup_nlevel_list_by_id($postarr['catid']);
        echo $catid.",".((!empty($p2ccategoryids)) ? implode(",",$p2ccategoryids) : "");
        exit;
    }

    function maxpricebycategory() {
        $postarr = $this->input->post();
        $catid = $postarr['catid'];
        $cids = $this->generalfront->categorywalkup_nlevel_list_by_id($catid);
        $cids = $catid.((!empty($cids)) ? ",".implode(",", $cids) : "");
        echo $this->generalfront->getMaxPriceForFilter($cids);
        exit;
    }

    function checkDisableFilterValue() {
        $postarr = $this->input->post();
        $filtervalue = $this->generalfront->getDisableFilterValue($postarr);
        $totalProduct = $this->generalfront->getTotalProductForFilter($postarr);
        $array = array();
        foreach($filtervalue as $key=>$value) {
            $array[] = $value['filterValue'];
        }
        $filtervalue = implode(",", $array);

        echo json_encode(array("filtervalue"=>$filtervalue, "totalProduct"=>$totalProduct));
        exit;
    }

    function checkDisableFilterValueByLoadFilterList($catid) {
        $p2cnlevelids = $this->generalfront->categorywalkup_nlevel_list_by_id($catid);
        $id['cid'] = $catid.(!empty($p2cnlevelids) ? ",".implode(",", $p2cnlevelids) : "");
        $filtervalue = $this->generalfront->getDisableFilterValue($id);
        $totalProduct = $this->generalfront->getTotalProductForFilter($id);
        $array = array();
        foreach($filtervalue as $key=>$value) {
            $array[] = $value['filterValue'];
        }
        // $filtervalue = implode(",", $array);
        $disablearray = array('filter'=>$array, 'totalProduct'=>$totalProduct);
        return $disablearray;
    }

    function filterKeywordListProductCounter($params) {
        $data = $this->generalfront->filterKeywordListProductCount($params);
        return $data;
    }

    function filterKeywordListProductCounterAjax() {
        $postarr = $this->input->post();
        $result = $this->filterKeywordListProductCounter($postarr);
        echo json_encode(array("productCounter"=>$result));
        exit;
    }
}
