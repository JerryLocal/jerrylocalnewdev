<%$this->css->add_css('multizoom.css')%>
<%$this->js->add_js("wishlist.js")%>
<%$this->js->add_js("review.js")%>
<%$this->js->add_js("multizoom.js","recentlyReview.js")%>
<script type="text/javascript">
var Review = "<%$review%>";
</script>
<div id="wishlist"></div>
<div class="main-containt">
<div class=""> <!-- container -->
<div class="row-fluid">
<div class="breadcrumbs">
       <div class="container">
    <div class="row-fluid">
    <%$this->general->Breadcrumb('P',"<%$product_detail_arr['query'][0]['mp_mst_products_id']%>","<%$product_detail_arr['query'][0]['mp_title']%>")%>      
        </div>
   </div>
      </div>
     
<div class="product-details-section">
  <div class="container">
    <div class="product-details-box">
      <div class="product-page">
      <div class="row">
          <div class="col-md-5 col-lg-4">
              <input type="hidden" name="productid" id="productid" value="<%$product_detail_arr['query'][0]['mp_mst_products_id']%>">
          <div class="product-main-image ">
          <img id="multizoom2" class="img-responsive" src="<%$product_img_arr['query_mid'][0]['mpi_img_path']%>"> 
            <a class="zoom-icon lightview" data-lightview-options="skin: 'mac'" href="<%$product_img_arr['query_large'][0]['mpi_img_path']%>"><i class="fa fa-search-plus fa-lg"></i></a> </div>
            <%if $product_img_arr['query_small']|@count gt '0'%>
            <div class="multizoom2 thumbs product-other-images">
              <ul class="bxslider">
              <%foreach $product_img_arr['query_small'] as $simg%> 
              <li><a class="img_sub" href="<%$product_img_arr['query_mid'][$simg@index]['mpi_img_path_1']%>" data-large="<%$product_img_arr['query_large'][$simg@index]['mpi_img_path_2']%>"><img src="<%$simg['mpi_img_path']%>"></a></li>
              <%/foreach%>
            </ul>
            </div>
            <%/if%> 
          </div>
          <div class="col-md-7 col-lg-6">
             <!-- To check is product in wishlist -->
            <%assign var=WishlistArr value=$this->generalfront->checkIsPrductInWishlist()%>
            <%if in_array($product_detail_arr['query'][0]['mp_mst_products_id'], $WishlistArr)%>
                <%assign var=cls value='color:orange'%>
            <%/if%>
            <h1><span><%$product_detail_arr['query'][0]['mp_title']%></span>
              <ul>
                <li class="fl"><a style="width:20px; display: block; padding: 0;" class="add_wish_list fl" data-id="<%$product_detail_arr['query'][0]['mp_mst_products_id']%>"><i class="fa fa-heart add_wish_list" data-id="<%$product_detail_arr['query'][0]['mp_mst_products_id']%>" style="<%$cls%>" onclick="addtowishlist(<%$product_detail_arr['query'][0]['mp_mst_products_id']%>,'detailpage')"></i></a><span id="wishlist_count" style="margin-right:5px;"><%if $product_detail_arr['query'][0].iWishlistState gt 0%>(<%$product_detail_arr['query'][0].iWishlistState%>)<%/if%></span></li>                
                <li><a href="<%$this->config->item('site_url')%><%$product_detail_arr['query'][0]['mp_mst_products_id']%>/mail-to-friend.html"><i class="fa fa-envelope"></i></a></li>
              <div class="clear"></div>
              </ul>
              <div class="clear"></div>
            </h1>
            <div class="review">
              <a id="seeReviews"><%$product_review_arr|@count%> Review(s)</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a id="add_your_review" class="add_your_review" data-rel="<%$product_detail_arr['query'][0]['mp_mst_products_id']%>">Add Your Review</a>
               </div>
               <form id="add_review" method="POST" action="<%$this->config->item('site_url')%>review.html" target="_blank">
                <input type="hidden" value="<%$product_detail_arr['query'][0]['mp_mst_products_id']%>" name="id" id="id">
               </form>
            <div class="description">
              <p><%$product_detail_arr['query'][0]['mp_short_description']%></p>
            </div>
            
            <div class="sku-code">
              <!-- <div class="text"> <strong>CODE :</strong> <%$product_detail_arr['query'][0]['mp_sku']%> </div> -->
              <%foreach $product_detail_arr['product_option'] as $option%>
              <%if $option['tpo_category_option_value_1'] neq '' && $option['tpo_option_value_1'] neq ''%>
              <div class="text"> <strong><%$option['tpo_category_option_value_1']%> : </strong> <%$option['tpo_option_value_1']%> </div>
              <%/if%>              
              <%/foreach%>
            </div>
            <%$this->general->ProductPriceFormat('3',$product_detail_arr['query'][0]['mp_regular_price'],$product_detail_arr['query'][0]['mp_sale_price'],$product_detail_arr['query'][0]['mp_difference_per'])%>
            <span class="text-success">
              Delivery Charges : 
              <%if $product_detail_arr['query'][0]['eFreeShipping'] eq 'No' && $product_detail_arr['query'][0]['fShippingCharge'] gt 0%>
                  <%$this->config->item('CURRENCY_SYMBOL')%> <%number_format($product_detail_arr['query'][0]['fShippingCharge'],2)%>
                  <%else%>Free<%/if%>
                </span><br>
                <span class="text-success">Return Policy : <%$product_detail_arr['query'][0]['iReturnDays']%> Days</span>
           
            <%if $product_detail_arr['query'][0]['tpo_option_value']|@count gt 0 && $product_detail_arr['query'][0]['tpo_option_value'] neq ''%>
            <div class="select-size clearfix">
              <ul class='pull-left'>
                <li><strong class="text"><!-- SELECT --> <%ucfirst($product_detail_arr['query'][0]['tpo_category_option_value'])%>:</strong></li>              
              </ul>

              <div class="btn-group  pull-left" data-toggle="buttons" id='size-handler-<%$product_detail_arr['query'][0]['mp_mst_products_id']%>'>
                <%foreach from=$product_detail_arr['query'][0]['tpo_option_value'] key=index item=option%>    
                <label class="btn  <%if $index eq 0%>active<%/if%>">
                <input type="radio" value='<%$option%>' name="productoption" class=''  id="sizeoption<%$option%>" autocomplete="off" <%if $index eq 0%>checked<%/if%>> <%$option%>
                </label>
                <%/foreach%>                
              </div>
            </div>
            <%/if%>

            <div class="product-page-cart"> 
              <!--  && $product_detail_arr['query'][0]['mp_stock'] gt $product_detail_arr['query'][0]['mp_low_avl_limit_notification'] -->
              <%if $product_detail_arr['query'][0]['mp_stock'] gt 0 && $product_detail_arr['query'][0]['mp_regular_price'] > 0 && $product_detail_arr['query'][0]['mp_sale_price'] > 0 && $product_detail_arr['query'][0]['mp_status'] eq 'Active'%>
              <strong class="text">QTY :</strong>
              <div class="col-md-3">
                    <div class="product-quantity-spinner">
                      <div class="input-group input-small">
                        <input type="text" class="spinner-input form-control" maxlength="3" readonly id='qty-handler-<%$product_detail_arr['query'][0]['mp_mst_products_id']%>'>
                        <div class="spinner-buttons input-group-btn btn-group-vertical">
                          <button type="button" class="btn spinner-up btn-xs blue">
                          <i class="fa fa-angle-up"></i>
                          </button>
                          <button type="button" class="btn spinner-down btn-xs blue">
                          <i class="fa fa-angle-down"></i>
                          </button>
                        </div>
                      </div>
                    </div>
              </div>
            <%else%>
            <button class='out_of_stock'>Out of Stock</button>
            <%/if%>
              <ul class="social-icons">
                <li><strong class="text">SHARE  VIA :</strong></li>
                <li><a data-original-title="facebook" class="facebook sprite" href="http://www.facebook.com/sharer.php?u=<%$this->general->cleanUrl("P",$product_detail_arr['query'][0]['mp_mst_products_id'])%>" target="_blank">
                    </a></li>
                <li><a href="https://plus.google.com/share?url=<%$this->general->cleanUrl("P",$product_detail_arr['query'][0]['mp_mst_products_id'])%>" data-original-title="googleplus" class="googleplus sprite" target="_blank"></a></li>
                <li><a href="https://twitter.com/share?url=<%$this->general->cleanUrl("P",$product_detail_arr['query'][0]['mp_mst_products_id'])%>" data-original-title="twitter" class="twitter sprite" target="_blank"></a></li>
              </ul>              
            </div>
            <!--  && $product_detail_arr['query'][0]['mp_stock'] gt $product_detail_arr['query'][0]['mp_low_avl_limit_notification'] -->
            <%if $product_detail_arr['query'][0]['mp_stock'] gt 0 && $product_detail_arr['query'][0]['mp_regular_price'] > 0 && $product_detail_arr['query'][0]['mp_sale_price'] > 0 && $product_detail_arr['query'][0]['mp_status'] eq 'Active'%>
            <button class="btn align-left buy-now atc-btn-handler" data-buynow='buynow' data-id='<%$product_detail_arr['query'][0]['mp_mst_products_id']%>'>Buy Now</button> 
              <%$product_detail_arr['custom_function'][0]['OrderBtn']%>
            <%/if%>
          </div>
          <div class="col-md-12 col-lg-2">
          <%$this->general->SellerBlock($product_detail_arr['query'][0]['mp_mst_store_id'])%>
          </div>
          <div class="product-page-content">
          <div class="container">
          <div class="row">
          <div class="container">
            <ul class="nav nav-tabs" id="myTab">
              <li class="active"><a aria-expanded="true" data-toggle="tab" href="#Description">Description</a></li>
              <li class=""><a aria-expanded="false" data-toggle="tab" href="#Information">Rating &amp; Reviews</a></li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div id="Description" class="tab-pane fade technical-des active in">
                    <div class="description">
                              <%$product_detail_arr['query'][0]['mp_description']%>
                    </div>
                    <!-- <div class="technical-des-main">
                        <div class="item-deaitl"> 
                            <div aria-multiselectable="true" role="tablist" id="accordion" class="panel-group">
                              <div class="panel panel-default">
                                <div id="headingThree" role="tab" class="panel-heading">
                                  <h4 class="panel-title">
                                    <a aria-controls="collapseThree" aria-expanded="false" href="#collapseThree" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed"><i class="fa fa-plus-square-o fa-lg"></i><i class="fa fa-minus-square-o fa-lg"></i>
                                      Detail
                                    </a>
                                  </h4>
                                </div>
                                <div aria-labelledby="headingThree" role="tabpanel" class="panel-collapse collapse" id="collapseThree" aria-expanded="false" style="height: 0px;">
                                  <div class="panel-body technical-des1">
                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>-->
              
            </div>
            <div id="Information" class="tab-pane fade">
                <div class="comment-reveiw-box">
                  <a id="add_your_review" class="add_your_review btn btn-common" data-rel="<%$product_detail_arr['query'][0]['mp_mst_products_id']%>">Add Your Review</a>
                </div>
                <%if $product_review_arr|@count gt 0%>

                      <%foreach $product_review_arr as $review%>             
                        
                       
                <div class="comment-reveiw-box">
                  <div class="row">
                      <div class="col-md-3">
                            <%$this->generalfront->productRatingFormat($review['tpr_rate'])%>
                            <p class="review-member-name"><%$review['tpr_name']%></p>
                            <p class="review-date"><%$review['tpr_date']%></p>
                      </div>
                      <div class="col-md-9">
                        <p><%$review['tpr_review']%></p>
                      </div>
                    </div>
                </div>
                 <%/foreach%>                      
                <%/if%> 
              </div>
          </div>
          <%if $related_product_arr|@count gt 0%>
          <div class="title-heading">similar Products</div>
          <div id="owl-example" class="owl-carousel01">
                        <%foreach $related_product_arr as $product%>
                        <div class="col-md-12">
                          <%$this->general->ProductBlockGenerator(
                          $product['mp_mst_products_id'],$product['mp_title'],$product['mp_regular_price'],$product['mp_sale_price'],$product['mp_rating_avg'],$product['mp_rating_avg'],$product['mp_stock'],$product['mp_difference_per'],$product['mp_date'],$product['mp_wishlist_state'],$product['mp_default_img'])%>
                        </div>
                      <%/foreach%>
                </div>
          <%/if%>      
          </div>
          </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
        
        <%javascript%>
            $(document).ready(function () {
              $('#multizoom2').addimagezoom({
                magnifiersize: [400,400],
                zIndex:5,
                disablewheel: true
              });
            });
          $( document ).ready(function() {
            //$pid = $('#pid').value();
            if(checkBrowserSupport){
                // Check Item Already Added in storage or not
              // $.ajax({
              //   method: "POST",
              //   url: "some.php",
              //   data: { pid: $pid}
              // }).done(function( msg ) {
              //     InsertToStorage("view_counter",$pid,false);
              // }); 
            //var testObject = {'title':1};
            //  var temp = JSON.stringify(testObject);
            // InsertToStorage("view_counter",temp);
            //  var obj = GetFromStorage('view_counter');
            //  if(obj != null){
             //   console.log("DATA",obj);
            //  }
              //alert("A");
              }
          });
        <%/javascript%>