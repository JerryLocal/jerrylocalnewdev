<%$this->js->add_js("dealofday.js")%>
<%$this->css->add_css("jquery.mCustomScrollbar.css")%>
<%$this->css->css_src()%>

<%assign var=settings value=$product_arr['settings']%>
<%assign var=per_page value=$settings['per_page']%>
<%assign var=totalPage value= ceil($settings.count/$per_page)%>
<%assign var=tpages value= ceil($settings.count/$per_page)%>
<!-- < %$product_arr|@pr%> -->

<script>
    var maxprice = "<%$this->generalfront->getMaxPriceForFilter('', 'true')%>";
</script>
<div class="main-containt">
  <div class=""> <!-- container -->
    <div class="row-fluid">
      <div class="breadcrumbs" style="padding: 0 0px;">
        <div class="container">
          <div class="row-fluid">
                 <%$this->general->Breadcrumb('CLEAN','Deal of the day')%>
          </div>
        </div>
      </div>      
      <div class="product-list">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
              <div class="accordion">
                <div class="accordion-group bt-bor">
                  <div class="accordion-heading filter-sub-heading"> <a class="accordion-toggle" data-toggle="collapse" href="#price"> Price <span></span></a> </div>
                  <div id="price" class="accordion-body collapse in">
                    <div class="accordion-inner filter-block">
                      <div class="price-rang">Range :
                        <input type="text" id="amount" readonly >
                      </div>
                      <div class="range-wrap">
                        <div id="slider-range"></div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
              <div id="loader" style="display:none;margin-top:125px;" align="center"><img src="<%$this->config->item('images_url')%>fancybox_loading.gif" /></div>
              <div class="short-bar">
                <div class="col-md-5">                  
                  <div class="row">Deal Of The Day</div>
                </div>
                <form id="sort_by_form" method="POST" action="productlist/productlist/productList">
                <div class="col-md-7">
                  <div class="row">
                    <div class="shortby">
                      <div class="shorten">
                        <label>Sort By</label>
                        <select id="sort_by" name="sort_by">
                          <option value='1'selected="selected">Best Selling</option>
                          <option value='2'>New</option>
                          <option value='3'>Discounts</option>
                          <option value='4'>Trending</option>
                        </select>
                      </div>
                      <div class="shorten">
                        <label>Price</label>
                        <select id="price_by" name="price_by">                          
                          <option value="2" selected="selected">Low</option>
                          <option value="1">High</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              </div>
              <div id="catalog-listing">
                <div class="category-products page-product-list">
                  <div class="row sort_result">                     
                      <%if $product_arr['data']|@count gt 0%>
                      <%assign var=products value=$product_arr['data']%>
                      <%foreach $products as $product%>    
                      <!-- < %assign var=product value=$product['get_product_details'][0]%> -->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">  
                        <%$this->general->ProductBlockGenerator($product.mp_mst_products_id_1,$product.mp_title_1,$product.mp_regular_price_1,$product.mp_sale_price_1,$product.mp_rating_avg_1,$product.mp_rating_avg_1,$product.mp_stock,$product.mp_difference_per_1,$product.mp_date_1,$product.mp_wishlist_state_1,$product.mp_default_img_1)%>
                        </div>
                        <%/foreach%>
                        <%else%>
                        <div class="short-bar">
                            <div class="col-md-12" style="text-align:center">                  
                              <div class="row">Store is currently unavailable</div>
                            </div>
                        </div>                    
                      <%/if%>                    
                  </div>
                </div>
              </div>
              <div class="pagination-box">
                <div class="col-md-7">
                  <%assign var=displayResult value=($settings.curr_page*$per_page gt $settings.count) ? $settings.count : $settings.curr_page*$per_page%>
                  <div class="row">
                    <div class="pagination-list">Items <%(($settings.curr_page-1)*$per_page)+1%> to <%$displayResult%> of <%$settings.count%> total</div>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="row">
                    <ul class="pagination">
                      <%assign var=reload value=$this->config->item('site_url')|cat:'deal-items.html'|cat:"?tpages="|cat:$tpages%>            
                        <%if $totalPage gt 1%>
                        <%$this->generalfront->paginateWithAjax($reload,$product_arr['settings'].curr_page,$totalPage)%>
                        <%/if%>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>