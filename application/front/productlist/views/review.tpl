<%$this->js->add_js("wishlist.js")%>
<%$this->js->add_js("review.js")%>
<script type="text/javascript">
var c = "<%$c%>";
</script>
<div id="review"></div>
<div class="main-containt">
<div class="container">
<div class="row-fluid">
<%$this->general->Breadcrumb('P',"<%$product_detail_arr['query'][0]['mp_mst_products_id']%>","<%$product_detail_arr['query'][0]['mp_title']%>")%>
</div></div>
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
  <div class="review-lft-section">
      <h3>You have choosen Review</h3>
        <div class="product-details">
          <%if $product_detail_arr['query'][0]['mp_default_img'] neq ''%>
              <img src="<%$this->config->item('product_img')%><%$product_detail_arr['query'][0]['mp_default_img']%>" alt="img" class="img-responsive">
          <%else%>
              <img src="<%$this->config->item('images_url')%>no-image-cat.jpg" alt="img" class="img-responsive">
          <%/if%>          
        <strong><%$product_detail_arr['query'][0]['mp_title']%></strong>
        <p><%$product_detail_arr['query'][0]['mp_short_description']%></p>
        </div>
    </div>
</div>
<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
<div class="delivery-box">
<%assign var=p_url value=$this->generalfront->getCleanURL('P',$product_detail_arr['query'][0]['mp_mst_products_id'])%>
<h3><div class="row"><div class="col-sm-8">Write Review</div><div class="col-sm-4"><span class="btn-back pull-right"><a href="<%$this->config->item('site_url')%>p/<%$product_detail_arr['query'][0]['mp_mst_products_id']%>/<%$p_url%>">Back to Product</a></span></div></div></h3>
<div class="form-add">
        <form class="form-horizontal" method="POST" action="productlist/productlist/addProductreview" id="frmReview" name="frmReview">
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Your Review</label>
                        <div class="col-sm-9">
                         <textarea class="form-control" id="inputComment" rows="3" name="review"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-3 control-label">Your Rating</label>
                        <div class="col-sm-9">
                          <div class="col-md-3">
                        <ul class="rating-star">
                                <li><span class="review-star" id="review-star-1" data-rel="1"></span></li>
                                <li><span class="review-star" id="review-star-2" data-rel="2"></span></li>
                                <li><span class="review-star" id="review-star-3" data-rel="3" ></span></li>
                                <li><span class="review-star" id="review-star-4" data-rel="4"></span></li>
                                <li><span class="review-star" id="review-star-5" data-rel="5"></span></li>
                        </ul>                        
                        <input type="hidden" value="1" name="hidden-star" id="hidden_star"> 
                        <input type="hidden" value="<%$product_detail_arr['query'][0]['mp_mst_products_id']%>" name="product-id" id="hidden_id"> 
                      </div>
                      <span id="starEr"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <input type="submit" class="btn btn-common" id="postReview" value="Submit">
                          <!-- <button type="submit" class="btn btn-blue"> -->
                            <a class="btn btn-common" href="<%$this->config->item('site_url')%>p/<%$product_detail_arr['query'][0]['mp_mst_products_id']%>/<%$p_url%>">Cancel</a>
                          <!-- </button> -->
                          <!-- <a href="<%$this->config->item('site_url')%>p/<%$product_detail_arr['query'][0]['mp_mst_products_id']%>/sample.html">Cancel</a> -->
                        </div>
                      </div>
                    </form>
    </div>
</div>
</div>
</div>
</div>
</div>