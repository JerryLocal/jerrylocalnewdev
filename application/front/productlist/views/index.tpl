<%$this->js->add_js("search.js","wishlist.js","add-to-cart.js")%>
<%$this->css->add_css("jquery.mCustomScrollbar.css")%>
<%$this->css->css_src()%>

<%assign var=settings value=$product_arr['settings']%>
<%assign var=per_page value=$this->config->item('REC_LIMIT_FRONT')%>
<%assign var=totalPage value= ceil($settings.count/$per_page)%>
<%assign var=tpages value= ceil($settings.count/$per_page)%>

<script>
    var maxprice = "<%$this->generalfront->getMaxPriceForFilter($cid)%>";
</script>
<div id="categoryPage"></div>
<div class="main-containt">
	<input type="hidden" name="catId" id="catId" value="<%$catId%>">
	<div class="container">
		<div class="row-fluid">
			<div class="breadcrumbs" style="padding: 0 0px;">
				<div class=""> <!-- container -->
					<div class="row-fluid">
						<%$this->general->Breadcrumb('C',"<%$cid%>","<%$this->generalfront->getColumnValue('iMstCategoriesId',$cid,'vTitle','trn_homepage_category')%>")%>
					</div>
				</div>
			</div>      
			<div class="product-list">
				<div class=""> <!-- container -->
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<div class="accordion">
								<div class="accordion-group">
									<div class="accordion-heading filter-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse" href="#category">
											<%assign var=catlist value=$this->generalfront->category_nlevel_list_by_id($catId, 0)%>
											<%if $catlist eq ''%>
											<%$this->generalfront->getColumnValue('iMstCategoriesId',$this->generalfront->getParentId($catId),'vTitle','mst_categories')%>
											<%else%>
											<%$title%>
											<%/if%>
											<!-- < %$this->generalfront->getColumnValue('iMstCategoriesId',$cid,'vTitle','trn_homepage_category')%>  -->
											<i class="fa fa-chevron-down pull-right"></i>
										</a>
									</div>
									<div id="category" class="accordion-body collapse in">
										<div id="footwear" class="accordion-inner filter-block">
											<!-- <%assign var=categorylist value=$this->generalfront->category_nlevel_list($sitemap_data)%>
											<ul>
											<%$categorylist%>
											</ul>-->
											<%assign var=categorylist value=$this->generalfront->category_nlevel_list_by_id($catId, 0)%>
											<%if $categorylist eq ''%>
											<%assign var=c2p_catKey value=$this->general->CategoryWalkup($catId)|@count - 1%>
											<%assign var=c2p_catArray value=$this->general->CategoryWalkup($catId)%>
											<%assign var=categorylist value=$this->generalfront->category_nlevel_list_by_id($c2p_catArray[$c2p_catKey-1]['iMstCategoriesId'])%>
											<%/if%>
											<ul>
											<%$categorylist%>
											</ul>
											<!-- <ul>
											<%foreach $sitemap_data  as $child_item %>    
												<li>
												<%if $child_item.children|@count gt 0%>
													<a href="#<%preg_replace("![^a-z0-9]+!i", "_",$child_item['vTitle'])%>" data-toggle="collapse" aria-expanded="true" class="<%preg_replace("![^a-z0-9]+!i", "_",$child_item['vTitle'])%>"><i class="fa fa-chevron-down"></i><%$child_item['vTitle']%> <span>(<%$this->generalfront->getProductCount($child_item.iMstCategoriesId)%>)</span></a>
												<%else%>
													<!-- <a href="<%$child_item['vCleanUrl']%>"><%$child_item['vTitle']%> <span>(<%$this->generalfront->getProductCount($child_item.iMstCategoriesId)%>)</span></a> - ->
													<a href="javascript:;" onclick="return productListingAjax('<%$child_item['vCleanUrl']%>');"><%$child_item['vTitle']%> <span>(<%$this->generalfront->getProductCount($child_item.iMstCategoriesId)%>)</span></a>
												<%/if%>
												
												<%if $child_item.children|@count gt 0%>
													<ul id="<%$child_item['vTitle']%>" class="collapse">
													<%foreach $child_item.children  as $child %>
														<li>
														<%if $child.children|@count gt 0%>
															<a href="#<%preg_replace("![^a-z0-9]+!i", "_",$child['vTitle'])%>" data-toggle="collapse" aria-expanded="true" class="<%preg_replace("![^a-z0-9]+!i", "_",$child['vTitle'])%>"><%$child['vTitle']%><%if $child.children|@count gt 0%><i class="fa fa-chevron-down"></i> <%/if%> <span>(<%$this->generalfront->getProductCount($child.iMstCategoriesId)%>)</span></a>
														<%else%>
															<!-- <a href="<%$child['vCleanUrl']%>"><%$child['vTitle']%> <span>(<%$this->generalfront->getProductCount($child.iMstCategoriesId)%>)</span></a> - ->
															<a href="javascript:;" onclick="return productListingAjax('<%$child['vCleanUrl']%>')"><%$child['vTitle']%> <span>(<%$this->generalfront->getProductCount($child.iMstCategoriesId)%>)</span></a>
														<%/if%>

															<%if $child.children|@count gt 0%>
															<ul id="<%$child['vTitle']%>" class="collapse">
															<%foreach $child.children  as $subchild %>
																<li>
																	<a href="<%$subchild['vCleanUrl']%>"><%$subchild['vTitle']%> <span>(<%$this->generalfront->getProductCount($subchild.iMstCategoriesId)%>)</span></a>
																</li>
															<%/foreach%>
															</ul>
															<%/if%>
														</li>
													<%/foreach%>
													</ul>
													<%/if%>
												</li>
											<%/foreach%>
											</ul> -->
										</div>
									</div>
								</div>
							</div>
							<div class="accordion">
								<div class="filter-heading-common">
									filter by <span class="clear-all"><a href="javascript:" id="clear_all_filter">Clear All</a></span>
								</div>
								<div class="accordion-group bt-bor">
									<div class="accordion-heading filter-sub-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse" href="#price"> Price <span></span></a>
									</div>
									<div id="price" class="accordion-body collapse">
										<div class="accordion-inner filter-block">
											<div class="price-rang">
												Range :<input type="text" id="amount" readonly >
											</div>
											<div class="range-wrap"><div id="slider-range"></div></div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
								<%assign var=filters value=$filter_data%>
								<%foreach $filters as $filter key=key1%>
									<%assign var=replaceKey value=$key1|replace:' ':'_'%>
									<%assign var=replaceKey value=$replaceKey|replace:'\'':'_'%>
									<%assign var=replaceKey value=$replaceKey|replace:'"':'_'%>
									<%assign var=replaceKey value=$replaceKey|cat:'1'%>
								<div class="accordion-group bt-bor prod-opt" data-value="<%$replaceKey%>">
									<div class="accordion-heading filter-sub-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse" href="#<%$replaceKey%>">
											<%$key1%> <span></span>
										</a>
									</div>
									<div id="<%$replaceKey%>" class="accordion-body collapse">
										<div class="accordion-inner filter-block">
											<div class="filter-search">
												<input type="text" class="input-text <%$key1%>-filter" value="" data-value="<%$key1%>" data-id="<%$replaceKey%>">
												<button type="submit" title="Search" class="button form-button"><span><span>Search</span></span></button>
											</div>
											<%if $filter|@count < 7%>
											<%assign var=style value="height:auto;"%>
											<%else%>
											<%assign var=style value="height:200px;"%>
											<%/if%>
											<div class="brand-list" style="<%$style%>">
												<ul style="width:90%;">
													<%foreach $filter as $filteritem key=key%>
													<li>
														<div class="checkbox">
															<input id="<%$replaceKey%>checkbox<%$key+1%>" type="checkbox" class="chk" name="<%$key1%>checkbox<%$key+1%>" value="<%$key1%>|<%$filteritem.value%>" onclick="return filterlistcheckbox();">
															<label for="<%$replaceKey%>checkbox<%$key+1%>"> <%$filteritem.value%><span id="tot_count">(<%$this->generalfront->getBrandCount($filteritem.value)%>)</span><span id="zero_count" style="display:none">(0)</span> </label>
														</div>
													</li>
													<%/foreach%>
													<li>
														<div class="checkbox">
															<input id="<%$replaceKey%>checkbox<%$key+2%>" type="checkbox" class="chk" name="<%$key1%>checkbox<%$key+2%>" value="<%$key1%>|other" onclick="return filterlistcheckbox();">
															<label for="<%$replaceKey%>checkbox<%$key+2%>"> Other </label>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>   
								<%/foreach%>
								<div class="accordion-group bt-bor">
									<div class="accordion-heading filter-sub-heading">
										<a class="accordion-toggle collapsed" data-toggle="collapse" href="#Free_Shipping">
											Free Shipping <span></span>
										</a>
									</div>
									<div id="Free_Shipping" class="accordion-body collapse">
										<div class="accordion-inner filter-block">
											<div id="brands" class="brand-list" style="height:auto;">
												<ul>
													<li>
														<div class="checkbox">
															<input id="freeshippingcheckbox" type="checkbox" class="chk" name="freeshippingcheckbox" value="freeshipping|freeshipping" onclick="return filterlistcheckbox();">
															<label for="freeshippingcheckbox">Free Shipping</label>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-main">
							<div id="loader" style="display:none;margin-top:125px;" align="center"><img src="<%$this->config->item('images_url')%>fancybox_loading.gif" /></div>
							<div class="short-bar">
								<div class="col-md-6" id="product-title">
									<div class="row">
										<%$list_title%>
										<!-- < %$this->generalfront->getColumnValue('iMstCategoriesId',$cid,'vTitle','trn_homepage_category')%> -->
										<span>(<%$settings.count%>)</span>
									</div>
								</div>
								<form id="sort_by_form" method="POST" action="productlist/productlist/productList">
									<div class="col-md-6">
										<div class="row">
											<div class="shortby">
												<input type="hidden" value="<%$cid%>" id="cid">
												<div class="shorten">
													<label>Sort By</label>
													<select id="sort_by" name="sort_by" onchange="return sort_by_product_MARKET();">
														<option value='1'>Best Selling</option>
														<option value='2'>New</option>
														<option value='3'>Discounts</option>
														<option value='4'>Trending</option>
													</select>
												</div>
												<div class="shorten">
													<label>Price</label>
													<select id="price_by" name="price_by" onchange="return sort_by_product_PRICE();">
														<option value="2" selected="selected">Low</option>
														<option value="1">High</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div id="catalog-listing">
							<%if $product_arr['data']|@count gt 0%>
								<div class="category-products page-product-list">
									<div class="row sort_result">                     
										<%if $product_arr['data']|@count gt 0%>
										<%assign var=products value=$product_arr['data']%>
										<%foreach $products as $product%>             
										<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">  
											<%$this->general->ProductBlockGenerator($product.mp1_mst_products_id,$product.mp1_title,$product.mp1_regular_price,$product.mp1_sale_price,$product.mp1_rating_avg,$product.mp1_rating_avg,$product.mp1_stock,$product.mp1_difference_per,$product.mp1_date,$product.mp1_wishlist_state,$product.mp1_default_img, "atc-btn-handler1")%>
										</div>
										<%/foreach%>                      
										<%/if%>                    
									</div>
								</div>
                                                        <%else%>
							<center><h1>Oops..No Record Found..!!</h1></center>
							<%/if%>    
							</div>
                                                        <div class="pagination-box">
								<div class="col-md-7">
									<%assign var=displayResult value=($settings.curr_page*$per_page gt $settings.count) ? $settings.count : $settings.curr_page*$per_page%>
									<div class="row">
										<div class="pagination-list">Items <%(($settings.curr_page-1)*$per_page)+1%> to <%$displayResult%> of <%$settings.count%> total</div>
									</div>
								</div>
								<div class="col-md-5">
									<div class="row">
										<ul class="pagination">
											<%assign var=reload value=$page|cat:"?tpages="|cat:$tpages%>            
											<%if $totalPage gt 1%>
											<!-- < %$this->generalfront->paginateWithAjax($reload,$product_arr['settings'].curr_page,$totalPage)%> -->
											<%assign var=paging value=$this->generalfront->paginateWithAjaxByFilterSearch($page, $product_arr['settings'].curr_page, $totalPage)%>
											<%$paging%>
											<%/if%>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>