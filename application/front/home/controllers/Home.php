<?php

/**
 * Description of Home Controller
 * 
 * @module Home
 * 
 * @class home.php
 * 
 * @path application\front\home\controllers\home.php
 * 
 * @author Sarvil Ajwaliya
 * 
 * @date 05.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Home extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');        
    }

    /**
     * index method is used to initialize index function.
     */
    public function index() {
        // pr($this->config->config);exit();
        $this->banner();
        $this->dealOfTheDay();
        $this->popularProducts();
        $this->getProductCategory();
    }

    /**
     * staticpage method is used to display static pages.
     */
    public function banner() {        
        $params = array();
        $ws_name = 'get_banners';
        $banner_arr = $this->hbmodel->getData($ws_name, $params);
        $banner_details = $merchant_details_arr['data'];
        $this->smarty->assign('banner_arr',$banner_arr);
        // pr($banner_arr);
        // exit;     
    }

    public function dealOfTheDay() {        
        $params = array();
        $ws_name = 'get_deal_of_day';
        $deal_arr = $this->hbmodel->getData($ws_name, $params);
        $deal_details = $merchant_details_arr['data'];
        $this->smarty->assign('deal_arr',$deal_arr);
        // pr($deal_arr);
        // exit;     
    }

    function popularProducts(){
        $params = array();
        $ws_name = 'get_popular_products';
        $popular_arr = $this->hbmodel->getData($ws_name, $params);
        $popular_details = $merchant_details_arr['data'];
        $this->smarty->assign('popular_arr',$popular_arr);
        // pr($popular_arr);
        // exit;    
    }
    
    
    function getProductCategory(){
        $params = array();
        $ws_name = 'get_product_category';
        $productcat_arr = $this->hbmodel->getData($ws_name, $params);
        $productcat_details = $merchant_details_arr['data'];        
        $this->smarty->assign('productcat_arr',$productcat_arr);
        // pr($productcat_arr);        
        // exit;    
    }

    function getRecentPrduct(){
        $postarr = $this->input->post();        
        if(!empty($postarr['pid'])){            
            if(count($postarr['pid']) > 1){
                $pids = implode(',',$postarr['pid']);        
            }else{
                $pids = $postarr['pid'][0];     
            }
            $recent_arr = $this->generalfront->getProductsBasedOnPids($pids);
            foreach ($recent_arr as $key => $product) {
                $str .= '<div class="col-md-12">';
                $str .= $this->general->ProductBlockGenerator(
                        $product['iMstProductsId'],
                        $product['vTitle'],
                        $product['fRegularPrice'],
                        $product['fSalePrice'],
                        $product['fRatingAvg'],
                        $product['fRatingAvg'],
                        $product['iStock'],
                        $product['iDifferencePer'],
                        $product['dDate'],
                        $product['iWishlistState'],
                        $product['vDefaultImg'],
                        'atc-btn-handler1');
                $str .= "</div>";
            }
            echo $str;
            exit();
        }
        
    }
}
