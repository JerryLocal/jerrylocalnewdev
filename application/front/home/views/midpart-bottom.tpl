<div class="row">
  <ul class="title-heading row top-padd">
    <li class="col-md-6" style="text-align:right;"><a href="#" class="active">Popular Products</a></li>
    <li class="col-md-6" style="text-align:left;"><a href="#">recently Reviewed</a></li>
  </ul>

  <!-- below code for the Popular Products -->
  <div class="col-md-6">
    <div id="popular-paroduct" class="owl-carousel01">          
      <%if $popular_arr['data']|@count gt '0'%>    
      <%foreach $popular_arr['data'] as $pdetails%>       
        <div class="col-md-12">      
        <%if $pdetails.mp_mst_products_id gt 0 && isset($pdetails.mp_mst_products_id)%>
          <%$this->general->ProductBlockGenerator($pdetails.mp_mst_products_id,$pdetails.mp_title,$pdetails.mp_regular_price,$pdetails.mp_sale_price,$pdetails.mp_rating_avg,$pdetails.mp_rating_avg,$pdetails.mp_stock_1,$pdetails.mp_difference_per,$pdetails.mp_date,$pdetails.mp_wishlist_state,$pdetails.mp_default_img)%>
        <%else%>
          <%$this->general->ProductBlockGenerator($pdetails.mp_mst_products_id_1,$pdetails.mp_title_1,$pdetails.mp_regular_price_1,$pdetails.mp_sale_price_1,$pdetails.mp_rating_avg_1,$pdetails.mp_rating_avg_1,$pdetails.mp_stock_1,$pdetails.mp_difference_per_1,$pdetails.mp_date_1,$pdetails.mp_wishlist_state_1,$pdetails.mp_default_img_1)%>
        <%/if%>          
        </div>      
      <%/foreach%>
      <%/if%>                              
    </div>
  </div>
  
<!-- below code for the Recently Viewed -->
  <div class="col-md-6">
    <div id="recently-view" class="owl-carousel01">          
        <%if $popular_arr['data']|@count gt '0'%>    
          <%foreach $popular_arr['data'] as $pdetails%>    
          <div class="col-md-12">      
          <%if $pdetails.mp_mst_products_id gt 0 && isset($pdetails.mp_mst_products_id)%>
            <%$this->general->ProductBlockGenerator($pdetails.mp_mst_products_id,$pdetails.mp_title,$pdetails.mp_regular_price,$pdetails.mp_sale_price,$pdetails.mp_rating_avg,$pdetails.mp_rating_avg,$pdetails.mp_stock_1,$pdetails.mp_difference_per,$pdetails.mp_date,$pdetails.mp_wishlist_state,$pdetails.mp_default_img)%>
          <%else%>
            <%$this->general->ProductBlockGenerator($pdetails.mp_mst_products_id_1,$pdetails.mp_title_1,$pdetails.mp_regular_price_1,$pdetails.mp_sale_price_1,$pdetails.mp_rating_avg_1,$pdetails.mp_rating_avg_1,$pdetails.mp_stock_1,$pdetails.mp_difference_per_1,$pdetails.mp_date_1,$pdetails.mp_wishlist_state_1,$pdetails.mp_default_img_1)%>
          <%/if%>          
          </div>     
          <%/foreach%>
        <%/if%>       
    </div>
    <div id="recentpr" class="owl-carousel01"></div>
  </div>
</div>

