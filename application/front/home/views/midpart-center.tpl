<%$this->js->add_js("wishlist.js")%> 

<%foreach $productcat_arr['data'] as $cdata key=key%>
  <%if $cdata.thc_position eq 'Position-1'%>
    <div class="woman-shoes-main">
      <div class="col-40">
        <div class="img-add"><img src="<%$cdata.thc_img%>" alt="save up" class="img-responsive"></div> <!-- < %$this->config->item('category_img')%> -->
        <div class="woman-shoes">
          <h3 class="<%($key eq '1') ? 'blue' : '' %>"><%$cdata.thc_title%></h3>              
          <ul>
            <%foreach $cdata.get_sub_cat as $subcat%>
            <li><a href="<%$subcat.url_1%>"><%$subcat.mc_title%></a></li>              
            <%/foreach%>
            <li><a href="<%$cdata.url%>" class="link">VIEW ALL</a></li>
          </ul>
        </div>
      </div> 
      <%assign var=cat_ids value=$this->generalfront->getCategoryWalkupIds($cdata.thc_mst_categories_id)%>
      <div class="col-60"> 
        <%assign var=randomproduct value=$this->general->getRandomProducts($cat_ids)%>
        <div id="owl-example<%$key+1%>" class="owl-carousel01 owl-carousel1">
          <%if $randomproduct|@count gt '0'%>    
          <%foreach $randomproduct as $deal%>    
          <!-- <div class="col-md-12"> -->
          <%$this->general->ProductBlockGenerator($deal.iMstProductsId,$deal.vTitle,$deal.fRegularPrice,$deal.fSalePrice,$deal.fRatingAvg,$deal.fRatingAvg,$deal.iStock,$deal.iDifferencePer,$deal.dDate,$deal.iWishlistState,$deal.vDefaultImg)%>
          <!-- </div> -->
          <%/foreach%>
          <%/if%>     
        </div>
      </div>
    </div>
  <%/if%>
<%/foreach%>

<%include file="midpart-advertise.tpl"%>      

<%foreach $productcat_arr['data'] as $cdata key=key%>
  <%if $cdata.thc_position eq 'Position-2'%>
    <div class="woman-shoes-main">
      <div class="col-40">
        <div class="img-add">
          <%if $cdata.thc_img neq ''%>
          <img src="<%$cdata.thc_img%>" alt="save up" class="img-responsive">
          <%else%>
          <img src="<%$this->config->item('images_url')%>no-image-cat.jpg" alt="save up" class="img-responsive">
          <%/if%>
        </div>
        <div class="woman-shoes">
          <h3 class="<%($key eq '1') ? '' : 'pink' %>"><%$cdata.thc_title%></h3>      
          <ul>
            <%foreach $cdata.get_sub_cat as $subcat%>
            <li><a href="<%$subcat.url_1%>"><%$subcat.mc_title%></a></li>              
            <%/foreach%>
            <li><a href="<%$cdata.url%>" class="link">VIEW ALL</a></li>
          </ul>
        </div>
      </div>      
      <%assign var=cat_ids value=$this->generalfront->getCategoryWalkupIds($cdata.thc_mst_categories_id)%>         
      <div class="col-60">
        <%assign var=randomproduct value=$this->general->getRandomProducts($cat_ids)%>
        <div id="owl-example<%$key+1%>" class="owl-carousel01 owl-carousel1">
          <%if $randomproduct|@count gt '0'%>    
            <%foreach $randomproduct as $deal%>                         
            <%$this->general->ProductBlockGenerator($deal.iMstProductsId,$deal.vTitle,$deal.fRegularPrice,$deal.fSalePrice,$deal.fRatingAvg,$deal.fRatingAvg,$deal.iStock,$deal.iDifferencePer,$deal.dDate,$deal.iWishlistState,$deal.vDefaultImg)%>            
            <%/foreach%>
          <%/if%>   
        </div>
      </div>
    </div>
  <%/if%>
<%/foreach%>

<%include file="midpart-bottom.tpl"%>      
