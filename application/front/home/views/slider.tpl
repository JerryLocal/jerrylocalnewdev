<div class="banner-section">
  <div class=""> <!-- container -->
    <div class="row">
      <%if $banner_arr['settings'].success eq 1%>
      <div class="col-md-12 col-lg-9">
        <%assign var=sliders value=$banner_arr['data'].get_slider%> 
        <%if $sliders|@count gt '0'%>         
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">           
          <ol class="carousel-indicators"> <!-- < %$sliders|@pr%> -->
          <%foreach $sliders as $slide key=key%>
            <!-- Indicators -->
              <%if $key eq '0'%>
                <li data-target="#carousel-example-generic" data-slide-to="<%$key%>" class="active"></li>
              <%else%>
                <li data-target="#carousel-example-generic" data-slide-to="<%$key%>"></li>
              <%/if%>
          <%/foreach%>
          </ol>
          <%assign var=noimg value=$this->config->item('images_url')|cat:'no-image.jpg'%>
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
          <%foreach $sliders as $slide key=key%>
            <%if $key eq '0'%>  
            <div class="item active"><a href="http://<%$slide.mb_link%>"> <img src="<%if $slide.mb_img eq ''%><%$noimg%><%else%><%$slide.mb_img%><%/if%>" alt="banner"></a>
              <div class="carousel-caption"> ... </div>
            </div>
            <%else%>
            <div class="item"> <a href="http://<%$slide.mb_link%>"> <img src="<%if $slide.mb_img eq ''%><%$noimg%><%else%><%$slide.mb_img%><%/if%>" alt="banner"></a>
              <div class="carousel-caption"> ... </div>
            </div>            
            <%/if%>
          <%/foreach%>
          </div>
          <!-- Controls --> 
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> 
            <span class="fa fa-angle-left fa-2x" aria-hidden="true"></span> 
            <span class="sr-only">Previous</span> 
          </a> 
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> 
            <span class="fa fa-angle-right fa-2x" aria-hidden="true"></span> 
            <span class="sr-only">Next</span> 
          </a> 
        </div>
        <%/if%>
      </div>
      <%/if%>
      <%include file="right_box.tpl"%>      
    </div>
  </div>
</div>
<div class="container">
    <%include file="deal_of_the_day.tpl"%>      
</div>
