<?php

/**
 * Description of News Controller
 * 
 * @module Content
 * 
 * @class News.php
 * 
 * @path application\front\news\controllers\news.php
 * 
 * @author Jaydeep Jagani
 * 
 * @date 07.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class News extends HB_Controller {

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');
    }

    /**
     * index method is used to initialize index function.
     */
    public function index() {
        $getarr = $this->input->get();
        $curr_page = $getarr['page'];
        $params = array();
        $params['page_index'] = $curr_page;
        $ws_name = 'get_news';
        $news_arr = $this->hbmodel->getData($ws_name, $params);
        $news_detail = $merchant_details_arr['data'];
        $this->smarty->assign('news_arr',$news_arr);
    }

    function newsdetail($id){
        $params = array();
        $params['iNewsId'] = $id;
        $ws_name = 'get_news_detail';
        $news_detail_arr = $this->hbmodel->getData($ws_name, $params);
        // pr($news_detail_arr);exit();
        $news_detail = $merchant_details_arr['data'];
        $this->smarty->assign('news_detail_arr',$news_detail_arr['data']);
         
    }
    function filterNews(){
       $params = array();
       if($_POST){
            $params['dStartDate'] =$_POST['start'];
            $params['dEndDate'] = $_POST['end'];
        } 
        $curr_page = $getarr['page'];
        $params['page_index'] = $curr_page;
        $ws_name = 'get_news_by_date';
        $news_arr = $this->hbmodel->getData($ws_name, $params);
        $news_detail = $merchant_details_arr['data'];
        $this->smarty->assign('news_arr',$news_arr);
        $this->loadview('index');
    }
}

