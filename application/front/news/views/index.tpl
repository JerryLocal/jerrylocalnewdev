<%$this->js->add_js("jquery-ui-1.9.2.min.js","news.js")%>
<%assign var=settings value=$news_arr['settings']%>
<!-- < %assign var=per_page value=$news_arr['settings']['per_page']%> -->
<%assign var=per_page value=$this->config->item('REC_LIMIT_FRONT')%>
<%assign var=totalPage value= ceil($settings.count/$per_page)%>
<%assign var=tpages value= ceil($settings.count/$per_page)%>

<div class="main-containt">
  <div class="container">
    <div class="row-fluid">
     <%$this->general->Breadcrumb('CLEAN','News')%>
   </div>
 </div>
 <div class="container">
  <div class="delivery-box">
   <h3><div class="row"><div class="col-sm-12">News</div></div></h3>
   <div class="form-add">
    <div class="date-range-news">
     <form class="form-inline" method="POST" action="<%$this->config->item('site_url')%>news/news/filterNews" >
      <div class="form-group">
        <label for="exampleInputName2">Filter &nbsp;&nbsp;</label>
        <input type="text" id="datepicker_start" value=""  name="start" class="form-control" required>
      </div>
      <div class="form-group">
        <label for="exampleInputName2">&nbsp;&nbsp; To &nbsp;&nbsp;</label>
        <input type="text" id="datepicker_end" value="" name="end" class="form-control" required>
      </div>
      <input class="btn" type="submit" name="GO" value="GO" />
      <input class="btn" type="button" name="RESET" value="RESET" id="RESET" />
    </form>
  </div>
  <%assign var=settings value=$news_arr['settings']%>
  <%assign var=totalPage value= ceil($settings.count/$settings.per_page)%>
  <%assign var=tpages value= ceil($settings.count/$settings.per_page)%>
  <%if $news_arr['settings'].count gt 0%>
  <%foreach $news_arr['data'] as $news%>
  <div class="news-des">
   <h3><a href="news-detail-<%$news['mn_mst_news_id']%>.html"><i class="fa fa-newspaper-o"></i>&nbsp; <%$news['mn_title']%></a> <span class="pull-right"><%$news['mn_date']%></span></h3>
   <p><%$news['mn_description']|truncate:250%></p>
 </div>
 <%/foreach%>  
 <%else%>
 <h1 class="TAC" style="border-bottom:none">There is no News Available right now...!!</h1>
 <%/if%>      
</div>
</div>
<%if $news_arr['settings'].count gt 0%>
<div class="col-sm-12">
  <div class="row">
    <div class="pagination-box">
     <div class="col-md-7">
       <%assign var=displayResult value=($settings.curr_page*$per_page gt $settings.count) ? $settings.count : $settings.curr_page*$per_page%>
       <div class="row">
         <div class="pagination-list">Items <%(($settings.curr_page-1)*$per_page)+1%> to <%$displayResult%> of <%$settings.count%> total</div>
       </div>
     </div>
     <div class="col-md-5">
       <div class="row">
         <ul class="pagination">
           <%assign var=reload value=$this->config->item('site_url')|cat:"news.html?tpages="|cat:$tpages%>            
           <%if $totalPage gt 1%>
           <%$this->generalfront->paginate($reload,$news_arr['settings'].curr_page,$totalPage)%>
           <%/if%>
         </ul>
       </div>
     </div>
   </div>
 </div>
</div>
<%/if%>      
</div>
</div>