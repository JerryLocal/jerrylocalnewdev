<?php

/**
 * Description of NS Engine Model
 * 
 * @module NS Engine
 * 
 * @class notify.php
 * 
 * @path application\front\nsengine\models\notify.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
class Notify extends CI_Model {

    public $_image_func_config_arr = array();

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
    }

    /**
     * loadEventDetails method is used to get specific notification details or settings.
     * 
     * @param string $function_name function_name is spcific webservice.
     * 
     * @return array $event_details returns events settings array.
     */
    function loadEventDetails($function_name = '') {
        //check and load all the available data of the function
        $event_details = array();
        $event_master_obj = $this->db->get_where('sys_event_master', 'vEventFunction = "' . $function_name . '"');
        $event_master = is_object($event_master_obj) ? $event_master_obj->result_array() : array();
        if ($event_master && count($event_master) > 0) {
            $event_flow_obj = $this->db->get_where('sys_event_flow', 'iEventMasterId = "' . $event_master[0]['iEventMasterId'] . '"');
            $event_params_obj = $this->db->get_where('sys_event_params', 'iEventMasterId = "' . $event_master[0]['iEventMasterId'] . '"');

            $event_details['event_master'] = $event_master[0];
            $event_details['event_flow'] = is_object($event_flow_obj) ? $event_flow_obj->result_array() : array();
            $event_details['event_params'] = is_object($event_params_obj) ? $event_params_obj->result_array() : array();
            return $event_details;
        } else {
            return $event_details;
        }
    }

    /**
     * eventMasterData method is used to get data from sys_event_master.
     * 
     * @param string $extracond extracond is the query condition for getting filtered data.
     * 
     * @param string $field fields are either array or string.
     * 
     * @param string $order_by order_by is to append order by condition.
     * 
     * @param string $group_by group_by is to append group by condition.
     * 
     * @param string $limit limit is to append limit condition.
     * 
     * @return array $list_data array of sys_event_master data will be return.
     */
    function eventMasterData($extracond = "", $field = "", $order_by = "", $group_by = "", $limit = "") {
        $field = ($field == "") ? "sem.*" : $field;

        $this->db->select($field, false);
        $this->db->from("sys_event_master AS sem");

        if (is_array($extracond) && count($extracond) > 0) {
            foreach ($extracond as $key => $val) {
                $this->db->where($val['field'], $val['value']);
            }
        } elseif ($extracond != "") {
            if (intval($extracond)) {
                $this->db->where("sem.iEventMasterId", $extracond);
            } else {
                $this->db->where($extracond, false, false);
            }
        }
        if ($group_by != "") {
            $this->db->group_by($group_by);
        }
        if ($order_by != "") {
            $this->db->order_by($order_by);
        }
        if ($limit != "") {
            list($offset, $limit) = @explode(",", $limit);
            $this->db->limit($offset, $limit);
        }
        $list_obj = $this->db->get();
        $list_data = is_object($list_obj) ? $list_obj->result_array() : array();
        return $list_data;
    }

    /**
     * execute_query_by_settings method is used to execute queries according to the query type.
     * 
     * @param array $query_settigs array of query settings.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @param type $settings_params to store the output setting variables.
     * 
     * @return array $return_arr after query execution array will be return.
     */
    function execute_query_by_settings($query_settigs = array(), $input_params = array(), &$settings_params = array()) {
        extract($query_settigs);
        $query_type = $query['query_type'];
        $main_type = $query['main_type'];
        $return_arr = array();
        $this->_image_func_config_arr = array();
        if ($main_type == "Custom") {
            $return_arr = $this->execute_custom_query($query_settigs, $input_params, $settings_params);
        } else {
            $query_type = $query['query_type'];
            switch ($query_type) {
                case 'Select':
                    $return_arr = $this->execute_select_query($query_settigs, $input_params, $settings_params);
                    break;
                case 'Insert':
                    $return_arr = $this->execute_insert_query($query_settigs, $input_params);
                    break;
                case 'Update':
                    $return_arr = $this->execute_update_query($query_settigs, $input_params);
                    break;
                case 'Delete':
                    $return_arr = $this->execute_delete_query($query_settigs, $input_params);
                    break;
            }
        }
        $this->db->_reset_all();
        return $return_arr;
    }

    /**
     * execute_custom_query method is used to execute custom queries.
     * 
     * @param array $query_settigs array of query settings.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @param type $settings_params to store the output setting variables.
     * 
     * @return array $return_arr after query execution array will be return.
     */
    function execute_custom_query($query_settigs = array(), $input_params = array(), &$settings_params = array()) {
        extract($query_settigs);
        $return_arr = array();
        try {
            $query_type = $query['query_type'];
            $custom_query = $query['custom_query'];
            $custom_query = $this->processRequestPregMatch($custom_query, $input_params);
            $custom_query = $this->processServerPregMatch($custom_query, $input_params);
            $custom_query = $this->processSystemPregMatch($custom_query, $input_params);
            $custom_fields = $query_fields['custom_field'];
            switch ($query_type) {
                case "Select" :

                    $rec_limit = trim($query['rec_limit']);
                    $rec_limit = $this->processRequestPregMatch($rec_limit, $input_params);
                    $rec_limit = $this->processSystemPregMatch($rec_limit, $input_params);
                    $rec_limit = intval($rec_limit);

                    $max_rec = trim($query['max_rec']);
                    $max_rec = $this->processRequestPregMatch($max_rec, $input_params);
                    $max_rec = $this->processSystemPregMatch($max_rec, $input_params);
                    $max_rec = intval($max_rec);

                    if ($query['paging'] == 'Yes' && $rec_limit > 0) {
                        $record_limit = $rec_limit;
                        $sql_count = $custom_query;

                        $db_count_obj = $this->db->query($sql_count);
                        $db_count = is_object($db_count_obj) ? $db_count_obj->result_array() : array();
                        $total_records = count($db_count);
                        $settings_params['count'] = $total_records;
                        if ($max_rec > 0) {
                            $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;
                            $total_pages = getTotalPages($total_records, $record_limit);
                            $start_index = getStartIndex($total_records, $current_page, $record_limit);
                            $tot_count = $start_index + $record_limit;
                            if (($start_index >= $max_rec) || ($start_index >= $total_records)) {
                                throw new Exception('No records found');
                            }
                            $record_limit = ($tot_count > $max_rec) ? $max_rec - $start_index : $record_limit;
                            $settings_params['curr_page'] = $current_page;
                            $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
                            if ($total_records > $tot_count) {
                                $settings_params['next_page'] = ($tot_count > $max_rec) ? 0 : 1;
                            } else {
                                $settings_params['next_page'] = '0';
                            }
                            $page_limit_cond = "LIMIT " . intval($start_index) . ", " . intval($record_limit);
                        } else {
                            $record_limit = $rec_limit;
                            $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;
                            $total_pages = getTotalPages($total_records, $record_limit);
                            $start_index = getStartIndex($total_records, $current_page, $record_limit);
                            $settings_params['curr_page'] = $current_page;
                            $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
                            $settings_params['next_page'] = ($current_page + 1 > $total_pages) ? 0 : 1;
                            $page_limit_cond = "LIMIT " . intval($start_index) . ", " . intval($record_limit);
                        }
                        $limit_custom_query = $custom_query . " " . $page_limit_cond;
                        $sql_query = $limit_custom_query;

                        $result_obj = $this->db->query($sql_query);
                        $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();

                        if (!is_array($result_arr) || count($result_arr) == 0) {
                            throw new Exception('No records found');
                        }
                    } else {
                        $custom_query = ($max_rec > 0) ? $custom_query . ' LIMIT ' . $max_rec : $custom_query;
                        $sql_query = $custom_query;

                        $result_obj = $this->db->query($sql_query);
                        $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
                        if (!is_array($result_arr) || count($result_arr) == 0) {
                            throw new Exception('No records found');
                        }
                    }
                    if (is_array($custom_fields) && count($custom_fields) > 0) {
                        foreach ((array) $custom_fields as $key => $val) {
                            $this->assignImageAndPHPFunctions($val);
                        }
                        $result_arr = $this->processSelectRecords($result_arr);
                    }
                    break;
                case "Insert" :
                    $sql_query = $custom_query;
                    $this->db->query($sql_query);
                    $insert_id = $this->db->insert_id();
                    if (!$insert_id) {
                        throw new Exception("Failure in insertion");
                    }
                    $result_param = $result_field_param;
                    $key_code = ($result_param != "") ? $result_param : "insert_id";
                    $result_arr[$key_code] = $insert_id;
                    break;
                case "Update" :
                    $sql_query = $custom_query;
                    $this->db->query($sql_query);
                    if (!$res) {
                        throw new Exception("Failure in updation");
                    }
                    $result_arr = $res;
                    break;
                case "Delete" :
                    $sql_query = $custom_query;
                    $this->db->query($sql_query);
                    if (!$res) {
                        throw new Exception("Failure in deletion");
                    }
                    $result_arr = $res;
                    break;
            }
            $return_arr['data'] = $result_arr;
            $return_arr['success'] = 1;
        } catch (Exception $exc) {
            $return_arr['data'] = array();
            $return_arr['success'] = 0;
            $return_arr['message'] = $exc->getMessage();
        }
        return $return_arr;
    }

    /**
     * execute_select_query method is used to execute select queries.
     * 
     * @param array $query_settigs array of query settings.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @param type $settings_params to store the output setting variables.
     * 
     * @return array $return_arr after query execution array will be return.
     */
    function execute_select_query($query_settigs = array(), $input_params = array(), &$settings_params = array()) {
        extract($query_settigs);
        $return_arr = array();
        try {
            $select_fields = $query_fields['select_field'];
            if (!is_array($select_fields) || count($select_fields) == 0) {
                throw new Exception('No fields found');
            }
            $select_arr = array();
            $this->db->start_cache();
            foreach ($select_fields as $key => $val) {
                $display_in_query = $val['display_in_query'];
                if (strstr($display_in_query, $this->requestVariable())) {
                    $display_in_query = $this->processRequestPregMatch($display_in_query, $input_params);
                }
                if (strstr($display_in_query, $this->serverVariable())) {
                    $display_in_query = $this->processServerPregMatch($display_in_query, $input_params);
                }
                if (strstr($display_in_query, $this->systemVariable())) {
                    $display_in_query = $this->processSystemPregMatch($display_in_query, $input_params);
                }
                if (strtolower($val['select_type']) == "custom") {
                    if (strtolower($val['custom_type']) == "text") {
                        $display_in_query = "'" . $display_in_query . "'";
                    }
                }
                $select_arr[] = "(" . $display_in_query . ") AS " . $val['field_code'];
                $this->assignImageAndPHPFunctions($val);
            }
            #$this->db->_protect_identifiers = false;
            $this->db->select($select_arr, false);

            $rec_limit = trim($query['rec_limit']);
            $rec_limit = $this->processRequestPregMatch($rec_limit, $input_params);
            $rec_limit = $this->processSystemPregMatch($rec_limit, $input_params);
            $rec_limit = intval($rec_limit);

            $max_rec = trim($query['max_rec']);
            $max_rec = $this->processRequestPregMatch($max_rec, $input_params);
            $max_rec = $this->processSystemPregMatch($max_rec, $input_params);
            $max_rec = intval($max_rec);

            if ($query['paging'] == 'Yes' && $rec_limit > 0) {
                $this->addOtherQuerySettings($query_settigs, $input_params);
                $this->db->stop_cache();

                $having_exists = ((is_array($query_fields['having_field']) && count($query_fields['having_field']) > 0) || $query['extra_having_condition'] != "") ? true : false;
                $grouping_exists = ((is_array($query_fields['group_field']) && count($query_fields['group_field']) > 0) || $query['extra_group_by_condition'] != "") ? true : false;
                if ($having_exists || $grouping_exists) {
                    $paging_data = $this->db->get();
                    $total_records = is_object($paging_data) ? $paging_data->num_rows() : 0;
                } else {
                    $total_records = $this->db->count_all_results();
                }
                $settings_params['count'] = $total_records;
                if ($max_rec > 0) {
                    $record_limit = $rec_limit;
                    $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;
                    $total_pages = getTotalPages($total_records, $record_limit);
                    $start_index = getStartIndex($total_records, $current_page, $record_limit);
                    $tot_count = $start_index + $record_limit;
                    if (($start_index >= $max_rec) || ($start_index >= $total_records)) {
                        throw new Exception('No records found');
                    }
                    $record_limit = ($tot_count > $max_rec) ? $max_rec - $start_index : $record_limit;
                    $settings_params['curr_page'] = $current_page;
                    $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
                    if ($total_records > $tot_count) {
                        $settings_params['next_page'] = ($tot_count > $max_rec) ? 0 : 1;
                    } else {
                        $settings_params['next_page'] = '0';
                    }
                } else {
                    $record_limit = $rec_limit;
                    $current_page = intval($input_params['page_index']) > 0 ? intval($input_params['page_index']) : 1;
                    $total_pages = getTotalPages($total_records, $record_limit);
                    $start_index = getStartIndex($total_records, $current_page, $record_limit);
                    $settings_params['curr_page'] = $current_page;
                    $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
                    $settings_params['next_page'] = ($current_page + 1 > $total_pages) ? 0 : 1;
                }
                $is_paging = true;
            } else {
                $this->addOtherQuerySettings($query_settigs, $input_params);
                $this->db->stop_cache();
                $is_paging = false;
            }

            //add order by
            if ((is_array($query_fields['order_field']) && count($query_fields['order_field']) > 0) || $query['extra_order_by_condition'] != "") {
                $this->AddOrder($query_fields['order_field'], $input_params, $query['extra_order_by_condition']);
            }
            if ($is_paging == true) {
                $this->db->limit($record_limit, $start_index);
            } else if ($max_rec > 0) {
                $this->db->limit($max_rec);
            }
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            $this->db->flush_cache();
            #$this->db->_protect_identifiers = true;

            if (!is_array($result_arr) || count($result_arr) == 0) {
                throw new Exception('No records found');
            }
            $result_arr = $this->processSelectRecords($result_arr);
            $return_arr['data'] = $result_arr;
            $return_arr['success'] = 1;
        } catch (Exception $exc) {
            $return_arr['data'] = array();
            $return_arr['success'] = 0;
            $return_arr['message'] = $exc->getMessage();
        }
        return $return_arr;
    }

    /**
     * execute_insert_query method is used to execute insert queries.
     * 
     * @param array $query_settigs array of query settings.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @return array $return_arr after query execution this array will be return.
     */
    function execute_insert_query($query_settigs = array(), $input_params = array()) {
        extract($query_settigs);
        $result_arr = array();
        try {
            $insert_fields = $query_fields['insert_field'];
            $table_name = $query_tables['table_name'];
            $insert_assoc_arr = $this->getInsertUpdateFields($insert_fields, $input_params);
            $insert_arr = $insert_assoc_arr['result'];
            if (!is_array($insert_arr) || count($insert_arr) == 0) {
                $error_msg = "Error in insertion";
                throw new Exception($error_msg);
            }
            $is_setted = false;
            foreach ($insert_arr as $field_name => $field_val) {
                $field_code = $field_val['field_code'];
                if ($field_val['param_3'] == "false") {
                    $this->db->set($field_name, $field_val['param_2'], false);
                    $is_setted = true;
                } else if ($field_val['param_4'] == "isset") {
                    if ($field_val['param_5'] == "file") {
                        $this->db->set($field_name, "");
                        $is_setted = true;
                    } else if (isset($input_params[$field_code])) {
                        $this->db->set($field_name, $field_val['param_2']);
                        $is_setted = true;
                        $input_params[$field_code] = $field_val['field_val'];
                    }
                } else {
                    $this->db->set($field_name, $field_val['param_2']);
                    $is_setted = true;
                    $input_params[$field_code] = $field_val['field_val'];
                }
            }
            if (!$is_setted) {
                $error_msg = "Error in insertion";
                throw new Exception($error_msg);
            }
            $this->db->insert($table_name);
            $insert_id = $this->db->insert_id();
            if (!$insert_id) {
                throw new Exception("Failure in insertion");
            }

            $result_param = $query['result_field_param'];
            $pr_key = $this->general->getTablePrimaryKey($table_name);
            $key_code = ($result_param != "") ? $result_param : $this->general->from_camel_case($pr_key);
            $result_arr[$key_code] = $insert_id;

            $return_arr['data'] = $result_arr;
            $return_arr['success'] = 1;
        } catch (Exception $exc) {
            $return_arr['data'] = array();
            $return_arr['success'] = 0;
            $return_arr['message'] = $exc->getMessage();
        }
        return $return_arr;
    }

    /**
     * execute_update_query method is used to execute update queries.
     * 
     * @param array $query_settigs array of query settings.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @return array $return_arr after query execution this array will be return.
     */
    function execute_update_query($query_settigs = array(), $input_params = array()) {
        extract($query_settigs);
        $return_arr = array();
        try {
            $update_fields = $query_fields['update_field'];
            $where_fields = $query_fields['where_field'];
            $table_name = $query_tables['table_name'];
            $update_assoc_arr = $this->getInsertUpdateFields($update_fields, $input_params);
            $update_arr = $update_assoc_arr['result'];
            if (!is_array($update_arr) || count($update_arr) == 0) {
                $error_msg = "Error in updation";
                throw new Exception($error_msg);
            }
            $is_setted = false;
            foreach ($update_arr as $field_name => $field_val) {
                $field_code = $field_val['field_code'];
                if ($field_val['param_3'] == "false") {
                    $this->db->set($field_name, $field_val['param_2'], false);
                    $is_setted = true;
                } else if ($field_val['param_4'] == "isset") {
                    if (isset($input_params[$field_code])) {
                        $this->db->set($field_name, $field_val['param_2']);
                        $is_setted = true;
                        $input_params[$field_code] = $field_val['field_val'];
                    }
                } else {
                    $this->db->set($field_name, $field_val['param_2']);
                    $is_setted = true;
                    $input_params[$field_code] = $field_val['field_val'];
                }
            }
            if (!$is_setted) {
                $error_msg = "Error in updation";
                throw new Exception($error_msg);
            }

            //add where condition
            if ((is_array($where_fields) && count($where_fields) > 0) || $query['extra_condition'] != "") {
                $this->addConditions($input_params, $where_fields, $query['extra_condition'], "AR", true);
            }
            $res = $this->db->update($table_name);

            if (!$res) {
                throw new Exception("Failure in updation");
            }
            $return_arr['data'] = $res;
            $return_arr['success'] = 1;
        } catch (Exception $exc) {
            $return_arr['data'] = array();
            $return_arr['success'] = 0;
            $return_arr['message'] = $exc->getMessage();
        }
        return $return_arr;
    }

    /**
     * execute_delete_query method is used to execute delete queries.
     * 
     * @param array $query_settigs array of query settings.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @return array $return_arr after query execution this array will be return.
     */
    function execute_delete_query($query_settigs = array(), $input_params = array()) {
        extract($query_settigs);
        $return_arr = array();
        try {
            $where_fields = $query_fields['where_field'];
            $table_name = $query_tables['table_name'];
            $table_alias = $query_tables['table_alias'];
            $relation_tables = $query_tables['relation_tables'];
            $join_tables = $this->addRelationTables($input_params, $relation_tables, "NR");
            if (is_array($join_tables) && count($join_tables) > 0) {
                $alias_keys = @implode(",", $join_tables['alias']);
                $join_tbls = @implode(" ", $join_tables['join']);

                if ($this->config->item('PHYSICAL_RECORD_DELETE') && $query['physical_remove'] == "No") {
                    if ((is_array($where_fields) && count($where_fields) > 0) || $query['extra_condition'] != "") {
                        $this->addConditions($input_params, $where_fields, $query['extra_condition'], "AR");
                    }
                    $data = $this->general->getPhysicalRecordUpdate($table_alias);
                    if (trim($join_tbls) != '') {
                        $res = $this->db->update($table_name . " AS " . $table_alias . " " . $join_tbls, $data);
                    } else {
                        $res = $this->db->update($table_name . " AS " . $table_alias, $data);
                    }
                } else {
                    $where_cond = $this->addConditions($input_params, $where_fields, $query['extra_condition'], "NR");
                    $sql_query = "DELETE " . $table_alias . ".*," . $alias_keys . " FROM " . $table_name . " AS " . $table_alias . "  " . $join_tbls . " WHERE 1 " . $where_cond;
                    $res = $this->db->query($sql_query);
                }
                if (!$this->db->affected_rows()) {
                    throw new Exception("Failure in deletion");
                }
            } else {
                //add where condition
                if ((is_array($where_fields) && count($where_fields) > 0) || $query['extra_condition'] != "") {
                    $this->addConditions($input_params, $where_fields, $query['extra_condition'], "AR", true);
                }
                if ($this->config->item('PHYSICAL_RECORD_DELETE') && $query['physical_remove'] == "No") {
                    $data = $this->general->getPhysicalRecordUpdate();
                    $res = $this->db->update($table_name, $data);
                } else {
                    $res = $this->db->delete($table_name);
                }

                if (!$this->db->affected_rows()) {
                    throw new Exception("Failure in deletion");
                }
            }
            $return_arr['data'] = $res;
            $return_arr['success'] = 1;
        } catch (Exception $exc) {
            $return_arr['data'] = array();
            $return_arr['success'] = 0;
            $return_arr['message'] = $exc->getMessage();
        }
        return $return_arr;
    }

    /**
     * send_mail_notification method is used to send notications via email.
     * 
     * @param array $query_settigs array of query settings.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @return array $return_arr array of success and message will be return.
     */
    function send_mail_notification($query_settigs = array(), $input_params = array()) {
        $email = $query_settigs['email'];
        try {
            $email_arr = array();
            if ($email['to_email']['type'] == "param") {
                $to_email = $input_params[$email['to_email']['value']];
            } else {
                $to_email = $email['to_email']['value'];
                $to_email = $this->processRequestPregMatch($to_email, $input_params);
                $to_email = $this->processServerPregMatch($to_email, $input_params);
                $to_email = $this->processSystemPregMatch($to_email, $input_params);
            }

            if ($email['cc_email']['type'] == "param") {
                $cc_email = $input_params[$email['cc_email']['value']];
            } else {
                $cc_email = $email['cc_email']['value'];
                $cc_email = $this->processRequestPregMatch($cc_email, $input_params);
                $cc_email = $this->processServerPregMatch($cc_email, $input_params);
                $cc_email = $this->processSystemPregMatch($cc_email, $input_params);
            }
            if ($email['bcc_email']['type'] == "param") {
                $bcc_email = $input_params[$email['bcc_email']['value']];
            } else {
                $bcc_email = $email['bcc_email']['value'];
                $bcc_email = $this->processRequestPregMatch($bcc_email, $input_params);
                $bcc_email = $this->processServerPregMatch($bcc_email, $input_params);
                $bcc_email = $this->processSystemPregMatch($bcc_email, $input_params);
            }

            if ($email['from_name']['type'] == "param") {
                $from_name = $input_params[$email['from_name']['value']];
            } else {
                $from_name = $email['from_name']['value'];
                $from_name = $this->processRequestPregMatch($from_name, $input_params);
                $from_name = $this->processServerPregMatch($from_name, $input_params);
                $from_name = $this->processSystemPregMatch($from_name, $input_params);
            }

            if ($email['from_email']['type'] == "param") {
                $from_email = $input_params[$email['from_email']['value']];
            } else {
                $from_email = $email['from_email']['value'];
                $from_email = $this->processRequestPregMatch($from_email, $input_params);
                $from_email = $this->processServerPregMatch($from_email, $input_params);
                $from_email = $this->processSystemPregMatch($from_email, $input_params);
            }
            $email_subject = $email['email_subject'];
            $email_subject = $this->processRequestPregMatch($email_subject, $input_params);
            $email_subject = $this->processServerPregMatch($email_subject, $input_params);
            $email_subject = $this->processSystemPregMatch($email_subject, $input_params);

            $email_code = $email['email_code'];

            $email_arr["vEmail"] = $to_email;
            $email_arr["vFromEmail"] = $from_email;
            $email_arr["vFromName"] = $from_name;
            $email_arr["vCCEmail"] = $cc_email;
            $email_arr["vCCName"] = $cc_email;
            $email_arr["vBCCEmail"] = $bcc_email;
            $email_arr["vBCCName"] = $bcc_email;
            $email_arr["vSubject"] = $email_subject;

            if ($email_code == "") {
                $email_subject = $this->processRequestPregMatch($email_subject, $input_params);
                $email_subject = $this->processServerPregMatch($email_subject, $input_params);
                $email_subject = $this->processSystemPregMatch($email_subject, $input_params);

                $email_content = $email['email_content'];
                $email_content = $this->processRequestPregMatch($email_content, $input_params);
                $email_content = $this->processServerPregMatch($email_content, $input_params);
                $email_content = $this->processSystemPregMatch($email_content, $input_params);
                $email_content = $this->general->getReplacedInputParams($email_content, $input_params);

                $success = $this->general->CISendMail($to_email, $email_subject, $email_content, $from_email, $from_name, $cc_email, $bcc_email);
            } else {
                $email_vars = (is_array($email['email_vars'])) ? $email['email_vars'] : array();
                for ($i = 0; $i < count($email_vars); $i++) {
                    if ($email_vars[$i]['type'] == "param") {
                        $param = $input_params[$email_vars[$i]['value']];
                    } else {
                        $param = $email_vars[$i]['value'];
                        $param = $this->processRequestPregMatch($param, $input_params);
                        $param = $this->processServerPregMatch($param, $input_params);
                        $param = $this->processSystemPregMatch($param, $input_params);
                    }
                    $code = trim($email_vars[$i]['code'], "#");
                    $email_arr[$code] = $param;
                }
                $success = $this->general->sendMail($email_arr, $email_code);
            }
            
            $insert_arr['eEntityType'] = 'General';
            $insert_arr['vReceiver'] = $to_email;
            $insert_arr['eNotificationType'] = "EmailNotify";
            $insert_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $insert_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success) {
                $insert_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $insert_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $insert_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($insert_arr);

            if (!$success) {
                throw new Exception('Failure in sending mail');
            }
            $success = 1;
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * send_push_notification method is used to sending push notifications to the mobile.
     * 
     * @param array $query_settigs array of query settings.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @return array $return_arr array of success and message will be return.
     */
    function send_push_notification($query_settigs = array(), $input_params = array()) {
        $push_arr = $query_settigs['push'];
        $device_token_arr = $push_arr['device_token'];
        $device_type_arr = $push_arr['device_type'];
        $token_mode = $push_arr['mode'];
        $code = $push_arr['code'];
        $sound = $push_arr['sound'];
        $badge_arr = $push_arr['badge'];
        $title = $push_arr['title'];
        $push_msg = $push_arr['message'];
        $variables = $push_arr['variables'];

        $type_1 = $device_token_arr['type'];
        $value_1 = $device_token_arr['value'];

        $type_2 = $device_type_arr['type'];
        $value_2 = $device_type_arr['value'];

        $type_3 = $badge_arr['type'];
        $value_3 = $badge_arr['value'];

        try {
            if ($type_1 == 'param') {
                $device_id = $input_params[$value_1];
            } else {
                $value_1 = $this->processRequestPregMatch($value_1, $input_params);
                $value_1 = $this->processServerPregMatch($value_1, $input_params);
                $value_1 = $this->processSystemPregMatch($value_1, $input_params);
                $device_id = $value_1;
            }

            if ($type_1 == 'param') {
                $device_type = $input_params[$value_2];
            } else {
                $value_2 = $this->processRequestPregMatch($value_2, $input_params);
                $value_2 = $this->processServerPregMatch($value_2, $input_params);
                $value_2 = $this->processSystemPregMatch($value_2, $input_params);
                $device_type = $value_2;
            }

            if (is_array($badge_arr)) {
                if ($type_3 == 'param') {
                    $badge = $input_params[$value_3];
                } else {
                    $value_3 = $this->processRequestPregMatch($value_3, $input_params);
                    $value_3 = $this->processServerPregMatch($value_3, $input_params);
                    $value_3 = $this->processSystemPregMatch($value_3, $input_params);
                    $badge = $value_3;
                }
            } else {
                $badge = $badge_arr;
            }

            $push_msg = $this->processRequestPregMatch($push_msg, $input_params);
            $push_msg = $this->processServerPregMatch($push_msg, $input_params);
            $push_msg = $this->processSystemPregMatch($push_msg, $input_params);

            $title = $this->processRequestPregMatch($title, $input_params);
            $title = $this->processServerPregMatch($title, $input_params);
            $title = $this->processSystemPregMatch($title, $input_params);

            $send_vars = array();
            if (is_array($variables) && count($variables) > 0) {
                foreach ($variables as $key => $val) {
                    $key_3 = $val['key'];
                    $type_3 = $val['type'];
                    $value_3 = $val['value'];
                    if ($key_3 != "") {
                        if ($type_3 == 'param') {
                            $final_val = $input_params[$value_3];
                        } else {
                            $value_3 = $this->processRequestPregMatch($value_3, $input_params);
                            $value_3 = $this->processServerPregMatch($value_3, $input_params);
                            $value_3 = $this->processSystemPregMatch($value_3, $input_params);
                            $final_val = $value_3;
                        }
                        $tmp_arr = array();
                        $tmp_arr['key'] = $key_3;
                        $tmp_arr['value'] = $final_val;
                        $tmp_arr['send'] = $val['send'];
                        $send_vars[] = $tmp_arr;
                    }
                }
            }

            $push_msg = $this->general->getReplacedInputParams($push_msg, $input_params);

            $send_arr = array();
            $send_arr['device_id'] = $device_id;
            $send_arr['mode'] = $token_mode;
            $send_arr['code'] = $code;
            $send_arr['sound'] = $sound;
            $send_arr['badge'] = intval($badge);
            $send_arr['title'] = $title;
            $send_arr['message'] = $push_msg;
            $send_arr['variables'] = json_encode($send_vars);
            $send_arr['device_type'] = $device_type;
            $uni_id = $this->general->insertPushNotification($send_arr);

            if (!$uni_id) {
                throw new Exception('Failure in insertion of push notification batch entry');
            }

            $success = 1;
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * send_sms_notification method is used to sending notifications via sms.
     * 
     * @param array $query_settigs array of query settings.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @return array $return_arr array of success and message will be return.
     */
    function send_sms_notification($query_settigs = array(), $input_params = array()) {
        $smsArr = $query_settigs['sms'];
        $phone_no_arr = $smsArr['phone_no'];
        $type = $phone_no_arr['type'];
        $value = $phone_no_arr['value'];
        try {
            if ($type == 'param') {
                $phone_no = $input_params[$value];
            } else {
                $phone_no_val = $value;
                $phone_no_val = $this->processRequestPregMatch($phone_no_val, $input_params);
                $phone_no_val = $this->processServerPregMatch($phone_no_val, $input_params);
                $phone_no_val = $this->processSystemPregMatch($phone_no_val, $input_params);
                $phone_no = $phone_no_val;
            }
            $sms_msg = $smsArr['message'];
            $sms_msg = $this->processRequestPregMatch($sms_msg, $input_params);
            $sms_msg = $this->processServerPregMatch($sms_msg, $input_params);
            $sms_msg = $this->processSystemPregMatch($sms_msg, $input_params);

            $text_message = $this->general->getReplacedInputParams($sms_msg, $input_params);
            $sms_array['message'] = $text_message;

            $success = $this->general->sendSMSNotification($phone_no, $sms_array);

            $insert_arr['eEntityType'] = 'General';
            $insert_arr['vReceiver'] = $phone_no;
            $insert_arr['eNotificationType'] = "SMS";
            $insert_arr['vSubject'] = 'SMS Notification - '.$phone_no;
            $insert_arr['tContent'] = $text_message;
            $insert_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $insert_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            if (!$success) {
                $insert_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $this->general->insertExecutedNotify($insert_arr);

            if (!$success) {
                throw new Exception('Failure in sending sms notification');
            }
            $return_arr['success'] = $success;
            $return_arr['message'] = "SMS Notification sent successfully";
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * send_desktop_notification method is used to sending desktop notification.
     * 
     * @param array $query_settigs array of query settings.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @return array $return_arr array of success and message will be return.
     */
    function send_desktop_notification($query_settigs = array(), $input_params = array()) {
        $desktopArr = $query_settigs['desktop'];
        $entity_id = ($desktopArr['entity_id']['type'] == "param") ? $input_params[$desktopArr['entity_id']['value']] : $desktopArr['entity_id']['value'];
        $entity_group = ($desktopArr['entity_group']['type'] == "param") ? $input_params[$desktopArr['entity_group']['value']] : $desktopArr['entity_group']['value'];
        $entity_receiver = ($desktopArr['entity_receiver']['type'] == "param") ? $input_params[$desktopArr['entity_receiver']['value']] : $desktopArr['entity_receiver']['value'];
        $entity_type = $desktopArr['entity_type'];
        $entity_subject = $this->general->getReplacedInputParams($desktopArr['entity_subject'], $input_params);
        $entity_content = $this->general->getReplacedInputParams($desktopArr['entity_content'], $input_params);
        $link_arr = $this->general->generateDesktopCustomLink($desktopArr['entity_link'], $input_params);
        try {
            $insert_arr['iEntityId'] = ($entity_id) ? $entity_id : '';
            $insert_arr['iGroupId'] = ($entity_group) ? $entity_group : '';
            $insert_arr['eEntityType'] = ($entity_type) ? $entity_type : '';
            $insert_arr['vReceiver'] = ($entity_receiver) ? $entity_receiver : '';
            $insert_arr['eNotificationType'] = "DesktopNotify";
            $insert_arr['vRedirectLink'] = addslashes(serialize($link_arr));
            $insert_arr['vSubject'] = ($entity_subject) ? $entity_subject : '';
            $insert_arr['tContent'] = ($entity_content) ? $entity_content : '';
            $insert_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $insert_arr['eStatus'] = "Pending";
            $success = $this->general->insertExecutedNotify($insert_arr);

            if (!$success) {
                throw new Exception('Failure in saving desktop notification');
            }
            $return_arr['success'] = $success;
            $return_arr['message'] = "Desktop notification saved successfully";
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * execute_condition_flow method is used for execution of flow conditions.
     * 
     * @param array $condition_settings array of condition settings will used as a parameter.
     * 
     * @param array $input_params array of value which will be compared.
     * 
     * @return array $return_arr array of success and message will be return.
     * 
     */
    function execute_condition_flow($condition_settings = array(), $input_params = array()) {
        $cond_type = $condition_settings['type']['satisfy'];
        $condition_array = $condition_settings['conditions'];
        $final_result = false;
        try {
            for ($i = 0; $i < count($condition_array); $i++) {
                $oper_1 = strtolower($condition_array[$i]['operand_1']);
                $value_1 = $condition_array[$i]['value_1'];
                $oper_2 = strtolower($condition_array[$i]['operand_2']);
                $value_2 = $condition_array[$i]['value_2'];
                $operator = $condition_array[$i]['operator'];
                $datatype = $condition_array[$i]['datatype'];
                $condition_result = null;
                if ($oper_1 == 'variable') {
                    $operand_1 = $input_params[$value_1];
                } else if ($oper_1 == "flow") {
                    $operand_1 = empty($input_params[$value_1]) ? 0 : 1;
                } else {
                    $operand_1 = $value_1;
                }
                if (!in_array($operator, array("nu", "em", "nn", "nem"))) {
                    if ($oper_2 == 'variable') {
                        $operand_2 = $input_params[$value_2];
                    } else {
                        $operand_2 = $value_2;
                    }
                } else {
                    $operand_2 = '';
                }
                
                $operand_1 = is_null($operand_1) ? '' : $operand_1;
                $operand_2 = is_null($operand_2) ? '' : $operand_2;
                
                $operand_1 = $this->general->getDataTypeWiseResult($datatype, $operand_1, true);
                $operand_2 = $this->general->getDataTypeWiseResult($datatype, $operand_2, false);
                $condition_result = $this->general->compareDataValues($operator, $operand_1, $operand_2);
                
                if ($cond_type == 'All') {
                    if (!$condition_result) {
                        $final_result = false;
                        break;
                    } else {
                        $final_result = true;
                    }
                } else {
                    if ($condition_result) {
                        $final_result = true;
                        break;
                    }
                }
            }
            if ($final_result == false) {
                throw new Exception('Some conditions does not match');
            }
            $success = 1;
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }

    /**
     * addOtherQuerySettings method is used to get query setings like extra conditions of where, group by, order by or having clause.
     * 
     * @param array $query_settigs array of query settings.
     * 
     * @param array $input_params array of input parameters.
     * 
     */
    function addOtherQuerySettings($query_settigs = array(), $input_params = array()) {
        extract($query_settigs);
        $this->db->from($query_tables['table_name'] . " AS " . $query_tables['table_alias']);

        $relation_tables = $query_tables['relation_tables'];
        // add relation tables
        if (is_array($relation_tables) && count($relation_tables) > 0) {
            $this->addRelationTables($input_params, $relation_tables);
        }

        //add where condition
        if ((is_array($query_fields['where_field']) && count($query_fields['where_field']) > 0) || $query['extra_condition'] != "") {
            $this->addConditions($input_params, $query_fields['where_field'], $query['extra_condition'], "AR", false, $query['where_operator']);
        }

        //physical remove condition
        if ($query['physical_remove'] == "No") {
            $this->general->getPhysicalRecordWhere($query_tables['table_name'], $query_tables['table_alias'], "AR");
        }

        //add group by
        if ((is_array($query_fields['group_field']) && count($query_fields['group_field']) > 0) || $query['extra_group_by_condition'] != "") {
            $this->AddGrouby($query_fields['group_field'], $input_params, $query['extra_group_by_condition']);
        }

        //add having 
        if ((is_array($query_fields['having_field']) && count($query_fields['having_field']) > 0) || $query['extra_having_condition']) {
            $this->AddHaving($query_fields['having_field'], $input_params, $query['extra_having_condition'], $query['having_operator']);
        }
    }

    /**
     * addRelationTables method is used to add relations table and their join conditions.
     * 
     * @param array $input_params array of input parameters will be used as a parameter.
     * 
     * @param array $relation_tables array of relation tables.
     * 
     * @param string $type type is used for active record and normal record.
     * 
     * @return array or string $join_tables join condition and relation table's alias will be return.
     */
    function addRelationTables($input_params = array(), $relation_tables = array(), $type = "AR") {
        $join_tables = "";
        if (!is_array($relation_tables) || count($relation_tables) == 0) {
            return $join_tables;
        }
        foreach ((array) $relation_tables as $key => $val) {
            $join_condition = $val['join_condition'];
            $extra_join_condition = $val['extra_join_condition'];
            if ($extra_join_condition != "") {
                $extra_join_condition = $this->processRequestPregMatch($extra_join_condition, $input_params);
                $extra_join_condition = $this->processServerPregMatch($extra_join_condition, $input_params);
                $extra_join_condition = $this->processSystemPregMatch($extra_join_condition, $input_params);
                $join_condition .= ' AND ' . $extra_join_condition;
            }
            $join_type = strtolower($val['join_type']);
            if ($type == "NR") {
                $join_type = (in_array($join_type, array("left", "right"))) ? strtoupper($join_type) . " JOIN" : "JOIN";
                $join_tables['join'][] = $join_type . ' ' . $val['table_name'] . ' AS ' . $val['table_alias'] . ' ON ' . $join_condition;
                $join_tables['alias'][] = $val['table_alias'] . '.*';
            } else {
                $join_type = (in_array($join_type, array("normal", "right"))) ? $join_type : "left";
                $this->db->join($val['table_name'] . ' AS ' . $val['table_alias'], $join_condition, $join_type);
            }
        }
        return $join_tables;
    }

    /**
     * addConditions method is used to make where conditions for query.
     * 
     * @param array $input_params array of input parameters
     * 
     * @param array $where_fields array of fields will be used as parameter which will be use in where clause.
     * 
     * @param string $extra_where where condition as a parameter.
     * 
     * @param string $type type is used for active record and normal record.
     * 
     * @param boolean $rem_alias true and false will be used as $rem_alias to keep table alias.
     * 
     * @return string $where_cond whole where condition will return.
     */
    function addConditions($input_params = array(), $where_fields = array(), $extra_where = '', $type = "AR", $rem_alias = false, $operator = "AND") {
        if (is_array($where_fields) && count($where_fields) > 0) {
            foreach ((array) $where_fields as $key => $val) {
                #$and_operator = ($where_fields[$key - 1]['and_operator']) ? strtolower($where_fields[$key - 1]['and_operator']) : "";
                $and_operator = strtolower($operator);
                $arr = $this->processWhereCondtion($input_params, $val, $and_operator, $type, $rem_alias);
                if ($type == "NR") {
                    $where_cond .= $arr;
                } else {
                    if (is_array($arr) && count($arr) > 0) {
                        if (in_array($arr['param_3'], array("none", "before", "after", "both"))) {
                            $this->db->$arr['condition']($arr['param_1'], $arr['param_2'], $arr['param_3']);
                        } else if ($arr['param_3'] == "false") {
                            if ($arr['param_2'] == "false") {
                                $this->db->$arr['condition']($arr['param_1'], false, false);
                            } else {
                                $this->db->$arr['condition']($arr['param_1'], $arr['param_2'], false);
                            }
                        } else if ($arr['param_2'] == "false") {
                            $this->db->$arr['condition']($arr['param_1'], false);
                        } else {
                            $this->db->$arr['condition']($arr['param_1'], $arr['param_2']);
                        }
                    }
                }
            }
        }
        if ($extra_where != "") {
            $extra_where = $this->processRequestPregMatch($extra_where, $input_params);
            $extra_where = $this->processServerPregMatch($extra_where, $input_params);
            $extra_where = $this->processSystemPregMatch($extra_where, $input_params);
            if ($extra_where != "") {
                if ($type == "NR") {
                    $where_cond = ($where_cond != '') ? $where_cond . ' AND ' . $extra_where : $extra_where;
                } else {
                    $this->db->where($extra_where, false, false);
                }
            }
        }
        return $where_cond;
    }

    /**
     * addGroupby method is used to create group by clause for query.
     * 
     * @param array $group_field array of group fields.
     * 
     * @param array $input_params array of input parameters will be used as a parameter.
     * 
     * @param string $extra_group it will be use as extra group by condition.
     */
    function addGrouby($group_field = array(), $input_params = array(), $extra_group = '') {
        for ($i = 0; $i < count($group_field); $i++) {
            if ($group_field[$i]['display_in_query'] != '') {
                $this->db->group_by($group_field[$i]['display_in_query']);
            }
        }
        if (trim($extra_group) != "") {
            $extra_group = $this->processRequestPregMatch($extra_group, $input_params);
            $extra_group = $this->processServerPregMatch($extra_group, $input_params);
            $extra_group = $this->processSystemPregMatch($extra_group, $input_params);
            if ($extra_group != "") {
                $this->db->group_by($extra_group);
            }
//            $extra_group_arr = array_filter(explode(",", $extra_group));
//            for ($i = 0; $i < count($extra_group_arr); $i++) {
//                if ($extra_group_arr[$i] != '') {
//                    $this->db->group_by($extra_group_arr[$i]);
//                }
//            }
        }
    }

    /**
     * addHaving method is used to create having clause for query.
     * 
     * @param array $having_fields array of having fields.
     * 
     * @param array $input_params array of input parameters will be used as a parameter.
     * 
     * @param string $extra_having it will be use as extra having condition.
     */
    function addHaving($having_fields = array(), $input_params = array(), $extra_having = '') {
        if (is_array($having_fields) && count($having_fields) > 0) {
            foreach ((array) $having_fields as $key => $val) {
                #$and_operator = ($having_fields[$key - 1]['and_operator']) ? strtolower($having_fields[$key - 1]['and_operator']) : "";
                $and_operator = strtolower($operator);
                $arr = $this->processHavingCondtion($input_params, $val, $and_operator);
                if (is_array($arr) && count($arr) > 0) {
                    if ($arr['param_3'] == "false") {
                        if ($arr['param_2'] == "false") {
                            $this->db->$arr['condition']($arr['param_1'], false, false);
                        } else {
                            $this->db->$arr['condition']($arr['param_1'], $arr['param_2'], false);
                        }
                    } else if ($arr['param_2'] == "false") {
                        $this->db->$arr['condition']($arr['param_1'], false);
                    } else {
                        $this->db->$arr['condition']($arr['param_1'], $arr['param_2']);
                    }
                }
            }
        }
        if ($extra_having != "") {
            $extra_having = $this->processRequestPregMatch($extra_having, $input_params);
            $extra_having = $this->processServerPregMatch($extra_having, $input_params);
            $extra_having = $this->processSystemPregMatch($extra_having, $input_params);
            if ($extra_having != "") {
                $this->db->having($extra_having, false, false);
            }
        }
    }

    /**
     * addOrder method is used to create order by clause for query.
     * 
     * @param array $order_field array of order fields.
     * 
     * @param array $input_params array of input parameters will be used as a parameter.
     * 
     * @param string $extra_order it will be use as extra order by condition.
     */
    function addOrder($order_field = array(), $input_params = array(), $extra_order = '') {
        for ($i = 0; $i < count($order_field); $i++) {
            if ($order_field[$i]['display_in_query'] != '') {
                if ($order_field[$i]['value'] == '') {
                    $order_field[$i]['value'] = "ASC";
                }
                $display_query = $order_field[$i]['display_in_query'];
                $display_query = $this->processRequestPregMatch($display_query, $input_params);
                $display_query = $this->processServerPregMatch($display_query, $input_params);
                $display_query = $this->processSystemPregMatch($display_query, $input_params);
                if (stripos($display_query, " ") === false) {
                    $this->db->order_by($display_query, $order_field[$i]['value']);
                } else {
                    $this->db->order_by($display_query, $order_field[$i]['value'], false);
                }
            }
        }
        if ($extra_order != "") {
            $extra_order = $this->processRequestPregMatch($extra_order, $input_params);
            $extra_order = $this->processServerPregMatch($extra_order, $input_params);
            $extra_order = $this->processSystemPregMatch($extra_order, $input_params);
            if ($extra_order != "") {
                $this->db->order_by($extra_order);
            }
        }
    }

    /**
     * processWhereCondtion method is used to process where condition.
     * 
     * @param array $input_params array of input parameters
     * 
     * @param array $arr array of where fields.
     * 
     * @param string $and_operator or and and operator can be use.
     * 
     * @param string $type type is used for active record and normal record.
     * 
     * @param boolean $rem_alias true and false will be used as $rem_alias to keep table alias.
     * 
     * @return array $ret_arr array with conditions and parameters will be return.
     */
    function processWhereCondtion($input_params = array(), $arr = array(), $and_operator = 'and', $type = "AR", $rem_alias = false) {
        $sql_entry = strtolower($arr['sql_entry']);
        $display_in_query = ($rem_alias === true) ? $arr['field_name'] : $arr['display_in_query'];
        $value = $arr['value'];
        $field_code = $arr['field_code'];
        $operator = $arr['operator'];
        if ($sql_entry == "yes") {
            $fina_val = $value;
            if (strstr($fina_val, $this->requestVariable())) {
                $fina_val = $this->processRequestPregMatch($fina_val, $input_params);
            }
            if (strstr($fina_val, $this->serverVariable())) {
                $fina_val = $this->processServerPregMatch($fina_val, $input_params);
            }
            if (strstr($fina_val, $this->systemVariable())) {
                $fina_val = $this->processSystemPregMatch($fina_val, $input_params);
            }
        } else {
            $fina_val = $value;
            if (strstr($fina_val, $this->requestVariable())) {
                $fina_val = $this->processRequestVariable($fina_val, $field_code, $input_params);
                if ($input_params[$field_code] == "" && $type == "AR") {
                    return;
                }
            }
            if (strstr($fina_val, $this->serverVariable())) {
                $fina_val = $this->processServerVariable($fina_val, $field_code, $input_params);
            }
            if (strstr($fina_val, $this->systemVariable())) {
                $fina_val = $this->processSystemVariable($fina_val, $field_code, $input_params);
            }
        }
        switch ($operator) {
            case "eq":
            case "li":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
                $ret_arr['param_1'] = $display_in_query . ' =';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '= ' . $fina_val . '';
                $delete_str = '= \'' . $fina_val . '\'';
                break;
            case "ne":
            case "nl":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
                $ret_arr['param_1'] = $display_in_query . ' <>';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '<> ' . $fina_val . '';
                $delete_str = '<> \'' . $fina_val . '\'';
                break;
            case "lt":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
                $ret_arr['param_1'] = $display_in_query . ' <';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '< ' . $fina_val . '';
                $delete_str = '< \'' . $fina_val . '\'';
                break;
            case "le":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
                $ret_arr['param_1'] = $display_in_query . ' <=';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '<= ' . $fina_val . '';
                $delete_str = '<= \'' . $fina_val . '\'';
                break;
            case "gt":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
                $ret_arr['param_1'] = $display_in_query . ' >';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '> ' . $fina_val . '';
                $delete_str = '> \'' . $fina_val . '\'';
                break;
            case "ge":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
                $ret_arr['param_1'] = $display_in_query . ' >=';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '>= ' . $fina_val . '';
                $delete_str = '>= \'' . $fina_val . '\'';
                break;
            /*
              case "li":
              $ret_arr['condition'] = ($and_operator == "or") ? "or_like" : "like";
              $ret_arr['param_1'] = $display_in_query;
              $ret_arr['param_2'] = $fina_val;
              $ret_arr['param_3'] = "none";
              $param_str = 'LIKE ' . $fina_val . '';
              $delete_str = 'LIKE \'' . $fina_val . '\'';
              break;
             */
            case "lis"://old
            case "bw"://new
                $ret_arr['condition'] = ($and_operator == "or") ? "or_like" : "like";
                $ret_arr['param_1'] = $display_in_query;
                $ret_arr['param_2'] = $fina_val;
                $ret_arr['param_3'] = "after";
                $param_str = 'LIKE ' . $fina_val . '%';
                $delete_str = 'LIKE \'' . $fina_val . '%\'';
                break;
            case "lie"://old
            case "ew"://new
                $ret_arr['condition'] = ($and_operator == "or") ? "or_like" : "like";
                $ret_arr['param_1'] = $display_in_query;
                $ret_arr['param_2'] = $fina_val;
                $ret_arr['param_3'] = "before";
                $param_str = 'LIKE %' . $fina_val . '';
                $delete_str = 'LIKE \'%' . $fina_val . '\'';
                $in_flag = true;
                break;
            case "lib"://old
            case "cn"://new
                $ret_arr['condition'] = ($and_operator == "or") ? "or_like" : "like";
                $ret_arr['param_1'] = $display_in_query;
                $ret_arr['param_2'] = $fina_val;
                $ret_arr['param_3'] = "both";
                $param_str = 'LIKE %' . $fina_val . '%';
                $delete_str = 'LIKE \'%' . $fina_val . '%\'';
                break;
            /*
              case "nl":
              $ret_arr['condition'] = ($and_operator == "or") ? "or_not_like" : "not_like";
              $ret_arr['param_1'] = $display_in_query;
              $ret_arr['param_2'] = $fina_val;
              $ret_arr['param_3'] = "none";
              $param_str = 'NOT LIKE ' . $fina_val . '';
              $delete_str = 'NOT LIKE \'' . $fina_val . '\'';
              break;
             */
            case "nls"://old
            case "en"://new
                $ret_arr['condition'] = ($and_operator == "or") ? "or_not_like" : "not_like";
                $ret_arr['param_1'] = $display_in_query;
                $ret_arr['param_2'] = $fina_val;
                $ret_arr['param_3'] = "before";
                $param_str = 'NOT LIKE %' . $fina_val . '';
                $delete_str = 'NOT LIKE \'%' . $fina_val . '\'';
                break;
            case "nle"://old
            case "bn"://new
                $ret_arr['condition'] = ($and_operator == "or") ? "or_not_like" : "not_like";
                $ret_arr['param_1'] = $display_in_query;
                $ret_arr['param_2'] = $fina_val;
                $ret_arr['param_3'] = "after";
                $param_str = 'NOT LIKE ' . $fina_val . '%';
                $delete_str = 'NOT LIKE \'' . $fina_val . '%\'';
                break;
            case "nlb"://old
            case "nc"://new
                $ret_arr['condition'] = ($and_operator == "or") ? "or_not_like" : "not_like";
                $ret_arr['param_1'] = $display_in_query;
                $ret_arr['param_2'] = $fina_val;
                $ret_arr['param_3'] = "both";
                $param_str = 'NOT LIKE %' . $fina_val . '%';
                $delete_str = 'NOT LIKE \'%' . $fina_val . '%\'';
                break;
            case "in":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_where_in" : "where_in";
                $ret_arr['param_1'] = $display_in_query;
                if ($fina_val != "") {
                    $fina_arr = explode(",", $fina_val);
                } else {
                    $fina_arr = array();
                }
                $ret_arr['param_2'] = $fina_arr;
                if ($sql_entry == "yes") {
                    $param_str = $delete_str = "IN (" . $fina_val . ")";
                } else {
                    $param_str = $delete_str = "IN ('" . @implode("','", $fina_arr) . "')";
                }
                break;
            case "ni":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_where_not_in" : "where_not_in";
                $ret_arr['param_1'] = $display_in_query;
                if ($fina_val != "") {
                    $fina_arr = explode(",", $fina_val);
                } else {
                    $fina_arr = array();
                }
                $ret_arr['param_2'] = $fina_arr;
                if ($sql_entry == "yes") {
                    $param_str = $delete_str = "NOT IN (" . $fina_val . ")";
                } else {
                    $param_str = $delete_str = "NOT IN ('" . @implode("','", $fina_arr) . "')";
                }
                break;
            case "nu":
            case "em":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
                $ret_arr['param_1'] = "(" . $display_in_query . " IS NULL OR " . $display_in_query . " = '')";
                $ret_arr['param_2'] = "false";
                $ret_arr['param_3'] = "false";
                $param_str = "IS NULL";
                $delete_str = "IS NULL";
                break;
            case "nn":
            case "nem":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
                $ret_arr['param_1'] = "(" . $display_in_query . " IS NOT NULL OR " . $display_in_query . " <> '')";
                $ret_arr['param_2'] = "false";
                $ret_arr['param_3'] = "false";
                $param_str = "IS NOT NULL";
                $delete_str = "IS NOT NULL";
                break;
            /*
              case "em":
              $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
              $ret_arr['param_1'] = $display_in_query . " = ''";
              $ret_arr['param_2'] = "false";
              $ret_arr['param_3'] = "false";
              $param_str = "= ''";
              $delete_str = "= ''";
              break;
              case "nem":
              $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
              $ret_arr['param_1'] = $display_in_query . " <> ''";
              $ret_arr['param_2'] = "false";
              $ret_arr['param_3'] = "false";
              $param_str = "<> ''";
              $delete_str = "<> ''";
              break;
             */
        }
        if ($sql_entry == "yes") {
            $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
            if (in_array($operator, array('nu', 'em', 'nn', 'nem'))) {
                $ret_arr['param_1'] = $ret_arr['param_1'];
            } else {
                $ret_arr['param_1'] = '' . $display_in_query . ' ' . $param_str . '';
            }
            $ret_arr['param_2'] = "false";
            $ret_arr['param_3'] = "false";
        }
        if ($type == "NR") {
            if (in_array($operator, array('nu', 'em', 'nn', 'nem'))) {
                $query_str = $ret_arr['param_1'];
            } else {
                $query_str = $display_in_query . ' ' . $delete_str;
            }
            if ($and_operator == "or") {
                $ret_arr = ' OR ' . $query_str;
            } else {
                $ret_arr = ' AND ' . $query_str;
            }
        }

        return $ret_arr;
    }

    /**
     * processHavingCondtion method is used to process having condition.
     * 
     * @param array $input_params array of input parameters
     * 
     * @param array $arr array of having fields.
     * 
     * @param string $and_operator or and and operator can be use.
     * 
     * @return array $ret_arr array with conditions and parameters will be return.
     */
    function processHavingCondtion($input_params = array(), $arr = array(), $and_operator = 'and') {
        $sql_entry = strtolower($arr['sql_entry']);
        $display_in_query = $arr['display_in_query'];
        $value = $arr['value'];
        $field_code = $arr['field_code'];
        $operator = $arr['operator'];
        $aggregate_operator = $arr['aggregate_operator'];
        $display_in_query = ($aggregate_operator != "") ? $aggregate_operator . "(" . $display_in_query . ")" : $display_in_query;
        if ($sql_entry == "yes") {
            $fina_val = $value;
            if (strstr($fina_val, $this->requestVariable())) {
                $fina_val = $this->processRequestPregMatch($fina_val, $input_params);
            }
            if (strstr($fina_val, $this->serverVariable())) {
                $fina_val = $this->processServerPregMatch($fina_val, $input_params);
            }
            if (strstr($fina_val, $this->systemVariable())) {
                $fina_val = $this->processSystemPregMatch($value, $field_code, $input_params);
            }
        } else {
            $fina_val = $value;
            if (strstr($fina_val, $this->requestVariable())) {
                $fina_val = $this->processRequestVariable($fina_val, $field_code, $input_params);
                if (!isset($input_params[$field_code]) && $type == "AR") {
                    return;
                }
            }
            if (strstr($fina_val, $this->serverVariable())) {
                $fina_val = $this->processServerVariable($fina_val, $field_code, $input_params);
            }
            if (strstr($fina_val, $this->systemVariable())) {
                $fina_val = $this->processSystemVariable($fina_val, $field_code, $input_params);
            }
        }
        switch ($operator) {
            case "eq":
            case "li":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_having" : "having";
                $ret_arr['param_1'] = $display_in_query . ' =';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '= ' . $fina_val . '';
                break;
            case "gt":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_having" : "having";
                $ret_arr['param_1'] = $display_in_query . ' >';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '> ' . $fina_val . '';
                break;
            case "ge":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_having" : "having";
                $ret_arr['param_1'] = $display_in_query . ' >=';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '>= ' . $fina_val . '';
                break;
            case "lt":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_having" : "having";
                $ret_arr['param_1'] = $display_in_query . ' <';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '< ' . $fina_val . '';
                break;
            case "le":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_having" : "having";
                $ret_arr['param_1'] = $display_in_query . ' <=';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '<= ' . $fina_val . '';
                break;
            case "ne":
                $ret_arr['condition'] = ($and_operator == "or") ? "or_having" : "having";
                $ret_arr['param_1'] = $display_in_query . ' <>';
                $ret_arr['param_2'] = $fina_val;
                $param_str = '<> ' . $fina_val . '';
                break;
        }
        if ($sql_entry == "yes") {
            $ret_arr['condition'] = ($and_operator == "or") ? "or_where" : "where";
            $ret_arr['param_1'] = '' . $display_in_query . ' ' . $param_str . '';
            $ret_arr['param_2'] = "false";
            $ret_arr['param_3'] = "false";
        }
        return $ret_arr;
    }

    /**
     * processSelectRecords method is used to process fetched records.
     * 
     * @param array $result_arr array of selected records.
     * 
     * @return array $result_arr array which will be return after processing.
     */
    function processSelectRecords($result_arr = array()) {
        $image_config_arr = $this->_image_func_config_arr;
        $ret_arr = $result_arr;
        if (!is_array($result_arr) || count($result_arr) == 0) {
            return $ret_arr;
        }
        if (!is_array($image_config_arr) || count($image_config_arr) == 0) {
            return $ret_arr;
        }
        for ($i = 0; $i < count($result_arr); $i++) {
            foreach ((array) $image_config_arr as $key => $val) {
                $field_val = $result_arr[$i][$key];
                $func_arr = $val['phpfn'];
                if (is_array($func_arr) && count($func_arr) > 0) {
                    $php_fn_arr = $this->getPHPFunctionResult($field_val, $func_arr, $result_arr[$i]);
                    if ($php_fn_arr['success']) {
                        $result_arr[$i][$key] = $php_fn_arr['result'];
                    }
                }
            }
        }

        return $result_arr;
    }

    /**
     * getInsertUpdateFields method is used to insert update the fields.
     * 
     * @param array $insert_fields array of fields.
     * 
     * @param array $input_params array of input parameter.
     * 
     * @return array $result_arr array of success and message will be return.
     */
    function getInsertUpdateFields($insert_fields = array(), $input_params = array()) {
        $result_arr['success'] = 0;
        $result_arr['message'] = "";
        $result_arr['result'] = array();
        if (!is_array($insert_fields) || count($insert_fields) == 0) {
            return $result_arr;
        }
        foreach ((array) $insert_fields as $key => $val) {
            $value = $val['value'];
            $field_code = $val['field_code'];
            $field_name = $val['field_name'];
            $sql_entry = $val['sql_entry'];
            $func_arr['function_name'] = $val['function_name'];
            $isset_str = false;
            if ($sql_entry == "Yes") {
                $ret_val = $value;
                if (strstr($ret_val, $this->requestVariable())) {
                    $ret_val = $this->processRequestPregMatch($ret_val, $input_params);
                }
                if (strstr($ret_val, $this->serverVariable())) {
                    $ret_val = $this->processServerPregMatch($ret_val, $input_params);
                }
                if (strstr($ret_val, $this->systemVariable())) {
                    $ret_val = $this->processSystemPregMatch($ret_val, $input_params);
                }
                $fina_val = $ret_val;
            } else {
                $ret_val = $value;
                if (strstr($ret_val, $this->requestVariable())) {
                    $ret_val = $this->processRequestVariable($ret_val, $field_code, $input_params);
                    $isset_str = true;
                }
                if (strstr($ret_val, $this->serverVariable())) {
                    $ret_val = $this->processServerVariable($ret_val, $field_code, $input_params);
                }
                if (strstr($ret_val, $this->systemVariable())) {
                    $ret_val = $this->processSystemVariable($ret_val, $input_params);
                }
                $fina_val = $ret_val;
            }
            $insert_arr[$field_name]['field_val'] = $fina_val;
            $insert_arr[$field_name]['field_code'] = $field_code;
            $php_fn_arr = $this->getPHPFunctionResult($fina_val, $func_arr);
            if ($php_fn_arr['success']) {
                $fina_val = $php_fn_arr['result'];
            }

            $insert_arr[$field_name]['param_2'] = $fina_val;
            if ($sql_entry == "Yes") {
                $insert_arr[$field_name]['param_3'] = "false";
            } else if ($isset_str == true) {
                $insert_arr[$field_name]['param_4'] = "isset";
            }
        }
        $result_arr['success'] = 1;
        $result_arr['result'] = $insert_arr;
        return $result_arr;
    }

    /**
     * getPHPFunctionResult method is used to get result of php function.
     * 
     * @param string $field_val database value of field.
     * 
     * @param array $func_arr array of function settings.
     * 
     * @param array $result_arr array of records of current row.
     * 
     * @return array $return_arr array of success and message will be return.
     */
    function getPHPFunctionResult($field_val = '', $func_arr = array(), $result_arr = array()) {
        $return_arr['success'] = 1;
        $return_arr['message'] = "";
        $return_arr['result'] = $reqparam;

        $function_name = $func_arr['function_name'];
        /* Process function */
        if (method_exists($this->general, $function_name)) {
            $return_arr['result'] = $this->general->$function_name($field_val, $result_arr);
        } elseif (function_exists($function_name)) {
            $return_arr['result'] = call_user_func($function_name, $field_val);
        } else {
            $return_arr['success'] = 0;
            $return_arr['message'] = "No such method exist in general library. Please create one method named '" . $function_name . "'. Please check Notification configuration panel.";
        }
        /* Process function */
        return $return_arr;
    }

    /**
     * assignImageAndPHPFunctions method is used to assign image or php function's properties to the object variable.
     * 
     * @param array $data_arr array of current row's data. 
     */
    function assignImageAndPHPFunctions($dataArr = array()) {
        if ($dataArr['function_name'] != "" && $dataArr['function_type'] != "") {
            $func_config['function_type'] = $dataArr['function_type'];
            $func_config['function_name'] = $dataArr['function_name'];
            $func_config['dynamic_function'] = $dataArr['dynamic_function'];
            $this->_image_func_config_arr[$dataArr['field_code']]['phpfn'] = $func_config;
        }
    }

    /**
     * processRequestVariable method is used to process request variable.
     * 
     * @param string $condition condition for which processing will done.
     * 
     * @param string $varaible value of request variable for processing.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @return string $condition after processing the request variable, this string will be return.
     */
    function processRequestVariable($condition = '', $varaible = '', $input_params = array()) {
        if (strstr($condition, $this->requestVariable())) {
            $condition = @str_replace("{%REQUEST." . $varaible . "%}", $input_params[$varaible], $condition);
        }
        return $condition;
    }

    /**
     * processServerVariable method is used to process server variable.
     * 
     * @param string $condition condition for which processing will done.
     * 
     * @param string $varaible value of server variable for processing.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @return string $condition after processing the server variable, this string will be return.
     */
    function processServerVariable($condition = '', $varaible = '', $input_params = array()) {
        if (strstr($condition, $this->serverVariable())) {
            $pos_1 = strpos($condition, ".");
            $pos_2 = strrpos($condition, "%");
            $varaible = substr($condition, $pos_1 + 1, ($pos_2 - $pos_1 - 1));
            if ($varaible != "") {
                $condition = $_SERVER[$varaible];
            }
        }
        return $condition;
    }

    /**
     * processSystemVariable method is used to process system variable.
     * 
     * @param string $condition condition for which processing will done.
     * 
     * @param string $varaible value of system variable for processing.
     * 
     * @param array $input_params array of input parameters.
     * 
     * @return string $condition after processing the system variable, this string will be return.
     */
    function processSystemVariable($condition = '', $varaible = '', $input_params = array()) {
        if (strstr($condition, $this->systemVariable())) {
            $pos_1 = strpos($condition, ".");
            $pos_2 = strrpos($condition, "%");
            $varaible = substr($condition, $pos_1 + 1, ($pos_2 - $pos_1 - 1));
            if ($varaible != "") {
                $condition = $this->config->item($varaible);
            }
        }
        return $condition;
    }

    /**
     * requestVariable method used to process request parameter.
     * 
     * @return string {%REQUEST will be return.
     */
    function requestVariable() {
        return "{%REQUEST";
    }

    /**
     * serverVariable method used to process server parameter.
     * 
     * @return string {%SERVER will be return.
     */
    function serverVariable() {
        return "{%SERVER";
    }

    /**
     * systemVariable method used to process system parameter.
     * 
     * @return string {%SYSTEM will be return.
     */
    function systemVariable() {
        return "{%SYSTEM";
    }

    /**
     * processRequestPregMatch method is used to match and process the request parameters
     * 
     * @param string $param request parameter.
     * 
     * @param array $input_params array of input params.
     * 
     * @return string $params after processing the request parameter this string will be return.
     */
    function processRequestPregMatch($param = '', $input_params = array()) {
        if ($param != "") {
            preg_match_all("/{%REQUEST\.([a-zA-Z0-9_-]{1,})/i", $param, $preg_all_arr);
            if (strstr($param, '{%REQUEST') !== false) {
                if (isset($preg_all_arr[1]) && is_array($preg_all_arr[1]) && count($preg_all_arr[1]) > 0) {
                    foreach ((array) $preg_all_arr[1] as $key => $value) {
                        if (strstr($param, '{%REQUEST') !== false) {
                            $param = str_replace('{%REQUEST.' . $value . '%}', $input_params[$value], $param);
                        }
                    }
                }
            }
        }
        return $param;
    }

    /**
     * processServerPregMatch method is used to match and process the server parameters
     * 
     * @param string $param server parameter.
     * 
     * @param array $input_params array of input params.
     * 
     * @return string $params after processing the server parameter this string will be return.
     */
    function processServerPregMatch($param = '', $input_params = array()) {
        if ($param != "") {
            preg_match_all("/{%SERVER\.([a-zA-Z0-9_-]{1,})/i", $param, $preg_all_arr);
            if (strstr($param, '{%SERVER') !== false) {
                if (isset($preg_all_arr[1]) && is_array($preg_all_arr[1]) && count($preg_all_arr[1]) > 0) {
                    foreach ((array) $preg_all_arr[1] as $key => $value) {
                        if (strstr($param, '{%SERVER') !== false) {
                            $param = str_replace('{%SERVER.' . $value . '%}', $_SERVER[$value], $param);
                        }
                    }
                }
            }
        }
        return $param;
    }

    /**
     * processSystemPregMatch method is used to match and process the system parameters
     * 
     * @param string $param system parameter.
     * 
     * @param array $input_params array of input params.
     * 
     * @return string $params after processing the system parameter this string will be return.
     */
    function processSystemPregMatch($param = '', $input_params = array()) {
        if ($param != "") {
            preg_match_all("/{%SYSTEM\.([a-zA-Z0-9_-]{1,})/i", $param, $preg_all_arr);
            if (strstr($param, '{%SYSTEM') !== false) {
                if (isset($preg_all_arr[1]) && is_array($preg_all_arr[1]) && count($preg_all_arr[1]) > 0) {
                    foreach ((array) $preg_all_arr[1] as $key => $value) {
                        if (strstr($param, '{%SYSTEM') !== false) {
                            $param = str_replace('{%SYSTEM.' . $value . '%}', $this->config->item($value), $param);
                        }
                    }
                }
            }
        }
        return $param;
    }

}
