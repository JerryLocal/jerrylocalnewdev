<?php
/**
 * Description of NS Engine Controller
 * 
 * @module NS Engine
 * 
 * @class notifycontroller.php
 * 
 * @path application\front\nsengine\controllers\notifycontroller.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class NotifyController extends HB_Controller
{

    protected $_debug_log = FALSE;
    protected $_debug_called = FALSE;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        if (!$this->input->get_post("ns_debug", true)) {
            $this->db->db_debug = FALSE;
        }
        $this->load->model('notify');
        $this->load->model('notify_schedule');
        $this->load->model('notify_values');
        $this->load->library('notifyresponse');
    }

    /**
     * listNSMethods method is used to get all notifications list.
     */
    public function listNSMethods()
    {
        $this->set_template('ns_template.tpl');
        if ($_ENV['debug_action']) {
            $this->config->load('hb_notifications', TRUE);
            $all_methods = $this->config->item('hb_notifications');
        }
        $all_methods = empty($all_methods) ? array() : $all_methods;
        $render_arr = array(
            'all_methods' => $all_methods
        );
        $this->smarty->assign($render_arr);
    }

    /**
     * executeNotifySchedule method is used to execute notifications.
     */
    public function executeNotifySchedule()
    {
        $this->executeOperationSchedule();
        $this->executeTimeSchedule();
        $ns_debug = $this->input->get_post("ns_debug", true);
        if ($ns_debug == 1 && $_ENV['debug_action']) {
            $arr['queries'] = $this->general->getDBQueriesList();
            $ret = $this->notifyresponse->json_safe_encode($arr);
            header('Content-Type: application/json; charset=utf-8');
            echo $ret;
        } else {
            echo 1;
        }
        exit;
    }

    /**
     * notifyExecuter method is used to text or debug notifications.
     * @param string $func_name func_name is the webservice name.
     * @return array $responce_arr returns webservice response. (json format)
     */
    public function notifyExecuter($func_name = '')
    {
        $this->config->load('hb_notifications', TRUE);
        $all_methods = $this->config->item('hb_notifications');
        if (empty($all_methods[$func_name])) {
            show_error('Notification code not found. Please save settings or update code.', 400);
        }
        $db_event_data = $all_methods[$func_name];
        $params_arr = $output_arr = array();
        if ($db_event_data['type'] == "Operation") {
            $extra_cond_arr = array();
            $extra_cond_arr[] = array("field" => "mns.eNotifyType", "value" => 'Operation');
            $extra_cond_arr[] = array("field" => "mns.eStatus", "value" => 'Pending');
            $extra_cond_arr[] = array("field" => "mns.vNotifyName", "value" => $func_name);

            $fields = "mns.iNotifyScheduleId, mns.vNotifyName, mns.eOperation, mns.dtAddDateTime";
            $db_notify_operation = $this->notify_schedule->getData($extra_cond_arr, $fields);

            if (!is_array($db_notify_operation) || count($db_notify_operation) == 0) {
                show_error('This notification does not have any input data. Please ' . @implode("/", $db_event_data['operations']) . ' record to table "' . $db_event_data['table'] . '"', 400);
            }

            $notify_schedule_id = $db_notify_operation[0]['iNotifyScheduleId'];

            $extra_cond_arr = array();
            $extra_cond_arr[] = array("field" => "mns.eNotifyType", "value" => 'Operation');
            $extra_cond_arr[] = array("field" => "mns.eStatus", "value" => 'Pending');
            $extra_cond_arr[] = array("field" => "mns.iNotifyScheduleId", "value" => $notify_schedule_id);
            $fields = "mns.iNotifyScheduleId, mnop.vFieldName, mnop.tOldValue, mnop.tNewValue";
            $db_notify_values = $this->notify_values->getData($extra_cond_arr, $fields);

            foreach ((array) $db_notify_values as $n_key => $n_val) {
                $old_key = "OLD_" . $n_val['vFieldName'];
                $new_key = "NEW_" . $n_val['vFieldName'];
                $temp_arr = array();
                $temp_arr[$old_key] = $n_val['tOldValue'];
                $temp_arr[$new_key] = $n_val['tNewValue'];
                $params_arr = array_merge($params_arr, $temp_arr);
            }
            $params_arr['OPERATION'] = $db_notify_operation[0]['eOperation'];

            $output_arr = $this->fireNotification($func_name, $params_arr, TRUE);

            $this->updateOperBasedRespose($notify_schedule_id, $output_arr);
        } else if ($db_event_data['type'] == "Time") {
            $output_arr = $this->fireNotification($func_name, $params_arr, TRUE);
            $this->insertTimeBasedRespose($func_name, $output_arr);
        }
        //print output response
        if ($this->_debug_called == TRUE) {
            $this->notifyresponse->sendNSResponse($output_arr, $this->notifyresponse->ns_debug_params);
        } else {
            $this->notifyresponse->sendNSResponse($output_arr);
        }
    }

    /**
     * executeOperationSchedule method is used to execute operation based notifications.
     */
    public function executeOperationSchedule()
    {
        $db_notify_assoc_values = $output_arr = $notify_arr = array();

        $extra_cond_arr = array();
        $extra_cond_arr[] = array("field" => "mns.eNotifyType", "value" => 'Operation');
        $extra_cond_arr[] = array("field" => "mns.eStatus", "value" => 'Pending');
        $fields = "mns.iNotifyScheduleId, mns.vNotifyName, mns.eOperation, mns.dtAddDateTime";
        $db_notify_operation = $this->notify_schedule->getData($extra_cond_arr, $fields, "mns.iNotifyScheduleId");

        if (!is_array($db_notify_operation) || count($db_notify_operation) == 0) {
            return $output_arr;
        }

        foreach ($db_notify_operation as $key => $val) {
            $notify_arr[] = $val['iNotifyScheduleId'];
        }

        $extra_cond_arr = array();
        $extra_cond_arr[] = array("field" => "mns.eNotifyType", "value" => 'Operation');
        $extra_cond_arr[] = array("field" => "mns.eStatus", "value" => 'Pending');
        $extra_cond_arr[] = array("field" => "mns.iNotifyScheduleId", "value" => $notify_arr, "oper" => "in");
        $fields = "mns.iNotifyScheduleId, mnop.vFieldName, mnop.tOldValue, mnop.tNewValue";
        $db_notify_values = $this->notify_values->getData($extra_cond_arr, $fields);

        foreach ((array) $db_notify_values as $n_key => $n_val) {
            $old_key = "OLD_" . $n_val['vFieldName'];
            $new_key = "NEW_" . $n_val['vFieldName'];
            $arr[$old_key] = $n_val['tOldValue'];
            $arr[$new_key] = $n_val['tNewValue'];
            if (is_array($db_notify_assoc_values[$n_val['iNotifyScheduleId']])) {
                $db_notify_assoc_values[$n_val['iNotifyScheduleId']] = array_merge($db_notify_assoc_values[$n_val['iNotifyScheduleId']], $arr);
            } else {
                $db_notify_assoc_values[$n_val['iNotifyScheduleId']] = $arr;
            }
        }
        foreach ((array) $db_notify_operation as $n_key => $n_val) {
            $db_notify_assoc_values[$n_val['iNotifyScheduleId']]['OPERATION'] = $n_val['eOperation'];
            $output_arr = $this->fireNotification($n_val['vNotifyName'], $db_notify_assoc_values[$n_val['iNotifyScheduleId']]);
            $res = $this->updateOperBasedRespose($n_val['iNotifyScheduleId'], $output_arr);
        }
    }

    /**
     * executeTimeSchedule method is used to execute time based notifications.
     */
    function executeTimeSchedule()
    {
        $output_arr = array();

        $all_methods = $this->config->item('hb_notifications');
        if (!is_array($all_methods) || count($all_methods) == 0) {
            return $output_arr;
        }
        foreach ($all_methods as $key => $val) {
            if ($val['type'] == "Time" && $val['status'] == "Active") {
                $db_time_data[$key] = $val;
            }
        }
        if (!is_array($db_time_data) || count($db_time_data) == 0) {
            return $output_arr;
        }

        $extra_cond_arr = array();
        $extra_cond_arr[] = array("field" => "mns.eNotifyType", "value" => 'Time');
        $fields_arr = array();
        $fields_arr[] = array("field" => "mns.vNotifyName");
        $fields_arr[] = array("MAX(" . $this->db->protect("mns.dtExeDateTime") . ") AS AS dtExeDateTime", "escape" => TRUE);
        $db_notify_data = $this->notify_schedule->getData($extra_cond_arr, $fields_arr, "mns.dtExeDateTime ASC", "mns.vNotifyName");
        $db_notify_time = array();
        for ($i = 0; $i < count($db_notify_data); $i++) {
            $dtExeDateTime = $db_notify_data[$i]['dtExeDateTime'];
            $vNotifyName = $db_notify_data[$i]['vNotifyName'];
            if (!$dtExeDateTime || $dtExeDateTime == "0000-00-00 00:00:00") {
                continue;
            }
            $db_notify_time[$vNotifyName] = date("Y-m-d H:i:s", strtotime($dtExeDateTime));
        }
        require_once (APPPATH . 'third_party/cronjob/CronIncludes.php');
        foreach ((array) $db_time_data as $n_key => $n_val) {
            try {
                $event_function = $n_key;
                $start_date_time = ($n_val['start_date'] != "") ? date("Y-m-d H:i:s", strtotime($n_val['start_date'])) : "";
                $end_date_time = ( $n_val['end_date'] != "") ? date("Y-m-d H:i:s", strtotime($n_val['end_date'])) : "";
                $curr_date_time = date("Y-m-d H:i:s");
                $cron_format = $n_val['cron_format'];
                if ($end_date_time != "" && $curr_date_time >= $end_date_time) {
                    continue;
                }
                if ($start_date_time != "" && ($curr_date_time < $start_date_time)) {
                    continue;
                }
                $cron = Cron\CronExpression::factory($cron_format);
                if ($db_notify_time[$event_function] != "") {
                    $cron_date_time = $cron->getNextRunDate($db_notify_time[$event_function])->format('Y-m-d H:i:s');
                    $first_run = false;
                } else {
                    $cron_date_time = $cron->getPreviousRunDate()->format('Y-m-d H:i:s');
                    $first_run = true;
                }
                if ($first_run) {
                    if ($cron_date_time <= $curr_date_time) {
                        $output_arr = $this->notifySubmit($event_function);
                    }
                } else {
                    if ($cron_date_time <= $curr_date_time) {
                        $output_arr = $this->notifySubmit($event_function);
                    }
                }
            } catch (Exception $e) {
                $e->getMessage();
            }
        }
        return $output_arr;
    }

    /**
     * notifySubmit method is used to submit time based notifications.
     * @param string $event_name event_name is execute specific notifications.
     * @return array $output_arr returns output data records array.
     */
    function notifySubmit($event_name = '')
    {
        $db_time_data = $this->getEventMasterData("sem.vEventFunction = '" . $event_name . "' AND sem.eTriggerType = 'Time'");
        $output_arr = array();
        if (!is_array($db_time_data) || count($db_time_data) == 0) {
            return $output_arr;
        }
        $output_arr = $this->fireNotification($event_name);
        if (is_array($output_arr) && array_key_exists("settings", $output_arr)) {
            $this->insertTimeBasedRespose($event_name, $output_arr);
        }
        return $output_arr;
    }

    /**
     * insertTimeBasedRespose method is used to insert time based notification flow.
     * @param string $event_name event_name is insert entry for specific notifications.
     * @param array $output_arr output_arr is array of output of notification flow.
     * @return bool $res returns notification inserted id.
     */
    public function insertTimeBasedRespose($event_name = '', $output_arr = array())
    {
        $insert_arr['vNotifyName'] = $event_name;
        $insert_arr['eNotifyType'] = "Time";
        $insert_arr['eOperation'] = "";
        $insert_arr['tOutputJSON'] = (is_array($output_arr)) ? json_encode($output_arr) : "";
        $insert_arr['vSuccess'] = ($output_arr['settings']['success']) ? $output_arr['settings']['success'] : 0;
        $insert_arr['tMessage'] = ($output_arr['settings']['message']) ? $output_arr['settings']['message'] : '';
        $insert_arr['dtAddDateTime'] = date("Y-m-d H:i:s");
        $insert_arr['dtExeDateTime'] = date("Y-m-d H:i:s");
        if ($this->db->_error_found === true) {
            $insert_arr['eStatus'] = "DBError";
        } else {
            $insert_arr['eStatus'] = "Executed";
        }
        $res = $this->notify_schedule->insert($insert_arr);
        return $res;
    }

    /**
     * updateOperBasedRespose method is used to update operation based notification flow.
     * @param integer $notify_schedule_id notify_schedule_id is update specific notifications.
     * @param array $output_arr output_arr is array of output of notification flow.
     * @return bool $res returns TRUE or FALSE.
     */
    public function updateOperBasedRespose($notify_schedule_id = '', $output_arr = array())
    {
        $update_arr = array();
        $update_arr['tOutputJSON'] = (is_array($output_arr)) ? json_encode($output_arr) : "";
        $update_arr['vSuccess'] = ($output_arr['settings']['success']) ? $output_arr['settings']['success'] : 0;
        $update_arr['tMessage'] = ($output_arr['settings']['message']) ? $output_arr['settings']['message'] : '';
        $update_arr['dtExeDateTime'] = date("Y-m-d H:i:s");
        if ($this->db->_error_found === true) {
            $update_arr['eStatus'] = "DBError";
        } else {
            $update_arr['eStatus'] = "Executed";
        }
        $res = $this->notify_schedule->update($update_arr, $notify_schedule_id);
        return $res;
    }

    /**
     * fireNotification method is used to call notification flow.
     * @param string $event_name event_name is execute specific notifications.
     * @param array $params params is array of inputs to notification flow.
     * @return array $output_arr returns output data records array.
     */
    public function fireNotification($event_name = '', $params = array(), $debug = FALSE)
    {
        $this->load->module("notification/" . $event_name);
        if ($debug == TRUE) {
            //checking for notift controller
            if (!is_object($this->$event_name)) {
                show_error('Notification code not found. Please save settings or update code.', 400);
            }
            //setup for debugger
            if (!is_null($this->input->get_post("ns_debug")) || !is_null($this->input->get_post("ns_ctrls"))) {
                $this->notifyresponse->ns_log_file = $this->input->get_post("ns_log");
                $debug_cache_dir = $this->config->item('api_debugger_path');
                if (!is_dir($debug_cache_dir)) {
                    $this->general->createFolder($debug_cache_dir);
                }
                if (!empty($this->notifyresponse->ns_log_file) && is_file($debug_cache_dir . $this->notifyresponse->ns_log_file)) {
                    $_log_params = @file_get_contents($debug_cache_dir . $this->notifyresponse->ns_log_file);
                    $_log_params = unserialize($_log_params);
                    if (is_array($_log_params) && count($_log_params) > 0) {
                        $this->notifyresponse->ns_debug_params = $_log_params['debug'];
                        $next_flow = $_log_params['next_flow'];
                        if (method_exists($this->$event_name, $next_flow)) {
                            if (!empty($_log_params['end_loop'])) {
                                $this->notifyresponse->pushDebugParams($_log_params['end_loop'], array(), $_log_params['params'], $next_flow, $_log_params['start_loop'], $_log_params['end_loop']);
                            }
                            $responce_arr = $this->$event_name->$next_flow($_log_params['params']);
                            $this->notifyresponse->sendNSResponse($responce_arr, $this->notifyresponse->ns_debug_params);
                        } else {
                            show_error('Notification debugger having some problem to detect next flow. Please try again.', 400);
                        }
                    }
                }
                if (!$this->_debug_log) {
                    $this->notifyresponse->ns_log_file = md5("debug_" . date("YmdHis") . "_" . rand(1000, 9999));
                }
                $this->_debug_called = TRUE;
            }
        }
        $output_arr = $this->$event_name->handler($params);
        return $output_arr;
    }
}
