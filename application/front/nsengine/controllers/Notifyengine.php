<?php

/**
 * Description of NS Engine Controller
 * 
 * @module NS Engine
 * 
 * @class notifyengine.php
 * 
 * @path application\front\nsengine\controllers\notifyengine.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class NotifyEngine extends HB_Controller {

    public $event_details;
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $inner_loop;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->event_details = array();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->inner_loop = array();

        $this->load->model('notify');
        $this->load->library('notifyresponse');
    }

    /**
     * notifyHandler method is used to get individual webservice list.
     * @param string $func_name func_name is the webservice name.
     * @param array $params_fwd params_fwd is the array of params to process.
     * @return array $responce_arr returns webservice response. (either json or array)
     */
    function notifyHandler($func_name = '', $params_fwd = array()) {
        $event_details = $this->notify->loadEventDetails($func_name);
        $event_details['event_flow'] = $this->jsonDecodeEventSettings($event_details['event_flow']);
        $this->event_details[$func_name] = $event_details;
        if (!$event_details) {
            show_error('Oh god you should not try to check the site!', 404);
        } else {
            //validate all the input params
            $input_params = is_array($params_fwd) ? $params_fwd : array();
            $output_array = array();
            //get the first flow details
            $event_flow_id = $event_details['event_master']['iEventFlowId'];
            $responce_arr = $this->handleFlowRecursively($func_name, $event_flow_id, $input_params);
            return $responce_arr;
        }
    }

    /**
     * handleFlowRecursively method is used to process different flows in webservices.
     * @param string $func_name func_name is the webservice name.
     * @param integer $event_flow_id event_flow_id is the current flow id.
     * @param array $input_params input_params are the array of input to webservice flow.
     * @return array $responce_arr returns webservice response array records.
     */
    function handleFlowRecursively($func_name = '', $event_flow_id = '', $input_params = array(), $ns_loop_name = '', $ns_loop_keys = array()) {
        $flow_details = $this->getFlowDetails($event_flow_id, $this->event_details[$func_name]['event_flow']);
        $this->notify->event_details = $this->event_details[$func_name];
        $has_next_flow = false;
        if ($flow_details) {
            $has_next_flow = true;
        }
        if ($has_next_flow) {
            $flow_type = $flow_details['eFlowType'];
            $flow_output_param = strtolower($flow_details['vFlowLabel']);
            $flow_query_details = $flow_details['tSettingsJSON'];
            $next_flow_key = 'iChildTrueFlowId';
            switch ($flow_type) {
                case 'Query':
                    $output_arr = $this->notify->execute_query_by_settings($flow_query_details, $input_params, $this->settings_params);
                    $input_params[$flow_output_param] = $output_arr["data"];
                    if ($flow_query_details['query']['max_rec'] == 1) {
                        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $output_arr["data"]);
                        if (count($this->inner_loop) == 0) {
                            $this->single_keys[] = $flow_output_param;
                            $this->notifyresponse->makeUniqueParams($this->single_keys);
                        }
                    } else {
                        if (count($this->inner_loop) == 0) {
                            $this->multiple_keys[] = $flow_output_param;
                            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
                        }
                    }
                    break;
                case 'NotifyEmail':
                    $output_arr = $this->notify->send_mail_notification($flow_query_details, $input_params);
                    $input_params[$flow_output_param] = $output_arr['success'];
                    break;
                case 'PushNotify':
                    $output_arr = $this->notify->send_push_notification($flow_query_details, $input_params);
                    $input_params[$flow_output_param] = $output_arr['success'];
                    break;
                case 'SMS':
                    $output_arr = $this->notify->send_sms_notification($flow_query_details, $input_params);
                    $input_params[$flow_output_param] = $output_arr['success'];
                    break;
                case 'DesktopNotify':
                    $output_arr = $this->notify->send_desktop_notification($flow_query_details, $input_params);
                    $input_params[$flow_output_param] = $output_arr['success'];
                    break;
                case 'Function':
                    $function_name = $flow_query_details['function']['function_name'];
                    $return_type = $flow_query_details['function']['return_type'];
                    $response_type = $flow_query_details['function']['response_type'];
                    $overwrite_flow = $flow_query_details['function']['overwrite_flow'];

                    if (method_exists($this->general, $function_name)) {
                        $result_arr['data'] = $this->general->$function_name($input_params);
                    }

                    if ($return_type == "single") {
                        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);
                        if (count($this->inner_loop) > 0) {
                            if ($response_type == "addparams") {
                                $input_params[$flow_output_param] = $this->notifyresponse->assignFunctionResponse($result_arr);
                            } elseif ($response_type == "mergeparams") {
                                if ($overwrite_flow == $ws_loop_name) {
                                    $input_params = $this->notifyresponse->assignAppendRecord($input_params, $result_arr['data'][0]);
                                } else {
                                    $input_params[$overwrite_flow] = $this->notifyresponse->assignAppendRecord($input_params[$overwrite_flow][0], $result_arr['data'][0]);
                                }
                            } elseif ($response_type == "replaceparams") {
                                if ($overwrite_flow == $ws_loop_name) {
                                    $input_params = $this->notifyresponse->unsetAppendRecord($input_params, $result_arr['data'][0], $ws_loop_keys);
                                } else {
                                    $input_params[$overwrite_flow] = $this->notifyresponse->assignFunctionResponse($result_arr);
                                }
                            }
                        } else {
                            if ($response_type == "addparams") {
                                $input_params[$flow_output_param] = $this->notifyresponse->assignFunctionResponse($result_arr);
                                $this->single_keys[] = $flow_output_param;
                                $this->notifyresponse->makeUniqueParams($this->single_keys);
                            } elseif ($response_type == "mergeparams") {
                                $input_params[$overwrite_flow] = $this->notifyresponse->assignAppendRecord($input_params[$overwrite_flow][0], $result_arr['data'][0]);
                            } elseif ($response_type == "replaceparams") {
                                $input_params[$overwrite_flow] = $this->notifyresponse->assignFunctionResponse($result_arr);
                            }
                        }
                    } else {
                        $input_params = $this->notifyresponse->assignSingleRecord($input_params, $result_arr["data"]);
                        $input_params[$flow_output_param] = $this->notifyresponse->assignFunctionResponse($result_arr);
                        if (count($this->inner_loop) == 0) {
                            $this->multiple_keys[] = $flow_output_param;
                            $this->notifyresponse->makeUniqueParams($this->multiple_keys);
                        }
                    }
                    break;
                case 'Condition':
                    $condition_result = $this->notify->execute_condition_flow($flow_query_details, $input_params);
                    if (!$condition_result['success']) {
                        $next_flow_key = 'iChildFalseFlowId';
                    }
                    break;
                case 'StartLoop':
                case 'StartLoop':
                    $loop_name = $flow_query_details['loop']['loop_name'];
                    $this->inner_loop[] = $loop_name;
                    $loop_array = $input_params[$loop_name];
                    $loop_loc_array = &$input_params[$loop_name];

                    if (is_array($loop_array) && count($loop_array) > 0) {
                        for ($i = 0; $i < count($loop_array); $i++) {
                            $loop_input_params = $loop_array[$i] + $input_params;
                            unset($loop_input_params[$loop_name]);
                            $response = $this->handleFlowRecursively($func_name, $flow_details[$next_flow_key], $loop_input_params, $loop_name, array_keys($loop_array[$i]));
                            $loop_loc_array[$i] = $this->notifyresponse->filterLoopParams($response, $loop_array[$i], $loop_input_params);
                        }
                    }

                    $loop_name = $flow_query_details['loop']['loop_name'];
                    $loop_key = array_search($loop_name, $this->inner_loop);
                    unset($this->inner_loop[$loop_key]);
                    $this->inner_loop = array_values($this->inner_loop);

                    //get next flow detail
                    $flow_details = $this->getFlowDetails($flow_query_details['loop']['end_id'], $this->event_details[$func_name]['event_flow']);
                    break;
                case 'EndLoop':
                    return $input_params;
                    break;
                case 'Finish':
                    //make the php array to output as json
                    $output_array = $func_array = array();
                    $output_array['settings'] = (is_array($flow_query_details['setting_fields'])) ? array_merge($this->settings_params, $flow_query_details['setting_fields']) : $this->settings_params;
                    $output_array['settings']['fields'] = (is_array($flow_query_details['output_fields'])) ? array_merge($this->output_params, $flow_query_details['output_fields']) : $this->output_params;
                    $output_array['data'] = $input_params;

                    $func_array['function']['name'] = $func_name;
                    $func_array['function']['messages'] = $this->event_details[$func_name]['ws_messages'];
                    $func_array['function']['output_keys'] = $flow_query_details['output_keys'];
                    $func_array['function']['output_alias'] = $flow_query_details['output_alias'];
                    $func_array['function']['inner_keys'] = $flow_query_details['inner_keys'];
                    $func_array['function']['single_keys'] = $this->single_keys;
                    $func_array['function']['multiple_keys'] = $this->multiple_keys;
                    $func_array['function']['custom_keys'] = $this->custom_keys;

                    $responce_arr = $this->notifyresponse->outputResponse($output_array, $func_array);

                    $has_next_flow = FALSE;
                    break;
            }
            if ($has_next_flow) {
                return $this->handleFlowRecursively($func_name, $flow_details[$next_flow_key], $input_params, $ns_loop_name, $ns_loop_keys);
            } else {
                return $responce_arr;
            }
        }
    }

    /**
     * getFlowDetails method is used to process different flows in webservices.
     * @param integer $flow_id flow_id is the specific flow id.
     * @param array $flow_array flow_array is the total flows for webservice.
     * @return array $ret_arr returns specific flow array data.
     */
    function getFlowDetails($flow_id = 0, $flow_array = array()) {
        for ($i = 0; $i < count($flow_array); $i++) {
            if ($flow_array[$i]['iEventFlowId'] == $flow_id) {
                $ret_arr = $flow_array[$i];
                return $ret_arr;
            }
        }
        return false;
    }

    /**
     * jsonDecodeEventSettings method is used to decode webservice settings.
     * @param array $data_arr data_arr is the webservice settings details.
     * @param array $flag flag is to differ assoc array of normal array.
     * @return array $ret_arr returns webservice settings array.
     */
    function jsonDecodeEventSettings($data_arr = array(), $flag = false) {
        $ret_arr = array();
        if (is_array($data_arr) && count($data_arr) > 0) {
            foreach ((array) $data_arr as $fd_key => $fd_val) {
                $tSettingsJSON = ($flag == true) ? json_decode($fd_val[0]['tSettingsJSON'], true) : json_decode($fd_val['tSettingsJSON'], true);
                $tSettingsArr = $this->recursiveStripSlashesArr($tSettingsJSON);
                if ($flag == true) {
                    $data_arr[$fd_key][0]['tSettingsJSON'] = $tSettingsArr;
                } else {
                    $data_arr[$fd_key]['tSettingsJSON'] = $tSettingsArr;
                }
            }
            $ret_arr = $data_arr;
        }
        return $ret_arr;
    }

    /**
     * recursiveStripSlashesArr method is used to recursively strip the slashes after decoding.
     * @param array $data_arr data_arr is the webservice settings details.
     * @return array $ret_arr returns webservice settings array.
     */
    function recursiveStripSlashesArr($data_arr = array()) {
        $ret_arr = array();
        foreach ((array) $data_arr as $key => $val) {
            if (is_array($val)) {
                $ret_arr[$key] = $this->recursiveStripSlashesArr($val);
            } else {
                $ret_arr[$key] = stripslashes($val);
            }
        }
        return $ret_arr;
    }

}
