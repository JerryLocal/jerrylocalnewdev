<%$this->css->add_css("ns_validate.css")%>
<%$this->css->css_src()%>
<%$this->js->add_js("jquery.validate.js","crypto-md5.js","application_ns.js")%>

<script type="text/javascript">
        var api_key = '<%$this->config->item("API_KEY")%>';
        var api_secret = '<%$this->config->item("API_SECRET")%>';     
        var ns_base = "";
</script>

<h1>All Available Notifications</h1>
<%foreach name="j" from=$all_methods key=key item=item%>    
    <div class="method-name">
        <%$smarty.foreach.j.iteration%>. <%$item.title%>
    </div>
    <div class="inputparams">
        <form action="<%$this->config->item('site_url')%>NS/<%$key%>?ns_debug=1" method="post" class="ns">
            <p>
                <button>Execute</button>
            </p>
        </form>
        <pre class="code"></pre>
    </div>
<%/foreach%>