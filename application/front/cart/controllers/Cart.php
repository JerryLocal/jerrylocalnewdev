<?php

/**
 * Description of Career Controller
 *
 * @module Career
 *
 * @class cart.php
 *
 * @path application\front\content\controllers\cart.php
 *
 * @author Dhananjay singh
 *
 * @date 20.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cart extends HB_Controller
{
    
    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    private $response = array();
    public function __construct() {
        parent::__construct();
        $this->load->model('hbmodel');
        $this->cart->product_name_rules = "\w \-\.\:'`=";
        $this->response['settings'] = array('success' => 1, 'message' => '');
        $this->response['data'] = array();
    }
    
    /**
     * add method is used to add product item into cart
     */
    public function add() {
        $this->native_cart();
        
        //$this->response['data'] =  $this->cart->contents();
        $this->_response();
    }
    
    /**
     *   native_cart private method
     *   @see system library cart
     *   @param void
     *   @return session_id alphanumeric
     */
    private function native_cart() {
        $product = $this->validate_product();
        $price = 0.0;
        if ($product['mp_sale_price'] > 0) $price = $product['mp_sale_price'];
        else $price = $product['mp_regular_price'];
        
        $data = array('id' => $product['mp_mst_products_id'],'shipping_charges' => $product['mp_shipping_charge'],'free_shipping'=>$product['mp_free_shipping'] ,'qty' => $this->input->post('qty'), 'price' => floatval($price), 'name' => $product['mp_title'], 'options' => array(), 'image' => $product['mp_default_img'],'store_id'=>$product['mp_store_id'],'store_name'=>$product['msd_store_name']);
        if ($this->input->post('itemsize') != '') {
            if (count($product['tpo_option_value'])) {
                if (in_array($this->input->post('itemsize'), $product['tpo_option_value'])) {
                    $data['options'][$product['tpo_category_option_value']] = $this->input->post('itemsize');
                }
            }
        } 
        else {
            if (count($product['tpo_option_value'])) {
                $this->response['settings']['success'] = 2;
                $this->response['settings']['message'] = 'This Product has option';
                $this->response['settings']['continue_url'] = $this->general->CleanUrl('P', $product['mp_mst_products_id']);
                $this->_response();
            }
        }
        
        $this->response['settings']['message'] = 'Item Added Into Your Cart.';
        
        if (isset($data['options']) && count($data['options']) > 0) {
            $rowid = md5($data['id'] . serialize($data['options']));
        } 
        else {
            
            // No options were submitted so we simply MD5 the product ID.
            // Technically, we don't need to MD5 the ID in this case, but it makes
            // sense to standardize the format of array indexes for both conditions
            $rowid = md5($data['id']);
        }
        
        $item = $this->cart->get_item($rowid);
        if ($item == FALSE) {
            $this->cart->insert($data);
            $this->response['settings']['message'] = 'Product is added into your cart.';
        } 
        else {
            $data = array(
               'rowid' => $rowid,
               'qty'   => $this->input->post('qty')
            );
            $this->cart->update($data);            
            $this->response['settings']['message'] = 'Product is already in your cart.';
        }
    }
    
    /**
     *   validate_product private method
     */
    private function validate_product() {
        $params = array();
        $ws_name = 'Validate_product_for_cart';
        $params['iProductId']   = $this->input->post('pid');
        $params['iQuantity']    = $this->input->post('qty');
        $product_detail_arr     = $this->hbmodel->getData($ws_name, $params);
        if ($product_detail_arr['settings']['success'] == 0) {
            $this->response['settings']['success'] = 0;
            $this->response['settings']['message'] = $product_detail_arr['settings']['message'];
            $this->_response();
        }
        if ($product_detail_arr['data'][0]['tpo_option_value'] != '') $product_detail_arr['data'][0]['tpo_option_value'] = explode(",", $product_detail_arr['data'][0]['tpo_option_value']);
        else $product_detail_arr['data'][0]['tpo_option_value'] = array();
        return $product_detail_arr['data'][0];
    }
    
    /**
     *
     */
    private function _response() {
        echo json_encode($this->response);
        exit;
    }
    
    public function cart_info() {
        echo $this->load->view('cart_information', array(), true);
        exit;
    }



    public function update_cart(){
        $product = $this->validate_product();
        $data = array(
           'rowid' => $this->input->post("rowid"),
           'qty'   => $this->input->post('qty')
        );
        $this->cart->update($data);
        $this->response['settings']['message'] = 'The quantity of item <b>'.$product['mp_title'].' ...</b> has been changed to '.$this->input->post('qty');
        $this->_response();
    }

    public function removeitem_from_cart(){
        $this->cart->remove($this->input->post("rowid"));
        $this->response['settings']['message'] = 'The item <b>'.$this->input->post('pname').' ...</b> has been removed from cart..';
        $this->_response();
    }
}
