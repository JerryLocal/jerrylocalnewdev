<div class="block-title">
   <div class="box-left"></div>
   <div class="box-right">
      <p class="amount">
         <a href="<?php echo base_url('checkout/myshoppingcart');?>"><?php echo count($this->cart->contents(true)); ?> <span class="item-amount">item - </span></a> 
         <span class="subtotal"><span class="price"><?php echo $this->config->item('CURRENCY_SYMBOL').number_format($this->cart->total(),0); ?></span>
         </span> <i class="fa fa-angle-down"></i> 
      </p>
   </div>
</div>
<?php if( $this->cart->total_items() > 0) { ?>
<div class="block-content">
   <div class="inner">
      <p class="block-subtitle">Recently added item(s)</p>
      <ol id="cart-sidebar" class="mini-products-list">
        <?php foreach ($this->cart->contents(true) as $key => $cartitem) { ?>        
          <li class="item last odd">
              <a href="<?php echo $this->general->CleanUrl('P', $cartitem['id']);?>" title="<?php echo $cartitem['name'];?>" class="product-image">
                  <img src="<?php echo $cartitem['image'];?>" alt="<?php echo $cartitem['name'];?>">
              </a>
             <!--  <a href="#" title="Remove This Item" class="btn-remove">
                  Remove This Item
              </a> -->
              <div class="product-details">
                  <p class="product-name">
                      <a title="<?php echo $cartitem['name']?>" href="<?php echo $this->general->CleanUrl('P', $cartitem['id']);?>">
                        <span class='text-danger'><?php echo $cartitem['qty'];?></span> x <?php echo $cartitem['name']?>
                      </a>
                      <?php if(isset($cartitem['options']['SIZE'])){?>
                      <br>
                      <i class='text-primary help-text'>Size:  <?=$cartitem['options']['SIZE']?></i>
                      <?php }?>
                  </p>
                  <span class="price">
                      <?php echo $this->config->item('CURRENCY_SYMBOL')." ".number_format($cartitem['price'],2);?>
                  </span>
                  <!-- <strong>
                      <a title="Decrement" class="flycart-qty-btn flycart-qty-change flycart-qty-change-left" href="#">
                          <i class="fa fa-minus">
                          </i>
                      </a>
                      <input class="input-text qty flycart-qty" type="text" id="flycart_sidebar_1894" value="<?php echo $cartitem['qty'];?>"/>
                      <a title="Increment" class="flycart-qty-btn flycart-qty-change flycart-qty-change-right" href="#">
                          <i class="fa fa-plus">
                          </i>
                      </a>
                  </strong> -->
              </div>
          </li>

          <?php }?>
      </ol>
      <div class="summary">
         <p class="subtotal"> <span class="label">Subtotal:</span> <span class="price"><?php echo $this->config->item('CURRENCY_SYMBOL')." ".number_format($this->cart->total(),2);?></span> </p>
      </div>
      <div class="actions">
         <div class="a-inner"> <a class="btn-mycart" href="<?php echo base_url('checkout/myshoppingcart');?>" title="View my cart"> view my cart </a>
         <!-- <a href="#" title="Checkout" class="btn-checkout"> Checkout </a>  -->
      </div>
      </div>
   </div>
</div>
<?php }?>

