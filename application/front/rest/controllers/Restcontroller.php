<?php
/**
 * Description of Rest Controller
 * 
 * @module Rest
 * 
 * @class restcontroller.php
 * 
 * @path application\front\rest\controllers\restcontroller.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class RestController extends HB_Controller
{

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('rest');
        $this->load->library('wsresponse');
    }

    /**
     * create_token method is used to create token for webservices security.
     */
    public function create_token()
    {
        $this->load->library('wschecker');
        $remote_addr = $this->wschecker->getHTTPRealIPAddr();
        $user_agent = $this->wschecker->getHTTPUserAgent();
        $prepare_str = $remote_addr . "@@" . $user_agent . "@@" . time();
        $ws_token = hash("SHA256", $this->wschecker->encrypt($prepare_str));

        $inser_arr['vWSToken'] = $ws_token;
        $inser_arr['vIPAddress'] = $remote_addr;
        $inser_arr['vUserAgent'] = $user_agent;
        $inser_arr['dLastAccess'] = date("Y-m-d H:i:s");
        $res = $this->rest->insertToken($inser_arr);
        if ($res) {
            $settings_arr['success'] = 1;
            $settings_arr['message'] = "Token generated successfully..!";
            $data_arr['ws_token'] = $ws_token;
        } else {
            $settings_arr['success'] = 0;
            $settings_arr['message'] = "Token generation failed..!";
            $data_arr = array();
        }
        $responce_arr['settings'] = $settings_arr;
        $responce_arr['data'] = $data_arr;
        $this->wsresponse->sendWSResponse($responce_arr);
    }

    /**
     * create_token method is used to create token for webservices security.
     */
    public function inactive_token()
    {
        $this->load->library('wschecker');
        $remote_addr = $this->wschecker->getHTTPRealIPAddr();
        $user_agent = $this->wschecker->getHTTPUserAgent();
        $ws_token = trim($this->input->get_post("ws_token"));

        if (empty($ws_token)) {
            $settings_arr['success'] = 0;
            $settings_arr['message'] = "Please send token to inactivate.!";
        } else {
            $update_arr['eStatus'] = "Inactive";
            $extra_cond = "vWSToken = '" . $ws_token . "'";
            $res = $this->rest->updateToken($update_arr, $extra_cond);
            if ($res) {
                $settings_arr['success'] = 1;
                $settings_arr['message'] = "Token inactivated successfully..!";
            } else {
                $settings_arr['success'] = 0;
                $settings_arr['message'] = "Token inactivation failed..!";
            }
        }
        $responce_arr['settings'] = $settings_arr;
        $responce_arr['data'] = array();
        $this->wsresponse->sendWSResponse($responce_arr);
    }

    /**
     * execute_notify_schedule method is used to get push notifications full data.
     */
    public function execute_notify_schedule()
    {
        $limit = $this->config->item('WS_PUSH_LIMIT');
        try {
            $data = array();

            $extra_cond = $this->db->protect("mpn.eStatus") . " = " . $this->db->escape("Pending");
            $data_arr = $this->rest->getPushNotify($extra_cond, "", "", "", $limit);

            if (!is_array($data_arr) || count($data_arr) == 0) {
                throw new Exception("There are no notification found to execute.");
            }

            foreach ($data_arr as $key => $val) {
                $notify_arr = array();
                $vars_arr = json_decode($val['tVarsJSON'], true);
                if (is_array($vars_arr) && count($vars_arr) > 0) {
                    foreach ($vars_arr as $vk => $vv) {
                        if ($vv['key'] != "" && $vv['send'] == "Yes") {
                            $notify_arr['others'][$vv['key']] = $vv['value'];
                        }
                    }
                }
                $notify_arr['mode'] = $this->config->item('PUSH_NOTIFY_SENDING_MODE');
                $notify_arr['message'] = $val['tMessage'];
                $notify_arr['title'] = $val['vTitle'];
                $notify_arr['badge'] = intval($val['vBadge']);
                $notify_arr['sound'] = $val['vSound'];
                $notify_arr['code'] = $val['eNotifyCode'];
                $notify_arr['id'] = $val['vUniqueId'];

                $success = $this->general->pushTestNotification($val['vDeviceId'], $notify_arr);

                $update_arr['tSendJSON'] = $this->general->getPushNotifyOutput("body");
                $update_arr['dtExeDateTime'] = date("Y-m-d H:i:s");
                if ($success) {
                    $update_arr['eStatus'] = 'Executed';
                } else {
                    $update_arr['eStatus'] = 'Failed';
                }

                $res = $this->rest->updatePushNotify($update_arr, $val['iPushNotifyId']);

                $send_arr = $notify_arr;
                $send_arr['device_id'] = $val['vDeviceId'];
                $data[] = $send_arr;
            }

            $settings_arr['success'] = 1;
            $settings_arr['count'] = count($data_arr);
            $settings_arr['message'] = "Push notification(s) send successfully";
        } catch (Exception $e) {
            $settings_arr['success'] = 0;
            $settings_arr['message'] = $e->getMessage();
        }
        $responce_arr['settings'] = $settings_arr;
        $responce_arr['data'] = $data;
        $this->wsresponse->sendWSResponse($responce_arr);
    }

    /**
     * get_push_notification method is used to get push notifications full data.
     */
    public function get_push_notification()
    {
        $this->load->library('wschecker');
        $this->load->model('rest');
        $get_arr = is_array($this->input->get(null)) ? $this->input->get(null) : array();
        $post_arr = is_array($this->input->post(null)) ? $this->input->post(null) : array();
        $post_params = array_merge($get_arr, $post_arr);

        try {
            if ($this->config->item('WS_RESPONSE_ENCRYPTION') == "Y") {
                $post_params = $this->wschecker->decrypt_params($post_params);
            }
            $verify_res = $this->wschecker->verify_webservice($post_params);
            if ($verify_res['success'] != "1") {
                $this->wschecker->show_error_code($verify_res);
            }

            $unique_id = $post_params["unique_id"];
            $data = $temp = array();
            if (empty($unique_id)) {
                throw new Exception("Please send unique id for this notification");
            }
            $extra_cond = $this->db->protect("mpn.vUniqueId") . " = " . $this->db->escape($unique_id);
            $data_arr = $this->rest->getPushNotify($extra_cond);

            if (!is_array($data_arr) || count($data_arr) == 0) {
                throw new Exception("Data not found for this unique id");
            }
            $variables = json_decode($data_arr[0]['tVarsJSON'], true);
            if (is_array($variables) && count($variables) > 0) {
                foreach ($variables as $vk => $vv) {
                    if ($vv['key'] != "") {
                        $temp[$vv['key']] = $vv['value'];
                    }
                }
            }
            $temp['code'] = $data_arr[0]['eNotifyCode'];
            $temp['title'] = $data_arr[0]['vTitle'];
            $temp['body'] = $data_arr[0]['tMessage'];

            $data[0] = $temp;
            $settings_arr['success'] = 1;
            $settings_arr['message'] = "Push notification data found";
        } catch (Exception $e) {
            $settings_arr['success'] = 0;
            $settings_arr['message'] = $e->getMessage();
        }
        $responce_arr['settings'] = $settings_arr;
        $responce_arr['data'] = $data;
        $this->wsresponse->sendWSResponse($responce_arr);
    }

    /**
     * image_resize method is used to resize image for different sizes.
     */
    public function image_resize()
    {
        $url = $this->input->get('pic');
        $width = $this->input->get('width');
        $height = $this->input->get('height');
        $bgcolor = $this->input->get('color');
        $bgcolor = (trim($bgcolor) == "") ? "FFFFFF" : $bgcolor;
        $props = array(
            'picture' => $url,
            'resize_width' => $width,
            'resize_height' => $height,
            'bg_color' => $bgcolor
        );

        $this->load->library('Image_resize', $props);
    }
}
