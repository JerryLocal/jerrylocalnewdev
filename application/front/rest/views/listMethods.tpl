<%$this->css->add_css("ws_validate.css")%>
<%$this->css->css_src()%>
<%$this->js->add_js("jquery.validate.js","crypto-md5.js","application.js")%>

<script type="text/javascript">
        var api_key = '<%$this->config->item('API_KEY')%>';
        var api_secret = '<%$this->config->item('API_SECRET')%>';     
        var ws_base = "";
</script>

<h1>All Available Methods</h1>
<%foreach name="j" from=$all_methods key=key item=item%>    
    <div class="method-name">
        <%$smarty.foreach.j.iteration%> .
        <%$item[0].vWSName%>
    </div>
    <div class="inputparams">
        <form action="<%$this->config->item('site_url')%>WS/<%$item[0].vWSFunction%>" method="post" class="ws">
            <%section name="i" loop=$item%>
                <%if $item[i].iWSParamId neq ''%>
                <p>
                    <label for="<%$item[i].vParameter%>"><%$item[i].vParameter%></label>
                    <input id="<%$item[i].vParameter%>" name="<%$item[i].vParameter%>" value="">          
                </p>
                <%/if%>
            <%/section%>  
            <p>
                <button>Submit</button>
            </p>
        </form>
        <pre class="code"></pre>
    </div>
<%/foreach%>    


<%javascript%>
$('form.ws').validate({
    <%*$validate_str*%>
});
<%/javascript%>
