<?php

/**
 * Description of Rest Model
 * 
 * @module Rest
 * 
 * @class rest.php
 * 
 * @path application\front\rest\models\rest.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
class Rest extends CI_Model
{

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insertToken method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    public function insertToken($data = array())
    {
        $this->db->insert("mod_ws_token", $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    /**
     * updateToken method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function updateToken($data = array(), $where = '')
    {
        if (is_numeric($where)) {
            $this->db->where("iWSTokenId", $where);
        } else if ($where) {
            $this->db->where($where, FALSE, FALSE);
        } else {
            return false;
        }
        $res = $this->db->update("mod_ws_token", $data);
        return $res;
    }

    /**
     * getToken method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @return array $data_arr returns data records array.
     */
    public function getToken($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No")
    {
        if (is_array($fields)) {
            $this->listing->addSelectFields($fields);
        } else if ($fields != "") {
            $this->db->select($fields);
        } else {
            $this->db->select("mwt.*");
        }
        $this->db->from("mod_ws_token AS mwt");

        if (is_array($extra_cond) && count($extra_cond) > 0) {
            $this->listing->addWhereFields($extra_cond);
        } else if (is_numeric($extra_cond)) {
            $this->db->where("mwt.iWSTokenId", intval($extra_cond));
        } else if ($extra_cond) {
            $this->db->where($extra_cond, FALSE, FALSE);
        }

        if ($group_by != "") {
            $this->db->group_by($group_by);
        }
        if ($order_by != "") {
            $this->db->order_by($order_by);
        } else {
            $this->db->order_by("mwt.dLastAccess", "DESC");
        }
        if ($limit != "") {
            list($offset, $limit) = @explode(",", $limit);
            $this->db->limit($offset, $limit);
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }

    /**
     * insertPushNotify method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    function insertPushNotify($insert_arr = array())
    {
        $this->db->insert("mod_push_notifications", $insert_arr);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    /**
     * updatePushNotify method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function updatePushNotify($data = array(), $where = '')
    {
        if (is_numeric($where)) {
            $this->db->where("iPushNotifyId", $where);
        } else if ($where) {
            $this->db->where($where, FALSE, FALSE);
        } else {
            return false;
        }
        $res = $this->db->update("mod_push_notifications", $data);
        return $res;
    }

    /**
     * getPushNotify method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @return array $data_arr returns data records array.
     */
    public function getPushNotify($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No")
    {
        if (is_array($fields)) {
            $this->listing->addSelectFields($fields);
        } else if ($fields != "") {
            $this->db->select($fields);
        } else {
            $this->db->select("mpn.*");
        }
        $this->db->from("mod_push_notifications AS mpn");

        if (is_array($extra_cond) && count($extra_cond) > 0) {
            $this->listing->addWhereFields($extra_cond);
        } else if (is_numeric($extra_cond)) {
            $this->db->where("mpn.iPushNotifyId", intval($extra_cond));
        } else if ($extra_cond) {
            $this->db->where($extra_cond, FALSE, FALSE);
        }

        if ($group_by != "") {
            $this->db->group_by($group_by);
        }
        if ($order_by != "") {
            $this->db->order_by($order_by);
        }
        if ($limit != "") {
            list($offset, $limit) = @explode(",", $limit);
            $this->db->limit($offset, $limit);
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }
}
