<?php
            
/**
 * Description of Category Management Extended Controller
 * 
 * @module Extended Category Management
 * 
 * @class HBC_Categorymanagement.php
 * 
 * @path application\admin\categorymanagement\controllers\HBC_Categorymanagement.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 20.10.2015
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
 
Class HBC_Categorymanagement extends Categorymanagement {
        function __construct() {
    parent::__construct();
}

function check_category(){
	$return_arr=array();
	
	$this->load->model("model_categorymanagement");
	//$this->load->model("hbc_model_categorymanagement");
	$postArr=$this->input->post();
	if(intval($postArr['category_id']) > 0){
		$condition= "iMstCategoriesId = '".$postArr['category_id']."'";
		
		$categoryArr=$this->model_categorymanagement->get_category_details('iParentId',$condition);
		if(is_array($categoryArr) && count($categoryArr) > 0){
			$parent_id=intval($categoryArr[0]['iParentId']);
			if($parent_id > 0){
				$return_arr['is_allowed_commission']="0";
			}
			else{
				$return_arr['is_allowed_commission']="1";
			}
		}
	}else{
			$return_arr['is_allowed_commission']="0";
	}
	echo json_encode($return_arr);
}
}
