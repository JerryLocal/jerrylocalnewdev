<?php

/**
 * Description of Category Management Extended Model
 * 
 * @module Extended Category Management
 * 
 * @class HBC_Model_categorymanagement.php
 * 
 * @path application\admin\categorymanagement\models\HBC_Model_categorymanagement.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 20.10.2015
 */
   
Class HBC_Model_categorymanagement extends Model_categorymanagement {
        function __construct() {
    parent::__construct();
}

function get_category_details($fields,$condition){
    	$this->db->select($fields,false);
        $this->db->from("mst_categories");
        $this->db->where($condition,null,false);
        $category_details= $this->db->get()->result_array();
		return $category_details;
}
}
