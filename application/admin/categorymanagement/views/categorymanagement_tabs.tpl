<ul class="nav nav-tabs">
    <li <%if $module_name eq "categorymanagement"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('CATEGORYMANAGEMENT_CATEGORY_MANAGEMENT')%>" 
            <%if $module_name eq "categorymanagement"%> 
                href="javascript://"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('categorymanagement/categorymanagement/add')%>|mode|<%$mod_enc_mode['Update']%>|id|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('CATEGORYMANAGEMENT_CATEGORY_MANAGEMENT')%>
        </a>
    </li>
    <li <%if $module_name eq "categoryoptions"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('CATEGORYMANAGEMENT_CATEGORY_OPTIONS')%> <%$this->lang->line('GENERIC_LIST')%>" 
            <%if $module_name eq "categoryoptions"%> 
                href="javascript://"
            <%elseif $module_name eq "categorymanagement"%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('categoryoptions/categoryoptions/index')%>|parMod|<%$this->general->getAdminEncodeURL('categorymanagement')%>|parID|<%$this->general->getAdminEncodeURL($data['iMstCategoriesId'])%>"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('categoryoptions/categoryoptions/index')%>|parMod|<%$this->general->getAdminEncodeURL('categorymanagement')%>|parID|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('CATEGORYMANAGEMENT_CATEGORY_OPTIONS')%>
        </a>
    </li>
</ul>            