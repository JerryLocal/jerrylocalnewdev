<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('CATEGORYMANAGEMENT_CATEGORY_MANAGEMENT')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','CATEGORYMANAGEMENT_CATEGORY_MANAGEMENT')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-tab-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="categorymanagement" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
        <div id="ad_form_outertab" class="module-navigation-tabs">
            <%if $tabing_allow eq true%>
                <%if $mode eq "Update"%>
                    <%include file="categorymanagement_tabs.tpl" %>
                <%/if%>
            <%/if%>
        </div>
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing top-frm-block-spacing">
        <div id="categorymanagement" class="frm-view-block frm-stand-view">
            <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
            <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
            <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
            <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
            <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
            <input type="hidden" name="mc_product_count" id="mc_product_count" value="<%$data['mc_product_count']|@htmlentities%>"  class='ignore-valid ' />
            <div class="main-content-block" id="main_content_block">
                <div style="width:98%;" class="frm-block-layout pad-calc-container">
                    <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                        <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('CATEGORYMANAGEMENT_CATEGORY_MANAGEMENT')%></h4></div>
                        <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                            <div class="form-row row-fluid" id="cc_sh_mc_parent_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('CATEGORYMANAGEMENT_PARENT_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mc_parent_id'], $opt_arr['mc_parent_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mc_title">
                                <label class="form-label span3">
                                    <%$this->lang->line('CATEGORYMANAGEMENT_TITLE')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mc_title']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mc_commission">
                                <label class="form-label span3">
                                    <%$this->lang->line('CATEGORYMANAGEMENT_COMMISSION')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mc_commission']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mc_category_img">
                                <label class="form-label span3">
                                    <%$this->lang->line('CATEGORYMANAGEMENT_CATEGORY_IMG')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <%$img_html['mc_category_img']%>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mc_display_order">
                                <label class="form-label span3">
                                    <%$this->lang->line('CATEGORYMANAGEMENT_DISPLAY_ORDER')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mc_display_order']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mc_status">
                                <label class="form-label span3">
                                    <%$this->lang->line('CATEGORYMANAGEMENT_STATUS')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mc_status'], $opt_arr['mc_status'])%></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>        
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js("admin/custom/category_managment_extended.js")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
