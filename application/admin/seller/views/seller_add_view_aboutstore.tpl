<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="4"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="aboutstore"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="aboutstore" />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_about_store">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_ABOUT_STORE')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  frm-editor-layout ">
            <strong><%$data['msd_about_store']%></strong>
        </div>
    </div>
</div>