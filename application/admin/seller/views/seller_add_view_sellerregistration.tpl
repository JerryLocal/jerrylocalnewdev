<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="sellerregistration"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="sellerregistration" />
<input type="hidden" name="ma_email" id="ma_email" value="<%$data['ma_email']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_admin_id" id="msd_admin_id" value="<%$data['msd_admin_id']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_store_rate_avg" id="msd_store_rate_avg" value="<%$data['msd_store_rate_avg']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_store_total_review" id="msd_store_total_review" value="<%$data['msd_store_total_review']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_date" id="msd_date" value="<%$data['msd_date']%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
<input type="hidden" name="ma_status" id="ma_status" value="<%$data['ma_status']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma_last_access" id="ma_last_access" value="<%$data['ma_last_access']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_modify_date" id="msd_modify_date" value="<%$data['msd_modify_date']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma_group_id" id="ma_group_id" value="<%$data['ma_group_id']|@htmlentities%>"  class='ignore-valid ' />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_code">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_STORE_CODE')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_store_code']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_name">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_STORE_NAME')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_store_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_logo">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_STORE_LOGO')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <%$img_html['msd_store_logo']%>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_description">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_STORE_DESCRIPTION')%>
        </label> 
        <div class="form-right-div frm-elements-div  frm-editor-layout ">
            <strong><%$data['msd_store_description']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_ma_user_name">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_EMAIL')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['ma_user_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_ma_password">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_PASSWORD')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            *****
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_status">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_STORE_STATUS')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_status'], $opt_arr['msd_status'])%></strong>
        </div>
    </div>
</div>