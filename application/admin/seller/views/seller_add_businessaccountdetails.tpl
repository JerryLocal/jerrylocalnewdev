<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="businessaccountdetails"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="businessaccountdetails" />
<input type="hidden" name="msd_contact_name" id="msd_contact_name" value="<%$data['msd_contact_name']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_address2" id="msd_address2" value="<%$data['msd_address2']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma_phonenumber" id="ma_phonenumber" value="<%$data['ma_phonenumber']|@htmlentities%>"  class='ignore-valid ' />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_company_name">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_COMPANY_NAME')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_company_name']|@htmlentities%>" name="msd_company_name" id="msd_company_name" title="<%$this->lang->line('SELLER_COMPANY_NAME')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_company_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_ma_name">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_CONTACT_NAME')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['ma_name']|@htmlentities%>" name="ma_name" id="ma_name" title="<%$this->lang->line('SELLER_CONTACT_NAME')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='ma_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_contact_number">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_CONTACT_NUMBER')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_contact_number']|@htmlentities%>" name="msd_contact_number" id="msd_contact_number" title="<%$this->lang->line('SELLER_CONTACT_NUMBER')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_contact_numberErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_address1">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_STREET_NO__C47_NAME')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_address1']|@htmlentities%>" name="msd_address1" id="msd_address1" title="<%$this->lang->line('SELLER_STREET_NO__C47_NAME')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_address1Err'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_area">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_AREA_NAME')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_area']|@htmlentities%>" name="msd_area" id="msd_area" title="<%$this->lang->line('SELLER_AREA_NAME')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_areaErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_country_id">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_COUNTRY')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_country_id']%>
            <%$this->dropdown->display("msd_country_id","msd_country_id","  title='<%$this->lang->line('SELLER_COUNTRY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SELLER_COUNTRY')%>'  ", "|||", "", $opt_selected,"msd_country_id")%>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_country_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_state_id">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_STATE')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_state_id']%>
            <%$this->dropdown->display("msd_state_id","msd_state_id","  title='<%$this->lang->line('SELLER_STATE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SELLER_STATE')%>'  ", "|||", "", $opt_selected,"msd_state_id")%><span style='display:none;margin-left:15%' id='ajax_loader_msd_state_id'><i class='fa fa-refresh fa-spin-light fa-2x fa-fw'></i></span>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_state_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_city_id">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_CITY')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_city_id']%>
            <%$this->dropdown->display("msd_city_id","msd_city_id","  title='<%$this->lang->line('SELLER_CITY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SELLER_CITY')%>'  ", "|||", "", $opt_selected,"msd_city_id")%><span style='display:none;margin-left:15%' id='ajax_loader_msd_city_id'><i class='fa fa-refresh fa-spin-light fa-2x fa-fw'></i></span>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_city_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_pin_code">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_PIN_CODE')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_pin_code']|@htmlentities%>" name="msd_pin_code" id="msd_pin_code" title="<%$this->lang->line('SELLER_PIN_CODE')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_pin_codeErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_website_url">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_COMPANY_WEBSITE_URL')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_website_url']|@htmlentities%>" name="msd_website_url" id="msd_website_url" title="<%$this->lang->line('SELLER_COMPANY_WEBSITE_URL')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_website_urlErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_mst_categories_id">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_PRIMARY_CATEGORY')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_mst_categories_id']%>
            <%$this->dropdown->display("msd_mst_categories_id","msd_mst_categories_id","  title='<%$this->lang->line('SELLER_PRIMARY_CATEGORY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SELLER_PRIMARY_CATEGORY')%>'  ", "|||", "", $opt_selected,"msd_mst_categories_id")%>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_mst_categories_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_have_online_store">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_DO_YOU_SELL_PRODUCT_ONLINE_ALREADY_C63')%> <em>*</em> 
        </label> 
        <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%>  ">
            <%assign var="opt_selected" value=$data['msd_have_online_store']%>
            <%assign var="combo_arr" value=$opt_arr["msd_have_online_store"]%>
            <%if $combo_arr|@is_array && $combo_arr|@count gt 0 %>
                <%foreach name=i from=$combo_arr item=v key=k%>
                    <input type="radio" value="<%$k%>" name="msd_have_online_store" id="msd_have_online_store_<%$k%>" title="<%$v%>" <%if $opt_selected eq  $k %> checked=true <%/if%>  class='regular-radio'  />
                    <label for="msd_have_online_store_<%$k%>" class="frm-horizon-row frm-column-layout">&nbsp;</label>
                    <label for="msd_have_online_store_<%$k%>" class="frm-horizon-row frm-column-layout"><%$v%></label>&nbsp;&nbsp;
                <%/foreach%>
            <%/if%>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_have_online_storeErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_url">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_STORE_URL')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_store_url']|@htmlentities%>" name="msd_store_url" id="msd_store_url" title="<%$this->lang->line('SELLER_STORE_URL')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_urlErr'></label></div>
    </div>
</div>
<div align="right" class="btn-below-spacing">
    <div style="float:left;">
        <input type="button" value="Prev" name="ctrlprev_1_1_2" class="btn" onclick="return getLoadAdminTab('1','1','1','sellerregistration')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Prev" name="ctrlsaveprev_1_1_2" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','1','sellerregistration')"/>
    </div>
    <div style="float:right;">
        <input type="submit" value="Save" name="ctrlsave_1_1_2" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','2','businessaccountdetails')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Next" name="ctrlsavenext_1_1_2" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','3','bankdetails')" />
        &nbsp;&nbsp;
        <input type="button" value="Next" name="ctrlnext_1_1_2" class="btn" onclick="return getNextAdminTab('1','1','2','3','bankdetails')" />
    </div>
    <div class="clear"></div>
</div>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initAdminTabRenderJSScript(eleObj){
Project.modules.seller.initEvents("businessaccountdetails");
callGoogleMapEvents();
}
<%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
<%foreach name=i from=$auto_arr item=v key=k%>
    if($("#<%$k%>").is("select")){
    $("#<%$k%>").ajaxChosen({
    dataType: "json",
    type: "POST",
    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
    },{
    loadingImg: admin_image_url+"chosen-loading.gif"
    });
}
<%/foreach%>
<%/if%>        
<%/javascript%>