<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="aboutstore"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="aboutstore" />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_about_store">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_ABOUT_STORE')%> <em>*</em> 
        </label> 
        <div class="form-right-div  frm-editor-layout  ">
            <textarea name="msd_about_store" id="msd_about_store" title="<%$this->lang->line('SELLER_ABOUT_STORE')%>"  style='width:80%;'  class='frm-size-medium frm-editor-small'  ><%$data['msd_about_store']%></textarea>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_about_storeErr'></label></div>
    </div>
</div>
<div align="right" class="btn-below-spacing">
    <div style="float:left;">
        <input type="button" value="Prev" name="ctrlprev_1_1_4" class="btn" onclick="return getLoadAdminTab('1','1','3','bankdetails')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Prev" name="ctrlsaveprev_1_1_4" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','3','bankdetails')"/>
    </div>
    <div style="float:right;">
        <input type="submit" value="Save" name="ctrlsave_1_1_4" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','4','aboutstore')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Next" name="ctrlsavenext_1_1_4" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','5','returnpolicy')" />
        &nbsp;&nbsp;
        <input type="button" value="Next" name="ctrlnext_1_1_4" class="btn" onclick="return getNextAdminTab('1','1','4','5','returnpolicy')" />
    </div>
    <div class="clear"></div>
</div>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initAdminTabRenderJSScript(eleObj){
Project.modules.seller.initEvents("aboutstore");
callGoogleMapEvents();
}
<%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
<%foreach name=i from=$auto_arr item=v key=k%>
    if($("#<%$k%>").is("select")){
    $("#<%$k%>").ajaxChosen({
    dataType: "json",
    type: "POST",
    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
    },{
    loadingImg: admin_image_url+"chosen-loading.gif"
    });
}
<%/foreach%>
<%/if%>        
<%/javascript%>