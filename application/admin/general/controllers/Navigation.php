<?php
/**
 * Description of Navigation Log Controller
 * 
 * @module NavigationLog
 * 
 * @class navigation.php
 * 
 * @path application\admin\general\controllers\navigation.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Navigation extends HB_Controller
{

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('filter');
        $this->load->model('model_navigation');
        $this->module_name = "navigation";
        $this->module_hurl = $this->config->item('admin_url') . "#general/navigation/";
        $this->module_nurl = $this->config->item('admin_url') . "general/navigation/";
        $this->_request_params();
    }

    /**
     * _request_params method is used to set post/get/request params.
     */
    public function _request_params()
    {
        $this->get_arr = is_array($this->input->get(null)) ? $this->input->get(null) : array();
        $this->post_arr = is_array($this->input->post(null)) ? $this->input->post(null) : array();
        $this->params_arr = array_merge($this->get_arr, $this->post_arr);
        return $this->params_arr;
    }

    /**
     * index method is used to intialize navigation log listing page.
     */
    public function index()
    {
        $dec_loc_module = $this->session->userdata('iAdminId') . "_Navigation";
        $enc_loc_module = json_encode($dec_loc_module);
        $column_config = $module_column_data['column_array'];
        $type = $this->params_arr['type'];
        $range = $this->params_arr['range'];
        $action = $this->params_arr['action'];
        $user_id = $this->params_arr['user_id'];
        $data_log_arr['today'] = "Today";
        $data_log_arr['yesterday'] = "Yesterday";
        $data_log_arr['last7days'] = "Last 7 Days";
        $data_log_arr['last30days'] = "Last 30 Days";
        $data_log_arr['thismonth'] = "This Month";
        $data_log_arr['lastmonth'] = "Last Month";
        $data_log_arr['last3months'] = "Last 3 Months";
        $data_log_arr['all'] = "All";
        $data_flush_arr['onehour'] = "the past hour";
        $data_flush_arr['today'] = "the today";
        $data_flush_arr['yesterday'] = "the past day";
        $data_flush_arr['last7days'] = "the past week";
        $data_flush_arr['last30days'] = "the last 30 days";
        $data_flush_arr['thismonth'] = "the present month";
        $data_flush_arr['lastmonth'] = "the last month";
        $data_flush_arr['last3months'] = "the last 3 months";
        $data_flush_arr['all'] = "the beginning of time";

        $time_stamp_pro = $this->db->protect("dTimeStamp");
        switch ($range) {
            case 'yesterday':
                $time_stamp = date("Y-m-d", strtotime('-1 day'));
                $extra_cond = " AND " . $this->db->date_format($time_stamp_pro) . " = " . $this->db->escape($time_stamp);
                break;
            case 'last7days':
                $start_date = date("Y-m-d", strtotime('-7 day'));
                $end_date = date("Y-m-d");
                $extra_cond = " AND " . $this->db->date_format($time_stamp_pro) . " BETWEEN " . $this->db->escape($start_date) . " AND " . $this->db->escape($end_date);
                break;
            case 'last30days':
                $start_date = date("Y-m-d", strtotime('-30 day'));
                $end_date = date("Y-m-d");
                $extra_cond = " AND " . $this->db->date_format($time_stamp_pro) . " BETWEEN " . $this->db->escape($start_date) . " AND " . $this->db->escape($end_date);
                break;
            case 'thismonth':
                $start_date = date("Y-m-01", mktime(0, 0, 0, date('m'), date('d'), date('Y')));
                $end_date = date("Y-m-d");
                $extra_cond = " AND " . $this->db->date_format($time_stamp_pro) . " BETWEEN " . $this->db->escape($start_date) . " AND " . $this->db->escape($end_date);
                break;
            case 'lastmonth':
                $start_date = date("Y-m-01", mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')));
                $end_date = date("Y-m-d", mktime(0, 0, 0, date('m'), 0, date('Y')));
                $extra_cond = " AND " . $this->db->date_format($time_stamp_pro) . " BETWEEN " . $this->db->escape($start_date) . " AND " . $this->db->escape($end_date);
                break;
            case 'last3months':
                $start_date = date("Y-m-d", strtotime('-3 month'));
                $end_date = date("Y-m-d");
                $extra_cond = " AND " . $this->db->date_format($time_stamp_pro) . " BETWEEN " . $this->db->escape($start_date) . " AND " . $this->db->escape($end_date);
                break;
            case 'all':
                break;
            default :
                $time_stamp = date("Y-m-d");
                $extra_cond = " AND " . $this->db->date_format($time_stamp_pro) . " = " . $this->db->escape($time_stamp);
                break;
        }

        if ($this->session->userdata('vUserName') != $this->config->item("ADMIN_USER_NAME")) {
            $admin_user = "No";
        } else {
            $admin_user = "Yes";
            $user_data = $this->model_navigation->getUserData();
        }

        $db_action_arr = array(
            "Viewed" => "Viewed",
            "Added" => "Added",
            "Modified" => "Modified",
            "Deleted" => "Deleted",
        );

        if (in_array($action, array_keys($db_action_arr))) {
            $extra_cond .= " AND " . $this->db->protect("eNavigAction") . " = " . $this->db->escape($action);
        }

        $user_id = ($user_id > 0) ? $user_id : $this->session->userdata('iAdminId');
        $extra_condition = $this->db->protect("iAdminId") . " = " . $this->db->escape($user_id) . " " . $extra_cond;
        $db_navig_data = $this->model_navigation->getData($extra_condition, '', '', '', 1000);

        $render_arr = array(
            'module_name' => $this->module_name,
            'column_config' => $column_config,
            'enc_loc_module' => $enc_loc_module,
            'status_array' => $status_array,
            'module_nurl' => $this->module_nurl,
            'module_hurl' => $this->module_hurl,
            'db_navig_data' => $db_navig_data,
            'dataLogArr' => $data_log_arr,
            'data_flush_arr' => $data_flush_arr,
            'dataActionArr' => $db_action_arr,
            'range' => $range,
            'action' => $action,
            'admin_user' => $admin_user,
            'user_data' => $user_data,
            'user_id' => $user_id
        );
        $this->smarty->assign($render_arr);
        $this->loadView("navigation_index");
    }

    /**
     * flush_record method is used to flush navigation log records.
     */
    function flush_record()
    {
        $flush = $this->params_arr['flush'];
        $user_id = $this->params_arr['user_id'];
        $time_stamp_pro = $this->db->protect("dTimeStamp");
        if ($this->session->userdata('iAdminId') > 0) {
            switch ($flush) {
                case 'onehour' :
                    $time_stamp_flush = date("Y-m-d H:i:s", strtotime('-1 hours'));
                    $curr_date = date("Y-m-d H:i:s");
                    $extra_cond_flush = " AND dTimeStamp < " . $this->db->escape($curr_date) . " AND dTimeStamp > " . $this->db->escape($time_stamp_flush);
                    break;
                case 'yesterday':
                    $time_stamp_flush = date("Y-m-d", strtotime('-1 day'));
                    $extra_cond_flush = " AND " . $this->db->date_format($time_stamp_pro) . " = " . $this->db->escape($time_stamp_flush);
                    break;
                case 'last7days':
                    $start_date_flush = date("Y-m-d", strtotime('-7 day'));
                    $end_date_flush = date("Y-m-d");
                    $extra_cond_flush = " AND " . $this->db->date_format($time_stamp_pro) . " BETWEEN " . $this->db->escape($start_date_flush) . " AND " . $this->db->escape($end_date_flush);
                    break;
                case 'last30days':
                    $start_date_flush = date("Y-m-d", strtotime('-30 day'));
                    $end_date_flush = date("Y-m-d");
                    $extra_cond_flush = " AND " . $this->db->date_format($time_stamp_pro) . " BETWEEN " . $this->db->escape($start_date_flush) . " AND " . $this->db->escape($end_date_flush);
                    break;
                case 'thismonth':
                    $start_date_flush = date("Y-m-01", mktime(0, 0, 0, date('m'), date('d'), date('Y')));
                    $end_date_flush = date("Y-m-d");
                    $extra_cond_flush = " AND " . $this->db->date_format($time_stamp_pro) . " BETWEEN " . $this->db->escape($start_date_flush) . " AND " . $this->db->escape($end_date_flush);
                    break;
                case 'lastmonth':
                    $start_date_flush = date("Y-m-01", mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')));
                    $end_date_flush = date("Y-m-d", mktime(0, 0, 0, date('m'), 0, date('Y')));
                    $extra_cond_flush = " AND " . $this->db->date_format($time_stamp_pro) . " BETWEEN " . $this->db->escape($start_date_flush) . " AND " . $this->db->escape($end_date_flush);
                    break;
                case 'last3months':
                    $start_date_flush = date("Y-m-d", strtotime('-3 month'));
                    $end_date_flush = date("Y-m-d");
                    $extra_cond_flush = " AND " . $this->db->date_format($time_stamp_pro) . " BETWEEN " . $this->db->escape($start_date_flush) . " AND " . $this->db->escape($end_date_flush);
                    break;
                case 'all':
                    break;
            }
            $user_id = ($user_id > 0) ? $user_id : $this->session->userdata('iAdminId');
            $extra_cond = $this->db->protect("iAdminId") . " = " . $this->db->escape($user_id) . " " . $extra_cond_flush;
            $sql_flush_data = $this->model_navigation->delete($extra_cond, 1000);
        }
        $this->index();
    }

    /**
     * error_log method is used to display query log records.
     */
    public function error_log()
    {
        $page = $this->params_arr['page'];
        $log_file = $this->config->item('query_error_path') . $page . ".html";
        if (@file_exists($log_file)) {
            $file_found = true;
        } else {
            $file_found = false;
        }
        $render_arr = array("error_log_file" => $log_file, "file_found" => $file_found);
        $this->smarty->assign($render_arr);
        $this->loadView("error_index");
    }

    /**
     * query_log method is used to display query log records.
     */
    public function query_log()
    {
        $log_file = $this->config->item('admin_query_log_path') . $this->session->userdata('queryLogFile') . ".html";
        $count_log_files = $this->general->getQueryLogFiles();
        if (@file_exists($log_file)) {
            $file_found = true;
        } else {
            $file_found = false;
        }
        $render_arr = array("query_log_file" => $log_file, "file_found" => $file_found, "log_files" => $count_log_files);
        $this->smarty->assign($render_arr);
        $this->loadView("query_index");
    }

    /**
     * query_log_page method is used to display query log different page records.
     */
    public function query_log_page()
    {
        $paging = $this->params_arr['type'];
        $page = $this->params_arr['page'];
        $admin_log_path = $this->config->item('admin_query_log_path');
        $count_log_files = $this->general->getQueryLogFiles();
        $log_curr_file = $count_log_files[$page - 1];
        $query_log_file = $admin_log_path . $log_curr_file;
        if (@file_exists($query_log_file) && $log_curr_file != "") {
            $file_found = true;
        }
        $render_arr = array(
            "paging" => $paging,
            "page" => $page,
            "log_curr_file" => $log_curr_file,
            "log_files" => $count_log_files,
            "query_log_file" => $query_log_file,
            "file_found" => $file_found
        );
        $this->smarty->assign($render_arr);
        $this->loadView("query_index");
    }

    /**
     * clear_query_log method is used to clear query log pages.
     */
    public function clear_query_log()
    {
        $flush_type = $this->params_arr['flush_type'];
        $flush_page = (intval($this->params_arr['flush_page'])) ? $this->params_arr['flush_page'] : 1;
        $admin_log_path = $this->config->item('admin_query_log_path');
        if ($this->session->userdata('iAdminId') > 0) {
            $total_files = $this->general->getQueryLogFiles();
            switch ($flush_type) {
                case 'All':
                    for ($i = 0; $i < count($total_files); $i++) {
                        $log_file = $total_files[$i];
                        $log_file_path = $admin_log_path . DS . $log_file;
                        if (@file_exists($log_file_path) && $log_file != "") {
                            $res = @unlink($log_file_path);
                        }
                    }
                    break;
                case 'First':
                    for ($i = 0, $j = 1; $i < count($total_files); $i++, $j++) {
                        if ($flush_page < $j) {
                            break;
                        }
                        $log_file = $total_files[$i];
                        $log_file_path = $admin_log_path . DS . $log_file;
                        if (@file_exists($log_file_path) && $log_file != "") {
                            $res = @unlink($log_file_path);
                        }
                    }
                    break;
                case 'Last':
                    for ($i = count($total_files) - 1, $j = 1; $i > 0; $i--, $j++) {
                        if ($flush_page < $j) {
                            break;
                        }
                        $log_file = $total_files[$i];
                        $log_file_path = $admin_log_path . DS . $log_file;
                        if (@file_exists($log_file_path) && $log_file != "") {
                            $res = @unlink($log_file_path);
                        }
                    }
                    break;
            }
        } else {
            $res = 0;
        }
        $msg = ($res) ? "Query log deleted successfully." : "Error in clearing query log.";
        $ret_arr['success'] = ($res) ? 1 : 0;
        $ret_arr['message'] = $msg;
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    /**
     * clear_query_cache method is used to clear query caching pages.
     */
    public function clear_query_cache()
    {
        if ($this->session->userdata('iAdminId') > 0) {
            $query_cache_path = $this->config->item('query_cache_path');
            if (is_dir($query_cache_path) && $this->db->_cache_init()) {
                $this->db->CACHE->delete_all();
                $res = 1;
            } else {
                $res = 0;
            }
            if ($this->config->item("ADMIN_ASSETS_APPCACHE") == 'Y') {
                $cisource_appcache_path = $this->config->item("site_path") . "cisource.appcache";
                $cache_contents = @file($cisource_appcache_path);
                $cache_contents[1] = '# version ' . date("Y-m-d h:i:A") . ' v' . $this->config->item("PROJECT_LATEST_VERSION") . '
';
                @file_put_contents($cisource_appcache_path, implode($cache_contents));
                $res = 1;
            }
            if ($this->config->item('GRID_SEARCH_PREFERENCES') == "Y") {
                $res = 1;
            }
        } else {
            $res = 0;
        }

        $msg = ($res) ? "Cache deleted successfully." : "Error in clearing cache.";
        $ret_arr['success'] = $res;
        $ret_arr['message'] = $msg;
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    public function change_preferences()
    {
        try {
            if ($this->session->userdata('vUserName') != $this->config->item("ADMIN_USER_NAME")) {
                throw new Exception("You are not authorised person to apply these changes");
            }
            $type = $this->params_arr['type'];
            $value = $this->params_arr['value'];
            $update_field = $update_value = "";

            if ($type == "menu") {
                $update_field = 'NAVIGATION_BAR';
                $update_value = ($value == "Left") ? 'Left' : 'Top';
            } else {
                $update_field = 'ADMIN_THEME_SETTINGS';
            }
            $this->load->model('general/systemsettings');
            $extra_cond = $this->db->protect("vName") . " = " . $this->db->escape($update_field);
            $db_settings_data = $this->systemsettings->getSettingsMaster('vValue', $extra_cond);

            if (!is_array($db_settings_data) || count($db_settings_data) == 0) {
                throw new Exception("Please upgrade project version to apply these changes");
            }

            if ($update_field == "NAVIGATION_BAR") {
                if ($db_settings_data[0]['vValue'] == $update_value) {
                    throw new Exception("Please change menu and go for apply changes");
                }
            } else if ($update_field == "ADMIN_THEME_SETTINGS") {
                $theme_str = $db_settings_data[0]['vValue'];
                $theme_arr = explode("@", $theme_str);
                $patte_arr = explode("||", $theme_arr[1]);
                switch ($type) {
                    case 'theme':
                        if (in_array($value, array("metronic", "cit"))) {
                            $theme_part_1 = $value;
                        } else {
                            $theme_part_1 = 'supr';
                        }
                        if ($theme_part_1 == $theme_arr[0]) {
                            throw new Exception("Please change theme and go for apply changes");
                        }
                        $update_value = $theme_part_1;
                        break;
                    case 'header':
                        if ($theme_arr[0] == "supr") {
                            $update_value = $theme_arr[0] . "@" . $value . "||" . $patte_arr[1] . "||" . $patte_arr[2] . "@" . $theme_arr[2];
                        }
                        break;
                    case 'sidebar':
                        if ($theme_arr[0] == "supr") {
                            $update_value = $theme_arr[0] . "@" . $patte_arr[0] . "||" . $value . "||" . $patte_arr[2] . "@" . $theme_arr[2];
                        }
                        break;
                    case 'body':
                        if ($theme_arr[0] == "supr") {
                            $update_value = $theme_arr[0] . "@" . $patte_arr[0] . "||" . $patte_arr[1] . "||" . $value . "@" . $theme_arr[2];
                        }
                        break;
                    case 'color':
                        if (in_array($theme_arr[0], array("metronic", "cit"))) {
                            $update_value = $theme_arr[0] . "@" . $value . "@" . $theme_arr[2];
                        }
                        break;
                    case 'custom':
                        $update_value = $theme_arr[0] . "@" . $theme_arr[1] . "@" . $value;
                        break;
                }
            }

            if ($update_field == '' || $update_value == '') {
                throw new Exception("Faliure in apply these changes");
            }
            $update_arr = array();
            $update_arr['vValue'] = $update_value;
            $update_cond = $this->db->protect("vName") . " = " . $this->db->escape($update_field);
            $res = $this->systemsettings->updateSetting($update_arr, $update_cond);

            if (!$res) {
                throw new Exception("Faliure in apply these changes");
            }

            $res = 1;
            $msg = "Changes applied successfully..!";
        } catch (Exception $e) {
            $res = 0;
            $msg = $e->getMessage();
        }
        $ret_arr['success'] = $res;
        $ret_arr['message'] = $msg;
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }
}
