<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('DASHBOARDCUSTOMERSTATE_DASHBOARD_CUSTOMER_STATE')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','DASHBOARDCUSTOMERSTATE_DASHBOARD_CUSTOMER_STATE')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="dashboardcustomerstate" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="dashboardcustomerstate" class="frm-elem-block frm-stand-view">
            <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                <div class="main-content-block" id="main_content_block">
                    <div style="width:98%" class="frm-block-layout pad-calc-container">
                        <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                            <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('DASHBOARDCUSTOMERSTATE_DASHBOARD_CUSTOMER_STATE')%></h4></div>
                            <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                <div class="form-row row-fluid" id="cc_sh_mc_first_name">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_FIRST_NAME')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mc_first_name']|@htmlentities%>" name="mc_first_name" id="mc_first_name" title="<%$this->lang->line('DASHBOARDCUSTOMERSTATE_FIRST_NAME')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_first_nameErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mc_last_name">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_LAST_NAME')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mc_last_name']|@htmlentities%>" name="mc_last_name" id="mc_last_name" title="<%$this->lang->line('DASHBOARDCUSTOMERSTATE_LAST_NAME')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_last_nameErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mc_email">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_EMAIL')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mc_email']|@htmlentities%>" name="mc_email" id="mc_email" title="<%$this->lang->line('DASHBOARDCUSTOMERSTATE_EMAIL')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_emailErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mc_user_name">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_USER_NAME')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mc_user_name']|@htmlentities%>" name="mc_user_name" id="mc_user_name" title="<%$this->lang->line('DASHBOARDCUSTOMERSTATE_USER_NAME')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_user_nameErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mc_password">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_PASSWORD')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mc_password']|@htmlentities%>" name="mc_password" id="mc_password" title="<%$this->lang->line('DASHBOARDCUSTOMERSTATE_PASSWORD')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_passwordErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mc_phone_no">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_PHONE_NO')%> <em>*</em> 
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" format="(999) 999-9999" value="<%$data['mc_phone_no']%>" name="mc_phone_no" id="mc_phone_no" title="<%$this->lang->line('DASHBOARDCUSTOMERSTATE_PHONE_NO')%>"  class='frm-phone-number frm-size-medium'  style='width:auto;' />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_phone_noErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mc_d_o_b">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_D_O_B')%> <em>*</em> 
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend   ">
                                        <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['mc_d_o_b'])%>" placeholder="" name="mc_d_o_b" id="mc_d_o_b" title="<%$this->lang->line('DASHBOARDCUSTOMERSTATE_D_O_B')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                        <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_d_o_bErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mc_gender">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_GENDER')%> <em>*</em> 
                                    </label> 
                                    <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%>  ">
                                        <%assign var="opt_selected" value=$data['mc_gender']%>
                                        <%assign var="combo_arr" value=$opt_arr["mc_gender"]%>
                                        <%if $combo_arr|@is_array && $combo_arr|@count gt 0 %>
                                            <%foreach name=i from=$combo_arr item=v key=k%>
                                                <input type="radio" value="<%$k%>" name="mc_gender" id="mc_gender_<%$k%>" title="<%$v%>" <%if $opt_selected eq  $k %> checked=true <%/if%>  class='regular-radio'  />
                                                <label for="mc_gender_<%$k%>" class="frm-horizon-row frm-column-layout">&nbsp;</label>
                                                <label for="mc_gender_<%$k%>" class="frm-horizon-row frm-column-layout"><%$v%></label>&nbsp;&nbsp;
                                            <%/foreach%>
                                        <%/if%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_genderErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mc_bank_name">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_BANK_NAME')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mc_bank_name']|@htmlentities%>" name="mc_bank_name" id="mc_bank_name" title="<%$this->lang->line('DASHBOARDCUSTOMERSTATE_BANK_NAME')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_bank_nameErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mc_bank_account_number">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_BANK_ACCOUNT_NUMBER')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mc_bank_account_number']%>
                                        <%$this->dropdown->display("mc_bank_account_number","mc_bank_account_number","  title='<%$this->lang->line('DASHBOARDCUSTOMERSTATE_BANK_ACCOUNT_NUMBER')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DASHBOARDCUSTOMERSTATE_BANK_ACCOUNT_NUMBER')%>'  ", "|||", "", $opt_selected,"mc_bank_account_number")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_bank_account_numberErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mc_t_registered_date">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_T_REGISTERED_DATE')%>
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend   ">
                                        <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['mc_t_registered_date'])%>" placeholder="" name="mc_t_registered_date" id="mc_t_registered_date" title="<%$this->lang->line('DASHBOARDCUSTOMERSTATE_T_REGISTERED_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                        <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_t_registered_dateErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mc_status">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDCUSTOMERSTATE_STATUS')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mc_status']%>
                                        <%$this->dropdown->display("mc_status","mc_status","  title='<%$this->lang->line('DASHBOARDCUSTOMERSTATE_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DASHBOARDCUSTOMERSTATE_STATUS')%>'  ", "|||", "", $opt_selected,"mc_status")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mc_statusErr'></label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%>">
                        <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                            <%assign var='rm_ctrl_directions' value=true%>
                        <%/if%>
                        <!-- Form Redirection Control Unit -->
                        <%if $controls_allow eq false || $rm_ctrl_directions eq true%>
                            <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" type="hidden" />
                        <%else%>
                            <div class='action-dir-align'>
                                <%if $prev_link_allow eq true%>
                                    <input value="Prev" id="ctrl_flow_prev" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Prev' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_prev">&nbsp;</label>
                                    <label for="ctrl_flow_prev" class="inline-elem-margin"><%$this->lang->line('GENERIC_PREV_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <%if $next_link_allow eq true || $mode eq 'Add'%>
                                    <input value="Next" id="ctrl_flow_next" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Next' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_next">&nbsp;</label>
                                    <label for="ctrl_flow_next" class="inline-elem-margin"><%$this->lang->line('GENERIC_NEXT_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <input value="List" id="ctrl_flow_list" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'List' %> checked=true <%/if%> />
                                <label for="ctrl_flow_list">&nbsp;</label>
                                <label for="ctrl_flow_list" class="inline-elem-margin"><%$this->lang->line('GENERIC_LIST_SHORT')%></label>&nbsp;&nbsp;
                                <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq '' || $ctrl_flow eq 'Stay' %> checked=true <%/if%> />
                                <label for="ctrl_flow_stay">&nbsp;</label>
                                <label for="ctrl_flow_stay" class="inline-elem-margin"><%$this->lang->line('GENERIC_STAY_SHORT')%></label>
                            </div>
                        <%/if%>
                        <!-- Form Action Control Unit -->
                        <%if $controls_allow eq false%>
                            <div class="clear">&nbsp;</div>
                        <%/if%>
                        <div class="action-btn-align" id="action_btn_container">
                            <%if $mode eq 'Update'%>
                                <%if $update_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_UPDATE')%>" name="ctrlupdate" type="submit" id="frmbtn_update" class="btn btn-info"/>&nbsp;&nbsp;
                                <%/if%>
                                <%if $delete_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_DELETE')%>" name="ctrldelete" type="button" id="frmbtn_delete" class="btn btn-danger" onclick="return deleteAdminRecordData('<%$enc_id%>', '<%$mod_enc_url.index%>','<%$mod_enc_url.inline_edit_action%>', '<%$extra_qstr%>', '<%$extra_hstr%>');" />&nbsp;&nbsp;
                                <%/if%>
                            <%else%>
                                <input value="<%$this->lang->line('GENERIC_SAVE')%>" name="ctrladd" type="submit" id="frmbtn_add" class="btn btn-info" />&nbsp;&nbsp;
                            <%/if%>
                            <%if $discard_allow eq true%>
                                <input value="<%$this->lang->line('GENERIC_DISCARD')%>" name="ctrldiscard" type="button" id="frmbtn_discard" class="btn" onclick="return loadAdminModuleListing('<%$mod_enc_url.index%>', '<%$extra_hstr%>')" />
                            <%/if%>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </form>
        </div>
    </div>
</div>
<%javascript%>    
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};        
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        <%foreach name=i from=$auto_arr item=v key=k%>
            if($("#<%$k%>").is("select")){
                $("#<%$k%>").ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                    },{
                    loadingImg: admin_image_url+"chosen-loading.gif"
                });
            }
        <%/foreach%>
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['buttons_arr'] = [];
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/dashboardcustomerstate_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.dashboardcustomerstate.callEvents();
<%/javascript%>