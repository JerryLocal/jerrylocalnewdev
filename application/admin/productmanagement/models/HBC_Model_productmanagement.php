<?php

/**
 * Description of Product Management Extended Model
 * 
 * @module Extended Product Management
 * 
 * @class HBC_Model_productmanagement.php
 * 
 * @path application\admin\productmanagement\models\HBC_Model_productmanagement.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 28.10.2015
 */
   
Class HBC_Model_productmanagement extends Model_productmanagement {
        function __construct() {
    parent::__construct();
}

function get_store_owner($fields,$condition){
    	$this->db->select($fields,false);
        $this->db->from("mst_store_detail");
        $this->db->where($condition,null,false);
        $store_details= $this->db->get()->result_array();
		return $store_details;
}
}
