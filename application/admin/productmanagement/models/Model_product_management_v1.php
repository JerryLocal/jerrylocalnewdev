<?php

/**
 * Description of Products Report Model
 *
 * @module Products Report
 *
 * @class model_product_management_v1.php
 *
 * @path application\admin\productmanagement\models\model_product_management_v1.php
 *
 * @author CIT Dev Team
 *
 * @date 12.01.2016
 */
class Model_product_management_v1 extends CI_Model {

    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $multi_lingual;
    public $physical_data_remove;
    //
    public $listing_data;
    public $rec_per_page;
    public $message;

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->table_name = "mst_products";
        $this->table_alias = "mp";
        $this->primary_key = "iMstProductsId";
        $this->primary_alias = "mp_mst_products_id";
        $this->physical_data_remove = "Yes";
        $this->grid_fields = array(
            "msd_store_name",
            "tpc_mst_categories_id",
            "mp_sku",
            "mp_title",
            "mp_status",
            "mp_low_avl_limit_notifcation",
            "mp_allow_max_purchase",
            "mp_wishlist_state",
            "mp_view_state",
            "mp_total_rate",
            "mc_title",
        );
        $this->join_tables = array(
            array(
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iAdminId",
                "rel_table_name" => "mst_products",
                "rel_table_alias" => "mp",
                "rel_field_name" => "iAdminId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "trn_product_categories",
                "table_alias" => "tpc",
                "field_name" => "iMstProductsId",
                "rel_table_name" => "mst_products",
                "rel_table_alias" => "mp",
                "rel_field_name" => "iMstProductsId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "iAdminId",
                "rel_table_name" => "mst_products",
                "rel_table_alias" => "mp",
                "rel_field_name" => "iAdminId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iMstProductsId",
                "rel_table_name" => "mst_products",
                "rel_table_alias" => "mp",
                "rel_field_name" => "iMstProductsId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mst_categories",
                "table_alias" => "mc",
                "field_name" => "iMstCategoriesId",
                "rel_table_name" => "trn_product_categories",
                "rel_table_alias" => "tpc",
                "rel_field_name" => "iMstCategoriesId",
                "join_type" => "left",
                "extra_condition" => "",
            )
        );
        $this->extra_cond = "IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)";
        $this->groupby_cond = array(
            "mp.iMstProductsId",
        );
        $this->having_cond = "";
        $this->orderby_cond = array();
        $this->unique_type = "OR";
        $this->unique_fields = array();
        $this->switchto_fields = array();
        $this->default_filters = array();
        $this->search_config = array();
        $this->relation_modules = array(
            "productimages" => array(
                "module" => "productimages",
                "folder" => "productimages",
                "rel_source" => "iMstProductsId",
                "rel_target" => "iMstProductsId",
                "extra_cond" => "",
                "popup" => "No",
            )
        );
        $this->deletion_modules = array(
            "productimages" => array(
                "module" => "productimages",
                "folder" => "productimages",
                "rel_source" => "iMstProductsId",
                "rel_target" => "iMstProductsId",
                "extra_cond" => "",
            )
        );
        $this->print_rec = "No";
        $this->multi_lingual = "No";

        $this->rec_per_page = $this->config->item('REC_LIMIT');
    }

    /**
     * insert method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    public function insert($data = array()) {
        $this->db->insert($this->table_name, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    /**
     * update method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while updating records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function update($data = array(), $where = '', $alias = "No", $join = "No") {
        if ($alias == "Yes") {
            if ($join == "Yes") {
                $join_tbls = $this->addJoinTables("NR");
            }
            if (trim($join_tbls) != '') {
                $set_cond = array();
                foreach ($data as $key => $val) {
                    $set_cond[] = $this->db->protect($key) . " = " . $this->db->escape($val);
                }
                if (is_numeric($where)) {
                    $extra_cond = " WHERE " . $this->db->protect($this->table_alias . "." . $this->primary_key) . " = " . $this->db->escape($where);
                } elseif ($where) {
                    $extra_cond = " WHERE " . $where;
                } else {
                    return FALSE;
                }
                $update_query = "UPDATE " . $this->db->protect($this->table_name) . " AS " . $this->db->protect($this->table_alias) . " " . $join_tbls . " SET " . @implode(", ", $set_cond) . " " . $extra_cond;
                $res = $this->db->query($update_query);
            } else {
                if (is_numeric($where)) {
                    $this->db->where($this->table_alias . "." . $this->primary_key, $where);
                } elseif ($where) {
                    $this->db->where($where, FALSE, FALSE);
                } else {
                    return FALSE;
                }
                $res = $this->db->update($this->table_name . " AS " . $this->table_alias, $data);
            }
        } else {
            if (is_numeric($where)) {
                $this->db->where($this->primary_key, $where);
            } elseif ($where) {
                $this->db->where($where, FALSE, FALSE);
            } else {
                return FALSE;
            }
            $res = $this->db->update($this->table_name, $data);
        }
        return $res;
    }

    /**
     * delete method is used to delete data records from the database table.
     * @param string $where where is the query condition for deletion.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while deleting records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function delete($where = "", $alias = "No", $join = "No") {
        if ($this->config->item('PHYSICAL_RECORD_DELETE') && $this->physical_data_remove == 'No') {
            if ($alias == "Yes") {
                if (is_array($join['joins']) && count($join['joins'])) {
                    $join_tbls = '';
                    if ($join['list'] == "Yes") {
                        $join_tbls = $this->addJoinTables("NR");
                    }
                    $join_tbls .= ' ' . $this->listing->addJoinTables($join['joins'], "NR");
                } elseif ($join == "Yes") {
                    $join_tbls = $this->addJoinTables("NR");
                }
                $data = $this->general->getPhysicalRecordUpdate($this->table_alias);
                if (trim($join_tbls) != '') {
                    $set_cond = array();
                    foreach ($data as $key => $val) {
                        $set_cond[] = $this->db->protect($key) . " = " . $this->db->escape($val);
                    }
                    if (is_numeric($where)) {
                        $extra_cond = " WHERE " . $this->db->protect($this->table_alias . "." . $this->primary_key) . " = " . $this->db->escape($where);
                    } elseif ($where) {
                        $extra_cond = " WHERE " . $where;
                    } else {
                        return FALSE;
                    }
                    $update_query = "UPDATE " . $this->db->protect($this->table_name) . " AS " . $this->db->protect($this->table_alias) . " " . $join_tbls . " SET " . @implode(", ", $set_cond) . " " . $extra_cond;
                    $res = $this->db->query($update_query);
                } else {
                    if (is_numeric($where)) {
                        $this->db->where($this->table_alias . "." . $this->primary_key, $where);
                    } elseif ($where) {
                        $this->db->where($where, FALSE, FALSE);
                    } else {
                        return FALSE;
                    }
                    $res = $this->db->update($this->table_name . " AS " . $this->table_alias, $data);
                }
            } else {
                if (is_numeric($where)) {
                    $this->db->where($this->primary_key, $where);
                } elseif ($where) {
                    $this->db->where($where, FALSE, FALSE);
                } else {
                    return FALSE;
                }
                $data = $this->general->getPhysicalRecordUpdate();
                $res = $this->db->update($this->table_name, $data);
            }
        } else {
            if ($alias == "Yes") {
                $del_query = "DELETE " . $this->db->protect($this->table_alias) . ".* FROM " . $this->db->protect($this->table_name) . " AS " . $this->db->protect($this->table_alias);
                if (is_array($join['joins']) && count($join['joins'])) {
                    if ($join['list'] == "Yes") {
                        $del_query .= $this->addJoinTables("NR");
                    }
                    $del_query .= ' ' . $this->listing->addJoinTables($join['joins'], "NR");
                } elseif ($join == "Yes") {
                    $del_query .= $this->addJoinTables("NR");
                }
                if (is_numeric($where)) {
                    $del_query .= " WHERE " . $this->db->protect($this->table_alias) . "." . $this->db->protect($this->primary_key) . " = " . $this->db->escape($where);
                } elseif ($where) {
                    $del_query .= " WHERE " . $where;
                } else {
                    return FALSE;
                }
                $res = $this->db->query($del_query);
            } else {
                if (is_numeric($where)) {
                    $this->db->where($this->primary_key, $where);
                } elseif ($where) {
                    $this->db->where($where, FALSE, FALSE);
                } else {
                    return FALSE;
                }
                $res = $this->db->delete($this->table_name);
            }
        }
        return $res;
    }

    /**
     * getData method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @param boolean $having_cond having cond is the query condition for getting conditional data.
     * @param boolean $list list is to differ listing fields or form fields.
     * @return array $data_arr returns data records array.
     */
    public function getData($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No", $having_cond = '', $list = FALSE) {
        if (is_array($fields)) {
            $this->listing->addSelectFields($fields);
        } elseif ($fields != "") {
            $this->db->select($fields);
        } elseif ($list == TRUE) {
            $this->db->select($this->table_alias . "." . $this->primary_key . " AS " . $this->primary_key);
            if ($this->primary_alias != "") {
                $this->db->select($this->table_alias . "." . $this->primary_key . " AS " . $this->primary_alias);
            }
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("tpc.iMstCategoriesId AS tpc_mst_categories_id");
            $this->db->select("mp.vSku AS mp_sku");
            $this->db->select("mp.vTitle AS mp_title");
            $this->db->select("mp.eStatus AS mp_status");
            $this->db->select("(SELECT SUM(msb.iProductQty) FROM `mst_sub_order` as msb where msb.iMstProductsId=" . $this->db->protect("mp.iMstProductsId") . ") AS mp_low_avl_limit_notifcation", FALSE);
            $this->db->select("(SELECT SUM(msb.fTotalCost) FROM `mst_sub_order` as msb where msb.iMstProductsId=" . $this->db->protect("mp.iMstProductsId") . ") AS mp_allow_max_purchase", FALSE);
            $this->db->select("mp.iWishlistState AS mp_wishlist_state");
            $this->db->select("mp.iViewState AS mp_view_state");
            $this->db->select("mp.iTotalRate AS mp_total_rate");
            $this->db->select("mp.dDate AS mp_date");
            $this->db->select("mc.vTitle AS mc_title");
        } else {
            $this->db->select("mp.iMstProductsId AS iMstProductsId");
            $this->db->select("mp.iMstProductsId AS mp_mst_products_id");
            $this->db->select("tpc.iTrnProductCategoriesId AS iTrnProductCategoriesId");
            $this->db->select("mp.iStoreId AS mp_admin_id2");
            $this->db->select("tpc.iMstCategoriesId AS tpc_mst_categories_id");
            $this->db->select("mp.vTitle AS mp_title");
            $this->db->select("mp.vSku AS mp_sku");
            $this->db->select("mp.fRegularPrice AS mp_regular_price");
            $this->db->select("mp.fSalePrice AS mp_sale_price");
            $this->db->select("mp.iAllowMaxPurchase AS mp_allow_max_purchase");
            $this->db->select("mp.iStock AS mp_stock");
            $this->db->select("mp.iDifferencePer AS mp_difference_per");
            $this->db->select("mp.eFreeShipping AS mp_free_shipping");
            $this->db->select("mp.fShippingCharge AS mp_shipping_charge");
            $this->db->select("mp.eDoReturn AS mp_do_return");
            $this->db->select("mp.iReturnDays AS mp_return_days");
            $this->db->select("mp.vShortDescription AS mp_short_description");
            $this->db->select("mp.tDescription AS mp_description");
            $this->db->select("mp.eStatus AS mp_status");
            $this->db->select("mp.vSearchTag AS mp_search_tag");
            $this->db->select("tpc.iMstProductsId AS tpc_mst_products_id");
            $this->db->select("mp.iAdminId AS mpistoreid");
            $this->db->select("mp.dModifyDate AS mp_modify_date");
            $this->db->select("mp.fRatingAvg AS mp_rating_avg");
            $this->db->select("mp.dDate AS mp_date");
            $this->db->select("mp.iLowAvlLimitNotifcation AS mp_low_avl_limit_notifcation");
            $this->db->select("mp.iTotalRate AS mp_total_rate");
            $this->db->select("mp.iWishlistState AS mp_wishlist_state");
            $this->db->select("mp.iSaleState AS mp_sale_state");
            $this->db->select("mp.iViewState AS mp_view_state");
            $this->db->select("mp.vDefaultImg AS mp_default_img");
            $this->db->select("mp.tSearchKeyword AS mp_search_keyword");
        }

        $this->db->from($this->table_name . " AS " . $this->table_alias);
        if (is_array($join) && is_array($join['joins']) && count($join['joins']) > 0) {
            $this->listing->addJoinTables($join['joins']);
            $this->addJoinTables("AR", array("tpc"));
            if ($join["list"] == "Yes") {
                $this->addJoinTables("AR", array("msd", "ma", "mso", "mc"));
            }
        } else {
            $this->addJoinTables("AR", array("tpc"));
            if ($join == "Yes") {
                $this->addJoinTables("AR", array("msd", "ma", "mso", "mc"));
            }
        }
        if (is_array($extra_cond) && count($extra_cond) > 0) {
            $this->listing->addWhereFields($extra_cond);
        } elseif (is_numeric($extra_cond)) {
            $this->db->where($this->table_alias . "." . $this->primary_key, intval($extra_cond));
        } elseif ($extra_cond) {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if ($group_by != "") {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "") {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        if ($order_by != "") {
            $this->db->order_by($order_by);
        }
        if ($limit != "") {
            if (is_numeric($limit)) {
                $this->db->limit($limit);
            } else {
                list($offset, $limit) = @explode(",", $limit);
                $this->db->limit($offset, $limit);
            }
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }

    /**
     * getListingData method is used to get grid listing data records for this module.
     * @param array $config_arr config_arr for grid listing settigs.
     * @return array $listing_data returns data records array for grid.
     */
    public function getListingData($config_arr = array()) {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $rec_per_page = (intval($rows) > 0) ? intval($rows) : $this->rec_per_page;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->start_cache();
        $this->db->from($this->table_name . " AS " . $this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "") {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0) {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "") {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;
        $filter_config['grid_fields'] = $this->grid_fields;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "") {
            $this->db->where("(" . $filter_main . ")", FALSE, FALSE);
        }
        if ($filter_left != "") {
            $this->db->where("(" . $filter_left . ")", FALSE, FALSE);
        }
        if ($filter_range != "") {
            $this->db->where("(" . $filter_range . ")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias . "." . $this->primary_key . " AS " . $this->primary_key);
        if ($this->primary_alias != "") {
            $this->db->select($this->table_alias . "." . $this->primary_key . " AS " . $this->primary_alias);
        }
        $this->db->select("msd.vStoreName AS msd_store_name");
        $this->db->select("tpc.iMstCategoriesId AS tpc_mst_categories_id");
        $this->db->select("mp.vSku AS mp_sku");
        $this->db->select("mp.vTitle AS mp_title");
        $this->db->select("mp.eStatus AS mp_status");
        $this->db->select("(SELECT SUM(msb.iProductQty) FROM `mst_sub_order` as msb where msb.iMstProductsId=" . $this->db->protect("mp.iMstProductsId") . ") AS mp_low_avl_limit_notifcation", FALSE);
        $this->db->select("(SELECT SUM(msb.fTotalCost) FROM `mst_sub_order` as msb where msb.iMstProductsId=" . $this->db->protect("mp.iMstProductsId") . ") AS mp_allow_max_purchase", FALSE);
        $this->db->select("mp.iWishlistState AS mp_wishlist_state");
        $this->db->select("mp.iViewState AS mp_view_state");
        $this->db->select("mp.iTotalRate AS mp_total_rate");
        $this->db->select("mp.dDate AS mp_date");
        $this->db->select("mc.vTitle AS mc_title");

        $this->db->stop_cache();
        if ((is_array($group_by) && count($group_by) > 0) || trim($having_cond) != "") {
            $this->db->select($this->table_alias . "." . $this->primary_key);
            $total_records_arr = $this->db->get();
            $total_records = is_object($total_records_arr) ? $total_records_arr->num_rows() : 0;
        } else {
            $total_records = $this->db->count_all_results();
        }

        $total_pages = $this->listing->getTotalPages($total_records, $rec_per_page);
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0) {
            foreach ($order_by as $orK => $orV) {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "") {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        $limit_offset = $this->listing->getStartIndex($total_records, $page, $rec_per_page);
        $this->db->limit($rec_per_page, $limit_offset);
        $return_data_obj = $this->db->get();
        $return_data = is_object($return_data_obj) ? $return_data_obj->result_array() : array();
        $this->db->flush_cache();
        $listing_data = $this->listing->getDataForJqGrid($return_data, $filter_config, $page, $total_pages, $total_records);
        $this->listing_data = $return_data;
        #echo $this->db->last_query();
        return $listing_data;
    }

    /**
     * getExportData method is used to get grid export data records for this module.
     * @param array $config_arr config_arr for grid export settigs.
     * @return array $export_data returns data records array for export.
     */
    public function getExportData($config_arr = array()) {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $rowlimit = $config_arr['rowlimit'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->from($this->table_name . " AS " . $this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "") {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0) {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "") {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "") {
            $this->db->where("(" . $filter_main . ")", FALSE, FALSE);
        }
        if ($filter_left != "") {
            $this->db->where("(" . $filter_left . ")", FALSE, FALSE);
        }
        if ($filter_range != "") {
            $this->db->where("(" . $filter_range . ")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias . "." . $this->primary_key . " AS " . $this->primary_key);
        if ($this->primary_alias != "") {
            $this->db->select($this->table_alias . "." . $this->primary_key . " AS " . $this->primary_alias);
        }
        $this->db->select("msd.vStoreName AS msd_store_name");
        $this->db->select("tpc.iMstCategoriesId AS tpc_mst_categories_id");
        $this->db->select("mp.vSku AS mp_sku");
        $this->db->select("mp.vTitle AS mp_title");
        $this->db->select("mp.eStatus AS mp_status");
        $this->db->select("(SELECT SUM(msb.iProductQty) FROM `mst_sub_order` as msb where msb.iMstProductsId=" . $this->db->protect("mp.iMstProductsId") . ") AS mp_low_avl_limit_notifcation", FALSE);
        $this->db->select("(SELECT SUM(msb.fTotalCost) FROM `mst_sub_order` as msb where msb.iMstProductsId=" . $this->db->protect("mp.iMstProductsId") . ") AS mp_allow_max_purchase", FALSE);
        $this->db->select("mp.iWishlistState AS mp_wishlist_state");
        $this->db->select("mp.iViewState AS mp_view_state");
        $this->db->select("mp.iTotalRate AS mp_total_rate");
        $this->db->select("mc.vTitle AS mc_title");
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0) {
            foreach ($order_by as $orK => $orV) {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "") {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        if ($rowlimit != "") {
            $offset = $rowlimit;
            $limit = ($rowlimit * $page - $rowlimit);
            $this->db->limit($offset, $limit);
        }
        $export_data_obj = $this->db->get();
        $export_data = is_object($export_data_obj) ? $export_data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $export_data;
    }

    /**
     * addJoinTables method is used to make relation tables joins with main table.
     * @param string $type type is to get active record or join string.
     * @param boolean $allow_tables allow_table is to restrict some set of tables.
     * @return string $ret_joins returns relation tables join string.
     */
    public function addJoinTables($type = 'AR', $allow_tables = FALSE) {
        $join_tables = $this->join_tables;
        if (!is_array($join_tables) || count($join_tables) == 0) {
            return '';
        }
        $ret_joins = $this->listing->addJoinTables($join_tables, $type, $allow_tables);
        return $ret_joins;
    }

    /**
     * getListConfiguration method is used to get listing configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns listing configuration array.
     */
    public function getListConfiguration($name = "") {
        $list_config = array(
            "msd_store_name" => array(
                "name" => "msd_store_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreName",
                "source_field" => "",
                "display_query" => "msd.vStoreName",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Store Name",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_STORE_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "tpc_mst_categories_id" => array(
                "name" => "tpc_mst_categories_id",
                "table_name" => "trn_product_categories",
                "table_alias" => "tpc",
                "field_name" => "iMstCategoriesId",
                "source_field" => "tpc_mst_categories_id",
                "display_query" => "tpc.iMstCategoriesId",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Parent category",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_PARENT_CATEGORY'),
                "width" => 120,
                "search" => "No",
                "sortable" => "No",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "php_func" => "getTopmostParentCateogry",
            ),
            "mp_sku" => array(
                "name" => "mp_sku",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "vSku",
                "source_field" => "mp_sku",
                "display_query" => "mp.vSku",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "SKU",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_SKU'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mp_title" => array(
                "name" => "mp_title",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "vTitle",
                "source_field" => "mp_title",
                "display_query" => "mp.vTitle",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Products",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_PRODUCTS'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "grand_tot" => "Yes",
            ),
            "mp_status" => array(
                "name" => "mp_status",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "eStatus",
                "source_field" => "mp_status",
                "display_query" => "mp.eStatus",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Status",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_STATUS'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("mp_status", "Text", "Active")
            ),
            "mp_low_avl_limit_notifcation" => array(
                "name" => "mp_low_avl_limit_notifcation",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iLowAvlLimitNotifcation",
                "source_field" => "mp_low_avl_limit_notifcation",
                "display_query" => "mp.iMstProductsId",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "right",
                "label" => "Qty. Sold",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_QTY_C46_SOLD'),
                "width" => 120,
                "search" => "No",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("mp_low_avl_limit_notifcation", "Text", "1"),
                "grand_tot" => "Yes",
                "sql_func" => "SELECT SUM(msb.iProductQty) FROM `mst_sub_order` as msb where msb.iMstProductsId=%q%",
            ),
            "mp_allow_max_purchase" => array(
                "name" => "mp_allow_max_purchase",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iAllowMaxPurchase",
                "source_field" => "mp_allow_max_purchase",
                "display_query" => "mp.iMstProductsId",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "right",
                "label" => "Total Sales",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_TOTAL_SALES'),
                "width" => 120,
                "search" => "No",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("mp_allow_max_purchase", "Text", "10"),
                "grand_tot" => "Yes",
                "sql_func" => "SELECT SUM(msb.fTotalCost) FROM `mst_sub_order` as msb where msb.iMstProductsId=%q%",
            ),
            "mp_wishlist_state" => array(
                "name" => "mp_wishlist_state",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iWishlistState",
                "source_field" => "mp_wishlist_state",
                "display_query" => "mp.iWishlistState",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "center",
                "label" => "Wishlist State",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_WISHLIST_STATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "grand_tot" => "Yes",
            ),
            "mp_view_state" => array(
                "name" => "mp_view_state",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iViewState",
                "source_field" => "mp_view_state",
                "display_query" => "mp.iViewState",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "center",
                "label" => "View State",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_VIEW_STATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "grand_tot" => "Yes",
            ),
            "mp_total_rate" => array(
                "name" => "mp_total_rate",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iTotalRate",
                "source_field" => "mp_total_rate",
                "display_query" => "mp.iTotalRate",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "center",
                "label" => "Total Rate",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_TOTAL_RATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mp_date" => array(
                "name" => "mp_date",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "dDate",
                "source_field" => "mp_date",
                "display_query" => "mp.dDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "None",
                "type" => "textbox",
                "align" => "center",
                "label" => "Added Date",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_ADDED_DATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("mp_date", "MySQL", "NOW()")
            ),
            "mc_title" => array(
                "name" => "mc_title",
                "table_name" => "mst_categories",
                "table_alias" => "mc",
                "field_name" => "vTitle",
                "source_field" => "mc_title",
                "display_query" => "mc.vTitle",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Category",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_CATEGORY'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0) {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++) {
                $config_arr[$name[$i]] = $list_config[$name[$i]];
            }
        } elseif ($name != "" && is_string($name)) {
            $config_arr = $list_config[$name];
        } else {
            $config_arr = $list_config;
        }
        return $config_arr;
    }

    /**
     * getFormConfiguration method is used to get form configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns form configuration array.
     */
    public function getFormConfiguration($name = "") {
        $form_config = array(
            "mp_admin_id2" => array(
                "name" => "mp_admin_id2",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iStoreId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Store",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_STORE')
            ),
            "tpc_mst_categories_id" => array(
                "name" => "tpc_mst_categories_id",
                "table_name" => "trn_product_categories",
                "table_alias" => "tpc",
                "field_name" => "iMstCategoriesId",
                "entry_type" => "Relation",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Category",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_CATEGORY')
            ),
            "mp_title" => array(
                "name" => "mp_title",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "vTitle",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Product Name",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_PRODUCT_NAME')
            ),
            "mp_sku" => array(
                "name" => "mp_sku",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "vSku",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "SKU",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_SKU')
            ),
            "mp_regular_price" => array(
                "name" => "mp_regular_price",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "fRegularPrice",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Regular Price",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_REGULAR_PRICE')
            ),
            "mp_sale_price" => array(
                "name" => "mp_sale_price",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "fSalePrice",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Sale Price",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_SALE_PRICE')
            ),
            "mp_allow_max_purchase" => array(
                "name" => "mp_allow_max_purchase",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iAllowMaxPurchase",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Allow Max Purchase",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_ALLOW_MAX_PURCHASE'),
                "default" => $this->filter->getDefaultValue("mp_allow_max_purchase", "Text", "10")
            ),
            "mp_stock" => array(
                "name" => "mp_stock",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iStock",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Current Stock",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_CURRENT_STOCK'),
                "default" => $this->filter->getDefaultValue("mp_stock", "Text", "1")
            ),
            "mp_difference_per" => array(
                "name" => "mp_difference_per",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iDifferencePer",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Difference Per",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_DIFFERENCE_PER')
            ),
            "mp_free_shipping" => array(
                "name" => "mp_free_shipping",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "eFreeShipping",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Free Shipping",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_FREE_SHIPPING'),
                "default" => $this->filter->getDefaultValue("mp_free_shipping", "Text", "Yes")
            ),
            "mp_shipping_charge" => array(
                "name" => "mp_shipping_charge",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "fShippingCharge",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Shipping Charge",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_SHIPPING_CHARGE')
            ),
            "mp_do_return" => array(
                "name" => "mp_do_return",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "eDoReturn",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Do Return",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_DO_RETURN'),
                "default" => $this->filter->getDefaultValue("mp_do_return", "Text", "No")
            ),
            "mp_return_days" => array(
                "name" => "mp_return_days",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iReturnDays",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Return Days",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_RETURN_DAYS')
            ),
            "mp_short_description" => array(
                "name" => "mp_short_description",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "vShortDescription",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textarea",
                "label" => "Short Description",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_SHORT_DESCRIPTION')
            ),
            "mp_description" => array(
                "name" => "mp_description",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "tDescription",
                "entry_type" => "Table",
                "data_type" => "text",
                "type" => "wysiwyg",
                "label" => "Description",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_DESCRIPTION')
            ),
            "mp_status" => array(
                "name" => "mp_status",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "eStatus",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Status",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_STATUS'),
                "default" => $this->filter->getDefaultValue("mp_status", "Text", "Active")
            ),
            "mp_search_tag" => array(
                "name" => "mp_search_tag",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "vSearchTag",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textarea",
                "label" => "Search Keywords",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_SEARCH_KEYWORDS')
            ),
            "tpc_mst_products_id" => array(
                "name" => "tpc_mst_products_id",
                "table_name" => "trn_product_categories",
                "table_alias" => "tpc",
                "field_name" => "iMstProductsId",
                "entry_type" => "Relation",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Mst Products Id",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_MST_PRODUCTS_ID')
            ),
            "mpistoreid" => array(
                "name" => "mpistoreid",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iAdminId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Store Owner",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_STORE_OWNER'),
                "show_input" => "Update",
            ),
            "mp_modify_date" => array(
                "name" => "mp_modify_date",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "dModifyDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "type" => "date",
                "label" => "Modify Date",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_MODIFY_DATE'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("mp_modify_date", "MySQL", "now()"),
                "format" => 'Y-m-d',
            ),
            "mp_rating_avg" => array(
                "name" => "mp_rating_avg",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "fRatingAvg",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "rating_master",
                "label" => "Avg. Rating",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_AVG_C46_RATING'),
                "show_input" => "Update",
            ),
            "mp_date" => array(
                "name" => "mp_date",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "dDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "type" => "textbox",
                "label" => "Date",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_DATE'),
                "default" => $this->filter->getDefaultValue("mp_date", "MySQL", "NOW()")
            ),
            "mp_low_avl_limit_notifcation" => array(
                "name" => "mp_low_avl_limit_notifcation",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iLowAvlLimitNotifcation",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Low Notification",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_LOW_NOTIFICATION'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("mp_low_avl_limit_notifcation", "Text", "1")
            ),
            "mp_total_rate" => array(
                "name" => "mp_total_rate",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iTotalRate",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Total Rating",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_TOTAL_RATING'),
                "show_input" => "Update",
            ),
            "mp_wishlist_state" => array(
                "name" => "mp_wishlist_state",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iWishlistState",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Wishlist State",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_WISHLIST_STATE'),
                "show_input" => "Update",
            ),
            "mp_sale_state" => array(
                "name" => "mp_sale_state",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iSaleState",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Sale State",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_SALE_STATE'),
                "show_input" => "Update",
            ),
            "mp_view_state" => array(
                "name" => "mp_view_state",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "iViewState",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "View State",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_VIEW_STATE'),
                "show_input" => "Update",
            ),
            "mp_default_img" => array(
                "name" => "mp_default_img",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "vDefaultImg",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Default Img",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_DEFAULT_IMG'),
                "show_input" => "Update",
            ),
            "mp_search_keyword" => array(
                "name" => "mp_search_keyword",
                "table_name" => "mst_products",
                "table_alias" => "mp",
                "field_name" => "tSearchKeyword",
                "entry_type" => "Table",
                "data_type" => "text",
                "type" => "textarea",
                "label" => "Search Keyword",
                "label_lang" => $this->lang->line('PRODUCT_MANAGEMENT_V1_SEARCH_KEYWORD'),
                "show_input" => "Update",
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0) {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++) {
                $config_arr[$name[$i]] = $form_config[$name[$i]];
            }
        } elseif ($name != "" && is_string($name)) {
            $config_arr = $form_config[$name];
        } else {
            $config_arr = $form_config;
        }
        return $config_arr;
    }

    /**
     * checkRecordExists method is used to check duplication of records.
     * @param array $field_arr field_arr is having fields to check.
     * @param array $field_val field_val is having values of respective fields.
     * @param numeric $id id is to avoid current records.
     * @param string $mode mode is having either Add or Update.
     * @param string $con con is having either AND or OR.
     * @return boolean $exists returns either TRUE of FALSE.
     */
    public function checkRecordExists($field_arr = array(), $field_val = array(), $id = '', $mode = '', $con = 'AND') {
        $exists = FALSE;
        if (!is_array($field_arr) || count($field_arr) == 0) {
            return $exists;
        }
        foreach ((array) $field_arr as $key => $val) {
            $extra_cond_arr[] = $this->db->protect($this->table_alias . "." . $field_arr[$key]) . " =  " . $this->db->escape($field_val[$val]);
        }
        $extra_cond = "(" . @implode(" " . $con . " ", $extra_cond_arr) . ")";
        if ($mode == "Add") {
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0) {
                $exists = TRUE;
            }
        } elseif ($mode == "Update") {
            $extra_cond = $this->db->protect($this->table_alias . "." . $this->primary_key) . " <> " . $this->db->escape($id) . " AND " . $extra_cond;
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0) {
                $exists = TRUE;
            }
        }
        return $exists;
    }

    /**
     * getSwitchTo method is used to get switch to dropdown array.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @return array $switch_data returns data records array.
     */
    public function getSwitchTo($extra_cond = '', $type = 'records', $limit = '') {
        $switchto_fields = $this->switchto_fields;
        $switch_data = array();
        if (!is_array($switchto_fields) || count($switchto_fields) == 0) {
            if ($type == "count") {
                return count($switch_data);
            } else {
                return $switch_data;
            }
        }
        $fields_arr = array();
        $fields_arr[] = array(
            "field" => $this->table_alias . "." . $this->primary_key . " AS id",
        );
        $fields_arr[] = array(
            "field" => $this->db->concat($switchto_fields) . " AS val",
            "escape" => TRUE,
        );
        if (trim($this->extra_cond) != "") {
            $extra_cond = (trim($extra_cond) != "") ? $extra_cond . " AND " . $this->extra_cond : $this->extra_cond;
        }
        $switch_data = $this->getData($extra_cond, $fields_arr, "val ASC", "", $limit, "Yes");
        #echo $this->db->last_query();
        if ($type == "count") {
            return count($switch_data);
        } else {
            return $switch_data;
        }
    }

    /**
     * relationData method is used to get data records from relation table.
     * @param string $table_alias table_alias is to get specific relation table data.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are comma seperated values.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @return array $data_arr returns relation data records array.
     */
    function relationData($table_alias = '', $extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "") {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_alias);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        if (is_array($fields)) {
            $this->listing->addSelectFields($fields);
        } elseif ($fields != "") {
            $this->db->select($fields);
        } else {
            $this->db->select($table_info['table_alias'] . ".*");
        }
        $this->db->from($table_info['table_name'] . " AS " . $table_info['table_alias']);
        if (is_array($extra_cond) && count($extra_cond) > 0) {
            $this->listing->addWhereFields($extra_cond);
        } elseif ($extra_cond != "") {
            if (is_numeric($extra_cond)) {
                $this->db->where($table_info['table_alias'] . "." . $table_info['primary_key'], $extra_cond);
            } else {
                $this->db->where($extra_cond, FALSE, FALSE);
            }
        }
        $this->general->getPhysicalRecordWhere($table_info['table_name'], $table_info['table_alias'], "AR");
        if ($group_by != "") {
            $this->db->group_by($group_by);
        }
        if ($order_by != "") {
            $this->db->order_by($order_by);
        }
        if ($limit != "") {
            list($offset, $limit) = @explode(",", $limit);
            $this->db->limit($offset, $limit);
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }

    /**
     * relationInsert method is used to insert data records to the relation table.
     * @param string $table_alias table_alias is to insert data into specific relation table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    function relationInsert($table_alias = '', $data = array()) {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_alias);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        $this->db->insert($table_name, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    /**
     * relationUpdate method is used to update data records to the relation table.
     * @param string $table_alias table_alias is to update data into specific relation table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @param string $join join is to make joins with main table.
     * @return boolean $res returns TRUE or FALSE.
     */
    function relationUpdate($table_alias = '', $data = array(), $where = '', $join = 'No') {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_alias);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        if ($join == "Yes") {
            $set_cond = array();
            foreach ($data as $key => $val) {
                $set_cond[] = $this->db->protect($key) . " = " . $this->db->escape($val);
            }
            $join_tbls = $this->listing->addJoinTables(array($table_info), "NR");
            if (is_numeric($where)) {
                $extra_cond = "WHERE " . $this->db->protect($table_info['primary_key']) . " = " . $this->db->escape($where);
            } elseif ($where != '') {
                $extra_cond = "WHERE " . $where;
            } else {
                return FALSE;
            }
            $update_query = "UPDATE " . $this->db->protect($table_name) . " AS " . $this->db->protect($table_info['table_alias']) . " " . $join_tbls . " SET " . @implode(", ", $set_cond) . " " . $extra_cond;
            $res = $this->db->query($update_query);
        } else {
            if (is_numeric($where)) {
                $this->db->where($table_info['primary_key'], $where);
            } elseif ($where != '') {
                $this->db->where($where, FALSE, FALSE);
            } else {
                return FALSE;
            }
            $res = $this->db->update($table_name, $data);
        }
        return $res;
    }

    /**
     * relationDelete method is used to delete data records from relation table.
     * @param string $table_alias table_alias is to update data into specific relation table.
     * @param string $where where is the query condition for deleting.
     * @return boolean $res returns TRUE or FALSE.
     */
    function relationDelete($table_alias = '', $where = '') {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_name);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        if (is_numeric($where)) {
            $this->db->where($table_info['primary_key'], $where);
        } else {
            $this->db->where($where, FALSE, FALSE);
        }
        $res = $this->db->delete($table_name);
        return $res;
    }

    /**
     * relationDetails method is used to get relation table detail.
     * @param string $table_alias table_alias is to get data of specific relation table.
     * @return array $ret_arr returns relation table detail array.
     */
    function relationDetails($table_alias = '') {
        $relation_config = array(
            "tpc" => array(
                "table_name" => "trn_product_categories",
                "table_alias" => "tpc",
                "field_name" => "iMstProductsId",
                "primary_key" => "iTrnProductCategoriesId",
                "rel_table_name" => "mst_products",
                "rel_table_alias" => "mp",
                "rel_field_name" => "iMstProductsId",
                "extra_condition" => "",
            )
        );
        $ret_arr = $relation_config[$table_alias];
        return $ret_arr;
    }

    /**
     * processGridColor method is used to process grid color settings configuration.
     * @param array $data_arr data_arr is to process data records.
     * @param array $config_arr config_arr for process color settings.
     * @return array $data_arr returns data records array.
     */
    function processGridColor($data_arr = array(), $config_arr = array()) {
        $grid_color_config = array(
            array(
                array(
                    "apply" => "Yes",
                    "oper" => "le",
                    "type" => "string",
                    "mod_1" => "Variable",
                    "val_1" => "mp_stock",
                    "mod_2" => "Static",
                    "val_2" => "1",
                    "color_code" => "Static",
                    "color_value" => "#FF8D85",
                    "fill_color" => "Cell",
                    "cell_name" => "mp_stock",
                ),
                array(
                    "apply" => "Yes",
                    "oper" => "le",
                    "type" => "string",
                    "mod_1" => "Variable",
                    "val_1" => "mp_return_days",
                    "mod_2" => "Static",
                    "val_2" => "1",
                    "color_code" => "Static",
                    "color_value" => "#FFAC9C",
                    "fill_color" => "Cell",
                    "cell_name" => "mp_return_days",
                )
            )
        );
        $listing_data = $this->listing_data;
        if (!is_array($listing_data) || count($listing_data) == 0) {
            return $data_arr;
        }
        foreach ($listing_data as $dKey => $dVal) {
            $id = $dVal["iMstProductsId"];
            $row_arr = $this->listing->getGridRowColors($grid_color_config, $dVal, $id);
            if (is_array($row_arr) && count($row_arr) > 0) {
                $data_arr['colors'][$id] = $row_arr;
            }
        }
        return $data_arr;
    }

    /**
     * processGrandCalc method is used to process grid grand calculation settings configuration.
     * @param array $data_arr data_arr is to process data records.
     * @param array $config_arr config_arr for process grand calculation settings.
     * @return array $data_arr returns data records array.
     */
    function processGrandCalc($data_arr = array(), $config_arr = array()) {
        $grand_calc_config = array(
            "mp_title" => array(
                "oper" => "count",
            ),
            "mp_low_avl_limit_notifcation" => array(
                "oper" => "sum",
            ),
            "mp_allow_max_purchase" => array(
                "oper" => "sum",
            ),
            "mp_wishlist_state" => array(
                "oper" => "sum",
            ),
            "mp_view_state" => array(
                "oper" => "sum",
            )
        );
        $grid_fields = $this->grid_fields;
        $listing_data = $this->listing_data;
        if (!is_array($listing_data) || count($listing_data) == 0) {
            return $data_arr;
        }
        $grand_tot_arr = array();
        foreach ($listing_data as $dKey => $dVal) {
            $custom_links_arr = array();
            $id = $dVal["iMstProductsId"];
            foreach ($grid_fields as $gKey => $gVal) {
                if ($config_arr[$gVal]['grand_tot'] == "Yes") {
                    $grand_tot_arr[$gVal]['attr'] = $grand_calc_config[$gVal];
                    $grand_tot_arr[$gVal]['list'][] = $this->listing->getGroupGrandTotal($dVal[$gVal], $config_arr);
                }
            }
        }
        if (is_array($grand_tot_arr) && count($grand_tot_arr) > 0) {
            $this->db->select("vName, vDesc, vValue, eSource, eSelectType");
            $this->db->where("eConfigType", "Prices");
            $db_assoc_settings = $this->db->select_assoc("mod_setting", "vName");
            foreach ((array) $grand_tot_arr as $key => $val) {
                $data_arr['userdata'][$key] = $this->listing->getGrandCalcBlock($val['list'], $val['attr'], $db_assoc_settings);
            }
        }
        return $data_arr;
    }

}
