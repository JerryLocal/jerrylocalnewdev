<?php
/**
 * Description of Products Report Controller
 *
 * @module Products Report
 *
 * @class product_management_v1.php
 *
 * @path application\admin\productmanagement\controllers\product_management_v1.php
 *
 * @author CIT Dev Team
 *
 * @date 12.01.2016
 */
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Product_management_v1 extends HB_Controller
{
    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->load->model('model_product_management_v1');
        $this->_request_params();
        $this->folder_name = "productmanagement";
        $this->module_name = "product_management_v1";
        $this->mod_enc_url = $this->general->getGeneralEncryptList($this->folder_name, $this->module_name);
        $this->mod_enc_mode = $this->general->getCustomEncryptMode(TRUE);
        $this->module_config = array(
            'module_name' => $this->module_name,
            'folder_name' => $this->folder_name,
            'mod_enc_url' => $this->mod_enc_url,
            'mod_enc_mode' => $this->mod_enc_mode,
            'delete' => "Yes",
            'xeditable' => "No",
            'top_detail' => "No",
            "multi_lingual" => "No",
            "physical_data_remove" => "Yes",
            "workflow_modes" => array()
        );
        $this->dropdown_arr = array(
            "mp_admin_id2" => array(
                "type" => "table",
                "table_name" => "mst_store_detail",
                "field_key" => "iMstStoreDetailId",
                "field_val" => array(
                    $this->db->protect("vStoreName")
                ),
                "extra_cond" => "eStatus = 'Active'",
                "order_by" => "vStoreName asc",
                "default" => "Yes",
            ),
            "mp_free_shipping" => array(
                "type" => "enum",
                "default" => "Yes",
                "values" => array(
                    array(
                        'id' => 'Yes',
                        'val' => $this->lang->line('PRODUCT_MANAGEMENT_V1_YES')
                    ),
                    array(
                        'id' => 'No',
                        'val' => $this->lang->line('PRODUCT_MANAGEMENT_V1_NO')
                    )
                )
            ),
            "mp_do_return" => array(
                "type" => "enum",
                "default" => "Yes",
                "values" => array(
                    array(
                        'id' => 'Yes',
                        'val' => $this->lang->line('PRODUCT_MANAGEMENT_V1_YES')
                    ),
                    array(
                        'id' => 'No',
                        'val' => $this->lang->line('PRODUCT_MANAGEMENT_V1_NO')
                    )
                )
            ),
            "mp_status" => array(
                "type" => "enum",
                "default" => "Yes",
                "values" => array(
                    array(
                        'id' => 'Active',
                        'val' => $this->lang->line('PRODUCT_MANAGEMENT_V1_ACTIVE')
                    ),
                    array(
                        'id' => 'Inactive',
                        'val' => $this->lang->line('PRODUCT_MANAGEMENT_V1_INACTIVE')
                    )
                )
            ),
            "tpc_mst_products_id" => array(
                "type" => "table",
                "table_name" => "mst_categories",
                "field_key" => "iMstCategoriesId",
                "field_val" => array(
                    $this->db->protect("vTitle")
                ),
                "extra_cond" => "eStatus='Active'",
                "order_by" => "val asc",
                "default" => "Yes",
            )
        );
        $this->parMod = $this->params_arr["parMod"];
        $this->parID = $this->params_arr["parID"];
        $this->parRefer = array();
        $this->expRefer = array();

        $this->topRefer = array();
        $this->dropdown_limit = $this->config->item('ADMIN_DROPDOWN_LIMIT');
        $this->search_combo_limit = $this->config->item('ADMIN_SEARCH_COMBO_LIMIT');
        $this->switchto_limit = $this->config->item('ADMIN_SWITCH_DROPDOWN_LIMIT');
        $this->count_arr = array();
    }

    /**
     * _request_params method is used to set post/get/request params.
     */
    public function _request_params()
    {
        $this->get_arr = is_array($this->input->get(NULL, TRUE)) ? $this->input->get(NULL, TRUE) : array();
        $this->post_arr = is_array($this->input->post(NULL, TRUE)) ? $this->input->post(NULL, TRUE) : array();
        $this->params_arr = array_merge($this->get_arr, $this->post_arr);
        return $this->params_arr;
    }

    /**
     * index method is used to intialize grid listing page.
     */
    public function index()
    {
        $params_arr = $this->params_arr;
        $extra_qstr = $extra_hstr = '';
        list($list_access, $view_access, $add_access, $edit_access, $del_access, $expo_access) = $this->filter->getModuleWiseAccess("product_management_v1", array("List", "View", "Add", "Update", "Delete", "Export"), TRUE, TRUE);
        try
        {
            if (!$list_access)
            {
                throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_VIEW_THIS_PAGE_C46_C46_C33'));
            }
            $enc_loc_module = $this->general->getMD5EncryptString("ListPrefer", "product_management_v1");

            $status_array = $status_values = array();
            $status_values = $this->dropdown_arr["mp_status"]["values"];
            $status_values_count = count($status_values);
            for ($i = 0; $i < $status_values_count; $i++)
            {
                $status_array[$i] = $status_values[$i]["id"];
            }

            $status_array_label = $this->general->getEnumValuesLabels($this->module_name, $status_array);
            $list_config = $this->model_product_management_v1->getListConfiguration();
            $this->processConfiguration($list_config, $add_access, $edit_access, TRUE);

            $extra_qstr .= $this->general->getRequestURLParams();
            $extra_hstr .= $this->general->getRequestHASHParams();
            $render_arr = array(

                'list_config' => $list_config,
                'count_arr' => $this->count_arr,
                'enc_loc_module' => $enc_loc_module,
                'status_array' => $status_array,
                'status_array_label' => $status_array_label,
                'view_access' => $view_access,
                'add_access' => $add_access,
                'edit_access' => $edit_access,
                'del_access' => $del_access,
                'expo_access' => $expo_access,
                'folder_name' => $this->folder_name,
                'module_name' => $this->module_name,
                'mod_enc_url' => $this->mod_enc_url,
                'mod_enc_mode' => $this->mod_enc_mode,
                'extra_qstr' => $extra_qstr,
                'extra_hstr' => $extra_hstr,
                'default_filters' => $this->model_product_management_v1->default_filters,
            );
            $this->smarty->assign($render_arr);
            $this->loadView("product_management_v1_index");
        }
        catch(Exception $e)
        {
            $render_arr['err_message'] = $e->getMessage();
            $this->smarty->assign($render_arr);
            $this->loadView($this->config->item('ADMIN_FORBIDDEN_TEMPLATE'));
        }
    }

    /**
     * listing method is used to load listing data records in json format.
     */
    public function listing()
    {
        $params_arr = $this->params_arr;
        $page = $params_arr['page'];
        $rows = $params_arr['rows'];
        $sidx = $params_arr['sidx'];
        $sord = $params_arr['sord'];
        $sdef = $params_arr['sdef'];
        $filters = $params_arr['filters'];
        if (!trim($sidx) && !trim($sord))
        {
            $sdef = 'Yes';
        }
        if ($this->general->allowStripSlashes())
        {
            $filters = stripslashes($filters);
        }
        $filters = json_decode($filters, TRUE);
        $list_config = $this->model_product_management_v1->getListConfiguration();
        $form_config = $this->model_product_management_v1->getFormConfiguration();
        $extra_cond = $this->model_product_management_v1->extra_cond;
        $groupby_cond = $this->model_product_management_v1->groupby_cond;
        $having_cond = $this->model_product_management_v1->having_cond;
        $orderby_cond = $this->model_product_management_v1->orderby_cond;

        $data_config = array();
        $data_config['page'] = $page;
        $data_config['rows'] = $rows;
        $data_config['sidx'] = $sidx;
        $data_config['sord'] = $sord;
        $data_config['sdef'] = $sdef;
        $data_config['filters'] = $filters;
        $data_config['module_config'] = $this->module_config;
        $data_config['list_config'] = $list_config;
        $data_config['form_config'] = $form_config;
        $data_config['dropdown_arr'] = $this->dropdown_arr;
        $data_config['extra_cond'] = $extra_cond;
        $data_config['group_by'] = $groupby_cond;
        $data_config['having_cond'] = $having_cond;
        $data_config['order_by'] = $orderby_cond;
        $data_recs = $this->model_product_management_v1->getListingData($data_config);
        $data_recs['no_records_msg'] = $this->general->processMessageLabel('');

        $data_recs = $this->model_product_management_v1->processGrandCalc($data_recs, $list_config);
        $data_recs = $this->model_product_management_v1->processGridColor($data_recs, $list_config);
        echo json_encode($data_recs);
        $this->skip_template_view();
    }

    /**
     * export method is used to export listing data records in csv or pdf formats.
     */
    public function export()
    {
        $this->filter->getModuleWiseAccess("product_management_v1", "Export", TRUE);
        $params_arr = $this->params_arr;
        $page = $params_arr['page'];
        $rowlimit = $params_arr['rowlimit'];
        $sidx = $params_arr['sidx'];
        $sord = $params_arr['sord'];
        $sdef = $params_arr['sdef'];
        if (!trim($sidx) && !trim($sord))
        {
            $sdef = 'Yes';
        }
        $export_type = $params_arr['export_type'];
        $export_mode = $params_arr['export_mode'];
        $filters = $params_arr['filters'];
        if ($this->general->allowStripSlashes())
        {
            $filters = stripslashes($filters);
        }
        $filters = json_decode(base64_decode($filters), TRUE);
        $fields = json_decode(base64_decode($params_arr['fields']), TRUE);
        $list_config = $this->model_product_management_v1->getListConfiguration();
        $form_config = $this->model_product_management_v1->getFormConfiguration();
        $table_name = $this->model_product_management_v1->table_name;
        $table_alias = $this->model_product_management_v1table_alias;
        $primary_key = $this->model_product_management_v1->primary_key;
        $extra_cond = $this->model_product_management_v1->extra_cond;
        $groupby_cond = $this->model_product_management_v1->groupby_cond;
        $having_cond = $this->model_product_management_v1->having_cond;
        $orderby_cond = $this->model_product_management_v1->orderby_cond;

        $export_config = array();
        $export_config['page'] = $page;
        $export_config['rowlimit'] = $rowlimit;
        $export_config['sidx'] = $sidx;
        $export_config['sord'] = $sord;
        $export_config['sdef'] = $sdef;
        $export_config['filters'] = $filters;
        $export_config['export_mode'] = $export_mode;
        $export_config['module_config'] = $this->module_config;
        $export_config['list_config'] = $list_config;
        $export_config['form_config'] = $form_config;
        $export_config['dropdown_arr'] = $this->dropdown_arr;
        $export_config['table_name'] = $table_name;
        $export_config['table_alias'] = $table_alias;
        $export_config['primary_key'] = $primary_key;
        $export_config['extra_cond'] = $extra_cond;
        $export_config['group_by'] = $groupby_cond;
        $export_config['having_cond'] = $having_cond;
        $export_config['order_by'] = $orderby_cond;
        $db_recs = $this->model_product_management_v1->getExportData($export_config);
        $db_recs = $this->listing->getDataForList($db_recs, $export_config, "GExport", array());
        if (!is_array($db_recs) || count($db_recs) == 0)
        {
            $this->session->set_flashdata('failure', $this->lang->line('GENERIC_GRID_NO_RECORDS_TO_PROCESS'));
            redirect($_SERVER['HTTP_REFERER']);
        }

        $filename = "Products_Report_".count($db_recs)."_Records";
        $heading = "Products Report";
        if ($export_mode == "all" && is_array($tot_fields_arr))
        {
            if (($pr_key = array_search($primary_key, $tot_fields_arr)) !== FALSE)
            {
                unset($tot_fields_arr[$pr_key]);
            }
            $fields = array_values($tot_fields_arr);
        }
        $numberOfColumns = count($fields);
        if ($export_type == 'pdf')
        {
            if (file_exists(APPPATH.'third_party/Pdf_export.php'))
            {
                $pdf_style = "TCPDF";
            }
            else
            {
                $pdf_style = "FPDF";
                $eachColumnWidth = 40;
            }
            $columns = $aligns = $widths = $data = array();
            //Table headers info
            for ($i = 0; $i < $numberOfColumns; $i++)
            {
                $size = 10;
                $position = '';
                if (array_key_exists($fields[$i], $list_config))
                {
                    $label = $list_config[$fields[$i]]['label_lang'];
                    $position = $list_config[$fields[$i]]['align'];
                    $size = $list_config[$fields[$i]]['width'];
                }
                elseif (array_key_exists($fields[$i], $form_config))
                {
                    $label = $form_config[$fields[$i]]['label_lang'];
                }
                else
                {
                    $label = $fields[$i];
                }
                $columns[] = $label;
                if ($pdf_style == "TCPDF")
                {
                    $aligns[] = in_array($position, array('right', 'center')) ? $position : "left";
                    $widths[] = $size;
                }
                else
                {
                    $aligns[] = in_array($position, array('right', 'center')) ? (($position == 'right') ? 'R' : 'C') : 'L';
                    $widths[] = $eachColumnWidth;
                }
            }

            //Table data info
            $db_rec_cnt = count($db_recs);
            for ($i = 0; $i < $db_rec_cnt; $i++)
            {
                foreach ((array) $db_recs[$i] as $key => $val)
                {
                    if (is_array($fields) && in_array($key, $fields))
                    {
                        $data[$i][$key] = $this->listing->dataForExportMode($val, "pdf", $pdf_style);
                    }
                }
            }
            if ($pdf_style == "TCPDF")
            {
                require_once (APPPATH.'third_party/Pdf_export.php');
                $pdf = new PDF_Export(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, TRUE, 'UTF-8', FALSE);
                $pdf->initialize($heading);

                $pdf->writeGridTable($columns, $data, $widths, $aligns);
                $pdf->Output($filename.".pdf", 'D');
            }
            else
            {
                require_once (APPPATH.'third_party/fpdfclasses/fpdf.php');
                require_once (APPPATH.'third_party/fpdfclasses/mc_table.php');
                $pdf = new PDF_MC_Table();

                $fpdf_width = ($numberOfColumns*$eachColumnWidth)+20;
                $fpdf_height = ($numberOfColumns*$eachColumnWidth)+30;

                $pdf->pageHeightWidth = array(
                    $fpdf_width,
                    $fpdf_height,
                );
                $pdf->AddPage('P', $pdf->pageHeightWidth);
                $pdf->SetFont('Arial', '', 18);
                $pdf->Cell(0, 6, $heading, 0, 1, 'C');
                $pdf->Ln(10);
                $pdf->SetWidths($widths);
                $pdf->SetAligns($aligns);
                $pdf->SetFont('Arial', 'B', 14);
                $pdf->Row($columns);
                $pdf->SetFont('Arial', '', 12);
                $data_cnt = count($data);
                for ($k = 0; $k < $data_cnt; $k++)
                {
                    $pdf->Row(array_values($data[$k]));
                }
                $pdf->Output($filename.".pdf", 'D');
            }
        }
        elseif ($export_type == 'csv')
        {
            require_once (APPPATH.'third_party/Csv_export.php');
            $columns = $data = array();

            for ($i = 0; $i < $numberOfColumns; $i++)
            {
                if (array_key_exists($fields[$i], $list_config))
                {
                    $label = $list_config[$fields[$i]]['label_lang'];
                }
                elseif (array_key_exists($fields[$i], $form_config))
                {
                    $label = $form_config[$fields[$i]]['label_lang'];
                }
                else
                {
                    $label = $fields[$i];
                }
                $columns[] = $label;
            }
            $db_recs_cnt = count($db_recs);
            for ($i = 0; $i < $db_recs_cnt; $i++)
            {
                foreach ((array) $db_recs[$i] as $key => $val)
                {
                    if (is_array($fields) && in_array($key, $fields))
                    {
                        $data[$i][$key] = $this->listing->dataForExportMode($val, "csv");
                    }
                }
            }
            $export_array = array_merge(array($columns), $data);
            $csv = new CSV_Writer($export_array);
            $csv->headers($filename);
            $csv->output();
        }
        $this->skip_template_view();
    }

    /**
     * add method is used to add or update data records.
     */
    public function add()
    {
        $params_arr = $this->params_arr;
        $extra_qstr = $extra_hstr = '';
        $hideCtrl = $params_arr['hideCtrl'];
        $mode = (in_array($params_arr['mode'], array("Update", "View"))) ? "Update" : "Add";
        $viewMode = ($params_arr['mode'] == "View") ? TRUE : FALSE;
        $id = $params_arr['id'];
        $enc_id = $this->general->getAdminEncodeURL($id);
        try
        {
            $extra_cond = $this->model_product_management_v1->extra_cond;
            if ($mode == "Update")
            {
                list($list_access, $view_access, $edit_access, $del_access, $expo_access) = $this->filter->getModuleWiseAccess("product_management_v1", array("List", "View", "Update", "Delete", "Export"), TRUE, TRUE);
                if (!$edit_access && !$view_access)
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_VIEW_THIS_PAGE_C46_C46_C33'));
                }
            }
            else
            {
                list($list_access, $add_access, $del_access) = $this->filter->getModuleWiseAccess("product_management_v1", array("List", "Add", "Delete"), TRUE, TRUE);
                if (!$add_access)
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_ADD_THESE_DETAILS_C46_C46_C33'));
                }
            }

            $data = array();
            if ($mode == 'Update')
            {
                $ctrl_flow = $this->ci_local->read($this->general->getMD5EncryptString("FlowEdit", "product_management_v1"), $this->session->userdata('iAdminId'));
                $data_arr = $this->model_product_management_v1->getData(intval($id));
                $data = $data_arr[0];
                if ((!is_array($data) || count($data) == 0) && $params_arr['rmPopup'] != "true")
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_RECORDS_WHICH_YOU_ARE_TRYING_TO_ACCESS_ARE_NOT_AVAILABLE_C46_C46_C33'));
                }
                $switch_arr = $this->model_product_management_v1->getSwitchTo($extra_cond, "records", $this->switchto_limit);
                $switch_combo = $this->filter->makeArrayDropDown($switch_arr);
                $switch_cit = array();
                $switch_tot = $this->model_product_management_v1->getSwitchTo($extra_cond, "count");
                if ($this->switchto_limit > 0 && $switch_tot > $this->switchto_limit)
                {
                    $switch_cit['param'] = "true";
                    $switch_cit['url'] = $this->mod_enc_url['get_self_switch_to'];
                    if (!array_key_exists($id, $switch_combo))
                    {
                        $extra_cond = $this->db->protect($this->model_product_management_v1->table_alias.".".$this->model_product_management_v1->primary_key)." = ".$this->db->escape($id);
                        $switch_cur = $this->model_product_management_v1->getSwitchTo($extra_cond, "records", 1);
                        if (is_array($switch_cur) && count($switch_cur) > 0)
                        {
                            $switch_combo[$switch_cur[0]['id']] = $switch_cur[0]['val'];
                        }
                    }
                }
                $recName = $switch_combo[$id];
                $switch_enc_combo = $this->filter->getSwitchEncryptRec($switch_combo);
                $this->dropdown->combo("array", "vSwitchPage", $switch_enc_combo, $enc_id);
                $next_prev_records = $this->filter->getNextPrevRecords($id, $switch_arr);
            }
            else
            {
                $recName = '';
                $ctrl_flow = $this->ci_local->read($this->general->getMD5EncryptString("FlowAdd", "product_management_v1"), $this->session->userdata('iAdminId'));
            }
            $opt_arr = $img_html = $auto_arr = $config_arr = array();

            $main_data = $this->model_product_management_v1->getData(intval($id), array('mp.iMstProductsId'));

            $relation_module = $this->model_product_management_v1->relation_modules["productimages"];
            $this->load->module("productimages/productimages");
            $this->load->model("productimages/model_productimages");
            $child_config["child_module"] = "productimages";
            if ($mode == "Update")
            {
                $extra_cond = $this->db->protect("mpi.iMstProductsId")." = ".$this->db->escape($main_data[0]["iMstProductsId"]);
                if ($relation_module["extra_cond"] != "")
                {
                    $extra_cond .= " AND ".$relation_module["extra_cond"];
                }
                $parent_join_arr = array(
                    "joins" => array(
                        array(
                            "table_name" => "mst_products",
                            "table_alias" => "mp",
                            "field_name" => "iMstProductsId",
                            "rel_table_name" => "mst_product_img",
                            "rel_table_alias" => "mpi",
                            "rel_field_name" => "iMstProductsId",
                            "join_type" => "left",
                        )
                    )
                );
                $child_data = $this->model_productimages->getData($extra_cond, "", "", "", "", $parent_join_arr);
                $child_config["mode"] = (is_array($child_data) && count($child_data) > 0) ? "Update" : "Add";
                $child_config["data"] = $child_data;
            }
            else
            {
                $child_config["mode"] = "Add";
                $child_config["data"] = array();
            }
            $child_config["perform"] = array(
                "data",
                "options",
            );
            $module_arr = $this->productimages->getRelationModule($child_config);
            $module_arr["config_arr"]["popup"] = $relation_module["popup"];
            $module_arr["config_arr"]["recMode"] = $child_config["mode"];
            $child_assoc_data["productimages"] = $module_arr["data"];
            $child_assoc_conf["productimages"] = $module_arr["config_arr"];
            $child_assoc_opt["productimages"] = $module_arr["opt_arr"];
            $child_assoc_img["productimages"] = $module_arr["img_html"];
            $child_assoc_auto["productimages"] = $module_arr["auto_arr"];

            $form_config = $this->model_product_management_v1->getFormConfiguration($config_arr);
            if (is_array($form_config) && count($form_config) > 0)
            {
                foreach ($form_config as $key => $val)
                {
                    if ($params_arr['rmPopup'] == "true" && $params_arr[$key] != "")
                    {
                        $data[$key] = $params_arr[$key];
                    }
                    elseif ($mode == "Add" || $val["show_input"] == "Update")
                    {
                        $data[$key] = (trim($data[$key]) != "") ? $data[$key] : $val['default'];
                    }
                    if ($val['encrypt'] == "Yes")
                    {
                        $data[$key] = $this->listing->adminLocalDecrypt($data[$key]);
                    }
                    if ($val['function'] != "")
                    {
                        if (method_exists($this->general, $val['function']))
                        {
                            $data[$key] = $this->general->$val['function']($mode, $data[$key], $data, $id, $key, $key);
                        }
                    }
                    $source_field = $val['name'];
                    $combo_config = $this->dropdown_arr[$source_field];
                    if (is_array($combo_config) && count($combo_config) > 0)
                    {
                        if ($combo_config['auto'] == "Yes")
                        {
                            $combo_count = $this->getSourceOptions($source_field, $mode, $id, $data, '', 'count');
                            if ($combo_count[0]['tot'] > $this->dropdown_limit)
                            {
                                $auto_arr[$source_field] = "Yes";
                            }
                        }
                        $combo_arr = $this->getSourceOptions($source_field, $mode, $id, $data);
                        $final_arr = $this->filter->makeArrayDropdown($combo_arr);
                        if ($combo_config['opt_group'] == "Yes")
                        {
                            $display_arr = $this->filter->makeOPTDropdown($combo_arr);
                        }
                        else
                        {
                            $display_arr = $final_arr;
                        }
                        $this->dropdown->combo("array", $source_field, $display_arr, $data[$key]);
                        $opt_arr[$source_field] = $final_arr;
                    }
                }
            }
            $extra_qstr .= $this->general->getRequestURLParams();
            $extra_hstr .= $this->general->getRequestHASHParams();

            /** access controls <<< **/
            $controls_allow = $prev_link_allow = $next_link_allow = $update_allow = $delete_allow = $backlink_allow = $switchto_allow = $discard_allow = $tabing_allow = TRUE;
            $delete_allow = ($this->module_config["delete"] == "No" || !$del_access) ? TRUE : FALSE;
            if (is_array($switch_combo) && count($switch_combo) > 0)
            {
                $prev_link_allow = ($next_prev_records['prev']['id'] != '') ? TRUE : FALSE;
                $next_link_allow = ($next_prev_records['next']['id'] != '') ? TRUE : FALSE;
            }
            else
            {
                $prev_link_allow = $next_link_allow = $switchto_allow = FALSE;
            }
            if (!$list_access)
            {
                $backlink_allow = $discard_allow = FALSE;
            }
            if ($hideCtrl == "true")
            {
                $controls_allow = $prev_link_allow = $next_link_allow = $delete_allow = $backlink_allow = $switchto_allow = $tabing_allow = FALSE;
            }
            /** access controls >>> **/
            $render_arr = array(

                "child_assoc_data" => $child_assoc_data,
                "child_assoc_conf" => $child_assoc_conf,
                "child_assoc_opt" => $child_assoc_opt,
                "child_assoc_img" => $child_assoc_img,
                "child_assoc_auto" => $child_assoc_auto,
                "edit_access" => $edit_access,
                "expo_access" => $expo_access,
                'controls_allow' => $controls_allow,
                'prev_link_allow' => $prev_link_allow,
                'next_link_allow' => $next_link_allow,
                'update_allow' => $update_allow,
                'delete_allow' => $delete_allow,
                'backlink_allow' => $backlink_allow,
                'switchto_allow' => $switchto_allow,
                'discard_allow' => $discard_allow,
                'tabing_allow' => $tabing_allow,
                'enc_id' => $enc_id,
                'id' => $id,
                'mode' => $mode,
                'data' => $data,
                'recName' => $recName,
                "opt_arr" => $opt_arr,
                "img_html" => $img_html,
                "auto_arr" => $auto_arr,
                'ctrl_flow' => $ctrl_flow,
                'switch_combo' => $switch_combo,
                'switch_cit' => $switch_cit,
                'next_prev_records' => $next_prev_records,
                'folder_name' => $this->folder_name,
                'module_name' => $this->module_name,
                'mod_enc_url' => $this->mod_enc_url,
                'mod_enc_mode' => $this->mod_enc_mode,
                'extra_qstr' => $extra_qstr,
                'extra_hstr' => $extra_hstr,
            );
            $this->smarty->assign($render_arr);
            if ($mode == "Update")
            {
                if ($edit_access && $viewMode != TRUE)
                {
                    $this->loadView("product_management_v1_add");
                }
                else
                {
                    $this->loadView("product_management_v1_add_view");
                }
            }
            else
            {
                $this->loadView("product_management_v1_add");
            }
        }
        catch(Exception $e)
        {
            $render_arr['err_message'] = $e->getMessage();
            $this->smarty->assign($render_arr);
            $this->loadView($this->config->item('ADMIN_FORBIDDEN_TEMPLATE'));
        }
    }

    /**
     * addAction method is used to save data, which is posted through form.
     */
    public function addAction()
    {
        $params_arr = $this->params_arr;
        $mode = ($params_arr['mode'] == "Update") ? "Update" : "Add";
        $id = $params_arr['id'];
        try
        {
            $add_edit_access = $this->filter->getModuleWiseAccess("product_management_v1", $mode, TRUE, TRUE);
            if (!$add_edit_access)
            {
                if ($mode == "Update")
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
                }
                else
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_ADD_THESE_DETAILS_C46_C46_C33'));
                }
            }
            if (method_exists($this->general, 'ProductPriceDiffrence'))
            {
                $event_res = $this->general->ProductPriceDiffrence($mode, $id, $params_arr['parID']);
                if ($event_res['success'] != '1')
                {
                    $before_error_msg = $this->general->processMessageLabel('ACTION_BEFORE_EVENT_HAS_BEEN_FAILED_C46_C46_C33');
                    $error_msg = ($event_res['message']) ? $event_res['message'] : $before_error_msg;
                    throw new Exception($error_msg);
                }
            }

            $form_config = $this->model_product_management_v1->getFormConfiguration();
            $params_arr = $this->_request_params();
            $mp_admin_id2 = $params_arr["mp_admin_id2"];
            $tpc_mst_categories_id = $params_arr["tpc_mst_categories_id"];
            $mp_title = $params_arr["mp_title"];
            $mp_sku = $params_arr["mp_sku"];
            $mp_regular_price = $params_arr["mp_regular_price"];
            $mp_sale_price = $params_arr["mp_sale_price"];
            $mp_allow_max_purchase = $params_arr["mp_allow_max_purchase"];
            $mp_stock = $params_arr["mp_stock"];
            $mp_difference_per = $params_arr["mp_difference_per"];
            $mp_free_shipping = $params_arr["mp_free_shipping"];
            $mp_shipping_charge = $params_arr["mp_shipping_charge"];
            $mp_do_return = $params_arr["mp_do_return"];
            $mp_return_days = $params_arr["mp_return_days"];
            $mp_short_description = $params_arr["mp_short_description"];
            $mp_description = $params_arr["mp_description"];
            $mp_status = $params_arr["mp_status"];
            $mp_search_tag = $params_arr["mp_search_tag"];
            $tpc_mst_products_id = $params_arr["tpc_mst_products_id"];
            $mpistoreid = $params_arr["mpistoreid"];
            $mp_modify_date = $params_arr["mp_modify_date"];
            $mp_rating_avg = $params_arr["mp_rating_avg"];
            $mp_date = $params_arr["mp_date"];
            $mp_low_avl_limit_notifcation = $params_arr["mp_low_avl_limit_notifcation"];
            $mp_total_rate = $params_arr["mp_total_rate"];
            $mp_wishlist_state = $params_arr["mp_wishlist_state"];
            $mp_sale_state = $params_arr["mp_sale_state"];
            $mp_view_state = $params_arr["mp_view_state"];
            $mp_default_img = $params_arr["mp_default_img"];
            $mp_search_keyword = $params_arr["mp_search_keyword"];

            $data = $save_data_arr = $file_data = array();
            $data["iStoreId"] = $mp_admin_id2;
            $data["vTitle"] = $mp_title;
            $data["vSku"] = $mp_sku;
            $data["fRegularPrice"] = $mp_regular_price;
            $data["fSalePrice"] = $mp_sale_price;
            $data["iAllowMaxPurchase"] = $mp_allow_max_purchase;
            $data["iStock"] = $mp_stock;
            $data["iDifferencePer"] = $mp_difference_per;
            $data["eFreeShipping"] = $mp_free_shipping;
            $data["fShippingCharge"] = $mp_shipping_charge;
            $data["eDoReturn"] = $mp_do_return;
            $data["iReturnDays"] = $mp_return_days;
            $data["vShortDescription"] = $mp_short_description;
            $data["tDescription"] = $this->input->get_post("mp_description", FALSE);
            $data["eStatus"] = $mp_status;
            $data["vSearchTag"] = $mp_search_tag;
            $data["iAdminId"] = $mpistoreid;
            $data["dModifyDate"] = $this->filter->formatActionData($mp_modify_date, $form_config["mp_modify_date"]);
            $data["fRatingAvg"] = $mp_rating_avg;
            $data["dDate"] = $mp_date;
            $data["iLowAvlLimitNotifcation"] = $mp_low_avl_limit_notifcation;
            $data["iTotalRate"] = $mp_total_rate;
            $data["iWishlistState"] = $mp_wishlist_state;
            $data["iSaleState"] = $mp_sale_state;
            $data["iViewState"] = $mp_view_state;
            $data["vDefaultImg"] = $mp_default_img;
            $data["tSearchKeyword"] = $mp_search_keyword;

            $save_data_arr["mp_admin_id2"] = $data["iStoreId"];
            $save_data_arr["mp_title"] = $data["vTitle"];
            $save_data_arr["mp_sku"] = $data["vSku"];
            $save_data_arr["mp_regular_price"] = $data["fRegularPrice"];
            $save_data_arr["mp_sale_price"] = $data["fSalePrice"];
            $save_data_arr["mp_allow_max_purchase"] = $data["iAllowMaxPurchase"];
            $save_data_arr["mp_stock"] = $data["iStock"];
            $save_data_arr["mp_difference_per"] = $data["iDifferencePer"];
            $save_data_arr["mp_free_shipping"] = $data["eFreeShipping"];
            $save_data_arr["mp_shipping_charge"] = $data["fShippingCharge"];
            $save_data_arr["mp_do_return"] = $data["eDoReturn"];
            $save_data_arr["mp_return_days"] = $data["iReturnDays"];
            $save_data_arr["mp_short_description"] = $data["vShortDescription"];
            $save_data_arr["mp_description"] = $data["tDescription"];
            $save_data_arr["mp_status"] = $data["eStatus"];
            $save_data_arr["mp_search_tag"] = $data["vSearchTag"];
            $save_data_arr["mpistoreid"] = $data["iAdminId"];
            $save_data_arr["mp_modify_date"] = $data["dModifyDate"];
            $save_data_arr["mp_rating_avg"] = $data["fRatingAvg"];
            $save_data_arr["mp_date"] = $data["dDate"];
            $save_data_arr["mp_low_avl_limit_notifcation"] = $data["iLowAvlLimitNotifcation"];
            $save_data_arr["mp_total_rate"] = $data["iTotalRate"];
            $save_data_arr["mp_wishlist_state"] = $data["iWishlistState"];
            $save_data_arr["mp_sale_state"] = $data["iSaleState"];
            $save_data_arr["mp_view_state"] = $data["iViewState"];
            $save_data_arr["mp_default_img"] = $data["vDefaultImg"];
            $save_data_arr["mp_search_keyword"] = $data["tSearchKeyword"];
            if ($mode == 'Add')
            {
                $id = $this->model_product_management_v1->insert($data);
                if (intval($id) > 0)
                {
                    $save_data_arr["iMstProductsId"] = $data["iMstProductsId"] = $id;
                    $msg = $this->general->processMessageLabel('ACTION_RECORD_ADDED_SUCCESSFULLY_C46_C46_C33');
                }
                else
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_ADDING_RECORD_C46_C46_C33'));
                }
            }
            elseif ($mode == 'Update')
            {
                $res = $this->model_product_management_v1->update($data, intval($id));
                if (intval($res) > 0)
                {
                    $save_data_arr["iMstProductsId"] = $data["iMstProductsId"] = $id;
                    $msg = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                }
                else
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                }
            }
            $ret_arr['message'] = $msg;
            $ret_arr['success'] = 1;

            $rel_data = array();
            $rel_data["iMstCategoriesId"] = $tpc_mst_categories_id;
            $rel_data["iMstProductsId"] = $data["iMstProductsId"];

            $rel_data["iMstProductsId"] = $data["iMstProductsId"];
            $save_data_arr["tpc_mst_categories_id"] = $rel_data["iMstCategoriesId"];
            $save_data_arr["tpc_mst_products_id"] = $rel_data["iMstProductsId"];

            $rel_table_info = $this->model_product_management_v1->relationDetails("tpc");
            $rel_extra_cond = $this->db->protect($rel_table_info["table_alias"].".iMstProductsId")." = ".$this->db->protect($data["iMstProductsId"]);
            if ($rel_table_info["extra_cond"] != "")
            {
                $rel_extra_cond .= " AND ".$rel_table_info["extra_cond"];
            }
            $rel_rec = $this->model_product_management_v1->relationData("tpc", $rel_extra_cond);
            if (is_array($rel_rec) && count($rel_rec) > 0)
            {
                $rel_id = $rel_rec[0][$rel_table_info['primary_key']];
                $res = $this->model_product_management_v1->relationUpdate("tpc", $rel_data, $rel_id);
            }
            else
            {
                $rel_id = $res = $this->model_product_management_v1->relationInsert("tpc", $rel_data);
            }
            $save_data_arr["iTrnProductCategoriesId"] = $rel_id;
            if (method_exists($this->general, 'ProductDefaultImg'))
            {
                $event_res = $this->general->ProductDefaultImg($mode, $id, $params_arr['parID']);
                if ($event_res['success'] != '1')
                {
                    $after_error_msg = $this->general->processMessageLabel('ACTION_AFTER_EVENT_HAS_BEEN_FAILED_C46_C46_C33');
                    $error_msg = ($event_res['message']) ? $event_res['message'] : $after_error_msg;
                    $ret_arr['message'] = $error_msg;
                    $ret_arr['success'] = $event_res['success'];
                }
            }

            $params_arr = $this->_request_params();

            $childModuleArr = $params_arr["childModule"];
            $childPostArr = $params_arr["child"];
            if (is_array($childModuleArr) && count($childModuleArr) > 0)
            {
                $main_data = $this->model_product_management_v1->getData(intval($id), array('mp.iMstProductsId'));
                foreach ((array) $childModuleArr as $mdKey => $mdVal)
                {
                    $child_module = $mdVal;
                    if ($params_arr["childModuleShowHide"][$child_module] == "No")
                    {
                        continue;
                    }
                    $child_module_post = $childPostArr[$child_module];
                    $child_id_arr = is_array($child_module_post["id"]) ? $child_module_post["id"] : array();
                    switch ($child_module)
                    {
                        case "productimages":
                            $relation_module = $this->model_product_management_v1->relation_modules["productimages"];

                            $extra_del = $this->db->protect("mpi.iMstProductsId")." = ".$this->db->escape($main_data[0]["iMstProductsId"])." AND ".$this->db->protect("mpi.iMstProductImgId")." NOT IN ('".@implode("','", $child_id_arr)."')";
                            if ($relation_module["extra_cond"] != "")
                            {
                                $extra_del .= " AND ".$relation_module["extra_cond"];
                            }
                            $child_del_arr["child_module"] = $child_module;
                            $child_del_arr["extra_cond"] = $extra_del;
                            $res = $this->childDataDelete($child_del_arr, true);
                            foreach ((array) $child_id_arr as $chKey => $chVal)
                            {
                                $child_save_arr["child_module"] = $child_module;
                                $child_save_arr["index"] = $chKey;
                                $child_save_arr["id"] = $chVal;
                                $child_save_arr["data"] = $child_module_post;
                                $child_save_arr["main_data"] = $main_data;
                                $child_res = $this->childDataSave($child_save_arr, true);
                                if (!$child_res["success"])
                                {
                                    $ret_arr["message"] = $child_res["message"];
                                    $ret_arr["success"] = 2;
                                }
                            }
                            break;
                        }
                }
            }
        }
        catch(Exception $e)
        {
            $ret_arr['message'] = $e->getMessage();
            $ret_arr['success'] = 0;
        }
        $ret_arr['mod_enc_url']['add'] = $this->mod_enc_url['add'];
        $ret_arr['mod_enc_url']['index'] = $this->mod_enc_url['index'];
        $ret_arr['red_type'] = 'List';
        $this->filter->getPageFlowURL($ret_arr, $this->module_config, $params_arr, $id, $data);

        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    /**
     * inlineEditAction method is used to save inline editing data records, status field updation,
     * delete records either from grid listing or update form, saving inline adding records from grid
     */
    public function inlineEditAction()
    {
        $params_arr = $this->params_arr;
        $operartor = $params_arr['oper'];
        $all_row_selected = $params_arr['AllRowSelected'];
        $primary_ids = explode(",", $params_arr['id']);
        $primary_ids = count($primary_ids) > 1 ? $primary_ids : $primary_ids[0];
        $filters = $params_arr['filters'];
        if ($this->general->allowStripSlashes())
        {
            $filters = stripslashes($filters);
        }
        $filters = json_decode($filters, TRUE);
        $extra_cond = '';
        $search_mode = $search_join = $search_alias = 'No';
        if ($all_row_selected == "true" && in_array($mode, array("del", "status")))
        {
            $search_mode = ($mode == "del") ? "Delete" : "Update";
            $search_join = $search_alias = "Yes";
            $config_arr['module_name'] = $this->module_name;
            $config_arr['list_config'] = $this->model_product_management_v1->getListConfiguration();
            $config_arr['form_config'] = $this->model_product_management_v1->getFormConfiguration();
            $config_arr['table_name'] = $this->model_product_management_v1->table_name;
            $config_arr['table_alias'] = $this->model_product_management_v1->table_alias;
            $filter_main = $this->filter->applyFilter($filters, $config_arr, $search_mode);
            $filter_left = $this->filter->applyLeftFilter($filters, $config_arr, $search_mode);
            $filter_range = $this->filter->applyRangeFilter($filters, $config_arr, $search_mode);
            if ($filter_main != "")
            {
                $extra_cond .= ($extra_cond != "") ? " AND (".$filter_main.")" : $filter_main;
            }
            if ($filter_left != "")
            {
                $extra_cond .= ($extra_cond != "") ? " AND (".$filter_left.")" : $filter_left;
            }
            if ($filter_range != "")
            {
                $extra_cond .= ($extra_cond != "") ? " AND (".$filter_range.")" : $filter_range;
            }
        }
        if ($search_alias == "Yes")
        {
            $primary_field = $this->model_product_management_v1->table_alias.".".$this->model_product_management_v1->primary_key;
        }
        else
        {
            $primary_field = $this->model_product_management_v1->primary_key;
        }
        if (is_array($primary_ids))
        {
            $pk_condition = $this->db->protect($primary_field)." IN ('".implode("','", $primary_ids)."')";
        }
        elseif (intval($primary_ids) > 0)
        {
            $pk_condition = $this->db->protect($primary_field)." = ".$this->db->escape($primary_ids);
        }
        else
        {
            $pk_condition = FALSE;
        }
        if ($pk_condition)
        {
            $extra_cond .= ($extra_cond != "") ? " AND (".$pk_condition.")" : $pk_condition;
        }
        $data_arr = $save_data_arr = array();
        try
        {
            switch ($operartor)
            {
                case 'del':
                    $mode = "Delete";
                    if (method_exists($this->general, 'Delete_product'))
                    {
                        $event_res = $this->general->Delete_product($mode, $primary_ids, $params_arr['parID']);
                        if ($event_res['success'] != '1')
                        {
                            $del_error_msg = $this->general->processMessageLabel('ACTION_DELETE_EVENT_HAS_BEEN_FAILED_C46_C46_C33');
                            $error_msg = ($event_res['message']) ? $event_res['message'] : $del_error_msg;
                            throw new Exception($error_msg);
                        }
                    }

                    $del_access = $this->filter->getModuleWiseAccess("product_management_v1", "Delete", TRUE, TRUE);
                    if (!$del_access)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_DELETE_THESE_DETAILS_C46_C46_C33'));
                    }
                    if ($search_mode == "No" && $pk_condition == FALSE)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }

                    $params_arr = $this->_request_params();
                    if (is_array($primary_ids))
                    {
                        $main_cond = $this->db->protect("mp.iMstProductsId")." IN ('".@implode("','", $primary_ids)."')";
                    }
                    else
                    {
                        $main_cond = $this->db->protect("mp.iMstProductsId")." = ".$this->db->escape($primary_ids);
                    }
                    $main_data = $this->model_product_management_v1->getData($main_cond, array('mp.iMstProductsId'));
                    $success = $this->model_product_management_v1->delete($extra_cond, $search_alias, $search_join);
                    if (!$success)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }
                    $message = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_DELETED_SUCCESSFULLY_C46_C46_C33');

                    $deletion_modules = $this->model_product_management_v1->deletion_modules;
                    $this->general->deleteReferenceModules($deletion_modules, $main_data, $this->module_config["physical_data_remove"]);
                    break;
                case 'edit':
                    $mode = "Update";
                    $edit_access = $this->filter->getModuleWiseAccess("product_management_v1", "Update", TRUE, TRUE);
                    if (!$edit_access)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
                    }
                    $post_name = $params_arr['name'];
                    $post_val = is_array($params_arr['value']) ? @implode(",", $params_arr['value']) : $params_arr['value'];

                    $list_config = $this->model_product_management_v1->getListConfiguration($post_name);
                    $form_config = $this->model_product_management_v1->getFormConfiguration($list_config['source_field']);
                    if (!is_array($form_config) || count($form_config) == 0)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
                    }
                    if (in_array($form_config['type'], array("date", "date_and_time", "time", 'phone_number')))
                    {
                        $post_val = $this->filter->formatActionData($post_val, $form_config);
                    }
                    if ($form_config["encrypt"] == "Yes")
                    {
                        $post_val = $this->listing->adminLocalEncrypt($post_val);
                    }
                    $field_name = $form_config['field_name'];
                    $unique_name = $form_config['name'];
                    if ($form_config['entry_type'] == "Relation")
                    {
                        $rel_table_info = $this->model_product_management_v1->relationDetails($form_config['table_alias']);
                        $update_field = $rel_table_info['table_alias'].".".$field_name;
                        $where_cond = $this->db->protect($this->model_product_management_v1->table_alias.".".$this->model_product_management_v1->primary_key)." = ".$this->db->escape($primary_ids);
                        $data_arr[$update_field] = $post_val;
                        $success = $this->model_product_management_v1->relationUpdate($form_config['table_alias'], $data_arr, $where_cond, "Yes");
                    }
                    else
                    {

                        $data_arr[$field_name] = $post_val;
                        $success = $this->model_product_management_v1->update($data_arr, intval($primary_ids));
                    }
                    if (!$success)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                    }
                    $message = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                    break;
                case 'status':
                    $mode = "Status";
                    $edit_access = $this->filter->getModuleWiseAccess("product_management_v1", "Update", TRUE, TRUE);
                    if (!$edit_access)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
                    }
                    if ($search_mode == "No" && $pk_condition == FALSE)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }
                    $status_field = "eStatus";
                    if ($status_field == "")
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
                    }
                    if ($search_mode == "Yes" || $search_alias == "Yes")
                    {
                        $field_name = $this->model_product_management_v1->table_alias.".eStatus";
                    }
                    else
                    {
                        $field_name = $status_field;
                    }
                    $data_arr[$field_name] = $params_arr['status'];
                    $success = $this->model_product_management_v1->update($data_arr, $extra_cond, $search_alias, $search_join);
                    if (!$success)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_MODIFYING_THESE_RECORDS_C46_C46_C33'));
                    }
                    $message = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_MODIFIED_SUCCESSFULLY_C46_C46_C33');
                    break;
            }
            $ret_arr['success'] = "true";
            $ret_arr['message'] = $message;
        }
        catch(Exception $e)
        {
            $ret_arr['success'] = "false";
            $ret_arr['message'] = $e->getMessage();
        }
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    /**
     * processConfiguration method is used to process add and edit permissions for grid intialization
     */
    public function processConfiguration(&$list_config = array(), $isAdd = TRUE, $isEdit = TRUE, $runCombo = FALSE)
    {
        if (!is_array($list_config) || count($list_config) == 0)
        {
            return $list_config;
        }
        $count_arr = array();
        foreach ((array) $list_config as $key => $val)
        {
            if (!$isAdd)
            {
                $list_config[$key]["addable"] = "No";
            }
            if (!$isEdit)
            {
                $list_config[$key]["editable"] = "No";
            }

            $source_field = $val['source_field'];
            $dropdown_arr = $this->dropdown_arr[$source_field];
            if (is_array($dropdown_arr) && in_array($val['type'], array("dropdown", "radio_buttons", "checkboxes", "multi_select_dropdown")))
            {
                $count_arr[$key]['ajax'] = "No";
                $count_arr[$key]['json'] = "No";
                $count_arr[$key]['data'] = array();
                $combo_arr = FALSE;
                if ($dropdown_arr['auto'] == "Yes")
                {
                    $combo_arr = $this->getSourceOptions($source_field, "Search", '', array(), '', 'count');
                    if ($combo_arr[0]['tot'] > $this->dropdown_limit)
                    {
                        $count_arr[$key]['ajax'] = "Yes";
                    }
                }
                if ($runCombo == TRUE)
                {
                    if (in_array($dropdown_arr['type'], array("enum", "phpfn")))
                    {
                        $data_arr = $this->getSourceOptions($source_field, "Search");
                        $json_arr = $this->filter->makeArrayDropdown($data_arr);
                        $count_arr[$key]['json'] = "Yes";
                        $count_arr[$key]['data'] = json_encode($json_arr);
                    }
                    else
                    {
                        if ($dropdown_arr['opt_group'] != "Yes")
                        {
                            if ($combo_arr == FALSE)
                            {
                                $combo_arr = $this->getSourceOptions($source_field, "Search", '', array(), '', 'count');
                            }
                            if ($combo_arr[0]['tot'] < $this->search_combo_limit)
                            {
                                $data_arr = $this->getSourceOptions($source_field, "Search");
                                $json_arr = $this->filter->makeArrayDropdown($data_arr);
                                $count_arr[$key]['json'] = "Yes";
                                $count_arr[$key]['data'] = json_encode($json_arr);
                            }
                        }
                    }
                }
            }
        }
        $this->count_arr = $count_arr;
        return $list_config;
    }

    /**
     * getSourceOptions method is used to get data array of enum, table, token or php function input types
     * @param string $name unique name of form configuration field.
     * @param string $mode mode for add or update form.
     * @param string $id update record id of add or update form.
     * @param array $data data array of add or update record.
     * @param string $extra extra query condition for searching data array.
     * @param string $rtype type for getting either records list or records count.
     * @return array $data_arr returns data records array
     */
    public function getSourceOptions($name = '', $mode = 'Add', $id = '', $data = array(), $extra = '', $rtype = 'records')
    {
        $combo_config = $this->dropdown_arr[$name];
        $data_arr = array();
        if (!is_array($combo_config) || count($combo_config) == 0)
        {
            return $data_arr;
        }
        $type = $combo_config['type'];
        switch ($type)
        {
            case 'enum':
                $data_arr = is_array($combo_config['values']) ? $combo_config['values'] : array();
                break;
            case 'token':
                if ($combo_config['parent_src'] == "Yes" && in_array($mode, array("Add", "Update", "Auto")))
                {
                    $source_field = $combo_config['source_field'];
                    $target_field = $combo_config['target_field'];
                    if (in_array($mode, array("Update", "Auto")) || $data[$source_field] != "")
                    {
                        $parent_src = (is_array($data[$source_field])) ? $data[$source_field] : explode(",", $data[$source_field]);
                        $extra_cond = $this->db->protect($target_field)." IN ('".implode("','", $parent_src)."')";
                    }
                    elseif ($mode == "Add")
                    {
                        $extra_cond = $this->db->protect($target_field)." = ''";
                    }
                    $extra = (trim($extra) != "") ? $extra." AND ".$extra_cond : $extra_cond;
                }
                $data_arr = $this->filter->getTableLevelDropdown($combo_config, $id, $extra, $rtype);
                break;
            case 'table':
                if ($combo_config['parent_src'] == "Yes" && in_array($mode, array("Add", "Update", "Auto")))
                {
                    $source_field = $combo_config['source_field'];
                    $target_field = $combo_config['target_field'];
                    if (in_array($mode, array("Update", "Auto")) || $data[$source_field] != "")
                    {
                        $parent_src = (is_array($data[$source_field])) ? $data[$source_field] : explode(",", $data[$source_field]);
                        $extra_cond = $this->db->protect($target_field)." IN ('".implode("','", $parent_src)."')";
                    }
                    elseif ($mode == "Add")
                    {
                        $extra_cond = $this->db->protect($target_field)." = ''";
                    }
                    $extra = (trim($extra) != "") ? $extra." AND ".$extra_cond : $extra_cond;
                }
                if ($combo_config['parent_child'] == "Yes" && $combo_config['nlevel_child'] == "Yes")
                {
                    $combo_config['main_table'] = $this->model_product_management_v1->table_name;
                    $data_arr = $this->filter->getTreeLevelDropdown($combo_config, $id, $extra, $rtype);
                }
                else
                {
                    if ($combo_config['parent_child'] == "Yes" && $combo_config['parent_field'] != "")
                    {
                        $parent_field = $combo_config['parent_field'];
                        $extra_cond = "(".$this->db->protect($parent_field)." = '0' OR ".$this->db->protect($parent_field)." = '' OR ".$this->db->protect($parent_field)." IS NULL )";
                        if ($mode == "Update" || ($mode == "Search" && $id > 0))
                        {
                            $par_cond .= " AND ".$this->db->protect($combo_config['field_key'])." <> ".$this->db->escape($id);
                        }
                        $extra = (trim($extra) != "") ? $extra." AND ".$extra_cond : $extra_cond;
                    }
                    $data_arr = $this->filter->getTableLevelDropdown($combo_config, $id, $extra, $rtype);
                }
                break;
            case 'phpfn':
                if (method_exists($this->general, $combo_config['function']))
                {
                    $parent_src = '';
                    if ($combo_config['parent_src'] == "Yes" && in_array($mode, array("Add", "Update", "Auto")))
                    {
                        $source_field = $combo_config['source_field'];
                        if (in_array($mode, array("Update", "Auto")) || $data[$source_field] != "")
                        {
                            $parent_src = $data[$source_field];
                        }
                    }
                    $data_arr = $this->general->$combo_config['function']($data[$name], $mode, $id, $data, $parent_src);
                }
                break;
        }
        return $data_arr;
    }

    /**
     * getSelfSwitchToPrint method is used to provide autocomplete for switchto dropdown, which is called through form.
     */
    public function getSelfSwitchTo()
    {
        $params_arr = $this->params_arr;

        $term = strtolower($params_arr['data']['q']);

        $switchto_fields = $this->model_product_management_v1->switchto_fields;
        $extra_cond = $this->model_product_management_v1->extra_cond;

        $concat_fields = $this->db->concat_cast($switchto_fields);
        $search_cond = "(LOWER(".$concat_fields.") LIKE '".$this->db->escape_like_str($term)."%' OR LOWER(".$concat_fields.") LIKE '% ".$this->db->escape_like_str($term)."%')";
        $extra_cond = ($extra_cond == "") ? $search_cond : $extra_cond." AND ".$search_cond;

        $switch_arr = $this->model_product_management_v1->getSwitchTo($extra_cond);
        $html_arr = $this->filter->getChosenAutoJSON($switch_arr, array(), FALSE, "auto");

        $json_array['q'] = $term;
        $json_array['results'] = $html_arr;
        $html_str = json_encode($json_array);

        echo $html_str;
        $this->skip_template_view();
    }

    /**
     * getListOptions method is used to get  dropdown values searching or inline editing in grid listing (select options in html or json string)
     */
    public function getListOptions()
    {
        $params_arr = $this->params_arr;
        $alias_name = $params_arr['alias_name'];
        $rformat = $params_arr['rformat'];
        $id = $params_arr['id'];
        $mode = ($params_arr['mode'] == "Search") ? "Search" : (($params_arr['mode'] == "Update") ? "Update" : "Add");
        $config_arr = $this->model_product_management_v1->getListConfiguration($alias_name);
        $source_field = $config_arr['source_field'];
        $combo_config = $this->dropdown_arr[$source_field];
        $data_arr = array();
        if ($mode == "Update")
        {
            $data_arr = $this->model_product_management_v1->getData(intval($id));
        }
        $combo_arr = $this->getSourceOptions($source_field, $mode, $id, $data_arr[0]);
        if ($rformat == "json")
        {
            $html_str = $this->filter->getChosenAutoJSON($combo_arr, $combo_config, TRUE, "grid");
        }
        else
        {
            if ($combo_config['opt_group'] == "Yes")
            {
                $combo_arr = $this->filter->makeOPTDropdown($combo_arr);
            }
            else
            {
                $combo_arr = $this->filter->makeArrayDropdown($combo_arr);
            }
            $this->dropdown->combo("array", $source_field, $combo_arr, $id);
            $top_option = (in_array($mode, array("Add", "Update")) && $combo_config['default'] == 'Yes') ? "|||" : '';
            $html_str = $this->dropdown->display($source_field, $source_field, ' multiple=true ', $top_option);
        }
        echo $html_str;
        $this->skip_template_view();
    }

    /**
     * childDataAdd method is used to add more inline child module records (relation module records) for add or update form
     */
    public function childDataAdd()
    {

        try
        {
            $params_arr = $this->params_arr;
            $child_module = $params_arr["child_module"];
            $row_index = $params_arr["incNo"];
            $mode = ($params_arr["mode"] == "Update") ? "Update" : "Add";
            $recMode = ($params_arr["recMode"] == "Update") ? "Update" : "Add";
            $rmPopup = ($params_arr["rmPopup"] == "Yes") ? "Yes" : "No";
            $id = $params_arr["id"];
            $child_data = array();
            switch ($child_module)
            {
                case "productimages":
                    $this->load->module("productimages/productimages");
                    $this->load->model("productimages/model_productimages");
                    $child_config["child_module"] = $child_module;
                    $child_config["mode"] = $recMode;
                    if ($recMode == "Update")
                    {
                        $child_mod_data = $this->model_productimages->getData(intval($id), "", "", "", "", "Yes");
                        $child_config["data"][$row_index] = $child_mod_data[0];
                    }
                    $child_config["row_index"] = $row_index;
                    $child_config["perform"] = array(
                        "data",
                        "options",
                    );
                    $module_arr = $this->productimages->getRelationModule($child_config);
                    $child_data[0] = $module_arr["data"][$row_index];
                    $child_conf_arr = $module_arr["config_arr"];
                    $child_opt_arr = $module_arr["opt_arr"];
                    $child_img_html = $module_arr["img_html"];
                    $child_auto_arr = $module_arr["auto_arr"];
                    $child_module_add_file = "product_management_v1_productimages_add_more";
                    break;
            }
            $enc_child_id = $this->general->getAdminEncodeURL($id);
            $render_arr = array(
                "mod_enc_url" => $this->mod_enc_url,
                "mod_enc_mode" => $this->mod_enc_mode,
                "child_module_name" => $child_module,
                "row_index" => $row_index,
                "child_data" => $child_data,
                "child_conf_arr" => $child_conf_arr,
                "child_opt_arr" => $child_opt_arr,
                "child_img_html" => $child_img_html,
                "child_auto_arr" => $child_auto_arr,
                "recMode" => $recMode,
                "popup" => $rmPopup,
                "mode" => $mode,
                "child_id" => $id,
                "enc_child_id" => $enc_child_id,
            );
            $parse_html = $this->parser->parse($child_module_add_file, $render_arr, true);
            echo $parse_html;
        }
        catch(Exception $e)
        {
            $success = 0;
            $msg = $e->getMessage();
            echo $msg;
        }
        $this->skip_template_view();
    }
    /**
     * childDataSave method is used to save inline child module records (relation module records) from add or update form
     * @param array $config_arr config array for saving child module data records.
     * @param boolean $called_func for setting flag to differ function call of url call.
     * @return array $res_arr returns saving data record response
     */
    public function childDataSave($config_arr = array(), $called_func = FALSE)
    {

        try
        {
            $params_arr = $this->params_arr;
            if ($called_func == true)
            {
                $child_module = $config_arr["child_module"];
                $index = $config_arr["index"];
                $id = $config_arr["id"];
                $child_post = $config_arr["data"];
                $main_data = $config_arr["main_data"];
            }
            else
            {
                $child_module = $params_arr["child_module"];
                $index = $params_arr["index"];
                $id = $params_arr["id"];
                $child_post = $params_arr["child"][$child_module];
            }
            switch ($child_module)
            {
                case "productimages":
                    $this->load->module("productimages/productimages");
                    $this->load->model("productimages/model_productimages");
                    $child_config["perform"] = array(
                        "config",
                    );
                    $module_arr = $this->productimages->getRelationModule($child_config);
                    $form_config = $module_arr["form_config"];
                    $data_arr = $this->model_productimages->getData(intval($id));
                    $mode = (is_array($data_arr) && count($data_arr) > 0) ? "Update" : "Add";
                    if ($called_func == true)
                    {
                        $params_arr["parID"] = $main_data[0]["iMstProductsId"];
                    }
                    $child_data = $save_data_arr = $file_data = array();
                    $mpi_mst_products_id = $child_post["mpi_mst_products_id"][$index];
                    $mpi_img_path = $child_post["mpi_img_path"][$index];
                    $mpi_default = $child_post["mpi_default"][$index];

                    $child_data["iMstProductsId"] = $mpi_mst_products_id;
                    $child_data["vImgPath"] = $mpi_img_path;
                    $child_data["eDefault"] = $mpi_default;

                    $child_data["iMstProductsId"] = $params_arr["parID"];
                    if ($mode == "Add")
                    {
                        $id = $res = $this->model_productimages->insert($child_data);
                        if (intval($id) > 0)
                        {
                            $data["iMstProductImgId"] = $id;
                            $save_data_arr["iMstProductImgId"] = $child_data["iMstProductImgId"] = $id;
                            $msg = $this->general->processMessageLabel('ACTION_RECORD_ADDED_SUCCESSFULLY_C46_C46_C33');
                        }
                        else
                        {
                            throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_ADDING_RECORD_C46_C46_C33'));
                        }
                    }
                    elseif ($mode == "Update")
                    {
                        $res = $this->model_productimages->update($child_data, intval($id));
                        if (intval($res) > 0)
                        {
                            $data["iMstProductImgId"] = $id;
                            $save_data_arr["iMstProductImgId"] = $child_data["iMstProductImgId"] = $id;
                            $msg = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                        }
                        else
                        {
                            throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                        }
                    }

                    $file_data["mpi_img_path"]["file_name"] = $mpi_img_path;
                    $file_data["mpi_img_path"]["old_file_name"] = $child_post["old_mpi_img_path"];
                    $file_data["mpi_img_path"]["primary_key"] = "iMstProductImgId";

                    $this->listing->uploadFilesOnSaveForm($file_data, $form_config, $save_data_arr);

                    $success = 1;
                    break;
            }
        }
        catch(Exception $e)
        {
            $success = 0;
            $msg = $e->getMessage();
        }
        $res_arr["success"] = $success;
        $res_arr["message"] = $msg;
        if ($called_func == true)
        {
            $res_arr["id"] = $id;
            return $res_arr;
        }
        else
        {
            $enc_id = $this->general->getAdminEncodeURL($id);
            $res_arr["id"] = $enc_id;
            $res_arr["enc_id"] = $enc_id;
            echo json_encode($res_arr);
            $this->skip_template_view();
        }
    }
    /**
     * childDataDelete method is used to delete inline child module records (relation module records) from add or update form
     * @param array $config_arr config array for saving child module data records.
     * @param boolean $called_func for setting flag to differ function call of url call.
     * @return array $res_arr returns delete data record response
     */
    public function childDataDelete($config_arr = array(), $called_func = FALSE)
    {

        try
        {
            $params_arr = $this->params_arr;
            if ($called_func == true)
            {
                $child_module = $config_arr["child_module"];
                $where_cond = $config_arr["extra_cond"];
            }
            else
            {
                $child_module = $params_arr["child_module"];
                $where_cond = $id = intval($params_arr["id"]);
            }
            switch ($child_module)
            {
                case "productimages":
                    $this->load->module("productimages/productimages");
                    $this->load->model("productimages/model_productimages");
                    if ($called_func == false)
                    {


                    }
                    $parent_join_arr = array(
                        "joins" => array(
                            array(
                                "table_name" => "mst_products",
                                "table_alias" => "mp",
                                "field_name" => "iMstProductsId",
                                "rel_table_name" => "mst_product_img",
                                "rel_table_alias" => "mpi",
                                "rel_field_name" => "iMstProductsId",
                                "join_type" => "left",
                            )
                        )
                    );
                    $res = $this->model_productimages->delete($where_cond, "Yes", $parent_join_arr);
                    if (!$res)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }
                    $msg = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_DELETED_SUCCESSFULLY_C46_C46_C33');
                    $success = 1;
                    break;
            }
        }
        catch(Exception $e)
        {
            $success = 0;
            $msg = $e->getMessage();
        }
        $res_arr["success"] = $success;
        $res_arr["message"] = $msg;
        if ($called_func == true)
        {
            return $res_arr;
        }
        else
        {
            echo json_encode($res_arr);
            $this->skip_template_view();
        }
    }
}
