<?php
            
/**
 * Description of Product Management Extended Controller
 * 
 * @module Extended Product Management
 * 
 * @class HBC_Productmanagement.php
 * 
 * @path application\admin\productmanagement\controllers\HBC_Productmanagement.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 28.10.2015
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
 
Class HBC_Productmanagement extends Productmanagement {
        function __construct() {
    parent::__construct();
	$this->load->model("model_productmanagement");
}

function getStoreAdmin(){
	$return_arr=array();
	$status="0";
	$postArr=$this->input->post();
	$store_id=intval($postArr['store_id']);	
	if($store_id > 0){
	$where = "iMstStoreDetailId = '".$store_id."'";
	$storeArr=$this->model_productmanagement->get_store_owner("iAdminId",$where);
	if(is_array($storeArr) && count($storeArr) > 0){
		$status="1";
		$admin_id=$storeArr[0]['iAdminId'];
	}
	}
	$return_arr['status']=$status;
	$return_arr['admin_id']=$admin_id;
	echo json_encode($return_arr);
}
}
