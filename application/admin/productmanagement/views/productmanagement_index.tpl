<script type="text/javascript">
    $(document).ready(function() {
        //$('.fancybox').fancybox();
        $('#video-tutorial').live('click',function(){
            var video_file_link = $(this).attr('video-data');
            $.fancybox.open({
                padding: 0,
                content: $('#inline_video_block').html(),
                type: "html", // cause you are using html tags
            });
            $('.fancybox-overlay').css('z-index','9999');
            $('video').css({'width':'800px','height':'auto'});
        });
    });
</script>
<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>
<!--Dont Remove this video content-->
<div id="inline_video_block" style="display: none;"><video id="video" controls><source src="<%$this->config->item('upload_url')%>upload_video_tutorial.mp4?autoplay=false" type="video/mp4"></video></div>
<!--End of video content-->
<textarea id="txt_module_help" class="perm-elem-hide"></textarea>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-titleSR">
                <%$this->lang->line('GENERIC_LISTING')%> :: <%$this->lang->line('PRODUCTMANAGEMENT_PRODUCT_MANAGEMENT')%>
                <div class="right">
                    <span class="icon12 icomoon-icon-movie"></span><span style="color:#778695;font-size:14px; cursor: pointer;" id="video-tutorial" video-data="<%$this->config->item('upload_url')%>upload_video_tutorial.mp4">Video Tutorial</span>
                    <a style="color:#778695;font-size:14px;" hijacked="yes" href="<%$this->config->item('upload_url')%>Upload_Guide.pdf" target="_blank" title="Upload Guide" class="nav-active-link"><span class="icon12 icomoon-icon-help"></span>Upload Guide</a>
                </div>
            </div>        
        </h3>
        <div class="header-right-drops">
        </div>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing box gradient">
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div id="scrollable_content" class="scrollable-content top-list-spacing">
        <div class="grid-data-container pad-calc-container">
            <div class="top-list-tab-layout" id="top_list_grid_layout">
            </div>
            <table class="grid-table-view " width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td id="grid_data_col">
                        <div id="pager2"></div>
                        <table id="list2"></table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>        
<input type="hidden" name="selAllRows" value="" id="selAllRows" />
<%javascript%>
$.jgrid.no_legacy_api = true; $.jgrid.useJSON = true;
var el_grid_settings = {}, js_col_model_json = {}, js_col_name_json = {}; 

el_grid_settings['module_name'] = '<%$module_name%>';
el_grid_settings['extra_hstr'] = '<%$extra_hstr%>';
el_grid_settings['extra_qstr'] = '<%$extra_qstr%>';
el_grid_settings['enc_location'] = '<%$enc_loc_module%>';
el_grid_settings['par_module '] = '<%$this->general->getAdminEncodeURL($parMod)%>';
el_grid_settings['par_data'] = '<%$this->general->getAdminEncodeURL($parID)%>';
el_grid_settings['par_field'] = '<%$parField%>';
el_grid_settings['par_type'] = 'parent';
el_grid_settings['add_page_url'] = '<%$mod_enc_url["add"]%>'; 
el_grid_settings['edit_page_url'] =  admin_url+'<%$mod_enc_url["inline_edit_action"]%>?<%$extra_qstr%>';
el_grid_settings['listing_url'] = admin_url+'<%$mod_enc_url["listing"]%>?<%$extra_qstr%>';
el_grid_settings['search_refresh_url'] = admin_url+'<%$mod_enc_url["get_left_search_content"]%>?<%$extra_qstr%>';
el_grid_settings['search_autocomp_url'] = admin_url+'<%$mod_enc_url["get_search_auto_complete"]%>?<%$extra_qstr%>';
el_grid_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
el_grid_settings['auto_complete_url'] = admin_url+'<%$mod_enc_url["get_token_auto_complete"]%>?<%$extra_qstr%>';
el_grid_settings['export_url'] =  admin_url+'<%$mod_enc_url["export"]%>?<%$extra_qstr%>';
el_grid_settings['subgrid_listing_url'] =  admin_url+'<%$mod_enc_url["get_subgrid_block"]%>?<%$extra_qstr%>';
el_grid_settings['jparent_switchto_url'] = admin_url+'<%$parent_switch_cit["url"]%>?<%$extra_qstr%>';

el_grid_settings['admin_rec_arr'] = {};
el_grid_settings['status_arr'] = $.parseJSON('<%$status_array|@json_encode%>');
el_grid_settings['status_lang_arr'] = $.parseJSON('<%$status_array_label|@json_encode%>');

el_grid_settings['hide_add_btn'] = '1';
el_grid_settings['hide_del_btn'] = '1';
el_grid_settings['hide_status_btn'] = '1';
el_grid_settings['hide_export_btn'] = '1';
el_grid_settings['hide_columns_btn'] = 'No';

el_grid_settings['hide_advance_search'] = 'No';
el_grid_settings['hide_search_tool'] = 'No';
el_grid_settings['hide_multi_select'] = 'No';
el_grid_settings['popup_add_form'] = 'No';
el_grid_settings['popup_edit_form'] = 'No';
el_grid_settings['popup_add_size'] = ['75%', '75%'];
el_grid_settings['popup_edit_size'] = ['75%', '75%'];
el_grid_settings['hide_paging_btn'] = 'No';
el_grid_settings['hide_refresh_btn'] = 'No';
el_grid_settings['group_search'] = '';

el_grid_settings['permit_add_btn'] = '<%$add_access%>';
el_grid_settings['permit_del_btn'] = '<%$del_access%>';
el_grid_settings['permit_edit_btn'] = '<%$view_access%>';
el_grid_settings['permit_expo_btn'] = '<%$expo_access%>';

el_grid_settings['default_sort'] = 'mp_date';
el_grid_settings['sort_order'] = 'desc';

el_grid_settings['footer_row'] = 'No';
el_grid_settings['grouping'] = 'No';
el_grid_settings['group_attr'] = {};

el_grid_settings['inline_add'] = 'No';
el_grid_settings['rec_position'] = 'Top';
el_grid_settings['auto_width'] = 'Yes';
el_grid_settings['subgrid'] = 'No';
el_grid_settings['colgrid'] = 'No';
el_grid_settings['rating_allow'] = 'No';
el_grid_settings['listview'] = 'list';
el_grid_settings['filters_arr'] = $.parseJSON('<%$default_filters|@json_encode%>');
el_grid_settings['top_filter'] = [];
el_grid_settings['buttons_arr'] = [{
"name": "add",
"type": "default",
"text": "<%$this->lang->line('GENERIC_ADD_NEW')%>",
"title": "<%$this->lang->line('GENERIC_ADD_NEW')%>",
"icon": "icomoon-icon-plus-2",
"icon_only": "No"
},
{
"name": "del",
"type": "default",
"text": "<%$this->lang->line('GENERIC_DELETE')%>",
"title": "<%$this->lang->line('GENERIC_DELETE_SELECTED_ROW')%>",
"icon": "icomoon-icon-remove-6",
"icon_only": "No"
},
{
"name": "status_active",
"type": "default",
"text": "<%$this->lang->line('GENERIC_ACTIVE')%>",
"title": "<%$this->lang->line('GENERIC_ACTIVE')%>",
"icon": "icomoon-icon-checkmark",
"icon_only": "No"
},
{
"name": "status_inactive",
"type": "default",
"text": "<%$this->lang->line('GENERIC_INACTIVE')%>",
"title": "<%$this->lang->line('GENERIC_INACTIVE')%>",
"icon": "icomoon-icon-blocked",
"icon_only": "No"
},
{
"name": "search",
"type": "default",
"text": "<%$this->lang->line('GENERIC_SEARCH')%>",
"title": "<%$this->lang->line('GENERIC_ADVANCE_SEARCH')%>",
"icon": "icomoon-icon-search-3",
"icon_only": "No"
},
{
"name": "refresh",
"type": "default",
"text": "<%$this->lang->line('GENERIC_SHOW_ALL')%>",
"title": "<%$this->lang->line('GENERIC_SHOW_ALL_LISTING_RECORDS')%>",
"icon": "icomoon-icon-loop-2",
"icon_only": "No"
},
{
"name": "columns",
"type": "default",
"text": "<%$this->lang->line('GENERIC_COLUMNS')%>",
"title": "<%$this->lang->line('GENERIC_HIDE_C47SHOW_COLUMNS')%>",
"icon": "silk-icon-columns",
"icon_only": "No"
},
{
"name": "export",
"type": "default",
"text": "<%$this->lang->line('GENERIC_EXPORT')%>",
"title": "<%$this->lang->line('GENERIC_EXPORT')%>",
"icon": "icomoon-icon-out",
"icon_only": "No"
},
{
"name": "custom_btn_1",
"type": "custom",
"text": "<%$this->lang->line('GENERIC_DOWNLOAD_CSV')%>",
"title": "<%$this->lang->line('GENERIC_DOWNLOAD_CSV')%>",
"icon": "silk-icon-popout",
"icon_only": "No",
"confirm": {
"type": "module",
"module": "<%$this->general->getAdminEncodeURL('productmanagement\/productmanagement')%>",
"url": "<%$this->config->item('admin_url')%>#<%$this->general->getAdminEncodeURL('download\/download\/add')%>|hideCtrl|true|mode|<%$this->general->getAdminEncodeURL('Add')%>",
"id": "id",
"open": "same",
"params": []
}
},
{
"name": "custom_btn_2",
"type": "custom",
"text": "<%$this->lang->line('GENERIC_UPLOAD_CSV')%>",
"title": "<%$this->lang->line('GENERIC_UPLOAD_CSV')%>",
"icon": "silk-icon-popout",
"icon_only": "No",
"confirm": {
"type": "module",
"module": "<%$this->general->getAdminEncodeURL('productmanagement\/productmanagement')%>",
"url": "<%$this->config->item('admin_url')%>#<%$this->general->getAdminEncodeURL('uploadcsv_for_seller\/upload_csv_for_seller\/add')%>|hideCtrl|true|mode|<%$this->general->getAdminEncodeURL('Add')%>",
"id": "id",
"open": "same",
"params": []
}
},
{
"name": "custom_btn_3",
"type": "custom",
"text": "<%$this->lang->line('GENERIC_UPLOAD_BULK_IMAGE')%>",
"title": "<%$this->lang->line('GENERIC_UPLOAD_BULK_IMAGE')%>",
"icon": "silk-icon-popout",
"icon_only": "No",
"confirm": {
"type": "module",
"module": "<%$this->general->getAdminEncodeURL('productmanagement\/productmanagement')%>",
"url": "<%$this->config->item('admin_url')%>#<%$this->general->getAdminEncodeURL('uploadbulkimageforseller\/upload_bulk_image_for_seller\/add')%>|hideCtrl|true|mode|<%$this->general->getAdminEncodeURL('Add')%>",
"id": "id",
"open": "same",
"params": []
}
}];

js_col_name_json = [{
"name": "mp_title",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_PRODUCTS')%>"
},
{
"name": "tpc_mst_categories_id",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_PARENT_CATEGORY')%>"
},
{
"name": "msd_store_name",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_STORE_NAME')%>"
},
{
"name": "mp_default_img",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_DEFAULT_IMG')%>"
},
{
"name": "mp_sku",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_SKU')%>"
},
{
"name": "mp_regular_price",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_REGULAR_PRICE')%>"
},
{
"name": "mp_sale_price",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_SALE_PRICE')%>"
},
{
"name": "mp_difference_per",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_DISCOUNT')%>"
},
{
"name": "mp_stock",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_STOCK')%>"
},
{
"name": "mp_shipping_charge",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_SHIPPING')%>"
},
{
"name": "mp_return_days",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_RETURN_DAYS')%>"
},
{
"name": "mp_status",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_STATUS')%>"
},
{
"name": "mc_title",
"label": "<%$this->lang->line('PRODUCTMANAGEMENT_CATEGORY')%>"
}];

js_col_model_json = [{
"name": "mp_title",
"index": "mp_title",
"label": "<%$list_config['mp_title']['label_lang']%>",
"labelClass": "header-align-left",
"resizable": true,
"width": "<%$list_config['mp_title']['width']%>",
"search": <%if $list_config['mp_title']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['mp_title']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['mp_title']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['mp_title']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['mp_title']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "left",
"edittype": "text",
"editrules": {
"required": true,
"infoArr": {
"required": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_PRODUCT_NAME)
}
}
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_title",
"autocomplete": "off"
},
"sopt": strSearchOpts,
"searchhidden": <%if $list_config['mp_title']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_title",
"placeholder": "",
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['mp_title']['default']%>",
"filterSopt": "bw",
"formatter": formatAdminModuleEditLink,
"unformat": unformatAdminModuleEditLink
},
{
"name": "tpc_mst_categories_id",
"index": "tpc_mst_categories_id",
"label": "<%$list_config['tpc_mst_categories_id']['label_lang']%>",
"labelClass": "header-align-left",
"resizable": true,
"width": "<%$list_config['tpc_mst_categories_id']['width']%>",
"search": <%if $list_config['tpc_mst_categories_id']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['tpc_mst_categories_id']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['tpc_mst_categories_id']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['tpc_mst_categories_id']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['tpc_mst_categories_id']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "left",
"edittype": "text",
"editrules": {
"required": true,
"infoArr": {
"required": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_CATEGORY)
}
}
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "tpc_mst_categories_id",
"autocomplete": "off"
},
"sopt": numSearchOpts,
"searchhidden": <%if $list_config['tpc_mst_categories_id']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "tpc_mst_categories_id",
"placeholder": "",
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['tpc_mst_categories_id']['default']%>",
"filterSopt": "bw"
},
{
"name": "msd_store_name",
"index": "msd_store_name",
"label": "<%$list_config['msd_store_name']['label_lang']%>",
"labelClass": "header-align-left",
"resizable": true,
"width": "<%$list_config['msd_store_name']['width']%>",
"search": <%if $list_config['msd_store_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['msd_store_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['msd_store_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['msd_store_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['msd_store_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "left",
"edittype": "select",
"editrules": {
"infoArr": []
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": null,
"autocomplete": "off"
},
"sopt": strSearchOpts,
"searchhidden": <%if $list_config['msd_store_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": null,
"placeholder": null,
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['msd_store_name']['default']%>",
"filterSopt": "bw"
},
{
"name": "mp_default_img",
"index": "mp_default_img",
"label": "<%$list_config['mp_default_img']['label_lang']%>",
"labelClass": "header-align-center",
"resizable": true,
"width": "<%$list_config['mp_default_img']['width']%>",
"search": <%if $list_config['mp_default_img']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['mp_default_img']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['mp_default_img']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['mp_default_img']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['mp_default_img']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "center",
"edittype": "text",
"editrules": {
"infoArr": []
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_default_img",
"autocomplete": "off"
},
"sopt": strSearchOpts,
"searchhidden": <%if $list_config['mp_default_img']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_default_img",
"placeholder": "",
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['mp_default_img']['default']%>",
"filterSopt": "bw"
},
{
"name": "mp_sku",
"index": "mp_sku",
"label": "<%$list_config['mp_sku']['label_lang']%>",
"labelClass": "header-align-left",
"resizable": true,
"width": "<%$list_config['mp_sku']['width']%>",
"search": <%if $list_config['mp_sku']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['mp_sku']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['mp_sku']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['mp_sku']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['mp_sku']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "left",
"edittype": "text",
"editrules": {
"required": true,
"infoArr": {
"required": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_SKU)
}
}
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_sku",
"autocomplete": "off"
},
"sopt": strSearchOpts,
"searchhidden": <%if $list_config['mp_sku']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_sku",
"placeholder": "",
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['mp_sku']['default']%>",
"filterSopt": "bw"
},
{
"name": "mp_regular_price",
"index": "mp_regular_price",
"label": "<%$list_config['mp_regular_price']['label_lang']%>",
"labelClass": "header-align-right",
"resizable": true,
"width": "<%$list_config['mp_regular_price']['width']%>",
"search": <%if $list_config['mp_regular_price']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['mp_regular_price']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['mp_regular_price']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['mp_regular_price']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['mp_regular_price']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "right",
"edittype": "text",
"editrules": {
"required": true,
"number": true,
"minValue": true,
"infoArr": {
"required": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_REGULAR_PRICE)
},
"number": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_REGULAR_PRICE)
},
"minValue": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MINIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_REGULAR_PRICE)
}
}
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_regular_price",
"autocomplete": "off"
},
"sopt": numSearchOpts,
"searchhidden": <%if $list_config['mp_regular_price']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_regular_price",
"placeholder": "<%$this->lang->line('GENERIC_0_C4600')%>",
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['mp_regular_price']['default']%>",
"filterSopt": "bw",
"formatter": "currency"
},
{
"name": "mp_sale_price",
"index": "mp_sale_price",
"label": "<%$list_config['mp_sale_price']['label_lang']%>",
"labelClass": "header-align-right",
"resizable": true,
"width": "<%$list_config['mp_sale_price']['width']%>",
"search": <%if $list_config['mp_sale_price']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['mp_sale_price']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['mp_sale_price']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['mp_sale_price']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['mp_sale_price']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "right",
"edittype": "text",
"editrules": {
"required": true,
"number": true,
"minValue": true,
"infoArr": {
"required": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_SALE_PRICE)
},
"number": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_SALE_PRICE)
},
"minValue": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MINIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_SALE_PRICE)
}
}
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_sale_price",
"autocomplete": "off"
},
"sopt": numSearchOpts,
"searchhidden": <%if $list_config['mp_sale_price']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_sale_price",
"placeholder": "<%$this->lang->line('GENERIC_0_C4600')%>",
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['mp_sale_price']['default']%>",
"filterSopt": "bw",
"formatter": "currency"
},
{
"name": "mp_difference_per",
"index": "mp_difference_per",
"label": "<%$list_config['mp_difference_per']['label_lang']%>",
"labelClass": "header-align-center",
"resizable": true,
"width": "<%$list_config['mp_difference_per']['width']%>",
"search": <%if $list_config['mp_difference_per']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['mp_difference_per']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['mp_difference_per']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['mp_difference_per']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['mp_difference_per']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "center",
"edittype": "text",
"editrules": {
"infoArr": []
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_difference_per",
"autocomplete": "off"
},
"sopt": numSearchOpts,
"searchhidden": <%if $list_config['mp_difference_per']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_difference_per",
"placeholder": "",
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['mp_difference_per']['default']%>",
"filterSopt": "bw"
},
{
"name": "mp_stock",
"index": "mp_stock",
"label": "<%$list_config['mp_stock']['label_lang']%>",
"labelClass": "header-align-center",
"resizable": true,
"width": "<%$list_config['mp_stock']['width']%>",
"search": <%if $list_config['mp_stock']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['mp_stock']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['mp_stock']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['mp_stock']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['mp_stock']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "center",
"edittype": "text",
"editrules": {
"required": true,
"number": true,
"infoArr": {
"required": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_CURRENT_STOCK)
},
"number": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_CURRENT_STOCK)
}
}
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_stock",
"autocomplete": "off"
},
"sopt": numSearchOpts,
"searchhidden": <%if $list_config['mp_stock']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_stock",
"placeholder": "",
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['mp_stock']['default']%>",
"filterSopt": "bw"
},
{
"name": "mp_shipping_charge",
"index": "mp_shipping_charge",
"label": "<%$list_config['mp_shipping_charge']['label_lang']%>",
"labelClass": "header-align-right",
"resizable": true,
"width": "<%$list_config['mp_shipping_charge']['width']%>",
"search": <%if $list_config['mp_shipping_charge']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['mp_shipping_charge']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['mp_shipping_charge']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['mp_shipping_charge']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['mp_shipping_charge']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "right",
"edittype": "text",
"editrules": {
"number": true,
"infoArr": {
"number": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_SHIPPING)
}
}
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_shipping_charge",
"autocomplete": "off"
},
"sopt": numSearchOpts,
"searchhidden": <%if $list_config['mp_shipping_charge']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_shipping_charge",
"placeholder": "",
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['mp_shipping_charge']['default']%>",
"filterSopt": "bw"
},
{
"name": "mp_return_days",
"index": "mp_return_days",
"label": "<%$list_config['mp_return_days']['label_lang']%>",
"labelClass": "header-align-center",
"resizable": true,
"width": "<%$list_config['mp_return_days']['width']%>",
"search": <%if $list_config['mp_return_days']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['mp_return_days']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['mp_return_days']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['mp_return_days']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['mp_return_days']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "center",
"edittype": "text",
"editrules": {
"number": true,
"infoArr": {
"number": {
"message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTMANAGEMENT_RETURN_DAYS)
}
}
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_return_days",
"autocomplete": "off"
},
"sopt": numSearchOpts,
"searchhidden": <%if $list_config['mp_return_days']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_return_days",
"placeholder": "",
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['mp_return_days']['default']%>",
"filterSopt": "bw"
},
{
"name": "mp_status",
"index": "mp_status",
"label": "<%$list_config['mp_status']['label_lang']%>",
"labelClass": "header-align-center",
"resizable": true,
"width": "<%$list_config['mp_status']['width']%>",
"search": <%if $list_config['mp_status']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['mp_status']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['mp_status']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['mp_status']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['mp_status']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "center",
"edittype": "select",
"editrules": {
"infoArr": []
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_status",
"autocomplete": "off",
"data-placeholder": " ",
"class": "search-chosen-select",
"multiple": "multiple"
},
"sopt": intSearchOpts,
"searchhidden": <%if $list_config['mp_status']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
"dataUrl": <%if $count_arr["mp_status"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=mp_status&mode=<%$mod_enc_mode["Search"]%>&rformat=html'<%/if%>,
"value": <%if $count_arr["mp_status"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["mp_status"]["data"]|@addslashes%>')<%else%>null<%/if%>,
"dataInit": <%if $count_arr['mp_status']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
"ajaxCall": '<%if $count_arr["mp_status"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
"multiple": true
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mp_status",
"dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=mp_status&mode=<%$mod_enc_mode["Update"]%>&rformat=html',
"dataInit": <%if $count_arr['mp_status']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
"ajaxCall": '<%if $count_arr["mp_status"] eq "Yes" %>ajax-call<%/if%>',
"data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTMANAGEMENT_STATUS')%>",
"class": "inline-edit-row chosen-select"
},
"ctrl_type": "dropdown",
"default_value": "<%$list_config['mp_status']['default']%>",
"filterSopt": "in",
"stype": "select"
},
{
"name": "mc_title",
"index": "mc_title",
"label": "<%$list_config['mc_title']['label_lang']%>",
"labelClass": "header-align-left",
"resizable": true,
"width": "<%$list_config['mc_title']['width']%>",
"search": <%if $list_config['mc_title']['search'] eq 'No' %>false<%else%>true<%/if%>,
"sortable": <%if $list_config['mc_title']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
"hidden": <%if $list_config['mc_title']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
"addable": <%if $list_config['mc_title']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
"editable": <%if $list_config['mc_title']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
"align": "left",
"edittype": "text",
"editrules": {
"infoArr": []
},
"searchoptions": {
"attr": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mc_title",
"autocomplete": "off"
},
"sopt": strSearchOpts,
"searchhidden": <%if $list_config['mc_title']['search'] eq 'Yes' %>true<%else%>false<%/if%>
},
"editoptions": {
"aria-grid-id": el_tpl_settings.main_grid_id,
"aria-module-name": "productmanagement",
"aria-unique-name": "mc_title",
"placeholder": "",
"class": "inline-edit-row "
},
"ctrl_type": "textbox",
"default_value": "<%$list_config['mc_title']['default']%>",
"filterSopt": "bw"
}];

initMainGridListing();
createTooltipHeading();
callSwitchToParent();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
<%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
<%$this->css->css_src()%>
<%/if%> 