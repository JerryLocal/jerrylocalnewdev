<ul class="nav nav-tabs">
    <li <%if $module_name eq "productmanagement"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('PRODUCTMANAGEMENT_PRODUCT_MANAGEMENT')%>" 
            <%if $module_name eq "productmanagement"%> 
                href="javascript://"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('productmanagement/productmanagement/add')%>|mode|<%$mod_enc_mode['Update']%>|id|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('PRODUCTMANAGEMENT_PRODUCT_MANAGEMENT')%>
        </a>
    </li>
    <li <%if $module_name eq "product_options_seller"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('PRODUCTMANAGEMENT_PRODUCT_OPTIONS_SELLER')%> <%$this->lang->line('GENERIC_LIST')%>" 
            <%if $module_name eq "product_options_seller"%> 
                href="javascript://"
            <%elseif $module_name eq "productmanagement"%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('productoptionsseller/product_options_seller/index')%>|parMod|<%$this->general->getAdminEncodeURL('productmanagement')%>|parID|<%$this->general->getAdminEncodeURL($data['iMstProductsId'])%>"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('productoptionsseller/product_options_seller/index')%>|parMod|<%$this->general->getAdminEncodeURL('productmanagement')%>|parID|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('PRODUCTMANAGEMENT_PRODUCT_OPTIONS_SELLER')%>
        </a>
    </li>
</ul>            