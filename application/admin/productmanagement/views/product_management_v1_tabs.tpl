<ul class="nav nav-tabs">
    <li <%if $module_name eq "product_management_v1"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('PRODUCT_MANAGEMENT_V1_PRODUCTS_REPORT')%>" 
            <%if $module_name eq "product_management_v1"%> 
                href="javascript://"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('productmanagement/product_management_v1/add')%>|mode|<%$mod_enc_mode['Update']%>|id|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('PRODUCT_MANAGEMENT_V1_PRODUCTS_REPORT')%>
        </a>
    </li>
    <li <%if $module_name eq "product_options_seller"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('PRODUCT_MANAGEMENT_V1_PRODUCT_OPTIONS_SELLER')%> <%$this->lang->line('GENERIC_LIST')%>" 
            <%if $module_name eq "product_options_seller"%> 
                href="javascript://"
            <%elseif $module_name eq "product_management_v1"%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('productoptionsseller/product_options_seller/index')%>|parMod|<%$this->general->getAdminEncodeURL('product_management_v1')%>|parID|<%$this->general->getAdminEncodeURL($data['iMstProductsId'])%>"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('productoptionsseller/product_options_seller/index')%>|parMod|<%$this->general->getAdminEncodeURL('product_management_v1')%>|parID|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('PRODUCT_MANAGEMENT_V1_PRODUCT_OPTIONS_SELLER')%>
        </a>
    </li>
</ul>            