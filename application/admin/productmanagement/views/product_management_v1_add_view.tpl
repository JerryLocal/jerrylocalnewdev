<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('PRODUCT_MANAGEMENT_V1_PRODUCTS_REPORT')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','PRODUCT_MANAGEMENT_V1_PRODUCTS_REPORT')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-tab-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="product_management_v1" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
        <div id="ad_form_outertab" class="module-navigation-tabs">
            <%if $tabing_allow eq true%>
                <%if $mode eq "Update"%>
                    <%include file="product_management_v1_tabs.tpl" %>
                <%/if%>
            <%/if%>
        </div>
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing top-frm-block-spacing">
        <div id="product_management_v1" class="frm-view-block frm-stand-view">
            <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
            <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
            <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
            <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
            <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
            <input type="hidden" name="sys_custom_field_2" id="sys_custom_field_2" value="<%$data['sys_custom_field_2']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="tpc_mst_products_id" id="tpc_mst_products_id" value="<%$data['tpc_mst_products_id']%>"  class='ignore-valid ' />
            <input type="hidden" name="mpistoreid" id="mpistoreid" value="<%$data['mpistoreid']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_modify_date" id="mp_modify_date" value="<%$data['mp_modify_date']%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
            <input type="hidden" name="mp_rating_avg" id="mp_rating_avg" value="<%$data['mp_rating_avg']%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_date" id="mp_date" value="<%$data['mp_date']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_low_avl_limit_notifcation" id="mp_low_avl_limit_notifcation" value="<%$data['mp_low_avl_limit_notifcation']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_total_rate" id="mp_total_rate" value="<%$data['mp_total_rate']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_wishlist_state" id="mp_wishlist_state" value="<%$data['mp_wishlist_state']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="sys_custom_field_1" id="sys_custom_field_1" value="<%$data['sys_custom_field_1']%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_sale_state" id="mp_sale_state" value="<%$data['mp_sale_state']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_view_state" id="mp_view_state" value="<%$data['mp_view_state']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_default_img" id="mp_default_img" value="<%$data['mp_default_img']|@htmlentities%>"  class='ignore-valid ' />
            <textarea style="display:none;" name="mp_search_keyword" id="mp_search_keyword"  class='ignore-valid ' ><%$data['mp_search_keyword']%></textarea>
            <div class="main-content-block" id="main_content_block">
                <div style="width:98%;" class="frm-block-layout pad-calc-container">
                    <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                        <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('PRODUCT_MANAGEMENT_V1_PRODUCTS_REPORT')%></h4></div>
                        <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                            <div class="form-row row-fluid" id="cc_sh_mp_admin_id2">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_STORE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mp_admin_id2'], $opt_arr['mp_admin_id2'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_tpc_mst_categories_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_CATEGORY')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
                                    <strong><%$this->general->displayKeyValueData($data['mp_admin_id2'], $opt_arr['mp_admin_id2'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_title">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_PRODUCT_NAME')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_title']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_sku">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_SKU')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_sku']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_regular_price">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_REGULAR_PRICE')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_regular_price']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_sale_price">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_SALE_PRICE')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_sale_price']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_allow_max_purchase">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_ALLOW_MAX_PURCHASE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_allow_max_purchase']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_stock">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_CURRENT_STOCK')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_stock']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_difference_per">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_DIFFERENCE_PER')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
                                    <strong><%$data['mp_difference_per']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_free_shipping">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_FREE_SHIPPING')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mp_free_shipping'], $opt_arr['mp_free_shipping'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_shipping_charge">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_SHIPPING_CHARGE')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_shipping_charge']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_do_return">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_DO_RETURN')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mp_do_return'], $opt_arr['mp_do_return'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_return_days">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_RETURN_DAYS')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_return_days']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_short_description">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_SHORT_DESCRIPTION')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_short_description']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_description">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_DESCRIPTION')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  frm-editor-layout ">
                                    <strong><%$data['mp_description']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_status">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_STATUS')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mp_status'], $opt_arr['mp_status'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_search_tag">
                                <label class="form-label span3">
                                    <%$this->lang->line('PRODUCT_MANAGEMENT_V1_SEARCH_KEYWORDS')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_search_tag']%></strong>
                                </div>
                            </div>
                            <%assign var="child_module_name" value="productimages"%>
                            <div class="box form-child-table" id="child_module_productimages">
                                <%assign var="child_data" value=$child_assoc_data[$child_module_name]%>
                                <%assign var="child_conf_arr" value=$child_assoc_conf[$child_module_name]%>
                                <%assign var="child_opt_arr" value=$child_assoc_opt[$child_module_name]%>
                                <%assign var="child_img_html" value=$child_assoc_img[$child_module_name]%>
                                <%assign var="child_auto_arr" value=$child_assoc_auto[$child_module_name]%>
                                <%if $child_conf_arr["recMode"] eq "Update"%>
                                    <%assign var="child_cnt" value=$child_data|@count%>
                                    <%assign var="recMode" value="Update"%>
                                <%else%>
                                    <%assign var="child_cnt" value="1"%>
                                    <%assign var="recMode" value="Add"%>
                                <%/if%>
                                <%assign var="popup" value=$child_conf_arr["popup"]%>
                                <div class="title">
                                    <input type="hidden" name="childModule[]" id="childModule_<%$child_module_name%>" value="<%$child_module_name%>" />
                                    <input type="hidden" name="childModuleParField[<%$child_module_name%>]" id="childModuleParField_<%$child_module_name%>" value="iMstProductsId"/>
                                    <input type="hidden" name="childModuleParData[<%$child_module_name%>]" id="childModuleParData_<%$child_module_name%>" value="<%$this->general->getAdminEncodeURL($data['iMstProductsId'])%>"/>
                                    <input type="hidden" name="childModuleType[<%$child_module_name%>]" id="childModuleType_<%$child_module_name%>" value="Table"/>
                                    <input type="hidden" name="childModuleLayout[<%$child_module_name%>]" id="childModuleLayout_<%$child_module_name%>" value="Column"/>
                                    <input type="hidden" name="childModuleCnt[<%$child_module_name%>]" id="childModuleCnt_<%$child_module_name%>" value="<%$child_cnt%>" />
                                    <input type="hidden" name="childModuleInc[<%$child_module_name%>]" id="childModuleInc_<%$child_module_name%>" value="<%$child_cnt%>" />
                                    <input type="hidden" name="childModulePopup[<%$child_module_name%>]" id="childModulePopup_<%$child_module_name%>" value="<%$popup%>" />
                                    <input type="hidden" name="childModuleUploadURL[<%$child_module_name%>]" id="childModuleUploadURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['upload_form_file']%>" />
                                    <input type="hidden" name="childModuleChosenURL[<%$child_module_name%>]" id="childModuleChosenURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['get_chosen_auto_complete']%>" />
                                    <input type="hidden" name="childModuleParentURL[<%$child_module_name%>]" id="childModuleParentURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['parent_source_options']%>" />
                                    <input type="hidden" name="childModuleTokenURL[<%$child_module_name%>]" id="childModuleTokenURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['get_token_auto_complete']%>" />
                                    <input type="hidden" name="childModuleShowHide[<%$child_module_name%>]" id="childModuleShowHide_<%$child_module_name%>" value="Yes" />
                                    <h4>
                                        <span class="icon12 icomoon-icon-equalizer-2"></span><span><%$this->lang->line('PRODUCT_MANAGEMENT_V1_PRODUCT_IMAGES')%></span>
                                        <span style="display:none;margin-left:32%" id="ajax_loader_childModule_<%$child_module_name%>"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
                                        <div class="box-addmore right">
                                            <a class="btn btn-success" href="javascript://" onclick="getChildModuleAjaxTable('<%$mod_enc_url.child_data_add%>','<%$child_module_name%>', '<%$mode%>')" title="<%$this->lang->line('GENERIC_ADD_NEW')%>">
                                                <span class="icon12 icomoon-icon-cog-2"></span><strong><%$this->lang->line('GENERIC_ADD_NEW')%></strong>
                                            </a>
                                        </div>
                                    </h4>
                                    <a href="javascript://" class="minimize" style="display: none;"></a>
                                </div>
                                <div class="content noPad form-table-row" style="display: block;">
                                    <table class="responsive table table-bordered" id="tbl_child_module_<%$child_module_name%>">
                                        <thead>
                                            <tr>
                                                <th width="3%">#</th>
                                                <th width="45%"><%$this->lang->line('PRODUCTIMAGES_IMG_PATH')%>  </th>
                                                <th width="45%"><%$this->lang->line('PRODUCTIMAGES_DEFAULT')%>  </th>
                                                <th width="7%"><div align="center">Actions</div></th>
                                            </tr>
                                        </thead>
                                        <tbody id="add_child_module_<%$child_module_name%>">
                                            <tr class="ch-mod-firstrow">
                                                <td width="3%"></td> 
                                                <td width="45%"></td>
                                                <td width="45%"></td> <td width="7%"></td>
                                            </tr>
                                            <%section name=i loop=$child_cnt%>
                                                <%assign var="row_index" value=$smarty.section.i.index%>
                                                <%assign var="row_number" value=$smarty.section.i.iteration%>
                                                <%assign var="child_id" value=$child_data[i]['iMstProductImgId']%>
                                                <%assign var="enc_child_id" value=$this->general->getAdminEncodeURL($child_id)%>
                                                <tr id="tr_child_row_<%$child_module_name%>_<%$row_index%>">
                                                    <td class="row-num-child">
                                                        <span class="row-num-span"><%$row_number%></span>
                                                        <input type="hidden" name="child[productimages][id][<%$row_index%>]" id="child_productimages_id_<%$row_index%>" value="<%$child_id%>" />
                                                        <input type="hidden" name="child[productimages][enc_id][<%$row_index%>]" id="child_productimages_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
                                                        <input type="hidden" name="child[productimages][mpi_mst_products_id][<%$row_index%>]" id="child_productimages_mpi_mst_products_id_<%$row_index%>" value="<%$child_data[i]['mpi_mst_products_id']%>"  class='ignore-valid ' />
                                                    </td>
                                                    <td>
                                                        <div class=" " id="ch_productimages_cc_sh_mpi_img_path_<%$row_index%>">
                                                            <div  class='btn-uploadify frm-size-medium' >
                                                                <input type="hidden" value="<%$child_data[i]['mpi_img_path']%>" name="child[productimages][old_mpi_img_path][<%$row_index%>]" id="child_productimages_old_mpi_img_path_<%$row_index%>" />
                                                                <input type="hidden" value="<%$child_data[i]['mpi_img_path']%>" name="child[productimages][mpi_img_path][<%$row_index%>]" id="child_productimages_mpi_img_path_<%$row_index%>"  />
                                                                <input type="hidden" value="<%$child_data[i]['mpi_img_path']%>" name="child[productimages][temp_mpi_img_path][<%$row_index%>]" id="child_productimages_temp_mpi_img_path_<%$row_index%>" />
                                                                <div id="upload_drop_zone_child_productimages_mpi_img_path_<%$row_index%>" class="upload-drop-zone"></div>
                                                                <div class="uploader upload-src-zone">
                                                                    <input type="file" name="uploadify_child[productimages][mpi_img_path][<%$row_index%>]" id="uploadify_child_productimages_mpi_img_path_<%$row_index%>" title="<%$this->lang->line('PRODUCTIMAGES_IMG_PATH')%>" />
                                                                    <span class="filename" id="preview_child_productimages_mpi_img_path_<%$row_index%>"></span>
                                                                    <span class="action">Choose File</span>
                                                                </div>
                                                            </div>
                                                            <div class='upload-image-btn'>
                                                                <%$child_img_html[$row_index]['mpi_img_path']%>
                                                            </div> 
                                                            <span class="input-comment">
                                                                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
                                                            </span>
                                                            <div class='clear'>
                                                                <div id='progress_child_productimages_mpi_img_path_<%$row_index%>' class='upload-progress progress progress-striped active'>
                                                                    <div class='bar' id='practive_child_productimages_mpi_img_path_<%$row_index%>'></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <label class='error' id='child_productimages_mpi_img_path_<%$row_index%>Err'></label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class=" " id="ch_productimages_cc_sh_mpi_default_<%$row_index%>">
                                                            <%assign var="opt_selected" value=$child_data[i]['mpi_default']%>
                                                            <%$this->dropdown->display("child_productimages_mpi_default_<%$row_index%>","child[productimages][mpi_default][<%$row_index%>]","  title='<%$this->lang->line('PRODUCTIMAGES_DEFAULT')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTIMAGES_DEFAULT')%>'  ","|||","",$opt_selected,"child_productimages_mpi_default_$row_index")%>  
                                                        </div>
                                                        <div>
                                                            <label class='error' id='child_productimages_mpi_default_<%$row_index%>Err'></label>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="controls center">
                                                            <%if $mode eq "Update"%>
                                                                <a onclick="saveChildModuleSingleData('<%$mod_enc_url.child_data_save%>', '<%$mod_enc_url.child_data_add%>', '<%$child_module_name%>', '<%$recMode%>', '<%$row_index%>', '<%$enc_child_id%>', 'No', '<%$mode%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_SAVE')%>">
                                                                    <span class="icon14 icomoon-icon-disk"></span>
                                                                </a>
                                                            <%/if%>
                                                            <%if $recMode eq "Update"%>
                                                                <a onclick="deleteChildModuleSingleData('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>','<%$enc_child_id%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                                                                    <span class="icon16 icomoon-icon-remove"></span>
                                                                </a>
                                                            <%else%>
                                                                <a onclick="deleteChildModuleRow('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                                                                    <span class="icon16 icomoon-icon-remove"></span>
                                                                </a>
                                                            <%/if%>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <%javascript%>
                                                <%if $child_auto_arr[$row_index]|@is_array && $child_auto_arr[$row_index]|@count gt 0%>
                                                    var $child_chosen_auto_complete_url = admin_url+""+$("#childModuleChosenURL_<%$child_module_name%>").val()+"?";
                                                    <%foreach name=i from=$child_auto_arr[$row_index] item=v key=k%>
                                                        if($("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").is("select")){
                                                        $("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").ajaxChosen({
                                                        dataType: "json",
                                                        type: "POST",
                                                        url: $child_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$recMode]%>&id=<%$enc_child_id%>"
                                                        },{
                                                        loadingImg: admin_image_url+"chosen-loading.gif"
                                                        });
                                                    }
                                                <%/foreach%>
                                            <%/if%>
                                            <%/javascript%>
                                        <%/section%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>        
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {
        "productimages": {
            "mpi_mst_products_id": {
                "type": "dropdown",
                "vUniqueName": "mpi_mst_products_id",
                "editrules": {
                    "infoArr": []
                }
            },
            "mpi_img_path": {
                "type": "file",
                "vUniqueName": "mpi_img_path",
                "editrules": {
                    "infoArr": []
                }
            },
            "mpi_default": {
                "type": "dropdown",
                "vUniqueName": "mpi_default",
                "editrules": {
                    "infoArr": []
                }
            }
        }
    };
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js("admin/custom/product_management_exntended.js")%><%$this->css->add_css("custom/product_managment_extended.css")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
