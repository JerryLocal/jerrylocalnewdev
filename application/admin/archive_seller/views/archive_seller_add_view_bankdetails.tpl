<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="3"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="bankdetails"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="bankdetails" />
<input type="hidden" name="sys_custom_field_1" id="sys_custom_field_1" value="<%$data['sys_custom_field_1']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_2" id="sys_custom_field_2" value="<%$data['sys_custom_field_2']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_3" id="sys_custom_field_3" value="<%$data['sys_custom_field_3']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_accept_term" id="msd_accept_term" value="<%$data['msd_accept_term']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_4" id="sys_custom_field_4" value="<%$data['sys_custom_field_4']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_5" id="sys_custom_field_5" value="<%$data['sys_custom_field_5']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_6" id="sys_custom_field_6" value="<%$data['sys_custom_field_6']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_7" id="sys_custom_field_7" value="<%$data['sys_custom_field_7']|@htmlentities%>"  class='ignore-valid ' />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_name">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NAME')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_bank_account_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_no">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NO')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->getPhoneMaskedView('99-9999-9999999-999',$data['msd_bank_account_no'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_g_s_t_no">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_G_S_T_NO')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->getPhoneMaskedView('999-999-999',$data['msd_g_s_t_no'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_g_s_t_i_r_d_proof">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_G_S_T_I_R_D_PROOF')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <%$img_html['msd_g_s_t_i_r_d_proof']%>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_proof">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_PROOF')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <%$img_html['msd_bank_account_proof']%>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_OTHER_PROOF')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <%$img_html['msd_other_proof']%>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof_detail">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_OTHER_PROOF_DETAIL')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_other_proof_detail']%></strong>
        </div>
    </div>
</div>