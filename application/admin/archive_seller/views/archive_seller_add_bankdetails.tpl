<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="bankdetails"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="bankdetails" />
<input type="hidden" name="sys_custom_field_1" id="sys_custom_field_1" value="<%$data['sys_custom_field_1']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_2" id="sys_custom_field_2" value="<%$data['sys_custom_field_2']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_3" id="sys_custom_field_3" value="<%$data['sys_custom_field_3']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_accept_term" id="msd_accept_term" value="<%$data['msd_accept_term']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_4" id="sys_custom_field_4" value="<%$data['sys_custom_field_4']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_5" id="sys_custom_field_5" value="<%$data['sys_custom_field_5']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_6" id="sys_custom_field_6" value="<%$data['sys_custom_field_6']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_7" id="sys_custom_field_7" value="<%$data['sys_custom_field_7']|@htmlentities%>"  class='ignore-valid ' />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_name">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NAME')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_bank_account_name']|@htmlentities%>" name="msd_bank_account_name" id="msd_bank_account_name" title="<%$this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NAME')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_bank_account_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_no">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NO')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" format="99-9999-9999999-999" value="<%$data['msd_bank_account_no']%>" name="msd_bank_account_no" id="msd_bank_account_no" title="<%$this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NO')%>"  class='frm-phone-number frm-size-medium'  style='width:auto;' />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_bank_account_noErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_g_s_t_no">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_G_S_T_NO')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" format="999-999-999" value="<%$data['msd_g_s_t_no']%>" name="msd_g_s_t_no" id="msd_g_s_t_no" title="<%$this->lang->line('ARCHIVE_SELLER_G_S_T_NO')%>"  class='frm-phone-number frm-size-medium'  style='width:auto;' />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_g_s_t_noErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_g_s_t_i_r_d_proof">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_G_S_T_I_R_D_PROOF')%>
        </label> 
        <div class="form-right-div   ">
            <div  class='btn-uploadify frm-size-medium' >
                <input type="hidden" value="<%$data['msd_g_s_t_i_r_d_proof']%>" name="old_msd_g_s_t_i_r_d_proof" id="old_msd_g_s_t_i_r_d_proof" />
                <input type="hidden" value="<%$data['msd_g_s_t_i_r_d_proof']%>" name="msd_g_s_t_i_r_d_proof" id="msd_g_s_t_i_r_d_proof"  />
                <input type="hidden" value="<%$data['msd_g_s_t_i_r_d_proof']%>" name="temp_msd_g_s_t_i_r_d_proof" id="temp_msd_g_s_t_i_r_d_proof"  />
                <div id="upload_drop_zone_msd_g_s_t_i_r_d_proof" class="upload-drop-zone"></div>
                <div class="uploader upload-src-zone">
                    <input type="file" name="uploadify_msd_g_s_t_i_r_d_proof" id="uploadify_msd_g_s_t_i_r_d_proof" title="<%$this->lang->line('ARCHIVE_SELLER_G_S_T_I_R_D_PROOF')%>" />
                    <span class="filename" id="preview_msd_g_s_t_i_r_d_proof"></span>
                    <span class="action">Choose File</span>
                </div>
            </div>
            <div class='upload-image-btn'>
                <%$img_html['msd_g_s_t_i_r_d_proof']%>
            </div>
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
            </span>
            <div class='clear upload-progress' id='progress_msd_g_s_t_i_r_d_proof'>
                <div class='upload-progress-bar progress progress-striped active'>
                    <div class='bar' id='practive_msd_g_s_t_i_r_d_proof'></div>
                </div>
            </div>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_g_s_t_i_r_d_proofErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_proof">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_PROOF')%>
        </label> 
        <div class="form-right-div   ">
            <div  class='btn-uploadify frm-size-medium' >
                <input type="hidden" value="<%$data['msd_bank_account_proof']%>" name="old_msd_bank_account_proof" id="old_msd_bank_account_proof" />
                <input type="hidden" value="<%$data['msd_bank_account_proof']%>" name="msd_bank_account_proof" id="msd_bank_account_proof"  />
                <input type="hidden" value="<%$data['msd_bank_account_proof']%>" name="temp_msd_bank_account_proof" id="temp_msd_bank_account_proof"  />
                <div id="upload_drop_zone_msd_bank_account_proof" class="upload-drop-zone"></div>
                <div class="uploader upload-src-zone">
                    <input type="file" name="uploadify_msd_bank_account_proof" id="uploadify_msd_bank_account_proof" title="<%$this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_PROOF')%>" />
                    <span class="filename" id="preview_msd_bank_account_proof"></span>
                    <span class="action">Choose File</span>
                </div>
            </div>
            <div class='upload-image-btn'>
                <%$img_html['msd_bank_account_proof']%>
            </div>
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
            </span>
            <div class='clear upload-progress' id='progress_msd_bank_account_proof'>
                <div class='upload-progress-bar progress progress-striped active'>
                    <div class='bar' id='practive_msd_bank_account_proof'></div>
                </div>
            </div>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_bank_account_proofErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_OTHER_PROOF')%>
        </label> 
        <div class="form-right-div   ">
            <div  class='btn-uploadify frm-size-medium' >
                <input type="hidden" value="<%$data['msd_other_proof']%>" name="old_msd_other_proof" id="old_msd_other_proof" />
                <input type="hidden" value="<%$data['msd_other_proof']%>" name="msd_other_proof" id="msd_other_proof"  />
                <input type="hidden" value="<%$data['msd_other_proof']%>" name="temp_msd_other_proof" id="temp_msd_other_proof"  />
                <div id="upload_drop_zone_msd_other_proof" class="upload-drop-zone"></div>
                <div class="uploader upload-src-zone">
                    <input type="file" name="uploadify_msd_other_proof" id="uploadify_msd_other_proof" title="<%$this->lang->line('ARCHIVE_SELLER_OTHER_PROOF')%>" />
                    <span class="filename" id="preview_msd_other_proof"></span>
                    <span class="action">Choose File</span>
                </div>
            </div>
            <div class='upload-image-btn'>
                <%$img_html['msd_other_proof']%>
            </div>
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
            </span>
            <div class='clear upload-progress' id='progress_msd_other_proof'>
                <div class='upload-progress-bar progress progress-striped active'>
                    <div class='bar' id='practive_msd_other_proof'></div>
                </div>
            </div>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_other_proofErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof_detail">
        <label class="form-label span3">
            <%$this->lang->line('ARCHIVE_SELLER_OTHER_PROOF_DETAIL')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_other_proof_detail']|@htmlentities%>" name="msd_other_proof_detail" id="msd_other_proof_detail" title="<%$this->lang->line('ARCHIVE_SELLER_OTHER_PROOF_DETAIL')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_other_proof_detailErr'></label></div>
    </div>
</div>
<div align="right" class="btn-below-spacing">
    <div style="float:left;">
        <input type="button" value="Prev" name="ctrlprev_1_1_3" class="btn" onclick="return getLoadAdminTab('1','1','2','businessaccountdetails')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Prev" name="ctrlsaveprev_1_1_3" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','2','businessaccountdetails')"/>
    </div>
    <div style="float:right;">
        <input type="submit" value="Save" name="ctrlsave_1_1_3" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','3','bankdetails')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Next" name="ctrlsavenext_1_1_3" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','4','aboutstore')" />
        &nbsp;&nbsp;
        <input type="button" value="Next" name="ctrlnext_1_1_3" class="btn" onclick="return getNextAdminTab('1','1','3','4','aboutstore')" />
    </div>
    <div class="clear"></div>
</div>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initAdminTabRenderJSScript(eleObj){
Project.modules.archive_seller.initEvents("bankdetails");
callGoogleMapEvents();
}
<%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
<%foreach name=i from=$auto_arr item=v key=k%>
    if($("#<%$k%>").is("select")){
    $("#<%$k%>").ajaxChosen({
    dataType: "json",
    type: "POST",
    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
    },{
    loadingImg: admin_image_url+"chosen-loading.gif"
    });
}
<%/foreach%>
<%/if%>        
<%/javascript%>