<?php
/**
 * Description of Archive Seller Model
 *
 * @module Archive Seller
 *
 * @class model_archive_seller.php
 *
 * @path application\admin\archive_seller\models\model_archive_seller.php
 *
 * @author Steve Smith
 *
 * @date 16.12.2015
 */

class Model_archive_seller extends CI_Model {
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $multi_lingual;
    public $physical_data_remove;
    //
    public $listing_data;
    public $rec_per_page;
    public $message;

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->table_name = "mod_admin";
        $this->table_alias = "ma";
        $this->primary_key = "iAdminId";
        $this->primary_alias = "ma_admin_id";
        $this->physical_data_remove = "Yes";
        $this->grid_fields = array(
            "msd_store_code",
            "msd_store_name",
            "msd_contact_name",
            "ma_user_name",
            "msd_contact_number",
            "msd_status",
            "msd_company_name",
            "msd_website_url",
            "msd_bank_account_no",
            "msd_bank_account_name",
            "msd_g_s_t_no",
            "msd_date",
            "ma_name",
        );
        $this->join_tables = array(
            array(
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iAdminId",
                "rel_table_name" => "mod_admin",
                "rel_table_alias" => "ma",
                "rel_field_name" => "iAdminId",
                "join_type" => "left",
                "extra_condition" => "",
            )
        );
        $this->extra_cond = "ma.iGroupId ='3' AND ma.eStatus ='Inactive'";
        $this->groupby_cond = array();
        $this->having_cond = "";
        $this->orderby_cond = array();
        $this->unique_type = "OR";
        $this->unique_fields = array(
            "vName",
            "vEmail",
        );
        $this->switchto_fields = array();
        $this->default_filters = array();
        $this->search_config = array();
        $this->relation_modules = array();
        $this->deletion_modules = array();
        $this->print_rec = "No";
        $this->multi_lingual = "No";

        $this->rec_per_page = $this->config->item('REC_LIMIT');
    }

    /**
     * insert method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    public function insert($data = array()) {
        $this->db->insert($this->table_name, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    /**
     * update method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while updating records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function update($data = array(), $where = '', $alias = "No", $join = "No") {
        if ($alias == "Yes") {
            if ($join == "Yes") {
                $join_tbls = $this->addJoinTables("NR");
            }
            if (trim($join_tbls) != '') {
                $set_cond = array();
                foreach ($data as $key => $val) {
                    $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                }
                if (is_numeric($where)) {
                    $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                } elseif ($where) {
                    $extra_cond = " WHERE ".$where;
                } else {
                    return FALSE;
                }
                $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
                $res = $this->db->query($update_query);
            } else {
                if (is_numeric($where)) {
                    $this->db->where($this->table_alias.".".$this->primary_key, $where);
                } elseif ($where) {
                    $this->db->where($where, FALSE, FALSE);
                } else {
                    return FALSE;
                }
                $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
            }
        } else {
            if (is_numeric($where)) {
                $this->db->where($this->primary_key, $where);
            } elseif ($where) {
                $this->db->where($where, FALSE, FALSE);
            } else {
                return FALSE;
            }
            $res = $this->db->update($this->table_name, $data);
        }
        return $res;
    }

    /**
     * delete method is used to delete data records from the database table.
     * @param string $where where is the query condition for deletion.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while deleting records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function delete($where = "", $alias = "No", $join = "No") {
        if ($this->config->item('PHYSICAL_RECORD_DELETE') && $this->physical_data_remove == 'No') {
            if ($alias == "Yes") {
                if (is_array($join['joins']) && count($join['joins'])) {
                    $join_tbls = '';
                    if ($join['list'] == "Yes") {
                        $join_tbls = $this->addJoinTables("NR");
                    }
                    $join_tbls .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                } elseif ($join == "Yes") {
                    $join_tbls = $this->addJoinTables("NR");
                }
                $data = $this->general->getPhysicalRecordUpdate($this->table_alias);
                if (trim($join_tbls) != '') {
                    $set_cond = array();
                    foreach ($data as $key => $val) {
                        $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                    }
                    if (is_numeric($where)) {
                        $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                    } elseif ($where) {
                        $extra_cond = " WHERE ".$where;
                    } else {
                        return FALSE;
                    }
                    $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
                    $res = $this->db->query($update_query);
                } else {
                    if (is_numeric($where)) {
                        $this->db->where($this->table_alias.".".$this->primary_key, $where);
                    } elseif ($where) {
                        $this->db->where($where, FALSE, FALSE);
                    } else {
                        return FALSE;
                    }
                    $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
                }
            } else {
                if (is_numeric($where)) {
                    $this->db->where($this->primary_key, $where);
                } elseif ($where) {
                    $this->db->where($where, FALSE, FALSE);
                } else {
                    return FALSE;
                }
                $data = $this->general->getPhysicalRecordUpdate();
                $res = $this->db->update($this->table_name, $data);
            }
        } else {
            if ($alias == "Yes") {
                $del_query = "DELETE ".$this->db->protect($this->table_alias).".* FROM ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias);
                if (is_array($join['joins']) && count($join['joins'])) {
                    if ($join['list'] == "Yes") {
                        $del_query .= $this->addJoinTables("NR");
                    }
                    $del_query .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                } elseif ($join == "Yes") {
                    $del_query .= $this->addJoinTables("NR");
                }
                if (is_numeric($where)) {
                    $del_query .= " WHERE ".$this->db->protect($this->table_alias).".".$this->db->protect($this->primary_key)." = ".$this->db->escape($where);
                } elseif ($where) {
                    $del_query .= " WHERE ".$where;
                } else {
                    return FALSE;
                }
                $res = $this->db->query($del_query);
            } else {
                if (is_numeric($where)) {
                    $this->db->where($this->primary_key, $where);
                } elseif ($where) {
                    $this->db->where($where, FALSE, FALSE);
                } else {
                    return FALSE;
                }
                $res = $this->db->delete($this->table_name);
            }
        }
        return $res;
    }

    /**
     * getData method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @param boolean $having_cond having cond is the query condition for getting conditional data.
     * @param boolean $list list is to differ listing fields or form fields.
     * @return array $data_arr returns data records array.
     */
    public function getData($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No", $having_cond = '', $list = FALSE) {
        if (is_array($fields)) {
            $this->listing->addSelectFields($fields);
        } elseif ($fields != "") {
            $this->db->select($fields);
        } elseif ($list == TRUE) {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
            if ($this->primary_alias != "") {
                $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
            }
            $this->db->select("msd.vStoreCode AS msd_store_code");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("msd.vContactName AS msd_contact_name");
            $this->db->select("ma.vUserName AS ma_user_name");
            $this->db->select("msd.vContactNumber AS msd_contact_number");
            $this->db->select("msd.eStatus AS msd_status");
            $this->db->select("msd.vCompanyName AS msd_company_name");
            $this->db->select("msd.vWebsiteUrl AS msd_website_url");
            $this->db->select("msd.vBankAccountNo AS msd_bank_account_no");
            $this->db->select("msd.vBankAccountName AS msd_bank_account_name");
            $this->db->select("msd.vGSTNo AS msd_g_s_t_no");
            $this->db->select("msd.dDate AS msd_date");
            $this->db->select("ma.vName AS ma_name");
        } else {
            $this->db->select("ma.iAdminId AS iAdminId");
            $this->db->select("ma.iAdminId AS ma_admin_id");
            $this->db->select("msd.iMstStoreDetailId AS iMstStoreDetailId");
            $this->db->select("msd.vStoreCode AS msd_store_code");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("msd.vStoreLogo AS msd_store_logo");
            $this->db->select("msd.tStoreDescription AS msd_store_description");
            $this->db->select("ma.vEmail AS ma_email");
            $this->db->select("ma.vUserName AS ma_user_name");
            $this->db->select("ma.vPassword AS ma_password");
            $this->db->select("msd.iAdminId AS msd_admin_id");
            $this->db->select("msd.fStoreRateAvg AS msd_store_rate_avg");
            $this->db->select("msd.iStoreTotalReview AS msd_store_total_review");
            $this->db->select("msd.eStatus AS msd_status");
            $this->db->select("msd.dDate AS msd_date");
            $this->db->select("ma.eStatus AS ma_status");
            $this->db->select("ma.dLastAccess AS ma_last_access");
            $this->db->select("msd.dModifyDate AS msd_modify_date");
            $this->db->select("ma.iGroupId AS ma_group_id");
            $this->db->select("msd.vCompanyName AS msd_company_name");
            $this->db->select("ma.vName AS ma_name");
            $this->db->select("msd.vContactName AS msd_contact_name");
            $this->db->select("msd.vContactNumber AS msd_contact_number");
            $this->db->select("msd.vAddress1 AS msd_address1");
            $this->db->select("msd.vAddress2 AS msd_address2");
            $this->db->select("msd.vArea AS msd_area");
            $this->db->select("msd.iCountryId AS msd_country_id");
            $this->db->select("msd.iStateId AS msd_state_id");
            $this->db->select("msd.iCityId AS msd_city_id");
            $this->db->select("msd.vPinCode AS msd_pin_code");
            $this->db->select("msd.vWebsiteUrl AS msd_website_url");
            $this->db->select("msd.iMstCategoriesId AS msd_mst_categories_id");
            $this->db->select("msd.eHaveOnlineStore AS msd_have_online_store");
            $this->db->select("msd.vStoreUrl AS msd_store_url");
            $this->db->select("ma.vPhonenumber AS ma_phonenumber");
            $this->db->select("msd.vBankAccountName AS msd_bank_account_name");
            $this->db->select("msd.vBankAccountNo AS msd_bank_account_no");
            $this->db->select("msd.vGSTNo AS msd_g_s_t_no");
            $this->db->select("msd.vGSTIRDProof AS msd_g_s_t_i_r_d_proof");
            $this->db->select("msd.vBankAccountProof AS msd_bank_account_proof");
            $this->db->select("msd.vOtherProof AS msd_other_proof");
            $this->db->select("msd.vOtherProofDetail AS msd_other_proof_detail");
            $this->db->select("msd.eAcceptTerm AS msd_accept_term");
            $this->db->select("msd.vAboutStore AS msd_about_store");
            $this->db->select("msd.vReturnPolicy AS msd_return_policy");
        }

        $this->db->from($this->table_name." AS ".$this->table_alias);
        if (is_array($join) && is_array($join['joins']) && count($join['joins']) > 0) {
            $this->listing->addJoinTables($join['joins']);
            $this->addJoinTables("AR", array("msd"));
            if ($join["list"] == "Yes") {
                $this->addJoinTables("AR", array(""));
            }
        } else {
            $this->addJoinTables("AR", array("msd"));
            if ($join == "Yes") {
                $this->addJoinTables("AR", array(""));
            }
        }
        if (is_array($extra_cond) && count($extra_cond) > 0) {
            $this->listing->addWhereFields($extra_cond);
        } elseif (is_numeric($extra_cond)) {
            $this->db->where($this->table_alias.".".$this->primary_key, intval($extra_cond));
        } elseif ($extra_cond) {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if ($group_by != "") {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "") {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        if ($order_by != "") {
            $this->db->order_by($order_by);
        }
        if ($limit != "") {
            if (is_numeric($limit)) {
                $this->db->limit($limit);
            } else {
                list($offset, $limit) = @explode(",", $limit);
                $this->db->limit($offset, $limit);
            }
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }

    /**
     * getListingData method is used to get grid listing data records for this module.
     * @param array $config_arr config_arr for grid listing settigs.
     * @return array $listing_data returns data records array for grid.
     */
    public function getListingData($config_arr = array()) {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $rec_per_page = (intval($rows) > 0) ? intval($rows) : $this->rec_per_page;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->start_cache();
        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "") {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0) {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "") {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;
        $filter_config['grid_fields'] = $this->grid_fields;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "") {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "") {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "") {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "") {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("msd.vStoreCode AS msd_store_code");
        $this->db->select("msd.vStoreName AS msd_store_name");
        $this->db->select("msd.vContactName AS msd_contact_name");
        $this->db->select("ma.vUserName AS ma_user_name");
        $this->db->select("msd.vContactNumber AS msd_contact_number");
        $this->db->select("msd.eStatus AS msd_status");
        $this->db->select("msd.vCompanyName AS msd_company_name");
        $this->db->select("msd.vWebsiteUrl AS msd_website_url");
        $this->db->select("msd.vBankAccountNo AS msd_bank_account_no");
        $this->db->select("msd.vBankAccountName AS msd_bank_account_name");
        $this->db->select("msd.vGSTNo AS msd_g_s_t_no");
        $this->db->select("msd.dDate AS msd_date");
        $this->db->select("ma.vName AS ma_name");

        $this->db->stop_cache();
        if ((is_array($group_by) && count($group_by) > 0) || trim($having_cond) != "") {
            $this->db->select($this->table_alias.".".$this->primary_key);
            $total_records_arr = $this->db->get();
            $total_records = is_object($total_records_arr) ? $total_records_arr->num_rows() : 0;
        } else {
            $total_records = $this->db->count_all_results();
        }

        $total_pages = $this->listing->getTotalPages($total_records, $rec_per_page);
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0) {
            foreach ($order_by as $orK => $orV) {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "") {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        $limit_offset = $this->listing->getStartIndex($total_records, $page, $rec_per_page);
        $this->db->limit($rec_per_page, $limit_offset);
        $return_data_obj = $this->db->get();
        $return_data = is_object($return_data_obj) ? $return_data_obj->result_array() : array();
        $this->db->flush_cache();
        $listing_data = $this->listing->getDataForJqGrid($return_data, $filter_config, $page, $total_pages, $total_records);
        $this->listing_data = $return_data;
        #echo $this->db->last_query();
        return $listing_data;
    }

    /**
     * getExportData method is used to get grid export data records for this module.
     * @param array $config_arr config_arr for grid export settigs.
     * @return array $export_data returns data records array for export.
     */
    public function getExportData($config_arr = array()) {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $rowlimit = $config_arr['rowlimit'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "") {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0) {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "") {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "") {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "") {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "") {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "") {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("msd.vStoreCode AS msd_store_code");
        $this->db->select("msd.vStoreName AS msd_store_name");
        $this->db->select("msd.vContactName AS msd_contact_name");
        $this->db->select("ma.vUserName AS ma_user_name");
        $this->db->select("msd.vContactNumber AS msd_contact_number");
        $this->db->select("msd.eStatus AS msd_status");
        $this->db->select("msd.vCompanyName AS msd_company_name");
        $this->db->select("msd.vWebsiteUrl AS msd_website_url");
        $this->db->select("msd.vBankAccountNo AS msd_bank_account_no");
        $this->db->select("msd.vBankAccountName AS msd_bank_account_name");
        $this->db->select("msd.vGSTNo AS msd_g_s_t_no");
        $this->db->select("msd.dDate AS msd_date");
        $this->db->select("ma.vName AS ma_name");
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0) {
            foreach ($order_by as $orK => $orV) {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "") {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        if ($rowlimit != "") {
            $offset = $rowlimit;
            $limit = ($rowlimit*$page-$rowlimit);
            $this->db->limit($offset, $limit);
        }
        $export_data_obj = $this->db->get();
        $export_data = is_object($export_data_obj) ? $export_data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $export_data;
    }

    /**
     * addJoinTables method is used to make relation tables joins with main table.
     * @param string $type type is to get active record or join string.
     * @param boolean $allow_tables allow_table is to restrict some set of tables.
     * @return string $ret_joins returns relation tables join string.
     */
    public function addJoinTables($type = 'AR', $allow_tables = FALSE) {
        $join_tables = $this->join_tables;
        if (!is_array($join_tables) || count($join_tables) == 0) {
            return '';
        }
        $ret_joins = $this->listing->addJoinTables($join_tables, $type, $allow_tables);
        return $ret_joins;
    }

    /**
     * getListConfiguration method is used to get listing configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns listing configuration array.
     */
    public function getListConfiguration($name = "") {
        $list_config = array(
            "msd_store_code" => array(
                "name" => "msd_store_code",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreCode",
                "source_field" => "msd_store_code",
                "display_query" => "msd.vStoreCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Store Code",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STORE_CODE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "edit_link" => "Yes",
            ),
            "msd_store_name" => array(
                "name" => "msd_store_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreName",
                "source_field" => "msd_store_name",
                "display_query" => "msd.vStoreName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Store Name",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STORE_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("msd_store_name",
                "MySQL",
                "NULL")
            ),
            "msd_contact_name" => array(
                "name" => "msd_contact_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vContactName",
                "source_field" => "msd_contact_name",
                "display_query" => "msd.vContactName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Contact Name",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_CONTACT_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("msd_contact_name",
                "MySQL",
                "NULL")
            ),
            "ma_user_name" => array(
                "name" => "ma_user_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vUserName",
                "source_field" => "ma_user_name",
                "display_query" => "ma.vUserName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Email",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_EMAIL'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "msd_contact_number" => array(
                "name" => "msd_contact_number",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vContactNumber",
                "source_field" => "msd_contact_number",
                "display_query" => "msd.vContactNumber",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Contact No.",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_CONTACT_NO_C46'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "msd_status" => array(
                "name" => "msd_status",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eStatus",
                "source_field" => "msd_status",
                "display_query" => "msd.eStatus",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Status",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STATUS'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("msd_status",
                "Text",
                "Pending")
            ),
            "msd_company_name" => array(
                "name" => "msd_company_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vCompanyName",
                "source_field" => "msd_company_name",
                "display_query" => "msd.vCompanyName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Company Name",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_COMPANY_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("msd_company_name",
                "MySQL",
                "NULL")
            ),
            "msd_website_url" => array(
                "name" => "msd_website_url",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vWebsiteUrl",
                "source_field" => "msd_website_url",
                "display_query" => "msd.vWebsiteUrl",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Website",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_WEBSITE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("msd_website_url",
                "MySQL",
                "NULL")
            ),
            "msd_bank_account_no" => array(
                "name" => "msd_bank_account_no",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountNo",
                "source_field" => "msd_bank_account_no",
                "display_query" => "msd.vBankAccountNo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "phone_number",
                "align" => "left",
                "label" => "Bank Account No",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NO'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => '99-9999-9999999-999',
            ),
            "msd_bank_account_name" => array(
                "name" => "msd_bank_account_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountName",
                "source_field" => "msd_bank_account_name",
                "display_query" => "msd.vBankAccountName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Bank Account Name",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("msd_bank_account_name",
                "MySQL",
                "NULL")
            ),
            "msd_g_s_t_no" => array(
                "name" => "msd_g_s_t_no",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vGSTNo",
                "source_field" => "msd_g_s_t_no",
                "display_query" => "msd.vGSTNo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "phone_number",
                "align" => "left",
                "label" => "G S T No",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_G_S_T_NO'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => '999-999-999',
            ),
            "msd_date" => array(
                "name" => "msd_date",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "dDate",
                "source_field" => "msd_date",
                "display_query" => "msd.dDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Registration Date",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_REGISTRATION_DATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => $this->general->getAdminPHPFormats('date')
            ),
            "ma_name" => array(
                "name" => "ma_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vName",
                "source_field" => "ma_name",
                "display_query" => "ma.vName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Contact Name",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_CONTACT_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0) {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++) {
                $config_arr[$name[$i]] = $list_config[$name[$i]];
            }
        } elseif ($name != "" && is_string($name)) {
            $config_arr = $list_config[$name];
        } else {
            $config_arr = $list_config;
        }
        return $config_arr;
    }

    /**
     * getFormConfiguration method is used to get form configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns form configuration array.
     */
    public function getFormConfiguration($name = "") {
        $form_config = array(
            "msd_store_code" => array(
                "name" => "msd_store_code",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreCode",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Store Code",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STORE_CODE'),
                "function" => "getLastStorecode",
            ),
            "msd_store_name" => array(
                "name" => "msd_store_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreName",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Store Name",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STORE_NAME'),
                "default" => $this->filter->getDefaultValue("msd_store_name",
                "MySQL",
                "NULL")
            ),
            "msd_store_logo" => array(
                "name" => "msd_store_logo",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreLogo",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "file",
                "label" => "Store Logo",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STORE_LOGO'),
                "default" => $this->filter->getDefaultValue("msd_store_logo",
                "MySQL",
                "NULL"),
                "file_upload" => "Yes",
                "file_server" => "local",
                "file_folder" => "store_logo",
                "file_width" => "50",
                "file_height" => "50",
                "file_format" => "gif,png,jpg,jpeg",
                "file_size" => "2048",
            ),
            "msd_store_description" => array(
                "name" => "msd_store_description",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "tStoreDescription",
                "entry_type" => "Relation",
                "data_type" => "text",
                "type" => "wysiwyg",
                "label" => "Store Description",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STORE_DESCRIPTION'),
                "default" => $this->filter->getDefaultValue("msd_store_description",
                "MySQL",
                "NULL")
            ),
            "ma_email" => array(
                "name" => "ma_email",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vEmail",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Email",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_EMAIL'),
                "show_input" => "Update",
            ),
            "ma_user_name" => array(
                "name" => "ma_user_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vUserName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Email",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_EMAIL')
            ),
            "ma_password" => array(
                "name" => "ma_password",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vPassword",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "password",
                "label" => "Password",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_PASSWORD'),
                "show_input" => "Update",
                "encrypt" => "Yes",
            ),
            "msd_admin_id" => array(
                "name" => "msd_admin_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iAdminId",
                "entry_type" => "Relation",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Admin Id",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_ADMIN_ID'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("msd_admin_id",
                "MySQL",
                "NULL")
            ),
            "msd_store_rate_avg" => array(
                "name" => "msd_store_rate_avg",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "fStoreRateAvg",
                "entry_type" => "Relation",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Store Rate Avg",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STORE_RATE_AVG'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("msd_store_rate_avg",
                "MySQL",
                "NULL")
            ),
            "msd_store_total_review" => array(
                "name" => "msd_store_total_review",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iStoreTotalReview",
                "entry_type" => "Relation",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Store Total Review",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STORE_TOTAL_REVIEW'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("msd_store_total_review",
                "MySQL",
                "NULL")
            ),
            "msd_status" => array(
                "name" => "msd_status",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eStatus",
                "entry_type" => "Relation",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Store Status",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STORE_STATUS'),
                "default" => $this->filter->getDefaultValue("msd_status",
                "Text",
                "Pending")
            ),
            "msd_date" => array(
                "name" => "msd_date",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "dDate",
                "entry_type" => "Relation",
                "data_type" => "datetime",
                "type" => "date",
                "label" => "Date",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_DATE'),
                "show_input" => "Update",
                "format" => $this->general->getAdminPHPFormats('date')
            ),
            "ma_status" => array(
                "name" => "ma_status",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "eStatus",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Status",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STATUS'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("ma_status",
                "Text",
                "Active")
            ),
            "ma_last_access" => array(
                "name" => "ma_last_access",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "dLastAccess",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "type" => "textbox",
                "label" => "Last Access",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_LAST_ACCESS')
            ),
            "msd_modify_date" => array(
                "name" => "msd_modify_date",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "dModifyDate",
                "entry_type" => "Relation",
                "data_type" => "datetime",
                "type" => "textbox",
                "label" => "Modify Date",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_MODIFY_DATE'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("msd_modify_date",
                "MySQL",
                "NULL")
            ),
            "ma_group_id" => array(
                "name" => "ma_group_id",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "iGroupId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "textbox",
                "label" => "Group Id",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_GROUP_ID'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("ma_group_id",
                "Text",
                "3")
            ),
            "msd_company_name" => array(
                "name" => "msd_company_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vCompanyName",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Company Name",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_COMPANY_NAME'),
                "default" => $this->filter->getDefaultValue("msd_company_name",
                "MySQL",
                "NULL")
            ),
            "ma_name" => array(
                "name" => "ma_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Name",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_NAME')
            ),
            "msd_contact_name" => array(
                "name" => "msd_contact_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vContactName",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Contact Name",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_CONTACT_NAME'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("msd_contact_name",
                "MySQL",
                "NULL")
            ),
            "msd_contact_number" => array(
                "name" => "msd_contact_number",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vContactNumber",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Contact Number",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_CONTACT_NUMBER')
            ),
            "msd_address1" => array(
                "name" => "msd_address1",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAddress1",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Address1",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_ADDRESS1'),
                "default" => $this->filter->getDefaultValue("msd_address1",
                "MySQL",
                "NULL")
            ),
            "msd_address2" => array(
                "name" => "msd_address2",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAddress2",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Address2",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_ADDRESS2'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("msd_address2",
                "MySQL",
                "NULL")
            ),
            "msd_area" => array(
                "name" => "msd_area",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vArea",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Area",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_AREA'),
                "default" => $this->filter->getDefaultValue("msd_area",
                "MySQL",
                "NULL")
            ),
            "msd_country_id" => array(
                "name" => "msd_country_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iCountryId",
                "entry_type" => "Relation",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Country",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_COUNTRY'),
                "default" => $this->filter->getDefaultValue("msd_country_id",
                "MySQL",
                "NULL")
            ),
            "msd_state_id" => array(
                "name" => "msd_state_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iStateId",
                "entry_type" => "Relation",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "State ",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STATE'),
                "default" => $this->filter->getDefaultValue("msd_state_id",
                "MySQL",
                "NULL")
            ),
            "msd_city_id" => array(
                "name" => "msd_city_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iCityId",
                "entry_type" => "Relation",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "City",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_CITY'),
                "default" => $this->filter->getDefaultValue("msd_city_id",
                "MySQL",
                "NULL")
            ),
            "msd_pin_code" => array(
                "name" => "msd_pin_code",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vPinCode",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Pin Code",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_PIN_CODE'),
                "default" => $this->filter->getDefaultValue("msd_pin_code",
                "MySQL",
                "NULL")
            ),
            "msd_website_url" => array(
                "name" => "msd_website_url",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vWebsiteUrl",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Company Website URL",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_COMPANY_WEBSITE_URL'),
                "default" => $this->filter->getDefaultValue("msd_website_url",
                "MySQL",
                "NULL")
            ),
            "msd_mst_categories_id" => array(
                "name" => "msd_mst_categories_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iMstCategoriesId",
                "entry_type" => "Relation",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Primary Category",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_PRIMARY_CATEGORY'),
                "default" => $this->filter->getDefaultValue("msd_mst_categories_id",
                "MySQL",
                "NULL")
            ),
            "msd_have_online_store" => array(
                "name" => "msd_have_online_store",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eHaveOnlineStore",
                "entry_type" => "Relation",
                "data_type" => "enum",
                "type" => "radio_buttons",
                "label" => "Do you sell product online already?",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_DO_YOU_SELL_PRODUCT_ONLINE_ALREADY_C63')
            ),
            "msd_store_url" => array(
                "name" => "msd_store_url",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreUrl",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Store Url",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_STORE_URL'),
                "default" => $this->filter->getDefaultValue("msd_store_url",
                "MySQL",
                "NULL")
            ),
            "ma_phonenumber" => array(
                "name" => "ma_phonenumber",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vPhonenumber",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Phonenumber",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_PHONENUMBER'),
                "show_input" => "Update",
            ),
            "msd_bank_account_name" => array(
                "name" => "msd_bank_account_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountName",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Bank Account Name",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NAME'),
                "default" => $this->filter->getDefaultValue("msd_bank_account_name",
                "MySQL",
                "NULL")
            ),
            "msd_bank_account_no" => array(
                "name" => "msd_bank_account_no",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountNo",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "phone_number",
                "label" => "Bank Account No",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NO'),
                "format" => '99-9999-9999999-999',
            ),
            "msd_g_s_t_no" => array(
                "name" => "msd_g_s_t_no",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vGSTNo",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "phone_number",
                "label" => "G S T No",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_G_S_T_NO'),
                "format" => '999-999-999',
            ),
            "sys_custom_field_1" => array(
                "name" => "sys_custom_field_1",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_1",
                "entry_type" => "Custom",
                "data_type" => "",
                "type" => "textbox",
                "label" => "Bank Account No",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NO'),
                "show_input" => "Update",
            ),
            "msd_g_s_t_i_r_d_proof" => array(
                "name" => "msd_g_s_t_i_r_d_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vGSTIRDProof",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "file",
                "label" => "G S T I R D Proof",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_G_S_T_I_R_D_PROOF'),
                "default" => $this->filter->getDefaultValue("msd_g_s_t_i_r_d_proof",
                "MySQL",
                "NULL"),
                "file_upload" => "Yes",
                "file_server" => "local",
                "file_folder" => "gst_proof",
                "file_width" => "50",
                "file_height" => "50",
                "file_format" => "gif,png,jpg,jpeg",
                "file_size" => "2048",
            ),
            "msd_bank_account_proof" => array(
                "name" => "msd_bank_account_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountProof",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "file",
                "label" => "Bank Account Proof",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_PROOF'),
                "default" => $this->filter->getDefaultValue("msd_bank_account_proof",
                "MySQL",
                "NULL"),
                "file_upload" => "Yes",
                "file_server" => "local",
                "file_folder" => "bank_proof",
                "file_width" => "50",
                "file_height" => "50",
                "file_format" => "gif,png,jpg,jpeg",
                "file_size" => "2048",
            ),
            "sys_custom_field_2" => array(
                "name" => "sys_custom_field_2",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_2",
                "entry_type" => "Custom",
                "data_type" => "",
                "type" => "textbox",
                "label" => "Bank Account No-2",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NO_C452'),
                "show_input" => "Update",
            ),
            "msd_other_proof" => array(
                "name" => "msd_other_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vOtherProof",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "file",
                "label" => "Other Proof",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_OTHER_PROOF'),
                "default" => $this->filter->getDefaultValue("msd_other_proof",
                "MySQL",
                "NULL"),
                "file_upload" => "Yes",
                "file_server" => "local",
                "file_folder" => "other_proof",
                "file_width" => "50",
                "file_height" => "50",
                "file_format" => "gif,png,jpg,jpeg",
                "file_size" => "2048",
            ),
            "msd_other_proof_detail" => array(
                "name" => "msd_other_proof_detail",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vOtherProofDetail",
                "entry_type" => "Relation",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Other Proof Detail",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_OTHER_PROOF_DETAIL')
            ),
            "sys_custom_field_3" => array(
                "name" => "sys_custom_field_3",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_3",
                "entry_type" => "Custom",
                "data_type" => "",
                "type" => "textbox",
                "label" => "Bank Account No-3",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NO_C453'),
                "show_input" => "Update",
            ),
            "msd_accept_term" => array(
                "name" => "msd_accept_term",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eAcceptTerm",
                "entry_type" => "Relation",
                "data_type" => "enum",
                "type" => "textbox",
                "label" => "Accept Term",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_ACCEPT_TERM'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("msd_accept_term",
                "Text",
                "Accepted")
            ),
            "sys_custom_field_4" => array(
                "name" => "sys_custom_field_4",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_4",
                "entry_type" => "Custom",
                "data_type" => "",
                "type" => "textbox",
                "label" => "Bank Account No-4",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_BANK_ACCOUNT_NO_C454'),
                "show_input" => "Update",
            ),
            "sys_custom_field_5" => array(
                "name" => "sys_custom_field_5",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_5",
                "entry_type" => "Custom",
                "data_type" => "",
                "type" => "textbox",
                "label" => "G S T No",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_G_S_T_NO'),
                "show_input" => "Update",
            ),
            "sys_custom_field_6" => array(
                "name" => "sys_custom_field_6",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_6",
                "entry_type" => "Custom",
                "data_type" => "",
                "type" => "textbox",
                "label" => "G S T No-2",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_G_S_T_NO_C452'),
                "show_input" => "Update",
            ),
            "sys_custom_field_7" => array(
                "name" => "sys_custom_field_7",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_7",
                "entry_type" => "Custom",
                "data_type" => "",
                "type" => "textbox",
                "label" => "G S T No-3",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_G_S_T_NO_C453'),
                "show_input" => "Update",
            ),
            "msd_about_store" => array(
                "name" => "msd_about_store",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAboutStore",
                "entry_type" => "Relation",
                "data_type" => "text",
                "type" => "wysiwyg",
                "label" => "About Store",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_ABOUT_STORE')
            ),
            "msd_return_policy" => array(
                "name" => "msd_return_policy",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vReturnPolicy",
                "entry_type" => "Relation",
                "data_type" => "text",
                "type" => "wysiwyg",
                "label" => "Return Policy",
                "label_lang" => $this->lang->line('ARCHIVE_SELLER_RETURN_POLICY')
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0) {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++) {
                $config_arr[$name[$i]] = $form_config[$name[$i]];
            }
        } elseif ($name != "" && is_string($name)) {
            $config_arr = $form_config[$name];
        } else {
            $config_arr = $form_config;
        }
        return $config_arr;
    }

    /**
     * checkRecordExists method is used to check duplication of records.
     * @param array $field_arr field_arr is having fields to check.
     * @param array $field_val field_val is having values of respective fields.
     * @param numeric $id id is to avoid current records.
     * @param string $mode mode is having either Add or Update.
     * @param string $con con is having either AND or OR.
     * @return boolean $exists returns either TRUE of FALSE.
     */
    public function checkRecordExists($field_arr = array(), $field_val = array(), $id = '', $mode = '', $con = 'AND') {
        $exists = FALSE;
        if (!is_array($field_arr) || count($field_arr) == 0) {
            return $exists;
        }
        foreach ((array) $field_arr as $key => $val) {
            $extra_cond_arr[] = $this->db->protect($this->table_alias.".".$field_arr[$key])." =  ".$this->db->escape($field_val[$val]);
        }
        $extra_cond = "(".@implode(" ".$con." ", $extra_cond_arr).")";
        if ($mode == "Add") {
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0) {
                $exists = TRUE;
            }
        } elseif ($mode == "Update") {
            $extra_cond = $this->db->protect($this->table_alias.".".$this->primary_key)." <> ".$this->db->escape($id)." AND ".$extra_cond;
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0) {
                $exists = TRUE;
            }
        }
        return $exists;
    }

    /**
     * getSwitchTo method is used to get switch to dropdown array.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @return array $switch_data returns data records array.
     */
    public function getSwitchTo($extra_cond = '', $type = 'records', $limit = '') {
        $switchto_fields = $this->switchto_fields;
        $switch_data = array();
        if (!is_array($switchto_fields) || count($switchto_fields) == 0) {
            if ($type == "count") {
                return count($switch_data);
            } else {
                return $switch_data;
            }
        }
        $fields_arr = array();
        $fields_arr[] = array(
            "field" => $this->table_alias.".".$this->primary_key." AS id",
        );
        $fields_arr[] = array(
            "field" => $this->db->concat($switchto_fields)." AS val",
            "escape" => TRUE,
        );
        if (trim($this->extra_cond) != "") {
            $extra_cond = (trim($extra_cond) != "") ? $extra_cond." AND ".$this->extra_cond : $this->extra_cond;
        }
        $switch_data = $this->getData($extra_cond, $fields_arr, "val ASC", "", $limit, "Yes");
        #echo $this->db->last_query();
        if ($type == "count") {
            return count($switch_data);
        } else {
            return $switch_data;
        }
    }

    /**
     * relationData method is used to get data records from relation table.
     * @param string $table_alias table_alias is to get specific relation table data.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are comma seperated values.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @return array $data_arr returns relation data records array.
     */
    function relationData($table_alias = '', $extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "") {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_alias);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        if (is_array($fields)) {
            $this->listing->addSelectFields($fields);
        } elseif ($fields != "") {
            $this->db->select($fields);
        } else {
            $this->db->select($table_info['table_alias'].".*");
        }
        $this->db->from($table_info['table_name']." AS ".$table_info['table_alias']);
        if (is_array($extra_cond) && count($extra_cond) > 0) {
            $this->listing->addWhereFields($extra_cond);
        } elseif ($extra_cond != "") {
            if (is_numeric($extra_cond)) {
                $this->db->where($table_info['table_alias'].".".$table_info['primary_key'], $extra_cond);
            } else {
                $this->db->where($extra_cond, FALSE, FALSE);
            }
        }
        $this->general->getPhysicalRecordWhere($table_info['table_name'], $table_info['table_alias'], "AR");
        if ($group_by != "") {
            $this->db->group_by($group_by);
        }
        if ($order_by != "") {
            $this->db->order_by($order_by);
        }
        if ($limit != "") {
            list($offset, $limit) = @explode(",", $limit);
            $this->db->limit($offset, $limit);
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }
    /**
     * relationInsert method is used to insert data records to the relation table.
     * @param string $table_alias table_alias is to insert data into specific relation table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    function relationInsert($table_alias = '', $data = array()) {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_alias);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        $this->db->insert($table_name, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    /**
     * relationUpdate method is used to update data records to the relation table.
     * @param string $table_alias table_alias is to update data into specific relation table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @param string $join join is to make joins with main table.
     * @return boolean $res returns TRUE or FALSE.
     */
    function relationUpdate($table_alias = '', $data = array(), $where = '', $join = 'No') {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_alias);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        if ($join == "Yes") {
            $set_cond = array();
            foreach ($data as $key => $val) {
                $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
            }
            $join_tbls = $this->listing->addJoinTables(array($table_info), "NR");
            if (is_numeric($where)) {
                $extra_cond = "WHERE ".$this->db->protect($table_info['primary_key'])." = ".$this->db->escape($where);
            } elseif ($where != '') {
                $extra_cond = "WHERE ".$where;
            } else {
                return FALSE;
            }
            $update_query = "UPDATE ".$this->db->protect($table_name)." AS ".$this->db->protect($table_info['table_alias'])." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
            $res = $this->db->query($update_query);
        } else {
            if (is_numeric($where)) {
                $this->db->where($table_info['primary_key'], $where);
            } elseif ($where != '') {
                $this->db->where($where, FALSE, FALSE);
            } else {
                return FALSE;
            }
            $res = $this->db->update($table_name, $data);
        }
        return $res;
    }
    /**
     * relationDelete method is used to delete data records from relation table.
     * @param string $table_alias table_alias is to update data into specific relation table.
     * @param string $where where is the query condition for deleting.
     * @return boolean $res returns TRUE or FALSE.
     */
    function relationDelete($table_alias = '', $where = '') {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_name);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        if (is_numeric($where)) {
            $this->db->where($table_info['primary_key'], $where);
        } else {
            $this->db->where($where, FALSE, FALSE);
        }
        $res = $this->db->delete($table_name);
        return $res;
    }
    /**
     * relationDetails method is used to get relation table detail.
     * @param string $table_alias table_alias is to get data of specific relation table.
     * @return array $ret_arr returns relation table detail array.
     */
    function relationDetails($table_alias = '') {
        $relation_config = array(
            "msd" => array(
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iAdminId",
                "primary_key" => "iMstStoreDetailId",
                "rel_table_name" => "mod_admin",
                "rel_table_alias" => "ma",
                "rel_field_name" => "iAdminId",
                "extra_condition" => "",
            )
        );
        $ret_arr = $relation_config[$table_alias];
        return $ret_arr;
    }
    /**
     * processGridColor method is used to process grid color settings configuration.
     * @param array $data_arr data_arr is to process data records.
     * @param array $config_arr config_arr for process color settings.
     * @return array $data_arr returns data records array.
     */
    function processGridColor($data_arr = array(), $config_arr = array()) {
        $grid_color_config = array(
            array(
                array(
                    "apply" => "Yes",
                    "oper" => "eq",
                    "type" => "string",
                    "mod_1" => "Variable",
                    "val_1" => "msd_status",
                    "mod_2" => "Static",
                    "val_2" => "Pending",
                    "color_code" => "Static",
                    "color_value" => "#FFF957",
                    "fill_color" => "Cell",
                    "cell_name" => "msd_status",
                )
            )
        );
        $listing_data = $this->listing_data;
        if (!is_array($listing_data) || count($listing_data) == 0) {
            return $data_arr;
        }
        foreach ($listing_data as $dKey => $dVal) {
            $id = $dVal["iAdminId"];
            $row_arr = $this->listing->getGridRowColors($grid_color_config, $dVal, $id);
            if (is_array($row_arr) && count($row_arr) > 0) {
                $data_arr['colors'][$id] = $row_arr;
            }
        }
        return $data_arr;
    }
}
