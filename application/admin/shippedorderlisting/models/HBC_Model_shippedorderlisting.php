<?php

/**
 * Description of Shipped Order Listing Extended Model
 * 
 * @module Extended Shipped Order Listing
 * 
 * @class HBC_Model_shippedorderlisting.php
 * 
 * @path application\admin\shippedorderlisting\models\HBC_Model_shippedorderlisting.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 08.01.2016
 */
   
Class HBC_Model_shippedorderlisting extends Model_shippedorderlisting {
        function __construct() {
    parent::__construct();
}


function getSubOrderInvoiceDetails($extracond = "", $field = "*", $orderby = "", $limit = "", $join_arr = '') {
    if ($field == "") {
        $field = "CONCAT_WS(' ',msb.vSubOrderNumberPre,msb.iSubOrderNumber) as sub_order_number,CONCAT_WS(' ',mo.vOrderNumberPre,mo.iOrderNumber) as order_number,mo.vBuyerName,mo.vBuyerAddress1 as buyer_address1,mo.vBuyerAddress2 as buyer_address2,mo.vBuyerArea as buyer_area,";
        $field .= "mo.vBuyerPinCode as buyer_pincode,mo.vBuyerPhone as buyer_phone,mo.vBuyerEmail as buyer_email,mo.vBuyerCity as buyer_city,mo.vBuyerState as buyer_state,mo.vBuyerCountry as buyer_country,";
        $field .= "mcc1.vCity as seller_city,ms1.vState as seller_state,mc1.vCountry as seller_country,msd.vCompanyName as seller_company_name,msd.vArea as seller_area,msd.vAddress1 as seller_address1,msd.vAddress2 as seller_address2,msd.vPinCode as seller_pin_code,";
        $field .= "msd.vGSTNo,msb.iMstProductsId,msb.vProductName,msb.iProductQty,msb.fProductPrice as product_price,msb.fProductSalePrice as sale_price,msb.fTotalCost as total_cost,msb.fShippingCost,msb.vProductSku,";
        $field .= "msb.vSellerInvoiceDate,msb.vSellerInvoiceNo,msd.vContactNumber as seller_phone,ma.vEmail as seller_email,ma.vName as seller_name";
    }
    $this->db->select($field, false);
    $this->db->from("mst_sub_order as msb");
    $this->db->join('mst_order as mo', 'mo.iMstOrderId = msb.iMstOrderId', 'join');
    $this->db->join('mst_store_detail as msd', 'msd.iMstStoreDetailId = msb.iMstStoreDetailId', 'left');
    //  $this->db->join('mod_state as ms', 'ms.iStateId = mo.iBuyerStateId', 'left');
    //  $this->db->join('mod_country as mc', 'mc.iCountryId = mo.iBuyerCountryId', 'left');
    //$this->db->join('mod_city as mcc', 'mcc.iCityId = mo.iBuyerCityId', 'left');
    $this->db->join('mod_state as ms1', 'ms1.iStateId = msd.iStateId', 'left');
    $this->db->join('mod_country as mc1', 'mc1.iCountryId = msd.iCountryId', 'left');
    $this->db->join('mod_city as mcc1', 'mcc1.iCityId = msd.iCityId', 'left');
    $this->db->join('mod_admin as ma', 'ma.iAdminId = msd.iAdminId', 'left');
    if (is_array($join_arr) && count($join_arr) > 0) {
        foreach ($join_arr as $key => $val) {
            $this->db->join($val['table'], $val['condition'], $val['jointype']);
        }
    }
    if ($extracond != "") {
        if (intval($extracond)) {
            $this->db->where("msb.iMstSubOrderId", $extracond);
        } else {
            $this->db->where($extracond);
        }
    }
    if ($orderby != "") {
        $this->db->order_by($orderby);
    }
    if ($limit != "") {
        list($offset, $limit) = @explode(",", $limit);
        $this->db->limit($offset, $limit);
    }
    $list_data = $this->db->get()->result_array();
    #   echo $this->db->last_query();
    return $list_data;
}
}
