<?php
            
/**
 * Description of Shipped Order Listing Extended Controller
 * 
 * @module Extended Shipped Order Listing
 * 
 * @class HBC_Shippedorderlisting.php
 * 
 * @path application\admin\shippedorderlisting\controllers\HBC_Shippedorderlisting.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 08.01.2016
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
 
Class HBC_Shippedorderlisting extends Shippedorderlisting {
        function __construct() {
    parent::__construct();
    $this->load->model("model_shippedorderlisting");
}

function shipping_invoice() {
        $shipArr = $this->params_arr;
	
        $sub_order_id = $shipArr['id'];
        $download = intval($shipArr['download']);
        $invoiceArr1 = $this->model_shippedorderlisting->getSubOrderInvoiceDetails($sub_order_id, '');
//print_r($invoiceArr1);exit;
        $invoiceArr = $invoiceArr1[0];
        if (is_array($invoiceArr) && count($invoiceArr) > 0) {
			$string=$invoiceArr['vGSTNo'];
			if($string !='' && isset($string)){
			$string=implode("-", str_split($string, 3));
		$invoiceArr['vGSTNo']=$string;
			}


			$invoiceArr['vSellerInvoiceDate']=$this->general->dateSystemFormat($invoiceArr['vSellerInvoiceDate']);
            $data = array();
            $data['mo_buyer_area'] = $invoiceArr['buyer_area'];
            $data['mo_buyer_address1'] = $invoiceArr['buyer_address1'];
            $data['mo_buyer_address2'] = $invoiceArr['buyer_address2'];
            $data['mc1_country'] = $invoiceArr['buyer_country'];
            $data['ms_state'] = $invoiceArr['buyer_state'];
            $data['mc2_city'] = $invoiceArr['buyer_city'];
            $data['mo_buyer_pin_code'] = $invoiceArr['buyer_pincode'];
            $buyer_address = $this->general->getBuyerShippingAddressExpandedView('', '', $data);
            $invoiceArr['buyer_address'] = $buyer_address;

            $data = array();
            $data['mo_buyer_area'] = $invoiceArr['seller_area'];
            $data['mo_buyer_address1'] = $invoiceArr['seller_address1'];
            $data['mo_buyer_address2'] = $invoiceArr['seller_address2'];
            $data['mc1_country'] = $invoiceArr['seller_country'];
            $data['ms_state'] = $invoiceArr['seller_state'];
            $data['mc2_city'] = $invoiceArr['seller_city'];
            $data['mo_buyer_pin_code'] = $invoiceArr['seller_pin_code'];
            $seller_address = $this->general->getBuyerShippingAddressExpandedView('', '', $data);
            $invoiceArr['seller_address'] = $seller_address;
            //pr($invoiceArr);exit;
            if ($download == 1) {
				
            require_once APPPATH . 'third_party/dompdf/dompdf_config.inc.php';
            $html = $this->parser->parse("shippedorderlisting_hbc_custom_invoice.tpl", $invoiceArr, true);
            $dompdf = new DOMPDF();
            $dompdf->load_html($html);
            $dompdf->render();
            $file_name = time() . "_invoice.pdf";
            $pdffile = "public/upload/pdf/" . $file_name;

            $this->general->custom_file_put_contents($pdffile, $dompdf->output());
		
            $dir_path = $this->config->item("upload_path") . "pdf/";
          $pdf_url = $this->config->item("upload_url") . 'pdf/' . $file_name;
            $this->general->force_download($file_name, $dir_path);
            exit;
        } else {
            $this->loadView("shippedorderlisting_hbc_custom_invoice", $invoiceArr);
        }
        } else {
            $this->session->set_flashdata('failure', 'Failure Occured in displaying the invoice');
            $redirect_url = $this->config->item("admin_url") . "shippedorderlisting/shippedorderlisting/index";
            redirect($redirect_url);
        }
        // $this->skip_template_view();
    }


function shipping_address() {
    $shipArr = $this->params_arr;
    $sub_order_id = $shipArr['id'];
    $invoiceArr1 = $this->model_shippedorderlisting->getSubOrderInvoiceDetails($sub_order_id, '');
    $invoiceArr = $invoiceArr1[0];
    if (is_array($invoiceArr) && count($invoiceArr) > 0) {
        $data = array();
        $data['mo_buyer_area'] = $invoiceArr['buyer_area'];
        $data['mo_buyer_address1'] = $invoiceArr['buyer_address1'];
        $data['mo_buyer_address2'] = $invoiceArr['buyer_address2'];
        $data['mc1_country'] = $invoiceArr['buyer_country'];
        $data['ms_state'] = $invoiceArr['buyer_state'];
        $data['mc2_city'] = $invoiceArr['buyer_city'];
        $data['mo_buyer_pin_code'] = $invoiceArr['buyer_pincode'];
        $buyer_address = $this->general->getBuyerShippingAddressExpandedView('', '', $data);
        $invoiceArr['buyer_address'] = $buyer_address;
        $this->loadView("shippedorderlisting_hbc_custom_address_new", $invoiceArr);
    } else {
        $this->session->set_flashdata('failure', 'Failure Occured in displaying the invoice');
        $redirect_url = $this->config->item("admin_url") . "shippedorderlisting/shippedorderlisting/index";
        redirect($redirect_url);
    }
}
}
