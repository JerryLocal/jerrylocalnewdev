<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>
<%$this->js->add_js('jquery-1.9.1.min.js')%>
<%$this->js->add_js('admin/bootstrap.min.js')%>
<%$this->js->js_src()%>
<%$this->css->add_css('admin/bootstrap.min.css')%>	
<%$this->css->add_css('admin/style_developer.css')%>		
<%$this->css->css_src()%> 
<style ="text/css">
    h1.invoice_heading{padding-bottom:28px !important;}
    table.invoice_seller_buyer th {background:none;}
</style>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" style="border: none; border-bottom: solid 1px #c1c1c1; margin: 0 1%; width: 98% !important;   height: auto;" id="top_heading_fix"> <h3 style="padding: 5px 0; margin: 0; text-align:right; width:100%;">Shipping Address</h3>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="shippedorderlisting" class="frm-view-block frm-stand-view">
            <div class="">
               
                <div class="clear"></div>
                <table align="center" style="font-size: 14px; width: 97%; line-height: 22px;">
                    <tr>
                        
                        <td> <%$vBuyerName%> </td>
                    </tr>
                    <tr>
                        <td> <%$buyer_address%></td>
                    </tr>

                    <tr>
                        
                        <td> M:- <%$buyer_phone%></td>
                    </tr>

                </table>
            </div>
        </div>

    </div>
</div>
