<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>

<%$this->js->add_js('jquery-1.9.1.min.js')%>
<%$this->js->add_js('admin/bootstrap.min.js')%>
<%$this->js->js_src()%>
<%$this->css->add_css('admin/bootstrap.min.css')%>	
<%$this->css->add_css('admin/style_developer.css')%>		
<%$this->css->css_src()%> 
<style type="text/css">
    h1.invoice_heading{padding-bottom:28px !important;}
    table.invoice_seller_buyer th {background:none;}
    bottom:0px;}
</style>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" style="border: none; border-bottom: solid 1px #c1c1c1; margin: 0 1%; width: 98% !important; height: auto;" id="top_heading_fix"> <!--<h3 style="margin-top:0px;">Shipping Order :: Invoice </h3>-->
        <h3 style="padding: 5px 0; margin: 0; text-align:right; width:100%;">ORDER DETAIL</h3>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing" style="font-size:14px;">
        <div id="shippedorderlisting" class="frm-view-block frm-stand-view">
            <div class="container">
                <!--<h1 align="center" class="invoice_heading">TAX INVOICE</h1>-->
                <div class="row">

                    <table width="100%" border="0" class="invoice_seller_buyer">
                        <tr>
                            <td scope="col" width="33%" valign="top">
                                <table width="100%">
                                    <tr>
                                        <th scope="col" align="left"><strong style="font-size:18px; line-height:26px;">Order Id : <%$order_number%></th>
                                    </tr>
                                    <tr>
                                        <td><strong>Sub Order Id : <%$sub_order_number%></strong></td>
                                    </tr>	
                                    <tr>
                                        <td><strong>Invoice No : </strong><%$vSellerInvoiceNo%></td>
                                    </tr>

                                    <tr>
                                        <td><strong>Invoice Date : </strong><%$vSellerInvoiceDate%></td>
                                    </tr>
                                </table>
                            </td>
                            <td scope="col" width="33%" valign="top">
                                <table width="100%">
                                    <tr>
                                        <th scope="col" width="50%" align="left"><strong style="font-size:18px; line-height:26px;">SELLER</strong></th>
                                    </tr>

                                    <tr>
                                        <td><strong><%$seller_company_name%></strong></td>
                                    </tr>

                                    <tr>
                                        <td><%$seller_address%></td>
                                    </tr>

                                    <tr>
                                        <td><strong>Phone Number : </strong><%$seller_phone%></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align:left;"><strong>Email : </strong><a href="mailto:<%$seller_email%>"><%$seller_email%></a> </td>
                                    </tr>

                                    <%if $vGSTNo neq ''%>
                                    <tr>
                                        <td><strong>GST Number : </strong><%$vGSTNo%></td>
                                    </tr>
                                    <%/if%>
                                </table>
                            </td>
                            <td scope="col" width="34%" style="text-align:left;">
                                <table width="100%">
                                    <tr>
                                        <th scope="col" width="50%" style="text-align:left;"><strong style="font-size:18px; line-height:26px;">BUYER</strong></th>
                                    </tr>

                                    <tr>
                                        <td style="text-align:left;"><strong><%$vBuyerName%></strong></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align:left;"><%$buyer_address%> </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align:left;"><strong>Phone Number : </strong><%$buyer_phone%></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align:left;"><strong>Email : </strong><a href="mailto:<%$buyer_email%>"><%$buyer_email%></a> </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align:left;">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td style="text-align:left;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <div class="row" style="margin-top:30px;">

                        <div class="product_detail">
                            <!--<h2 align="center">Product Details</h2>-->
                            <div class="product_table" style="border:none; background: #c3c3c3;">
                                <table width="100%" border="0">
                                    <tr>
                                        <th scope="col" style="text-align:left; background:#e4e4e4; padding:4px;">Product</th>
                                        <!--<th scope="col" style="text-align:left; background:#e4e4e4; padding:4px;">Product SKU</th>-->
                                        <th scope="col" style="text-align:right; background:#e4e4e4; padding:4px;">Qty</th>
                                        <th scope="col" style="text-align:right; background:#e4e4e4; padding:4px;">Unit Price</th>    
                                        <!--<th scope="col" style="text-align:left; background:#e4e4e4;">Shipping Cost</th>-->    
                                        <th scope="col" style="text-align:right; background:#e4e4e4; padding:4px;">Total</th>
                                    </tr>
                                    <tr class="bg_tr" style="background: #fff;">
                                        <td style="padding:4px;"><%$vProductName%></td>
                                        <!--<td style="padding:4px;"><%$vProductSku%></td>-->
                                        <td style="padding:4px; text-align: right;"><%$iProductQty%></td>
                                        <td style="padding:4px; text-align: right;"><%$this->general->get_formated_currency_details($sale_price)%></td>
                                        <!--<td><%$this->general->get_formated_currency_details($fShippingCost)%></td>-->
                                        <td style="padding:4px; text-align: right;"><%$this->general->get_formated_currency_details($product_price)%></td>
                                    </tr>	

                                </table>

                            </div>
                            <table width="100%" border="0" style="float: right; margin: 15px 0; font-size: 16px; color: #717171; text-align: right;">
                                <tr class="bg_tr">
                                    <td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td width="34%" style="text-align:left;">Sub Total</td>
                                    <td style="text-align:right;"><%$this->general->get_formated_currency_details($product_price)%></td>
                                </tr>
                                <tr class="bg_tr">
                                    <td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="text-align:left;">Shipping</td>
                                    <td style="text-align:right;"><%$this->general->get_formated_currency_details($fShippingCost)%></td>
                                </tr>	
                                <tr class="bg_tr">
                                    <td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="color:#99153b; font-size:16px; text-align:left;">Total (Incl GST at rate Shown)</td>
                                    <td style="color:#99153b; font-size:16px; text-align: right;"><%$this->general->get_formated_currency_details($total_cost)%></td>
                                </tr>
                            </table>
                        </div>
                    </div>
<!--
                    <div class="row">
                        <div class="prise">
                            <h2 style="font-size: 13px; font-weight: bold; font-style: normal; text-decoration: none;  color: #333333; text-align: right;">This invoice is issued by JerryLocal Ltd on behalf of the Seller</h2>
                        </div>
                    </div>
-->

                </div>
            </div>
        </div>
    </div>        
</div>