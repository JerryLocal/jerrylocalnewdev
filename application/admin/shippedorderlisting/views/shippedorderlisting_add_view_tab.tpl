<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="tab"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="tab" />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_mst_order_id">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_MST_ORDER_ID')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['mso_mst_order_id'], $opt_arr['mso_mst_order_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_mst_store_detail_id">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_MST_STORE_DETAIL_ID')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['mso_mst_store_detail_id'], $opt_arr['mso_mst_store_detail_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_mst_products_id">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_MST_PRODUCTS_ID')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['mso_mst_products_id'], $opt_arr['mso_mst_products_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_name">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_NAME')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_product_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_sku">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_SKU')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_product_sku']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_regular_price">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_REGULAR_PRICE')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_product_regular_price']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_sale_price">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_SALE_PRICE')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_product_sale_price']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_price">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_PRICE')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_product_price']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_qty">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_QTY')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['mso_product_qty'], $opt_arr['mso_product_qty'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_canel_by">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_CANEL_BY')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['mso_canel_by'], $opt_arr['mso_canel_by'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_item_status">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_ITEM_STATUS')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['mso_item_status'], $opt_arr['mso_item_status'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_shipping_cost">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SHIPPING_COST')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_shipping_cost']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_total_cost">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_TOTAL_COST')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_total_cost']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_return_period">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_RETURN_PERIOD')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['mso_return_period'], $opt_arr['mso_return_period'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_extra_info">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_EXTRA_INFO')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_extra_info']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_option">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_OPTION')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_product_option']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_seller_invoice_date">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SELLER_INVOICE_DATE')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_seller_invoice_date']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_seller_invoice_no">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SELLER_INVOICE_NO')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_seller_invoice_no']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_shipping_date">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SHIPPING_DATE')%>
        </label> 
        <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
            <strong><%$this->general->dateDefinedFormat('Y-m-d',$data['mso_shipping_date'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_shipper_name">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SHIPPER_NAME')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_shipper_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_mst_shipper_id">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_MST_SHIPPER_ID')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['mso_mst_shipper_id'], $opt_arr['mso_mst_shipper_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_cancel_detail">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_CANCEL_DETAIL')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_cancel_detail']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_cancel_date">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_CANCEL_DATE')%>
        </label> 
        <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
            <strong><%$this->general->dateDefinedFormat('Y-m-d',$data['mso_cancel_date'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_cancel_user_id">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_CANCEL_USER_ID')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['mso_cancel_user_id'], $opt_arr['mso_cancel_user_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_gateway_tdr">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_GATEWAY_TDR')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_gateway_tdr']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_market_place_fee">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_MARKET_PLACE_FEE')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_market_place_fee']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_admin_commision_per">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_ADMIN_COMMISION_PER')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_admin_commision_per']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_admin_commision">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_ADMIN_COMMISION')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_admin_commision']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_shipping_remark">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SHIPPING_REMARK')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_shipping_remark']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_gateway_tdrper">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_GATEWAY_TDRPER')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_gateway_tdrper']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_tracking_number">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_TRACKING_NUMBER')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['mso_tracking_number']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_delivered_date">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_DELIVERED_DATE')%>
        </label> 
        <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
            <strong><%$this->general->dateDefinedFormat('Y-m-d',$data['mso_delivered_date'])%></strong>
        </div>
    </div>
</div>