<ul class="nav nav-tabs">
    <li <%if $module_name eq "shippedorderlisting"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('SHIPPEDORDERLISTING_SHIPPED_ORDER_LISTING')%>" 
            <%if $module_name eq "shippedorderlisting"%> 
                href="javascript://"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('shippedorderlisting/shippedorderlisting/add')%>|mode|<%$mod_enc_mode['Update']%>|id|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('SHIPPEDORDERLISTING_SHIPPED_ORDER_LISTING')%>
        </a>
    </li>
    <li <%if $module_name eq "productmanagement"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_MANAGEMENT')%> <%$this->lang->line('GENERIC_LIST')%>" 
            <%if $module_name eq "productmanagement"%> 
                href="javascript://"
            <%elseif $module_name eq "shippedorderlisting"%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('productmanagement/productmanagement/index')%>|parMod|<%$this->general->getAdminEncodeURL('shippedorderlisting')%>|parID|<%$this->general->getAdminEncodeURL($data['mso_mst_products_id'])%>"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('productmanagement/productmanagement/index')%>|parMod|<%$this->general->getAdminEncodeURL('shippedorderlisting')%>|parID|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_MANAGEMENT')%>
        </a>
    </li>
</ul>            