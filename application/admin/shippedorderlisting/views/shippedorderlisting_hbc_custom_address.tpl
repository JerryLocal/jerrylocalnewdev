<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>
<%$this->js->add_js('jquery-1.9.1.min.js')%>
<%$this->js->add_js('admin/bootstrap.min.js')%>
<%$this->js->js_src()%>
<%$this->css->add_css('admin/bootstrap.min.css')%>  
<%$this->css->add_css('admin/style_developer.css')%>        
<%$this->css->css_src()%> 
<style ="text/css">
h4.invoice_heading{padding-bottom:28px !important;}
</style>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>

<div id="ajax_content_div" class="ajax-content-div top-frm-spacing">
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
	<div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="shippedorderlisting" class="frm-view-block frm-stand-view">
            <div class="container">
                <h4 align="center" class="invoice_heading">Shipping Address</h4>
				<div class="clear"></div>
				 <table align="center">
					<tr>
						<td> <b> Buyer Name :- </b></td>
						<td> <%$vBuyerName%> </td>
					</tr>
					<tr>
						<td valign="top"> <b>Address :-</b> </td>
						<td> <%$buyer_address%></td>
					</tr>

					<tr>
						
						<td> M: <%$buyer_phone%></td>
					</tr>

				</table>
			</div>
        </div>
    
</div>
</div>