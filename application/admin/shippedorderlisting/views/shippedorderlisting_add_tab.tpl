<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="tab"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="tab" />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_mst_order_id">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_MST_ORDER_ID')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['mso_mst_order_id']%>
            <%$this->dropdown->display("mso_mst_order_id","mso_mst_order_id","  title='<%$this->lang->line('SHIPPEDORDERLISTING_MST_ORDER_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SHIPPEDORDERLISTING_MST_ORDER_ID')%>'  ", "|||", "", $opt_selected,"mso_mst_order_id")%>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_mst_order_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_mst_store_detail_id">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_MST_STORE_DETAIL_ID')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['mso_mst_store_detail_id']%>
            <%$this->dropdown->display("mso_mst_store_detail_id","mso_mst_store_detail_id","  title='<%$this->lang->line('SHIPPEDORDERLISTING_MST_STORE_DETAIL_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SHIPPEDORDERLISTING_MST_STORE_DETAIL_ID')%>'  ", "|||", "", $opt_selected,"mso_mst_store_detail_id")%>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_mst_store_detail_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_mst_products_id">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_MST_PRODUCTS_ID')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['mso_mst_products_id']%>
            <%$this->dropdown->display("mso_mst_products_id","mso_mst_products_id","  title='<%$this->lang->line('SHIPPEDORDERLISTING_MST_PRODUCTS_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SHIPPEDORDERLISTING_MST_PRODUCTS_ID')%>'  ", "|||", "", $opt_selected,"mso_mst_products_id")%>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_mst_products_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_name">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_NAME')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_product_name']|@htmlentities%>" name="mso_product_name" id="mso_product_name" title="<%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_NAME')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_product_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_sku">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_SKU')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_product_sku']|@htmlentities%>" name="mso_product_sku" id="mso_product_sku" title="<%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_SKU')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_product_skuErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_regular_price">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_REGULAR_PRICE')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_product_regular_price']|@htmlentities%>" name="mso_product_regular_price" id="mso_product_regular_price" title="<%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_REGULAR_PRICE')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_product_regular_priceErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_sale_price">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_SALE_PRICE')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_product_sale_price']|@htmlentities%>" name="mso_product_sale_price" id="mso_product_sale_price" title="<%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_SALE_PRICE')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_product_sale_priceErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_price">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_PRICE')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_product_price']|@htmlentities%>" name="mso_product_price" id="mso_product_price" title="<%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_PRICE')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_product_priceErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_qty">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_QTY')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['mso_product_qty']%>
            <%$this->dropdown->display("mso_product_qty","mso_product_qty","  title='<%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_QTY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SHIPPEDORDERLISTING_PRODUCT_QTY')%>'  ", "|||", "", $opt_selected,"mso_product_qty")%>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_product_qtyErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_canel_by">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_CANEL_BY')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['mso_canel_by']%>
            <%$this->dropdown->display("mso_canel_by","mso_canel_by","  title='<%$this->lang->line('SHIPPEDORDERLISTING_CANEL_BY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SHIPPEDORDERLISTING_CANEL_BY')%>'  ", "|||", "", $opt_selected,"mso_canel_by")%>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_canel_byErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_item_status">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_ITEM_STATUS')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['mso_item_status']%>
            <%$this->dropdown->display("mso_item_status","mso_item_status","  title='<%$this->lang->line('SHIPPEDORDERLISTING_ITEM_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SHIPPEDORDERLISTING_ITEM_STATUS')%>'  ", "|||", "", $opt_selected,"mso_item_status")%>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_item_statusErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_shipping_cost">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SHIPPING_COST')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_shipping_cost']|@htmlentities%>" name="mso_shipping_cost" id="mso_shipping_cost" title="<%$this->lang->line('SHIPPEDORDERLISTING_SHIPPING_COST')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_shipping_costErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_total_cost">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_TOTAL_COST')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_total_cost']|@htmlentities%>" name="mso_total_cost" id="mso_total_cost" title="<%$this->lang->line('SHIPPEDORDERLISTING_TOTAL_COST')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_total_costErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_return_period">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_RETURN_PERIOD')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['mso_return_period']%>
            <%$this->dropdown->display("mso_return_period","mso_return_period","  title='<%$this->lang->line('SHIPPEDORDERLISTING_RETURN_PERIOD')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SHIPPEDORDERLISTING_RETURN_PERIOD')%>'  ", "|||", "", $opt_selected,"mso_return_period")%>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_return_periodErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_extra_info">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_EXTRA_INFO')%>
        </label> 
        <div class="form-right-div   ">
            <textarea placeholder=""  name="mso_extra_info" id="mso_extra_info" title="<%$this->lang->line('SHIPPEDORDERLISTING_EXTRA_INFO')%>"  class='elastic frm-size-medium'  ><%$data['mso_extra_info']%></textarea>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_extra_infoErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_product_option">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_OPTION')%>
        </label> 
        <div class="form-right-div   ">
            <textarea placeholder=""  name="mso_product_option" id="mso_product_option" title="<%$this->lang->line('SHIPPEDORDERLISTING_PRODUCT_OPTION')%>"  class='elastic frm-size-medium'  ><%$data['mso_product_option']%></textarea>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_product_optionErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_seller_invoice_date">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SELLER_INVOICE_DATE')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_seller_invoice_date']|@htmlentities%>" name="mso_seller_invoice_date" id="mso_seller_invoice_date" title="<%$this->lang->line('SHIPPEDORDERLISTING_SELLER_INVOICE_DATE')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_seller_invoice_dateErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_seller_invoice_no">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SELLER_INVOICE_NO')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_seller_invoice_no']|@htmlentities%>" name="mso_seller_invoice_no" id="mso_seller_invoice_no" title="<%$this->lang->line('SHIPPEDORDERLISTING_SELLER_INVOICE_NO')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_seller_invoice_noErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_shipping_date">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SHIPPING_DATE')%>
        </label> 
        <div class="form-right-div  input-append text-append-prepend   ">
            <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['mso_shipping_date'])%>" placeholder="" name="mso_shipping_date" id="mso_shipping_date" title="<%$this->lang->line('SHIPPEDORDERLISTING_SHIPPING_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
            <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_shipping_dateErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_shipper_name">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SHIPPER_NAME')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_shipper_name']|@htmlentities%>" name="mso_shipper_name" id="mso_shipper_name" title="<%$this->lang->line('SHIPPEDORDERLISTING_SHIPPER_NAME')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_shipper_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_mst_shipper_id">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_MST_SHIPPER_ID')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['mso_mst_shipper_id']%>
            <%$this->dropdown->display("mso_mst_shipper_id","mso_mst_shipper_id","  title='<%$this->lang->line('SHIPPEDORDERLISTING_MST_SHIPPER_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SHIPPEDORDERLISTING_MST_SHIPPER_ID')%>'  ", "|||", "", $opt_selected,"mso_mst_shipper_id")%>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_mst_shipper_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_cancel_detail">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_CANCEL_DETAIL')%>
        </label> 
        <div class="form-right-div   ">
            <textarea placeholder=""  name="mso_cancel_detail" id="mso_cancel_detail" title="<%$this->lang->line('SHIPPEDORDERLISTING_CANCEL_DETAIL')%>"  class='elastic frm-size-medium'  ><%$data['mso_cancel_detail']%></textarea>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_cancel_detailErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_cancel_date">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_CANCEL_DATE')%>
        </label> 
        <div class="form-right-div  input-append text-append-prepend   ">
            <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['mso_cancel_date'])%>" placeholder="" name="mso_cancel_date" id="mso_cancel_date" title="<%$this->lang->line('SHIPPEDORDERLISTING_CANCEL_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
            <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_cancel_dateErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_cancel_user_id">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_CANCEL_USER_ID')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['mso_cancel_user_id']%>
            <%$this->dropdown->display("mso_cancel_user_id","mso_cancel_user_id","  title='<%$this->lang->line('SHIPPEDORDERLISTING_CANCEL_USER_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SHIPPEDORDERLISTING_CANCEL_USER_ID')%>'  ", "|||", "", $opt_selected,"mso_cancel_user_id")%>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_cancel_user_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_gateway_tdr">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_GATEWAY_TDR')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_gateway_tdr']|@htmlentities%>" name="mso_gateway_tdr" id="mso_gateway_tdr" title="<%$this->lang->line('SHIPPEDORDERLISTING_GATEWAY_TDR')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_gateway_tdrErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_market_place_fee">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_MARKET_PLACE_FEE')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_market_place_fee']|@htmlentities%>" name="mso_market_place_fee" id="mso_market_place_fee" title="<%$this->lang->line('SHIPPEDORDERLISTING_MARKET_PLACE_FEE')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_market_place_feeErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_admin_commision_per">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_ADMIN_COMMISION_PER')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_admin_commision_per']|@htmlentities%>" name="mso_admin_commision_per" id="mso_admin_commision_per" title="<%$this->lang->line('SHIPPEDORDERLISTING_ADMIN_COMMISION_PER')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_admin_commision_perErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_admin_commision">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_ADMIN_COMMISION')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_admin_commision']|@htmlentities%>" name="mso_admin_commision" id="mso_admin_commision" title="<%$this->lang->line('SHIPPEDORDERLISTING_ADMIN_COMMISION')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_admin_commisionErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_shipping_remark">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_SHIPPING_REMARK')%>
        </label> 
        <div class="form-right-div   ">
            <textarea placeholder=""  name="mso_shipping_remark" id="mso_shipping_remark" title="<%$this->lang->line('SHIPPEDORDERLISTING_SHIPPING_REMARK')%>"  class='elastic frm-size-medium'  ><%$data['mso_shipping_remark']%></textarea>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_shipping_remarkErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_gateway_tdrper">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_GATEWAY_TDRPER')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_gateway_tdrper']|@htmlentities%>" name="mso_gateway_tdrper" id="mso_gateway_tdrper" title="<%$this->lang->line('SHIPPEDORDERLISTING_GATEWAY_TDRPER')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_gateway_tdrperErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_tracking_number">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_TRACKING_NUMBER')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['mso_tracking_number']|@htmlentities%>" name="mso_tracking_number" id="mso_tracking_number" title="<%$this->lang->line('SHIPPEDORDERLISTING_TRACKING_NUMBER')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='mso_tracking_numberErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_mso_delivered_date">
        <label class="form-label span3">
            <%$this->lang->line('SHIPPEDORDERLISTING_DELIVERED_DATE')%>
        </label> 
        <div class="form-right-div  input-append text-append-prepend   ">
            <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['mso_delivered_date'])%>" placeholder="" name="mso_delivered_date" id="mso_delivered_date" title="<%$this->lang->line('SHIPPEDORDERLISTING_DELIVERED_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
            <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
        </div>
        <div class="error-msg-form "><label class='error' id='mso_delivered_dateErr'></label></div>
    </div>
</div>
<div align="right" class="btn-below-spacing">
    <div style="float:right;">
        <input type="submit" value="Save" name="ctrlsave_1_1_1" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','1','tab')"/>
        &nbsp;&nbsp;
    </div>
    <div class="clear"></div>
</div>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initAdminTabRenderJSScript(eleObj){
Project.modules.shippedorderlisting.initEvents("tab");
callGoogleMapEvents();
}
<%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
<%foreach name=i from=$auto_arr item=v key=k%>
    if($("#<%$k%>").is("select")){
    $("#<%$k%>").ajaxChosen({
    dataType: "json",
    type: "POST",
    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
    },{
    loadingImg: admin_image_url+"chosen-loading.gif"
    });
}
<%/foreach%>
<%/if%>        
<%/javascript%>