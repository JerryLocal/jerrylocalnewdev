<%section name=i loop=1%>
    <tr id="tr_child_row_<%$child_module_name%>_<%$row_index%>">
        <td class="row-num-child">
            <span class="row-num-span"><%$row_index%></span>
            <input type="hidden" name="child[productmanagement][id][<%$row_index%>]" id="child_productmanagement_id_<%$row_index%>" value="<%$child_id%>" />
            <input type="hidden" name="child[productmanagement][enc_id][<%$row_index%>]" id="child_productmanagement_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
            <input type="hidden" name="child[productmanagement][mp_admin_id2][<%$row_index%>]" id="child_productmanagement_mp_admin_id2_<%$row_index%>" value="<%$child_data[i]['mp_admin_id2']%>"  class='ignore-valid ' />
            <input type="hidden" name="child[productmanagement][tpc_mst_products_id][<%$row_index%>]" id="child_productmanagement_tpc_mst_products_id_<%$row_index%>" value="<%$child_data[i]['tpc_mst_products_id']%>"  class='ignore-valid ' />
            <input type="hidden" name="child[productmanagement][mp_rating_avg][<%$row_index%>]" id="child_productmanagement_mp_rating_avg_<%$row_index%>" value="<%$child_data[i]['mp_rating_avg']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="child[productmanagement][mp_low_avl_limit_notifcation][<%$row_index%>]" id="child_productmanagement_mp_low_avl_limit_notifcation_<%$row_index%>" value="<%$child_data[i]['mp_low_avl_limit_notifcation']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="child[productmanagement][mp_total_rate][<%$row_index%>]" id="child_productmanagement_mp_total_rate_<%$row_index%>" value="<%$child_data[i]['mp_total_rate']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="child[productmanagement][mp_date][<%$row_index%>]" id="child_productmanagement_mp_date_<%$row_index%>" value="<%$child_data[i]['mp_date']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="child[productmanagement][mp_modify_date][<%$row_index%>]" id="child_productmanagement_mp_modify_date_<%$row_index%>" value="<%$child_data[i]['mp_modify_date']%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
            <input type="hidden" name="child[productmanagement][mp_wishlist_state][<%$row_index%>]" id="child_productmanagement_mp_wishlist_state_<%$row_index%>" value="<%$child_data[i]['mp_wishlist_state']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="child[productmanagement][mp_sale_state][<%$row_index%>]" id="child_productmanagement_mp_sale_state_<%$row_index%>" value="<%$child_data[i]['mp_sale_state']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="child[productmanagement][mp_view_state][<%$row_index%>]" id="child_productmanagement_mp_view_state_<%$row_index%>" value="<%$child_data[i]['mp_view_state']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="child[productmanagement][mp_default_img][<%$row_index%>]" id="child_productmanagement_mp_default_img_<%$row_index%>" value="<%$child_data[i]['mp_default_img']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="child[productmanagement][sys_custom_field_1][<%$row_index%>]" id="child_productmanagement_sys_custom_field_1_<%$row_index%>" value="<%$child_data[i]['sys_custom_field_1']|@htmlentities%>"  class='ignore-valid ' />
            <textarea style="display:none;" name="child[productmanagement][mp_search_keyword][<%$row_index%>]" id="child_productmanagement_mp_search_keyword_<%$row_index%>"  class='ignore-valid ' ><%$child_data[i]['mp_search_keyword']%></textarea>
        </td>
        <td>
            <div class=" <%if $recMode eq 'Update'%>frm-elements-div<%/if%> " id="ch_productmanagement_cc_sh_mpistoreid_<%$row_index%>">
                <%assign var="opt_selected" value=$child_data[i]['mpistoreid']%>
                <%if $recMode eq "Update"%>
                    <%assign var="combo_arr" value=$child_opt_arr["child_productmanagement_mpistoreid_$row_index"]%>
                    <strong><%$this->general->displayKeyValueData($opt_selected, $combo_arr)%></strong>
                    <input type="hidden" class="ignore-valid" name="child[productmanagement][mpistoreid][<%$row_index%>]" id="child_productmanagement_mpistoreid_<%$row_index%>" value="<%$child_data[i]['mpistoreid']%>"  />
                <%else%>
                    <%$this->dropdown->display("child_productmanagement_mpistoreid_<%$row_index%>","child[productmanagement][mpistoreid][<%$row_index%>]","  title='<%$this->lang->line('PRODUCTMANAGEMENT_STORE_OWNER')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTMANAGEMENT_STORE_OWNER')%>'  ","|||","",$opt_selected,"child_productmanagement_mpistoreid_$row_index")%>
                <%/if%>  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mpistoreid_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_tpc_mst_categories_id_<%$row_index%>">
                <%$this->general->getCatDropdown($recMode, $child_data[i]["tpc_mst_categories_id"], $child_data, $child_id, "child[productmanagement][tpc_mst_categories_id]","child_productmanagement_tpc_mst_categories_id")%>  
            </div>
            <div>
                <label class='error' id='child_productmanagement_tpc_mst_categories_id_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_title_<%$row_index%>">
                <input type="text" placeholder="" value="<%$child_data[i]['mp_title']%>" name="child[productmanagement][mp_title][<%$row_index%>]" id="child_productmanagement_mp_title_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_PRODUCT_NAME')%>"  class='frm-size-medium'  />  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_title_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_sku_<%$row_index%>">
                <input type="text" placeholder="" value="<%$child_data[i]['mp_sku']%>" name="child[productmanagement][mp_sku][<%$row_index%>]" id="child_productmanagement_mp_sku_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_SKU')%>"  class='frm-size-medium'  />  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_sku_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_regular_price_<%$row_index%>">
                <input type="text" placeholder="<%$this->lang->line('GENERIC_0_C4600')%>" value="<%$child_data[i]['mp_regular_price']%>" name="child[productmanagement][mp_regular_price][<%$row_index%>]" id="child_productmanagement_mp_regular_price_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_REGULAR_PRICE')%>"  class='frm-size-medium'  />  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_regular_price_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_sale_price_<%$row_index%>">
                <input type="text" placeholder="<%$this->lang->line('GENERIC_0_C4600')%>" value="<%$child_data[i]['mp_sale_price']%>" name="child[productmanagement][mp_sale_price][<%$row_index%>]" id="child_productmanagement_mp_sale_price_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_SALE_PRICE')%>"  class='frm-size-medium'  />  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_sale_price_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_allow_max_purchase_<%$row_index%>">
                <input type="text" placeholder="" value="<%$child_data[i]['mp_allow_max_purchase']%>" name="child[productmanagement][mp_allow_max_purchase][<%$row_index%>]" id="child_productmanagement_mp_allow_max_purchase_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_ALLOW_MAX_PURCHASE')%>"  class='frm-size-medium'  />  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_allow_max_purchase_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_stock_<%$row_index%>">
                <input type="text" placeholder="" value="<%$child_data[i]['mp_stock']%>" name="child[productmanagement][mp_stock][<%$row_index%>]" id="child_productmanagement_mp_stock_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_CURRENT_STOCK')%>"  class='frm-size-medium'  />  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_stock_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" <%if $recMode eq 'Update'%>frm-elements-div<%/if%> " id="ch_productmanagement_cc_sh_mp_difference_per_<%$row_index%>">
                <%if $recMode eq "Update"%>
                    <%if $recMode eq "Update"%>
                        <strong><%if $child_data[i]['mp_difference_per'] neq "" %><%$child_data[i]['mp_difference_per']%><%else%>---<%/if%></strong>
                        <input type="hidden" class="ignore-valid" name="child[productmanagement][mp_difference_per][<%$row_index%>]" id="child_productmanagement_mp_difference_per_<%$row_index%>" value="<%$child_data[i]['mp_difference_per']%>" />
                    <%else%>
                        <input type="text" placeholder="" value="<%$child_data[i]['mp_difference_per']%>" name="child[productmanagement][mp_difference_per][<%$row_index%>]" id="child_productmanagement_mp_difference_per_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_DIFFERENCE_PER')%>"  class='frm-size-medium'  />
                    <%/if%>
                <%else%>
                    --- <input type="hidden" class="ignore-valid" name="child[productmanagement][mp_difference_per][<%$row_index%>]" id="child_productmanagement_mp_difference_per_<%$row_index%>" value="<%$child_data[i]['mp_difference_per']%>" />
                <%/if%>  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_difference_per_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_free_shipping_<%$row_index%>">
                <%assign var="opt_selected" value=$child_data[i]['mp_free_shipping']%>
                <%$this->dropdown->display("child_productmanagement_mp_free_shipping_<%$row_index%>","child[productmanagement][mp_free_shipping][<%$row_index%>]","  title='<%$this->lang->line('PRODUCTMANAGEMENT_FREE_SHIPPING')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTMANAGEMENT_FREE_SHIPPING')%>'  ","|||","",$opt_selected,"child_productmanagement_mp_free_shipping_$row_index")%>  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_free_shipping_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_shipping_charge_<%$row_index%>">
                <input type="text" placeholder="" value="<%$child_data[i]['mp_shipping_charge']%>" name="child[productmanagement][mp_shipping_charge][<%$row_index%>]" id="child_productmanagement_mp_shipping_charge_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_SHIPPING_CHARGE')%>"  class='frm-size-medium'  />  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_shipping_charge_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_do_return_<%$row_index%>">
                <%assign var="opt_selected" value=$child_data[i]['mp_do_return']%>
                <%$this->dropdown->display("child_productmanagement_mp_do_return_<%$row_index%>","child[productmanagement][mp_do_return][<%$row_index%>]","  title='<%$this->lang->line('PRODUCTMANAGEMENT_DO_RETURN')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTMANAGEMENT_DO_RETURN')%>'  ","|||","",$opt_selected,"child_productmanagement_mp_do_return_$row_index")%>  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_do_return_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_return_days_<%$row_index%>">
                <input type="text" placeholder="" value="<%$child_data[i]['mp_return_days']%>" name="child[productmanagement][mp_return_days][<%$row_index%>]" id="child_productmanagement_mp_return_days_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_RETURN_DAYS')%>"  class='frm-size-medium'  />  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_return_days_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_short_description_<%$row_index%>">
                <textarea placeholder=""  name="child[productmanagement][mp_short_description][<%$row_index%>]" id="child_productmanagement_mp_short_description_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_SHORT_DESCRIPTION')%>"  class='elastic frm-size-medium'  ><%$child_data[i]['mp_short_description']%></textarea>  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_short_description_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class="frm-editor-layout  " id="ch_productmanagement_cc_sh_mp_description_<%$row_index%>">
                <textarea name="child[productmanagement][mp_description][<%$row_index%>]" id="child_productmanagement_mp_description_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_DESCRIPTION')%>"  style='width:80%;'  class='frm-size-medium frm-editor-small'  ><%$child_data[i]['mp_description']%></textarea>  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_description_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_status_<%$row_index%>">
                <%assign var="opt_selected" value=$child_data[i]['mp_status']%>
                <%$this->dropdown->display("child_productmanagement_mp_status_<%$row_index%>","child[productmanagement][mp_status][<%$row_index%>]","  title='<%$this->lang->line('PRODUCTMANAGEMENT_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTMANAGEMENT_STATUS')%>'  ","|||","",$opt_selected,"child_productmanagement_mp_status_$row_index")%>  
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_status_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productmanagement_cc_sh_mp_search_tag_<%$row_index%>">
                <textarea placeholder=""  name="child[productmanagement][mp_search_tag][<%$row_index%>]" id="child_productmanagement_mp_search_tag_<%$row_index%>" title="<%$this->lang->line('PRODUCTMANAGEMENT_SEARCH_KEYWORDS')%>"  class='elastic frm-size-medium'  ><%$child_data[i]['mp_search_tag']%></textarea> 
                <span class="input-comment">
                    <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_ADD_SEARCH_KEYWORD_COMMA_SEPRATED')%>"><span class="icomoon-icon-help"></span></a>
                </span>
            </div>
            <div>
                <label class='error' id='child_productmanagement_mp_search_tag_<%$row_index%>Err'></label>
            </div>
        </td>
        <td align="center">
            <div class="controls center">
                <%if $mode eq "Update"%>
                    <a onclick="saveChildModuleSingleData('<%$mod_enc_url.child_data_save%>', '<%$mod_enc_url.child_data_add%>', '<%$child_module_name%>', '<%$recMode%>', '<%$row_index%>', '<%$enc_child_id%>', 'No', '<%$mode%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_SAVE')%>">
                        <span class="icon14 icomoon-icon-disk"></span>
                    </a>
                <%/if%>
            </div>
        </td>
    </tr>
    <%javascript%>
    <%if $child_auto_arr[$row_index]|@is_array && $child_auto_arr[$row_index]|@count gt 0%>
        var $child_chosen_auto_complete_url = admin_url+""+$("#childModuleChosenURL_<%$child_module_name%>").val()+"?";
        <%foreach name=i from=$child_auto_arr[$row_index] item=v key=k%>
            if($("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").is("select")){
            $("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").ajaxChosen({
            dataType: "json",
            type: "POST",
            url: $child_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$recMode]%>&id=<%$enc_child_id%>"
            },{
            loadingImg: admin_image_url+"chosen-loading.gif"
            });
        }
    <%/foreach%>
<%/if%>
<%/javascript%>
<%/section%>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initChildRenderJSScript(){
Project.modules.shippedorderlisting.childEvents("productmanagement", "#tr_child_row_<%$child_module_name%>_<%$row_index%>");
callGoogleMapEvents();
}
<%/javascript%>