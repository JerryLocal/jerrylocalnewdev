<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<textarea id="txt_module_help" class="perm-elem-hide"></textarea>
<div id="txt_summary_grid_id" class="perm-elem-hide">    
    <!-- Footer Summary -->
    <div>
        <!-- mso_product_qty summary -->
        <div>
            <div id="txt_esum_foot_mso_product_qty_list2">
               {0}
            </div>
        </div>
        <!-- mso_shipping_cost summary -->
        <div>
            <div id="txt_esum_foot_mso_shipping_cost_list2">
                ${0}
            </div>
        </div>
        <!-- mso_total_cost summary -->
        <div>
            <div id="txt_esum_foot_mso_total_cost_list2">
                ${0}
            </div>
        </div>
        <!-- mso_admin_commision summary -->
        <div>
            <div id="txt_esum_foot_mso_admin_commision_list2">
                ${0}
            </div>
        </div>
        <!-- mso_gateway_t_d_r summary -->
        <div>
            <div id="txt_esum_foot_mso_gateway_t_d_r_list2">
                ${0}
            </div>
        </div>
        <!-- mso_admin_commision_per summary -->
        <div>
            <div id="txt_esum_foot_mso_admin_commision_per_list2">
                ${0}
            </div>
        </div>
    </div>
</div>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line('GENERIC_LISTING')%> :: <%$this->lang->line('DELIVERED_ORDER_LISTING_V1_SALES_REPORT')%>
            </div>        
        </h3>
        <div class="header-right-drops">
        </div>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing box gradient">
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div id="scrollable_content" class="scrollable-content top-list-spacing">
        <div class="grid-data-container pad-calc-container">
            <div class="top-list-tab-layout" id="top_list_grid_layout">
            </div>
            <table class="grid-table-view " width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td id="grid_data_col">
                        <div id="pager2"></div>
                        <table id="list2"></table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>        
<input type="hidden" name="selAllRows" value="" id="selAllRows" />
<%javascript%>
    $.jgrid.no_legacy_api = true; $.jgrid.useJSON = true;
    var el_grid_settings = {}, js_col_model_json = {}, js_col_name_json = {}; 
                    
    el_grid_settings['module_name'] = '<%$module_name%>';
    el_grid_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_grid_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_grid_settings['enc_location'] = '<%$enc_loc_module%>';
    el_grid_settings['par_module '] = '<%$this->general->getAdminEncodeURL($parMod)%>';
    el_grid_settings['par_data'] = '<%$this->general->getAdminEncodeURL($parID)%>';
    el_grid_settings['par_field'] = '<%$parField%>';
    el_grid_settings['par_type'] = 'parent';
    el_grid_settings['add_page_url'] = '<%$mod_enc_url["add"]%>'; 
    el_grid_settings['edit_page_url'] =  admin_url+'<%$mod_enc_url["inline_edit_action"]%>?<%$extra_qstr%>';
    el_grid_settings['listing_url'] = admin_url+'<%$mod_enc_url["listing"]%>?<%$extra_qstr%>';
    el_grid_settings['search_refresh_url'] = admin_url+'<%$mod_enc_url["get_left_search_content"]%>?<%$extra_qstr%>';
    el_grid_settings['search_autocomp_url'] = admin_url+'<%$mod_enc_url["get_search_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['auto_complete_url'] = admin_url+'<%$mod_enc_url["get_token_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['export_url'] =  admin_url+'<%$mod_enc_url["export"]%>?<%$extra_qstr%>';
    el_grid_settings['subgrid_listing_url'] =  admin_url+'<%$mod_enc_url["get_subgrid_block"]%>?<%$extra_qstr%>';
    el_grid_settings['jparent_switchto_url'] = admin_url+'<%$parent_switch_cit["url"]%>?<%$extra_qstr%>';
    
    el_grid_settings['admin_rec_arr'] = {};
    el_grid_settings['status_arr'] = $.parseJSON('<%$status_array|@json_encode%>');
    el_grid_settings['status_lang_arr'] = $.parseJSON('<%$status_array_label|@json_encode%>');
                
    el_grid_settings['hide_add_btn'] = '';
    el_grid_settings['hide_del_btn'] = '';
    el_grid_settings['hide_status_btn'] = '';
    el_grid_settings['hide_export_btn'] = '1';
    el_grid_settings['hide_columns_btn'] = 'No';
    
    el_grid_settings['hide_advance_search'] = 'No';
    el_grid_settings['hide_search_tool'] = 'No';
    el_grid_settings['hide_multi_select'] = 'No';
    el_grid_settings['popup_add_form'] = 'No';
    el_grid_settings['popup_edit_form'] = 'No';
    el_grid_settings['popup_add_size'] = ['75%', '75%'];
    el_grid_settings['popup_edit_size'] = ['75%', '75%'];
    el_grid_settings['hide_paging_btn'] = 'No';
    el_grid_settings['hide_refresh_btn'] = 'No';
    el_grid_settings['group_search'] = '';
    
    el_grid_settings['permit_add_btn'] = '<%$add_access%>';
    el_grid_settings['permit_del_btn'] = '<%$del_access%>';
    el_grid_settings['permit_edit_btn'] = '<%$view_access%>';
    el_grid_settings['permit_expo_btn'] = '<%$expo_access%>';
    
    el_grid_settings['default_sort'] = 'msd_store_name';
    el_grid_settings['sort_order'] = 'asc';
    
    el_grid_settings['footer_row'] = 'Yes';
    el_grid_settings['grouping'] = 'No';
    el_grid_settings['group_attr'] = {};
    
    el_grid_settings['inline_add'] = 'No';
    el_grid_settings['rec_position'] = 'Top';
    el_grid_settings['auto_width'] = 'Yes';
    el_grid_settings['subgrid'] = 'Yes';
    el_grid_settings['colgrid'] = 'No';
    el_grid_settings['rating_allow'] = 'No';
    el_grid_settings['listview'] = 'list';
    el_grid_settings['filters_arr'] = $.parseJSON('<%$default_filters|@json_encode%>');
    el_grid_settings['top_filter'] = [];
    el_grid_settings['buttons_arr'] = [];
    
    js_col_name_json = [{
        "name": "msd_store_name",
        "label": "<%$this->lang->line('DELIVERED_ORDER_LISTING_V1_STORE_NAME')%>"
    },
    {
        "name": "mo_mst_order_id",
        "label": "<%$this->lang->line('DELIVERED_ORDER_LISTING_V1_ORDER_NO')%>"
    },
    {
        "name": "mo_date",
        "label": "<%$this->lang->line('DELIVERED_ORDER_LISTING_V1_ORDER_DATE')%>"
    },
    {
        "name": "mso_product_qty",
        "label": "<%$this->lang->line('DELIVERED_ORDER_LISTING_V1_QTY_C46')%>"
    },
    {
        "name": "mso_mst_order_id",
        "label": "<%$this->lang->line('DELIVERED_ORDER_LISTING_V1_SUBORDER_NO')%>"
    },
    {
        "name": "mso_product_price",
        "label": "<%$this->lang->line('DELIVERED_ORDER_LISTING_V1_UNIT_PRICE')%>"
    },
    {
        "name": "mso_shipping_cost",
        "label": "<%$this->lang->line('DELIVERED_ORDER_LISTING_V1_SHIPPING')%>"
    },
    {
        "name": "mso_total_cost",
        "label": "<%$this->lang->line('DELIVERED_ORDER_LISTING_V1_SUB_TOTAL')%>"
    },
    {
        "name": "mso_admin_commision",
        "label": "<%$this->lang->line('DELIVERED_ORDER_LISTING_V1_COMMISION')%>"
    },
    {
        "name": "mso_gateway_t_d_r",
        "label": "<%$this->lang->line('DELIVERED_ORDER_LISTING_V1_PAYPAL')%>"
    },
    {
        "name": "mso_admin_commision_per",
        "label": "<%$this->lang->line('DELIVERED_ORDER_LISTING_V1_PROFIT')%>"
    }];
    
    js_col_model_json = [{
        "name": "msd_store_name",
        "index": "msd_store_name",
        "label": "<%$list_config['msd_store_name']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['msd_store_name']['width']%>",
        "search": <%if $list_config['msd_store_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['msd_store_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['msd_store_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['msd_store_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['msd_store_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "delivered_order_listing_v1",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['msd_store_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "delivered_order_listing_v1",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['msd_store_name']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mo_mst_order_id",
        "index": "mo_mst_order_id",
        "label": "<%$list_config['mo_mst_order_id']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mo_mst_order_id']['width']%>",
        "search": <%if $list_config['mo_mst_order_id']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_mst_order_id']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_mst_order_id']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_mst_order_id']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_mst_order_id']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "delivered_order_listing_v1",
                "aria-unique-name": "mo_mst_order_id",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mo_mst_order_id']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "delivered_order_listing_v1",
            "aria-unique-name": "mo_mst_order_id",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mo_mst_order_id']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mo_date",
        "index": "mo_date",
        "label": "<%$list_config['mo_date']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mo_date']['width']%>",
        "search": <%if $list_config['mo_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "delivered_order_listing_v1",
                "aria-unique-name": "mo_date",
                "autocomplete": "off",
                "class": "search-inline-date",
                "aria-date-format": "<%$this->general->getAdminJSMoments('date')%>"
            },
            "sopt": dateSearchOpts,
            "searchhidden": <%if $list_config['mo_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchGridDateRangePicker
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "delivered_order_listing_v1",
            "aria-unique-name": "mo_date",
            "aria-date-format": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
            "aria-min": "",
            "aria-max": "",
            "placeholder": "",
            "class": "inline-edit-row inline-date-edit date-picker-icon dateOnly"
        },
        "ctrl_type": "date",
        "default_value": "<%$list_config['mo_date']['default']%>",
        "filterSopt": "bt"
    },
    {
        "name": "mso_product_qty",
        "index": "mso_product_qty",
        "label": "<%$list_config['mso_product_qty']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['mso_product_qty']['width']%>",
        "search": <%if $list_config['mso_product_qty']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_product_qty']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_product_qty']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_product_qty']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_product_qty']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "delivered_order_listing_v1",
                "aria-unique-name": "mso_product_qty",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['mso_product_qty']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["mso_product_qty"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=mso_product_qty&mode=<%$mod_enc_mode["Search"]%>&rformat=html'<%/if%>,
            "value": <%if $count_arr["mso_product_qty"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["mso_product_qty"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['mso_product_qty']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["mso_product_qty"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "delivered_order_listing_v1",
            "aria-unique-name": "mso_product_qty",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=mso_product_qty&mode=<%$mod_enc_mode["Update"]%>&rformat=html',
            "dataInit": <%if $count_arr['mso_product_qty']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["mso_product_qty"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DELIVERED_ORDER_LISTING_V1_QTY_C46')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['mso_product_qty']['default']%>",
        "filterSopt": "in",
        "stype": "select",
        "footerTpl": {
            "subTPL": $.trim($("#txt_esum_foot_mso_product_qty_list2").html())
        }
    },
    {
        "name": "mso_mst_order_id",
        "index": "mso_mst_order_id",
        "label": "<%$list_config['mso_mst_order_id']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mso_mst_order_id']['width']%>",
        "search": <%if $list_config['mso_mst_order_id']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_mst_order_id']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_mst_order_id']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_mst_order_id']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_mst_order_id']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "delivered_order_listing_v1",
                "aria-unique-name": "mso_mst_order_id",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mso_mst_order_id']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "delivered_order_listing_v1",
            "aria-unique-name": "mso_mst_order_id",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_mst_order_id']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mso_product_price",
        "index": "mso_product_price",
        "label": "<%$list_config['mso_product_price']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_product_price']['width']%>",
        "search": <%if $list_config['mso_product_price']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_product_price']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_product_price']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_product_price']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_product_price']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "delivered_order_listing_v1",
                "aria-unique-name": "mso_product_price",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_product_price']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "delivered_order_listing_v1",
            "aria-unique-name": "mso_product_price",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_product_price']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency"
    },
    {
        "name": "mso_shipping_cost",
        "index": "mso_shipping_cost",
        "label": "<%$list_config['mso_shipping_cost']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_shipping_cost']['width']%>",
        "search": <%if $list_config['mso_shipping_cost']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_shipping_cost']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_shipping_cost']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_shipping_cost']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_shipping_cost']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "delivered_order_listing_v1",
                "aria-unique-name": "mso_shipping_cost",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_shipping_cost']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "delivered_order_listing_v1",
            "aria-unique-name": "mso_shipping_cost",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_shipping_cost']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency",
        "footerTpl": {
            "subTPL": $.trim($("#txt_esum_foot_mso_shipping_cost_list2").html())
        }
    },
    {
        "name": "mso_total_cost",
        "index": "mso_total_cost",
        "label": "<%$list_config['mso_total_cost']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_total_cost']['width']%>",
        "search": <%if $list_config['mso_total_cost']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_total_cost']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_total_cost']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_total_cost']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_total_cost']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "delivered_order_listing_v1",
                "aria-unique-name": "mso_total_cost",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_total_cost']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "delivered_order_listing_v1",
            "aria-unique-name": "mso_total_cost",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_total_cost']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency",
        "footerTpl": {
            "subTPL": $.trim($("#txt_esum_foot_mso_total_cost_list2").html())
        }
    },
    {
        "name": "mso_admin_commision",
        "index": "mso_admin_commision",
        "label": "<%$list_config['mso_admin_commision']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_admin_commision']['width']%>",
        "search": <%if $list_config['mso_admin_commision']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_admin_commision']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_admin_commision']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_admin_commision']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_admin_commision']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "delivered_order_listing_v1",
                "aria-unique-name": "mso_admin_commision",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_admin_commision']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "delivered_order_listing_v1",
            "aria-unique-name": "mso_admin_commision",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_admin_commision']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency",
        "footerTpl": {
            "subTPL": $.trim($("#txt_esum_foot_mso_admin_commision_list2").html())
        }
    },
    {
        "name": "mso_gateway_t_d_r",
        "index": "mso_gateway_t_d_r",
        "label": "<%$list_config['mso_gateway_t_d_r']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_gateway_t_d_r']['width']%>",
        "search": <%if $list_config['mso_gateway_t_d_r']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_gateway_t_d_r']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_gateway_t_d_r']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_gateway_t_d_r']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_gateway_t_d_r']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "delivered_order_listing_v1",
                "aria-unique-name": "mso_gateway_tdr",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_gateway_t_d_r']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "delivered_order_listing_v1",
            "aria-unique-name": "mso_gateway_tdr",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_gateway_t_d_r']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency",
        "footerTpl": {
            "subTPL": $.trim($("#txt_esum_foot_mso_gateway_t_d_r_list2").html())
        }
    },
    {
        "name": "mso_admin_commision_per",
        "index": "mso_admin_commision_per",
        "label": "<%$list_config['mso_admin_commision_per']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_admin_commision_per']['width']%>",
        "search": <%if $list_config['mso_admin_commision_per']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_admin_commision_per']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_admin_commision_per']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_admin_commision_per']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_admin_commision_per']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "delivered_order_listing_v1",
                "aria-unique-name": "mso_admin_commision_per",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_admin_commision_per']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "delivered_order_listing_v1",
            "aria-unique-name": "mso_admin_commision_per",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_admin_commision_per']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency",
        "footerTpl": {
            "subTPL": $.trim($("#txt_esum_foot_mso_admin_commision_per_list2").html())
        }
    }];
         
    initMainGridListing();
    createTooltipHeading();
    callSwitchToParent();
<%/javascript%>
<%$this->js->add_js("admin/custom/delivered_order_extended.js")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 