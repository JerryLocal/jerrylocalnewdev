<%if $this->input->get_post('iframe') eq 'true'%>
    <div>
        <h1 class="center" style="width:90%"><%$this->lang->line('GENERIC_FORBIDDEN')%></h1>
        <h2 class="center errormsg">
            <%if $err_message neq ''%>
                <%$err_message%>
            <%else%>
                You don't have permissions to access this page.
            <%/if%>
        </h2>
    </div>
<%else%>
    <div class="container-fluid">
        <div class="errorContainer">
            <div class="page-header">
                <h1 class="center">404 ERROR</h1>
                <h2>
                    <%if $err_message neq ''%>
                        <%$err_message%>
                    <%else%>
                        The page you are looking for has not been found.
                    <%/if%>
                </h2>
                <p>The page you are looking for might have been removed, had its name changed, or unavailable. </p>
            </div>
            <div class="error-link-back"><p>Please <a href="#">click here</a> to go back to our home page </p></div>
        </div>
    </div>
    <style>
        .errorContainer {margin-top: 10%;}
        .errorContainer h1 {font-size: 6em;line-height: 1.5em;background: none;padding-left: 0;height: auto;padding-top: 0;}
    </style>
<%/if%>