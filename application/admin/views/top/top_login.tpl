<%assign var="logo_file_url" value=$rl_theme_arr['gen_default_logo']%>
<%if $this->config->item('COMPANY_LOGO') neq ''%>
    <%assign var="view_logo_path" value=$this->config->item("settings_files_path")|@cat:$this->config->item('COMPANY_LOGO')%>
    <%assign var="view_logo_url" value=$this->config->item("settings_files_url")|@cat:$this->config->item('COMPANY_LOGO')%>
    <%if $view_logo_path|@file_exists%>
        <%assign var="logo_file_url" value=$this->general->getResizedLogoImage($view_logo_path, $view_logo_url)%>
    <%/if%>
<%/if%>

<div class="top-model-view logo container-fluid navbar">
    <a href="<%$this->config->item('admin_url')%>" class="brand">
        <img alt="<%$this->config->item('COMPANY_NAME')%>" class="admin-logo-top" src="<%$logo_file_url%>" title="<%$this->config->item('COMPANY_NAME')%>">
    </a>
</div>
<div class="toprightarea">
    <div class="date-right">
        <%assign var="now_date_time" value=$smarty.now|date_format:"%Y-%m-%d %H:%M:%S"%>
        <span><%$this->general->dateTimeSystemFormat($now_date_time)%></span>
    </div>
</div>