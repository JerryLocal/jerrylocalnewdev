<%if $this->input->get_post('iframe') eq 'true'%>
    <div>
        <h1 class="center" style="width:90%"><%$this->lang->line('GENERIC_FORBIDDEN')%></h1>
        <h2 class="center errormsg">
            <%if $err_message neq ''%>
                <%$err_message%>
            <%else%>
                You don't have permissions to access this page.
            <%/if%>
        </h2>
    </div>
<%else%>
    <div class="container-fluid">
        <div class="errorContainer">
            <div class="page-header">
                <h1 class="center"><%$this->lang->line('GENERIC_FORBIDDEN')%></h1>
            </div>
            <h2 class="center errormsg">
                <%if $err_message neq ''%>
                    <%$err_message%>
                <%else%>
                    You don't have permissions to access this page.
                <%/if%>
            </h2>
            <div class="center">
                <a href="javascript://" onclick="loadLastVisitedURL()" class="btn" style="margin-right:10px;"><span class="icon16 icomoon-icon-arrow-left-10"></span><%$this->lang->line('GENERIC_GO_BACK')%></a>
                <a href="javascript://" onclick="loadAdminDashboardPage()" class="btn"><span class="icon16 icomoon-icon-screen"></span><%$this->lang->line('GENERIC_SITEMAP')%></a>
            </div>
        </div>
    </div>
    <style>
        .errorContainer {margin-top: 10%;}
        .errorContainer h1 {font-size: 6em;line-height: 1.5em;background: none;padding-left: 0;height: auto;padding-top: 0;}
    </style>
<%/if%>