<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <TITLE><%$this->systemsettings->getSettings('CPANEL_TITLE')%></TITLE>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
        <base href="<%$admin_url%>">
        <%$this->css->add_css("admin/style.css","admin/icons.css","bootstrap/bootstrap.css","bootstrap/main.css","theme/`$this->config->item('ADMIN_THEME_DISPLAY')`/theme.css")%>
        <%$this->css->add_css("theme/`$this->config->item('ADMIN_THEME_DISPLAY')`/`$this->config->item('ADMIN_THEME_PATTERN')`","theme/`$this->config->item('ADMIN_THEME_CUSTOMIZE')`")%>
        <%$this->css->css_src()%>
        <%$this->js->add_js("admin/bootstrap/bootstrap.js","admin/misc/cookie/jquery.cookie.js","admin/misc/mouse/jquery.mousewheel.js")%>
        <script type="text/javascript">
            var admin_url = "<%$admin_url%>";
        </script>
        <style>
            .errorContainer {margin-top: 10%;}
            .errorContainer h1 {font-size: 6em;line-height: 1.5em;background: none;padding-left: 0;height: auto;padding-top: 0;}
        </style>
    </head>
    <body>
        <div>
            <div class="container-fluid">
                <div class="errorContainer">
                    <div class="page-header">
                        <h1 class="center">Session <small>expired</small></h1>
                    </div>
                    <h2 class="center errormsg">Your session is expired. Please login again.</h2>
                    <div class="center">
                        <a href="<%$admin_url%>user/login/entry?_=<%$smarty.now|date_format:'%Y%m%d%H%M%S'%>"  class="btn btn-default"><span class="icon16 icomoon-icon-enter"></span>Login here</a>
                    </div>
                </div>
            </div>         
        </div>
        <%$this->js->js_src()%>
    </body>
</html>
