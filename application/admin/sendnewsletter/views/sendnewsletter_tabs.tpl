<ul class="nav nav-tabs">
    <li <%if $module_name eq "sendnewsletter"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('SENDNEWSLETTER_SEND_NEWSLETTER')%>" 
            <%if $module_name eq "sendnewsletter"%> 
                href="javascript://"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('sendnewsletter/sendnewsletter/add')%>|mode|<%$mod_enc_mode['Update']%>|id|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('SENDNEWSLETTER_SEND_NEWSLETTER')%>
        </a>
    </li>
    <li <%if $module_name eq "newslettersubscription"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('SENDNEWSLETTER_NEWSLETTER__SUBSCRIPTION')%> <%$this->lang->line('GENERIC_LIST')%>" 
            <%if $module_name eq "newslettersubscription"%> 
                href="javascript://"
            <%elseif $module_name eq "sendnewsletter"%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('newslettersubscription/newslettersubscription/index')%>|parMod|<%$this->general->getAdminEncodeURL('sendnewsletter')%>|parID|<%$this->general->getAdminEncodeURL($data['tnc_ns_group_id'])%>"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('newslettersubscription/newslettersubscription/index')%>|parMod|<%$this->general->getAdminEncodeURL('sendnewsletter')%>|parID|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('SENDNEWSLETTER_NEWSLETTER__SUBSCRIPTION')%>
        </a>
    </li>
</ul>            