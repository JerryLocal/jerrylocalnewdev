<%section name=i loop=1%>
    <div id="div_child_row_<%$child_module_name%>_<%$row_index%>">
        <input type="hidden" name="child[newslettersubscription][id][<%$row_index%>]" id="child_newslettersubscription_id_<%$row_index%>" value="<%$child_id%>" />
        <input type="hidden" name="child[newslettersubscription][enc_id][<%$row_index%>]" id="child_newslettersubscription_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
        <input type="hidden" name="child[newslettersubscription][mns_login_user_id][<%$row_index%>]" id="child_newslettersubscription_mns_login_user_id_<%$row_index%>" value="<%$child_data[i]['mns_login_user_id']|@htmlentities%>"  class='ignore-valid ' />
        <input type="hidden" name="child[newslettersubscription][mns_newsletter_group_id][<%$row_index%>]" id="child_newslettersubscription_mns_newsletter_group_id_<%$row_index%>" value="<%$child_data[i]['mns_newsletter_group_id']%>"  class='ignore-valid ' />
        <div class="col-del controls">
            <%if $mode eq "Update"%>
                <a onclick="saveChildModuleSingleData('<%$mod_enc_url.child_data_save%>', '<%$mod_enc_url.child_data_add%>', '<%$child_module_name%>', '<%$recMode%>', '<%$row_index%>', '<%$enc_child_id%>', 'No', '<%$mode%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_SAVE')%>">
                    <span class="icon14 icomoon-icon-disk"></span>
                </a>
            <%/if%>
            <%if $recMode eq "Update"%>
                <a onclick="deleteChildModuleSingleData('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>','<%$enc_child_id%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                    <span class="icon16 icomoon-icon-remove"></span>
                </a>
            <%else%>
                <a onclick="deleteChildModuleRow('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                    <span class="icon16 icomoon-icon-remove"></span>
                </a>
            <%/if%>
        </div>
        <div class="form-row row-fluid" id="ch_newslettersubscription_cc_sh_mns_name">
            <label class="form-label span3">
                <%$this->lang->line('NEWSLETTERSUBSCRIPTION_NAME')%>  
            </label> 
            <div class="form-right-div  ">
                <input type="text" placeholder="" value="<%$child_data[i]['mns_name']%>" name="child[newslettersubscription][mns_name][<%$row_index%>]" id="child_newslettersubscription_mns_name_<%$row_index%>" title="<%$this->lang->line('NEWSLETTERSUBSCRIPTION_NAME')%>"  class='frm-size-medium'  readonly='readonly'  />  
            </div>
            <div class="error-msg-form " >
                <label class='error' id='child_newslettersubscription_mns_name_<%$row_index%>Err'></label>
            </div>
        </div>
        <div class="form-row row-fluid" id="ch_newslettersubscription_cc_sh_mns_email">
            <label class="form-label span3">
                <%$this->lang->line('NEWSLETTERSUBSCRIPTION_EMAIL')%>  
            </label> 
            <div class="form-right-div  ">
                <input type="text" placeholder="" value="<%$child_data[i]['mns_email']%>" name="child[newslettersubscription][mns_email][<%$row_index%>]" id="child_newslettersubscription_mns_email_<%$row_index%>" title="<%$this->lang->line('NEWSLETTERSUBSCRIPTION_EMAIL')%>"  class='frm-size-medium'  readonly='readonly'  />  
            </div>
            <div class="error-msg-form " >
                <label class='error' id='child_newslettersubscription_mns_email_<%$row_index%>Err'></label>
            </div>
        </div>
        <div class="form-row row-fluid" id="ch_newslettersubscription_cc_sh_mns_date">
            <label class="form-label span3">
                <%$this->lang->line('NEWSLETTERSUBSCRIPTION_DATE')%>  
            </label> 
            <div class="form-right-div  ">
                <input type="text" placeholder="" value="<%$child_data[i]['mns_date']%>" name="child[newslettersubscription][mns_date][<%$row_index%>]" id="child_newslettersubscription_mns_date_<%$row_index%>" title="<%$this->lang->line('NEWSLETTERSUBSCRIPTION_DATE')%>"  class='frm-size-medium'  readonly='readonly'  />  
            </div>
            <div class="error-msg-form " >
                <label class='error' id='child_newslettersubscription_mns_date_<%$row_index%>Err'></label>
            </div>
        </div>
        <div class="form-row row-fluid" id="ch_newslettersubscription_cc_sh_mns_modify_date">
            <label class="form-label span3">
                <%$this->lang->line('NEWSLETTERSUBSCRIPTION_MODIFY_DATE')%>  
            </label> 
            <div class="form-right-div  ">
                <input type="text" placeholder="" value="<%$child_data[i]['mns_modify_date']%>" name="child[newslettersubscription][mns_modify_date][<%$row_index%>]" id="child_newslettersubscription_mns_modify_date_<%$row_index%>" title="<%$this->lang->line('NEWSLETTERSUBSCRIPTION_MODIFY_DATE')%>"  class='frm-size-medium'  readonly='readonly'  />  
            </div>
            <div class="error-msg-form " >
                <label class='error' id='child_newslettersubscription_mns_modify_date_<%$row_index%>Err'></label>
            </div>
        </div>
        <div class="form-row row-fluid" id="ch_newslettersubscription_cc_sh_mns_status">
            <label class="form-label span3">
                <%$this->lang->line('NEWSLETTERSUBSCRIPTION_STATUS')%>  
            </label> 
            <div class="form-right-div  ">
                <%assign var="opt_selected" value=$child_data[i]['mns_status']%>
                <%$this->dropdown->display("child_newslettersubscription_mns_status_<%$row_index%>","child[newslettersubscription][mns_status][<%$row_index%>]","  title='<%$this->lang->line('NEWSLETTERSUBSCRIPTION_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  readonly='readonly'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'NEWSLETTERSUBSCRIPTION_STATUS')%>'  ","|||","",$opt_selected,"child_newslettersubscription_mns_status_$row_index")%>  
            </div>
            <div class="error-msg-form " >
                <label class='error' id='child_newslettersubscription_mns_status_<%$row_index%>Err'></label>
            </div>
        </div>
    </div>
    <%javascript%>
    <%if $child_auto_arr[$row_index]|@is_array && $child_auto_arr[$row_index]|@count gt 0%>
        var $child_chosen_auto_complete_url = admin_url+""+$("#childModuleChosenURL_<%$child_module_name%>").val()+"?";
        <%foreach name=i from=$child_auto_arr[$row_index] item=v key=k%>
            if($("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").is("select")){
            $("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").ajaxChosen({
            dataType: "json",
            type: "POST",
            url: $child_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$recMode]%>&id=<%$enc_child_id%>"
            },{
            loadingImg: admin_image_url+"chosen-loading.gif"
            });
        }
    <%/foreach%>
<%/if%>
<%/javascript%>
<%/section%>
<hr class="hr-line">
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initChildRenderJSScript(){
Project.modules.sendnewsletter.childEvents("newslettersubscription", "#div_child_row_<%$child_module_name%>_<%$row_index%>");
callGoogleMapEvents();
}
<%/javascript%>