<%javascript%>
    $.fn.editable.defaults.mode = 'inline', $.fn.editable.defaults.clear = false;
    var el_subview_settings = {}, view_js_col_model_json = {}, view_token_input_assign = {}, view_token_pre_populates = {};
      
    el_subview_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_subview_settings['extra_qstr'] = '<%$extra_qstr%>';
                    
    el_subview_settings['view_id'] = '<%$subgrid_view_id%>';
    el_subview_settings['edit_id'] = '<%$enc_view_primary_id%>';
    el_subview_settings['module_name'] = '<%$module_name%>';
            
    el_subview_settings['edit_page_url'] = admin_url+'<%$mod_enc_url["inline_edit_action"]%>?&oper=edit&<%$extra_qstr%>';
    el_subview_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_subview_settings['permit_edit_btn'] = '<%$edit_access%>';

    view_js_col_model_json =  {};
    
    initSubGridDetailView();
<%/javascript%>
    
<div class="expand-detail-view">                        
    <%if $exp_module_name eq 'confirmedorderslisiting'%>
        <table id="detail_view_block" class="jqgrid-subview" width="100%" cellpadding="2" cellspacing="2">
            <tr>
                <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_SKU')%>: </strong></td>
                <td valign="top" width="18%"><%$data['mso_product_sku']%></td>
                <td valign="top" width="2%">&nbsp; </td>
                <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_QTY')%>: </strong></td>
                <td valign="top" width="18%"><%$data['mso_product_qty']%></td>
                <td valign="top" width="2%">&nbsp; </td>
                <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_NAME')%>: </strong></td>
                <td valign="top" width="18%"><%$data['mo_buyer_name']%></td>
            </tr>
            <tr>
                <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_NAME')%>: </strong></td>
                <td valign="top" width="18%"><%$data['mso_product_name']%></td>
                <td valign="top" width="2%">&nbsp; </td>
                <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_PRICE')%>: </strong></td>
                <td valign="top" width="18%"><%$data['mso_product_price']%></td>
                <td valign="top" width="2%">&nbsp; </td>
                <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_PHONE')%>: </strong></td>
                <td valign="top" width="18%"><%$data['mo_buyer_phone']%></td>
            </tr>
            <tr>
                <td valign="top" width="12%"><strong>Sub Order No: </strong></td>
                <td valign="top" width="18%"><%$data['custom_order_item_id']%></td>
                <td valign="top" width="2%">&nbsp; </td>
                <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING')%>: </strong></td>
                <td valign="top" width="18%"><%$data['mso_shipping_cost']%></td>
                <td valign="top" width="2%">&nbsp; </td>
                <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_EMAIL')%>: </strong></td>
                <td valign="top" width="18%"><%$data['mo_buyer_email']%></td>
            </tr>
            <tr>
                <td valign="top" width="12%"><strong>Order No: </strong></td>
                <td valign="top" width="18%"><%$data['mso_mst_order_id']%></td>
                <td valign="top" width="2%">&nbsp; </td>
                <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_TOTAL')%>: </strong></td>
                <td valign="top" width="18%"><%$data['mso_total_cost']%></td>
                <td valign="top" width="2%">&nbsp; </td>
                <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_ADDRESS')%>: </strong></td>
                <td valign="top" width="18%"><%$data['sys_custom_field_2']%></td>
            </tr>
            <tr>
                <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_SELLER_NAME')%>: </strong></td>
                <td valign="top" width="18%"><%$data['ma_name']%></td>
                <td valign="top" width="2%">&nbsp; </td>
                <td valign="top" width="12%"></td>
                <td valign="top" width="18%"></td>
                <td valign="top" width="2%">&nbsp; </td>
                <td valign="top" width="12%"></td>
                <td valign="top" width="18%"></td>
            </tr>
        </table>
    <%/if%>
</div>