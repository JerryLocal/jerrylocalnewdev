<%section name=i loop=1%>
    <tr id="tr_child_row_<%$child_module_name%>_<%$row_index%>">
        <td class="row-num-child">
            <span class="row-num-span"><%$row_index%></span>
            <input type="hidden" name="child[optionvaluemaster][id][<%$row_index%>]" id="child_optionvaluemaster_id_<%$row_index%>" value="<%$child_id%>" />
            <input type="hidden" name="child[optionvaluemaster][enc_id][<%$row_index%>]" id="child_optionvaluemaster_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
            <input type="hidden" name="child[optionvaluemaster][mcov_mst_category_options_id][<%$row_index%>]" id="child_optionvaluemaster_mcov_mst_category_options_id_<%$row_index%>" value="<%$child_data[i]['mcov_mst_category_options_id']%>"  class='ignore-valid ' />
            <input type="hidden" name="child[optionvaluemaster][sys_custom_field_1][<%$row_index%>]" id="child_optionvaluemaster_sys_custom_field_1_<%$row_index%>" value="<%$child_data[i]['sys_custom_field_1']|@htmlentities%>"  class='ignore-valid ' />
        </td>
        <td>
            <div class=" " id="ch_optionvaluemaster_cc_sh_mcov_value_<%$row_index%>">
                <input type="text" placeholder="" value="<%$child_data[i]['mcov_value']%>" name="child[optionvaluemaster][mcov_value][<%$row_index%>]" id="child_optionvaluemaster_mcov_value_<%$row_index%>" title="<%$this->lang->line('OPTIONVALUEMASTER_VALUE')%>"  class='frm-size-medium'  />  
            </div>
            <div>
                <label class='error' id='child_optionvaluemaster_mcov_value_<%$row_index%>Err'></label>
            </div>
        </td>
        <td align="center">
            <div class="controls center">
                <%if $mode eq "Update"%>
                    <a onclick="saveChildModuleSingleData('<%$mod_enc_url.child_data_save%>', '<%$mod_enc_url.child_data_add%>', '<%$child_module_name%>', '<%$recMode%>', '<%$row_index%>', '<%$enc_child_id%>', 'No', '<%$mode%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_SAVE')%>">
                        <span class="icon14 icomoon-icon-disk"></span>
                    </a>
                <%/if%>
                <%if $recMode eq "Update"%>
                    <a onclick="deleteChildModuleSingleData('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>','<%$enc_child_id%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                        <span class="icon16 icomoon-icon-remove"></span>
                    </a>
                <%else%>
                    <a onclick="deleteChildModuleRow('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                        <span class="icon16 icomoon-icon-remove"></span>
                    </a>
                <%/if%>
            </div>
        </td>
    </tr>
    <%javascript%>
    <%if $child_auto_arr[$row_index]|@is_array && $child_auto_arr[$row_index]|@count gt 0%>
        var $child_chosen_auto_complete_url = admin_url+""+$("#childModuleChosenURL_<%$child_module_name%>").val()+"?";
        <%foreach name=i from=$child_auto_arr[$row_index] item=v key=k%>
            if($("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").is("select")){
            $("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").ajaxChosen({
            dataType: "json",
            type: "POST",
            url: $child_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$recMode]%>&id=<%$enc_child_id%>"
            },{
            loadingImg: admin_image_url+"chosen-loading.gif"
            });
        }
    <%/foreach%>
<%/if%>
<%/javascript%>
<%/section%>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initChildRenderJSScript(){
Project.modules.categoryoptions.childEvents("optionvaluemaster", "#tr_child_row_<%$child_module_name%>_<%$row_index%>");
callGoogleMapEvents();
}
<%/javascript%>