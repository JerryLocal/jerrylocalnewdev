<%section name=i loop=1%>
    <div id="div_child_row_<%$child_module_name%>_<%$row_index%>">
        <input type="hidden" name="child[categoryoptionvalue][id][<%$row_index%>]" id="child_categoryoptionvalue_id_<%$row_index%>" value="<%$child_id%>" />
        <input type="hidden" name="child[categoryoptionvalue][enc_id][<%$row_index%>]" id="child_categoryoptionvalue_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
        <input type="hidden" name="child[categoryoptionvalue][mcov_category_option_id][<%$row_index%>]" id="child_categoryoptionvalue_mcov_category_option_id_<%$row_index%>" value="<%$child_data[i]['mcov_category_option_id']|@htmlentities%>"  class='ignore-valid ' />
        <div class="col-del controls">
            <%if $mode eq "Update"%>
                <a onclick="saveChildModuleSingleData('<%$mod_enc_url.child_data_save%>', '<%$mod_enc_url.child_data_add%>', '<%$child_module_name%>', '<%$recMode%>', '<%$row_index%>', '<%$enc_child_id%>', 'No', '<%$mode%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_SAVE')%>">
                    <span class="icon14 icomoon-icon-disk"></span>
                </a>
            <%/if%>
            <%if $recMode eq "Update"%>
                <a onclick="deleteChildModuleSingleData('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>','<%$enc_child_id%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                    <span class="icon16 icomoon-icon-remove"></span>
                </a>
            <%else%>
                <a onclick="deleteChildModuleRow('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                    <span class="icon16 icomoon-icon-remove"></span>
                </a>
            <%/if%>
        </div>
        <div class="form-row row-fluid" id="ch_categoryoptionvalue_cc_sh_mcov_value">
            <label class="form-label span3">
                <%$this->lang->line('CATEGORYOPTIONVALUE_VALUE')%>  
            </label> 
            <div class="form-right-div  ">
                <input type="text" placeholder="" value="<%$child_data[i]['mcov_value']%>" name="child[categoryoptionvalue][mcov_value][<%$row_index%>]" id="child_categoryoptionvalue_mcov_value_<%$row_index%>" title="<%$this->lang->line('CATEGORYOPTIONVALUE_VALUE')%>"  class='frm-size-medium'  />  
            </div>
            <div class="error-msg-form " >
                <label class='error' id='child_categoryoptionvalue_mcov_value_<%$row_index%>Err'></label>
            </div>
        </div>
    </div>
    <%javascript%>
    <%if $child_auto_arr[$row_index]|@is_array && $child_auto_arr[$row_index]|@count gt 0%>
        var $child_chosen_auto_complete_url = admin_url+""+$("#childModuleChosenURL_<%$child_module_name%>").val()+"?";
        <%foreach name=i from=$child_auto_arr[$row_index] item=v key=k%>
            if($("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").is("select")){
            $("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").ajaxChosen({
            dataType: "json",
            type: "POST",
            url: $child_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$recMode]%>&id=<%$enc_child_id%>"
            },{
            loadingImg: admin_image_url+"chosen-loading.gif"
            });
        }
    <%/foreach%>
<%/if%>
<%/javascript%>
<%/section%>
<hr class="hr-line">
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initChildRenderJSScript(){
Project.modules.categoryoptions.childEvents("categoryoptionvalue", "#div_child_row_<%$child_module_name%>_<%$row_index%>");
callGoogleMapEvents();
}
<%/javascript%>