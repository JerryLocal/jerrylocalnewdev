<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: 
                <%if $parent_switch_combo[$parID] neq ""%>
                    <%$parent_switch_combo[$parID]%> :: 
                <%/if%>
                <%$this->lang->line('CATEGORYOPTIONS_CATEGORY_OPTIONS')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','CATEGORYOPTIONS_CATEGORY_OPTIONS')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div <%if $parMod neq ''%>top-frm-tab-spacing<%else%>top-frm-spacing<%/if%>" >
    <input type="hidden" id="projmod" name="projmod" value="categoryoptions" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
        <div id="ad_form_outertab" class="module-navigation-tabs">
            <%if $tabing_allow eq true%>
                <%if $parMod eq "categorymanagement" && $parID neq ""%>
                    <%include file="../../categorymanagement/views/categorymanagement_tabs.tpl"%>
                <%/if%>
            <%/if%>
        </div>
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing top-frm-block-spacing">
        <div id="categoryoptions" class="frm-elem-block frm-stand-view">
            <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                <div class="main-content-block" id="main_content_block">
                    <div style="width:98%" class="frm-block-layout pad-calc-container">
                        <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                            <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('CATEGORYOPTIONS_CATEGORY_OPTIONS')%></h4></div>
                            <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                <%if $parMod eq "categorymanagement"%>
                                    <%if $mode eq "Add"%>
                                        <input type="hidden" value="<%$parID%>" name="mco_mst_categories_id" id="mco_mst_categories_id"  class="ignore-valid"/>
                                    <%else%>
                                        <input type="hidden" value="<%$data['mco_mst_categories_id']%>" name="mco_mst_categories_id" id="mco_mst_categories_id"  class="ignore-valid"/>
                                    <%/if%>
                                <%else%>
                                    <div class="form-row row-fluid" id="cc_sh_mco_mst_categories_id">
                                        <label class="form-label span3">
                                            <%$this->lang->line('CATEGORYOPTIONS_CATEGORY_NAME')%> <em>*</em> 
                                        </label> 
                                        <div class="form-right-div   ">
                                            <%assign var="opt_selected" value=$data['mco_mst_categories_id']%>
                                            <%$this->dropdown->display("mco_mst_categories_id","mco_mst_categories_id","  title='<%$this->lang->line('CATEGORYOPTIONS_CATEGORY_NAME')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'CATEGORYOPTIONS_CATEGORY_NAME')%>'  ", "|||", "", $opt_selected,"mco_mst_categories_id")%>
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='mco_mst_categories_idErr'></label></div>
                                    </div>
                                <%/if%>
                                <div class="form-row row-fluid" id="cc_sh_mco_mst_options_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('CATEGORYOPTIONS_OPTION_NAME')%> <em>*</em> 
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mco_mst_options_id']%>
                                        <%$this->dropdown->display("mco_mst_options_id","mco_mst_options_id","  title='<%$this->lang->line('CATEGORYOPTIONS_OPTION_NAME')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'CATEGORYOPTIONS_OPTION_NAME')%>'  ", "|||", "", $opt_selected,"mco_mst_options_id")%>
                                        <a class="tipR fancybox-hash-iframe" style="text-decoration: none;" target="_blank" href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('options/options/add')%>|mode|<%$mod_enc_mode['Add']%>|hideCtrl|true|rfMod|<%$this->general->getAdminEncodeURL('categoryoptions')%>|rfFod|<%$this->general->getAdminEncodeURL('categoryoptions')%>|rfField|mco_mst_options_id|rfhtmlID|mco_mst_options_id" title=" <%$this->lang->line('GENERIC_ADD_NEW_RECORD')%> ">
                                            <span class="icon16 minia-icon-file-add"></span>
                                        </a>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mco_mst_options_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mco_filter">
                                    <label class="form-label span3">
                                        <%$this->lang->line('CATEGORYOPTIONS_FILTER')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mco_filter']%>
                                        <%$this->dropdown->display("mco_filter","mco_filter","  title='<%$this->lang->line('CATEGORYOPTIONS_FILTER')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'CATEGORYOPTIONS_FILTER')%>'  ", "|||", "", $opt_selected,"mco_filter")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mco_filterErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mco_order_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('CATEGORYOPTIONS_SORT_ORDER')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mco_order_id']|@htmlentities%>" name="mco_order_id" id="mco_order_id" title="<%$this->lang->line('CATEGORYOPTIONS_SORT_ORDER')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mco_order_idErr'></label></div>
                                </div>
                                <%assign var="child_module_name" value="optionvaluemaster"%>
                                <div class="box form-child-table" id="child_module_optionvaluemaster">
                                    <%assign var="child_data" value=$child_assoc_data[$child_module_name]%>
                                    <%assign var="child_conf_arr" value=$child_assoc_conf[$child_module_name]%>
                                    <%assign var="child_opt_arr" value=$child_assoc_opt[$child_module_name]%>
                                    <%assign var="child_img_html" value=$child_assoc_img[$child_module_name]%>
                                    <%assign var="child_auto_arr" value=$child_assoc_auto[$child_module_name]%>
                                    <%if $child_conf_arr["recMode"] eq "Update"%>
                                        <%assign var="child_cnt" value=$child_data|@count%>
                                        <%assign var="recMode" value="Update"%>
                                    <%else%>
                                        <%assign var="child_cnt" value="1"%>
                                        <%assign var="recMode" value="Add"%>
                                    <%/if%>
                                    <%assign var="popup" value=$child_conf_arr["popup"]%>
                                    <div class="title">
                                        <input type="hidden" name="childModule[]" id="childModule_<%$child_module_name%>" value="<%$child_module_name%>" />
                                        <input type="hidden" name="childModuleParField[<%$child_module_name%>]" id="childModuleParField_<%$child_module_name%>" value="iMstCategoryOptionsId"/>
                                        <input type="hidden" name="childModuleParData[<%$child_module_name%>]" id="childModuleParData_<%$child_module_name%>" value="<%$this->general->getAdminEncodeURL($data['iMstCategoryOptionsId'])%>"/>
                                        <input type="hidden" name="childModuleType[<%$child_module_name%>]" id="childModuleType_<%$child_module_name%>" value="Table"/>
                                        <input type="hidden" name="childModuleLayout[<%$child_module_name%>]" id="childModuleLayout_<%$child_module_name%>" value="Column"/>
                                        <input type="hidden" name="childModuleCnt[<%$child_module_name%>]" id="childModuleCnt_<%$child_module_name%>" value="<%$child_cnt%>" />
                                        <input type="hidden" name="childModuleInc[<%$child_module_name%>]" id="childModuleInc_<%$child_module_name%>" value="<%$child_cnt%>" />
                                        <input type="hidden" name="childModulePopup[<%$child_module_name%>]" id="childModulePopup_<%$child_module_name%>" value="<%$popup%>" />
                                        <input type="hidden" name="childModuleUploadURL[<%$child_module_name%>]" id="childModuleUploadURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['upload_form_file']%>" />
                                        <input type="hidden" name="childModuleChosenURL[<%$child_module_name%>]" id="childModuleChosenURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['get_chosen_auto_complete']%>" />
                                        <input type="hidden" name="childModuleParentURL[<%$child_module_name%>]" id="childModuleParentURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['parent_source_options']%>" />
                                        <input type="hidden" name="childModuleTokenURL[<%$child_module_name%>]" id="childModuleTokenURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['get_token_auto_complete']%>" />
                                        <input type="hidden" name="childModuleShowHide[<%$child_module_name%>]" id="childModuleShowHide_<%$child_module_name%>" value="Yes" />
                                        <h4>
                                            <span class="icon12 icomoon-icon-equalizer-2"></span><span><%$this->lang->line('CATEGORYOPTIONS_OPTION_VALUE_MASTER')%></span>
                                            <span style="display:none;margin-left:32%" id="ajax_loader_childModule_<%$child_module_name%>"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
                                            <div class="box-addmore right">
                                                <a class="btn btn-success" href="javascript://" onclick="getChildModuleAjaxTable('<%$mod_enc_url.child_data_add%>','<%$child_module_name%>', '<%$mode%>')" title="<%$this->lang->line('GENERIC_ADD_NEW')%>">
                                                    <span class="icon12 icomoon-icon-cog-2"></span><strong><%$this->lang->line('GENERIC_ADD_NEW')%></strong>
                                                </a>
                                            </div>
                                        </h4>
                                        <a href="javascript://" class="minimize" style="display: none;"></a>
                                    </div>
                                    <div class="content noPad form-table-row" style="display: block;">
                                        <table class="responsive table table-bordered" id="tbl_child_module_<%$child_module_name%>">
                                            <thead>
                                                <tr>
                                                    <th width="3%">#</th>
                                                    <th width="90%"><%$this->lang->line('OPTIONVALUEMASTER_VALUE')%>  <em>*</em></th>
                                                    <th width="7%"><div align="center">Actions</div></th>
                                                </tr>
                                            </thead>
                                            <tbody id="add_child_module_<%$child_module_name%>">
                                                <tr class="ch-mod-firstrow">
                                                    <td width="3%"></td> 
                                                    <td width="90%"></td> <td width="7%"></td>
                                                </tr>
                                                <%section name=i loop=$child_cnt%>
                                                    <%assign var="row_index" value=$smarty.section.i.index%>
                                                    <%assign var="row_number" value=$smarty.section.i.iteration%>
                                                    <%assign var="child_id" value=$child_data[i]['iMstCategoryOptionValuesId']%>
                                                    <%assign var="enc_child_id" value=$this->general->getAdminEncodeURL($child_id)%>
                                                    <tr id="tr_child_row_<%$child_module_name%>_<%$row_index%>">
                                                        <td class="row-num-child">
                                                            <span class="row-num-span"><%$row_number%></span>
                                                            <input type="hidden" name="child[optionvaluemaster][id][<%$row_index%>]" id="child_optionvaluemaster_id_<%$row_index%>" value="<%$child_id%>" />
                                                            <input type="hidden" name="child[optionvaluemaster][enc_id][<%$row_index%>]" id="child_optionvaluemaster_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
                                                            <input type="hidden" name="child[optionvaluemaster][mcov_mst_category_options_id][<%$row_index%>]" id="child_optionvaluemaster_mcov_mst_category_options_id_<%$row_index%>" value="<%$child_data[i]['mcov_mst_category_options_id']%>"  class='ignore-valid ' />
                                                            <input type="hidden" name="child[optionvaluemaster][sys_custom_field_1][<%$row_index%>]" id="child_optionvaluemaster_sys_custom_field_1_<%$row_index%>" value="<%$child_data[i]['sys_custom_field_1']|@htmlentities%>"  class='ignore-valid ' />
                                                        </td>
                                                        <td>
                                                            <div class=" " id="ch_optionvaluemaster_cc_sh_mcov_value_<%$row_index%>">
                                                                <input type="text" placeholder="" value="<%$child_data[i]['mcov_value']%>" name="child[optionvaluemaster][mcov_value][<%$row_index%>]" id="child_optionvaluemaster_mcov_value_<%$row_index%>" title="<%$this->lang->line('OPTIONVALUEMASTER_VALUE')%>"  class='frm-size-medium'  />  
                                                            </div>
                                                            <div>
                                                                <label class='error' id='child_optionvaluemaster_mcov_value_<%$row_index%>Err'></label>
                                                            </div>
                                                        </td>
                                                        <td align="center">
                                                            <div class="controls center">
                                                                <%if $mode eq "Update"%>
                                                                    <a onclick="saveChildModuleSingleData('<%$mod_enc_url.child_data_save%>', '<%$mod_enc_url.child_data_add%>', '<%$child_module_name%>', '<%$recMode%>', '<%$row_index%>', '<%$enc_child_id%>', 'No', '<%$mode%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_SAVE')%>">
                                                                        <span class="icon14 icomoon-icon-disk"></span>
                                                                    </a>
                                                                <%/if%>
                                                                <%if $recMode eq "Update"%>
                                                                    <a onclick="deleteChildModuleSingleData('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>','<%$enc_child_id%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                                                                        <span class="icon16 icomoon-icon-remove"></span>
                                                                    </a>
                                                                <%else%>
                                                                    <a onclick="deleteChildModuleRow('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                                                                        <span class="icon16 icomoon-icon-remove"></span>
                                                                    </a>
                                                                <%/if%>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <%javascript%>
                                                    <%if $child_auto_arr[$row_index]|@is_array && $child_auto_arr[$row_index]|@count gt 0%>
                                                        var $child_chosen_auto_complete_url = admin_url+""+$("#childModuleChosenURL_<%$child_module_name%>").val()+"?";
                                                        <%foreach name=i from=$child_auto_arr[$row_index] item=v key=k%>
                                                            if($("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").is("select")){
                                                            $("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").ajaxChosen({
                                                            dataType: "json",
                                                            type: "POST",
                                                            url: $child_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$recMode]%>&id=<%$enc_child_id%>"
                                                            },{
                                                            loadingImg: admin_image_url+"chosen-loading.gif"
                                                            });
                                                        }
                                                    <%/foreach%>
                                                <%/if%>
                                                <%/javascript%>
                                            <%/section%>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%>">
                    <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                        <%assign var='rm_ctrl_directions' value=true%>
                    <%/if%>
                    <!-- Form Redirection Control Unit -->
                    <%if $controls_allow eq false || $rm_ctrl_directions eq true%>
                        <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" type="hidden" />
                    <%else%>
                        <div class='action-dir-align'>
                            <%if $prev_link_allow eq true%>
                                <input value="Prev" id="ctrl_flow_prev" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Prev' %> checked=true <%/if%> />
                                <label for="ctrl_flow_prev">&nbsp;</label>
                                <label for="ctrl_flow_prev" class="inline-elem-margin"><%$this->lang->line('GENERIC_PREV_SHORT')%></label>&nbsp;&nbsp;
                            <%/if%>
                            <%if $next_link_allow eq true || $mode eq 'Add'%>
                                <input value="Next" id="ctrl_flow_next" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Next' %> checked=true <%/if%> />
                                <label for="ctrl_flow_next">&nbsp;</label>
                                <label for="ctrl_flow_next" class="inline-elem-margin"><%$this->lang->line('GENERIC_NEXT_SHORT')%></label>&nbsp;&nbsp;
                            <%/if%>
                            <input value="List" id="ctrl_flow_list" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'List' %> checked=true <%/if%> />
                            <label for="ctrl_flow_list">&nbsp;</label>
                            <label for="ctrl_flow_list" class="inline-elem-margin"><%$this->lang->line('GENERIC_LIST_SHORT')%></label>&nbsp;&nbsp;
                            <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq '' || $ctrl_flow eq 'Stay' %> checked=true <%/if%> />
                            <label for="ctrl_flow_stay">&nbsp;</label>
                            <label for="ctrl_flow_stay" class="inline-elem-margin"><%$this->lang->line('GENERIC_STAY_SHORT')%></label>
                        </div>
                    <%/if%>
                    <!-- Form Action Control Unit -->
                    <%if $controls_allow eq false%>
                        <div class="clear">&nbsp;</div>
                    <%/if%>
                    <div class="action-btn-align" id="action_btn_container">
                        <%if $mode eq 'Update'%>
                            <%if $update_allow eq true%>
                                <input value="<%$this->lang->line('GENERIC_UPDATE')%>" name="ctrlupdate" type="submit" id="frmbtn_update" class="btn btn-info"/>&nbsp;&nbsp;
                            <%/if%>
                            <%if $delete_allow eq true%>
                                <input value="<%$this->lang->line('GENERIC_DELETE')%>" name="ctrldelete" type="button" id="frmbtn_delete" class="btn btn-danger" onclick="return deleteAdminRecordData('<%$enc_id%>', '<%$mod_enc_url.index%>','<%$mod_enc_url.inline_edit_action%>', '<%$extra_qstr%>', '<%$extra_hstr%>');" />&nbsp;&nbsp;
                            <%/if%>
                        <%else%>
                            <input value="<%$this->lang->line('GENERIC_SAVE')%>" name="ctrladd" type="submit" id="frmbtn_add" class="btn btn-info" />&nbsp;&nbsp;
                        <%/if%>
                        <%if $discard_allow eq true%>
                            <input value="<%$this->lang->line('GENERIC_DISCARD')%>" name="ctrldiscard" type="button" id="frmbtn_discard" class="btn" onclick="return loadAdminModuleListing('<%$mod_enc_url.index%>', '<%$extra_hstr%>')" />
                        <%/if%>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </form>
    </div>
</div>
</div>
<%javascript%>    
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {
        "optionvaluemaster": {
            "mcov_mst_category_options_id": {
                "type": "dropdown",
                "vUniqueName": "mcov_mst_category_options_id",
                "editrules": {
                    "infoArr": []
                }
            },
            "mcov_value": {
                "type": "textbox",
                "vUniqueName": "mcov_value",
                "editrules": {
                    "required": true,
                    "infoArr": {
                        "required": {
                            "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.OPTIONVALUEMASTER_VALUE)
                        }
                    }
                }
            },
            "sys_custom_field_1": {
                "type": "textbox",
                "vUniqueName": "sys_custom_field_1",
                "editrules": {
                    "infoArr": []
                }
            }
        }
    };        
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        <%foreach name=i from=$auto_arr item=v key=k%>
            if($("#<%$k%>").is("select")){
                $("#<%$k%>").ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                    },{
                    loadingImg: admin_image_url+"chosen-loading.gif"
                });
            }
        <%/foreach%>
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['buttons_arr'] = [];
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/categoryoptions_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.categoryoptions.callEvents();
<%/javascript%>