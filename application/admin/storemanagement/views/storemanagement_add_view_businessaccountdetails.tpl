<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="2"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="businessaccountdetails"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="businessaccountdetails" />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_NAME')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_store_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_company_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_COMPANY_NAME')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_company_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_contact_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_CONTACT_NAME')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_contact_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_contact_number">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_CONTACT_NUMBER')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->getPhoneMaskedView($this->general->getAdminPHPFormats('phone'),$data['msd_contact_number'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_address1">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ADDRESS1')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_address1']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_address2">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ADDRESS2')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_address2']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_area">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_AREA')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_area']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_country_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_COUNTRY')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_country_id'], $opt_arr['msd_country_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_state_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STATE')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_state_id'], $opt_arr['msd_state_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_city_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_CITY')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_city_id'], $opt_arr['msd_city_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_pin_code">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_PIN_CODE')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_pin_code']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_website_url">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_COMPANY_WEBSITE_URL')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_website_url']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_mst_categories_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_PRIMARY_CATEGORY')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_mst_categories_id'], $opt_arr['msd_mst_categories_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_have_online_store">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_DO_YOU_SELL_PRODUCT_ONLINE_ALREADY_C63')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
            <strong><%$this->general->displayKeyValueData($data['msd_have_online_store'], $opt_arr['msd_have_online_store'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_url">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_URL')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_store_url']%></strong>
        </div>
    </div>
</div>