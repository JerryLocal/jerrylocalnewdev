<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="tab"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="tab" />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_admin_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ADMIN_ID')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_admin_id'], $opt_arr['msd_admin_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_NAME')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_store_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_company_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_COMPANY_NAME')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_company_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_contact_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_CONTACT_NAME')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_contact_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_state_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STATE')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_state_id'], $opt_arr['msd_state_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_city_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_CITY')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_city_id'], $opt_arr['msd_city_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_area">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_AREA')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_area']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_address2">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ADDRESS2')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_address2']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_address1">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ADDRESS1')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_address1']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_contact_number">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_CONTACT_NUMBER')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->getPhoneMaskedView($this->general->getAdminPHPFormats('phone'),$data['msd_contact_number'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_country_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_COUNTRY')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_country_id'], $opt_arr['msd_country_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_pin_code">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_PIN_CODE')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_pin_code']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_website_url">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_WEBSITE_URL')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_website_url']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_logo">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_LOGO')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <%$img_html['msd_store_logo']%>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_description">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_DESCRIPTION')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_store_description']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof_detail">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_OTHER_PROOF_DETAIL')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_other_proof_detail']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_PROOF')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_bank_account_proof']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_g_s_t_i_r_d_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_G_S_T_I_R_D_PROOF')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_g_s_t_i_r_d_proof']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_g_s_t_no">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_G_S_T_NO')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_g_s_t_no']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_no">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NO')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_bank_account_no']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NAME')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_bank_account_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_url">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_URL')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_store_url']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_have_online_store">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_HAVE_ONLINE_STORE')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_have_online_store'], $opt_arr['msd_have_online_store'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_mst_categories_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_MST_CATEGORIES_ID')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_mst_categories_id'], $opt_arr['msd_mst_categories_id'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_OTHER_PROOF')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_other_proof']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_rate_avg">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_RATE_AVG')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_store_rate_avg']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_total_review">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_TOTAL_REVIEW')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_store_total_review'], $opt_arr['msd_store_total_review'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_accept_term">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ACCEPT_TERM')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_accept_term'], $opt_arr['msd_accept_term'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_status">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STATUS')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$this->general->displayKeyValueData($data['msd_status'], $opt_arr['msd_status'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_date">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_DATE')%>
        </label> 
        <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
            <strong><%$this->general->dateDefinedFormat('Y-m-d',$data['msd_date'])%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_modify_date">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_MODIFY_DATE')%>
        </label> 
        <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
            <strong><%$this->general->dateDefinedFormat('Y-m-d',$data['msd_modify_date'])%></strong>
        </div>
    </div>
</div>