<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('STOREMANAGEMENT_STORE_MANAGEMENT')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','STOREMANAGEMENT_STORE_MANAGEMENT')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display: none;position:inherit;" id="ajax_lang_loader">
            <img src="<%$this->config->item('admin_images_url')%>loaders/circular/020.gif">
        </span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="storemanagement" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="storemanagement" class="frm-edit-block frm-custm-view">
            <div class="main-content-block" id="main_content_block">
                <div>
                    <div style="width:98%" class="frm-block-layout frm-resize-block pad-calc-container">
                        <div class="box gradient resize-box custom-view-box <%$rl_theme_arr['frm_custm_content_row']%> <%$rl_theme_arr['frm_custm_border_view']%>" id="blocktab_1_1">
                            <div class="title"></div>
                            <div class="content resize-content one-column-block <%$rl_theme_arr['frm_custm_label_align']%>">
                                <ul id="formtoptabs_1_1" class="nav nav-tabs show-hide-tab">
                                    <li id="headinglist_1_1_1" class="<%if $mode eq "Add"%>active<%else%>active<%/if%>">
                                        <a id="tabanchor_1_1_1" class="anchor" href="javascript://"  aria-curr="sellerregistration" aria-prev="" aria-next="businessaccountdetails" isdone="<%if $mode eq "Update"%>1<%else%>0<%/if%>">
                                            <%$this->lang->line('STOREMANAGEMENT_SELLER_REGISTRATION')%>
                                        </a>
                                    </li>
                                    <li id="headinglist_1_1_2" class="<%if $mode eq "Add"%>active disabled<%else%><%/if%>">
                                        <a id="tabanchor_1_1_2" class="anchor" href="javascript://"  aria-curr="businessaccountdetails" aria-prev="sellerregistration" aria-next="bankdetails" isdone="<%if $mode eq "Update"%>1<%else%>0<%/if%>">
                                            <%$this->lang->line('STOREMANAGEMENT_BUSINESS_ACCOUNT_DETAILS')%>
                                        </a>
                                    </li>
                                    <li id="headinglist_1_1_3" class="<%if $mode eq "Add"%>active disabled<%else%><%/if%>">
                                        <a id="tabanchor_1_1_3" class="anchor" href="javascript://"  aria-curr="bankdetails" aria-prev="businessaccountdetails" aria-next="" isdone="<%if $mode eq "Update"%>1<%else%>0<%/if%>">
                                            <%$this->lang->line('STOREMANAGEMENT_BANK_DETAILS')%>
                                        </a>
                                    </li>
                                </ul>
                                <div id="tabcontent_1_1" class="content-animate">
                                    <form name="frmaddupdate_1_1" id="frmaddupdate_1_1" action="<%$admin_url%><%$mod_enc_url['save_tab_wise_block']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                                        <div id="tabheading_1_1_1" class="tab-fade active tab-focus-child" style="display:block;">
                                            <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                                            <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                                            <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                                            <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records["prev"]["id"]%>" />
                                            <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records["next"]["id"]%>" />
                                            <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                                            <input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
                                            <input type="hidden" name="load_tab" id="load_tab_1_1" value="sellerregistration"/>
                                            <input type="hidden" name="tab_code" id="tab_code_1_1" value="sellerregistration" />
                                            <input type="hidden" name="msd_admin_id" id="msd_admin_id" value="<%$data['msd_admin_id']|@htmlentities%>"  class='ignore-valid ' />
                                            <input type="hidden" name="msd_store_rate_avg" id="msd_store_rate_avg" value="<%$data['msd_store_rate_avg']|@htmlentities%>"  class='ignore-valid ' />
                                            <input type="hidden" name="msd_store_total_review" id="msd_store_total_review" value="<%$data['msd_store_total_review']|@htmlentities%>"  class='ignore-valid ' />
                                            <input type="hidden" name="msd_status" id="msd_status" value="<%$data['msd_status']|@htmlentities%>"  class='ignore-valid ' />
                                            <input type="hidden" name="msd_date" id="msd_date" value="<%$data['msd_date']%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
                                            <input type="hidden" name="msd_modify_date" id="msd_modify_date" value="<%$data['msd_modify_date']%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
                                            <input type="hidden" name="ma1_name" id="ma1_name" value="<%$data['ma1_name']|@htmlentities%>"  class='ignore-valid ' />
                                            <input type="hidden" name="ma1_user_name" id="ma1_user_name" value="<%$data['ma1_user_name']|@htmlentities%>"  class='ignore-valid ' />
                                            <input type="hidden" name="ma1_phonenumber" id="ma1_phonenumber" value="<%$data['ma1_phonenumber']|@htmlentities%>"  class='ignore-valid ' />
                                            <input type="hidden" name="ma1_group_id" id="ma1_group_id" value="<%$data['ma1_group_id']|@htmlentities%>"  class='ignore-valid ' />
                                            <input type="hidden" name="ma1_last_access" id="ma1_last_access" value="<%$data['ma1_last_access']|@htmlentities%>"  class='ignore-valid ' />
                                            <input type="hidden" name="ma1_status" id="ma1_status" value="<%$data['ma1_status']|@htmlentities%>"  class='ignore-valid ' />
                                            <div class="column-view-parent form-row row-fluid tab-focus-element" >
                                                <div class="one-block-view" id="cc_sh_msd_store_code">
                                                    <label class="form-label span3"><%$this->lang->line('STOREMANAGEMENT_STORE_CODE')%></label>
                                                    <div class="form-right-div   ">
                                                        <input type="text" placeholder="" value="<%$data['msd_store_code']|@htmlentities%>" name="msd_store_code" id="msd_store_code" title="<%$this->lang->line('STOREMANAGEMENT_STORE_CODE')%>"  />
                                                    </div>
                                                    <div class="error-msg-form "><label class='error' id='msd_store_codeErr'></label></div>
                                                </div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid tab-focus-element" >
                                                <div class="one-block-view" id="cc_sh_msd_store_logo">
                                                    <label class="form-label span3"><%$this->lang->line('STOREMANAGEMENT_STORE_LOGO')%> <em>*</em>
                                                </label>
                                                <div class="form-right-div   ">
                                                    <div  class='btn-uploadify frm-size-medium' >
                                                        <input type="hidden" value="<%$data['msd_store_logo']%>" name="old_msd_store_logo" id="old_msd_store_logo" />
                                                        <input type="hidden" value="<%$data['msd_store_logo']%>" name="msd_store_logo" id="msd_store_logo"  />
                                                        <input type="hidden" value="<%$data['msd_store_logo']%>" name="temp_msd_store_logo" id="temp_msd_store_logo"  />
                                                        <div id="upload_drop_zone_msd_store_logo" class="upload-drop-zone"></div>
                                                        <div class="uploader upload-src-zone">
                                                            <input type="file" name="uploadify_msd_store_logo" id="uploadify_msd_store_logo" title="<%$this->lang->line('STOREMANAGEMENT_STORE_LOGO')%>" />
                                                            <span class="filename" id="preview_msd_store_logo"></span>
                                                            <span class="action">Choose File</span>
                                                        </div>
                                                    </div>
                                                    <div class='upload-image-btn'>
                                                        <%$img_html['msd_store_logo']%>
                                                    </div>
                                                    <span class="input-comment">
                                                        <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
                                                    </span>
                                                    <div class='clear upload-progress' id='progress_msd_store_logo'>
                                                        <div class='upload-progress-bar progress progress-striped active'>
                                                            <div class='bar' id='practive_msd_store_logo'></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='msd_store_logoErr'></label></div>
                                            </div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-element" >
                                            <div class="one-block-view" id="cc_sh_msd_store_description">
                                                <label class="form-label span3"><%$this->lang->line('STOREMANAGEMENT_STORE_DESCRIPTION')%></label>
                                                <div class="form-right-div   ">
                                                    <textarea placeholder=""  name="msd_store_description" id="msd_store_description" title="<%$this->lang->line('STOREMANAGEMENT_STORE_DESCRIPTION')%>"  class='elastic frm-size-medium'  ><%$data['msd_store_description']%></textarea>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='msd_store_descriptionErr'></label></div>
                                            </div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-element" >
                                            <div class="one-block-view" id="cc_sh_ma1_email">
                                                <label class="form-label span3"><%$this->lang->line('STOREMANAGEMENT_EMAIL')%></label>
                                                <div class="form-right-div   ">
                                                    <input type="text" placeholder="" value="<%$data['ma1_email']|@htmlentities%>" name="ma1_email" id="ma1_email" title="<%$this->lang->line('STOREMANAGEMENT_EMAIL')%>"  class='frm-size-medium'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma1_emailErr'></label></div>
                                            </div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-element" >
                                            <div class="one-block-view" id="cc_sh_ma1_password">
                                                <label class="form-label span3"><%$this->lang->line('STOREMANAGEMENT_PASSWORD')%></label>
                                                <div class="form-right-div   ">
                                                    <input placeholder="" autocomplete="off" type="password" value="<%$data['ma1_password']%>" name="ma1_password" id="ma1_password" title="<%$this->lang->line('STOREMANAGEMENT_PASSWORD')%>"  class='frm-size-medium'  />
                                                    <a href="javascript://" id="a_password_ma1_password" class="tipR pwd-show-icon" onclick="adminShowHidePasswordField('ma1_password');" title="<%$this->lang->line('GENERIC_CLICK_THIS_TO_SHOW_PASSWORD')%>"><span id="span_password_ma1_password" class="icon16 iconic-icon-lock-fill" ></span></a>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma1_passwordErr'></label></div>
                                            </div>
                                            <%if $mode eq "Add"%>
                                                <div class="one-block-view clear form-row row-fluid">
                                                    <label class="form-label span3">
                                                        Re-type <%$this->lang->line('STOREMANAGEMENT_PASSWORD')%> <em>*</em>
                                                    </label> 
                                                    <div class="form-right-div">
                                                        <input autocomplete="off" type="password" value="<%$data['ma1_password']%>" name="retypema1_password" id="retypema1_password" title="<%$this->lang->line('STOREMANAGEMENT_PASSWORD')%>"  class='frm-size-medium'  />
                                                    </div>
                                                    <div class="error-msg-form">
                                                        <label class='error' id='retypema1_passwordErr'></label>
                                                    </div>
                                                </div>
                                            <%/if%>
                                        </div>
                                        <div align="right" class="btn-below-spacing">
                                            <div style="float:right;">
                                                <input type="submit" value="Save" name="ctrlsave_1_1_1" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','1','sellerregistration')"/>
                                                &nbsp;&nbsp;
                                                <input type="submit" value="Save &amp; Next" name="ctrlsavenext_1_1_1" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','2','businessaccountdetails')" />
                                                &nbsp;&nbsp;
                                                <input type="button" value="Next" name="ctrlnext_1_1_1" class="btn" onclick="return getNextAdminTab('1','1','1','2','businessaccountdetails')" />
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div id="tabheading_1_1_2" class="tab-fade inactive tab-focus-child" style="display:block;"></div>
                                </form>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>            
<%javascript%>    
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};        
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        <%foreach name=i from=$auto_arr item=v key=k%>
            if($("#<%$k%>").is("select")){
                $("#<%$k%>").ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                    },{
                    loadingImg: admin_image_url+"chosen-loading.gif"
                });
            }
        <%/foreach%>
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_action_url'] = admin_url+'<%$mod_enc_url["save_tab_wise_block"]%>?<%$extra_qstr%>';
    el_form_settings['buttons_arr'] = [];
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/storemanagement_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.storemanagement.callEvents();
<%/javascript%>