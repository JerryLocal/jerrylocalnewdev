<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="3"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="bankdetails"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="bankdetails" />
<input type="hidden" name="msd_bank_account_no" id="msd_bank_account_no" value="<%$data['msd_bank_account_no']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_g_s_t_no" id="msd_g_s_t_no" value="<%$data['msd_g_s_t_no']|@htmlentities%>"  class='ignore-valid ' />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NAME__C40NZ_ONLY_C41')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_bank_account_name']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_sys_custom_field_39">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NO')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['sys_custom_field_39']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_sys_custom_field_43">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_G_S_T_NO_C451')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['sys_custom_field_43']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_g_s_t_i_r_d_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_UPLOAD_GST_C47IRD_DETAIL_PROOF')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <%$img_html['msd_g_s_t_i_r_d_proof']%>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_UPLOAD_BANK_ACCOUNT_PROOF')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <%$img_html['msd_bank_account_proof']%>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_UPLOAD_ANY_OTHER_PROOF')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <%$img_html['msd_other_proof']%>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof_detail">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_OTHER_PROOF_DETAIL')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_other_proof_detail']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_accept_term">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ACCEPT_TERM')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
            <strong><%$this->general->displayKeyValueData(explode(",",$data['msd_accept_term']), $opt_arr['msd_accept_term'])%></strong>
        </div>
    </div>
</div>