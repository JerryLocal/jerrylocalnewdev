<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('STOREMANAGEMENT_STORE_MANAGEMENT')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','STOREMANAGEMENT_STORE_MANAGEMENT')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display: none;position:inherit;" id="ajax_lang_loader">
            <img src="<%$this->config->item('admin_images_url')%>loaders/circular/020.gif">
        </span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="storemanagement" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="storemanagement" class="frm-view-block frm-custm-view">
            <div class="main-content-block" id="main_content_block">
                <div>
                    <div style="width:98%" class="frm-block-layout frm-resize-block pad-calc-container">
                        <div class="box gradient resize-box custom-view-box <%$rl_theme_arr['frm_custm_content_row']%> <%$rl_theme_arr['frm_custm_border_view']%>" id="blocktab_1_1">
                            <div class="title"></div>
                            <div class="content resize-content  <%$rl_theme_arr['frm_custm_label_align']%>">
                                <ul id="formtoptabs_1_1" class="nav nav-tabs show-hide-tab">
                                    <li id="headinglist_1_1_1" class="<%if $mode eq "Add"%>active<%else%>active<%/if%>">
                                        <a id="tabanchor_1_1_1" class="anchor" href="javascript://"  aria-curr="sellerregistration" aria-prev="" aria-next="businessaccountdetails" isdone="<%if $mode eq "Update"%>1<%else%>0<%/if%>">
                                            <%$this->lang->line('STOREMANAGEMENT_SELLER_REGISTRATION')%>
                                        </a>
                                    </li>
                                    <li id="headinglist_1_1_2" class="<%if $mode eq "Add"%>active disabled<%else%><%/if%>">
                                        <a id="tabanchor_1_1_2" class="anchor" href="javascript://"  aria-curr="businessaccountdetails" aria-prev="sellerregistration" aria-next="bankdetails" isdone="<%if $mode eq "Update"%>1<%else%>0<%/if%>">
                                            <%$this->lang->line('STOREMANAGEMENT_BUSINESS_ACCOUNT_DETAILS')%>
                                        </a>
                                    </li>
                                    <li id="headinglist_1_1_3" class="<%if $mode eq "Add"%>active disabled<%else%><%/if%>">
                                        <a id="tabanchor_1_1_3" class="anchor" href="javascript://"  aria-curr="bankdetails" aria-prev="businessaccountdetails" aria-next="" isdone="<%if $mode eq "Update"%>1<%else%>0<%/if%>">
                                            <%$this->lang->line('STOREMANAGEMENT_BANK_DETAILS')%>
                                        </a>
                                    </li>
                                </ul>
                                <div id="tabcontent_1_1" class="content-animate">
                                    <div id="tabheading_1_1_1" class="tab-fade active " style="display:block;">
                                        <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                                        <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                                        <input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
                                        <input type="hidden" name="load_tab" id="load_tab_1_1" value="sellerregistration"/>
                                        <input type="hidden" name="tab_code" id="tab_code_1_1" value="sellerregistration" />
                                        <input type="hidden" name="msd_admin_id" id="msd_admin_id" value="<%$data['msd_admin_id']|@htmlentities%>"  class='ignore-valid ' />
                                        <input type="hidden" name="msd_store_rate_avg" id="msd_store_rate_avg" value="<%$data['msd_store_rate_avg']|@htmlentities%>"  class='ignore-valid ' />
                                        <input type="hidden" name="msd_store_total_review" id="msd_store_total_review" value="<%$data['msd_store_total_review']|@htmlentities%>"  class='ignore-valid ' />
                                        <input type="hidden" name="msd_status" id="msd_status" value="<%$data['msd_status']|@htmlentities%>"  class='ignore-valid ' />
                                        <input type="hidden" name="msd_date" id="msd_date" value="<%$data['msd_date']%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
                                        <input type="hidden" name="msd_modify_date" id="msd_modify_date" value="<%$data['msd_modify_date']%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
                                        <input type="hidden" name="ma1_name" id="ma1_name" value="<%$data['ma1_name']|@htmlentities%>"  class='ignore-valid ' />
                                        <input type="hidden" name="ma1_user_name" id="ma1_user_name" value="<%$data['ma1_user_name']|@htmlentities%>"  class='ignore-valid ' />
                                        <input type="hidden" name="ma1_phonenumber" id="ma1_phonenumber" value="<%$data['ma1_phonenumber']|@htmlentities%>"  class='ignore-valid ' />
                                        <input type="hidden" name="ma1_group_id" id="ma1_group_id" value="<%$data['ma1_group_id']|@htmlentities%>"  class='ignore-valid ' />
                                        <input type="hidden" name="ma1_last_access" id="ma1_last_access" value="<%$data['ma1_last_access']|@htmlentities%>"  class='ignore-valid ' />
                                        <input type="hidden" name="ma1_status" id="ma1_status" value="<%$data['ma1_status']|@htmlentities%>"  class='ignore-valid ' />
                                        <div class="column-view-parent form-row row-fluid tab-focus-element" >
                                            <div class="one-block-view" id="cc_sh_msd_store_code">
                                                <label class="form-label span3"><%$this->lang->line('STOREMANAGEMENT_STORE_CODE')%></label>
                                                <div class="form-right-div frm-elements-div  ">
                                                    <strong><%$data['msd_store_code']%></strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-element" >
                                            <div class="one-block-view" id="cc_sh_msd_store_logo">
                                                <label class="form-label span3"><%$this->lang->line('STOREMANAGEMENT_STORE_LOGO')%> <em>*</em>
                                            </label>
                                            <div class="form-right-div frm-elements-div  ">
                                                <%$img_html['msd_store_logo']%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid tab-focus-element" >
                                        <div class="one-block-view" id="cc_sh_msd_store_description">
                                            <label class="form-label span3"><%$this->lang->line('STOREMANAGEMENT_STORE_DESCRIPTION')%></label>
                                            <div class="form-right-div frm-elements-div  ">
                                                <strong><%$data['msd_store_description']%></strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid tab-focus-element" >
                                        <div class="one-block-view" id="cc_sh_ma1_email">
                                            <label class="form-label span3"><%$this->lang->line('STOREMANAGEMENT_EMAIL')%></label>
                                            <div class="form-right-div frm-elements-div  ">
                                                <strong><%$data['ma1_email']%></strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid tab-focus-element" >
                                        <div class="one-block-view" id="cc_sh_ma1_password">
                                            <label class="form-label span3"><%$this->lang->line('STOREMANAGEMENT_PASSWORD')%></label>
                                            <div class="form-right-div frm-elements-div  ">
                                                *****
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tabheading_1_1_2" class="tab-fade inactive " style="display:block;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>        
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
    callSwitchToSelf();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
