<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="sellerregistration"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="sellerregistration" />
<input type="hidden" name="msd_admin_id" id="msd_admin_id" value="<%$data['msd_admin_id']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_store_rate_avg" id="msd_store_rate_avg" value="<%$data['msd_store_rate_avg']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_store_total_review" id="msd_store_total_review" value="<%$data['msd_store_total_review']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_status" id="msd_status" value="<%$data['msd_status']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_date" id="msd_date" value="<%$data['msd_date']%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
<input type="hidden" name="msd_modify_date" id="msd_modify_date" value="<%$data['msd_modify_date']%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
<input type="hidden" name="ma1_name" id="ma1_name" value="<%$data['ma1_name']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma1_user_name" id="ma1_user_name" value="<%$data['ma1_user_name']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma1_phonenumber" id="ma1_phonenumber" value="<%$data['ma1_phonenumber']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma1_group_id" id="ma1_group_id" value="<%$data['ma1_group_id']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma1_last_access" id="ma1_last_access" value="<%$data['ma1_last_access']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma1_status" id="ma1_status" value="<%$data['ma1_status']|@htmlentities%>"  class='ignore-valid ' />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_code">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_CODE')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_store_code']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_logo">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_LOGO')%> <em>*</em> 
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <%$img_html['msd_store_logo']%>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_description">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_DESCRIPTION')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['msd_store_description']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_ma1_email">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_EMAIL')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            <strong><%$data['ma1_email']%></strong>
        </div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_ma1_password">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_PASSWORD')%>
        </label> 
        <div class="form-right-div frm-elements-div  ">
            *****
        </div>
    </div>
</div>