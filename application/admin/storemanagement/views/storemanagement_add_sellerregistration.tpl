<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="sellerregistration"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="sellerregistration" />
<input type="hidden" name="msd_admin_id" id="msd_admin_id" value="<%$data['msd_admin_id']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_store_rate_avg" id="msd_store_rate_avg" value="<%$data['msd_store_rate_avg']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_store_total_review" id="msd_store_total_review" value="<%$data['msd_store_total_review']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_status" id="msd_status" value="<%$data['msd_status']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_date" id="msd_date" value="<%$data['msd_date']%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
<input type="hidden" name="msd_modify_date" id="msd_modify_date" value="<%$data['msd_modify_date']%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
<input type="hidden" name="ma1_name" id="ma1_name" value="<%$data['ma1_name']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma1_user_name" id="ma1_user_name" value="<%$data['ma1_user_name']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma1_phonenumber" id="ma1_phonenumber" value="<%$data['ma1_phonenumber']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma1_group_id" id="ma1_group_id" value="<%$data['ma1_group_id']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma1_last_access" id="ma1_last_access" value="<%$data['ma1_last_access']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma1_status" id="ma1_status" value="<%$data['ma1_status']|@htmlentities%>"  class='ignore-valid ' />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_code">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_CODE')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_store_code']|@htmlentities%>" name="msd_store_code" id="msd_store_code" title="<%$this->lang->line('STOREMANAGEMENT_STORE_CODE')%>"  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_codeErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_logo">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_LOGO')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <div  class='btn-uploadify frm-size-medium' >
                <input type="hidden" value="<%$data['msd_store_logo']%>" name="old_msd_store_logo" id="old_msd_store_logo" />
                <input type="hidden" value="<%$data['msd_store_logo']%>" name="msd_store_logo" id="msd_store_logo"  />
                <input type="hidden" value="<%$data['msd_store_logo']%>" name="temp_msd_store_logo" id="temp_msd_store_logo"  />
                <div id="upload_drop_zone_msd_store_logo" class="upload-drop-zone"></div>
                <div class="uploader upload-src-zone">
                    <input type="file" name="uploadify_msd_store_logo" id="uploadify_msd_store_logo" title="<%$this->lang->line('STOREMANAGEMENT_STORE_LOGO')%>" />
                    <span class="filename" id="preview_msd_store_logo"></span>
                    <span class="action">Choose File</span>
                </div>
            </div>
            <div class='upload-image-btn'>
                <%$img_html['msd_store_logo']%>
            </div>
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
            </span>
            <div class='clear upload-progress' id='progress_msd_store_logo'>
                <div class='upload-progress-bar progress progress-striped active'>
                    <div class='bar' id='practive_msd_store_logo'></div>
                </div>
            </div>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_logoErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_description">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_DESCRIPTION')%>
        </label> 
        <div class="form-right-div   ">
            <textarea placeholder=""  name="msd_store_description" id="msd_store_description" title="<%$this->lang->line('STOREMANAGEMENT_STORE_DESCRIPTION')%>"  class='elastic frm-size-medium'  ><%$data['msd_store_description']%></textarea>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_descriptionErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_ma1_email">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_EMAIL')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['ma1_email']|@htmlentities%>" name="ma1_email" id="ma1_email" title="<%$this->lang->line('STOREMANAGEMENT_EMAIL')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='ma1_emailErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_ma1_password">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_PASSWORD')%>
        </label> 
        <div class="form-right-div   ">
            <input placeholder="" autocomplete="off" type="password" value="<%$data['ma1_password']%>" name="ma1_password" id="ma1_password" title="<%$this->lang->line('STOREMANAGEMENT_PASSWORD')%>"  class='frm-size-medium'  />
            <a href="javascript://" id="a_password_ma1_password" class="tipR pwd-show-icon" onclick="adminShowHidePasswordField('ma1_password');" title="<%$this->lang->line('GENERIC_CLICK_THIS_TO_SHOW_PASSWORD')%>"><span id="span_password_ma1_password" class="icon16 iconic-icon-lock-fill" ></span></a>
        </div>
        <div class="error-msg-form "><label class='error' id='ma1_passwordErr'></label></div>
    </div>
    <%if $mode eq "Add"%>
        <div class="one-block-view clear form-row row-fluid">
            <label class="form-label span3">
                Re-type <%$this->lang->line('STOREMANAGEMENT_PASSWORD')%> <em>*</em>
            </label> 
            <div class="form-right-div">
                <input autocomplete="off" type="password" value="<%$data['ma1_password']%>" name="retypema1_password" id="retypema1_password" title="<%$this->lang->line('STOREMANAGEMENT_PASSWORD')%>"  class='frm-size-medium'  />
            </div>
            <div class="error-msg-form">
                <label class='error' id='retypema1_passwordErr'></label>
            </div>
        </div>
    <%/if%>
</div>
<div align="right" class="btn-below-spacing">
    <div style="float:right;">
        <input type="submit" value="Save" name="ctrlsave_1_1_1" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','1','sellerregistration')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Next" name="ctrlsavenext_1_1_1" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','2','businessaccountdetails')" />
        &nbsp;&nbsp;
        <input type="button" value="Next" name="ctrlnext_1_1_1" class="btn" onclick="return getNextAdminTab('1','1','1','2','businessaccountdetails')" />
    </div>
    <div class="clear"></div>
</div>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initAdminTabRenderJSScript(eleObj){
Project.modules.storemanagement.initEvents("sellerregistration");
callGoogleMapEvents();
}
<%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
<%foreach name=i from=$auto_arr item=v key=k%>
    if($("#<%$k%>").is("select")){
    $("#<%$k%>").ajaxChosen({
    dataType: "json",
    type: "POST",
    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
    },{
    loadingImg: admin_image_url+"chosen-loading.gif"
    });
}
<%/foreach%>
<%/if%>        
<%/javascript%>