<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="bankdetails"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="bankdetails" />
<input type="hidden" name="msd_bank_account_no" id="msd_bank_account_no" value="<%$data['msd_bank_account_no']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_g_s_t_no" id="msd_g_s_t_no" value="<%$data['msd_g_s_t_no']|@htmlentities%>"  class='ignore-valid ' />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NAME__C40NZ_ONLY_C41')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_bank_account_name']|@htmlentities%>" name="msd_bank_account_name" id="msd_bank_account_name" title="<%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NAME__C40NZ_ONLY_C41')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_bank_account_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_sys_custom_field_39">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NO')%> <em>*</em> 
        </label> 
        <div class="form-right-div   frm-controls-merge ">
            <div class="frm-merge-ctrl-block frm-merge-ctrl-20" style='width:20%!important;'><input type="text" placeholder="" value="<%$data['sys_custom_field_39']|@htmlentities%>" name="sys_custom_field_39" id="sys_custom_field_39" title="<%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NO')%>"  class='frm-size-medium'  /></div><div class="frm-merge-ctrl-block frm-merge-ctrl-20" style='width:20%!important;'><input type="text" placeholder="" value="<%$data['sys_custom_field_40']|@htmlentities%>" name="sys_custom_field_40" id="sys_custom_field_40" title="<%$this->lang->line('STOREMANAGEMENT_BANK_AC_C452')%>"  class='frm-size-medium'  /></div><div class="frm-merge-ctrl-block frm-merge-ctrl-20" style='width:20%!important;'><input type="text" placeholder="" value="<%$data['sys_custom_field_41']|@htmlentities%>" name="sys_custom_field_41" id="sys_custom_field_41" title="<%$this->lang->line('STOREMANAGEMENT_BANK_AC_C453')%>"  class='frm-size-medium'  /></div><div class="frm-merge-ctrl-block frm-merge-ctrl-20" style='width:20%!important;'><input type="text" placeholder="" value="<%$data['sys_custom_field_42']|@htmlentities%>" name="sys_custom_field_42" id="sys_custom_field_42" title="<%$this->lang->line('STOREMANAGEMENT_BANK_AC_C454')%>"  class='frm-size-medium'  /></div>
        </div>
        <div class="error-msg-form  frm-errormsg-merge"><div class="frm-merge-eror-block frm-merge-ctrl-20" style='width:20%!important;'><label class='error' id='sys_custom_field_39Err'></label></div><div class="frm-merge-eror-block frm-merge-ctrl-20" style='width:20%!important;'><label class='error' id='sys_custom_field_40Err'></label></div><div class="frm-merge-eror-block frm-merge-ctrl-20" style='width:20%!important;'><label class='error' id='sys_custom_field_41Err'></label></div><div class="frm-merge-eror-block frm-merge-ctrl-20" style='width:20%!important;'><label class='error' id='sys_custom_field_42Err'></label></div></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_sys_custom_field_43">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_G_S_T_NO_C451')%> <em>*</em> 
        </label> 
        <div class="form-right-div   frm-controls-merge ">
            <div class="frm-merge-ctrl-block frm-merge-ctrl-20" style='width:20%!important;'><input type="text" placeholder="" value="<%$data['sys_custom_field_43']|@htmlentities%>" name="sys_custom_field_43" id="sys_custom_field_43" title="<%$this->lang->line('STOREMANAGEMENT_G_S_T_NO_C451')%>"  class='frm-size-medium'  /></div><div class="frm-merge-ctrl-block frm-merge-ctrl-20" style='width:20%!important;'><input type="text" placeholder="" value="<%$data['sys_custom_field_44']|@htmlentities%>" name="sys_custom_field_44" id="sys_custom_field_44" title="<%$this->lang->line('STOREMANAGEMENT_G_S_T_NO_C452')%>"  class='frm-size-medium'  /></div><div class="frm-merge-ctrl-block frm-merge-ctrl-20" style='width:20%!important;'><input type="text" placeholder="" value="<%$data['sys_custom_field_45']|@htmlentities%>" name="sys_custom_field_45" id="sys_custom_field_45" title="<%$this->lang->line('STOREMANAGEMENT_G_S_T_NO_C453')%>"  class='frm-size-medium'  /></div>
        </div>
        <div class="error-msg-form  frm-errormsg-merge"><div class="frm-merge-eror-block frm-merge-ctrl-20" style='width:20%!important;'><label class='error' id='sys_custom_field_43Err'></label></div><div class="frm-merge-eror-block frm-merge-ctrl-20" style='width:20%!important;'><label class='error' id='sys_custom_field_44Err'></label></div><div class="frm-merge-eror-block frm-merge-ctrl-20" style='width:20%!important;'><label class='error' id='sys_custom_field_45Err'></label></div></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_g_s_t_i_r_d_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_UPLOAD_GST_C47IRD_DETAIL_PROOF')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <div  class='btn-uploadify frm-size-medium' >
                <input type="hidden" value="<%$data['msd_g_s_t_i_r_d_proof']%>" name="old_msd_g_s_t_i_r_d_proof" id="old_msd_g_s_t_i_r_d_proof" />
                <input type="hidden" value="<%$data['msd_g_s_t_i_r_d_proof']%>" name="msd_g_s_t_i_r_d_proof" id="msd_g_s_t_i_r_d_proof"  />
                <input type="hidden" value="<%$data['msd_g_s_t_i_r_d_proof']%>" name="temp_msd_g_s_t_i_r_d_proof" id="temp_msd_g_s_t_i_r_d_proof"  />
                <div id="upload_drop_zone_msd_g_s_t_i_r_d_proof" class="upload-drop-zone"></div>
                <div class="uploader upload-src-zone">
                    <input type="file" name="uploadify_msd_g_s_t_i_r_d_proof" id="uploadify_msd_g_s_t_i_r_d_proof" title="<%$this->lang->line('STOREMANAGEMENT_UPLOAD_GST_C47IRD_DETAIL_PROOF')%>" />
                    <span class="filename" id="preview_msd_g_s_t_i_r_d_proof"></span>
                    <span class="action">Choose File</span>
                </div>
            </div>
            <div class='upload-image-btn'>
                <%$img_html['msd_g_s_t_i_r_d_proof']%>
            </div>
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
            </span>
            <div class='clear upload-progress' id='progress_msd_g_s_t_i_r_d_proof'>
                <div class='upload-progress-bar progress progress-striped active'>
                    <div class='bar' id='practive_msd_g_s_t_i_r_d_proof'></div>
                </div>
            </div>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_g_s_t_i_r_d_proofErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_UPLOAD_BANK_ACCOUNT_PROOF')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <div  class='btn-uploadify frm-size-medium' >
                <input type="hidden" value="<%$data['msd_bank_account_proof']%>" name="old_msd_bank_account_proof" id="old_msd_bank_account_proof" />
                <input type="hidden" value="<%$data['msd_bank_account_proof']%>" name="msd_bank_account_proof" id="msd_bank_account_proof"  />
                <input type="hidden" value="<%$data['msd_bank_account_proof']%>" name="temp_msd_bank_account_proof" id="temp_msd_bank_account_proof"  />
                <div id="upload_drop_zone_msd_bank_account_proof" class="upload-drop-zone"></div>
                <div class="uploader upload-src-zone">
                    <input type="file" name="uploadify_msd_bank_account_proof" id="uploadify_msd_bank_account_proof" title="<%$this->lang->line('STOREMANAGEMENT_UPLOAD_BANK_ACCOUNT_PROOF')%>" />
                    <span class="filename" id="preview_msd_bank_account_proof"></span>
                    <span class="action">Choose File</span>
                </div>
            </div>
            <div class='upload-image-btn'>
                <%$img_html['msd_bank_account_proof']%>
            </div>
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
            </span>
            <div class='clear upload-progress' id='progress_msd_bank_account_proof'>
                <div class='upload-progress-bar progress progress-striped active'>
                    <div class='bar' id='practive_msd_bank_account_proof'></div>
                </div>
            </div>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_bank_account_proofErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_UPLOAD_ANY_OTHER_PROOF')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <div  class='btn-uploadify frm-size-medium' >
                <input type="hidden" value="<%$data['msd_other_proof']%>" name="old_msd_other_proof" id="old_msd_other_proof" />
                <input type="hidden" value="<%$data['msd_other_proof']%>" name="msd_other_proof" id="msd_other_proof"  />
                <input type="hidden" value="<%$data['msd_other_proof']%>" name="temp_msd_other_proof" id="temp_msd_other_proof"  />
                <div id="upload_drop_zone_msd_other_proof" class="upload-drop-zone"></div>
                <div class="uploader upload-src-zone">
                    <input type="file" name="uploadify_msd_other_proof" id="uploadify_msd_other_proof" title="<%$this->lang->line('STOREMANAGEMENT_UPLOAD_ANY_OTHER_PROOF')%>" />
                    <span class="filename" id="preview_msd_other_proof"></span>
                    <span class="action">Choose File</span>
                </div>
            </div>
            <div class='upload-image-btn'>
                <%$img_html['msd_other_proof']%>
            </div>
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
            </span>
            <div class='clear upload-progress' id='progress_msd_other_proof'>
                <div class='upload-progress-bar progress progress-striped active'>
                    <div class='bar' id='practive_msd_other_proof'></div>
                </div>
            </div>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_other_proofErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof_detail">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_OTHER_PROOF_DETAIL')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_other_proof_detail']|@htmlentities%>" name="msd_other_proof_detail" id="msd_other_proof_detail" title="<%$this->lang->line('STOREMANAGEMENT_OTHER_PROOF_DETAIL')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_other_proof_detailErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_accept_term">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ACCEPT_TERM')%> <em>*</em> 
        </label> 
        <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%>  ">
            <%assign var="opt_selected" value=","|@explode:$data['msd_accept_term']%>
            <%assign var="combo_arr" value=$opt_arr["msd_accept_term"]%>
            <%if $combo_arr|@is_array && $combo_arr|@count gt 0 %>
                <%foreach name=i from=$combo_arr item=v key=k%>
                    <input type="checkbox" value="<%$k%>" name="msd_accept_term[]" id="msd_accept_term_<%$k%>" title="<%$v%>" <%if $opt_selected|@is_array && $k|@in_array:$opt_selected %> checked=true <%/if%>  class='regular-checkbox'  />
                    <label for="msd_accept_term_<%$k%>" class="frm-horizon-row frm-column-layout">&nbsp;</label>
                    <label for="msd_accept_term_<%$k%>" class="frm-horizon-row frm-column-layout"><%$v%></label>&nbsp;&nbsp;
                <%/foreach%>
            <%/if%>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_accept_termErr'></label></div>
    </div>
</div>
<div align="right" class="btn-below-spacing">
    <div style="float:left;">
        <input type="button" value="Prev" name="ctrlprev_1_1_3" class="btn" onclick="return getLoadAdminTab('1','1','2','businessaccountdetails')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Prev" name="ctrlsaveprev_1_1_3" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','2','businessaccountdetails')"/>
    </div>
    <div style="float:right;">
        <input type="submit" value="Save" name="ctrlsave_1_1_3" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','3','bankdetails')"/>
        &nbsp;&nbsp;
    </div>
    <div class="clear"></div>
</div>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initAdminTabRenderJSScript(eleObj){
Project.modules.storemanagement.initEvents("bankdetails");
callGoogleMapEvents();
}
<%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
<%foreach name=i from=$auto_arr item=v key=k%>
    if($("#<%$k%>").is("select")){
    $("#<%$k%>").ajaxChosen({
    dataType: "json",
    type: "POST",
    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
    },{
    loadingImg: admin_image_url+"chosen-loading.gif"
    });
}
<%/foreach%>
<%/if%>        
<%/javascript%>