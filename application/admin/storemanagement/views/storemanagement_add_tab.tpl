<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="tab"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="tab" />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_admin_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ADMIN_ID')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_admin_id']%>
            <%$this->dropdown->display("msd_admin_id","msd_admin_id","  title='<%$this->lang->line('STOREMANAGEMENT_ADMIN_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'STOREMANAGEMENT_ADMIN_ID')%>'  ", "|||", "", $opt_selected,"msd_admin_id")%>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_admin_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_NAME')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_store_name']|@htmlentities%>" name="msd_store_name" id="msd_store_name" title="<%$this->lang->line('STOREMANAGEMENT_STORE_NAME')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_company_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_COMPANY_NAME')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_company_name']|@htmlentities%>" name="msd_company_name" id="msd_company_name" title="<%$this->lang->line('STOREMANAGEMENT_COMPANY_NAME')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_company_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_contact_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_CONTACT_NAME')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_contact_name']|@htmlentities%>" name="msd_contact_name" id="msd_contact_name" title="<%$this->lang->line('STOREMANAGEMENT_CONTACT_NAME')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_contact_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_state_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STATE')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_state_id']%>
            <%$this->dropdown->display("msd_state_id","msd_state_id","  title='<%$this->lang->line('STOREMANAGEMENT_STATE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'STOREMANAGEMENT_STATE')%>'  ", "|||", "", $opt_selected,"msd_state_id")%><span style='display:none;margin-left:15%' id='ajax_loader_msd_state_id'><img src='<%$this->config->item("admin_images_url")%>loaders/circular/020.gif' /></span>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_state_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_city_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_CITY')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_city_id']%>
            <%$this->dropdown->display("msd_city_id","msd_city_id","  title='<%$this->lang->line('STOREMANAGEMENT_CITY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'STOREMANAGEMENT_CITY')%>'  ", "|||", "", $opt_selected,"msd_city_id")%><span style='display:none;margin-left:15%' id='ajax_loader_msd_city_id'><img src='<%$this->config->item("admin_images_url")%>loaders/circular/020.gif' /></span>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_city_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_area">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_AREA')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_area']|@htmlentities%>" name="msd_area" id="msd_area" title="<%$this->lang->line('STOREMANAGEMENT_AREA')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_areaErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_address2">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ADDRESS2')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_address2']|@htmlentities%>" name="msd_address2" id="msd_address2" title="<%$this->lang->line('STOREMANAGEMENT_ADDRESS2')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_address2Err'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_address1">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ADDRESS1')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_address1']|@htmlentities%>" name="msd_address1" id="msd_address1" title="<%$this->lang->line('STOREMANAGEMENT_ADDRESS1')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_address1Err'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_contact_number">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_CONTACT_NUMBER')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <input type="text" format="<%$this->general->getAdminPHPFormats('phone')%>" value="<%$data['msd_contact_number']%>" name="msd_contact_number" id="msd_contact_number" title="<%$this->lang->line('STOREMANAGEMENT_CONTACT_NUMBER')%>"  class='frm-phone-number frm-size-medium'  style='width:auto;' />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_contact_numberErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_country_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_COUNTRY')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_country_id']%>
            <%$this->dropdown->display("msd_country_id","msd_country_id","  title='<%$this->lang->line('STOREMANAGEMENT_COUNTRY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'STOREMANAGEMENT_COUNTRY')%>'  ", "|||", "", $opt_selected,"msd_country_id")%>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_country_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_pin_code">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_PIN_CODE')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_pin_code']|@htmlentities%>" name="msd_pin_code" id="msd_pin_code" title="<%$this->lang->line('STOREMANAGEMENT_PIN_CODE')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_pin_codeErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_website_url">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_WEBSITE_URL')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_website_url']|@htmlentities%>" name="msd_website_url" id="msd_website_url" title="<%$this->lang->line('STOREMANAGEMENT_WEBSITE_URL')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_website_urlErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_logo">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_LOGO')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <div  class='btn-uploadify frm-size-medium' >
                <input type="hidden" value="<%$data['msd_store_logo']%>" name="old_msd_store_logo" id="old_msd_store_logo" />
                <input type="hidden" value="<%$data['msd_store_logo']%>" name="msd_store_logo" id="msd_store_logo"  />
                <input type="hidden" value="<%$data['msd_store_logo']%>" name="temp_msd_store_logo" id="temp_msd_store_logo"  />
                <div id="upload_drop_zone_msd_store_logo" class="upload-drop-zone"></div>
                <div class="uploader upload-src-zone">
                    <input type="file" name="uploadify_msd_store_logo" id="uploadify_msd_store_logo" title="<%$this->lang->line('STOREMANAGEMENT_STORE_LOGO')%>" />
                    <span class="filename" id="preview_msd_store_logo"></span>
                    <span class="action">Choose File</span>
                </div>
            </div>
            <div class='upload-image-btn'>
                <%$img_html['msd_store_logo']%>
            </div>
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
            </span>
            <div class='clear upload-progress' id='progress_msd_store_logo'>
                <div class='upload-progress-bar progress progress-striped active'>
                    <div class='bar' id='practive_msd_store_logo'></div>
                </div>
            </div>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_logoErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_description">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_DESCRIPTION')%>
        </label> 
        <div class="form-right-div   ">
            <textarea placeholder=""  name="msd_store_description" id="msd_store_description" title="<%$this->lang->line('STOREMANAGEMENT_STORE_DESCRIPTION')%>"  class='elastic frm-size-medium'  ><%$data['msd_store_description']%></textarea>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_descriptionErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof_detail">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_OTHER_PROOF_DETAIL')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_other_proof_detail']|@htmlentities%>" name="msd_other_proof_detail" id="msd_other_proof_detail" title="<%$this->lang->line('STOREMANAGEMENT_OTHER_PROOF_DETAIL')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_other_proof_detailErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_PROOF')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_bank_account_proof']|@htmlentities%>" name="msd_bank_account_proof" id="msd_bank_account_proof" title="<%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_PROOF')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_bank_account_proofErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_g_s_t_i_r_d_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_G_S_T_I_R_D_PROOF')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_g_s_t_i_r_d_proof']|@htmlentities%>" name="msd_g_s_t_i_r_d_proof" id="msd_g_s_t_i_r_d_proof" title="<%$this->lang->line('STOREMANAGEMENT_G_S_T_I_R_D_PROOF')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_g_s_t_i_r_d_proofErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_g_s_t_no">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_G_S_T_NO')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_g_s_t_no']|@htmlentities%>" name="msd_g_s_t_no" id="msd_g_s_t_no" title="<%$this->lang->line('STOREMANAGEMENT_G_S_T_NO')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_g_s_t_noErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_no">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NO')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_bank_account_no']|@htmlentities%>" name="msd_bank_account_no" id="msd_bank_account_no" title="<%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NO')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_bank_account_noErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_name">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NAME')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_bank_account_name']|@htmlentities%>" name="msd_bank_account_name" id="msd_bank_account_name" title="<%$this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NAME')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_bank_account_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_url">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_URL')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_store_url']|@htmlentities%>" name="msd_store_url" id="msd_store_url" title="<%$this->lang->line('STOREMANAGEMENT_STORE_URL')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_urlErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_have_online_store">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_HAVE_ONLINE_STORE')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_have_online_store']%>
            <%$this->dropdown->display("msd_have_online_store","msd_have_online_store","  title='<%$this->lang->line('STOREMANAGEMENT_HAVE_ONLINE_STORE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'STOREMANAGEMENT_HAVE_ONLINE_STORE')%>'  ", "|||", "", $opt_selected,"msd_have_online_store")%>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_have_online_storeErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_mst_categories_id">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_MST_CATEGORIES_ID')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_mst_categories_id']%>
            <%$this->dropdown->display("msd_mst_categories_id","msd_mst_categories_id","  title='<%$this->lang->line('STOREMANAGEMENT_MST_CATEGORIES_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'STOREMANAGEMENT_MST_CATEGORIES_ID')%>'  ", "|||", "", $opt_selected,"msd_mst_categories_id")%>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_mst_categories_idErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_OTHER_PROOF')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_other_proof']|@htmlentities%>" name="msd_other_proof" id="msd_other_proof" title="<%$this->lang->line('STOREMANAGEMENT_OTHER_PROOF')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_other_proofErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_rate_avg">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_RATE_AVG')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_store_rate_avg']|@htmlentities%>" name="msd_store_rate_avg" id="msd_store_rate_avg" title="<%$this->lang->line('STOREMANAGEMENT_STORE_RATE_AVG')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_rate_avgErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_total_review">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STORE_TOTAL_REVIEW')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_store_total_review']%>
            <%$this->dropdown->display("msd_store_total_review","msd_store_total_review","  title='<%$this->lang->line('STOREMANAGEMENT_STORE_TOTAL_REVIEW')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'STOREMANAGEMENT_STORE_TOTAL_REVIEW')%>'  ", "|||", "", $opt_selected,"msd_store_total_review")%>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_total_reviewErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_accept_term">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_ACCEPT_TERM')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_accept_term']%>
            <%$this->dropdown->display("msd_accept_term","msd_accept_term","  title='<%$this->lang->line('STOREMANAGEMENT_ACCEPT_TERM')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'STOREMANAGEMENT_ACCEPT_TERM')%>'  ", "|||", "", $opt_selected,"msd_accept_term")%>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_accept_termErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_status">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_STATUS')%>
        </label> 
        <div class="form-right-div   ">
            <%assign var="opt_selected" value=$data['msd_status']%>
            <%$this->dropdown->display("msd_status","msd_status","  title='<%$this->lang->line('STOREMANAGEMENT_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'STOREMANAGEMENT_STATUS')%>'  ", "|||", "", $opt_selected,"msd_status")%>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_statusErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_date">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_DATE')%>
        </label> 
        <div class="form-right-div  input-append text-append-prepend   ">
            <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['msd_date'])%>" placeholder="" name="msd_date" id="msd_date" title="<%$this->lang->line('STOREMANAGEMENT_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
            <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_dateErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_modify_date">
        <label class="form-label span3">
            <%$this->lang->line('STOREMANAGEMENT_MODIFY_DATE')%>
        </label> 
        <div class="form-right-div  input-append text-append-prepend   ">
            <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['msd_modify_date'])%>" placeholder="" name="msd_modify_date" id="msd_modify_date" title="<%$this->lang->line('STOREMANAGEMENT_MODIFY_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
            <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_modify_dateErr'></label></div>
    </div>
</div>
<div align="right" class="btn-below-spacing">
    <div style="float:right;">
        <input type="submit" value="Save" name="ctrlsave_1_1_1" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','1','tab')"/>
        &nbsp;&nbsp;
    </div>
    <div class="clear"></div>
</div>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initAdminTabRenderJSScript(eleObj){
Project.modules.storemanagement.initEvents("tab");
callGoogleMapEvents();
}
<%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
<%foreach name=i from=$auto_arr item=v key=k%>
    if($("#<%$k%>").is("select")){
    $("#<%$k%>").ajaxChosen({
    dataType: "json",
    type: "POST",
    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
    },{
    loadingImg: admin_image_url+"chosen-loading.gif"
    });
}
<%/foreach%>
<%/if%>        
<%/javascript%>