<?php
/**
 * Description of Store Management Controller
 *
 * @module Store Management
 *
 * @class storemanagement.php
 *
 * @path application\admin\storemanagement\controllers\storemanagement.php
 *
 * @author Steve Smith
 *
 * @date 05.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Storemanagement extends HB_Controller {
    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->load->model('model_storemanagement');
        $this->_request_params();
        $this->folder_name = "storemanagement";
        $this->module_name = "storemanagement";
        $this->mod_enc_url = $this->general->getGeneralEncryptList($this->folder_name, $this->module_name);
        $this->mod_enc_mode = $this->general->getCustomEncryptMode(TRUE);
        $this->module_config = array(
            'module_name' => $this->module_name,
            'folder_name' => $this->folder_name,
            'mod_enc_url' => $this->mod_enc_url,
            'mod_enc_mode' => $this->mod_enc_mode,
            'delete' => "No",
            'xeditable' => "No",
            'top_detail' => "No",
            "multi_lingual" => "No",
            "physical_data_remove" => "Yes",
            "workflow_modes" => array()
        );
        $this->dropdown_arr = array(
            "msd_country_id" => array(
                "type" => "table",
                "table_name" => "mod_country",
                "field_key" => "iCountryId",
                "field_val" => array(
                    $this->db->protect("vCountry")
                ),
                "order_by" => "val asc",
                "default" => "Yes",
            ),
            "msd_state_id" => array(
                "type" => "table",
                "table_name" => "mod_state",
                "field_key" => "iStateId",
                "field_val" => array(
                    $this->db->protect("vState")
                ),
                "order_by" => "val asc",
                "default" => "Yes",
                "parent_src" => "Yes",
                "source_field" => "msd_country_id",
                "target_field" => "iCountryId",
            ),
            "msd_city_id" => array(
                "type" => "table",
                "table_name" => "mod_city",
                "field_key" => "iCityId",
                "field_val" => array(
                    $this->db->protect("vCity")
                ),
                "order_by" => "val asc",
                "default" => "Yes",
                "parent_src" => "Yes",
                "source_field" => "msd_state_id",
                "target_field" => "iStateId",
            ),
            "msd_mst_categories_id" => array(
                "type" => "table",
                "table_name" => "mst_categories",
                "field_key" => "iMstCategoriesId",
                "field_val" => array(
                    $this->db->protect("vTitle")
                ),
                "order_by" => "val asc",
                "default" => "Yes",
            ),
            "msd_have_online_store" => array(
                "type" => "enum",
                "default" => "No",
                "values" => array(
                    array(
                        'id' => 'Yes',
                        'val' => $this->lang->line('STOREMANAGEMENT_YES')
                    ),
                    array(
                        'id' => 'No',
                        'val' => $this->lang->line('STOREMANAGEMENT_NO')
                    )
                )
            ),
            "msd_accept_term" => array(
                "type" => "enum",
                "default" => "No",
                "values" => array(
                    array(
                        'id' => 'Accept Terms and Conditions',
                        'val' => $this->lang->line('STOREMANAGEMENT_ACCEPTED')
                    )
                )
            )
        );
        $this->parMod = $this->params_arr["parMod"];
        $this->parID = $this->params_arr["parID"];
        $this->parRefer = array();
        $this->expRefer = array();

        $this->topRefer = array();
        $this->dropdown_limit = $this->config->item('ADMIN_DROPDOWN_LIMIT');
        $this->search_combo_limit = $this->config->item('ADMIN_SEARCH_COMBO_LIMIT');
        $this->switchto_limit = $this->config->item('ADMIN_SWITCH_DROPDOWN_LIMIT');
        $this->count_arr = array();
    }

    /**
     * _request_params method is used to set post/get/request params.
     */
    public function _request_params() {
        $this->get_arr = is_array($this->input->get(NULL, TRUE)) ? $this->input->get(NULL, TRUE) : array();
        $this->post_arr = is_array($this->input->post(NULL, TRUE)) ? $this->input->post(NULL, TRUE) : array();
        $this->params_arr = array_merge($this->get_arr, $this->post_arr);
        return $this->params_arr;
    }

    /**
     * index method is used to intialize grid listing page.
     */
    public function index() {
        $params_arr = $this->params_arr;
        $extra_qstr = $extra_hstr = '';
        list($list_access, $view_access, $add_access, $edit_access, $del_access, $expo_access) = $this->filter->getModuleWiseAccess("storemanagement", array("List", "View", "Add", "Update", "Delete", "Export"), TRUE, TRUE);
        try {
            if (!$list_access) {
                throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_VIEW_THIS_PAGE_C46_C46_C33'));
            }
            $enc_loc_module = $this->general->getMD5EncryptString("ListPrefer", "storemanagement");

            $status_array = $status_values = array();
            $status_values = $this->dropdown_arr["msd_have_online_store"]["values"];
            $status_values_count = count($status_values);
            for ($i = 0; $i < $status_values_count; $i++) {
                $status_array[$i] = $status_values[$i]["id"];
            }

            $status_array_label = $this->general->getEnumValuesLabels($this->module_name, $status_array);
            $list_config = $this->model_storemanagement->getListConfiguration();
            $this->processConfiguration($list_config, $add_access, $edit_access, TRUE);
            $this->general->trackModuleNavigation("Module", "List", "Viewed", $this->mod_enc_url["index"], "storemanagement");

            $extra_qstr .= $this->general->getRequestURLParams();
            $extra_hstr .= $this->general->getRequestHASHParams();
            $render_arr = array(

                'list_config' => $list_config,
                'count_arr' => $this->count_arr,
                'enc_loc_module' => $enc_loc_module,
                'status_array' => $status_array,
                'status_array_label' => $status_array_label,
                'view_access' => $view_access,
                'add_access' => $add_access,
                'edit_access' => $edit_access,
                'del_access' => $del_access,
                'expo_access' => $expo_access,
                'folder_name' => $this->folder_name,
                'module_name' => $this->module_name,
                'mod_enc_url' => $this->mod_enc_url,
                'mod_enc_mode' => $this->mod_enc_mode,
                'extra_qstr' => $extra_qstr,
                'extra_hstr' => $extra_hstr,
                'default_filters' => $this->model_storemanagement->default_filters,
            );
            $this->smarty->assign($render_arr);
            $this->loadView("storemanagement_index");
        } catch(Exception $e) {
            $render_arr['err_message'] = $e->getMessage();
            $this->smarty->assign($render_arr);
            $this->loadView($this->config->item('ADMIN_FORBIDDEN_TEMPLATE'));
        }
    }

    /**
     * listing method is used to load listing data records in json format.
     */
    public function listing() {
        $params_arr = $this->params_arr;
        $page = $params_arr['page'];
        $rows = $params_arr['rows'];
        $sidx = $params_arr['sidx'];
        $sord = $params_arr['sord'];
        $sdef = $params_arr['sdef'];
        $filters = $params_arr['filters'];
        if (!trim($sidx) && !trim($sord)) {
            $sdef = 'Yes';
        }
        if ($this->general->allowStripSlashes()) {
            $filters = stripslashes($filters);
        }
        $filters = json_decode($filters, TRUE);
        $list_config = $this->model_storemanagement->getListConfiguration();
        $form_config = $this->model_storemanagement->getFormConfiguration();
        $extra_cond = $this->model_storemanagement->extra_cond;
        $groupby_cond = $this->model_storemanagement->groupby_cond;
        $having_cond = $this->model_storemanagement->having_cond;
        $orderby_cond = $this->model_storemanagement->orderby_cond;

        $data_config = array();
        $data_config['page'] = $page;
        $data_config['rows'] = $rows;
        $data_config['sidx'] = $sidx;
        $data_config['sord'] = $sord;
        $data_config['sdef'] = $sdef;
        $data_config['filters'] = $filters;
        $data_config['module_config'] = $this->module_config;
        $data_config['list_config'] = $list_config;
        $data_config['form_config'] = $form_config;
        $data_config['dropdown_arr'] = $this->dropdown_arr;
        $data_config['extra_cond'] = $extra_cond;
        $data_config['group_by'] = $groupby_cond;
        $data_config['having_cond'] = $having_cond;
        $data_config['order_by'] = $orderby_cond;
        $data_recs = $this->model_storemanagement->getListingData($data_config);
        $data_recs['no_records_msg'] = $this->general->processMessageLabel('');

        echo json_encode($data_recs);
        $this->skip_template_view();
    }

    /**
     * export method is used to export listing data records in csv or pdf formats.
     */
    public function export() {
        $this->filter->getModuleWiseAccess("storemanagement", "Export", TRUE);
        $params_arr = $this->params_arr;
        $page = $params_arr['page'];
        $rowlimit = $params_arr['rowlimit'];
        $sidx = $params_arr['sidx'];
        $sord = $params_arr['sord'];
        $sdef = $params_arr['sdef'];
        if (!trim($sidx) && !trim($sord)) {
            $sdef = 'Yes';
        }
        $export_type = $params_arr['export_type'];
        $export_mode = $params_arr['export_mode'];
        $filters = $params_arr['filters'];
        if ($this->general->allowStripSlashes()) {
            $filters = stripslashes($filters);
        }
        $filters = json_decode(base64_decode($filters), TRUE);
        $fields = json_decode(base64_decode($params_arr['fields']), TRUE);
        $list_config = $this->model_storemanagement->getListConfiguration();
        $form_config = $this->model_storemanagement->getFormConfiguration();
        $table_name = $this->model_storemanagement->table_name;
        $table_alias = $this->model_storemanagementtable_alias;
        $primary_key = $this->model_storemanagement->primary_key;
        $extra_cond = $this->model_storemanagement->extra_cond;
        $groupby_cond = $this->model_storemanagement->groupby_cond;
        $having_cond = $this->model_storemanagement->having_cond;
        $orderby_cond = $this->model_storemanagement->orderby_cond;

        $export_config = array();
        $export_config['page'] = $page;
        $export_config['rowlimit'] = $rowlimit;
        $export_config['sidx'] = $sidx;
        $export_config['sord'] = $sord;
        $export_config['sdef'] = $sdef;
        $export_config['filters'] = $filters;
        $export_config['export_mode'] = $export_mode;
        $export_config['module_config'] = $this->module_config;
        $export_config['list_config'] = $list_config;
        $export_config['form_config'] = $form_config;
        $export_config['dropdown_arr'] = $this->dropdown_arr;
        $export_config['table_name'] = $table_name;
        $export_config['table_alias'] = $table_alias;
        $export_config['primary_key'] = $primary_key;
        $export_config['extra_cond'] = $extra_cond;
        $export_config['group_by'] = $groupby_cond;
        $export_config['having_cond'] = $having_cond;
        $export_config['order_by'] = $orderby_cond;
        $db_recs = $this->model_storemanagement->getExportData($export_config);
        $db_recs = $this->listing->getDataForList($db_recs, $export_config, "GExport", array());
        if (!is_array($db_recs) || count($db_recs) == 0) {
            $this->session->set_flashdata('failure', $this->lang->line('GENERIC_GRID_NO_RECORDS_TO_PROCESS'));
            redirect($_SERVER['HTTP_REFERER']);
        }

        $filename = "Store_Management_".count($db_recs)."_Records";
        $heading = "Store Management";
        if ($export_mode == "all" && is_array($tot_fields_arr)) {
            if (($pr_key = array_search($primary_key, $tot_fields_arr)) !== FALSE) {
                unset($tot_fields_arr[$pr_key]);
            }
            $fields = array_values($tot_fields_arr);
        }
        $numberOfColumns = count($fields);
        if ($export_type == 'pdf') {
            if (file_exists(APPPATH.'third_party/Pdf_export.php')) {
                $pdf_style = "TCPDF";
            } else {
                $pdf_style = "FPDF";
                $eachColumnWidth = 40;
            }
            $columns = $aligns = $widths = $data = array();
            //Table headers info
            for ($i = 0; $i < $numberOfColumns; $i++) {
                $size = 10;
                $position = '';
                if (array_key_exists($fields[$i], $list_config)) {
                    $label = $list_config[$fields[$i]]['label_lang'];
                    $position = $list_config[$fields[$i]]['align'];
                    $size = $list_config[$fields[$i]]['width'];
                } elseif (array_key_exists($fields[$i], $form_config)) {
                    $label = $form_config[$fields[$i]]['label_lang'];
                } else {
                    $label = $fields[$i];
                }
                $columns[] = $label;
                if ($pdf_style == "TCPDF") {
                    $aligns[] = in_array($position, array('right', 'center')) ? $position : "left";
                    $widths[] = $size;
                } else {
                    $aligns[] = in_array($position, array('right', 'center')) ? (($position == 'right') ? 'R' : 'C') : 'L';
                    $widths[] = $eachColumnWidth;
                }
            }

            //Table data info
            $db_rec_cnt = count($db_recs);
            for ($i = 0; $i < $db_rec_cnt; $i++) {
                foreach ((array) $db_recs[$i] as $key => $val) {
                    if (is_array($fields) && in_array($key, $fields)) {
                        $data[$i][$key] = $this->listing->dataForExportMode($val, "pdf", $pdf_style);
                    }
                }
            }
            if ($pdf_style == "TCPDF") {
                require_once (APPPATH.'third_party/Pdf_export.php');
                $pdf = new PDF_Export(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, TRUE, 'UTF-8', FALSE);
                $pdf->initialize($heading);

                $pdf->writeGridTable($columns, $data, $widths, $aligns);
                $pdf->Output($filename.".pdf", 'D');
            } else {
                require_once (APPPATH.'third_party/fpdfclasses/fpdf.php');
                require_once (APPPATH.'third_party/fpdfclasses/mc_table.php');
                $pdf = new PDF_MC_Table();

                $fpdf_width = ($numberOfColumns*$eachColumnWidth)+20;
                $fpdf_height = ($numberOfColumns*$eachColumnWidth)+30;

                $pdf->pageHeightWidth = array(
                    $fpdf_width,
                    $fpdf_height,
                );
                $pdf->AddPage('P', $pdf->pageHeightWidth);
                $pdf->SetFont('Arial', '', 18);
                $pdf->Cell(0, 6, $heading, 0, 1, 'C');
                $pdf->Ln(10);
                $pdf->SetWidths($widths);
                $pdf->SetAligns($aligns);
                $pdf->SetFont('Arial', 'B', 14);
                $pdf->Row($columns);
                $pdf->SetFont('Arial', '', 12);
                $data_cnt = count($data);
                for ($k = 0; $k < $data_cnt; $k++) {
                    $pdf->Row(array_values($data[$k]));
                }
                $pdf->Output($filename.".pdf", 'D');
            }
        } elseif ($export_type == 'csv') {
            require_once (APPPATH.'third_party/Csv_export.php');
            $columns = $data = array();

            for ($i = 0; $i < $numberOfColumns; $i++) {
                if (array_key_exists($fields[$i], $list_config)) {
                    $label = $list_config[$fields[$i]]['label_lang'];
                } elseif (array_key_exists($fields[$i], $form_config)) {
                    $label = $form_config[$fields[$i]]['label_lang'];
                } else {
                    $label = $fields[$i];
                }
                $columns[] = $label;
            }
            $db_recs_cnt = count($db_recs);
            for ($i = 0; $i < $db_recs_cnt; $i++) {
                foreach ((array) $db_recs[$i] as $key => $val) {
                    if (is_array($fields) && in_array($key, $fields)) {
                        $data[$i][$key] = $this->listing->dataForExportMode($val, "csv");
                    }
                }
            }
            $export_array = array_merge(array($columns), $data);
            $csv = new CSV_Writer($export_array);
            $csv->headers($filename);
            $csv->output();
        }
        $this->skip_template_view();
    }

    /**
     * add method is used to add or update data records.
     */
    public function add() {
        $params_arr = $this->params_arr;
        $extra_qstr = $extra_hstr = '';
        $hideCtrl = $params_arr['hideCtrl'];
        $mode = (in_array($params_arr['mode'], array("Update", "View"))) ? "Update" : "Add";
        $viewMode = ($params_arr['mode'] == "View") ? TRUE : FALSE;
        $id = $params_arr['id'];
        $enc_id = $this->general->getAdminEncodeURL($id);
        try {
            $extra_cond = $this->model_storemanagement->extra_cond;
            if ($mode == "Update") {
                list($list_access, $view_access, $edit_access, $del_access, $expo_access) = $this->filter->getModuleWiseAccess("storemanagement", array("List", "View", "Update", "Delete", "Export"), TRUE, TRUE);
                if (!$edit_access && !$view_access) {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_VIEW_THIS_PAGE_C46_C46_C33'));
                }
            } else {
                list($list_access, $add_access, $del_access) = $this->filter->getModuleWiseAccess("storemanagement", array("List", "Add", "Delete"), TRUE, TRUE);
                if (!$add_access) {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_ADD_THESE_DETAILS_C46_C46_C33'));
                }
            }

            $data = array();
            if ($mode == 'Update') {
                $ctrl_flow = $this->ci_local->read($this->general->getMD5EncryptString("FlowEdit", "storemanagement"), $this->session->userdata('iAdminId'));
                $data_arr = $this->model_storemanagement->getData(intval($id));
                $data = $data_arr[0];
                if ((!is_array($data) || count($data) == 0) && $params_arr['rmPopup'] != "true") {
                    throw new Exception($this->general->processMessageLabel('ACTION_RECORDS_WHICH_YOU_ARE_TRYING_TO_ACCESS_ARE_NOT_AVAILABLE_C46_C46_C33'));
                }
                $switch_arr = $this->model_storemanagement->getSwitchTo($extra_cond, "records", $this->switchto_limit);
                $switch_combo = $this->filter->makeArrayDropDown($switch_arr);
                $switch_cit = array();
                $switch_tot = $this->model_storemanagement->getSwitchTo($extra_cond, "count");
                if ($this->switchto_limit > 0 && $switch_tot > $this->switchto_limit) {
                    $switch_cit['param'] = "true";
                    $switch_cit['url'] = $this->mod_enc_url['get_self_switch_to'];
                    if (!array_key_exists($id, $switch_combo)) {
                        $extra_cond = $this->db->protect($this->model_storemanagement->table_alias.".".$this->model_storemanagement->primary_key)." = ".$this->db->escape($id);
                        $switch_cur = $this->model_storemanagement->getSwitchTo($extra_cond, "records", 1);
                        if (is_array($switch_cur) && count($switch_cur) > 0) {
                            $switch_combo[$switch_cur[0]['id']] = $switch_cur[0]['val'];
                        }
                    }
                }
                $recName = $switch_combo[$id];
                $switch_enc_combo = $this->filter->getSwitchEncryptRec($switch_combo);
                $this->dropdown->combo("array", "vSwitchPage", $switch_enc_combo, $enc_id);
                $next_prev_records = $this->filter->getNextPrevRecords($id, $switch_arr);

                $this->general->trackModuleNavigation("Module", "Form", "Viewed", $this->mod_enc_url["add"], "storemanagement", $recName);
            } else {
                $recName = '';
                $ctrl_flow = $this->ci_local->read($this->general->getMD5EncryptString("FlowAdd", "storemanagement"), $this->session->userdata('iAdminId'));
                $this->general->trackModuleNavigation("Module", "Form", "Viewed", $this->mod_enc_url["add"], "storemanagement");
            }
            $opt_arr = $img_html = $auto_arr = $config_arr = array();

            $config_arr = array(
                'msd_store_code',
                'msd_store_logo',
                'msd_store_description',
                'ma1_email',
                'ma1_password',
                'msd_admin_id',
                'msd_store_rate_avg',
                'msd_store_total_review',
                'msd_status',
                'msd_date',
                'msd_modify_date',
                'ma1_name',
                'ma1_user_name',
                'ma1_phonenumber',
                'ma1_group_id',
                'ma1_last_access',
                'ma1_status',
            );
            $form_config = $this->model_storemanagement->getFormConfiguration($config_arr);
            if (is_array($form_config) && count($form_config) > 0) {
                foreach ($form_config as $key => $val) {
                    if ($params_arr['rmPopup'] == "true" && $params_arr[$key] != "") {
                        $data[$key] = $params_arr[$key];
                    }
                    elseif ($mode == "Add" || $val["show_input"] == "Update") {
                        $data[$key] = (trim($data[$key]) != "") ? $data[$key] : $val['default'];
                    }
                    if ($val['encrypt'] == "Yes") {
                        $data[$key] = $this->listing->adminLocalDecrypt($data[$key]);
                    }
                    if ($val['function'] != "") {
                        if (method_exists($this->general, $val['function'])) {
                            $data[$key] = $this->general->$val['function']($mode, $data[$key], $data, $id, $key, $key);
                        }
                    }
                    $source_field = $val['name'];
                    $combo_config = $this->dropdown_arr[$source_field];
                    if (is_array($combo_config) && count($combo_config) > 0) {
                        if ($combo_config['auto'] == "Yes") {
                            $combo_count = $this->getSourceOptions($source_field, $mode, $id, $data, '', 'count');
                            if ($combo_count[0]['tot'] > $this->dropdown_limit) {
                                $auto_arr[$source_field] = "Yes";
                            }
                        }
                        $combo_arr = $this->getSourceOptions($source_field, $mode, $id, $data);
                        $final_arr = $this->filter->makeArrayDropdown($combo_arr);
                        if ($combo_config['opt_group'] == "Yes") {
                            $display_arr = $this->filter->makeOPTDropdown($combo_arr);
                        } else {
                            $display_arr = $final_arr;
                        }
                        $this->dropdown->combo("array", $source_field, $display_arr, $data[$key]);
                        $opt_arr[$source_field] = $final_arr;
                    }
                    if ($val['file_upload'] == 'Yes') {
                        $del_file = ($edit_access && $viewMode != TRUE) ? TRUE : FALSE;
                        $val['htmlID'] = $val['name'];
                        $img_html[$key] = $this->listing->parseFormFile($data[$key], $id, $data, $val, $this->module_config, "Form", $del_file);
                    }
                }
            }
            $extra_qstr .= $this->general->getRequestURLParams();
            $extra_hstr .= $this->general->getRequestHASHParams();

            /** access controls <<< **/
            $controls_allow = $prev_link_allow = $next_link_allow = $update_allow = $delete_allow = $backlink_allow = $switchto_allow = $discard_allow = $tabing_allow = TRUE;
            $delete_allow = ($this->module_config["delete"] == "No" || !$del_access) ? TRUE : FALSE;
            if (is_array($switch_combo) && count($switch_combo) > 0) {
                $prev_link_allow = ($next_prev_records['prev']['id'] != '') ? TRUE : FALSE;
                $next_link_allow = ($next_prev_records['next']['id'] != '') ? TRUE : FALSE;
            } else {
                $prev_link_allow = $next_link_allow = $switchto_allow = FALSE;
            }
            if (!$list_access) {
                $backlink_allow = $discard_allow = FALSE;
            }
            if ($hideCtrl == "true") {
                $controls_allow = $prev_link_allow = $next_link_allow = $delete_allow = $backlink_allow = $switchto_allow = $tabing_allow = FALSE;
            }
            $wf_mode_arr = $this->module_config["workflow_modes"];
            if (is_array($wf_mode_arr) && in_array($mode, $wf_mode_arr)) {
                $controls_allow = FALSE;
            }
            /** access controls >>> **/
            $render_arr = array(

                "edit_access" => $edit_access,
                "expo_access" => $expo_access,
                'controls_allow' => $controls_allow,
                'prev_link_allow' => $prev_link_allow,
                'next_link_allow' => $next_link_allow,
                'update_allow' => $update_allow,
                'delete_allow' => $delete_allow,
                'backlink_allow' => $backlink_allow,
                'switchto_allow' => $switchto_allow,
                'discard_allow' => $discard_allow,
                'tabing_allow' => $tabing_allow,
                'enc_id' => $enc_id,
                'id' => $id,
                'mode' => $mode,
                'data' => $data,
                'recName' => $recName,
                "opt_arr" => $opt_arr,
                "img_html" => $img_html,
                "auto_arr" => $auto_arr,
                'ctrl_flow' => $ctrl_flow,
                'switch_combo' => $switch_combo,
                'switch_cit' => $switch_cit,
                'next_prev_records' => $next_prev_records,
                'folder_name' => $this->folder_name,
                'module_name' => $this->module_name,
                'mod_enc_url' => $this->mod_enc_url,
                'mod_enc_mode' => $this->mod_enc_mode,
                'extra_qstr' => $extra_qstr,
                'extra_hstr' => $extra_hstr,
            );
            $this->smarty->assign($render_arr);
            if ($mode == "Update") {
                if ($edit_access && $viewMode != TRUE) {
                    $this->loadView("storemanagement_edit");
                } else {
                    $this->loadView("storemanagement_add_view");
                }
            } else {
                $this->loadView("storemanagement_add");
            }
        } catch(Exception $e) {
            $render_arr['err_message'] = $e->getMessage();
            $this->smarty->assign($render_arr);
            $this->loadView($this->config->item('ADMIN_FORBIDDEN_TEMPLATE'));
        }
    }

    /**
     * addAction method is used to save data, which is posted through form.
     */
    public function addAction() {
        $params_arr = $this->params_arr;
        $mode = ($params_arr['mode'] == "Update") ? "Update" : "Add";
        $id = $params_arr['id'];
        try {
            $add_edit_access = $this->filter->getModuleWiseAccess("storemanagement", $mode, TRUE, TRUE);
            if (!$add_edit_access) {
                if ($mode == "Update") {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
                } else {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_ADD_THESE_DETAILS_C46_C46_C33'));
                }
            }
            if (method_exists($this->general, 'setcustomer_storeData')) {
                $event_res = $this->general->setcustomer_storeData($mode, $id, $params_arr['parID']);
                if (!$event_res['success']) {
                    $before_error_msg = $this->general->processMessageLabel('ACTION_BEFORE_EVENT_HAS_BEEN_FAILED_C46_C46_C33');
                    $error_msg = ($event_res['message']) ? $event_res['message'] : $before_error_msg;
                    throw new Exception($error_msg);
                }
            }

            $form_config = $this->model_storemanagement->getFormConfiguration();
            $params_arr = $this->_request_params();
            $msd_store_code = $params_arr["msd_store_code"];
            $msd_store_logo = $params_arr["msd_store_logo"];
            $msd_store_description = $params_arr["msd_store_description"];
            $ma1_email = $params_arr["ma1_email"];
            $ma1_password = $params_arr["ma1_password"];
            $msd_admin_id = $params_arr["msd_admin_id"];
            $msd_store_rate_avg = $params_arr["msd_store_rate_avg"];
            $msd_store_total_review = $params_arr["msd_store_total_review"];
            $msd_status = $params_arr["msd_status"];
            $msd_date = $params_arr["msd_date"];
            $msd_modify_date = $params_arr["msd_modify_date"];
            $ma1_name = $params_arr["ma1_name"];
            $ma1_user_name = $params_arr["ma1_user_name"];
            $ma1_phonenumber = $params_arr["ma1_phonenumber"];
            $ma1_group_id = $params_arr["ma1_group_id"];
            $ma1_last_access = $params_arr["ma1_last_access"];
            $ma1_status = $params_arr["ma1_status"];

            $data = $save_data_arr = $file_data = array();
            $data["vStoreCode"] = $msd_store_code;
            $data["vStoreLogo"] = $msd_store_logo;
            $data["tStoreDescription"] = $msd_store_description;
            $data["iAdminId"] = $msd_admin_id;
            $data["fStoreRateAvg"] = $msd_store_rate_avg;
            $data["iStoreTotalReview"] = $msd_store_total_review;
            $data["eStatus"] = $msd_status;
            $data["dDate"] = $this->filter->formatActionData($msd_date, $form_config["msd_date"]);
            $data["dModifyDate"] = $this->filter->formatActionData($msd_modify_date, $form_config["msd_modify_date"]);

            $save_data_arr["msd_store_code"] = $data["vStoreCode"];
            $save_data_arr["msd_store_logo"] = $data["vStoreLogo"];
            $save_data_arr["msd_store_description"] = $data["tStoreDescription"];
            $save_data_arr["msd_admin_id"] = $data["iAdminId"];
            $save_data_arr["msd_store_rate_avg"] = $data["fStoreRateAvg"];
            $save_data_arr["msd_store_total_review"] = $data["iStoreTotalReview"];
            $save_data_arr["msd_status"] = $data["eStatus"];
            $save_data_arr["msd_date"] = $data["dDate"];
            $save_data_arr["msd_modify_date"] = $data["dModifyDate"];
            if ($mode == 'Add') {
                $id = $this->model_storemanagement->insert($data);
                if (intval($id) > 0) {
                    $save_data_arr["iMstStoreDetailId"] = $data["iMstStoreDetailId"] = $id;
                    $msg = $this->general->processMessageLabel('ACTION_RECORD_ADDED_SUCCESSFULLY_C46_C46_C33');
                } else {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_ADDING_RECORD_C46_C46_C33'));
                }
                $track_cond = $this->db->protect("msd.iMstStoreDetailId")." = ".$this->db->escape($id);
                $switch_combo = $this->model_storemanagement->getSwitchTo($track_cond);
                $recName = $switch_combo[0]["val"];
                $this->general->trackModuleNavigation("Module", "Form", "Added", $this->mod_enc_url["add"], "storemanagement", $recName, "mode|".$this->general->getAdminEncodeURL("Update")."|id|".$this->general->getAdminEncodeURL($id));
            } elseif ($mode == 'Update') {
                $res = $this->model_storemanagement->update($data, intval($id));
                if (intval($res) > 0) {
                    $save_data_arr["iMstStoreDetailId"] = $data["iMstStoreDetailId"] = $id;
                    $msg = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                } else {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                }
                $track_cond = $this->db->protect("msd.iMstStoreDetailId")." = ".$this->db->escape($id);
                $switch_combo = $this->model_storemanagement->getSwitchTo($track_cond);
                $recName = $switch_combo[0]["val"];
                $this->general->trackModuleNavigation("Module", "Form", "Modified", $this->mod_enc_url["add"], "storemanagement", $recName, "mode|".$this->general->getAdminEncodeURL("Update")."|id|".$this->general->getAdminEncodeURL($id));
            }

            $rel_data = array();
            $rel_data["vEmail"] = $ma1_email;
            $rel_data["vPassword"] = $ma1_password;
            $rel_data["vName"] = $ma1_name;
            $rel_data["vUserName"] = $ma1_user_name;
            $rel_data["vPhonenumber"] = $ma1_phonenumber;
            $rel_data["iGroupId"] = $ma1_group_id;
            $rel_data["dLastAccess"] = $ma1_last_access;
            $rel_data["eStatus"] = $ma1_status;

            $rel_data["iAdminId"] = $data["iAdminId"];
            $save_data_arr["ma1_email"] = $rel_data["vEmail"];
            $save_data_arr["ma1_password"] = $rel_data["vPassword"];
            $save_data_arr["ma1_name"] = $rel_data["vName"];
            $save_data_arr["ma1_user_name"] = $rel_data["vUserName"];
            $save_data_arr["ma1_phonenumber"] = $rel_data["vPhonenumber"];
            $save_data_arr["ma1_group_id"] = $rel_data["iGroupId"];
            $save_data_arr["ma1_last_access"] = $rel_data["dLastAccess"];
            $save_data_arr["ma1_status"] = $rel_data["eStatus"];

            $rel_table_info = $this->model_storemanagement->relationDetails("ma1");
            $rel_extra_cond = $this->db->protect($rel_table_info["table_alias"].".iAdminId")." = ".$this->db->protect($data["iAdminId"]);
            if ($rel_table_info["extra_cond"] != "") {
                $rel_extra_cond .= " AND ".$rel_table_info["extra_cond"];
            }
            $rel_rec = $this->model_storemanagement->relationData("ma1", $rel_extra_cond);
            if (is_array($rel_rec) && count($rel_rec) > 0) {
                $rel_id = $rel_rec[0][$rel_table_info['primary_key']];
                $res = $this->model_storemanagement->relationUpdate("ma1", $rel_data, $rel_id);
            } else {
                $rel_id = $res = $this->model_storemanagement->relationInsert("ma1", $rel_data);
            }
            $save_data_arr["iAdminId"] = $rel_id;

            $file_data["msd_store_logo"]["file_name"] = $msd_store_logo;
            $file_data["msd_store_logo"]["old_file_name"] = $params_arr["old_msd_store_logo"];
            $file_data["msd_store_logo"]["primary_key"] = "iMstStoreDetailId";

            $this->listing->uploadFilesOnSaveForm($file_data, $form_config, $save_data_arr);

            $params_arr = $this->_request_params();

            $ret_arr['message'] = $msg;
            $ret_arr['success'] = 1;
        } catch(Exception $e) {
            $ret_arr['message'] = $e->getMessage();
            $ret_arr['success'] = 0;
        }
        $ret_arr['mod_enc_url']['add'] = $this->mod_enc_url['add'];
        $ret_arr['mod_enc_url']['index'] = $this->mod_enc_url['index'];
        $ret_arr['red_type'] = 'List';
        $this->filter->getPageFlowURL($ret_arr, $this->module_config, $params_arr, $id, $data);

        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    /**
     * inlineEditAction method is used to save inline editing data records, status field updation,
     * delete records either from grid listing or update form, saving inline adding records from grid
     */
    public function inlineEditAction() {
        $params_arr = $this->params_arr;
        $operartor = $params_arr['oper'];
        $all_row_selected = $params_arr['AllRowSelected'];
        $primary_ids = explode(",", $params_arr['id']);
        $primary_ids = count($primary_ids) > 1 ? $primary_ids : $primary_ids[0];
        $filters = $params_arr['filters'];
        if ($this->general->allowStripSlashes()) {
            $filters = stripslashes($filters);
        }
        $filters = json_decode($filters, TRUE);
        $extra_cond = '';
        $search_mode = $search_join = $search_alias = 'No';
        if ($all_row_selected == "true" && in_array($mode, array("del", "status"))) {
            $search_mode = ($mode == "del") ? "Delete" : "Update";
            $search_join = $search_alias = "Yes";
            $config_arr['module_name'] = $this->module_name;
            $config_arr['list_config'] = $this->model_storemanagement->getListConfiguration();
            $config_arr['form_config'] = $this->model_storemanagement->getFormConfiguration();
            $config_arr['table_name'] = $this->model_storemanagement->table_name;
            $config_arr['table_alias'] = $this->model_storemanagement->table_alias;
            $filter_main = $this->filter->applyFilter($filters, $config_arr, $search_mode);
            $filter_left = $this->filter->applyLeftFilter($filters, $config_arr, $search_mode);
            $filter_range = $this->filter->applyRangeFilter($filters, $config_arr, $search_mode);
            if ($filter_main != "") {
                $extra_cond .= ($extra_cond != "") ? " AND (".$filter_main.")" : $filter_main;
            }
            if ($filter_left != "") {
                $extra_cond .= ($extra_cond != "") ? " AND (".$filter_left.")" : $filter_left;
            }
            if ($filter_range != "") {
                $extra_cond .= ($extra_cond != "") ? " AND (".$filter_range.")" : $filter_range;
            }
        }
        if ($search_alias == "Yes") {
            $primary_field = $this->model_storemanagement->table_alias.".".$this->model_storemanagement->primary_key;
        } else {
            $primary_field = $this->model_storemanagement->primary_key;
        }
        if (is_array($primary_ids)) {
            $pk_condition = $this->db->protect($primary_field)." IN ('".implode("','", $primary_ids)."')";
        } elseif (intval($primary_ids) > 0) {
            $pk_condition = $this->db->protect($primary_field)." = ".$this->db->escape($primary_ids);
        } else {
            $pk_condition = FALSE;
        }
        if ($pk_condition) {
            $extra_cond .= ($extra_cond != "") ? " AND (".$pk_condition.")" : $pk_condition;
        }
        $data_arr = $save_data_arr = array();
        try {
            switch ($operartor) {
                case 'del':
                    $mode = "Delete";

                    $del_access = $this->filter->getModuleWiseAccess("storemanagement", "Delete", TRUE, TRUE);
                    if (!$del_access) {
                        throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_DELETE_THESE_DETAILS_C46_C46_C33'));
                    }
                    if ($search_mode == "No" && $pk_condition == FALSE) {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }

                    $params_arr = $this->_request_params();
                    if (is_array($primary_ids)) {
                        $main_cond = $this->db->protect("msd.iMstStoreDetailId")." IN ('".@implode("','", $primary_ids)."')";
                    } else {
                        $main_cond = $this->db->protect("msd.iMstStoreDetailId")." = ".$this->db->escape($primary_ids);
                    }
                    $main_data = $this->model_storemanagement->getData($main_cond, array('msd.vStoreLogo', 'msd.vGSTIRDProof', 'msd.vBankAccountProof', 'msd.vOtherProof'));
                    $success = $this->model_storemanagement->delete($extra_cond, $search_alias, $search_join);
                    if (!$success) {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }
                    $message = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_DELETED_SUCCESSFULLY_C46_C46_C33');

                    $form_config = $this->model_storemanagement->getFormConfiguration();
                    $this->general->deleteMediaFiles($form_config, $main_data, $this->module_config["physical_data_remove"]);
                    break;
                case 'edit':
                    $mode = "Update";
                    $edit_access = $this->filter->getModuleWiseAccess("storemanagement", "Update", TRUE, TRUE);
                    if (!$edit_access) {
                        throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
                    }
                    $post_name = $params_arr['name'];
                    $post_val = is_array($params_arr['value']) ? @implode(",", $params_arr['value']) : $params_arr['value'];

                    $list_config = $this->model_storemanagement->getListConfiguration($post_name);
                    $form_config = $this->model_storemanagement->getFormConfiguration($list_config['source_field']);
                    if (!is_array($form_config) || count($form_config) == 0) {
                        throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
                    }
                    if (in_array($form_config['type'], array("date", "date_and_time", "time", 'phone_number'))) {
                        $post_val = $this->filter->formatActionData($post_val, $form_config);
                    }
                    if ($form_config["encrypt"] == "Yes") {
                        $post_val = $this->listing->adminLocalEncrypt($post_val);
                    }
                    $field_name = $form_config['field_name'];
                    $unique_name = $form_config['name'];

                    $db_rec_arr = $this->model_storemanagement->getData(intval($primary_ids));
                    if ($form_config['entry_type'] == "Relation") {
                        $rel_table_info = $this->model_storemanagement->relationDetails($form_config['table_alias']);
                        $update_field = $rel_table_info['table_alias'].".".$field_name;
                        $where_cond = $this->db->protect($this->model_storemanagement->table_alias.".".$this->model_storemanagement->primary_key)." = ".$this->db->escape($primary_ids);
                        $data_arr[$update_field] = $post_val;
                        $success = $this->model_storemanagement->relationUpdate($form_config['table_alias'], $data_arr, $where_cond, "Yes");
                    } else {

                        $data_arr[$field_name] = $post_val;
                        $success = $this->model_storemanagement->update($data_arr, intval($primary_ids));
                    }
                    if (!$success) {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                    }
                    $message = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                    break;
                case 'status':
                    $mode = "Status";
                    $edit_access = $this->filter->getModuleWiseAccess("storemanagement", "Update", TRUE, TRUE);
                    if (!$edit_access) {
                        throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
                    }
                    if ($search_mode == "No" && $pk_condition == FALSE) {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }
                    $status_field = "eHaveOnlineStore";
                    if ($status_field == "") {
                        throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
                    }
                    if ($search_mode == "Yes" || $search_alias == "Yes") {
                        $field_name = $this->model_storemanagement->table_alias.".eHaveOnlineStore";
                    } else {
                        $field_name = $status_field;
                    }
                    $data_arr[$field_name] = $params_arr['status'];
                    $success = $this->model_storemanagement->update($data_arr, $extra_cond, $search_alias, $search_join);
                    if (!$success) {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_MODIFYING_THESE_RECORDS_C46_C46_C33'));
                    }
                    $message = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_MODIFIED_SUCCESSFULLY_C46_C46_C33');
                    break;
            }
            $ret_arr['success'] = "true";
            $ret_arr['message'] = $message;
            if ($mode == "Update") {
                $save_data_arr = $db_rec_arr[0];
                $save_data_arr[$unique_name] = $post_val;
                if ($form_config["type"] == "file") {
                    $file_data[$unique_name]["file_name"] = $post_val;
                    $file_data[$unique_name]["old_file_name"] = $params_arr["old_file"];
                    if ($form_config["entry_type"] == "Relation") {
                        $rel_table_info = $this->model_storemanagement->relationDetails($form_config["table_alias"]);
                        $file_data[$unique_name]["primary_key"] = $rel_table_info["primary_key"];
                    } else {
                        $file_data[$unique_name]["primary_key"] = "iMstStoreDetailId";
                    }
                    $this->listing->uploadFilesOnSaveForm($file_data, array($unique_name => $form_config), $save_data_arr);
                }
            }
        } catch(Exception $e) {
            $ret_arr['success'] = "false";
            $ret_arr['message'] = $e->getMessage();
        }
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    /**
     * processConfiguration method is used to process add and edit permissions for grid intialization
     */
    public function processConfiguration(&$list_config = array(), $isAdd = TRUE, $isEdit = TRUE, $runCombo = FALSE) {
        if (!is_array($list_config) || count($list_config) == 0) {
            return $list_config;
        }
        $count_arr = array();
        foreach ((array) $list_config as $key => $val) {
            if (!$isAdd) {
                $list_config[$key]["addable"] = "No";
            }
            if (!$isEdit) {
                $list_config[$key]["editable"] = "No";
            }
        }
        $this->count_arr = $count_arr;
        return $list_config;
    }

    /**
     * getSourceOptions method is used to get data array of enum, table, token or php function input types
     * @param string $name unique name of form configuration field.
     * @param string $mode mode for add or update form.
     * @param string $id update record id of add or update form.
     * @param array $data data array of add or update record.
     * @param string $extra extra query condition for searching data array.
     * @param string $rtype type for getting either records list or records count.
     * @return array $data_arr returns data records array
     */
    public function getSourceOptions($name = '', $mode = 'Add', $id = '', $data = array(), $extra = '', $rtype = 'records') {
        $combo_config = $this->dropdown_arr[$name];
        $data_arr = array();
        if (!is_array($combo_config) || count($combo_config) == 0) {
            return $data_arr;
        }
        $type = $combo_config['type'];
        switch ($type) {
            case 'enum':
                $data_arr = is_array($combo_config['values']) ? $combo_config['values'] : array();
                break;
            case 'token':
                if ($combo_config['parent_src'] == "Yes" && in_array($mode, array("Add", "Update", "Auto"))) {
                    $source_field = $combo_config['source_field'];
                    $target_field = $combo_config['target_field'];
                    if (in_array($mode, array("Update", "Auto")) || $data[$source_field] != "") {
                        $parent_src = (is_array($data[$source_field])) ? $data[$source_field] : explode(",", $data[$source_field]);
                        $extra_cond = $this->db->protect($target_field)." IN ('".implode("','", $parent_src)."')";
                    } elseif ($mode == "Add") {
                        $extra_cond = $this->db->protect($target_field)." = ''";
                    }
                    $extra = (trim($extra) != "") ? $extra." AND ".$extra_cond : $extra_cond;
                }
                $data_arr = $this->filter->getTableLevelDropdown($combo_config, $id, $extra, $rtype);
                break;
            case 'table':
                if ($combo_config['parent_src'] == "Yes" && in_array($mode, array("Add", "Update", "Auto"))) {
                    $source_field = $combo_config['source_field'];
                    $target_field = $combo_config['target_field'];
                    if (in_array($mode, array("Update", "Auto")) || $data[$source_field] != "") {
                        $parent_src = (is_array($data[$source_field])) ? $data[$source_field] : explode(",", $data[$source_field]);
                        $extra_cond = $this->db->protect($target_field)." IN ('".implode("','", $parent_src)."')";
                    } elseif ($mode == "Add") {
                        $extra_cond = $this->db->protect($target_field)." = ''";
                    }
                    $extra = (trim($extra) != "") ? $extra." AND ".$extra_cond : $extra_cond;
                }
                if ($combo_config['parent_child'] == "Yes" && $combo_config['nlevel_child'] == "Yes") {
                    $combo_config['main_table'] = $this->model_storemanagement->table_name;
                    $data_arr = $this->filter->getTreeLevelDropdown($combo_config, $id, $extra, $rtype);
                } else {
                    if ($combo_config['parent_child'] == "Yes" && $combo_config['parent_field'] != "") {
                        $parent_field = $combo_config['parent_field'];
                        $extra_cond = "(".$this->db->protect($parent_field)." = '0' OR ".$this->db->protect($parent_field)." = '' OR ".$this->db->protect($parent_field)." IS NULL )";
                        if ($mode == "Update" || ($mode == "Search" && $id > 0)) {
                            $par_cond .= " AND ".$this->db->protect($combo_config['field_key'])." <> ".$this->db->escape($id);
                        }
                        $extra = (trim($extra) != "") ? $extra." AND ".$extra_cond : $extra_cond;
                    }
                    $data_arr = $this->filter->getTableLevelDropdown($combo_config, $id, $extra, $rtype);
                }
                break;
            case 'phpfn':
                if (method_exists($this->general, $combo_config['function'])) {
                    $parent_src = '';
                    if ($combo_config['parent_src'] == "Yes" && in_array($mode, array("Add", "Update", "Auto"))) {
                        $source_field = $combo_config['source_field'];
                        if (in_array($mode, array("Update", "Auto")) || $data[$source_field] != "") {
                            $parent_src = $data[$source_field];
                        }
                    }
                    $data_arr = $this->general->$combo_config['function']($data[$name], $mode, $id, $data, $parent_src);
                }
                break;
        }
        return $data_arr;
    }

    /**
     * getSelfSwitchToPrint method is used to provide autocomplete for switchto dropdown, which is called through form.
     */
    public function getSelfSwitchTo() {
        $params_arr = $this->params_arr;

        $term = strtolower($params_arr['data']['q']);

        $switchto_fields = $this->model_storemanagement->switchto_fields;
        $extra_cond = $this->model_storemanagement->extra_cond;

        $concat_fields = $this->db->concat_cast($switchto_fields);
        $search_cond = "(LOWER(".$concat_fields.") LIKE '".$this->db->escape_like_str($term)."%' OR LOWER(".$concat_fields.") LIKE '% ".$this->db->escape_like_str($term)."%')";
        $extra_cond = ($extra_cond == "") ? $search_cond : $extra_cond." AND ".$search_cond;

        $switch_arr = $this->model_storemanagement->getSwitchTo($extra_cond);
        $html_arr = $this->filter->getChosenAutoJSON($switch_arr, array(), FALSE, "auto");

        $json_array['q'] = $term;
        $json_array['results'] = $html_arr;
        $html_str = json_encode($json_array);

        echo $html_str;
        $this->skip_template_view();
    }

    /**
     * parentSourceOptions method is used to get dropdown values of parent source relationship in update form (select options in html string)
     */
    public function parentSourceOptions() {
        $params_arr = $this->params_arr;
        $unique_name = $params_arr['unique_name'];
        //$id = $params_arr['id'];
        $parent_src = $params_arr['parent_src'];
        $mode = $params_arr['mode'];
        $parent_src = (is_array($parent_src)) ? $parent_src : explode(",", $parent_src);
        $source_field = $unique_name;
        $combo_config = $this->dropdown_arr[$source_field];
        $html_str = '';
        if ($combo_config['target_field'] != "") {
            $data_arr[$combo_config['source_field']] = $parent_src;
            $combo_arr = $this->getSourceOptions($source_field, 'Auto', $id, $data_arr, $extra_cond);
            if ($combo_config['opt_group'] == "Yes") {
                $combo_arr = $this->filter->makeOPTDropdown($combo_arr);
            } else {
                $combo_arr = $this->filter->makeArrayDropdown($combo_arr);
            }
            $this->dropdown->combo("array", $source_field, $combo_arr, $id);
            $top_option = (in_array($mode, array("Add", "Update")) && $combo_config['default'] == 'Yes') ? "|||" : '';
            $html_str = $this->dropdown->display($source_field, $source_field, ' multiple=true ', $top_option);
            $ret_arr['status'] = TRUE;
            $ret_arr['content'] = $html_str;
        } else {
            $ret_arr['status'] = FALSE;
            $ret_arr['content'] = "";
        }
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    /**
     * uploadFormFile method is used to upload files or images through add or update form
     */
    public function uploadFormFile() {
        $this->load->library('upload');
        $params_arr = $this->params_arr;
        $unique_name = $params_arr['unique_name'];
        $id = $params_arr['id'];
        $old_file = $params_arr['oldFile'];
        $type = $params_arr['type'];
        $upload_files = $_FILES['Filedata'];
        list($file_name, $extension) = $this->general->get_file_attributes($upload_files['name']);
        $this->general->createUploadFolderIfNotExists('__temp');
        $config_arr = $this->model_storemanagement->getFormConfiguration($unique_name);

        $temp_folder_path = $this->config->item('admin_upload_temp_path');
        $temp_folder_url = $this->config->item('admin_upload_temp_url');
        try {
            if ($type == 'webcam') {
                $img_pic = $params_arr['newFile'];
                $res = $this->general->imageupload_base64($temp_folder_path, $img_pic);
                if ($res[0] == FALSE) {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPLOADING_C46_C46_C33'));
                }
                $file_name = $res[0];
            } else {
                $file_size = ($config_arr['file_size']) ? $config_arr['file_size'] : $this->config->item('ADMIN_MAX_UPLOAD_FILE_SIZE');
                if ($config_arr['file_folder'] == "") {
                    throw new Exception($this->general->processMessageLabel('ACTION_PLEASE_SPECIFY_THE_UPLOAD_FOLDER_NAME_C46_C46_C33'));
                }
                if ($upload_files['name'] == "") {
                    throw new Exception($this->general->processMessageLabel('ACTION_UPLOAD_FILE_NOT_FOUND_C46_C46_C33'));
                }
                if (!$this->general->validateFileSize($file_size, $upload_files['size'])) {
                    throw new Exception($this->general->processMessageLabel('ACTION_FILE_SIZE_NOT_A_VALID_ONE_C46_C46_C33'));
                }
                $upload_config = array(
                    'upload_path' => $temp_folder_path,
                    'allowed_types' => '*',
                    'max_size' => $file_size,
                    'file_name' => $file_name,
                    'remove_space' => TRUE,
                    'overwrite' => FALSE,
                );
                $this->upload->initialize($upload_config);
                if (!$this->upload->do_upload('Filedata')) {
                    $upload_error = $this->upload->display_errors('', '');
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPLOADING_C46_C46_C33'));
                }
                $file_info = $this->upload->data();
                $file_name = $file_info['file_name'];
            }
            if (!$file_name) {
                throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPLOADING_C46_C46_C33'));
            }
            $image_valid_ext = @explode('.', $file_name);
            if (in_array(strtolower(end($image_valid_ext)), $this->config->item('IMAGE_EXTENSION_ARR'))) {
                $ret_arr['fileURL'] = $temp_folder_url.$file_name;
                $ret_arr['width'] = ($config_arr['file_width']) ? $config_arr['file_width'] : $this->config->item('ADMIN_DEFAULT_IMAGE_WIDTH');
                $ret_arr['height'] = ($config_arr['file_height']) ? $config_arr['file_height'] : $this->config->item('ADMIN_DEFAULT_IMAGE_HEIGHT');
            }
            if (file_exists($temp_folder_path.$old_file) && $old_file != '') {
                @unlink($temp_folder_path.$old_file);
            }

            $ret_arr['success'] = 1;
            $ret_arr['message'] = $this->general->processMessageLabel('ACTION_FILE_UPLOADED_SUCCESSFULLY_C46_C46_C33');
            $ret_arr['uploadfile'] = $file_name;
            $ret_arr['oldfile'] = $file_name;
        } catch(Exception $e) {
            $ret_arr['success'] = 0;
            $ret_arr['message'] = $e->getMessage();
        }
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }
    /**
     * downloadFormFile method is used to download add or update form files
     */
    public function downloadFormFile() {
        $params_arr = $this->params_arr;
        $unique_name = $params_arr['unique_name'];
        $folder = $params_arr['folder'];
        $id = $params_arr['id'];
        $config_arr = $this->model_storemanagement->getFormConfiguration($unique_name);
        try {
            if (!is_array($config_arr) || count($config_arr) == 0) {
                throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
            }
            $fields = $config_arr['table_alias'].".".$config_arr['field_name']." AS ".$unique_name;
            $data_arr = $this->model_storemanagement->getData(intval($id), $fields, '', '', '', 'Yes');
            if (!is_array($data_arr) || count($data_arr) == 0) {
                throw new Exception($this->general->processMessageLabel('ACTION_RECORDS_WHICH_YOU_ARE_TRYING_TO_ACCESS_ARE_NOT_AVAILABLE_C46_C46_C33'));
            }
            $file_name = $data_arr[0][$unique_name];
            $this->listing->downloadFiles("Form", $config_arr, $file_name, $folder);
        } catch(Exception $e) {
            $ret_arr['success'] = 0;
            $ret_arr['message'] = $e->getMessage();
            echo json_encode($ret_arr);
        }
        $this->skip_template_view();
    }
    /**
     * deleteFormFile method is used to delete add or update form files
     */
    public function deleteFormFile() {
        $params_arr = $this->params_arr;
        $unique_name = $params_arr['unique_name'];
        $folder = $params_arr['folder'];
        $id = $params_arr['id'];
        $config_arr = $this->model_storemanagement->getFormConfiguration($unique_name);
        try {
            if (!is_array($config_arr) || count($config_arr) == 0) {
                throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
            }
            $field_name = $config_arr['field_name'];
            $fields = $config_arr['table_alias'].".".$field_name." AS ".$unique_name;
            $data_arr = $this->model_storemanagement->getData(intval($id), $fields, '', '', '', 'Yes');
            if (!is_array($data_arr) || count($data_arr) == 0) {
                throw new Exception($this->general->processMessageLabel('ACTION_RECORDS_WHICH_YOU_ARE_TRYING_TO_ACCESS_ARE_NOT_AVAILABLE_C46_C46_C33'));
            }
            if ($config_arr['entry_type'] == "Relation") {
                $rel_table_info = $this->model_storemanagement->relationDetails($config_arr['table_alias']);
                $update_field = $rel_table_info['table_alias'].".".$field_name;
                $where_cond = $this->db->protect($this->model_storemanagement->table_alias.".".$this->model_storemanagement->primary_key)." = ".$this->db->escape($id);
                $update_arr[$update_field] = '';
                $res = $this->model_storemanagement->relationUpdate($rel_table_info['table_alias'], $update_arr, $where_cond, "Yes");
            } else {
                $update_arr[$field_name] = '';
                $res = $this->model_storemanagement->update($update_arr, intval($id));
            }
            if (!$res) {
                throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_FILE_DELETION_C46_C46_C33'));
            }
            $file_name = $data_arr[0][$unique_name];
            $res = $this->listing->deleteFiles($config_arr, $file_name, $folder, "Form");
            if (!$res) {
                throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_FILE_DELETION_C46_C46_C33'));
            }
            $success = 1;
            $message = $this->general->processMessageLabel('ACTION_FILE_DELETED_SUCCESSFULLY_C46_C46_C33');
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $ret_arr['success'] = $success;
        $ret_arr['message'] = $message;
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }
    /**
     * getTabWiseBlock method is used to render custom view tab wise update form data
     */
    public function getTabWiseBlock() {
        $params_arr = $this->params_arr;
        $extra_qstr = $extra_hstr = '';
        $mode = "Update";
        $tab_code = $params_arr["tab_code"];
        $id = $params_arr['id'];
        list($view_access, $edit_access, $del_access, $expo_access) = $this->filter->getModuleWiseAccess("storemanagement", array("View", "Update", "Delete", "Export"), TRUE, TRUE);
        try {
            if (!$edit_access && !$view_access) {
                throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_VIEW_THIS_PAGE_C46_C46_C33'));
            }
            $data = $config_arr = array();
            /*{*parent module condition*}*/
            $data_arr = $this->model_storemanagement->getData(intval($id));
            $data = $data_arr[0];
            if (!is_array($data) || count($data) == 0) {
                throw new Exception($this->general->processMessageLabel('ACTION_RECORDS_WHICH_YOU_ARE_TRYING_TO_ACCESS_ARE_NOT_AVAILABLE_C46_C46_C33'));
            }

            $extra_cond = $this->model_storemanagement->extra_cond;
            $switch_arr = $this->model_storemanagement->getSwitchTo($extra_cond);
            $switch_combo = $this->filter->makeArrayDropDown($switch_arr);
            $recName = $switch_combo[$id];

            $this->general->trackModuleNavigation("Module", "Form", "Viewed", $this->mod_enc_url["add"], "storemanagement", $recName);

            $opt_arr = $img_html = $auto_arr = $config_arr = array();
            switch ($tab_code) {
                case "sellerregistration":
                    $tab_view_html = "storemanagement_add_view_sellerregistration";
                    $tab_add_html = "storemanagement_add_sellerregistration";

                    $config_arr = array(
                        'msd_store_code',
                        'msd_store_logo',
                        'msd_store_description',
                        'ma1_email',
                        'ma1_password',
                    );
                    break;
                case "businessaccountdetails":
                    $tab_view_html = "storemanagement_add_view_businessaccountdetails";
                    $tab_add_html = "storemanagement_add_businessaccountdetails";

                    $config_arr = array(
                        'msd_store_name',
                        'msd_company_name',
                        'msd_contact_name',
                        'msd_contact_number',
                        'msd_address1',
                        'msd_address2',
                        'msd_area',
                        'msd_country_id',
                        'msd_state_id',
                        'msd_city_id',
                        'msd_pin_code',
                        'msd_website_url',
                        'msd_mst_categories_id',
                        'msd_have_online_store',
                        'msd_store_url',
                    );
                    break;
                case "bankdetails":
                    $tab_view_html = "storemanagement_add_view_bankdetails";
                    $tab_add_html = "storemanagement_add_bankdetails";

                    $config_arr = array(
                        'msd_bank_account_name',
                        'sys_custom_field_39',
                        'sys_custom_field_40',
                        'sys_custom_field_41',
                        'sys_custom_field_42',
                        'sys_custom_field_43',
                        'sys_custom_field_44',
                        'sys_custom_field_45',
                        'msd_g_s_t_i_r_d_proof',
                        'msd_bank_account_proof',
                        'msd_other_proof',
                        'msd_other_proof_detail',
                        'msd_accept_term',
                    );
                    break;
            }
            $form_config = $this->model_storemanagement->getFormConfiguration($config_arr);
            if (is_array($form_config) && count($form_config) > 0) {
                foreach ($form_config as $key => $val) {
                    $source_field = $val['name'];
                    $combo_config = $this->dropdown_arr[$source_field];
                    if (is_array($combo_config) && count($combo_config) > 0) {
                        if ($combo_config['auto'] == "Yes") {
                            $combo_count = $this->getSourceOptions($source_field, $mode, $id, $data, '', 'count');
                            if ($combo_count[0]['tot'] > $this->dropdown_limit) {
                                $auto_arr[$source_field] = "Yes";
                            }
                        }
                        $combo_arr = $this->getSourceOptions($source_field, $mode, $id, $data);
                        $final_arr = $this->filter->makeArrayDropdown($combo_arr);
                        if ($combo_config['opt_group'] == "Yes") {
                            $display_arr = $this->filter->makeOPTDropdown($combo_arr);
                        } else {
                            $display_arr = $final_arr;
                        }
                        $this->dropdown->combo("array", $source_field, $display_arr, $data[$key]);
                        $opt_arr[$source_field] = $final_arr;
                    }
                    if ($mode == "Add" || $val["show_input"] == "Update") {
                        $data[$key] = (trim($data[$key]) != "") ? $data[$key] : $val['default'];
                    }
                    if ($val['file_upload'] == 'Yes') {
                        $del_file = ($edit_access && $viewMode != TRUE) ? TRUE : FALSE;
                        $val['htmlID'] = $val['name'];
                        $img_html[$key] = $this->listing->parseFormFile($data[$key], $id, $data, $val, $this->module_config, "Form", $del_file);
                    }
                    if ($val['encrypt'] == "Yes") {
                        $data[$key] = $this->listing->adminLocalDecrypt($data[$key]);
                    }
                    if ($val['function'] != "") {
                        if (method_exists($this->general, $val['function'])) {
                            $data[$key] = $this->general->$val['function']($mode, $data[$key], $data, $id, $key, $key);
                        }
                    }
                }
            }
            $extra_qstr .= $this->general->getRequestURLParams();
            $extra_hstr .= $this->general->getRequestHASHParams();
            $enc_id = $this->general->getAdminEncodeURL($id);
            $render_arr = array(

                'enc_id' => $enc_id,
                'id' => $id,
                'mode' => $mode,
                'data' => $data,
                "opt_arr" => $opt_arr,
                "img_html" => $img_html,
                "auto_arr" => $auto_arr,
                'del_access' => $del_access,
                'expo_access' => $expo_access,
                'folder_name' => $this->folder_name,
                'module_name' => $this->module_name,
                'mod_enc_url' => $this->mod_enc_url,
                'mod_enc_mode' => $this->mod_enc_mode,
                'extra_qstr' => $extra_qstr,
                'extra_hstr' => $extra_hstr,
            );
            if ($edit_access) {
                $render_tpl = $tab_add_html;
            } else {
                $render_tpl = $tab_view_html;
            }
            $parse_html = $this->parser->parse($render_tpl, $render_arr, TRUE);
            echo $parse_html;
        } catch(Exception $e) {
            $render_arr['err_message'] = $e->getMessage();
            echo $this->parser->parse($this->CI->config->item('ADMIN_FORBIDDEN_TEMPLATE').".tpl", $render_arr, TRUE);
        }
        $this->skip_template_view();
    }
    /**
     * saveTabWiseBlock method is used to save custom view tab wise update form data
     */
    public function saveTabWiseBlock() {
        $params_arr = $this->params_arr;
        $mode = "Update";
        $id = $params_arr['id'];
        $tab_code = $params_arr["tab_code"];
        $extra_hstr = '';
        /*{*parent module condition*}*/
        try {
            $edit_access = $this->filter->getModuleWiseAccess("storemanagement", $mode, TRUE, TRUE);
            if (!$edit_access) {
                throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
            }
            if (method_exists($this->general, 'setcustomer_storeData')) {
                $event_res = $this->general->setcustomer_storeData($mode, $id, $params_arr['parID']);
                if (!$event_res['success']) {
                    $before_error_msg = $this->general->processMessageLabel('ACTION_BEFORE_EVENT_HAS_BEEN_FAILED_C46_C46_C33');
                    $error_msg = ($event_res['message']) ? $event_res['message'] : $before_error_msg;
                    throw new Exception($error_msg);
                }
            }

            $data = $save_data_arr = $file_data = array();
            $form_config = $this->model_storemanagement->getFormConfiguration();
            $params_arr = $this->_request_params();
            switch ($tab_code) {
                case "sellerregistration":
                    $msd_store_code = $params_arr["msd_store_code"];
                    $msd_store_logo = $params_arr["msd_store_logo"];
                    $msd_store_description = $params_arr["msd_store_description"];
                    $ma1_email = $params_arr["ma1_email"];
                    $ma1_password = $params_arr["ma1_password"];

                    $data["vStoreCode"] = $msd_store_code;
                    $data["vStoreLogo"] = $msd_store_logo;
                    $data["tStoreDescription"] = $msd_store_description;

                    $res = $this->model_storemanagement->update($data, intval($id));
                    if (intval($res) > 0) {
                        $msg = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                    } else {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                    }
                    $save_data_arr["iMstStoreDetailId"] = $data["iMstStoreDetailId"] = $id;
                    $rel_data = array();
                    $rel_data["vEmail"] = $ma1_email;
                    $rel_data["vPassword"] = $ma1_password;

                    $rel_data["iAdminId"] = $data["iAdminId"];
                    $save_data_arr["ma1_email"] = $rel_data["vEmail"];
                    $save_data_arr["ma1_password"] = $rel_data["vPassword"];

                    $rel_table_info = $this->model_storemanagement->relationDetails("ma1");
                    $rel_extra_cond = $this->db->protect($rel_table_info["table_alias"].".iAdminId")." = ".$this->db->protect($data["iAdminId"]);
                    if ($rel_table_info["extra_cond"] != "") {
                        $rel_extra_cond .= " AND ".$rel_table_info["extra_cond"];
                    }
                    $rel_rec = $this->model_storemanagement->relationData("ma1", $rel_extra_cond);
                    if (is_array($rel_rec) && count($rel_rec) > 0) {
                        $rel_id = $rel_rec[0][$rel_table_info['primary_key']];
                        $res = $this->model_storemanagement->relationUpdate("ma1", $rel_data, $rel_id);
                    } else {
                        $rel_id = $res = $this->model_storemanagement->relationInsert("ma1", $rel_data);
                    }
                    $save_data_arr["iAdminId"] = $rel_id;

                    $file_data["msd_store_logo"]["file_name"] = $msd_store_logo;
                    $file_data["msd_store_logo"]["old_file_name"] = $params_arr["old_msd_store_logo"];
                    $file_data["msd_store_logo"]["primary_key"] = "iMstStoreDetailId";

                    $this->listing->uploadFilesOnSaveForm($file_data, $form_config, $save_data_arr);
                    break;
                case "businessaccountdetails":
                    $msd_store_name = $params_arr["msd_store_name"];
                    $msd_company_name = $params_arr["msd_company_name"];
                    $msd_contact_name = $params_arr["msd_contact_name"];
                    $msd_contact_number = $params_arr["msd_contact_number"];
                    $msd_address1 = $params_arr["msd_address1"];
                    $msd_address2 = $params_arr["msd_address2"];
                    $msd_area = $params_arr["msd_area"];
                    $msd_country_id = $params_arr["msd_country_id"];
                    $msd_state_id = $params_arr["msd_state_id"];
                    $msd_city_id = $params_arr["msd_city_id"];
                    $msd_pin_code = $params_arr["msd_pin_code"];
                    $msd_website_url = $params_arr["msd_website_url"];
                    $msd_mst_categories_id = $params_arr["msd_mst_categories_id"];
                    $msd_have_online_store = $params_arr["msd_have_online_store"];
                    $msd_store_url = $params_arr["msd_store_url"];

                    $data["vStoreName"] = $msd_store_name;
                    $data["vCompanyName"] = $msd_company_name;
                    $data["vContactName"] = $msd_contact_name;
                    $data["vContactNumber"] = $this->filter->formatActionData($msd_contact_number, $form_config["msd_contact_number"]);
                    $data["vAddress1"] = $msd_address1;
                    $data["vAddress2"] = $msd_address2;
                    $data["vArea"] = $msd_area;
                    $data["iCountryId"] = $msd_country_id;
                    $data["iStateId"] = $msd_state_id;
                    $data["iCityId"] = $msd_city_id;
                    $data["vPinCode"] = $msd_pin_code;
                    $data["vWebsiteUrl"] = $msd_website_url;
                    $data["iMstCategoriesId"] = $msd_mst_categories_id;
                    $data["eHaveOnlineStore"] = $msd_have_online_store;
                    $data["vStoreUrl"] = $msd_store_url;

                    $res = $this->model_storemanagement->update($data, intval($id));
                    if (intval($res) > 0) {
                        $msg = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                    } else {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                    }
                    $save_data_arr["iMstStoreDetailId"] = $data["iMstStoreDetailId"] = $id;
                    break;
                case "bankdetails":
                    $msd_bank_account_name = $params_arr["msd_bank_account_name"];
                    $msd_g_s_t_i_r_d_proof = $params_arr["msd_g_s_t_i_r_d_proof"];
                    $msd_bank_account_proof = $params_arr["msd_bank_account_proof"];
                    $msd_other_proof = $params_arr["msd_other_proof"];
                    $msd_other_proof_detail = $params_arr["msd_other_proof_detail"];
                    $msd_accept_term = $params_arr["msd_accept_term"];

                    $data["vBankAccountName"] = $msd_bank_account_name;
                    $data["vGSTIRDProof"] = $msd_g_s_t_i_r_d_proof;
                    $data["vBankAccountProof"] = $msd_bank_account_proof;
                    $data["vOtherProof"] = $msd_other_proof;
                    $data["vOtherProofDetail"] = $msd_other_proof_detail;
                    $data["eAcceptTerm"] = $this->filter->formatActionData($msd_accept_term, $form_config["msd_accept_term"]);

                    $res = $this->model_storemanagement->update($data, intval($id));
                    if (intval($res) > 0) {
                        $msg = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                    } else {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                    }
                    $save_data_arr["iMstStoreDetailId"] = $data["iMstStoreDetailId"] = $id;

                    $file_data["msd_g_s_t_i_r_d_proof"]["file_name"] = $msd_g_s_t_i_r_d_proof;
                    $file_data["msd_g_s_t_i_r_d_proof"]["old_file_name"] = $params_arr["old_msd_g_s_t_i_r_d_proof"];
                    $file_data["msd_g_s_t_i_r_d_proof"]["primary_key"] = "iMstStoreDetailId";
                    $file_data["msd_bank_account_proof"]["file_name"] = $msd_bank_account_proof;
                    $file_data["msd_bank_account_proof"]["old_file_name"] = $params_arr["old_msd_bank_account_proof"];
                    $file_data["msd_bank_account_proof"]["primary_key"] = "iMstStoreDetailId";
                    $file_data["msd_other_proof"]["file_name"] = $msd_other_proof;
                    $file_data["msd_other_proof"]["old_file_name"] = $params_arr["old_msd_other_proof"];
                    $file_data["msd_other_proof"]["primary_key"] = "iMstStoreDetailId";

                    $this->listing->uploadFilesOnSaveForm($file_data, $form_config, $save_data_arr);
                    break;
            }

            $params_arr = $this->_request_params();

            $ret_arr['message'] = $msg;
            $ret_arr['success'] = 1;
            $ret_arr['return_url'] = $url;
        } catch(Exception $e) {
            $ret_arr['message'] = $e->getMessage();
            $ret_arr['success'] = 0;
        }
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }
}
