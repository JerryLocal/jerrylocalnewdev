<?php
/**
 * Description of Store Management Model
 *
 * @module Store Management
 *
 * @class model_storemanagement.php
 *
 * @path application\admin\storemanagement\models\model_storemanagement.php
 *
 * @author Steve Smith
 *
 * @date 05.10.2015
 */

class Model_storemanagement extends CI_Model {
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $multi_lingual;
    public $physical_data_remove;
    //
    public $listing_data;
    public $rec_per_page;
    public $message;

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->table_name = "mst_store_detail";
        $this->table_alias = "msd";
        $this->primary_key = "iMstStoreDetailId";
        $this->primary_alias = "msd_mst_store_detail_id";
        $this->physical_data_remove = "Yes";
        $this->grid_fields = array(
            "ma1_name",
            "msd_store_name",
            "msd_company_name",
            "msd_contact_name",
            "msd_address1",
        );
        $this->join_tables = array(
            array(
                "table_name" => "mod_country",
                "table_alias" => "mc",
                "field_name" => "iCountryId",
                "rel_table_name" => "mst_store_detail",
                "rel_table_alias" => "msd",
                "rel_field_name" => "iCountryId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mod_state",
                "table_alias" => "ms",
                "field_name" => "iStateId",
                "rel_table_name" => "mst_store_detail",
                "rel_table_alias" => "msd",
                "rel_field_name" => "iStateId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mod_city",
                "table_alias" => "mc1",
                "field_name" => "iCityId",
                "rel_table_name" => "mst_store_detail",
                "rel_table_alias" => "msd",
                "rel_field_name" => "iCityId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mod_admin",
                "table_alias" => "ma1",
                "field_name" => "iAdminId",
                "rel_table_name" => "mst_store_detail",
                "rel_table_alias" => "msd",
                "rel_field_name" => "iAdminId",
                "join_type" => "left",
                "extra_condition" => "",
            )
        );
        $this->extra_cond = "";
        $this->groupby_cond = array();
        $this->having_cond = "";
        $this->orderby_cond = array();
        $this->unique_type = "OR";
        $this->unique_fields = array();
        $this->switchto_fields = array();
        $this->default_filters = array();
        $this->search_config = array();
        $this->relation_modules = array();
        $this->deletion_modules = array();
        $this->print_rec = "No";
        $this->multi_lingual = "No";

        $this->rec_per_page = $this->config->item('REC_LIMIT');
    }

    /**
     * insert method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    public function insert($data = array()) {
        $this->db->insert($this->table_name, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    /**
     * update method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while updating records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function update($data = array(), $where = '', $alias = "No", $join = "No") {
        if ($alias == "Yes") {
            if ($join == "Yes") {
                $join_tbls = $this->addJoinTables("NR");
            }
            if (trim($join_tbls) != '') {
                $set_cond = array();
                foreach ($data as $key => $val) {
                    $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                }
                if (is_numeric($where)) {
                    $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                } elseif ($where) {
                    $extra_cond = " WHERE ".$where;
                } else {
                    return FALSE;
                }
                $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
                $res = $this->db->query($update_query);
            } else {
                if (is_numeric($where)) {
                    $this->db->where($this->table_alias.".".$this->primary_key, $where);
                } elseif ($where) {
                    $this->db->where($where, FALSE, FALSE);
                } else {
                    return FALSE;
                }
                $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
            }
        } else {
            if (is_numeric($where)) {
                $this->db->where($this->primary_key, $where);
            } elseif ($where) {
                $this->db->where($where, FALSE, FALSE);
            } else {
                return FALSE;
            }
            $res = $this->db->update($this->table_name, $data);
        }
        return $res;
    }

    /**
     * delete method is used to delete data records from the database table.
     * @param string $where where is the query condition for deletion.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while deleting records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function delete($where = "", $alias = "No", $join = "No") {
        if ($this->config->item('PHYSICAL_RECORD_DELETE') && $this->physical_data_remove == 'No') {
            if ($alias == "Yes") {
                if (is_array($join['joins']) && count($join['joins'])) {
                    $join_tbls = '';
                    if ($join['list'] == "Yes") {
                        $join_tbls = $this->addJoinTables("NR");
                    }
                    $join_tbls .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                } elseif ($join == "Yes") {
                    $join_tbls = $this->addJoinTables("NR");
                }
                $data = $this->general->getPhysicalRecordUpdate($this->table_alias);
                if (trim($join_tbls) != '') {
                    $set_cond = array();
                    foreach ($data as $key => $val) {
                        $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                    }
                    if (is_numeric($where)) {
                        $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                    } elseif ($where) {
                        $extra_cond = " WHERE ".$where;
                    } else {
                        return FALSE;
                    }
                    $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
                    $res = $this->db->query($update_query);
                } else {
                    if (is_numeric($where)) {
                        $this->db->where($this->table_alias.".".$this->primary_key, $where);
                    } elseif ($where) {
                        $this->db->where($where, FALSE, FALSE);
                    } else {
                        return FALSE;
                    }
                    $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
                }
            } else {
                if (is_numeric($where)) {
                    $this->db->where($this->primary_key, $where);
                } elseif ($where) {
                    $this->db->where($where, FALSE, FALSE);
                } else {
                    return FALSE;
                }
                $data = $this->general->getPhysicalRecordUpdate();
                $res = $this->db->update($this->table_name, $data);
            }
        } else {
            if ($alias == "Yes") {
                $del_query = "DELETE ".$this->db->protect($this->table_alias).".* FROM ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias);
                if (is_array($join['joins']) && count($join['joins'])) {
                    if ($join['list'] == "Yes") {
                        $del_query .= $this->addJoinTables("NR");
                    }
                    $del_query .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                } elseif ($join == "Yes") {
                    $del_query .= $this->addJoinTables("NR");
                }
                if (is_numeric($where)) {
                    $del_query .= " WHERE ".$this->db->protect($this->table_alias).".".$this->db->protect($this->primary_key)." = ".$this->db->escape($where);
                } elseif ($where) {
                    $del_query .= " WHERE ".$where;
                } else {
                    return FALSE;
                }
                $res = $this->db->query($del_query);
            } else {
                if (is_numeric($where)) {
                    $this->db->where($this->primary_key, $where);
                } elseif ($where) {
                    $this->db->where($where, FALSE, FALSE);
                } else {
                    return FALSE;
                }
                $res = $this->db->delete($this->table_name);
            }
        }
        return $res;
    }

    /**
     * getData method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @param boolean $having_cond having cond is the query condition for getting conditional data.
     * @param boolean $list list is to differ listing fields or form fields.
     * @return array $data_arr returns data records array.
     */
    public function getData($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No", $having_cond = '', $list = FALSE) {
        if (is_array($fields)) {
            $this->listing->addSelectFields($fields);
        } elseif ($fields != "") {
            $this->db->select($fields);
        } elseif ($list == TRUE) {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
            if ($this->primary_alias != "") {
                $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
            }
            $this->db->select("ma1.vName AS ma1_name");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("msd.vCompanyName AS msd_company_name");
            $this->db->select("msd.vContactName AS msd_contact_name");
            $this->db->select("msd.vAddress1 AS msd_address1");
        } else {
            $this->db->select("msd.iMstStoreDetailId AS iMstStoreDetailId");
            $this->db->select("msd.iMstStoreDetailId AS msd_mst_store_detail_id");
            $this->db->select("ma1.iAdminId AS iAdminId");
            $this->db->select("msd.vStoreCode AS msd_store_code");
            $this->db->select("msd.vStoreLogo AS msd_store_logo");
            $this->db->select("msd.tStoreDescription AS msd_store_description");
            $this->db->select("ma1.vEmail AS ma1_email");
            $this->db->select("ma1.vPassword AS ma1_password");
            $this->db->select("msd.iAdminId AS msd_admin_id");
            $this->db->select("msd.fStoreRateAvg AS msd_store_rate_avg");
            $this->db->select("msd.iStoreTotalReview AS msd_store_total_review");
            $this->db->select("msd.eStatus AS msd_status");
            $this->db->select("msd.dDate AS msd_date");
            $this->db->select("msd.dModifyDate AS msd_modify_date");
            $this->db->select("ma1.vName AS ma1_name");
            $this->db->select("ma1.vUserName AS ma1_user_name");
            $this->db->select("ma1.vPhonenumber AS ma1_phonenumber");
            $this->db->select("ma1.iGroupId AS ma1_group_id");
            $this->db->select("ma1.dLastAccess AS ma1_last_access");
            $this->db->select("ma1.eStatus AS ma1_status");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("msd.vCompanyName AS msd_company_name");
            $this->db->select("msd.vContactName AS msd_contact_name");
            $this->db->select("msd.vContactNumber AS msd_contact_number");
            $this->db->select("msd.vAddress1 AS msd_address1");
            $this->db->select("msd.vAddress2 AS msd_address2");
            $this->db->select("msd.vArea AS msd_area");
            $this->db->select("msd.iCountryId AS msd_country_id");
            $this->db->select("msd.iStateId AS msd_state_id");
            $this->db->select("msd.iCityId AS msd_city_id");
            $this->db->select("msd.vPinCode AS msd_pin_code");
            $this->db->select("msd.vWebsiteUrl AS msd_website_url");
            $this->db->select("msd.iMstCategoriesId AS msd_mst_categories_id");
            $this->db->select("msd.eHaveOnlineStore AS msd_have_online_store");
            $this->db->select("msd.vStoreUrl AS msd_store_url");
            $this->db->select("msd.vBankAccountName AS msd_bank_account_name");
            $this->db->select("msd.vBankAccountNo AS msd_bank_account_no");
            $this->db->select("msd.vGSTNo AS msd_g_s_t_no");
            $this->db->select("msd.vGSTIRDProof AS msd_g_s_t_i_r_d_proof");
            $this->db->select("msd.vBankAccountProof AS msd_bank_account_proof");
            $this->db->select("msd.vOtherProof AS msd_other_proof");
            $this->db->select("msd.vOtherProofDetail AS msd_other_proof_detail");
            $this->db->select("msd.eAcceptTerm AS msd_accept_term");
        }

        $this->db->from($this->table_name." AS ".$this->table_alias);
        if (is_array($join) && is_array($join['joins']) && count($join['joins']) > 0) {
            $this->listing->addJoinTables($join['joins']);
            $this->addJoinTables("AR", array("ma1"));
            if ($join["list"] == "Yes") {
                $this->addJoinTables("AR", array("mc", "ms", "mc1"));
            }
        } else {
            $this->addJoinTables("AR", array("ma1"));
            if ($join == "Yes") {
                $this->addJoinTables("AR", array("mc", "ms", "mc1"));
            }
        }
        if (is_array($extra_cond) && count($extra_cond) > 0) {
            $this->listing->addWhereFields($extra_cond);
        } elseif (is_numeric($extra_cond)) {
            $this->db->where($this->table_alias.".".$this->primary_key, intval($extra_cond));
        } elseif ($extra_cond) {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if ($group_by != "") {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "") {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        if ($order_by != "") {
            $this->db->order_by($order_by);
        }
        if ($limit != "") {
            if (is_numeric($limit)) {
                $this->db->limit($limit);
            } else {
                list($offset, $limit) = @explode(",", $limit);
                $this->db->limit($offset, $limit);
            }
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }

    /**
     * getListingData method is used to get grid listing data records for this module.
     * @param array $config_arr config_arr for grid listing settigs.
     * @return array $listing_data returns data records array for grid.
     */
    public function getListingData($config_arr = array()) {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $rec_per_page = (intval($rows) > 0) ? intval($rows) : $this->rec_per_page;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->start_cache();
        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "") {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0) {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "") {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;
        $filter_config['grid_fields'] = $this->grid_fields;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "") {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "") {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "") {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "") {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("ma1.vName AS ma1_name");
        $this->db->select("msd.vStoreName AS msd_store_name");
        $this->db->select("msd.vCompanyName AS msd_company_name");
        $this->db->select("msd.vContactName AS msd_contact_name");
        $this->db->select("msd.vAddress1 AS msd_address1");

        $this->db->stop_cache();
        if ((is_array($group_by) && count($group_by) > 0) || trim($having_cond) != "") {
            $this->db->select($this->table_alias.".".$this->primary_key);
            $total_records_arr = $this->db->get();
            $total_records = is_object($total_records_arr) ? $total_records_arr->num_rows() : 0;
        } else {
            $total_records = $this->db->count_all_results();
        }

        $total_pages = $this->listing->getTotalPages($total_records, $rec_per_page);
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0) {
            foreach ($order_by as $orK => $orV) {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "") {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        $limit_offset = $this->listing->getStartIndex($total_records, $page, $rec_per_page);
        $this->db->limit($rec_per_page, $limit_offset);
        $return_data_obj = $this->db->get();
        $return_data = is_object($return_data_obj) ? $return_data_obj->result_array() : array();
        $this->db->flush_cache();
        $listing_data = $this->listing->getDataForJqGrid($return_data, $filter_config, $page, $total_pages, $total_records);
        $this->listing_data = $return_data;
        #echo $this->db->last_query();
        return $listing_data;
    }

    /**
     * getExportData method is used to get grid export data records for this module.
     * @param array $config_arr config_arr for grid export settigs.
     * @return array $export_data returns data records array for export.
     */
    public function getExportData($config_arr = array()) {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $rowlimit = $config_arr['rowlimit'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "") {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0) {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "") {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "") {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "") {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "") {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "") {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("ma1.vName AS ma1_name");
        $this->db->select("msd.vStoreName AS msd_store_name");
        $this->db->select("msd.vCompanyName AS msd_company_name");
        $this->db->select("msd.vContactName AS msd_contact_name");
        $this->db->select("msd.vAddress1 AS msd_address1");
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0) {
            foreach ($order_by as $orK => $orV) {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "") {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        if ($rowlimit != "") {
            $offset = $rowlimit;
            $limit = ($rowlimit*$page-$rowlimit);
            $this->db->limit($offset, $limit);
        }
        $export_data_obj = $this->db->get();
        $export_data = is_object($export_data_obj) ? $export_data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $export_data;
    }

    /**
     * addJoinTables method is used to make relation tables joins with main table.
     * @param string $type type is to get active record or join string.
     * @param boolean $allow_tables allow_table is to restrict some set of tables.
     * @return string $ret_joins returns relation tables join string.
     */
    public function addJoinTables($type = 'AR', $allow_tables = FALSE) {
        $join_tables = $this->join_tables;
        if (!is_array($join_tables) || count($join_tables) == 0) {
            return '';
        }
        $ret_joins = $this->listing->addJoinTables($join_tables, $type, $allow_tables);
        return $ret_joins;
    }

    /**
     * getListConfiguration method is used to get listing configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns listing configuration array.
     */
    public function getListConfiguration($name = "") {
        $list_config = array(
            "ma1_name" => array(
                "name" => "ma1_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma1",
                "field_name" => "vName",
                "source_field" => "ma1_name",
                "display_query" => "ma1.vName",
                "entry_type" => "Table",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Name",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_store_name" => array(
                "name" => "msd_store_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreName",
                "source_field" => "msd_store_name",
                "display_query" => "msd.vStoreName",
                "entry_type" => "Table",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Store Name",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_STORE_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
                "edit_link" => "Yes",
            ),
            "msd_company_name" => array(
                "name" => "msd_company_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vCompanyName",
                "source_field" => "msd_company_name",
                "display_query" => "msd.vCompanyName",
                "entry_type" => "Table",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Company Name",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_COMPANY_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_contact_name" => array(
                "name" => "msd_contact_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vContactName",
                "source_field" => "msd_contact_name",
                "display_query" => "msd.vContactName",
                "entry_type" => "Table",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Contact Name",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_CONTACT_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_address1" => array(
                "name" => "msd_address1",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAddress1",
                "source_field" => "msd_address1",
                "display_query" => "msd.vAddress1",
                "entry_type" => "Table",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Address1",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_ADDRESS1'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0) {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++) {
                $config_arr[$name[$i]] = $list_config[$name[$i]];
            }
        } elseif ($name != "" && is_string($name)) {
            $config_arr = $list_config[$name];
        } else {
            $config_arr = $list_config;
        }
        return $config_arr;
    }

    /**
     * getFormConfiguration method is used to get form configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns form configuration array.
     */
    public function getFormConfiguration($name = "") {
        $form_config = array(
            "msd_store_code" => array(
                "name" => "msd_store_code",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreCode",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Store Code",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_STORE_CODE')
            ),
            "msd_store_logo" => array(
                "name" => "msd_store_logo",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreLogo",
                "entry_type" => "Table",
                "type" => "file",
                "label" => "Store Logo",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_STORE_LOGO'),
                "file_upload" => "Yes",
                "file_server" => "local",
                "file_folder" => "store_logo",
                "file_width" => "50",
                "file_height" => "50",
                "file_format" => "gif,png,jpg,jpeg",
                "file_size" => "2048",
            ),
            "msd_store_description" => array(
                "name" => "msd_store_description",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "tStoreDescription",
                "entry_type" => "Table",
                "type" => "textarea",
                "label" => "Store Description",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_STORE_DESCRIPTION')
            ),
            "ma1_email" => array(
                "name" => "ma1_email",
                "table_name" => "mod_admin",
                "table_alias" => "ma1",
                "field_name" => "vEmail",
                "entry_type" => "Relation",
                "type" => "textbox",
                "label" => "Email",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_EMAIL')
            ),
            "ma1_password" => array(
                "name" => "ma1_password",
                "table_name" => "mod_admin",
                "table_alias" => "ma1",
                "field_name" => "vPassword",
                "entry_type" => "Relation",
                "type" => "password",
                "label" => "Password",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_PASSWORD')
            ),
            "msd_admin_id" => array(
                "name" => "msd_admin_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iAdminId",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Admin Id",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_ADMIN_ID'),
                "show_input" => "Update",
            ),
            "msd_store_rate_avg" => array(
                "name" => "msd_store_rate_avg",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "fStoreRateAvg",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Store Rate Avg",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_STORE_RATE_AVG'),
                "show_input" => "Update",
            ),
            "msd_store_total_review" => array(
                "name" => "msd_store_total_review",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iStoreTotalReview",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Store Total Review",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_STORE_TOTAL_REVIEW'),
                "show_input" => "Update",
            ),
            "msd_status" => array(
                "name" => "msd_status",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eStatus",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Status",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_STATUS'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("msd_status",
                "Text",
                "Active")
            ),
            "msd_date" => array(
                "name" => "msd_date",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "dDate",
                "entry_type" => "Table",
                "type" => "date",
                "label" => "Date",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_DATE'),
                "show_input" => "Update",
                "format" => $this->general->getAdminPHPFormats('date')
            ),
            "msd_modify_date" => array(
                "name" => "msd_modify_date",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "dModifyDate",
                "entry_type" => "Table",
                "type" => "date",
                "label" => "Modify Date",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_MODIFY_DATE'),
                "show_input" => "Update",
                "format" => $this->general->getAdminPHPFormats('date')
            ),
            "ma1_name" => array(
                "name" => "ma1_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma1",
                "field_name" => "vName",
                "entry_type" => "Relation",
                "type" => "textbox",
                "label" => "Name",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_NAME'),
                "show_input" => "Update",
            ),
            "ma1_user_name" => array(
                "name" => "ma1_user_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma1",
                "field_name" => "vUserName",
                "entry_type" => "Relation",
                "type" => "textbox",
                "label" => "User Name",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_USER_NAME'),
                "show_input" => "Update",
            ),
            "ma1_phonenumber" => array(
                "name" => "ma1_phonenumber",
                "table_name" => "mod_admin",
                "table_alias" => "ma1",
                "field_name" => "vPhonenumber",
                "entry_type" => "Relation",
                "type" => "textbox",
                "label" => "Phonenumber",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_PHONENUMBER'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("ma1_phonenumber",
                "MySQL",
                "NULL")
            ),
            "ma1_group_id" => array(
                "name" => "ma1_group_id",
                "table_name" => "mod_admin",
                "table_alias" => "ma1",
                "field_name" => "iGroupId",
                "entry_type" => "Relation",
                "type" => "textbox",
                "label" => "Group Id",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_GROUP_ID'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("ma1_group_id",
                "MySQL",
                "3")
            ),
            "ma1_last_access" => array(
                "name" => "ma1_last_access",
                "table_name" => "mod_admin",
                "table_alias" => "ma1",
                "field_name" => "dLastAccess",
                "entry_type" => "Relation",
                "type" => "textbox",
                "label" => "Last Access",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_LAST_ACCESS'),
                "show_input" => "Update",
            ),
            "ma1_status" => array(
                "name" => "ma1_status",
                "table_name" => "mod_admin",
                "table_alias" => "ma1",
                "field_name" => "eStatus",
                "entry_type" => "Relation",
                "type" => "textbox",
                "label" => "Status",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_STATUS'),
                "show_input" => "Update",
                "default" => $this->filter->getDefaultValue("ma1_status",
                "Text",
                "Active")
            ),
            "msd_store_name" => array(
                "name" => "msd_store_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreName",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Store Name",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_STORE_NAME')
            ),
            "msd_company_name" => array(
                "name" => "msd_company_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vCompanyName",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Company Name",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_COMPANY_NAME')
            ),
            "msd_contact_name" => array(
                "name" => "msd_contact_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vContactName",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Contact Name",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_CONTACT_NAME')
            ),
            "msd_contact_number" => array(
                "name" => "msd_contact_number",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vContactNumber",
                "entry_type" => "Table",
                "type" => "phone_number",
                "label" => "Contact Number",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_CONTACT_NUMBER'),
                "format" => $this->general->getAdminPHPFormats('phone')
            ),
            "msd_address1" => array(
                "name" => "msd_address1",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAddress1",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Address1",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_ADDRESS1')
            ),
            "msd_address2" => array(
                "name" => "msd_address2",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAddress2",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Address2",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_ADDRESS2')
            ),
            "msd_area" => array(
                "name" => "msd_area",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vArea",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Area",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_AREA')
            ),
            "msd_country_id" => array(
                "name" => "msd_country_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iCountryId",
                "entry_type" => "Table",
                "type" => "dropdown",
                "label" => "Country",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_COUNTRY')
            ),
            "msd_state_id" => array(
                "name" => "msd_state_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iStateId",
                "entry_type" => "Table",
                "type" => "dropdown",
                "label" => "State",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_STATE')
            ),
            "msd_city_id" => array(
                "name" => "msd_city_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iCityId",
                "entry_type" => "Table",
                "type" => "dropdown",
                "label" => "City",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_CITY')
            ),
            "msd_pin_code" => array(
                "name" => "msd_pin_code",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vPinCode",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Pin Code",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_PIN_CODE')
            ),
            "msd_website_url" => array(
                "name" => "msd_website_url",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vWebsiteUrl",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Company Website URL",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_COMPANY_WEBSITE_URL')
            ),
            "msd_mst_categories_id" => array(
                "name" => "msd_mst_categories_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iMstCategoriesId",
                "entry_type" => "Table",
                "type" => "dropdown",
                "label" => "Primary Category",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_PRIMARY_CATEGORY')
            ),
            "msd_have_online_store" => array(
                "name" => "msd_have_online_store",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eHaveOnlineStore",
                "entry_type" => "Table",
                "type" => "radio_buttons",
                "label" => "Do you sell product online already?",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_DO_YOU_SELL_PRODUCT_ONLINE_ALREADY_C63')
            ),
            "msd_store_url" => array(
                "name" => "msd_store_url",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreUrl",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Store Url",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_STORE_URL')
            ),
            "msd_bank_account_name" => array(
                "name" => "msd_bank_account_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountName",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "  Bank Account Name (NZ Only)",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NAME__C40NZ_ONLY_C41')
            ),
            "sys_custom_field_39" => array(
                "name" => "sys_custom_field_39",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_39",
                "entry_type" => "Custom",
                "type" => "textbox",
                "label" => "Bank Account No",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NO')
            ),
            "sys_custom_field_40" => array(
                "name" => "sys_custom_field_40",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_40",
                "entry_type" => "Custom",
                "type" => "textbox",
                "label" => "Bank Ac-2",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_BANK_AC_C452')
            ),
            "sys_custom_field_41" => array(
                "name" => "sys_custom_field_41",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_41",
                "entry_type" => "Custom",
                "type" => "textbox",
                "label" => "Bank Ac-3",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_BANK_AC_C453')
            ),
            "sys_custom_field_42" => array(
                "name" => "sys_custom_field_42",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_42",
                "entry_type" => "Custom",
                "type" => "textbox",
                "label" => "Bank Ac-4",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_BANK_AC_C454')
            ),
            "msd_bank_account_no" => array(
                "name" => "msd_bank_account_no",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountNo",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Bank Account No",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_BANK_ACCOUNT_NO'),
                "show_input" => "Update",
            ),
            "msd_g_s_t_no" => array(
                "name" => "msd_g_s_t_no",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vGSTNo",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "G S T No",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_G_S_T_NO'),
                "show_input" => "Update",
            ),
            "sys_custom_field_43" => array(
                "name" => "sys_custom_field_43",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_43",
                "entry_type" => "Custom",
                "type" => "textbox",
                "label" => "G S T No-1",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_G_S_T_NO_C451')
            ),
            "sys_custom_field_44" => array(
                "name" => "sys_custom_field_44",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_44",
                "entry_type" => "Custom",
                "type" => "textbox",
                "label" => "G S T No-2",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_G_S_T_NO_C452')
            ),
            "sys_custom_field_45" => array(
                "name" => "sys_custom_field_45",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysCustomField_45",
                "entry_type" => "Custom",
                "type" => "textbox",
                "label" => "G S T No-3",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_G_S_T_NO_C453')
            ),
            "msd_g_s_t_i_r_d_proof" => array(
                "name" => "msd_g_s_t_i_r_d_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vGSTIRDProof",
                "entry_type" => "Table",
                "type" => "file",
                "label" => "Upload GST/IRD Detail Proof",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_UPLOAD_GST_C47IRD_DETAIL_PROOF'),
                "file_upload" => "Yes",
                "file_server" => "local",
                "file_folder" => "GST_Proof",
                "file_width" => "50",
                "file_height" => "50",
                "file_format" => "gif,png,jpg,jpeg",
                "file_size" => "2048",
            ),
            "msd_bank_account_proof" => array(
                "name" => "msd_bank_account_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountProof",
                "entry_type" => "Table",
                "type" => "file",
                "label" => "Upload Bank Account Proof",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_UPLOAD_BANK_ACCOUNT_PROOF'),
                "file_upload" => "Yes",
                "file_server" => "local",
                "file_folder" => "BankAccount_proof",
                "file_width" => "50",
                "file_height" => "50",
                "file_format" => "gif,png,jpg,jpeg",
                "file_size" => "2048",
            ),
            "msd_other_proof" => array(
                "name" => "msd_other_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vOtherProof",
                "entry_type" => "Table",
                "type" => "file",
                "label" => "Upload Any Other Proof",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_UPLOAD_ANY_OTHER_PROOF'),
                "file_upload" => "Yes",
                "file_server" => "local",
                "file_folder" => "other_proof",
                "file_width" => "50",
                "file_height" => "50",
                "file_format" => "gif,png,jpg,jpeg",
                "file_size" => "2048",
            ),
            "msd_other_proof_detail" => array(
                "name" => "msd_other_proof_detail",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vOtherProofDetail",
                "entry_type" => "Table",
                "type" => "textbox",
                "label" => "Other Proof Detail",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_OTHER_PROOF_DETAIL')
            ),
            "msd_accept_term" => array(
                "name" => "msd_accept_term",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eAcceptTerm",
                "entry_type" => "Table",
                "type" => "checkboxes",
                "label" => "Accept Term",
                "label_lang" => $this->lang->line('STOREMANAGEMENT_ACCEPT_TERM')
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0) {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++) {
                $config_arr[$name[$i]] = $form_config[$name[$i]];
            }
        } elseif ($name != "" && is_string($name)) {
            $config_arr = $form_config[$name];
        } else {
            $config_arr = $form_config;
        }
        return $config_arr;
    }

    /**
     * checkRecordExists method is used to check duplication of records.
     * @param array $field_arr field_arr is having fields to check.
     * @param array $field_val field_val is having values of respective fields.
     * @param numeric $id id is to avoid current records.
     * @param string $mode mode is having either Add or Update.
     * @param string $con con is having either AND or OR.
     * @return boolean $exists returns either TRUE of FALSE.
     */
    public function checkRecordExists($field_arr = array(), $field_val = array(), $id = '', $mode = '', $con = 'AND') {
        $exists = FALSE;
        if (!is_array($field_arr) || count($field_arr) == 0) {
            return $exists;
        }
        foreach ((array) $field_arr as $key => $val) {
            $extra_cond_arr[] = $this->db->protect($this->table_alias.".".$field_arr[$key])." =  ".$this->db->escape($field_val[$val]);
        }
        $extra_cond = "(".@implode(" ".$con." ", $extra_cond_arr).")";
        if ($mode == "Add") {
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0) {
                $exists = TRUE;
            }
        } elseif ($mode == "Update") {
            $extra_cond = $this->db->protect($this->table_alias.".".$this->primary_key)." <> ".$this->db->escape($id)." AND ".$extra_cond;
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0) {
                $exists = TRUE;
            }
        }
        return $exists;
    }

    /**
     * getSwitchTo method is used to get switch to dropdown array.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @return array $switch_data returns data records array.
     */
    public function getSwitchTo($extra_cond = '', $type = 'records', $limit = '') {
        $switchto_fields = $this->switchto_fields;
        $switch_data = array();
        if (!is_array($switchto_fields) || count($switchto_fields) == 0) {
            if ($type == "count") {
                return count($switch_data);
            } else {
                return $switch_data;
            }
        }
        $fields_arr = array();
        $fields_arr[] = array(
            "field" => $this->table_alias.".".$this->primary_key." AS id",
        );
        $fields_arr[] = array(
            "field" => $this->db->concat($switchto_fields)." AS val",
            "escape" => TRUE,
        );
        if (trim($this->extra_cond) != "") {
            $extra_cond = (trim($extra_cond) != "") ? $extra_cond." AND ".$this->extra_cond : $this->extra_cond;
        }
        $switch_data = $this->getData($extra_cond, $fields_arr, "val ASC", "", $limit, "Yes");
        #echo $this->db->last_query();
        if ($type == "count") {
            return count($switch_data);
        } else {
            return $switch_data;
        }
    }

    /**
     * relationData method is used to get data records from relation table.
     * @param string $table_alias table_alias is to get specific relation table data.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are comma seperated values.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @return array $data_arr returns relation data records array.
     */
    function relationData($table_alias = '', $extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "") {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_alias);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        if (is_array($fields)) {
            $this->listing->addSelectFields($fields);
        } elseif ($fields != "") {
            $this->db->select($fields);
        } else {
            $this->db->select($table_info['table_alias'].".*");
        }
        $this->db->from($table_info['table_name']." AS ".$table_info['table_alias']);
        if (is_array($extra_cond) && count($extra_cond) > 0) {
            $this->listing->addWhereFields($extra_cond);
        } elseif ($extra_cond != "") {
            if (is_numeric($extra_cond)) {
                $this->db->where($table_info['table_alias'].".".$table_info['primary_key'], $extra_cond);
            } else {
                $this->db->where($extra_cond, FALSE, FALSE);
            }
        }
        $this->general->getPhysicalRecordWhere($table_info['table_name'], $table_info['table_alias'], "AR");
        if ($group_by != "") {
            $this->db->group_by($group_by);
        }
        if ($order_by != "") {
            $this->db->order_by($order_by);
        }
        if ($limit != "") {
            list($offset, $limit) = @explode(",", $limit);
            $this->db->limit($offset, $limit);
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }
    /**
     * relationInsert method is used to insert data records to the relation table.
     * @param string $table_alias table_alias is to insert data into specific relation table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    function relationInsert($table_alias = '', $data = array()) {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_alias);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        $this->db->insert($table_name, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    /**
     * relationUpdate method is used to update data records to the relation table.
     * @param string $table_alias table_alias is to update data into specific relation table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @param string $join join is to make joins with main table.
     * @return boolean $res returns TRUE or FALSE.
     */
    function relationUpdate($table_alias = '', $data = array(), $where = '', $join = 'No') {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_alias);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        if ($join == "Yes") {
            $set_cond = array();
            foreach ($data as $key => $val) {
                $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
            }
            $join_tbls = $this->listing->addJoinTables(array($table_info), "NR");
            if (is_numeric($where)) {
                $extra_cond = "WHERE ".$this->db->protect($table_info['primary_key'])." = ".$this->db->escape($where);
            } elseif ($where != '') {
                $extra_cond = "WHERE ".$where;
            } else {
                return FALSE;
            }
            $update_query = "UPDATE ".$this->db->protect($table_name)." AS ".$this->db->protect($table_info['table_alias'])." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
            $res = $this->db->query($update_query);
        } else {
            if (is_numeric($where)) {
                $this->db->where($table_info['primary_key'], $where);
            } elseif ($where != '') {
                $this->db->where($where, FALSE, FALSE);
            } else {
                return FALSE;
            }
            $res = $this->db->update($table_name, $data);
        }
        return $res;
    }
    /**
     * relationDelete method is used to delete data records from relation table.
     * @param string $table_alias table_alias is to update data into specific relation table.
     * @param string $where where is the query condition for deleting.
     * @return boolean $res returns TRUE or FALSE.
     */
    function relationDelete($table_alias = '', $where = '') {
        if ($table_alias == "") {
            return FALSE;
        }
        $table_info = $this->relationDetails($table_name);
        $table_name = $table_info['table_name'];
        if ($table_name == "") {
            return FALSE;
        }
        if (is_numeric($where)) {
            $this->db->where($table_info['primary_key'], $where);
        } else {
            $this->db->where($where, FALSE, FALSE);
        }
        $res = $this->db->delete($table_name);
        return $res;
    }
    /**
     * relationDetails method is used to get relation table detail.
     * @param string $table_alias table_alias is to get data of specific relation table.
     * @return array $ret_arr returns relation table detail array.
     */
    function relationDetails($table_alias = '') {
        $relation_config = array(
            "ma1" => array(
                "table_name" => "mod_admin",
                "table_alias" => "ma1",
                "field_name" => "iAdminId",
                "primary_key" => "iAdminId",
                "rel_table_name" => "mst_store_detail",
                "rel_table_alias" => "msd",
                "rel_field_name" => "iAdminId",
                "extra_condition" => "",
            )
        );
        $ret_arr = $relation_config[$table_alias];
        return $ret_arr;
    }
}
