<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<textarea id="txt_module_help" class="perm-elem-hide"></textarea>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line('GENERIC_LISTING')%> :: <%$this->lang->line('PENDINGREFUND_REFUND_REQUESTS_ALL')%>
                <div class="right">
                    <a style="color:#778695;font-size:14px;" hijacked="yes" href="<%$this->config->item('upload_url')%>Refund_Process_Guide_01.pdf" target="_blank" title="Refund Process Guide" class="nav-active-link"><span class="icon12 icomoon-icon-help"></span>Refund Process Guide</a>
                </div>                
            </div>        
        </h3>
        <div class="header-right-drops">
        </div>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing box gradient">
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div id="scrollable_content" class="scrollable-content top-list-spacing">
        <div class="grid-data-container pad-calc-container">
            <div class="top-list-tab-layout" id="top_list_grid_layout">
            </div>
            <table class="grid-table-view " width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td id="grid_data_col">
                        <div id="pager2"></div>
                        <table id="list2"></table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>        
<input type="hidden" name="selAllRows" value="" id="selAllRows" />
<%javascript%>
    $.jgrid.no_legacy_api = true; $.jgrid.useJSON = true;
    var el_grid_settings = {}, js_col_model_json = {}, js_col_name_json = {}; 
                    
    el_grid_settings['module_name'] = '<%$module_name%>';
    el_grid_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_grid_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_grid_settings['enc_location'] = '<%$enc_loc_module%>';
    el_grid_settings['par_module '] = '<%$this->general->getAdminEncodeURL($parMod)%>';
    el_grid_settings['par_data'] = '<%$this->general->getAdminEncodeURL($parID)%>';
    el_grid_settings['par_field'] = '<%$parField%>';
    el_grid_settings['par_type'] = 'parent';
    el_grid_settings['add_page_url'] = '<%$mod_enc_url["add"]%>'; 
    el_grid_settings['edit_page_url'] =  admin_url+'<%$mod_enc_url["inline_edit_action"]%>?<%$extra_qstr%>';
    el_grid_settings['listing_url'] = admin_url+'<%$mod_enc_url["listing"]%>?<%$extra_qstr%>';
    el_grid_settings['search_refresh_url'] = admin_url+'<%$mod_enc_url["get_left_search_content"]%>?<%$extra_qstr%>';
    el_grid_settings['search_autocomp_url'] = admin_url+'<%$mod_enc_url["get_search_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['auto_complete_url'] = admin_url+'<%$mod_enc_url["get_token_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['export_url'] =  admin_url+'<%$mod_enc_url["export"]%>?<%$extra_qstr%>';
    el_grid_settings['subgrid_listing_url'] =  admin_url+'<%$mod_enc_url["get_subgrid_block"]%>?<%$extra_qstr%>';
    el_grid_settings['jparent_switchto_url'] = admin_url+'<%$parent_switch_cit["url"]%>?<%$extra_qstr%>';
    
    el_grid_settings['admin_rec_arr'] = {};
    el_grid_settings['status_arr'] = $.parseJSON('<%$status_array|@json_encode%>');
    el_grid_settings['status_lang_arr'] = $.parseJSON('<%$status_array_label|@json_encode%>');
                
    el_grid_settings['hide_add_btn'] = '';
    el_grid_settings['hide_del_btn'] = '';
    el_grid_settings['hide_status_btn'] = '1';
    el_grid_settings['hide_export_btn'] = '1';
    el_grid_settings['hide_columns_btn'] = 'No';
    
    el_grid_settings['hide_advance_search'] = 'No';
    el_grid_settings['hide_search_tool'] = 'No';
    el_grid_settings['hide_multi_select'] = 'No';
    el_grid_settings['popup_add_form'] = 'No';
    el_grid_settings['popup_edit_form'] = 'No';
    el_grid_settings['popup_add_size'] = ['75%', '75%'];
    el_grid_settings['popup_edit_size'] = ['75%', '75%'];
    el_grid_settings['hide_paging_btn'] = 'No';
    el_grid_settings['hide_refresh_btn'] = 'Yes';
    el_grid_settings['group_search'] = '';
    
    el_grid_settings['permit_add_btn'] = '<%$add_access%>';
    el_grid_settings['permit_del_btn'] = '<%$del_access%>';
    el_grid_settings['permit_edit_btn'] = '<%$view_access%>';
    el_grid_settings['permit_expo_btn'] = '<%$expo_access%>';
    
    el_grid_settings['default_sort'] = 'msd_store_name';
    el_grid_settings['sort_order'] = 'asc';
    
    el_grid_settings['footer_row'] = 'No';
    el_grid_settings['grouping'] = 'No';
    el_grid_settings['group_attr'] = {};
    
    el_grid_settings['inline_add'] = 'No';
    el_grid_settings['rec_position'] = 'Top';
    el_grid_settings['auto_width'] = 'Yes';
    el_grid_settings['subgrid'] = 'Yes';
    el_grid_settings['colgrid'] = 'No';
    el_grid_settings['rating_allow'] = 'No';
    el_grid_settings['listview'] = 'list';
    el_grid_settings['filters_arr'] = $.parseJSON('<%$default_filters|@json_encode%>');
    el_grid_settings['top_filter'] = [];
    el_grid_settings['buttons_arr'] = [];
    
    js_col_name_json = [{
        "name": "msd_store_name",
        "label": "<%$this->lang->line('PENDINGREFUND_STORE_NAME')%>"
    },
    {
        "name": "sys_custom_field_2",
        "label": "<%$this->lang->line('PENDINGREFUND_ORDER_NO')%>"
    },
    {
        "name": "trr_trn_rma_id",
        "label": "<%$this->lang->line('PENDINGREFUND_RETURN_ID')%>"
    },
    {
        "name": "mso_total_cost",
        "label": "<%$this->lang->line('PENDINGREFUND_ORDER_TOTAL')%>"
    },
    {
        "name": "trr_total_amount",
        "label": "<%$this->lang->line('PENDINGREFUND_REFUND_AMT_C46')%>"
    },
    {
        "name": "trr_date",
        "label": "<%$this->lang->line('PENDINGREFUND_REQUEST_DATE')%>"
    },
    {
        "name": "mo_date",
        "label": "<%$this->lang->line('PENDINGREFUND_ORDER_DATE')%>"
    },
    {
        "name": "trr_type",
        "label": "<%$this->lang->line('PENDINGREFUND_TYPE')%>"
    },
    {
        "name": "mso_sub_order_number",
        "label": "<%$this->lang->line('PENDINGREFUND_SUB_ORDER_NO')%>"
    }];
    
    js_col_model_json = [{
        "name": "msd_store_name",
        "index": "msd_store_name",
        "label": "<%$list_config['msd_store_name']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['msd_store_name']['width']%>",
        "search": <%if $list_config['msd_store_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['msd_store_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['msd_store_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['msd_store_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['msd_store_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "pendingrefund",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['msd_store_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "pendingrefund",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['msd_store_name']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "sys_custom_field_2",
        "index": "sys_custom_field_2",
        "label": "<%$list_config['sys_custom_field_2']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['sys_custom_field_2']['width']%>",
        "search": <%if $list_config['sys_custom_field_2']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['sys_custom_field_2']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['sys_custom_field_2']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['sys_custom_field_2']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['sys_custom_field_2']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "pendingrefund",
                "aria-unique-name": "sys_custom_field_2",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['sys_custom_field_2']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "pendingrefund",
            "aria-unique-name": "sys_custom_field_2",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['sys_custom_field_2']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "trr_trn_rma_id",
        "index": "trr_trn_rma_id",
        "label": "<%$list_config['trr_trn_rma_id']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['trr_trn_rma_id']['width']%>",
        "search": <%if $list_config['trr_trn_rma_id']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_trn_rma_id']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_trn_rma_id']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_trn_rma_id']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_trn_rma_id']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "pendingrefund",
                "aria-unique-name": "trr_trn_rma_id",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['trr_trn_rma_id']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["trr_trn_rma_id"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=trr_trn_rma_id&mode=<%$mod_enc_mode["Search"]%>&rformat=html'<%/if%>,
            "value": <%if $count_arr["trr_trn_rma_id"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["trr_trn_rma_id"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['trr_trn_rma_id']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["trr_trn_rma_id"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "pendingrefund",
            "aria-unique-name": "trr_trn_rma_id",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=trr_trn_rma_id&mode=<%$mod_enc_mode["Update"]%>&rformat=html',
            "dataInit": <%if $count_arr['trr_trn_rma_id']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["trr_trn_rma_id"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PENDINGREFUND_RETURN_ID')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['trr_trn_rma_id']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "mso_total_cost",
        "index": "mso_total_cost",
        "label": "<%$list_config['mso_total_cost']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_total_cost']['width']%>",
        "search": <%if $list_config['mso_total_cost']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_total_cost']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_total_cost']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_total_cost']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_total_cost']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "pendingrefund",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mso_total_cost']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "pendingrefund",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_total_cost']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency"
    },
    {
        "name": "trr_total_amount",
        "index": "trr_total_amount",
        "label": "<%$list_config['trr_total_amount']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['trr_total_amount']['width']%>",
        "search": <%if $list_config['trr_total_amount']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_total_amount']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_total_amount']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_total_amount']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_total_amount']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "pendingrefund",
                "aria-unique-name": "trr_total_amount",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['trr_total_amount']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "pendingrefund",
            "aria-unique-name": "trr_total_amount",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['trr_total_amount']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency"
    },
    {
        "name": "trr_date",
        "index": "trr_date",
        "label": "<%$list_config['trr_date']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['trr_date']['width']%>",
        "search": <%if $list_config['trr_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "pendingrefund",
                "aria-unique-name": "trr_date",
                "autocomplete": "off",
                "class": "search-inline-date",
                "aria-date-format": "<%$this->general->getAdminJSMoments('date')%>"
            },
            "sopt": dateSearchOpts,
            "searchhidden": <%if $list_config['trr_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchGridDateRangePicker
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "pendingrefund",
            "aria-unique-name": "trr_date",
            "aria-date-format": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
            "aria-min": "",
            "aria-max": "",
            "placeholder": "",
            "class": "inline-edit-row inline-date-edit date-picker-icon dateOnly"
        },
        "ctrl_type": "date",
        "default_value": "<%$list_config['trr_date']['default']%>",
        "filterSopt": "bt"
    },
    {
        "name": "mo_date",
        "index": "mo_date",
        "label": "<%$list_config['mo_date']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['mo_date']['width']%>",
        "search": <%if $list_config['mo_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "pendingrefund",
                "aria-unique-name": "mo_date",
                "autocomplete": "off",
                "class": "search-inline-date",
                "aria-date-format": "<%$this->general->getAdminJSMoments('date')%>"
            },
            "sopt": dateSearchOpts,
            "searchhidden": <%if $list_config['mo_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchGridDateRangePicker
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "pendingrefund",
            "aria-unique-name": "mo_date",
            "aria-date-format": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
            "aria-min": "",
            "aria-max": "",
            "placeholder": "",
            "class": "inline-edit-row inline-date-edit date-picker-icon dateOnly"
        },
        "ctrl_type": "date",
        "default_value": "<%$list_config['mo_date']['default']%>",
        "filterSopt": "bt"
    },
    {
        "name": "trr_type",
        "index": "trr_type",
        "label": "<%$list_config['trr_type']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['trr_type']['width']%>",
        "search": <%if $list_config['trr_type']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_type']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_type']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_type']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_type']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "pendingrefund",
                "aria-unique-name": "trr_type",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['trr_type']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["trr_type"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=trr_type&mode=<%$mod_enc_mode["Search"]%>&rformat=html'<%/if%>,
            "value": <%if $count_arr["trr_type"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["trr_type"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['trr_type']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["trr_type"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "pendingrefund",
            "aria-unique-name": "trr_type",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=trr_type&mode=<%$mod_enc_mode["Update"]%>&rformat=html',
            "dataInit": <%if $count_arr['trr_type']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["trr_type"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PENDINGREFUND_TYPE')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['trr_type']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "mso_sub_order_number",
        "index": "mso_sub_order_number",
        "label": "<%$list_config['mso_sub_order_number']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mso_sub_order_number']['width']%>",
        "search": <%if $list_config['mso_sub_order_number']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_sub_order_number']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_sub_order_number']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_sub_order_number']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_sub_order_number']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "pendingrefund",
                "aria-unique-name": "mso_sub_order_number",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mso_sub_order_number']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "pendingrefund",
            "aria-unique-name": "mso_sub_order_number",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_sub_order_number']['default']%>",
        "filterSopt": "bw"
    }];
         
    initMainGridListing();
    createTooltipHeading();
    callSwitchToParent();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 