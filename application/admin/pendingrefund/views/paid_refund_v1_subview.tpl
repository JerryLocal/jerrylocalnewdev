<%javascript%>
    $.fn.editable.defaults.mode = 'inline', $.fn.editable.defaults.clear = false;
    var el_subview_settings = {}, view_js_col_model_json = {}, view_token_input_assign = {}, view_token_pre_populates = {};
      
    el_subview_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_subview_settings['extra_qstr'] = '<%$extra_qstr%>';
                    
    el_subview_settings['view_id'] = '<%$subgrid_view_id%>';
    el_subview_settings['edit_id'] = '<%$enc_view_primary_id%>';
    el_subview_settings['module_name'] = '<%$module_name%>';
            
    el_subview_settings['edit_page_url'] = admin_url+'<%$mod_enc_url["inline_edit_action"]%>?&oper=edit&<%$extra_qstr%>';
    el_subview_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_subview_settings['permit_edit_btn'] = '<%$edit_access%>';

    view_js_col_model_json =  {
        "gv_mso_product_sku": {
            "htmlID": "gv_mso_product_sku",
            "name": "mso_product_sku",
            "label": "SKU",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_SKU')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mso_product_sku']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_product_sku']['value']%>",
            "dbval": "<%$list_config['mso_product_sku']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "infoArr": []
            },
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_custom_order_item_id": {
            "htmlID": "gv_custom_order_item_id",
            "name": "custom_order_item_id",
            "label": "Order Item ID",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_ORDER_ITEM_ID')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['custom_order_item_id']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['custom_order_item_id']['value']%>",
            "dbval": "<%$list_config['custom_order_item_id']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mso_product_qty": {
            "htmlID": "gv_mso_product_qty",
            "name": "mso_product_qty",
            "label": "Qty",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_QTY')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['mso_product_qty']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_product_qty']['value']%>",
            "dbval": "<%$list_config['mso_product_qty']['dbval']%>",
            "edittype": "select",
            "editrules": {
                "infoArr": []
            },
            "editoptions": {
                "ajaxCall": '<%if $count_arr['mso_product_qty']['ajax'] eq 'Yes' %>ajax-call<%/if%>',
                "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=mso_product_qty&id=<%$enc_view_primary_id%>&par_field=<%$exp_par_field%>&par_data=<%$exp_par_id%>&mode=<%$mod_enc_mode["Update"]%>&rformat=json',
                "rel": "mso_product_qty",
                "data_placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_QTY')%>"
            }
        },
        "gv_mso_mst_order_id": {
            "htmlID": "gv_mso_mst_order_id",
            "name": "mso_mst_order_id",
            "label": "Order ID",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_ORDER_ID')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mso_mst_order_id']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_mst_order_id']['value']%>",
            "dbval": "<%$list_config['mso_mst_order_id']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mso_product_price": {
            "htmlID": "gv_mso_product_price",
            "name": "mso_product_price",
            "label": "Price",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_PRICE')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mso_product_price']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_product_price']['value']%>",
            "dbval": "<%$list_config['mso_product_price']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "infoArr": []
            },
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mso_shipping_cost": {
            "htmlID": "gv_mso_shipping_cost",
            "name": "mso_shipping_cost",
            "label": "Shipping",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mso_shipping_cost']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_shipping_cost']['value']%>",
            "dbval": "<%$list_config['mso_shipping_cost']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "infoArr": []
            },
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mso_total_cost": {
            "htmlID": "gv_mso_total_cost",
            "name": "mso_total_cost",
            "label": "Total",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_TOTAL')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mso_total_cost']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_total_cost']['value']%>",
            "dbval": "<%$list_config['mso_total_cost']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "infoArr": []
            },
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mo_buyer_name": {
            "htmlID": "gv_mo_buyer_name",
            "name": "mo_buyer_name",
            "label": "Buyer Name",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_NAME')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mo_buyer_name']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mo_buyer_name']['value']%>",
            "dbval": "<%$list_config['mo_buyer_name']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mo_buyer_phone": {
            "htmlID": "gv_mo_buyer_phone",
            "name": "mo_buyer_phone",
            "label": "Buyer Phone",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_PHONE')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['mo_buyer_phone']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mo_buyer_phone']['value']%>",
            "dbval": "<%$list_config['mo_buyer_phone']['dbval']%>",
            "edittype": "select",
            "editrules": {
                "infoArr": []
            },
            "editoptions": {
                "ajaxCall": '<%if $count_arr['mo_buyer_phone']['ajax'] eq 'Yes' %>ajax-call<%/if%>',
                "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=mo_buyer_phone&id=<%$enc_view_primary_id%>&par_field=<%$exp_par_field%>&par_data=<%$exp_par_id%>&mode=<%$mod_enc_mode["Update"]%>&rformat=json',
                "rel": "mso_mst_order_id",
                "data_placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_BUYER_PHONE')%>"
            }
        },
        "gv_mo_buyer_email": {
            "htmlID": "gv_mo_buyer_email",
            "name": "mo_buyer_email",
            "label": "Buyer Email",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_EMAIL')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mo_buyer_email']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mo_buyer_email']['value']%>",
            "dbval": "<%$list_config['mo_buyer_email']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mo_buyer_area": {
            "htmlID": "gv_mo_buyer_area",
            "name": "mo_buyer_area",
            "label": "Buyer Area",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_AREA')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mo_buyer_area']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mo_buyer_area']['value']%>",
            "dbval": "<%$list_config['mo_buyer_area']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mo_buyer_address2": {
            "htmlID": "gv_mo_buyer_address2",
            "name": "mo_buyer_address2",
            "label": "Buyer Address2",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_ADDRESS2')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mo_buyer_address2']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mo_buyer_address2']['value']%>",
            "dbval": "<%$list_config['mo_buyer_address2']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mo_buyer_address1": {
            "htmlID": "gv_mo_buyer_address1",
            "name": "mo_buyer_address1",
            "label": "Buyer Address1",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_ADDRESS1')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mo_buyer_address1']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mo_buyer_address1']['value']%>",
            "dbval": "<%$list_config['mo_buyer_address1']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mc1_country": {
            "htmlID": "gv_mc1_country",
            "name": "mc1_country",
            "label": "Country",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_COUNTRY')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mc1_country']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mc1_country']['value']%>",
            "dbval": "<%$list_config['mc1_country']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_ms_state": {
            "htmlID": "gv_ms_state",
            "name": "ms_state",
            "label": "State",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_STATE')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['ms_state']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['ms_state']['value']%>",
            "dbval": "<%$list_config['ms_state']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mc2_city": {
            "htmlID": "gv_mc2_city",
            "name": "mc2_city",
            "label": "City",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_CITY')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mc2_city']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mc2_city']['value']%>",
            "dbval": "<%$list_config['mc2_city']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mso_seller_invoice_date": {
            "htmlID": "gv_mso_seller_invoice_date",
            "name": "mso_seller_invoice_date",
            "label": "Invoice Date",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_INVOICE_DATE')%>",
            "type": "date",
            "ctrl_type": "date",
            "editable": <%if $list_config['mso_seller_invoice_date']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_seller_invoice_date']['value']%>",
            "dbval": "<%$list_config['mso_seller_invoice_date']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "infoArr": []
            },
            "editoptions": {
                "dateFormat": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
                "minDate": "",
                "maxDate": "",
                "placeholder": ""
            }
        },
        "gv_mo_buyer_pin_code": {
            "htmlID": "gv_mo_buyer_pin_code",
            "name": "mo_buyer_pin_code",
            "label": "Buyer Pin Code",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_PIN_CODE')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mo_buyer_pin_code']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mo_buyer_pin_code']['value']%>",
            "dbval": "<%$list_config['mo_buyer_pin_code']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mso_seller_invoice_no": {
            "htmlID": "gv_mso_seller_invoice_no",
            "name": "mso_seller_invoice_no",
            "label": "Invoice No",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_INVOICE_NO')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mso_seller_invoice_no']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_seller_invoice_no']['value']%>",
            "dbval": "<%$list_config['mso_seller_invoice_no']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "infoArr": []
            },
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mso_shipping_date": {
            "htmlID": "gv_mso_shipping_date",
            "name": "mso_shipping_date",
            "label": "Shipping Date",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING_DATE')%>",
            "type": "date",
            "ctrl_type": "date",
            "editable": <%if $list_config['mso_shipping_date']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_shipping_date']['value']%>",
            "dbval": "<%$list_config['mso_shipping_date']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "infoArr": []
            },
            "editoptions": {
                "dateFormat": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
                "minDate": "",
                "maxDate": "",
                "placeholder": ""
            }
        },
        "gv_mso_shipper_name": {
            "htmlID": "gv_mso_shipper_name",
            "name": "mso_shipper_name",
            "label": "Courier Company",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_COURIER_COMPANY')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mso_shipper_name']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_shipper_name']['value']%>",
            "dbval": "<%$list_config['mso_shipper_name']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "infoArr": []
            },
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mso_tracking_number": {
            "htmlID": "gv_mso_tracking_number",
            "name": "mso_tracking_number",
            "label": "Tracking No",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_TRACKING_NO')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mso_tracking_number']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_tracking_number']['value']%>",
            "dbval": "<%$list_config['mso_tracking_number']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "infoArr": []
            },
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mso_shipping_remark": {
            "htmlID": "gv_mso_shipping_remark",
            "name": "mso_shipping_remark",
            "label": "IMEI\/SL No",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_IMEI_C47SL_NO')%>",
            "type": "textarea",
            "ctrl_type": "textarea",
            "editable": <%if $list_config['mso_shipping_remark']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_shipping_remark']['value']%>",
            "dbval": "<%$list_config['mso_shipping_remark']['dbval']%>",
            "edittype": "textarea",
            "editrules": {
                "infoArr": []
            },
            "editoptions": {
                "case": "",
                "placeholder": ""
            }
        },
        "gv_mso_delivered_date": {
            "htmlID": "gv_mso_delivered_date",
            "name": "mso_delivered_date",
            "label": "Delivered Date",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_DELIVERED_DATE')%>",
            "type": "date",
            "ctrl_type": "date",
            "editable": <%if $list_config['mso_delivered_date']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_delivered_date']['value']%>",
            "dbval": "<%$list_config['mso_delivered_date']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "infoArr": []
            },
            "editoptions": {
                "dateFormat": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
                "minDate": "",
                "maxDate": "",
                "placeholder": ""
            }
        },
        "gv_ma_name": {
            "htmlID": "gv_ma_name",
            "name": "ma_name",
            "label": "Seller Name",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_SELLER_NAME')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['ma_name']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['ma_name']['value']%>",
            "dbval": "<%$list_config['ma_name']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mso_cancel_date": {
            "htmlID": "gv_mso_cancel_date",
            "name": "mso_cancel_date",
            "label": "Cancel Date",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_CANCEL_DATE')%>",
            "type": "date",
            "ctrl_type": "date",
            "editable": <%if $list_config['mso_cancel_date']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_cancel_date']['value']%>",
            "dbval": "<%$list_config['mso_cancel_date']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "infoArr": []
            },
            "editoptions": {
                "dateFormat": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
                "minDate": "",
                "maxDate": "",
                "placeholder": ""
            }
        },
        "gv_mso_cancel_detail": {
            "htmlID": "gv_mso_cancel_detail",
            "name": "mso_cancel_detail",
            "label": "Reason for cancellation",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_REASON_FOR_CANCELLATION')%>",
            "type": "textarea",
            "ctrl_type": "textarea",
            "editable": <%if $list_config['mso_cancel_detail']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_cancel_detail']['value']%>",
            "dbval": "<%$list_config['mso_cancel_detail']['dbval']%>",
            "edittype": "textarea",
            "editrules": {
                "infoArr": []
            },
            "editoptions": {
                "case": "",
                "placeholder": ""
            }
        },
        "gv_mso_action_taken_by": {
            "htmlID": "gv_mso_action_taken_by",
            "name": "mso_action_taken_by",
            "label": "Cancelled By",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_CANCELLED_BY')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['mso_action_taken_by']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_action_taken_by']['value']%>",
            "dbval": "<%$list_config['mso_action_taken_by']['dbval']%>",
            "edittype": "select",
            "editrules": {
                "infoArr": []
            },
            "editoptions": {
                "ajaxCall": '<%if $count_arr['mso_action_taken_by']['ajax'] eq 'Yes' %>ajax-call<%/if%>',
                "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=mso_action_taken_by&id=<%$enc_view_primary_id%>&par_field=<%$exp_par_field%>&par_data=<%$exp_par_id%>&mode=<%$mod_enc_mode["Update"]%>&rformat=json',
                "rel": "mso_action_taken_by",
                "data_placeholder": null
            }
        },
        "gv_sys_custom_field_2": {
            "htmlID": "gv_sys_custom_field_2",
            "name": "sys_custom_field_2",
            "label": "Buyer Address",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_ADDRESS')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['sys_custom_field_2']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['sys_custom_field_2']['value']%>",
            "dbval": "<%$list_config['sys_custom_field_2']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        },
        "gv_mso_product_name": {
            "htmlID": "gv_mso_product_name",
            "name": "mso_product_name",
            "label": "Product Name",
            "label_lang": "<%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_NAME')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['mso_product_name']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mso_product_name']['value']%>",
            "dbval": "<%$list_config['mso_product_name']['dbval']%>",
            "addon_apply": false,
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "addon_pos": "",
            "addon_html": "",
            "extra_class": ""
        }
    };
    
    initSubGridDetailView();
<%/javascript%>
    
<div class="expand-detail-view">                        
    <table id="detail_view_block" class="jqgrid-subview" width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_SKU')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_product_sku']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_QTY')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_product_qty']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_NAME')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mo_buyer_name']%></td>
        </tr>
        <tr>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_NAME')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_product_name']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_PRICE')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_product_price']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_PHONE')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mo_buyer_phone']%></td>
        </tr>
        <tr>
            <td valign="top" width="12%"><strong>Sub Order No: </strong></td>
            <td valign="top" width="18%"><%$data['custom_order_item_id']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_shipping_cost']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_EMAIL')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mo_buyer_email']%></td>
        </tr>
        <tr>
            <td valign="top" width="12%"><strong>Order No: </strong></td>
            <td valign="top" width="18%"><%$data['mso_mst_order_id']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_TOTAL')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_total_cost']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_BUYER_ADDRESS')%>: </strong></td>
            <td valign="top" width="18%"><%$data['sys_custom_field_2']%></td>
        </tr>
    </table>
    <br/>
    <b>&nbsp;&nbsp;Shipping and invoice details:-</b>
    <br/>
    <table id="detail_view_block" class="jqgrid-subview" width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_INVOICE_NO')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_seller_invoice_no']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_COURIER_COMPANY')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_shipper_name']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_INVOICE_DATE')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_seller_invoice_date']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_TRACKING_NO')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_tracking_number']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong>Remarks: </strong></td>
            <td valign="top" width="27%"><%$data['mso_shipping_remark']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong>Shipped Date: </strong></td>
            <td valign="top" width="27%"><%$data['mso_shipping_date']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETORDEREXPANDEDVIEW_DELIVERED_DATE')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_delivered_date']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"></strong></td>
            <td valign="top" width="27%"></td> 
        </tr>
    </table>
</div>