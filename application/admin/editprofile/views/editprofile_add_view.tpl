<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('EDITPROFILE_EDIT_PROFILE')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','EDITPROFILE_EDIT_PROFILE')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="editprofile" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="editprofile" class="frm-view-block frm-stand-view">
            <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
            <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
            <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
            <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
            <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
            <input type="hidden" name="mp_admin_id" id="mp_admin_id" value="<%$data['mp_admin_id']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="ma_name" id="ma_name" value="<%$data['ma_name']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="ma_group_id" id="ma_group_id" value="<%$data['ma_group_id']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="ma_last_access" id="ma_last_access" value="<%$data['ma_last_access']%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
            <div class="main-content-block" id="main_content_block">
                <div style="width:98%;" class="frm-block-layout pad-calc-container">
                    <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                        <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('EDITPROFILE_EDIT_PROFILE')%></h4></div>
                        <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                            <div class="form-row row-fluid" id="cc_sh_mp_first_name">
                                <label class="form-label span3">
                                    <%$this->lang->line('EDITPROFILE_FIRST_NAME')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_first_name']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_dob">
                                <label class="form-label span3">
                                    <%$this->lang->line('EDITPROFILE_DATE_OF_BIRTH')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
                                    <strong><%$this->general->dateSystemFormat($data['mp_dob'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_gender">
                                <label class="form-label span3">
                                    <%$this->lang->line('EDITPROFILE_GENDER')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mp_gender'], $opt_arr['mp_gender'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_bank_name">
                                <label class="form-label span3">
                                    <%$this->lang->line('EDITPROFILE_BANK_NAME')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_bank_name']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_bank_account_no">
                                <label class="form-label span3">
                                    <%$this->lang->line('EDITPROFILE_BANK_ACCOUNT_NO')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_bank_account_no']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_ma_email">
                                <label class="form-label span3">
                                    <%$this->lang->line('EDITPROFILE_EMAIL')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['ma_email']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_ma_user_name">
                                <label class="form-label span3">
                                    <%$this->lang->line('EDITPROFILE_USER_NAME')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['ma_user_name']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_ma_password">
                                <label class="form-label span3">
                                    <%$this->lang->line('EDITPROFILE_PASSWORD')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    *****
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_ma_phonenumber">
                                <label class="form-label span3">
                                    <%$this->lang->line('EDITPROFILE_PHONENUMBER')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['ma_phonenumber']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_ma_status">
                                <label class="form-label span3">
                                    <%$this->lang->line('EDITPROFILE_STATUS')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['ma_status'], $opt_arr['ma_status'])%></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>        
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
    callSwitchToSelf();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
