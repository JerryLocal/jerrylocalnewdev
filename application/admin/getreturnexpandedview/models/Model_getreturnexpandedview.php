<?php
/**
 * Description of Get Return Expanded View Model
 *
 * @module Get Return Expanded View
 *
 * @class model_getreturnexpandedview.php
 *
 * @path application\admin\getreturnexpandedview\models\model_getreturnexpandedview.php
 *
 * @author CIT Dev Team
 *
 * @date 08.01.2016
 */

class Model_getreturnexpandedview extends CI_Model
{
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $multi_lingual;
    public $physical_data_remove;
    //
    public $listing_data;
    public $rec_per_page;
    public $message;

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->table_name = "trn_rma";
        $this->table_alias = "tr";
        $this->primary_key = "iTrnRmaId";
        $this->primary_alias = "tr_trn_rma_id";
        $this->physical_data_remove = "Yes";
        $this->grid_fields = array(
            "mso_product_sku",
            "mo_buyer_name",
            "mo_buyer_phone",
            "mso_mst_sub_order_id",
            "mo_buyer_email",
            "mo_buyer_area",
            "mo_buyer_address2",
            "mo_buyer_address1",
            "tr_request_img",
            "mso_seller_invoice_date",
            "mso_shipping_date",
            "mso_delivered_date",
            "mo_mst_order_id",
            "mso_tracking_number",
            "tr_request_detail",
            "mso_shipper_name",
            "mc1_country",
            "ms_state",
            "mc2_city",
            "mso_product_price",
            "mso_product_qty",
            "mso_shipping_cost",
            "mso_total_cost",
            "mo_buyer_pin_code",
            "mso_seller_invoice_no",
            "mso_product_name",
            "mso_shipping_remark",
            "sys_custom_field_2",
            "custom_reason_id",
        );
        $this->join_tables = array(
            array(
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iMstStoreDetailId",
                "rel_table_name" => "trn_rma",
                "rel_table_alias" => "tr",
                "rel_field_name" => "iMstStoreDetailId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "iMstOrderId",
                "rel_table_name" => "trn_rma",
                "rel_table_alias" => "tr",
                "rel_field_name" => "iOrderId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iMstSubOrderId",
                "rel_table_name" => "trn_rma",
                "rel_table_alias" => "tr",
                "rel_field_name" => "iSubOrderId",
                "join_type" => "left",
                "extra_condition" => "",
            )
        );
        $this->extra_cond = "";
        $this->groupby_cond = array();
        $this->having_cond = "";
        $this->orderby_cond = array();
        $this->unique_type = "OR";
        $this->unique_fields = array();
        $this->switchto_fields = array();
        $this->default_filters = array();
        $this->search_config = array();
        $this->relation_modules = array();
        $this->deletion_modules = array();
        $this->print_rec = "No";
        $this->multi_lingual = "No";

        $this->rec_per_page = $this->config->item('REC_LIMIT');
    }

    /**
     * insert method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    public function insert($data = array())
    {
        $this->db->insert($this->table_name, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    /**
     * update method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while updating records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function update($data = array(), $where = '', $alias = "No", $join = "No")
    {
        if ($alias == "Yes")
        {
            if ($join == "Yes")
            {
                $join_tbls = $this->addJoinTables("NR");
            }
            if (trim($join_tbls) != '')
            {
                $set_cond = array();
                foreach ($data as $key => $val)
                {
                    $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                }
                if (is_numeric($where))
                {
                    $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                }
                elseif ($where)
                {
                    $extra_cond = " WHERE ".$where;
                }
                else
                {
                    return FALSE;
                }
                $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
                $res = $this->db->query($update_query);
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->table_alias.".".$this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
            }
        }
        else
        {
            if (is_numeric($where))
            {
                $this->db->where($this->primary_key, $where);
            }
            elseif ($where)
            {
                $this->db->where($where, FALSE, FALSE);
            }
            else
            {
                return FALSE;
            }
            $res = $this->db->update($this->table_name, $data);
        }
        return $res;
    }

    /**
     * delete method is used to delete data records from the database table.
     * @param string $where where is the query condition for deletion.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while deleting records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function delete($where = "", $alias = "No", $join = "No")
    {
        if ($this->config->item('PHYSICAL_RECORD_DELETE') && $this->physical_data_remove == 'No')
        {
            if ($alias == "Yes")
            {
                if (is_array($join['joins']) && count($join['joins']))
                {
                    $join_tbls = '';
                    if ($join['list'] == "Yes")
                    {
                        $join_tbls = $this->addJoinTables("NR");
                    }
                    $join_tbls .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                }
                elseif ($join == "Yes")
                {
                    $join_tbls = $this->addJoinTables("NR");
                }
                $data = $this->general->getPhysicalRecordUpdate($this->table_alias);
                if (trim($join_tbls) != '')
                {
                    $set_cond = array();
                    foreach ($data as $key => $val)
                    {
                        $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                    }
                    if (is_numeric($where))
                    {
                        $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                    }
                    elseif ($where)
                    {
                        $extra_cond = " WHERE ".$where;
                    }
                    else
                    {
                        return FALSE;
                    }
                    $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
                    $res = $this->db->query($update_query);
                }
                else
                {
                    if (is_numeric($where))
                    {
                        $this->db->where($this->table_alias.".".$this->primary_key, $where);
                    }
                    elseif ($where)
                    {
                        $this->db->where($where, FALSE, FALSE);
                    }
                    else
                    {
                        return FALSE;
                    }
                    $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
                }
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $data = $this->general->getPhysicalRecordUpdate();
                $res = $this->db->update($this->table_name, $data);
            }
        }
        else
        {
            if ($alias == "Yes")
            {
                $del_query = "DELETE ".$this->db->protect($this->table_alias).".* FROM ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias);
                if (is_array($join['joins']) && count($join['joins']))
                {
                    if ($join['list'] == "Yes")
                    {
                        $del_query .= $this->addJoinTables("NR");
                    }
                    $del_query .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                }
                elseif ($join == "Yes")
                {
                    $del_query .= $this->addJoinTables("NR");
                }
                if (is_numeric($where))
                {
                    $del_query .= " WHERE ".$this->db->protect($this->table_alias).".".$this->db->protect($this->primary_key)." = ".$this->db->escape($where);
                }
                elseif ($where)
                {
                    $del_query .= " WHERE ".$where;
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->query($del_query);
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->delete($this->table_name);
            }
        }
        return $res;
    }

    /**
     * getData method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @param boolean $having_cond having cond is the query condition for getting conditional data.
     * @param boolean $list list is to differ listing fields or form fields.
     * @return array $data_arr returns data records array.
     */
    public function getData($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No", $having_cond = '', $list = FALSE)
    {
        if (is_array($fields))
        {
            $this->listing->addSelectFields($fields);
        }
        elseif ($fields != "")
        {
            $this->db->select($fields);
        }
        elseif ($list == TRUE)
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
            if ($this->primary_alias != "")
            {
                $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
            }
            $this->db->select("mso.vProductSku AS mso_product_sku");
            $this->db->select("mo.vBuyerName AS mo_buyer_name");
            $this->db->select("mo.vBuyerPhone AS mo_buyer_phone");
            $this->db->select("(CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber)) AS mso_mst_sub_order_id", FALSE);
            $this->db->select("mo.vBuyerEmail AS mo_buyer_email");
            $this->db->select("mo.vBuyerArea AS mo_buyer_area");
            $this->db->select("mo.vBuyerAddress2 AS mo_buyer_address2");
            $this->db->select("mo.vBuyerAddress1 AS mo_buyer_address1");
            $this->db->select("tr.vRequestImg AS tr_request_img");
            $this->db->select("mso.vSellerInvoiceDate AS mso_seller_invoice_date");
            $this->db->select("mso.dShippingDate AS mso_shipping_date");
            $this->db->select("mso.dDeliveredDate AS mso_delivered_date");
            $this->db->select("(CONCAT_WS('',mo.vOrderNumberPre,mo.iOrderNumber)) AS mo_mst_order_id", FALSE);
            $this->db->select("mso.vTrackingNumber AS mso_tracking_number");
            $this->db->select("tr.tRequestDetail AS tr_request_detail");
            $this->db->select("mso.vShipperName AS mso_shipper_name");
            $this->db->select("mo.vBuyerCountry AS mc1_country");
            $this->db->select("mo.vBuyerState AS ms_state");
            $this->db->select("mo.vBuyerCity AS mc2_city");
            $this->db->select("mso.fProductPrice AS mso_product_price");
            $this->db->select("mso.iProductQty AS mso_product_qty");
            $this->db->select("mso.fShippingCost AS mso_shipping_cost");
            $this->db->select("mso.fTotalCost AS mso_total_cost");
            $this->db->select("mo.vBuyerPinCode AS mo_buyer_pin_code");
            $this->db->select("mso.vSellerInvoiceNo AS mso_seller_invoice_no");
            $this->db->select("(IF(mso.vProductName !='' AND mso.tProductOption !='' ,CONCAT(mso.vProductName,' (',mso.tProductOption,')'),mso.vProductName)) AS mso_product_name", FALSE);
            $this->db->select("mso.tShippingRemark AS mso_shipping_remark");
            $this->db->select("('mc.vEmail') AS sys_custom_field_2", FALSE);
            $this->db->select("(tr.iTrnRmaId) AS custom_reason_id", FALSE);
        }
        else
        {
            $this->db->select("tr.iTrnRmaId AS iTrnRmaId");
            $this->db->select("tr.iTrnRmaId AS tr_trn_rma_id");
            $this->db->select("tr.vRequestImg AS tr_request_img");
            $this->db->select("tr.tRequestDetail AS tr_request_detail");
            $this->db->select("tr.dRequestDate AS tr_request_date");
            $this->db->select("tr.iCustomerId AS tr_customer_id");
            $this->db->select("tr.iSellerId AS tr_seller_id");
            $this->db->select("tr.iMstStoreDetailId AS tr_mst_store_detail_id");
            $this->db->select("tr.iSubOrderId AS tr_sub_order_id");
            $this->db->select("tr.iOrderId AS tr_order_id");
            $this->db->select("tr.ePreference AS tr_preference");
            $this->db->select("tr.eRquestType AS tr_rquest_type");
        }

        $this->db->from($this->table_name." AS ".$this->table_alias);
        if (is_array($join) && is_array($join['joins']) && count($join['joins']) > 0)
        {
            $this->listing->addJoinTables($join['joins']);
            if ($join["list"] == "Yes")
            {
                $this->addJoinTables("AR");
            }
        }
        else
        {
            if ($join == "Yes")
            {
                $this->addJoinTables("AR");
            }
        }
        if (is_array($extra_cond) && count($extra_cond) > 0)
        {
            $this->listing->addWhereFields($extra_cond);
        }
        elseif (is_numeric($extra_cond))
        {
            $this->db->where($this->table_alias.".".$this->primary_key, intval($extra_cond));
        }
        elseif ($extra_cond)
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if ($group_by != "")
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        if ($order_by != "")
        {
            $this->db->order_by($order_by);
        }
        if ($limit != "")
        {
            if (is_numeric($limit))
            {
                $this->db->limit($limit);
            }
            else
            {
                list($offset, $limit) = @explode(",", $limit);
                $this->db->limit($offset, $limit);
            }
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }

    /**
     * getListingData method is used to get grid listing data records for this module.
     * @param array $config_arr config_arr for grid listing settigs.
     * @return array $listing_data returns data records array for grid.
     */
    public function getListingData($config_arr = array())
    {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $rec_per_page = (intval($rows) > 0) ? intval($rows) : $this->rec_per_page;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->start_cache();
        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "")
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0)
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;
        $filter_config['grid_fields'] = $this->grid_fields;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "")
        {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "")
        {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "")
        {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("mso.vProductSku AS mso_product_sku");
        $this->db->select("mo.vBuyerName AS mo_buyer_name");
        $this->db->select("mo.vBuyerPhone AS mo_buyer_phone");
        $this->db->select("(CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber)) AS mso_mst_sub_order_id", FALSE);
        $this->db->select("mo.vBuyerEmail AS mo_buyer_email");
        $this->db->select("mo.vBuyerArea AS mo_buyer_area");
        $this->db->select("mo.vBuyerAddress2 AS mo_buyer_address2");
        $this->db->select("mo.vBuyerAddress1 AS mo_buyer_address1");
        $this->db->select("tr.vRequestImg AS tr_request_img");
        $this->db->select("mso.vSellerInvoiceDate AS mso_seller_invoice_date");
        $this->db->select("mso.dShippingDate AS mso_shipping_date");
        $this->db->select("mso.dDeliveredDate AS mso_delivered_date");
        $this->db->select("(CONCAT_WS('',mo.vOrderNumberPre,mo.iOrderNumber)) AS mo_mst_order_id", FALSE);
        $this->db->select("mso.vTrackingNumber AS mso_tracking_number");
        $this->db->select("tr.tRequestDetail AS tr_request_detail");
        $this->db->select("mso.vShipperName AS mso_shipper_name");
        $this->db->select("mo.vBuyerCountry AS mc1_country");
        $this->db->select("mo.vBuyerState AS ms_state");
        $this->db->select("mo.vBuyerCity AS mc2_city");
        $this->db->select("mso.fProductPrice AS mso_product_price");
        $this->db->select("mso.iProductQty AS mso_product_qty");
        $this->db->select("mso.fShippingCost AS mso_shipping_cost");
        $this->db->select("mso.fTotalCost AS mso_total_cost");
        $this->db->select("mo.vBuyerPinCode AS mo_buyer_pin_code");
        $this->db->select("mso.vSellerInvoiceNo AS mso_seller_invoice_no");
        $this->db->select("(IF(mso.vProductName !='' AND mso.tProductOption !='' ,CONCAT(mso.vProductName,' (',mso.tProductOption,')'),mso.vProductName)) AS mso_product_name", FALSE);
        $this->db->select("mso.tShippingRemark AS mso_shipping_remark");
        $this->db->select("('mc.vEmail') AS sys_custom_field_2", FALSE);
        $this->db->select("(tr.iTrnRmaId) AS custom_reason_id", FALSE);

        $this->db->stop_cache();
        if ((is_array($group_by) && count($group_by) > 0) || trim($having_cond) != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key);
            $total_records_arr = $this->db->get();
            $total_records = is_object($total_records_arr) ? $total_records_arr->num_rows() : 0;
        }
        else
        {
            $total_records = $this->db->count_all_results();
        }

        $total_pages = $this->listing->getTotalPages($total_records, $rec_per_page);
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0)
        {
            foreach ($order_by as $orK => $orV)
            {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "")
        {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        $limit_offset = $this->listing->getStartIndex($total_records, $page, $rec_per_page);
        $this->db->limit($rec_per_page, $limit_offset);
        $return_data_obj = $this->db->get();
        $return_data = is_object($return_data_obj) ? $return_data_obj->result_array() : array();
        $this->db->flush_cache();
        $listing_data = $this->listing->getDataForJqGrid($return_data, $filter_config, $page, $total_pages, $total_records);
        $this->listing_data = $return_data;
        #echo $this->db->last_query();
        return $listing_data;
    }

    /**
     * getExportData method is used to get grid export data records for this module.
     * @param array $config_arr config_arr for grid export settigs.
     * @return array $export_data returns data records array for export.
     */
    public function getExportData($config_arr = array())
    {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $rowlimit = $config_arr['rowlimit'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "")
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0)
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "")
        {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "")
        {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "")
        {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("mso.vProductSku AS mso_product_sku");
        $this->db->select("mo.vBuyerName AS mo_buyer_name");
        $this->db->select("mo.vBuyerPhone AS mo_buyer_phone");
        $this->db->select("(CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber)) AS mso_mst_sub_order_id", FALSE);
        $this->db->select("mo.vBuyerEmail AS mo_buyer_email");
        $this->db->select("mo.vBuyerArea AS mo_buyer_area");
        $this->db->select("mo.vBuyerAddress2 AS mo_buyer_address2");
        $this->db->select("mo.vBuyerAddress1 AS mo_buyer_address1");
        $this->db->select("tr.vRequestImg AS tr_request_img");
        $this->db->select("mso.vSellerInvoiceDate AS mso_seller_invoice_date");
        $this->db->select("mso.dShippingDate AS mso_shipping_date");
        $this->db->select("mso.dDeliveredDate AS mso_delivered_date");
        $this->db->select("(CONCAT_WS('',mo.vOrderNumberPre,mo.iOrderNumber)) AS mo_mst_order_id", FALSE);
        $this->db->select("mso.vTrackingNumber AS mso_tracking_number");
        $this->db->select("tr.tRequestDetail AS tr_request_detail");
        $this->db->select("mso.vShipperName AS mso_shipper_name");
        $this->db->select("mo.vBuyerCountry AS mc1_country");
        $this->db->select("mo.vBuyerState AS ms_state");
        $this->db->select("mo.vBuyerCity AS mc2_city");
        $this->db->select("mso.fProductPrice AS mso_product_price");
        $this->db->select("mso.iProductQty AS mso_product_qty");
        $this->db->select("mso.fShippingCost AS mso_shipping_cost");
        $this->db->select("mso.fTotalCost AS mso_total_cost");
        $this->db->select("mo.vBuyerPinCode AS mo_buyer_pin_code");
        $this->db->select("mso.vSellerInvoiceNo AS mso_seller_invoice_no");
        $this->db->select("(IF(mso.vProductName !='' AND mso.tProductOption !='' ,CONCAT(mso.vProductName,' (',mso.tProductOption,')'),mso.vProductName)) AS mso_product_name", FALSE);
        $this->db->select("mso.tShippingRemark AS mso_shipping_remark");
        $this->db->select("('mc.vEmail') AS sys_custom_field_2", FALSE);
        $this->db->select("(tr.iTrnRmaId) AS custom_reason_id", FALSE);
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0)
        {
            foreach ($order_by as $orK => $orV)
            {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "")
        {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        if ($rowlimit != "")
        {
            $offset = $rowlimit;
            $limit = ($rowlimit*$page-$rowlimit);
            $this->db->limit($offset, $limit);
        }
        $export_data_obj = $this->db->get();
        $export_data = is_object($export_data_obj) ? $export_data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $export_data;
    }

    /**
     * addJoinTables method is used to make relation tables joins with main table.
     * @param string $type type is to get active record or join string.
     * @param boolean $allow_tables allow_table is to restrict some set of tables.
     * @return string $ret_joins returns relation tables join string.
     */
    public function addJoinTables($type = 'AR', $allow_tables = FALSE)
    {
        $join_tables = $this->join_tables;
        if (!is_array($join_tables) || count($join_tables) == 0)
        {
            return '';
        }
        $ret_joins = $this->listing->addJoinTables($join_tables, $type, $allow_tables);
        return $ret_joins;
    }

    /**
     * getListConfiguration method is used to get listing configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns listing configuration array.
     */
    public function getListConfiguration($name = "")
    {
        $list_config = array(
            "mso_product_sku" => array(
                "name" => "mso_product_sku",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vProductSku",
                "source_field" => "",
                "display_query" => "mso.vProductSku",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "SKU",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_SKU'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "edit_link" => "Yes",
            ),
            "mo_buyer_name" => array(
                "name" => "mo_buyer_name",
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "vBuyerName",
                "source_field" => "",
                "display_query" => "mo.vBuyerName",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Buyer Name",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mo_buyer_phone" => array(
                "name" => "mo_buyer_phone",
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "vBuyerPhone",
                "source_field" => "",
                "display_query" => "mo.vBuyerPhone",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Buyer Phone",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_PHONE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mso_mst_sub_order_id" => array(
                "name" => "mso_mst_sub_order_id",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "",
                "source_field" => "",
                "display_query" => "CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber)",
                "entry_type" => "Custom",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Order Item ID",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_ORDER_ITEM_ID'),
                "width" => 120,
                "search" => "No",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mo_buyer_email" => array(
                "name" => "mo_buyer_email",
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "vBuyerEmail",
                "source_field" => "",
                "display_query" => "mo.vBuyerEmail",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Buyer Email",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_EMAIL'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mo_buyer_area" => array(
                "name" => "mo_buyer_area",
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "vBuyerArea",
                "source_field" => "",
                "display_query" => "mo.vBuyerArea",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Buyer Area",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_AREA'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mo_buyer_address2" => array(
                "name" => "mo_buyer_address2",
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "vBuyerAddress2",
                "source_field" => "",
                "display_query" => "mo.vBuyerAddress2",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Buyer Address2",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_ADDRESS2'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mo_buyer_address1" => array(
                "name" => "mo_buyer_address1",
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "vBuyerAddress1",
                "source_field" => "",
                "display_query" => "mo.vBuyerAddress1",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Buyer Address1",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_ADDRESS1'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "tr_request_img" => array(
                "name" => "tr_request_img",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "vRequestImg",
                "source_field" => "tr_request_img",
                "display_query" => "tr.vRequestImg",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Request Image",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_REQUEST_IMAGE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "file_upload" => "Yes",
                "file_inline" => "Yes",
                "file_server" => "local",
                "file_folder" => "request_image",
                "file_width" => "50",
                "file_height" => "50",
                "file_tooltip" => "Yes",
            ),
            "mso_seller_invoice_date" => array(
                "name" => "mso_seller_invoice_date",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vSellerInvoiceDate",
                "source_field" => "mso_seller_invoice_date",
                "display_query" => "mso.vSellerInvoiceDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Invoice Date",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_INVOICE_DATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => $this->general->getAdminPHPFormats('date')
            ),
            "mso_shipping_date" => array(
                "name" => "mso_shipping_date",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "dShippingDate",
                "source_field" => "mso_shipping_date",
                "display_query" => "mso.dShippingDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Shipping Date",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_SHIPPING_DATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => $this->general->getAdminPHPFormats('date')
                
            ),
            "mso_delivered_date" => array(
                "name" => "mso_delivered_date",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "dDeliveredDate",
                "source_field" => "mso_delivered_date",
                "display_query" => "mso.dDeliveredDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Delivered Date",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_DELIVERED_DATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => $this->general->getAdminPHPFormats('date')
                
            ),
            "mo_mst_order_id" => array(
                "name" => "mo_mst_order_id",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "",
                "source_field" => "",
                "display_query" => "CONCAT_WS('',mo.vOrderNumberPre,mo.iOrderNumber)",
                "entry_type" => "Custom",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Order ID",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_ORDER_ID'),
                "width" => 120,
                "search" => "No",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mso_tracking_number" => array(
                "name" => "mso_tracking_number",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vTrackingNumber",
                "source_field" => "",
                "display_query" => "mso.vTrackingNumber",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Tracking No",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_TRACKING_NO'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "tr_request_detail" => array(
                "name" => "tr_request_detail",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "tRequestDetail",
                "source_field" => "tr_request_detail",
                "display_query" => "tr.tRequestDetail",
                "entry_type" => "Table",
                "data_type" => "text",
                "show_in" => "Both",
                "type" => "textarea",
                "align" => "left",
                "label" => "Reason",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_REASON'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "mso_shipper_name" => array(
                "name" => "mso_shipper_name",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vShipperName",
                "source_field" => "",
                "display_query" => "mso.vShipperName",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Courier Company",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_COURIER_COMPANY'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mc1_country" => array(
                "name" => "mc1_country",
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "vBuyerCountry",
                "source_field" => "",
                "display_query" => "mo.vBuyerCountry",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Country",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_COUNTRY'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ms_state" => array(
                "name" => "ms_state",
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "vBuyerState",
                "source_field" => "",
                "display_query" => "mo.vBuyerState",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "State",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_STATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mc2_city" => array(
                "name" => "mc2_city",
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "vBuyerCity",
                "source_field" => "",
                "display_query" => "mo.vBuyerCity",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "City",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_CITY'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mso_product_price" => array(
                "name" => "mso_product_price",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fProductPrice",
                "source_field" => "",
                "display_query" => "mso.fProductPrice",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "right",
                "label" => "Price",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_PRICE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "php_func" => "get_formated_currency_details",
            ),
            "mso_product_qty" => array(
                "name" => "mso_product_qty",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iProductQty",
                "source_field" => "",
                "display_query" => "mso.iProductQty",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "center",
                "label" => "Qty",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_QTY'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mso_shipping_cost" => array(
                "name" => "mso_shipping_cost",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fShippingCost",
                "source_field" => "",
                "display_query" => "mso.fShippingCost",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "right",
                "label" => "Shipping",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_SHIPPING'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "php_func" => "get_formated_currency_details",
            ),
            "mso_total_cost" => array(
                "name" => "mso_total_cost",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fTotalCost",
                "source_field" => "",
                "display_query" => "mso.fTotalCost",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "right",
                "label" => "Total",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_TOTAL'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "php_func" => "get_formated_currency_details",
            ),
            "mo_buyer_pin_code" => array(
                "name" => "mo_buyer_pin_code",
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "vBuyerPinCode",
                "source_field" => "",
                "display_query" => "mo.vBuyerPinCode",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Buyer Pin Code",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_PIN_CODE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mso_seller_invoice_no" => array(
                "name" => "mso_seller_invoice_no",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vSellerInvoiceNo",
                "source_field" => "",
                "display_query" => "mso.vSellerInvoiceNo",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Invoice No",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_INVOICE_NO'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mso_product_name" => array(
                "name" => "mso_product_name",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "",
                "source_field" => "",
                "display_query" => "IF(mso.vProductName !='' AND mso.tProductOption !='' ,CONCAT(mso.vProductName,' (',mso.tProductOption,')'),mso.vProductName)",
                "entry_type" => "Custom",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Product Name",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_PRODUCT_NAME'),
                "width" => 120,
                "search" => "No",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mso_shipping_remark" => array(
                "name" => "mso_shipping_remark",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "tShippingRemark",
                "source_field" => "",
                "display_query" => "mso.tShippingRemark",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "IMEI/SL No",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_IMEI_C47SL_NO'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "sys_custom_field_2" => array(
                "name" => "sys_custom_field_2",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "",
                "source_field" => "",
                "display_query" => "mc.vEmail",
                "entry_type" => "Custom",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Buyer Address",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_ADDRESS'),
                "width" => 120,
                "search" => "No",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "php_func" => "getBuyerShippingAddressExpandedView",
            ),
            "custom_reason_id" => array(
                "name" => "custom_reason_id",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "",
                "source_field" => "",
                "display_query" => "tr.iTrnRmaId",
                "entry_type" => "Custom",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Reason ID",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_REASON_ID'),
                "width" => 120,
                "search" => "No",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0)
        {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++)
            {
                $config_arr[$name[$i]] = $list_config[$name[$i]];
            }
        }
        elseif ($name != "" && is_string($name))
        {
            $config_arr = $list_config[$name];
        }
        else
        {
            $config_arr = $list_config;
        }
        return $config_arr;
    }

    /**
     * getFormConfiguration method is used to get form configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns form configuration array.
     */
    public function getFormConfiguration($name = "")
    {
        $form_config = array(
            "tr_request_img" => array(
                "name" => "tr_request_img",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "vRequestImg",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Request Img",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_REQUEST_IMG')
            ),
            "tr_request_detail" => array(
                "name" => "tr_request_detail",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "tRequestDetail",
                "entry_type" => "Table",
                "data_type" => "text",
                "type" => "textarea",
                "label" => "Request Detail",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_REQUEST_DETAIL')
            ),
            "tr_request_date" => array(
                "name" => "tr_request_date",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "dRequestDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "type" => "date",
                "label" => "Request Date",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_REQUEST_DATE'),
                "format" => $this->general->getAdminPHPFormats('date')
            ),
            "tr_customer_id" => array(
                "name" => "tr_customer_id",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "iCustomerId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Customer Id",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_CUSTOMER_ID')
            ),
            "tr_seller_id" => array(
                "name" => "tr_seller_id",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "iSellerId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Seller Id",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_SELLER_ID')
            ),
            "tr_mst_store_detail_id" => array(
                "name" => "tr_mst_store_detail_id",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "iMstStoreDetailId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Mst Store Detail Id",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_MST_STORE_DETAIL_ID')
            ),
            "tr_sub_order_id" => array(
                "name" => "tr_sub_order_id",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "iSubOrderId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Sub Order Id",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_SUB_ORDER_ID')
            ),
            "tr_order_id" => array(
                "name" => "tr_order_id",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "iOrderId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Order Id",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_ORDER_ID')
            ),
            "tr_preference" => array(
                "name" => "tr_preference",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "ePreference",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Preference",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_PREFERENCE'),
                "default" => $this->filter->getDefaultValue("tr_preference",
                "Text",
                "Return")
            ),
            "tr_rquest_type" => array(
                "name" => "tr_rquest_type",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "eRquestType",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Rquest Type",
                "label_lang" => $this->lang->line('GETRETURNEXPANDEDVIEW_RQUEST_TYPE'),
                "default" => $this->filter->getDefaultValue("tr_rquest_type",
                "Text",
                "Pending")
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0)
        {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++)
            {
                $config_arr[$name[$i]] = $form_config[$name[$i]];
            }
        }
        elseif ($name != "" && is_string($name))
        {
            $config_arr = $form_config[$name];
        }
        else
        {
            $config_arr = $form_config;
        }
        return $config_arr;
    }

    /**
     * checkRecordExists method is used to check duplication of records.
     * @param array $field_arr field_arr is having fields to check.
     * @param array $field_val field_val is having values of respective fields.
     * @param numeric $id id is to avoid current records.
     * @param string $mode mode is having either Add or Update.
     * @param string $con con is having either AND or OR.
     * @return boolean $exists returns either TRUE of FALSE.
     */
    public function checkRecordExists($field_arr = array(), $field_val = array(), $id = '', $mode = '', $con = 'AND')
    {
        $exists = FALSE;
        if (!is_array($field_arr) || count($field_arr) == 0)
        {
            return $exists;
        }
        foreach ((array) $field_arr as $key => $val)
        {
            $extra_cond_arr[] = $this->db->protect($this->table_alias.".".$field_arr[$key])." =  ".$this->db->escape($field_val[$val]);
        }
        $extra_cond = "(".@implode(" ".$con." ", $extra_cond_arr).")";
        if ($mode == "Add")
        {
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0)
            {
                $exists = TRUE;
            }
        }
        elseif ($mode == "Update")
        {
            $extra_cond = $this->db->protect($this->table_alias.".".$this->primary_key)." <> ".$this->db->escape($id)." AND ".$extra_cond;
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0)
            {
                $exists = TRUE;
            }
        }
        return $exists;
    }

    /**
     * getSwitchTo method is used to get switch to dropdown array.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @return array $switch_data returns data records array.
     */
    public function getSwitchTo($extra_cond = '', $type = 'records', $limit = '')
    {
        $switchto_fields = $this->switchto_fields;
        $switch_data = array();
        if (!is_array($switchto_fields) || count($switchto_fields) == 0)
        {
            if ($type == "count")
            {
                return count($switch_data);
            }
            else
            {
                return $switch_data;
            }
        }
        $fields_arr = array();
        $fields_arr[] = array(
            "field" => $this->table_alias.".".$this->primary_key." AS id",
        );
        $fields_arr[] = array(
            "field" => $this->db->concat($switchto_fields)." AS val",
            "escape" => TRUE,
        );
        if (trim($this->extra_cond) != "")
        {
            $extra_cond = (trim($extra_cond) != "") ? $extra_cond." AND ".$this->extra_cond : $this->extra_cond;
        }
        $switch_data = $this->getData($extra_cond, $fields_arr, "val ASC", "", $limit, "Yes");
        #echo $this->db->last_query();
        if ($type == "count")
        {
            return count($switch_data);
        }
        else
        {
            return $switch_data;
        }
    }
}
