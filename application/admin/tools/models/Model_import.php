<?php

/**
 * Description of Import Model
 * 
 * @module Import
 * 
 * @class model_import.php
 * 
 * @path application\admin\tools\models\model_import.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 15.10.2015
 */
class Model_import extends CI_Model
{

    public $main_table = "";

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * getTableRowCount method is used to get total records count of table.
     * @param array $table table name to get count.
     * @return integer $result returns integer response.
     */
    public function getTableRowCount($table = '')
    {
        $result = $this->db->count_all($table);
        return $result;
    }

    /**
     * truncateTable method is used to import records into database.
     * @param array $table table name to insert data.
     * @return array $result returns array data.
     */
    public function getTableUniqueRows($table = '', $unique = array())
    {
        $this->db->from($table);
        $this->db->select($unique);
        $data = $this->db->get();
        $result = (is_object($data)) ? $data->result_array() : array();
        return $result;
    }
    
    /**
     * getTableLookupRows method is used to import records into database.
     * @param array $table table name to get data.
     * @param array $field field name to lookup field.
     * @return array $result returns array data.
     */
    public function getTableLookupRows($table = '', $field = '')
    {
        $data = $this->db->select_single($table, $field);
        $result = (is_object($data)) ? $data->result_single_array() : array();
        return $result;
    }

    /**
     * copyTable method is used to import records into database.
     * @param array $table table name to copy data.
     * @param array $temp temp name to insert data.
     * @return array $result returns boolean response, either TRUE or FALSE.
     */
    public function copyTable($table = '', $temp = '')
    {
        $result = false;
        if (!$this->db->table_exists($temp)) {
            if ($this->db->query("CREATE TABLE " . $this->db->protect($temp) . " LIKE " . $this->db->protect($table))) {
                if ($this->db->query("INSERT " . $this->db->protect($temp) . " SELECT * FROM " . $this->db->protect($table))) {
                    $result = $this->truncateTable($table);
                }
            }
        }
        return $result;
    }

    /**
     * truncateTable method is used to import records into database.
     * @param array $table table name to insert data.
     * @return array $result returns boolean response, either TRUE or FALSE.
     */
    public function truncateTable($table = '')
    {
        $result = $this->db->query("TRUNCATE TABLE " . $this->db->protect($table));
        return $result;
    }

    /**
     * truncateTable method is used to import records into database.
     * @param array $table table name to insert data.
     * @return array $result returns boolean response, either TRUE or FALSE.
     */
    public function revertTable($table = '', $temp = '')
    {
        $result = $this->db->query("INSERT " . $this->db->protect($table) . " SELECT * FROM " . $this->db->protect($temp));
        $this->db->query("DROP TABLE " . $this->db->protect($temp));
        return $result;
    }

    /**
     * startTransaction method is used to start transaction in database.
     */
    public function startTransaction()
    {
        $this->db->trans_begin();
    }

    /**
     * rollbackTransaction method is used to roolback transaction in database.
     */
    public function rollbackTransaction()
    {
        $this->db->trans_rollback();
    }

    /**
     * commitTransaction method is used to commit transaction in database.
     */
    public function commitTransaction()
    {
        $this->db->trans_commit();
    }

    /**
     * completeTransaction method is used to complete transaction in database.
     */
    public function completeTransaction()
    {
        if ($this->db->trans_status() === FALSE) {
            $this->rollbackTransaction();
        } else {
            $this->commitTransaction();
        }
    }

    /**
     * importBatch method is used to import records into database.
     * @param array $table table name to insert data.
     * @param array $data data array for insert into table.
     * @return array $result returns affected rows response, returns -1 on failure.
     */
    public function importBatch($table = '', $data = array())
    {
        $result = $this->db->insert_batch($table, $data);
        return $result;
    }

    /**
     * updateTable method is used to import records into database.
     * @param array $table table name to insert data.
     * @param array $data data array for insert into table.
     * @return array $result returns affected rows response, returns -1 on failure.
     */
    public function updateTable($table = '', $data = array(), $where = array())
    {
        if (!is_array($where) || count($where) == 0) {
            return false;
        }
        foreach ($where as $key => $val) {
            $this->db->where($key, $val);
        }
        $result = $this->db->update($table, $data);
        return $result;
    }
}
