<?php

/**
 * Description of PushNotify Model
 * 
 * @module PushNotify
 * 
 * @class model_pushnotify.php
 * 
 * @path application\admin\tools\models\model_pushnotify.php
 * 
 * @author Ajay Gandham
 * 
 * @date 18.03.2014
 */
class Model_pushnotify extends CI_Model
{

    public $table_name = "";

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table_name = 'mod_push_notifications';
    }
}
