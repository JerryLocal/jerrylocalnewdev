<?php

/**
 * Description of BulkEmail Model
 * 
 * @module BulkEmail
 * 
 * @class model_bulkemail.php
 * 
 * @path application\admin\tools\models\model_bulkemail.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 18.03.2014
 */
class Model_bulkemail extends CI_Model
{

    public $table_name = "";

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->table_name = 'mod_group_master';
    }

    /**
     * getGroupDetails method is used to get all group details.
     * @return array $db_email returns group data records array.
     */
    function getGroupDetails()
    {
        $this->db->select('iGroupId AS Id, vGroupName AS Val');
        $db_email_obj = $this->db->get($this->table_name);
        $db_email = is_object($db_email_obj) ? $db_email_obj->result_array() : array();
        return $db_email;
    }

    /**
     * getEmailTemplateVariables method is used to get system email template variables.
     * @return array $temp_var_arr returns variables data records array.
     */
    function getEmailTemplateVariables($email_code = "")
    {
        $this->db->select('msev.vVarName, msev.vVarDesc');
        $this->db->join('mod_system_email AS mse', 'mse.iEmailTemplateId = msev.iEmailTemplateId', 'inner');
        $this->db->where('mse.vEmailCode', $email_code);
        $temp_var_obj = $this->db->get('mod_system_email_vars AS msev');
        $temp_var_arr = is_object($temp_var_obj) ? $temp_var_obj->result_array() : array();
        return $temp_var_arr;
    }
}
