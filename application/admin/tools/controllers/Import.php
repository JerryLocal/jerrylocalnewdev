<?php
/**
 * Description of Import Controller
 * 
 * @module Import
 * 
 * @class import.php
 * 
 * @path application\admin\tools\controllers\import.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 15.10.2015
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Import extends HB_Controller
{

    private $_loopup_entries = array();
    private $_track_failed = array();
    private $_track_success = array();
    private $_chunk_size = 100;
    private $_info_limit = 100;
    private $_history_limit = 25;
    private $_gdrive_redirect_uri;
    private $_dropbox_redirect_uri;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->mod_url_cod = array(
            "import_index",
            "import_upload",
            "import_read",
            "import_process",
            "import_valid",
            "import_info",
            "import_history",
            "import_gdrive_manager",
            "import_gdrive_config",
            "import_gdrive_auth",
            "import_get_gdrive_data",
            "import_save_gdrive_data",
            "import_get_weburl_data",
            "import_dropbox_auth",
            "import_get_dropbox_data",
            "import_save_dropbox_data"
        );
        $this->import_settings = array(
            "file_extensions" => "csv|xls|xlsx|ods",
            "file_maxsize_txt" => "50 MB",
            "file_maxsize_org" => 51200,
            "file_maxcols" => 100,
            "file_maxrows" => 25000,
        );
        $this->mod_enc_url = $this->general->getCustomEncryptURL($this->mod_url_cod, true);
        $this->load->model('model_import');
        $this->load->library('dropdown');
        $this->load->library('filter');
        $this->load->library('csv_import');
        $this->_gdrive_redirect_uri = $this->config->item('admin_url') . $this->mod_enc_url['import_gdrive_auth'];
        $this->_dropbox_redirect_uri = $this->config->item('admin_url') . $this->mod_enc_url['import_dropbox_auth'];
    }

    /**
     * index method is used to intialize import page.
     */
    public function index()
    {
        list($add_access) = $this->filter->getModuleWiseAccess("UtilitiesImport", array("Add"), FALSE, TRUE);
        try {
            if (!$add_access) {
                throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_VIEW_THIS_PAGE_C46_C46_C33'));
            }
            $this->general->trackCustomNavigation('List', 'Viewed', $this->mod_enc_url['import_index'], $this->db->protect("m.vUniqueMenuCode") . " = " . $this->db->escape("UtilitiesImport"));

            $this->config->load('hb_importdata', TRUE);
            $import_modules = $this->config->item('hb_importdata');

            $upload_location_arr = array(
                "local" => "Local Drive",
                "cloud" => "Cloud Drive",
                "web" => "Web URL"
            );
            $respose_format_arr = array(
                "csv" => "CSV",
                "json" => "JSON",
                "xml" => "XML"
            );

            $first_row = array(
                "Yes" => "Yes",
                "No" => "No"
            );
            $this->dropdown->combo("array", "import_first_row", $first_row);

            $columns_separator = array(
                "comma" => "Comma (,)",
                "tab" => "Tab",
                "semicolon" => "Semicolon (;)",
                "space" => "Space"
            );
            $this->dropdown->combo("array", "import_columns_separator", $columns_separator);

            $text_delimiter = array(
                "double" => "Double Quote (\")",
                "single" => "Single Quote (')"
            );
            $this->dropdown->combo("array", "import_text_delimiter", $text_delimiter);

            $decimal_separator = array(
                "dot" => "Dot (.)",
                "comma" => "Comma (,)"
            );
            $this->dropdown->combo("array", "import_decimal_separator", $decimal_separator);

            $thousand_separator = array(
                "none" => "None",
                "comma" => "Comma (,)",
                "dot" => "Dot (.)",
                "space" => "Space",
                "single" => "Single (')"
            );

            $this->dropdown->combo("array", "import_thousand_separator", $thousand_separator);

            $modules_list = array();
            if (is_array($import_modules) && count($import_modules) > 0) {
                foreach ($import_modules as $key => $val) {
                    $title = $this->lang->line($val['name']);
                    $modules_list[$key] = ($title) ? $title : $key;
                }
            }
            $this->dropdown->combo("array", "upload_module", $modules_list);

            $render_arr = array(
                'func_name' => 'index',
                'mod_enc_url' => $this->mod_enc_url,
                'import_settings' => $this->import_settings,
                'upload_location_arr' => $upload_location_arr,
                'respose_format_arr' => $respose_format_arr,
                'add_access' => $add_access
            );
            $this->smarty->assign($render_arr);
            $this->loadView('import_index');
        } catch (Exception $e) {
            $render_arr['err_message'] = $e->getMessage();
            $this->smarty->assign($render_arr);
            $this->loadView($this->config->item('ADMIN_FORBIDDEN_TEMPLATE'));
        }
    }

    /**
     * upload method is used to upload file to import data.
     */
    public function upload()
    {
        $this->load->library('upload');
        $old_file = $this->input->get_post('oldFile', TRUE);
        $upload_files = $_FILES['Filedata'];
        list($file_name, $extension) = $this->general->get_file_attributes($upload_files['name']);
        $this->general->createUploadFolderIfNotExists('__temp');
        $temp_folder_path = $this->config->item('admin_upload_temp_path');

        try {

            $file_size = $this->import_settings['file_maxsize_org'];
            if ($upload_files['name'] == "") {
                throw new Exception($this->general->processMessageLabel('ACTION_UPLOAD_FILE_NOT_FOUND_C46_C46_C33'));
            }
            if (!$this->general->validateFileSize($file_size, $upload_files['size'])) {
                throw new Exception($this->general->processMessageLabel('ACTION_FILE_SIZE_NOT_A_VALID_ONE_C46_C46_C33'));
            }
            $upload_config = array(
                'upload_path' => $temp_folder_path,
                'allowed_types' => '*',
                'max_size' => $file_size,
                'file_name' => $file_name,
                'remove_space' => TRUE,
                'overwrite' => FALSE,
            );
            $this->upload->initialize($upload_config);
            if (!$this->upload->do_upload('Filedata')) {
                $upload_error = $this->upload->display_errors('', '');
                throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPLOADING_C46_C46_C33'));
            }
            $file_info = $this->upload->data();
            $file_name = $file_info['file_name'];

            if (!$file_name) {
                throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPLOADING_C46_C46_C33'));
            }

            if (file_exists($temp_folder_path . $old_file) && $old_file != '') {
                @unlink($temp_folder_path . $old_file);
            }

            $ret_arr['success'] = 1;
            $ret_arr['message'] = $this->general->processMessageLabel('ACTION_FILE_UPLOADED_SUCCESSFULLY_C46_C46_C33');
            $ret_arr['uploadfile'] = $file_name;
            $ret_arr['oldfile'] = $file_name;
        } catch (Exception $e) {
            $ret_arr['success'] = 0;
            $ret_arr['message'] = $e->getMessage();
        }
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    /**
     * read method is used to read imported file contents and displays to user.
     */
    public function read()
    {
        $data = $header = $mapping = $columns = array();
        try {

            $upload_module = $this->input->get_post('upload_module', TRUE);
            $upload_location = $this->input->get_post('upload_location', TRUE);
            $upload_csv = $this->input->get_post('upload_csv', TRUE);
            $upload_sheet = $this->input->get_post('upload_sheet', TRUE);
            $web_data_url = $this->input->get_post('web_data_url', TRUE);
            $response_format = $this->input->get_post('response_format', TRUE);
            $first_row = $this->input->get_post('import_first_row', TRUE);
            $columns_separator = $this->input->get_post('import_columns_separator', TRUE);
            $text_delimiter = $this->input->get_post('import_text_delimiter', TRUE);
            $decimal_separator = $this->input->get_post('import_decimal_separator', TRUE);
            $thousand_separator = $this->input->get_post('import_thousand_separator', TRUE);

            $temp_folder_path = $this->config->item('admin_upload_temp_path');
            if ($upload_location == "cloud") {
                $file_name = $upload_sheet;
            } else if ($upload_location == "web") {
                if ($response_format == "csv") {
                    $file_name = "Web_Csv_" . time() . ".csv";
                    $file_path = $temp_folder_path . $file_name;
                    $fp = fopen($file_path, 'w');
                    fwrite($fp, file_get_contents($web_data_url));
                    fclose($fp);
                } else if (in_array($response_format, array('xml', 'json'))) {
                    $web_data_url = $this->input->get_post('web_data_url', TRUE);
                    $web_data_url_keypath = $this->input->get_post('web_data_url_keypath', TRUE);
                    $response = "";
                    if ($response_format == "xml") {
                        $response = $this->csv_import->callCurlGet($web_data_url, array(), TRUE);
                    } else if ($response_format == "json") {
                        $response = $this->csv_import->callCurlGet($web_data_url);
                        $response = json_decode($response, TRUE);
                    }
                    $response = $this->csv_import->getArrayvalueForKeyPath($response, $web_data_url_keypath);
                    $file_name = "Web_" . ucfirst($response_format) . "_" . time() . ".csv";
                    $file_path = $temp_folder_path . $file_name;
                    $fp = fopen($file_path, 'w');
                    if (is_array($response[0])) {
                        fputcsv($fp, array_keys($response[0]));
                    }
                    foreach ($response as $respv) {
                        $temp_row = array();
                        if (is_array($respv)) {
                            foreach ($respv as $respv2) {
                                $temp_row[] = is_array($respv2) ? "" : $respv2;
                            }
                        } else {
                            $temp_row[] = $respv;
                        }
                        fputcsv($fp, $temp_row);
                    }
                    fclose($fp);
                }
            } else {
                $file_name = $upload_csv;
            }
            $file_path = $temp_folder_path . DS . $file_name;
            if (!is_file($file_path)) {
                throw new Exception($this->general->processMessageLabel('ACTION_FILE_NOT_FOUND_C46_C46_C33'));
            }
            //load the excel library
            $this->load->library('csv_import');
            $result = $this->csv_import->readExcelContent($file_path, $columns_separator, $text_delimiter);

            if (!$result['success']) {
                throw new Exception($result['message']);
            }

            $this->config->load('hb_importdata', TRUE);
            $import_modules = $this->config->item('hb_importdata');
            $current_module = $import_modules[$upload_module];

            if (!is_array($current_module) || count($current_module) == 0) {
                throw new Exception("Import module configuration not found.");
            }

            $import_data_action = array(
                "Replace" => "Replace",
                "Merge" => "Merge"
            );

            $duplicate_data_action = array(
                "Skip" => "Skip",
                "Update" => "Update"
            );

            $import_error_action = array(
                "Skip" => "Skip Row",
                "Empty" => "Make Empty"
            );

            $skip_lookup_action = array(
                "Yes" => "Yes",
                "No" => "No"
            );

            $skip_validation_action = array(
                "Yes" => "Yes",
                "No" => "No"
            );

            $upload_table = $current_module['table'];
            $unique_arr = $current_module['unique'];
            $columns_arr = $current_module['cols'];
            if (!is_array($columns_arr) || count($columns_arr) == 0) {
                throw new Exception("Import module mapping columns not found.");
            }

            $row_count = $this->model_import->getTableRowCount($upload_table);
            foreach ($columns_arr as $key => $val) {
                if ($val['hide'] === TRUE) {
                    continue;
                }
                $title = $this->lang->line($val['name']);
                $columns[$key] = ($title) ? $title : $key;
            }
            $this->dropdown->combo("array", "map_column", $columns);

            $unique_fields = array();
            $unique_str = '---';
            $unique_cols = $unique_arr['cols'];
            if (is_array($unique_cols) && count($unique_cols) > 0) {
                foreach ($unique_cols as $key => $val) {
                    $title = $this->lang->line($columns_arr[$val]['name']);
                    $unique_fields[] = ($title) ? $title : $val;
                }
                $unique_type = ($unique_arr['type'] == "OR") ? "|" : "&";
                $unique_str = @implode(" " . $unique_type . " ", $unique_fields);
            }

            $total = $result['data'];
            if ($first_row == "Yes") {
                $header = $total[0];
                unset($total[0]);
                $total = array_values($total);
            } else {
                for ($i = 0, $j = 1; $i < count($total[0]); $i++, $j++) {
                    $header[] = "Column - " . $j;
                }
            }
            for ($i = 0; $i < count($header); $i++) {
                if (array_key_exists($header[$i], $columns)) {
                    $mapping[] = $header[$i];
                } else {
                    $mapping[] = array_search($header[$i], $columns);
                }
            }

            for ($i = 0; $i < count($total); $i++) {
                if ($i == 5) {
                    break;
                }
                $data[] = $total[$i];
            }
            $success = 1;
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $render_arr = array(
            "success" => $success,
            "message" => $message,
            "module_name" => $upload_module,
            "table_name" => $upload_table,
            "row_count" => $row_count,
            "unique_str" => $unique_str,
            "file_name" => $file_name,
            "first_row" => $first_row,
            "columns_separator" => $columns_separator,
            "text_delimiter" => $text_delimiter,
            "import_action" => $import_data_action,
            "duplicate_action" => $duplicate_data_action,
            "error_action" => $import_error_action,
            "lookup_action" => $skip_lookup_action,
            "validation_action" => $skip_validation_action,
            "mapping" => $mapping,
            "header" => $header,
            "data" => $data
        );
        $this->smarty->assign($render_arr);
        $this->loadView('import_read');
    }

    /**
     * process method is used to insert records into database for selected table.
     */
    public function process()
    {
        set_time_limit(0);
        $import_module_name = $this->input->get_post('import_module_name', TRUE);
        $import_file_name = $this->input->get_post('import_file_name', TRUE);
        $import_first_row = $this->input->get_post('import_first_row', TRUE);
        $import_columns_separator = $this->input->get_post('import_columns_separator', TRUE);
        $import_text_delimiter = $this->input->get_post('import_text_delimiter', TRUE);
        $import_data_action = $this->input->get_post('import_data_action', TRUE);
        $duplicate_data_action = $this->input->get_post('duplicate_data_action', TRUE);
        $import_error_action = $this->input->get_post('import_error_action', TRUE);
        $skip_lookup_action = $this->input->get_post('skip_lookup_action', TRUE);
        $skip_validation_action = $this->input->get_post('skip_validation_action', TRUE);
        $skip_top_rows = $this->input->get_post('skip_top_rows', TRUE);
        $map_column_arr = $this->input->get_post('map_column', TRUE);
        $skip_column_arr = $this->input->get_post('skip_column', TRUE);
        $import_type = $this->input->get_post('import_type', TRUE);

        $skip_top_rows = intval($skip_top_rows);
        $import_type = ($import_type == "commit") ? "commit" : "preview";
        if ($import_type == "commit") {
            $map_column_arr = explode("@@", $map_column_arr);
            $skip_column_arr = explode("@@", $skip_column_arr);
        }

        try {

            $temp_folder_path = $this->config->item('admin_upload_temp_path');
            $file_path = $temp_folder_path . DS . $import_file_name;
            if (!is_file($file_path)) {
                throw new Exception($this->general->processMessageLabel('ACTION_FILE_NOT_FOUND_C46_C46_C33'));
            }
            if ($import_type == "commit") {
                $this->general->trackCustomNavigation('Form', 'Added', $return_url, $this->db->protect("m.vUniqueMenuCode") . " = " . $this->db->escape("UtilitiesImport"));
            }

            //load the excel library
            $this->load->library('csv_import');
            $result = $this->csv_import->readExcelContent($file_path, $import_columns_separator, $import_text_delimiter);

            if (!$result['success']) {
                throw new Exception($result['message']);
            }

            $this->config->load('hb_importdata', TRUE);
            $import_modules = $this->config->item('hb_importdata');
            $current_module = $import_modules[$import_module_name];

            if (!is_array($current_module) || count($current_module) == 0) {
                throw new Exception("Import module configuration not found.");
            }

            $import_table_name = $current_module['table'];
            $unique_arr = $current_module['unique'];
            $columns_arr = $current_module['cols'];
            if (!is_array($columns_arr) || count($columns_arr) == 0) {
                throw new Exception("Import module mapping columns not found.");
            }

            $total = $result['data'];
            if ($import_first_row == "Yes") {
                unset($total[0]);
            }

            $data = array_values($total);
            if (!is_array($data) || count($data) == 0) {
                throw new Exception("No data found to import.");
            }

            $duplicate_arr = $unique_cols = array();
            $duplicate_chk = false;
            $flush_rows = $total_count = $existed_rec = 0;
            if ($import_data_action == "Replace") {
                $temp_table_name = "__import_" . $import_table_name;
                $flush_rows = $existed_rec = $this->model_import->getTableRowCount($import_table_name);
                $backup_file = date("YmdHis") . "_" . $import_table_name . "_Backup.sql";
                $backup_path = $this->config->item('admin_backup_path');
                $this->load->library('ci_backup');
                $this->ci_backup->setFileLocations(array("file_path" => $backup_path, "file_name" => $backup_file));
                $this->ci_backup->setKeepDropTable(true);
                $this->ci_backup->setAllowTables(true);
                $this->ci_backup->setDatabaseTables(array($import_table_name));
                $this->ci_backup->createDBDump();
                if ($import_type == "commit") {
                    $this->model_import->truncateTable($import_table_name, $temp_table_name);
                } else {
                    $this->model_import->copyTable($import_table_name, $temp_table_name);
                }
            } else {
                $total_count = $existed_rec = $this->model_import->getTableRowCount($import_table_name);
                $unique_cols = $unique_arr['cols'];
                if (is_array($unique_cols) && count($unique_cols) > 0) {
                    sort($unique_cols);
                    $unique_data = $this->model_import->getTableUniqueRows($import_table_name, $unique_cols);
                    if (is_array($unique_data) && count($unique_data) > 0) {
                        foreach ($unique_data as $key => $val) {
                            $tmp_arr = array_values($val);
                            if ($unique_arr['type'] == "OR") {
                                $duplicate_arr = array_merge($duplicate_arr, $tmp_arr);
                            } else {
                                $duplicate_arr[] = @implode("*&&*", $tmp_arr);
                            }
                        }
                    }
                    $duplicate_chk = true;
                }
            }

            $track_failed = $track_inserted = $track_updated = $track_duplicate = $track_lookup = $track_valid = array();
            $import_failed = $import_inserted = $import_updated = $import_duplicate = $import_skipped = 0;
            $import_batch_arr = $import_dupli_arr = $import_rows_arr = $import_hide_cols = array();

            foreach ($columns_arr as $key => $val) {
                if ($val['hide'] === TRUE && is_array($val['default'])) {
                    $import_hide_cols[$key] = $this->csv_import->importDefaultValue($val['default']);
                }
            }
            for ($i = 0, $n = 1; $i < count($data); $i++, $n++) {
                if ($skip_top_rows > 0 && $skip_top_rows > $n) {
                    continue;
                }
                $tmp_arr = $uni_arr = array();
                $dup_row = false;
                for ($j = 0; $j < count($map_column_arr); $j++) {
                    if (!in_array($j, $skip_column_arr)) {
                        continue;
                    }
                    $key = trim($map_column_arr[$j]);
                    $val = trim($data[$i][$j]);
                    $col = $columns_arr[$key];
                    if (isset($col['phpfn']) && $col['phpfn'] != "") {
                        if (function_exists($col['phpfn'])) {
                            $val = call_user_func($col['phpfn'], $val);
                        } else if (method_exists($this->general, $col['phpfn'])) {
                            $val = $this->general->$col['phpfn']($val, $data[$i], $map_column_arr);
                        }
                    }
                    $valid_result = $lookup_result = TRUE;
                    if ($skip_validation_action == "No") {
                        $valid_result = $this->_validate_column($key, $val, $col);
                    }
                    if ($skip_lookup_action == "No" && $valid_result === TRUE) {
                        $lookup_result = $this->_validate_lookup($key, $val, $col, $n);
                    }
                    if ($valid_result === FALSE || $lookup_result === FALSE) {
                        if ($import_error_action == "Skip") {
                            $import_skipped += 1;
                            if ($valid_result === FALSE) {
                                $track_valid[] = $n;
                            } else if ($lookup_result === FALSE) {
                                $track_lookup[] = $n;
                            }
                            continue 2;
                        } else {
                            $val = '';
                        }
                    }
                    if ($this->csv_import->isDecimalType($col['type'])) {
                        $val = $this->csv_import->convertPriceToFloat($val);
                    } elseif ($this->csv_import->isDateTimeType($col['type'])) {
                        $val = $this->csv_import->convertToDate($val);
                    } elseif ($this->csv_import->isDateType($col['type'])) {
                        $val = $this->csv_import->convertToDateTime($val);
                    } elseif ($this->csv_import->isTimeType($col['type'])) {
                        $val = $this->csv_import->convertToTime($val);
                    }
                    $tmp_arr[$key] = $val;
                    if (in_array($key, $unique_cols)) {
                        if ($unique_arr['type'] == "OR") {
                            if (in_array($val, $duplicate_arr)) {
                                $dup_row = true;
                            }
                            $duplicate_arr[] = $val;
                        }
                        $uni_arr[$key] = $data[$i][$j];
                    }
                }

                $tmp_arr = array_merge($tmp_arr, $import_hide_cols);

                if ($duplicate_chk == true) {
                    if ($unique_arr['type'] == "OR") {
                        if ($dup_row == true) {
                            $import_duplicate += 1;
                            $import_dupli_arr[] = array(
                                "data" => $tmp_arr,
                                "where" => $uni_arr,
                                "row" => $n
                            );
                        } else {
                            $import_batch_arr[] = $tmp_arr;
                            $import_rows_arr[] = $n;
                        }
                    } else {
                        ksort($uni_arr);
                        $uni_str = @implode("*&&*", $uni_arr);
                        if (in_array($uni_str, $duplicate_arr)) {
                            $import_duplicate += 1;
                            $import_dupli_arr[] = array(
                                "data" => $tmp_arr,
                                "where" => $uni_arr,
                                "row" => $n
                            );
                        } else {
                            $import_batch_arr[] = $tmp_arr;
                            $import_rows_arr[] = $n;
                        }
                        $duplicate_arr[] = $uni_str;
                    }
                } else {
                    $import_batch_arr[] = $tmp_arr;
                    $import_rows_arr[] = $n;
                }
            }

            $chunk_size = $this->_chunk_size;
            $import_batch = array_chunk($import_batch_arr, $chunk_size);

            $this->model_import->startTransaction();
            for ($i = 0, $j = 0; $i < count($import_batch); $i++, $j += $chunk_size) {
                $result = $this->model_import->importBatch($import_table_name, $import_batch[$i]);
                if ($result === -1) {
                    list($bin_failed, $bin_success) = $this->_do_binary_approach($import_table_name, $import_batch[$i], $j, $import_rows_arr);
                    $import_failed += $bin_failed;
                    $import_inserted += $bin_success;
                    $track_failed = is_array($this->_track_failed) ? array_merge($track_failed, $this->_track_failed) : $track_failed;
                    $track_inserted = is_array($this->_track_success) ? array_merge($track_inserted, $this->_track_success) : $track_inserted;
                    $this->_track_failed = array();
                    $this->_track_success = array();
                } else {
                    $chunk_count = count($import_batch[$i]);
                    $import_inserted += $chunk_count;
                    $track_inserted = array_merge($track_inserted, array_slice($import_rows_arr, $j, $chunk_count));
                }
            }
            if ($duplicate_data_action == "Update") {
                for ($i = 0; $i < count($import_dupli_arr); $i++) {
                    $result = $this->model_import->updateTable($import_table_name, $import_dupli_arr[$i]['data'], $import_dupli_arr[$i]['where']);
                    if ($result === -1) {
                        $import_failed += 1;
                        $track_failed[] = $import_dupli_arr[$i]['row'];
                    } else if ($result) {
                        $import_updated += 1;
                        $track_updated[] = $import_dupli_arr[$i]['row'];
                    }
                }
                $import_duplicate = 0;
            } else {
                for ($i = 0; $i < count($import_dupli_arr); $i++) {
                    $track_duplicate[] = $import_dupli_arr[$i]['row'];
                }
            }

            if ($import_type == "commit") {
                $this->model_import->commitTransaction();
            } else {
                $this->model_import->rollbackTransaction();
            }

            if ($import_type != "commit") {
                if ($import_data_action == "Replace") {
                    $this->model_import->revertTable($import_table_name, $temp_table_name);
                }
            }

            $message = "Data import done successfully.";
            $success = 1;
        } catch (Exception $e) {
            $message = $e->getMessage();
            $success = 0;
        }
        $total_count += $import_inserted;
        $import_success = $import_inserted + $import_updated;

        if ($import_type == "commit") {
            $import_folder_path = $this->config->item('import_files_path');
            copy($file_path, $import_folder_path . $import_file_name);

            $module_title = $this->lang->line($current_module['name']);
            $history_arr = array();
            $history_arr['module'] = ($module_title) ? $module_title : $current_module['name'];
            $history_arr['table'] = $import_table_name;
            $history_arr['file'] = $import_file_name;
            $history_arr['date'] = date("Y-m-d H:i:s");
            $history_arr['existed'] = $existed_rec;
            $history_arr['success'] = $import_success;
            $history_arr['inserted'] = $import_inserted;
            $history_arr['updated'] = $import_updated;
            $history_arr['duplicate'] = $import_duplicate;
            $history_arr['failed'] = $import_failed;
            $history_arr['skipped'] = $import_skipped;
            $history_arr['flushed'] = $flush_rows;
            $history_arr['total'] = $total_count;

            $histroy_file = $import_folder_path . "import_history.json";
            $history_json = file_get_contents($histroy_file);
            $history_data = json_decode($history_json, TRUE);
            if (is_array($history_data) && count($history_data) >= $this->_history_limit) {
                $rename_name = "import_history_1";
                $rename_file = $import_folder_path . $rename_name . ".json";
                $r = 2;
                while (is_file($rename_file_path)) {
                    $rename_file_path = $import_folder_path . $rename_name . "_" . $r . ".json";
                }
                rename($histroy_file, $rename_file);
                $history_data = array();
            } else {
                $history_data = array();
            }
            $history_data[] = $history_arr;

            $fp = fopen($histroy_file, 'w');
            fwrite($fp, json_encode($history_data));
            fclose($fp);

            $ret_arr = array();
            $ret_arr['success'] = $success;
            $ret_arr['message'] = $message;
            echo json_encode($ret_arr);
            $this->skip_template_view();
        } else {
            $render_arr = array(
                "success" => $success,
                "message" => $message,
                "module_name" => $import_module_name,
                "table_name" => $import_table_name,
                "file_name" => $import_file_name,
                "first_row" => $import_first_row,
                "columns_separator" => $import_columns_separator,
                "text_delimiter" => $import_text_delimiter,
                "import_action" => $import_data_action,
                "duplicate_action" => $duplicate_data_action,
                "error_action" => $import_error_action,
                "lookup_action" => $skip_lookup_action,
                "validation_action" => $skip_validation_action,
                "skip_top_rows" => $skip_top_rows,
                "import_duplicate" => $import_duplicate,
                "import_failed" => $import_failed,
                "import_success" => $import_success,
                "import_skipped" => $import_skipped,
                "flush_rows" => $flush_rows,
                "total_count" => $total_count,
                "map_column_arr" => @implode("@@", $map_column_arr),
                "skip_column_arr" => @implode("@@", $skip_column_arr),
                "track_failed" => @implode("@@", array_slice($track_failed, 0, $this->_info_limit)),
                "track_inserted" => @implode("@@", array_slice($track_inserted, 0, $this->_info_limit)),
                "track_updated" => @implode("@@", array_slice($track_updated, 0, $this->_info_limit)),
                "track_duplicate" => @implode("@@", array_slice($track_duplicate, 0, $this->_info_limit)),
                "track_lookup" => @implode("@@", array_slice($track_lookup, 0, $this->_info_limit)),
                "track_valid" => @implode("@@", array_slice($track_valid, 0, $this->_info_limit)),
            );
            $this->smarty->assign($render_arr);
            $this->loadView('import_process');
        }
    }

    private function _do_binary_approach($table_name = '', $insert_arr = array(), $j = 0, $rows_arr = array())
    {
        $failed = $sucess = 0;
        if (!is_array($insert_arr) || count($insert_arr) == 0) {
            return array($failed, $sucess);
        }
        $array_count = count($insert_arr);
        if ($array_count == 1) {
            $result = $this->model_import->importBatch($table_name, $insert_arr);
            if ($result === -1) {
                $failed += 1;
                $this->_track_failed[] = $rows_arr[$j];
            } else {
                $sucess += 1;
                $this->_track_success[] = $rows_arr[$j];
            }
        } else {
            $chunk_size = round($array_count / 2);
            $batch_arr = array_chunk($insert_arr, $chunk_size);
            for ($i = 0, $k = 0; $i < count($batch_arr); $i++, $k += $chunk_size) {
                $result = $this->model_import->importBatch($table_name, $batch_arr[$i]);
                if ($result === -1) {
                    list($re_failed, $re_success) = $this->_do_binary_approach($table_name, $batch_arr[$i], ($j + $k), $rows_arr);
                    $failed += $re_failed;
                    $sucess += $re_success;
                } else {
                    $chunk_count = count($batch_arr[$i]);
                    $sucess += $chunk_count;
                    $this->_track_success = array_merge($this->_track_success, array_slice($rows_arr, ($j + $k), $chunk_count));
                }
            }
        }
        return array($failed, $sucess);
    }

    private function _validate_column($key = '', $val = '', $config = array())
    {
        $rules = $config['rules'];
        $verified = TRUE;
        if (is_array($rules) && count($rules) > 0) {
            foreach ($rules as $rk => $rv) {
                $_check_func = "_check_" . $rk;
                if (method_exists($this, $_check_func)) {
                    $verified = $this->$_check_func($val, $rv);
                    if ($verified === FALSE) {
                        break;
                    }
                }
            }
        }
        if (!isset($config['null'])) {
            if ($val == "") {
                $verified = FALSE;
            }
        }
        return $verified;
    }

    private function _check_required($val = '', $rule = '')
    {
        return ($val == "") ? FALSE : TRUE;
    }

    private function _check_email($val = '', $rule = '')
    {
        return filter_var($val, FILTER_VALIDATE_EMAIL) ? TRUE : FALSE;
    }

    private function _check_url($val = '', $rule = '')
    {
        return filter_var($val, FILTER_VALIDATE_URL) ? TRUE : FALSE;
    }

    private function _check_date($val = '', $rule = '')
    {
        $currentSec = strtotime($val);
        $actualSec = strtotime(date("Y-m-d", strtotime($val)));
        return ($currentSec == $actualSec && $currentSec !== FALSE) ? TRUE : FALSE;
    }

    private function _check_datetime($val = '', $rule = '')
    {
        $currentSec = strtotime($val);
        $actualSec = strtotime(date("Y-m-d H:i:s", strtotime($sValue)));
        return ($currentSec == $actualSec && $currentSec !== FALSE) ? TRUE : FALSE;
    }

    private function _check_time($val = '', $rule = '')
    {
        $currentSec = strtotime($val);
        $actualSec = strtotime(date("H:i:s", strtotime($sValue)));
        return ($currentSec == $actualSec && $currentSec !== FALSE) ? TRUE : FALSE;
    }

    private function _check_digits($val = '', $rule = '')
    {
        return (preg_match('~^[0-9]+$~', $val) === 1) ? TRUE : FALSE;
    }

    private function _check_number($val = '', $rule = '')
    {
        return is_numeric($val) ? TRUE : FALSE;
    }

    private function _check_max($val = '', $rule = '')
    {
        return (is_numeric($val) && $val <= $rule) ? TRUE : FALSE;
    }

    private function _check_maxlength($val = '', $rule = '')
    {
        return (isset($val{$rule}) === FALSE) ? TRUE : FALSE;
    }

    private function _check_min($val = '', $rule = '')
    {
        return (is_numeric($val) && $val >= $rule) ? TRUE : FALSE;
    }

    private function _check_minlength($val = '', $rule = '')
    {
        return (isset($val{ --$rule})) ? TRUE : FALSE;
    }

    private function _check_range($val = '', $rule = '')
    {
        return $this->_check_rangelength(intval($val), $rule);
    }

    private function _check_rangelength($val = '', $rule = '')
    {
        if (is_array($val)) {
            $iCount = count($val);
        } elseif (is_numeric($val)) {
            $iCount = $val;
        } else {
            $iCount = strlen($val);
        }
        return ($iCount >= $rule[0] && $iCount <= $rule[1]) ? TRUE : FALSE;
    }

    private function _validate_lookup($key = '', $val = '', $config = array())
    {
        if (!isset($config['lookup'])) {
            return TRUE;
        }
        if (array_key_exists($this->_loopup_entries, $key)) {
            $data_arr = $this->_loopup_entries[$key];
        } else {
            if ($config['lookup']['type'] == 'table') {
                $data_arr = $this->model_import->getTableLookupRows($config['lookup']['table'][0], $config['lookup']['table'][1]);
            } else if ($config['lookup']['type'] == 'list') {
                $data_arr = $config['lookup']['list'];
            }
            $data_arr = is_array($data_arr) ? $data_arr : array();
            $this->_loopup_entries[$key] = $data_arr;
        }
        if (empty($data_arr) || in_array($val, $data_arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function gdrive_manager()
    {
        $apiType = $this->session->userdata('__oauth_api_type');
        $apiType = (($apiType != '') ? $apiType : 'gdrive');
        $render_arr = array(
            'apiType' => $apiType,
            'gdrive_client_id' => $this->config->item('GOOGLE_OAUTH_CLIENT_ID'),
            'gdrive_client_secret' => $this->config->item('GOOGLE_OAUTH_CLIENT_SECRET'),
            'gdrive_redirect_uri' => $this->_gdrive_redirect_uri,
            'dropbox_client_id' => $this->config->item('DROPBOX_OAUTH_CLIENT_ID'),
            'dropbox_client_secret' => $this->config->item('DROPBOX_OAUTH_CLIENT_SECRET'),
            'dropbox_redirect_uri' => $this->_dropbox_redirect_uri
        );
        $this->smarty->assign($render_arr);
        $this->loadView('import_gdrive_manager');
    }

    public function gdrive_config()
    {
        $type = $this->input->get_post('type', TRUE);
        $client_id = $this->input->get_post('client_id', TRUE);
        $client_secret = $this->input->get_post('client_secret', TRUE);
        if ($type == "gdrive") {
            $data = array();
            $data['vValue'] = $client_id;
            $extra_cond = $this->db->protect("vName") . " = " . $this->db->escape('GOOGLE_OAUTH_CLIENT_ID');
            $success = $this->systemsettings->updateSetting($data, $extra_cond);

            $data = array();
            $data['vValue'] = $client_secret;
            $extra_cond = $this->db->protect("vName") . " = " . $this->db->escape('GOOGLE_OAUTH_CLIENT_SECRET');
            $success = $this->systemsettings->updateSetting($data, $extra_cond);
        } elseif ($type == 'dropbox') {
            $data = array();
            $data['vValue'] = $client_id;
            $extra_cond = $this->db->protect("vName") . " = " . $this->db->escape('DROPBOX_OAUTH_CLIENT_ID');
            $success = $this->systemsettings->updateSetting($data, $extra_cond);

            $data = array();
            $data['vValue'] = $client_secret;
            $extra_cond = $this->db->protect("vName") . " = " . $this->db->escape('DROPBOX_OAUTH_CLIENT_SECRET');
            $success = $this->systemsettings->updateSetting($data, $extra_cond);
        }
        $message = ($success) ? "Config setting updated successfully." : "Failure in adding config settings.";
        $return_arr = array();
        $return_arr['success'] = $success;
        $return_arr['message'] = $message;

        echo json_encode($return_arr);
        $this->skip_template_view();
    }

    public function gdrive_auth()
    {
        $_nA = $this->input->get_post('_nA', TRUE);
        $code = $this->input->get_post('code', TRUE);
        if (!is_null($_nA) && $_nA == '1') {
            unset($_SESSION['OAUTH_ACCESS_TOKEN']);
            unset($_SESSION['OAUTH_STATE']);
            unset($_SESSION['__oauth_api_token']);
            $this->session->unset_userdata('__oauth_api_token');
            $this->session->unset_userdata('OAUTH_ACCESS_TOKEN');
            $this->session->unset_userdata('OAUTH_STATE');
        }

        try {
            $scopes = array(
                'https://spreadsheets.google.com/feeds',
                'https://www.googleapis.com/auth/drive'
            );
            require(APPPATH . 'third_party/oauth/vendor/autoload.php');
            $client = new oauth_client_class;

            $client->server = 'Google';
            $client->debug = false;
            $client->offline = true;
            $client->debug_http = true;
            $client->access_type = "offline";
            $client->approval_prompt = "force";
            $client->redirect_uri = $this->_gdrive_redirect_uri;
            $client->client_id = $this->config->item('GOOGLE_OAUTH_CLIENT_ID');
            $client->client_secret = $this->config->item('GOOGLE_OAUTH_CLIENT_SECRET');
            $client->scope = implode(" ", $scopes);
            if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0) {
                throw new Exception('Please go to Google APIs console page ' .
                '<a href="http://code.google.com/apis/console" target="_blank">http://code.google.com/apis/console</a> in the API access tab, ' .
                'create a new client ID, and set the client_id to Client ID and client_secret. ' .
                'The callback URL must be <b>' . $client->redirect_uri . '</b> but make sure ' .
                'the domain is valid and can be resolved by a public DNS.');
            }

            if ($success = $client->Initialize()) {
                if ($success = $client->Process()) {
                    if (strlen($client->authorization_error)) {
                        throw new Exception($client->authorization_error);
                    }
                }
                $success = $client->Finalize($success);
                $this->session->set_userdata('__oauth_api_token', $client->access_token);
            }
            if (!isset($client->access_token) || trim($client->access_token) == "") {
                throw new Exception($client->error);
            }
            $this->session->set_userdata('__oauth_api_type', 'gdrive');
            $success = 1;
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $render_arr = array(
            "api" => 'gdrive',
            "success" => $success,
            "message" => $message,
        );
        $htm_str = $this->parser->parse('import_gdrive_auth_redirect', $render_arr, true);
        echo $htm_str;
        $this->skip_template_view();
    }

    public function get_gdrive_data()
    {
        $params_arr = array();
        $params_arr['type'] = $this->input->get_post('type', TRUE);
        $params_arr['docId'] = $this->input->get_post('docId', TRUE);
        $params_arr['sheetId'] = $this->input->get_post('sheetId', TRUE);
        $access_token = $this->session->userdata('__oauth_api_token');
        $render_arr = $full_arr = array();
        try {
            if (!isset($access_token) || trim($access_token) == "") {
                throw new Exception("Google authentication required.");
            }
            $header_params = array();
            $header_params[] = "Authorization: Bearer " . $access_token;
            if ($params_arr['type'] == "docs") {
                $header_params[] = "Content-Type: application/atom+xml";
                // Fetching all spreadsheets
                $uri_list_files = "https://spreadsheets.google.com/feeds/spreadsheets/private/full";
                $list_files_response = $this->csv_import->callCurlGet($uri_list_files, $header_params, TRUE);
                $render_arr['list_files_response'] = $list_files_response;
                if (is_array($list_files_response) && count($list_files_response) > 0) {
                    if (isset($list_files_response['feed']['entry']) && is_array($list_files_response['feed']['entry']) && count($list_files_response['feed']['entry']) > 0) {
                        $loop_lf_arr = $list_files_response['feed']['entry'];
                        if (array_keys($loop_lf_arr) !== range(0, count($loop_lf_arr) - 1)) {
                            $loop_lf_arr = array($loop_lf_arr);
                        }
                        foreach ($loop_lf_arr as $lfKey => $lfValue) {
                            $temp_arr = array();
                            $temp_arr['docName'] = $lfValue['title'];
                            $file_id = basename($lfValue['id']);
                            $temp_arr['docId'] = $file_id;
                            $full_arr[] = $temp_arr;
                        }
                    }
                }
            } else if ($params_arr['type'] == "sheets") {
                $file_id = $params_arr['docId'];
                $file_name = trim($params_arr['docName']);
                $uri_file_path = "https://docs.google.com/spreadsheets/export?id=" . $file_id . "&exportFormat=xlsx";
                $temp_folder_path = $this->config->item('admin_upload_temp_path');
                $file_name = $file_name . ".xlsx";
                list($file_name, $extension) = $this->general->get_file_attributes($file_name);

                $fp = fopen($temp_folder_path . $file_name, "wb");
                $this->csv_import->callCurlGetFile($uri_file_path, $header_params, $fp);
                fclose($fp);

                $sheets_arr = $this->csv_import->prepareExcelData($temp_folder_path . $file_name, 5);
                $params_arr['docFile'] = $file_name;

                $full_arr['sheets'] = $sheets_arr;
            }

            $render_arr['data'] = $full_arr;
            $success = 1;
            $message = "Access granted.";
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $render_arr['success'] = $success;
        $render_arr['message'] = $message;
        $render_arr['params'] = $params_arr;

        echo $this->parser->parse('import_gdrive_sheets', $render_arr, TRUE);
    }

    public function save_gdrive_data()
    {
        $params_arr = array();
        $params_arr['docFile'] = $this->input->get_post('docFile', TRUE);
        $params_arr['sheetId'] = $this->input->get_post('sheetId', TRUE);
        $access_token = $this->session->userdata('__oauth_api_token');
        try {
            if (!isset($access_token) || trim($access_token) == "") {
                throw new Exception("Google authentication failed.");
            }
            $header_params = array();
            $header_params[] = "Authorization: Bearer " . $access_token;

            $sheetId = $params_arr['sheetId'];

            $temp_folder_path = $this->config->item('admin_upload_temp_path');
            $file_name = $params_arr['docFile'];

            $sheet_arr = $this->csv_import->prepareExcelData($temp_folder_path . $file_name, -1, $sheetId);
            @unlink($temp_folder_path . $file_name);

            $temp_folder_path = $this->config->item('admin_upload_temp_path');
            $file_name = substr_replace($file_name, ".csv", -5, 5);
            $file_path = $temp_folder_path . $file_name;
            $fp2 = fopen($file_path, 'w');
            foreach ($sheet_arr[0]['rows'] as $v) {
                fputcsv($fp2, $v);
            }
            fclose($fp2);
            $this->skip_template_view();

            $success = 1;
            $message = "Data saved successfully.";
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $ret_arr['success'] = $success;
        $ret_arr['message'] = $message;
        $ret_arr['file_name'] = $file_name;

        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    public function dropbox_auth()
    {
        $_nA = $this->input->get_post('_nA', TRUE);
        $code = $this->input->get_post('code', TRUE);
        if (!is_null($_nA) && $_nA == '1') {
            unset($_SESSION['OAUTH_ACCESS_TOKEN']);
            unset($_SESSION['OAUTH_STATE']);
            unset($_SESSION['__oauth_api_token']);
            $this->session->unset_userdata('__oauth_api_token');
            $this->session->unset_userdata('OAUTH_ACCESS_TOKEN');
            $this->session->unset_userdata('OAUTH_STATE');
        }

        try {

            require(APPPATH . 'third_party/oauth/vendor/autoload.php');
            $client = new oauth_client_class;

            $client->server = 'Dropbox2';
            $client->debug = false;
            $client->debug_http = true;
            $client->configuration_file_path = APPPATH . 'third_party/oauth/vendor/phpclasses/oauth-api/';
            $client->redirect_uri = $this->_dropbox_redirect_uri;
            $client->client_id = $this->config->item('DROPBOX_OAUTH_CLIENT_ID');
            $client->client_secret = $this->config->item('DROPBOX_OAUTH_CLIENT_SECRET');

            if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0) {
                throw new Exception('Please go to Dropbox APIs console page ' .
                'https://www.dropbox.com/developers in the API access link, ' .
                'create a new client ID, and set the client_id to Client ID and client_secret. ' .
                'The callback URL must be ' . $client->redirect_uri . ' but make sure ' .
                'the domain is valid and can be resolved by a public DNS.');
            }

            if ($success = $client->Initialize()) {
                if ($success = $client->Process()) {
                    if (strlen($client->authorization_error)) {
                        throw new Exception($client->authorization_error);
                    }
                }
                $success = $client->Finalize($success);
                $this->session->set_userdata('__oauth_api_token', $client->access_token);
            }
            if (!isset($client->access_token) || trim($client->access_token) == "") {
                throw new Exception($client->error);
            }
            $success = 1;
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $render_arr = array(
            "api" => 'dropbox',
            "success" => $success,
            "message" => $message,
        );
        $htm_str = $this->parser->parse('import_gdrive_auth_redirect', $render_arr, true);
        echo $htm_str;
        $this->skip_template_view();
    }

    public function get_dropbox_data()
    {
        $render_arr = $params_arr = array();
        $access_token = $this->session->userdata('__oauth_api_token');
        $params_arr['type'] = $this->input->get_post('type', TRUE);
        try {
            if (!isset($access_token) || trim($access_token) == "") {
                throw new Exception("Dropbox authentication required.");
            }
            $header_params = array();
            $header_params[] = "Authorization: Bearer " . $access_token;
            if ($params_arr['type'] == 'files') {
                $culr_exts = explode("|", $this->import_settings["file_extensions"]);
                $culr_parmas = array();
                foreach ($culr_exts as $cxt) {
                    $culr_parmas[] = array('url' => 'https://api.dropbox.com/1/search/auto?query=.' . $cxt, 'headers' => $header_params);
                }
                $files_response = $this->csv_import->callMultiCurlGet($culr_parmas, TRUE);
                $files_list = $this->csv_import->prepareFiles($files_response);
                $render_arr['assets'] = $files_list;
                $success = 1;
                $message = "Access granted.";
            } else if ($params_arr['type'] == 'content') {
                $params_arr['fileId'] = $this->input->get_post('fileId', TRUE);
                $params_arr['fileRev'] = $this->input->get_post('fileRev', TRUE);

                $fileId = $params_arr['fileId'];
                $fileRev = $params_arr['fileRev'];
                $uri_file_path = "https://api-content.dropbox.com/1/files/auto" . $fileId . "?rev=" . $fileRev;
                $fileName = basename($fileId);
                list($s_file_name, $s_extension) = $this->general->get_file_attributes($fileName);

                $temp_folder_path = $this->config->item('admin_upload_temp_path');
                $file_name = $s_file_name . "." . $s_extension;

                $fp = fopen($temp_folder_path . $file_name, "wb");
                $this->csv_import->callCurlGetFile($uri_file_path, $header_params, $fp);
                fclose($fp);

                $data_arr = $this->csv_import->prepareExcelData($temp_folder_path . $file_name, 5);
                $params_arr['docFile'] = $file_name;
                $render_arr['data'] = $data_arr;
            }
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $render_arr['success'] = $success;
        $render_arr['message'] = $message;
        $render_arr['params'] = $params_arr;

        echo $this->parser->parse('import_dropbox_assetlist', $render_arr, TRUE);
    }

    public function save_dropbox_data()
    {
        $params_arr = array();
        $params_arr['docFile'] = $this->input->get_post('docFile', TRUE);
        $params_arr['fileTabId'] = $this->input->get_post('fileTabId', TRUE);
        $access_token = $this->session->userdata('__oauth_api_token');
        try {
            if (!isset($access_token) || trim($access_token) == "") {
                throw new Exception("Dropbox authentication failed.");
            }
            $header_params = array();
            $header_params[] = "Authorization: Bearer " . $access_token;

            $fileTabId = $params_arr['fileTabId'];

            $temp_folder_path = $this->config->item('admin_upload_temp_path');
            $file_name = $params_arr['docFile'];

            $data_arr = $this->csv_import->prepareExcelData($temp_folder_path . $file_name, -1, $fileTabId);
            @unlink($temp_folder_path . $file_name);

            if (substr($file_name, -5) == ".xlsx") {
                $file_name = substr_replace($file_name, ".csv", -5, 5);
            } else {
                $file_name = substr_replace($file_name, ".csv", -4, 4);
            }

            $file_path = $temp_folder_path . $file_name;
            $fp2 = fopen($file_path, 'w');
            foreach ($data_arr[0]['rows'] as $v) {
                fputcsv($fp2, $v);
            }
            fclose($fp2);

            $success = 1;
            $message = "Data saved successfully.";
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $ret_arr['success'] = $success;
        $ret_arr['message'] = $message;
        $ret_arr['file_name'] = $file_name;

        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    public function get_weburl_data()
    {
        $web_url = $this->input->get_post('web_url', TRUE);
        $url_type = $this->input->get_post('url_type', TRUE);
        $response = "";
        if ($url_type == "xml") {
            $response = $this->csv_import->callCurlGet($web_url, array(), TRUE);
            $response = json_encode($response);
        } else if ($url_type == "json") {
            $response = $this->csv_import->callCurlGet($web_url);
        }
        echo $response;
        $this->skip_template_view();
    }

    public function import_valid()
    {
        $module_name = $this->input->get_post('module_name', TRUE);
        $type = $this->input->get_post('type', TRUE);
        $this->config->load('hb_importdata', TRUE);
        $import_modules = $this->config->item('hb_importdata');
        $current_module = $import_modules[$module_name];
        $render_arr['columns'] = $current_module['cols'];
        $render_arr['type'] = $type;
        $this->smarty->assign($render_arr);
        $this->loadView("import_valid");
    }

    public function import_info()
    {
        $import_module_name = $this->input->get_post('module_name', TRUE);
        $import_file_name = $this->input->get_post('file_name', TRUE);
        $import_first_row = $this->input->get_post('first_row', TRUE);
        $import_columns_separator = $this->input->get_post('columns_separator', TRUE);
        $import_text_delimiter = $this->input->get_post('text_delimiter', TRUE);
        $import_type = $this->input->get_post('type', TRUE);
        $map_column_arr = $this->input->get_post('map_column', TRUE);
        $skip_column_arr = $this->input->get_post('skip_column', TRUE);

        $map_column_arr = explode("@@", $map_column_arr);
        $skip_column_arr = explode("@@", $skip_column_arr);
        $info = array();
        try {
            $temp_folder_path = $this->config->item('admin_upload_temp_path');
            $file_path = $temp_folder_path . DS . $import_file_name;
            if (!is_file($file_path)) {
                throw new Exception($this->general->processMessageLabel('ACTION_FILE_NOT_FOUND_C46_C46_C33'));
            }

            //load the excel library
            $this->load->library('csv_import');
            $result = $this->csv_import->readExcelContent($file_path, $import_columns_separator, $import_text_delimiter);

            if (!$result['success']) {
                throw new Exception($result['message']);
            }

            $this->config->load('hb_importdata', TRUE);
            $import_modules = $this->config->item('hb_importdata');
            $current_module = $import_modules[$import_module_name];

            if (!is_array($current_module) || count($current_module) == 0) {
                throw new Exception("Import module configuration not found.");
            }

            $import_table_name = $current_module['table'];
            $columns_arr = $current_module['cols'];
            if (!is_array($columns_arr) || count($columns_arr) == 0) {
                throw new Exception("Import module mapping columns not found.");
            }

            $total = $result['data'];
            if ($import_first_row == "Yes") {
                unset($total[0]);
            }

            $data = array_values($total);
            if (!is_array($data) || count($data) == 0) {
                throw new Exception("No data found to show.");
            }
            $matched_arr = array();
            if ($import_type == 'success') {
                $inserted_arr = @explode("@@", $this->input->get_post('inserted', TRUE));
                $updated_arr = @explode("@@", $this->input->get_post('updated', TRUE));
                $matched_arr = array_merge($inserted_arr, $updated_arr);
                $heading = "Successful Records";
            } else if ($import_type == 'failed') {
                $failed_arr = @explode("@@", $this->input->get_post('failed', TRUE));
                $matched_arr = $failed_arr;
                $heading = "Failed Records";
            } else if ($import_type == 'duplicate') {
                $duplicate_arr = @explode("@@", $this->input->get_post('duplicate', TRUE));
                $matched_arr = $duplicate_arr;
                $heading = "Duplicate Records";
            } else if ($import_type == 'skipped') {
                $valid_arr = @explode("@@", $this->input->get_post('valid', TRUE));
                $lookup_arr = @explode("@@", $this->input->get_post('lookup', TRUE));
                $matched_arr = array_merge($valid_arr, $lookup_arr);
                $heading = "Skipped Records";
            }
            $matched_arr = is_array($matched_arr) ? array_filter(array_unique($matched_arr)) : array();

            if (!is_array($matched_arr) || count($matched_arr) == 0) {
                throw new Exception("No data found to show.");
            }
            $header = array();
            $h = FALSE;
            for ($i = 0, $n = 1; $i < count($data); $i++, $n++) {
                if (in_array($n, $matched_arr)) {
                    $tmp_arr = array();
                    $tmp_arr[] = $n;
                    for ($j = 0; $j < count($map_column_arr); $j++) {
                        if (!in_array($j, $skip_column_arr)) {
                            continue;
                        }
                        $key = trim($map_column_arr[$j]);
                        $val = trim($data[$i][$j]);
                        $col = $columns_arr[$key];
                        if ($h === FALSE) {
                            $title = $this->lang->line($col['name']);
                            $header[] = ($title) ? $title : $col['name'];
                        }
                        $tmp_arr[] = $val;
                    }
                    $h = TRUE;
                    $info[] = $tmp_arr;
                }
            }
            $success = 1;
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        $render_arr = array(
            "success" => $success,
            "message" => $message,
            "header" => $header,
            "data" => $info,
            "title" => $heading,
        );

        $this->smarty->assign($render_arr);
        $this->loadView('import_info');
    }

    public function import_history()
    {
        $import_files_url = $this->config->item('import_files_url');
        $import_folder_path = $this->config->item('import_files_path');
        $histroy_file = $import_folder_path . "import_history.json";
        $history_json = file_get_contents($histroy_file);
        $history_data = json_decode($history_json, TRUE);
        $history_data = is_array($history_data) ? array_reverse($history_data) : array();
        $render_arr = array(
            'data' => $history_data,
            'import_files_url' => $import_files_url
        );
        $this->smarty->assign($render_arr);
        $this->loadView('import_history');
    }
}
