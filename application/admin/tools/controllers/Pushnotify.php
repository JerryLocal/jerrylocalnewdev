<?php
/**
 * Description of PushNotify Controller
 * 
 * @module PushNotify
 * 
 * @class pushnotify.php
 * 
 * @path application\admin\tools\controllers\pushnotify.php
 * 
 * @author Ajay Gandham
 * 
 * @date 18.03.2014
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class PushNotify extends HB_Controller
{

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    function __construct()
    {
        parent::__construct();
        $this->folder_name = "tools";
        $this->module_name = "pushnotify";
        $this->mod_url_cod = array(
            "pushnotify_action",
            "pushnotify_variables",
            "pushnotify_module_fields",
            "pushnotify_select_fields"
        );
        $this->get_arr = is_array($this->input->get(NULL, TRUE)) ? $this->input->get(NULL, TRUE) : array();
        $this->post_arr = is_array($this->input->post(NULL, TRUE)) ? $this->input->post(NULL, TRUE) : array();
        $this->params_arr = array_merge($this->get_arr, $this->post_arr);
        $this->mod_enc_url = $this->general->getCustomEncryptURL($this->mod_url_cod, TRUE);
        $this->load->model('model_pushnotify');
        $this->load->library('filter');
        $this->load->library('ci_misc');
    }

    /**
     * index method is used to display bulk mail form to send emails.
     */
    public function index()
    {
        $this->filter->getModuleWiseAccess("PushNotify", "Add", FALSE);
        $db_module_data = $this->ci_misc->getPushNotifyModules();
        $db_notify_code = array(
            "USER" => "USER",
            "ALERT" => "ALERT"
        );
        if (is_array($db_module_data) && ($db_module_data) > 0) {
            $db_module['Module'] = $db_module_data;
        }
        $sound_arr = $this->config->item('PUSH_NOTIFY_SOUND_ARR');
        $field_arr = $this->ci_misc->getPushNotifyModuleListFields();
        $render_arr = array(
            'db_module' => $db_module,
            'db_notify_code' => $db_notify_code,
            'sound_arr' => $sound_arr,
            'mod_enc_url' => $this->mod_enc_url,
            'field_arr' => $field_arr
        );
        $this->smarty->assign($render_arr);
        $this->loadView('pushnotify');
    }

    public function pushnotify_variables()
    {
        $params_arr = $this->params_arr;
        $row_id = $params_arr['row_id'];
        $dis_no = $params_arr['dis_no'];
        $device_id = $params_arr['device_id'];
        $send_details = explode('@@', $device_id);
        $type = $send_details[0];
        $value = $send_details[1];
        $field_arr = $this->ci_misc->getPushNotifyModuleListFields($value);
        $render_arr = array(
            "row_id" => $row_id,
            'dis_no' => $dis_no,
            'field_arr' => $field_arr,
            'select_fields' => 'No',
        );
        $this->smarty->assign($render_arr);
        $this->loadView("ajax_pushnotify_variables");
    }

    public function pushnotify_module_fields()
    {
        $sent_id = $this->input->get_post('sent_id');
        if (trim($sent_id) == 'Other') {
            $sent_type = "Other";
        } else {
            $sent_idarr = explode("@@", $sent_id);
            $sent_type = "Modules";
            $id = $sent_idarr[1];
            $params_arr['module_name'] = $id;
            $email_arr = $this->ci_misc->getPushNotifyModuleFields($params_arr);
        }
        $render_arr = array(
            'sent_id' => $sent_id,
            'sent_type' => $sent_type,
            'email_arr' => $email_arr
        );
        $this->smarty->assign($render_arr);
        $this->loadView('ajax_push_notify_modules');
    }

    public function pushnotify_select_fields()
    {
        $params_arr = $this->params_arr;
        $device_id = $params_arr['device_id'];
        $send_details = explode('@@', $device_id);
        $type = $send_details[0];
        $value = $send_details[1];
        $field_arr = $this->ci_misc->getPushNotifyModuleListFields($value);
        $render_arr = array(
            'select_fields' => 'Yes',
            'field_arr' => $field_arr
        );
        $this->smarty->assign($render_arr);
        $this->loadView("ajax_pushnotify_variables");
    }

    /**
     * pushnotify_action method is used to send push notifications to the specfied receivers.
     */
    function pushnotify_action()
    {
        try {
            $limit = $this->config->item('WS_PUSH_LIMIT');
            $this->load->model('tools/push');
            $send_to = $this->input->post('vSendTo');
            $device_ids = $this->input->post('iDeviceId');
            $device_field_name = trim($this->input->post('vFieldName'));
            $token_mode = $this->input->post('vTokenMode');
            $code = $this->input->post('vCode');
            $sound = $this->input->post('vSound');
            $badge = $this->input->post('vBadge');
            $title = $this->input->post('vButtonTitle');
            $message = $this->input->post('vMessage');
            $push_notify_variable_arr = $this->input->post('push_notify_variable');
            $push_notify_value_arr = $this->input->post('push_notify_value');
            $push_notify_other_arr = $this->input->post('push_notify_value_other');
            $push_notify_comp_arr = $this->input->post('push_notify_value_compulsory');

            $device_var_arr = array();
            if ($send_to == 'Other') {
                $device_ids_arr = explode(',', $device_ids);
            } else {
                $send_details = explode('@@', $send_to);
                $data_arr = $this->ci_misc->getPushNotifyModuleData($send_details[1]);
                if (is_array($data_arr) && count($data_arr) > 0) {
                    for ($i = 0; $i < count($data_arr); $i++) {
                        $device_token = $data_arr[$i][$device_field_name];
                        $device_ids_arr[] = $device_token;
                        $device_var_arr[$device_token] = $data_arr[$i];
                    }
                }
            }
            $succ = $fail = $cron = 0;
            if (!is_array($device_ids_arr) || count($device_ids_arr) == 0) {
                throw new Exception("Device tokens are missing..!");
            }
            for ($k = 0, $j = 1; $k < count($device_ids_arr); $k++, $j++) {
                $to = $device_ids_arr[$k];
                $row_arr = $device_var_arr[$to];
                $send_vars = $pair_vars = $notify_arr = array();
                if (is_array($push_notify_variable_arr) && count($push_notify_variable_arr) > 0) {
                    foreach ($push_notify_variable_arr as $key => $val) {
                        $param = $val;
                        if ($push_notify_value_arr[$key] == "Other") {
                            $value = $push_notify_other_arr[$key];
                        } else {
                            $value = $row_arr[$push_notify_value_arr[$key]];
                        }
                        $tmp_arr = array();
                        $tmp_arr['key'] = $param;
                        $tmp_arr['value'] = $value;
                        $tmp_arr['send'] = ($push_notify_comp_arr[$key] == "Yes") ? "Yes" : "No";
                        $send_vars[] = $tmp_arr;
                        if ($tmp_arr['send'] == "Yes") {
                            $pair_vars[$param] = $value;
                        }
                    }
                }
                $push_msg = $this->general->getReplacedInputParams($message, $row_arr);

                $send_arr = array();
                $send_arr['device_id'] = $to;
                $send_arr['mode'] = $token_mode;
                $send_arr['code'] = $code;
                $send_arr['sound'] = $sound;
                $send_arr['badge'] = intval($badge);
                $send_arr['title'] = $title;
                $send_arr['message'] = $push_msg;
                $send_arr['variables'] = json_encode($send_vars);

                list($uni_id, $ins_id) = $this->general->insertPushNotification($send_arr, "admin");

                if ($ins_id) {
                    if ($j > $limit) {
                        $cron++;
                    } else {
                        $notify_arr['mode'] = $token_mode;
                        $notify_arr['message'] = $push_msg;
                        $notify_arr['title'] = $title;
                        $notify_arr['badge'] = intval($badge);
                        $notify_arr['sound'] = $sound;
                        $notify_arr['code'] = $code;
                        $notify_arr['id'] = $uni_id;
                        $notify_arr['others'] = $pair_vars;
                        $success = $this->general->pushTestNotification($to, $notify_arr);

                        $update_arr = array();
                        $update_arr['tSendJSON'] = $this->general->getPushNotifyOutput("body");
                        $update_arr['dtExeDateTime'] = date("Y-m-d H:i:s");
                        if ($success) {
                            $update_arr['eStatus'] = 'Executed';
                            $succ++;
                        } else {
                            $update_arr['tError'] = $this->general->getNotifyErrorOutput();
                            $update_arr['eStatus'] = 'Failed';
                            $fail++;
                        }
                        $res = $this->push->updatePushNotify($update_arr, $ins_id);
                    }
                }
            }
            $ret_arr['success'] = 1;
            $msg = '';
            if ($succ == 0 && $cron == 0 && $fail == 0) {
                $msg = 'No notifications sent..!';
                $ret_arr['success'] = 0;
            } else {
                if ($succ > 0) {
                    $msg .= 'Sent notifications(' . $succ . '). ';
                }
                if ($cron > 0) {
                    $msg .= 'Notifications added to cron(' . $cron . '). ';
                }
                if ($fail > 0) {
                    $msg .= 'Failed notifications(' . $fail . ')';
                }
                $ret_arr['success'] = 1;
            }
            $ret_arr['message'] = $msg;
        } catch (Exception $e) {
            $ret_arr['success'] = 0;
            $ret_arr['message'] = $msg;
        }
        $ret_arr['red_type'] = 'Stay';
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }
}
