<%assign var="upload_event_arr" value=[]%>
<%assign var="editor_event_arr" value=[]%>
<%assign var="upload_event_str" value=""%>
<%assign var="editor_event_str" value=""%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%$this->js->add_js("admin/admin/js_settings_page.js")%>
<div id="ajax_qLbar"></div>
<div id="settings" class="headingfix">
    <input type="hidden" name="sess_extra_val" value="<%$type%>" />
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                Settings
            </div>
        </h3>
        <div class="header-right-btns"></div>
        <span style="display: none;position:inherit;" id="ajax_lang_loader">
            <img src="<%$this->config->item('admin_images_url')%>loaders/circular/020.gif">
        </span>
    </div>
</div>

<div id="ajax_content_div" class="ajax-content-div top-frm-spacing settings-class">
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="frm-elem-block">
        <form id="frmsettingslist" name="frmsettingslist" method="post" action="<%$admin_url%><%$action_url%>">
            <input type="hidden" name="mode" value="Update" />
            <input type="hidden" name="type" value="<%$type%>" />
            <%if $db_total|@is_array && $db_total|@count gt 0%>
                <div style="width:98%" class="frm-block-layout pad-calc-container">
                    <%assign var=db_res value=$db_total%>
                    <div class="box">
                        <div class="title"><h4><span><%$type%></span></h4></div>
                        <div class="content clearfix">
                            <%section name="i" loop=$db_res%>
                                <%if $db_res[i]["eLang"] eq 'Yes'%>
                                    <%assign var="aria_multi" value="aria-multi-lingual='parent'"%>
                                <%else%>
                                    <%assign var="aria_multi" value=""%>
                                <%/if%>
                                <%if $db_res[i]["eDisplayType"] eq 'hidden'%>  
                                    <input type="hidden" name="<%$db_res[i]['vName']%>" class="settings-width" value="<%$db_res[i]["vValue"]%>">
                                <%else%>          
                                    <div class="settings-main" >
                                        <div class="settings-left form-label">
                                            <%$db_res[i]["vDesc"]%>
                                        </div>
                                        <div class="settings-right <%if $db_res[$i]['eDisplayType'] eq 'editor'%>frm-editor-layout<%/if%>">
                                            <%if $lang_data[$prlang][$db_res[i]["vName"]]|@trim neq ''%>
                                                <%assign var='data_display' value=$lang_data[$prlang][$db_res[i]["vName"]]%>
                                            <%else%>
                                                <%assign var='data_display' value=$db_res[i]["vValue"]%>
                                            <%/if%>
                                            <%if $db_res[i]["eDisplayType"] eq 'readonly'%>
                                                <%$db_res[i]["vValue"]%>
                                            <%elseif $db_res[i]["eConfigType"] eq "Prices"%>    
                                                <select style="width:12%" name="<%$db_res[i]['vName']%>[eSelectType]"  title="Increase / Decrease">
                                                    <option value="Plus" <%if $db_res[i]["eSelectType"] eq 'Plus'%>selected=true <%/if%> >+</option>
                                                    <option value="Minus"  <%if $db_res[i]["eSelectType"] eq 'Minus'%>selected=true <%/if%> >-</option>
                                                </select>
                                                &nbsp;&nbsp;
                                                <input style="width:25%" type="text" name="<%$db_res[i]['vName']%>[Value]" title="<%$db_res[i]["vDesc"]%>" value="<%$data_display|@trim%>" />
                                                &nbsp;&nbsp;
                                                <select style="width:25%" name="<%$db_res[i]['vName']%>[eSource]"  title="Value / Percent">
                                                    <option value="Value" <%if $db_res[i]["eSource"] eq 'Value'%>selected=true <%/if%> >Value (Eg. 10)</option>
                                                    <option value="Percent" <%if $db_res[i]["eSource"] eq 'Percent'%>selected=true <%/if%> >Percent (Eg. 10%)</option>
                                                </select>
                                            <%elseif $db_res[i]["eDisplayType"] eq 'text'%>
                                                <input type="text" id="<%$db_res[i]['vName']%>" name="<%$db_res[i]['vName']%>" title="<%$db_res[i]["vDesc"]%>" class="settings-width" value="<%($db_res[i]["vValue"]|@trim)|@htmlentities%>" <%$aria_multi%>/>
                                            <%elseif $db_res[i]["eDisplayType"] eq 'textarea'%>   
                                                <textarea row="3"  id="<%$db_res[i]['vName']%>" cols="5" class="settings-width elastic" name="<%$db_res[i]['vName']%>" <%$aria_multi%>><%$data_display|@trim%></textarea>
                                            <%elseif $db_res[i]["eDisplayType"] eq 'checkbox'%>  
                                                <input <%if $db_res[i]["vValue"] == 'Y'%>checked="true"<%/if%> class="noinput regular-checkbox" type="checkbox" name="<%$db_res[i]['vName']%>" id="<%$db_res[i]['vName']%>">
                                                <label for="<%$db_res[i]['vName']%>">&nbsp;</label>
                                            <%elseif $db_res[i]["eDisplayType"] eq 'password'%>
                                                <input type="password" name="<%$db_res[i]['vName']%>" title="<%$db_res[i]["vDesc"]%>" class="settings-width" value="<%$db_res[i]["vValue"]|@htmlentities%>">
                                            <%elseif $db_res[i]["eDisplayType"] eq 'selectbox'%> 
                                                <%assign var="multiple_attr" value=$db_res[i].multiple_attr%>
                                                <%assign var="name_attr" value=$db_res[i].name_attr%>
                                                <%assign var="value_arr" value='|'|@explode:$db_res[i]['vValue']%>
                                                <%assign var="format_arr" value=['ADMIN_DATE_FORMAT','ADMIN_DATE_TIME_FORMAT','ADMIN_TIME_FORMAT']%>
                                                <%if $db_res[i]["eSource"] eq 'List'%>
                                                    <%assign var="source_arr" value=$db_res[i].source_arr%>
                                                    <select class="settings-width chosen-select" name="<%$name_attr%>" <%$multiple_attr%> data-placeholder='<< Select <%$db_res[i]["vDesc"]%> >>'>
                                                        <option value="-9"></option>
                                                        <%section name="j" loop=$source_arr%>
                                                            <%assign var="list_arr" value='::'|@explode:$source_arr[j]%>
                                                            <%assign var="setting_key" value= $list_arr[0]%>
                                                            <%assign var="setting_val" value= $list_arr[1]%>
                                                            <%if ($db_res[i]['eConfigType'] eq 'Formats' && $db_res[i]['vName']|@in_array:$format_arr) %>
                                                                <%assign var="setting_val" value=$this->filter->getDateTimeFormatDropdown($db_res[i]['vName'], $setting_key)%>
                                                            <%/if%>
                                                            <%if $setting_val eq ""%>
                                                                <%assign var="setting_val" value=$setting_key%>
                                                            <%/if%>
                                                            <%assign var="selected" value=""%>
                                                            <%if $db_res[i]["eSelectType"] eq 'Multiple' && $setting_key|@in_array:$value_arr%>
                                                                <%assign var="selected" value="selected='selected'" %>
                                                            <%elseif $setting_key eq $db_res[i]["vValue"]%>
                                                                <%assign var="selected" value="selected='selected'" %>
                                                            <%/if%>   
                                                            <option value="<%$setting_key%>" <%$selected%>><%$setting_val%></option>
                                                        <%/section%>   
                                                    </select>
                                                <%/if%>
                                                <%if $db_res[i]["eSource"] eq 'Query'%>
                                                    <%assign var="db_select_source_rs" value=$db_res[i]['db_select_source_rs']%>
                                                    <select class="settings-width chosen-select" name="<%$name_attr%>" <%$multiple_attr%> data-placeholder='<< Select <%$db_res[i]["vDesc"]%> >>'>
                                                        <option value="-9"></option>
                                                        <%section name="j" loop=$db_select_source_rs%>
                                                            <%assign var="selected" value=""%>
                                                            <%if $db_res[i]["eSelectType"] eq 'Multiple' && $db_select_source_rs[j]['id']|@in_array:$value_arr%>
                                                                <%assign var="selected" value="selected='selected'" %>
                                                            <%elseif $db_select_source_rs[j]['id'] eq $db_res[i]["vValue"] %>
                                                                <%assign var="selected" value="selected='selected'" %>
                                                            <%/if%> 
                                                            <option value="<%$db_select_source_rs[j]['id']%>" <%$selected%>><%$db_select_source_rs[j]['val']%></option>
                                                        <%/section%>    
                                                    </select>
                                                <%/if%>
                                            <%elseif $db_res[i]["eDisplayType"] eq 'editor'%>
                                                <%assign var="elename" value="editor_"|@cat:$db_res[i]["vName"]%>
                                                <%assign var="editor_event_arr" value=$editor_event_arr|@array_merge:array($elename)%>
                                                <textarea title="<%$db_res[i]['vDesc']%>" id="<%$elename%>" value="" name="<%$db_res[i]['vName']%>" style="width:100%;min-height:300px;"><%$data_display|@stripslashes%></textarea>
                                            <%elseif $db_res[i]["eDisplayType"] eq 'file'%>
                                                <%assign var="vName" value=$db_res[i]["vName"]%>
                                                <%assign var="vValue" value=$db_res[i]["vValue"]%>
                                                <%assign var="fileSize" value=$db_res[i]["vSourceValue"]["FILE_SIZE"]%>
                                                <%assign var="fileExt" value=$db_res[i]["vSourceValue"]["FILE_EXT"]%>
                                                <%assign var="view_file_url" value=""%>
                                                <%assign var="fileType" value="file"%>
                                                <%assign var="fileExisted" value="0"%>
                                                <%if $vValue|trim neq ""%>
                                                    <%assign var="extarr" value="."|@explode:$vValue%>
                                                    <%assign var="extval" value=$extarr|@end|@strtolower%>
                                                    <%if $extval|@in_array:$image_extension_arr%>
                                                        <%assign var="fileType" value="image"%>
                                                    <%/if%>
                                                    <%assign var="view_file_path" value=$settings_files_path|@cat:$vValue%>
                                                    <%assign var="view_file_url" value=$settings_files_url|@cat:$vValue%>
                                                    <%if $view_file_path|@file_exists %>
                                                        <%assign var="fileExisted" value="1"%>
                                                    <%/if%>
                                                <%/if%>
                                                <%assign var="upload_event_arr_temp" value=["name" => $vName, "val" => $vValue, "file_exist" => $fileExisted, "file_url" => $view_file_url, "file_type" => $fileType, "fwidth" => $image_width, "fheight" => $image_height, "file_size" => $fileSize, "file_ext" => $fileExt]%>
                                                <%assign var="upload_event_arr" value=$upload_event_arr|@array_merge:array($upload_event_arr_temp)%>
                                                <div>
                                                    <div class="btn-uploadify frm-size-small">
                                                        <input type='hidden' value='<%$vValue%>' name='old_<%$vName%>' id='old_<%$vName%>' />
                                                        <input type='hidden' value='<%$vValue%>' name='<%$vName%>' id='<%$vName%>' />
                                                        <input type='hidden' value='<%$vValue%>' name='temp_<%$vName%>' id='temp_<%$vName%>' />
                                                        <div id="upload_drop_zone_<%$vName%>" class="upload-drop-zone"></div>
                                                        <div class="uploader upload-src-zone">
                                                            <input type='file' name='uploadify_<%$vName%>' id='uploadify_<%$vName%>' title='<%$db_res[$i]["vDesc"]%>' />
                                                            <span class="filename" id="preview_<%$vName%>"><%$vValue%></span>
                                                            <span class="action">Choose File</span>
                                                        </div>
                                                    </div>
                                                    <div class="upload-image-btn">
                                                        <div id="img_buttons_<%$vName%>" class="img-inline-display">
                                                            <div id="img_view_<%$vName%>" class="img-view-section">
                                                                <%if $fileExisted eq '0'%>
                                                                    <span class="errormsg">Not Available</span>
                                                                <%/if%>
                                                            </div>
                                                            <div id="img_del_<%$vName%>" class="img-del-section"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clear upload-progress" id="progress_<%$vName%>">
                                                        <div class="upload-progress-bar progress progress-striped active">
                                                            <div class="bar" id="practive_<%$vName%>"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <%/if%>   
                                            <%if $db_res[i]['tHelpText']|trim neq ""%>
                                                <span class="input-comment">
                                                    <a href="javascript://"  class="tipR" title="<%$db_res[i]['tHelpText']%>">
                                                        <span class="icomoon-icon-help"></span>
                                                    </a>
                                                </span>
                                            <%/if%>
                                            <label id="<%$db_res[i]['vName']%>Err" class="error"></label>
                                        </div>
                                        <%if $lang_fields|@is_array && $lang_fields|@count gt 0%>
                                            <%if $db_res[i]["vName"]|@in_array:$lang_fields%>
                                                <%if $exlang_arr|@is_array && $exlang_arr|@count gt 0%>
                                                    <%section name=ml loop=$exlang_arr%>
                                                        <%assign var="exlang" value=$exlang_arr[ml]%>
                                                        <div class="clear" id="lnsh_<%$db_res[i]['vName']%>_<%$exlang%>" style="<%if $exlang neq $dflang%>display:none;<%/if%>">
                                                            <div class="settings-left form-label">
                                                                <%$db_res[i]["vDesc"]%> [<%$lang_info[$exlang]['vLangTitle']%>]
                                                            </div>
                                                            <div class="settings-right <%if $db_res[$i]['eDisplayType'] eq 'editor'%>frm-editor-layout<%/if%>">
                                                                <%if $db_res[i]["eDisplayType"] eq 'text'%>
                                                                    <input type="text" name="lang<%$db_res[i]['vName']%>[<%$exlang%>]" id="lang_<%$db_res[i]['vName']%>_<%$exlang%>" class="settings-width" value="<%$lang_data[$exlang][$db_res[i]['vName']]|@trim%>" />
                                                                <%elseif $db_res[i]["eDisplayType"] eq 'textarea'%>   
                                                                    <textarea placeholder="" name="lang<%$db_res[i]['vName']%>[<%$exlang%>]" id="lang_<%$db_res[i]['vName']%>_<%$exlang%>" class='elastic settings-width'  ><%$lang_data[$exlang][$db_res[i]["vName"]]%></textarea>
                                                                <%elseif $db_res[i]["eDisplayType"] eq 'editor'%>
                                                                    <%assign var="temp_elename" value=$db_res[i]["vName"]|@cat:$exlang%>
                                                                    <%assign var="lang_elename" value="lang_editor_"|@cat:$temp_elename%>
                                                                    <%assign var="editor_event_arr" value=$editor_event_arr|@array_merge:array($lang_elename)%>
                                                                    <textarea id="<%$lang_elename%>" name="lang<%$db_res[i]['vName']%>[<%$exlang%>]" style="width:100%;min-height:300px;"><%$lang_data[$exlang][$db_res[i]["vName"]]%></textarea>
                                                                <%/if%>    
                                                            </div>
                                                        </div>
                                                    <%/section%>    
                                                    <div class="lang-flag-css clear" style="margin-left:35%!important">
                                                        <%$this->general->getAdminLangFlagHTML($db_res[i]["vName"], $exlang_arr, $lang_info)%>
                                                    </div>
                                                <%/if%>
                                            <%/if%>
                                        <%/if%> 
                                        <div class="clear"></div>
                                    </div>
                                <%/if%>       
                            <%/section%>    
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <%if $edit_access eq '1'%>
                    <div class="frm-bot-btn <%$rl_theme_arr['frm_gener_action_bar']%> <%$rl_theme_arr['frm_gener_action_btn']%>">
                        <div class="action-btn-align">
                            <input type="submit" value="Save" onclick="return Project.modules.settings.getAdminSettingsValidate()" class='btn btn-info'>&nbsp;&nbsp;
                            <input type="button" value="Discard" class='btn' onclick="loadAdminSiteMapPage()">
                        </div>
                    </div>
                <%/if%>
            <%else%>
                <div class="errormsg" align="center">No records found</div>
            <%/if%>
        </form>
    </div>
</div>
<%if $editor_event_arr|@is_array && $editor_event_arr|@count gt 0%>
    <%assign var="editor_event_str" value=$editor_event_arr|@json_encode%>
<%/if%>
<%if $upload_event_arr|@is_array && $upload_event_arr|@count gt 0%>
    <%assign var="upload_event_str" value=$upload_event_arr|@json_encode%>
<%/if%>
<%javascript%> 
    <%$this->js->add_js('admin/validate/addon.validation.js')%>
    var editor_events_arr = "", upload_events_arr = "";
    var $upload_form_file = admin_url+'<%$upload_url%>';
    <%if $editor_event_str|trim neq ''%>   
        editor_events_arr = $.parseJSON('<%$editor_event_str%>');
    <%/if%>
    
    <%if $upload_event_str|trim neq ''%>   
        upload_events_arr = $.parseJSON('<%$upload_event_str%>');
    <%/if%>

    $(function(){
        $('#frmsettingslist').validate({
            <%$validate_rules%>
        });
    });
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
    <%$this->css->css_src()%>
<%/if%>
    
<%javascript%>
    Project.modules.settings.init();
    var prime_lang_code = '<%$prlang%>';
    var other_lang_JSON = '<%$exlang_arr|@json_encode%>';
    intializeLanguageAutoEntry(prime_lang_code, other_lang_JSON);
<%/javascript%>