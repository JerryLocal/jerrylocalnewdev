            

<div class="jq-subgrid-block">
    <div id="<%$subgrid_pager_id%>"></div>
    <table id="<%$subgrid_table_id%>"></table>
</div>
<%javascript%>
    var el_subgrid_settings = {}, sub_js_col_name_json = {}, sub_js_col_model_json = {};
           
    el_subgrid_settings['table_id'] = '<%$subgrid_table_id%>';
    el_subgrid_settings['pager_id'] = '<%$subgrid_pager_id%>';
    el_subgrid_settings['module_name'] = '<%$module_name%>';
    el_subgrid_settings['advanced_grid'] = '<%$exp_advanced_grid%>';
    el_subgrid_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_subgrid_settings['extra_qstrs'] = '<%$extra_qstr%>';
    el_subgrid_settings['par_module'] = '<%$exp_module_name%>';
    el_subgrid_settings['par_data'] = '<%$exp_par_id%>';
    el_subgrid_settings['par_field'] = '<%$exp_par_field%>';
    el_subgrid_settings['par_type'] = 'grid';
            
    el_subgrid_settings['add_page_url'] = '<%$mod_enc_url["add"]%>'; 
    el_subgrid_settings['edit_page_url'] = admin_url+'<%$mod_enc_url["inline_edit_action"]%>?<%$extra_qstr%>';
    el_subgrid_settings['listing_url'] = admin_url+'<%$mod_enc_url["listing"]%>?<%$extra_qstr%>';
    el_subgrid_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_subgrid_settings['auto_complete_url'] = admin_url+'<%$mod_enc_url["get_token_auto_complete"]%>?<%$extra_qstr%>';
    el_subgrid_settings['nesgrid_listing_url'] =  admin_url+'<%$mod_enc_url["get_subgrid_block"]%>?<%$extra_qstr%>';
    
    el_subgrid_settings['admin_rec_arr'] = {};
    el_subgrid_settings['status_arr'] = $.parseJSON('<%$status_array|@json_encode%>');
    el_subgrid_settings['status_lang_arr'] = $.parseJSON('<%$status_array_label|@json_encode%>');
            
    el_subgrid_settings['hide_add_btn'] = '';
    el_subgrid_settings['hide_del_btn'] = '';
    el_subgrid_settings['hide_status_btn'] = '';
    
    el_subgrid_settings['hide_advance_search'] = 'Yes';
    el_subgrid_settings['hide_search_tool'] = 'Yes';
    el_subgrid_settings['hide_multi_select'] = 'Yes';
    el_subgrid_settings['popup_add_form'] = 'No';
    el_subgrid_settings['popup_edit_form'] = 'No';
    el_subgrid_settings['popup_add_size'] = ['75%', '75%'];
    el_subgrid_settings['popup_edit_size'] = ['75%', '75%'];
    el_subgrid_settings['hide_paging_btn'] = 'Yes';
    el_subgrid_settings['hide_refresh_btn'] = 'Yes';
    el_subgrid_settings['group_search'] = '';
    
    el_subgrid_settings['permit_add_btn'] = '<%$add_access%>';
    el_subgrid_settings['permit_del_btn'] = '<%$del_access%>';
    el_subgrid_settings['permit_edit_btn'] = '<%$view_access%>';
    
    el_subgrid_settings['default_sort'] = 'trh_action_date';
    el_subgrid_settings['sort_order'] = 'asc';
    
    el_subgrid_settings['footer_row'] = 'No';
    el_subgrid_settings['grouping'] = 'No';
    el_subgrid_settings['group_attr'] = {};
    
    el_subgrid_settings['inline_add'] = 'No';
    el_subgrid_settings['rec_position'] = 'Top';
    el_subgrid_settings['auto_width'] = 'Yes';
    el_subgrid_settings['nesgrid'] = '<%$exp_nested_grid%>';
    el_subgrid_settings['rating_allow'] = 'No';
    el_subgrid_settings['listview'] = 'list';
    el_subgrid_settings['top_filter'] = [];
    el_subgrid_settings['buttons_arr'] = [];
    
    sub_js_col_name_json = [{
        "name": "trh_action_date",
        "label": "<%$this->lang->line('REASONHISTORY_ACTION_DATE')%>"
    },
    {
        "name": "trh_action_detail",
        "label": "<%$this->lang->line('REASONHISTORY_REASON_COMMENT')%>"
    },
    {
        "name": "trh_action_type",
        "label": "<%$this->lang->line('REASONHISTORY_REASON_TYPE')%>"
    }];

    sub_js_col_model_json = [{
        "name": "trh_action_date",
        "index": "trh_action_date",
        "label": "<%$list_config['trh_action_date']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['trh_action_date']['width']%>",
        "search": <%if $list_config['trh_action_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trh_action_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trh_action_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trh_action_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trh_action_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "reasonhistory",
                "aria-unique-name": "trh_action_date",
                "autocomplete": "off",
                "class": "search-inline-date",
                "aria-date-format": "<%$this->general->getAdminJSMoments('date')%>"
            },
            "sopt": dateSearchOpts,
            "searchhidden": <%if $list_config['trh_action_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchGridDateRangePicker
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "reasonhistory",
            "aria-unique-name": "trh_action_date",
            "aria-date-format": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
            "aria-min": "",
            "aria-max": "",
            "placeholder": "",
            "class": "inline-edit-row inline-date-edit date-picker-icon dateOnly"
        },
        "ctrl_type": "date",
        "default_value": "<%$list_config['trh_action_date']['default']%>",
        "filterSopt": "bt",
        "formatter": formatAdminModuleEditLink,
        "unformat": unformatAdminModuleEditLink
    },
    {
        "name": "trh_action_detail",
        "index": "trh_action_detail",
        "label": "<%$list_config['trh_action_detail']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['trh_action_detail']['width']%>",
        "search": <%if $list_config['trh_action_detail']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trh_action_detail']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trh_action_detail']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trh_action_detail']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trh_action_detail']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "textarea",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "reasonhistory",
                "aria-unique-name": "trh_action_detail",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['trh_action_detail']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "reasonhistory",
            "aria-unique-name": "trh_action_detail",
            "rows": "1",
            "placeholder": "",
            "dataInit": initEditGridElasticEvent,
            "class": "inline-edit-row inline-textarea-edit "
        },
        "ctrl_type": "textarea",
        "default_value": "<%$list_config['trh_action_detail']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "trh_action_type",
        "index": "trh_action_type",
        "label": "<%$list_config['trh_action_type']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['trh_action_type']['width']%>",
        "search": <%if $list_config['trh_action_type']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trh_action_type']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trh_action_type']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trh_action_type']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trh_action_type']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "reasonhistory",
                "aria-unique-name": "trh_action_type",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['trh_action_type']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["trh_action_type"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=trh_action_type&mode=<%$mod_enc_mode["Search"]%>&rformat=html'<%/if%>,
            "value": <%if $count_arr["trh_action_type"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["trh_action_type"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['trh_action_type']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["trh_action_type"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "reasonhistory",
            "aria-unique-name": "trh_action_type",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=trh_action_type&mode=<%$mod_enc_mode["Update"]%>&rformat=html',
            "dataInit": <%if $count_arr['trh_action_type']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["trh_action_type"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'REASONHISTORY_REASON_TYPE')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['trh_action_type']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    }];
                  
    initSubGridListing();
<%/javascript%>
    