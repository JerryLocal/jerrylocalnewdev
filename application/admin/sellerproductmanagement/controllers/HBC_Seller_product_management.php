<?php
            
/**
 * Description of Seller Product Management Extended Controller
 * 
 * @module Extended Seller Product Management
 * 
 * @class HBC_Seller_product_management.php
 * 
 * @path application\admin\sellerproductmanagement\controllers\HBC_Seller_product_management.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 24.11.2015
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
 
Class HBC_Seller_product_management extends Seller_product_management {
        function __construct() {
    parent::__construct();
 	$this->load->model("model_seller_product_management");
}

function delete_product(){
	$render_arr = $this->input->get_post();
	$ret_arr=array();
	$id=trim($render_arr['id']);
    $status = 0;
	$message = "Failure in deleting products";
	if(strlen($id) && $id !=''){
		$query = 'select * from mst_sub_order where iMstProductsId IN ("'.$id.'")';
		$productid = $this->db->query($query)->result_array();
		if(empty($productid)){
			$product = explode(',',$render_arr['id']);				
			foreach ($product as $value){
				$del_query= "DELETE FROM mst_products WHERE  iMstProductsId ='".$value."'";
				$result = $this->db->query($del_query)->result_array();
			}
 			$status = 1;
			$message = "Product deleted successfully";
			$this->session->set_flashdata('success', 'Product deleted successfully');
		}else
		{
			$status = 1;
			$message = "Error in deleting your products";
			$this->session->set_flashdata('failure', 'Error in deleting your products');
		}
	}else{
		$status =1;
		$message = "Please Select Atlease one product to delete";
		$this->session->set_flashdata('failure', 'Please Select Atlease one product to delete');
	}
    $ret_arr['success'] = $status;
	$ret_arr['message'] = $message;
    return $ret_arr;
	
}
}
