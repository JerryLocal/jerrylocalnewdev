<%section name=i loop=1%>
    <tr id="tr_child_row_<%$child_module_name%>_<%$row_index%>">
        <td class="row-num-child">
            <span class="row-num-span"><%$row_index%></span>
            <input type="hidden" name="child[productimages][id][<%$row_index%>]" id="child_productimages_id_<%$row_index%>" value="<%$child_id%>" />
            <input type="hidden" name="child[productimages][enc_id][<%$row_index%>]" id="child_productimages_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
            <input type="hidden" name="child[productimages][mpi_mst_products_id][<%$row_index%>]" id="child_productimages_mpi_mst_products_id_<%$row_index%>" value="<%$child_data[i]['mpi_mst_products_id']%>"  class='ignore-valid ' />
        </td>
        <td>
            <div class=" " id="ch_productimages_cc_sh_mpi_img_path_<%$row_index%>">
                <div  class='btn-uploadify frm-size-medium' >
                    <input type="hidden" value="<%$child_data[i]['mpi_img_path']%>" name="child[productimages][old_mpi_img_path][<%$row_index%>]" id="child_productimages_old_mpi_img_path_<%$row_index%>" />
                    <input type="hidden" value="<%$child_data[i]['mpi_img_path']%>" name="child[productimages][mpi_img_path][<%$row_index%>]" id="child_productimages_mpi_img_path_<%$row_index%>"  />
                    <input type="hidden" value="<%$child_data[i]['mpi_img_path']%>" name="child[productimages][temp_mpi_img_path][<%$row_index%>]" id="child_productimages_temp_mpi_img_path_<%$row_index%>" />
                    <div id="upload_drop_zone_child_productimages_mpi_img_path_<%$row_index%>" class="upload-drop-zone"></div>
                    <div class="uploader upload-src-zone">
                        <input type="file" name="uploadify_child[productimages][mpi_img_path][<%$row_index%>]" id="uploadify_child_productimages_mpi_img_path_<%$row_index%>" title="<%$this->lang->line('PRODUCTIMAGES_IMG_PATH')%>" />
                        <span class="filename" id="preview_child_productimages_mpi_img_path_<%$row_index%>"></span>
                        <span class="action">Choose File</span>
                    </div>
                </div>
                <div class='upload-image-btn'>
                    <%$child_img_html[$row_index]['mpi_img_path']%>
                </div> 
                <span class="input-comment">
                    <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
                </span>
                <div class='clear'>
                    <div id='progress_child_productimages_mpi_img_path_<%$row_index%>' class='upload-progress progress progress-striped active'>
                        <div class='bar' id='practive_child_productimages_mpi_img_path_<%$row_index%>'></div>
                    </div>
                </div>
            </div>
            <div>
                <label class='error' id='child_productimages_mpi_img_path_<%$row_index%>Err'></label>
            </div>
        </td>
        <td>
            <div class=" " id="ch_productimages_cc_sh_mpi_default_<%$row_index%>">
                <%assign var="opt_selected" value=$child_data[i]['mpi_default']%>
                <%$this->dropdown->display("child_productimages_mpi_default_<%$row_index%>","child[productimages][mpi_default][<%$row_index%>]","  title='<%$this->lang->line('PRODUCTIMAGES_DEFAULT')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTIMAGES_DEFAULT')%>'  ","|||","",$opt_selected,"child_productimages_mpi_default_$row_index")%>  
            </div>
            <div>
                <label class='error' id='child_productimages_mpi_default_<%$row_index%>Err'></label>
            </div>
        </td>
        <td align="center">
            <div class="controls center">
                <%if $mode eq "Update"%>
                    <a onclick="saveChildModuleSingleData('<%$mod_enc_url.child_data_save%>', '<%$mod_enc_url.child_data_add%>', '<%$child_module_name%>', '<%$recMode%>', '<%$row_index%>', '<%$enc_child_id%>', 'No', '<%$mode%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_SAVE')%>">
                        <span class="icon14 icomoon-icon-disk"></span>
                    </a>
                <%/if%>
                <%if $recMode eq "Update"%>
                    <a onclick="deleteChildModuleSingleData('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>','<%$enc_child_id%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                        <span class="icon16 icomoon-icon-remove"></span>
                    </a>
                <%else%>
                    <a onclick="deleteChildModuleRow('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>')" href="javascript://" class="tip" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                        <span class="icon16 icomoon-icon-remove"></span>
                    </a>
                <%/if%>
            </div>
        </td>
    </tr>
    <%javascript%>
    <%if $child_auto_arr[$row_index]|@is_array && $child_auto_arr[$row_index]|@count gt 0%>
        var $child_chosen_auto_complete_url = admin_url+""+$("#childModuleChosenURL_<%$child_module_name%>").val()+"?";
        <%foreach name=i from=$child_auto_arr[$row_index] item=v key=k%>
            if($("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").is("select")){
            $("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").ajaxChosen({
            dataType: "json",
            type: "POST",
            url: $child_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$recMode]%>&id=<%$enc_child_id%>"
            },{
            loadingImg: admin_image_url+"chosen-loading.gif"
            });
        }
    <%/foreach%>
<%/if%>
<%/javascript%>
<%/section%>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initChildRenderJSScript(){
Project.modules.seller_product_management.childEvents("productimages", "#tr_child_row_<%$child_module_name%>_<%$row_index%>");
callGoogleMapEvents();
}
<%/javascript%>