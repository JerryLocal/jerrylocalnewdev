<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SELLER_PRODUCT_MANAGEMENT')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','SELLERPRODUCTMANAGEMENT_SELLER_PRODUCT_MANAGEMENT')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display: none;position:inherit;" id="ajax_lang_loader">
            <img src="<%$this->config->item('admin_images_url')%>loaders/circular/020.gif">
        </span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="sellerproductmanagement" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="sellerproductmanagement" class="frm-view-block frm-stand-view">
            <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
            <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
            <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
            <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
            <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
            <input type="hidden" name="mp_difference_per" id="mp_difference_per" value="<%$data['mp_difference_per']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_low_avl_limit_notifcation" id="mp_low_avl_limit_notifcation" value="<%$data['mp_low_avl_limit_notifcation']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_total_rate" id="mp_total_rate" value="<%$data['mp_total_rate']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_view_state" id="mp_view_state" value="<%$data['mp_view_state']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mp_default_img" id="mp_default_img" value="<%$data['mp_default_img']|@htmlentities%>"  class='ignore-valid ' />
            <div class="main-content-block" id="main_content_block">
                <div style="width:98%;" class="frm-block-layout pad-calc-container">
                    <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                        <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('SELLERPRODUCTMANAGEMENT_SELLER_PRODUCT_MANAGEMENT')%></h4></div>
                        <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                            <div class="form-row row-fluid" id="cc_sh_mp_store_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_STORE_NAME')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
                                    <strong><%$this->general->displayKeyValueData($data['mp_store_id'], $opt_arr['mp_store_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_admin_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_STORE_OWNER')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
                                    <strong><%$data['mp_admin_id']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_title">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_TITLE')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_title']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_sku">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SKU')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_sku']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_regular_price">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_REGULAR_PRICE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  input-append text-append-prepend ">
                                    <strong><%$data['mp_regular_price']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_sale_price">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SALE_PRICE')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  input-append text-append-prepend ">
                                    <strong><%$data['mp_sale_price']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_stock">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_STOCK')%> <em>*</em> 
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_stock']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_free_shipping">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_FREE_SHIPPING')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mp_free_shipping'], $opt_arr['mp_free_shipping'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_shipping_charge">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SHIPPING_CHARGE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_shipping_charge']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_return_days">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_RETURN_DAYS')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  input-append text-append-prepend ">
                                    <strong><%$data['mp_return_days']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_allow_max_purchase">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_ALLOW_MAX_PURCHASE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_allow_max_purchase']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_short_description">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SHORT_DESCRIPTION')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mp_short_description']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_description">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_DESCRIPTION')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  frm-editor-layout ">
                                    <strong><%$data['mp_description']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_rating_avg">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_RATING_AVG')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
                                    <span id='rscore_mp_rating_avg' style='display:none;'><%$data['mp_rating_avg']%></span>
                                    <span id='rshow_mp_rating_avg'></span>
                                    <%javascript%>
                                    $('#rshow_mp_rating_avg').raty({
                                    number : 5, 
                                    cancel : false, 
                                    half : true, 
                                    targetKeep : true, 
                                    precision : true, 
                                    cancelOff : 'cancel-custom-off.png', 
                                    cancelOn : 'cancel-custom-on.png', 
                                    starOff : 'star-off.png', 
                                    starOn : 'star-on.png', 
                                    starHalf : 'star-half.png', 
                                    clear : false, 
                                    readOnly : true,
                                    score: "<%$this->general->processQuotes($data['mp_rating_avg'])%>",
                                    hints : ['1','2','3','4','5']
                                    });
                                    <%/javascript%>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_wishlist_state">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_WISHLIST_COUNT')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
                                    <strong><%$data['mp_wishlist_state']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_sale_state">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SALES_COUNT')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
                                    <strong><%$data['mp_sale_state']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_date">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_ADDED_DATE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
                                    <strong><%$this->general->dateDefinedFormat('F d, Y',$data['mp_date'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_modify_date">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_MODIFY_DATE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
                                    <strong><%$this->general->dateDefinedFormat('Y-m-d',$data['mp_modify_date'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_do_return">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_DO_RETURN')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mp_do_return'], $opt_arr['mp_do_return'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mp_status">
                                <label class="form-label span3">
                                    <%$this->lang->line('SELLERPRODUCTMANAGEMENT_STATUS')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mp_status'], $opt_arr['mp_status'])%></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>        
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
    callSwitchToSelf();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
