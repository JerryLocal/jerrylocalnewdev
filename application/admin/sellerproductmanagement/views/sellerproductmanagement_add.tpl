<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SELLER_PRODUCT_MANAGEMENT')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','SELLERPRODUCTMANAGEMENT_SELLER_PRODUCT_MANAGEMENT')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display: none;position:inherit;" id="ajax_lang_loader">
            <img src="<%$this->config->item('admin_images_url')%>loaders/circular/020.gif">
        </span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="sellerproductmanagement" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="sellerproductmanagement" class="frm-elem-block frm-stand-view">
            <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                <input type="hidden" name="mp_difference_per" id="mp_difference_per" value="<%$data['mp_difference_per']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mp_low_avl_limit_notifcation" id="mp_low_avl_limit_notifcation" value="<%$data['mp_low_avl_limit_notifcation']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mp_total_rate" id="mp_total_rate" value="<%$data['mp_total_rate']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mp_view_state" id="mp_view_state" value="<%$data['mp_view_state']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mp_default_img" id="mp_default_img" value="<%$data['mp_default_img']|@htmlentities%>"  class='ignore-valid ' />
                <div class="main-content-block" id="main_content_block">
                    <div style="width:98%" class="frm-block-layout pad-calc-container">
                        <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                            <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('SELLERPRODUCTMANAGEMENT_SELLER_PRODUCT_MANAGEMENT')%></h4></div>
                            <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                <div class="form-row row-fluid" id="cc_sh_mp_store_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_STORE_NAME')%>
                                    </label> 
                                    <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%>  ">
                                        <%assign var="opt_selected" value=$data['mp_store_id']%>
                                        <%if $mode eq "Update"%>
                                            <input type="hidden" name="mp_store_id" id="mp_store_id" value="<%$data['mp_store_id']%>" class="ignore-valid"/>
                                            <%assign var="combo_arr" value=$opt_arr["mp_store_id"]%>
                                            <strong><%$this->general->displayKeyValueData($opt_selected, $combo_arr)%></strong>
                                        <%else%>
                                            <%$this->dropdown->display("mp_store_id","mp_store_id","  title='<%$this->lang->line('SELLERPRODUCTMANAGEMENT_STORE_NAME')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SELLERPRODUCTMANAGEMENT_STORE_NAME')%>'  ", "|||", "", $opt_selected,"mp_store_id")%>
                                        <%/if%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_store_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_admin_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_STORE_OWNER')%>
                                    </label> 
                                    <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%>  ">
                                        <%if $mode eq "Update"%>
                                            <input type="hidden" class="ignore-valid" name="mp_admin_id" id="mp_admin_id" value="<%$data['mp_admin_id']%>" />
                                            <strong><%if $data['mp_admin_id'] neq "" %><%$data['mp_admin_id']%><%else%>---<%/if%></strong>
                                        <%else%>
                                            <input type="text" placeholder="" value="<%$data['mp_admin_id']|@htmlentities%>" name="mp_admin_id" id="mp_admin_id" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_STORE_OWNER')%>"  class='frm-size-medium'  />
                                        <%/if%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_admin_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_title">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_TITLE')%> <em>*</em> 
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mp_title']|@htmlentities%>" name="mp_title" id="mp_title" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_TITLE')%>"  class='frm-size-medium apply-text-uc_first'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_titleErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_sku">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SKU')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mp_sku']|@htmlentities%>" name="mp_sku" id="mp_sku" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_SKU')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_skuErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_regular_price">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_REGULAR_PRICE')%>
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend  ">
                                        <input type="text" placeholder="" value="<%$data['mp_regular_price']|@htmlentities%>" name="mp_regular_price" id="mp_regular_price" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_REGULAR_PRICE')%>"  class='frm-size-medium ctrl-append-prepend '  />
                                        <span class='add-on text-addon'>NZ $</span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_regular_priceErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_sale_price">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SALE_PRICE')%> <em>*</em> 
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend  ">
                                        <input type="text" placeholder="" value="<%$data['mp_sale_price']|@htmlentities%>" name="mp_sale_price" id="mp_sale_price" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_SALE_PRICE')%>"  class='frm-size-medium ctrl-append-prepend '  />
                                        <span class='add-on text-addon'>NZ $</span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_sale_priceErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_stock">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_STOCK')%> <em>*</em> 
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mp_stock']|@htmlentities%>" name="mp_stock" id="mp_stock" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_STOCK')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_stockErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_free_shipping">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_FREE_SHIPPING')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mp_free_shipping']%>
                                        <%$this->dropdown->display("mp_free_shipping","mp_free_shipping","  title='<%$this->lang->line('SELLERPRODUCTMANAGEMENT_FREE_SHIPPING')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SELLERPRODUCTMANAGEMENT_FREE_SHIPPING')%>'  ", "", "", $opt_selected,"mp_free_shipping")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_free_shippingErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_shipping_charge">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SHIPPING_CHARGE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mp_shipping_charge']|@htmlentities%>" name="mp_shipping_charge" id="mp_shipping_charge" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_SHIPPING_CHARGE')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_shipping_chargeErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_return_days">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_RETURN_DAYS')%>
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend  ">
                                        <input type="text" placeholder="" value="<%$data['mp_return_days']|@htmlentities%>" name="mp_return_days" id="mp_return_days" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_RETURN_DAYS')%>"  class='frm-size-medium ctrl-append-prepend '  />
                                        <span class='add-on text-addon'>Days</span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_return_daysErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_allow_max_purchase">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_ALLOW_MAX_PURCHASE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mp_allow_max_purchase']|@htmlentities%>" name="mp_allow_max_purchase" id="mp_allow_max_purchase" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_ALLOW_MAX_PURCHASE')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_allow_max_purchaseErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_short_description">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SHORT_DESCRIPTION')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <textarea placeholder=""  name="mp_short_description" id="mp_short_description" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_SHORT_DESCRIPTION')%>"  class='elastic frm-size-medium'  ><%$data['mp_short_description']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_short_descriptionErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_description">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_DESCRIPTION')%>
                                    </label> 
                                    <div class="form-right-div  frm-editor-layout  ">
                                        <textarea name="mp_description" id="mp_description" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_DESCRIPTION')%>"  style='width:80%;'  class='frm-size-medium frm-editor-small'  ><%$data['mp_description']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_descriptionErr'></label></div>
                                </div>
                                <%if $mode eq "Update"%>
                                    <div class="form-row row-fluid" id="cc_sh_mp_rating_avg">
                                        <label class="form-label span3">
                                            <%$this->lang->line('SELLERPRODUCTMANAGEMENT_RATING_AVG')%>
                                        </label> 
                                        <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%>  ">
                                            <%if $mode eq "Update"%>
                                                <input aria-raty-clear="false" aria-raty-readonly="false" type="hidden" class="ignore-valid" name="mp_rating_avg" id="mp_rating_avg" value="<%$data['mp_rating_avg']%>" />  
                                                <span id='rating_mp_rating_avg' class='rating-icons-block'></span>
                                            <%else%>
                                                <input aria-raty-clear="true" aria-raty-readonly="true" type="hidden" value="<%$data['mp_rating_avg']%>" name="mp_rating_avg" id="mp_rating_avg" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_RATING_AVG')%>"  />
                                                <span id='rating_mp_rating_avg' class='rating-icons-block'></span>
                                            <%/if%>
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='mp_rating_avgErr'></label></div>
                                    </div>
                                <%else%>
                                    <input type="hidden" class="ignore-valid" name="mp_rating_avg" id="mp_rating_avg" value="<%$data['mp_rating_avg']%>" />
                                <%/if%>
                                <%if $mode eq "Update"%>
                                    <div class="form-row row-fluid" id="cc_sh_mp_wishlist_state">
                                        <label class="form-label span3">
                                            <%$this->lang->line('SELLERPRODUCTMANAGEMENT_WISHLIST_COUNT')%>
                                        </label> 
                                        <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%>  ">
                                            <%if $mode eq "Update"%>
                                                <input type="hidden" class="ignore-valid" name="mp_wishlist_state" id="mp_wishlist_state" value="<%$data['mp_wishlist_state']%>" />
                                                <strong><%if $data['mp_wishlist_state'] neq "" %><%$data['mp_wishlist_state']%><%else%>---<%/if%></strong>
                                            <%else%>
                                                <input type="text" placeholder="" value="<%$data['mp_wishlist_state']|@htmlentities%>" name="mp_wishlist_state" id="mp_wishlist_state" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_WISHLIST_COUNT')%>"  class='frm-size-medium'  />
                                            <%/if%>
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='mp_wishlist_stateErr'></label></div>
                                    </div>
                                <%else%>
                                    <input type="hidden" class="ignore-valid" name="mp_wishlist_state" id="mp_wishlist_state" value="<%$data['mp_wishlist_state']%>" />
                                <%/if%>
                                <%if $mode eq "Update"%>
                                    <div class="form-row row-fluid" id="cc_sh_mp_sale_state">
                                        <label class="form-label span3">
                                            <%$this->lang->line('SELLERPRODUCTMANAGEMENT_SALES_COUNT')%>
                                        </label> 
                                        <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%>  ">
                                            <%if $mode eq "Update"%>
                                                <input type="hidden" class="ignore-valid" name="mp_sale_state" id="mp_sale_state" value="<%$data['mp_sale_state']%>" />
                                                <strong><%if $data['mp_sale_state'] neq "" %><%$data['mp_sale_state']%><%else%>---<%/if%></strong>
                                            <%else%>
                                                <input type="text" placeholder="" value="<%$data['mp_sale_state']|@htmlentities%>" name="mp_sale_state" id="mp_sale_state" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_SALES_COUNT')%>"  class='frm-size-medium'  />
                                            <%/if%>
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='mp_sale_stateErr'></label></div>
                                    </div>
                                <%else%>
                                    <input type="hidden" class="ignore-valid" name="mp_sale_state" id="mp_sale_state" value="<%$data['mp_sale_state']%>" />
                                <%/if%>
                                <div class="form-row row-fluid" id="cc_sh_mp_date">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_ADDED_DATE')%>
                                    </label> 
                                    <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%>  ">
                                        <%if $mode eq "Update"%>
                                            <input type="hidden" name="mp_date" id="mp_date" value="<%$data['mp_date']%>" class="ignore-valid view-label-only"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='MM d, yy'  aria-format-type='date' />
                                            <strong><%$this->general->dateDefinedFormat('F d, Y',$data['mp_date'])%></strong>
                                        <%else%>
                                            <input type="text" value="<%$this->general->dateDefinedFormat('F d, Y',$data['mp_date'])%>" placeholder="" name="mp_date" id="mp_date" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_ADDED_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='MM d, yy'  aria-format-type='date'  />
                                            <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                        <%/if%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_dateErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_modify_date">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_MODIFY_DATE')%>
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend   ">
                                        <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['mp_modify_date'])%>" placeholder="" name="mp_modify_date" id="mp_modify_date" title="<%$this->lang->line('SELLERPRODUCTMANAGEMENT_MODIFY_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                        <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_modify_dateErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_do_return">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_DO_RETURN')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mp_do_return']%>
                                        <%$this->dropdown->display("mp_do_return","mp_do_return","  title='<%$this->lang->line('SELLERPRODUCTMANAGEMENT_DO_RETURN')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SELLERPRODUCTMANAGEMENT_DO_RETURN')%>'  ", "|||", "", $opt_selected,"mp_do_return")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_do_returnErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mp_status">
                                    <label class="form-label span3">
                                        <%$this->lang->line('SELLERPRODUCTMANAGEMENT_STATUS')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mp_status']%>
                                        <%$this->dropdown->display("mp_status","mp_status","  title='<%$this->lang->line('SELLERPRODUCTMANAGEMENT_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SELLERPRODUCTMANAGEMENT_STATUS')%>'  ", "|||", "", $opt_selected,"mp_status")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mp_statusErr'></label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%>">
                        <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                            <%assign var='rm_ctrl_directions' value=true%>
                        <%/if%>
                        <!-- Form Redirection Control Unit -->
                        <%if $controls_allow eq false || $rm_ctrl_directions eq true%>
                            <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" type="hidden" />
                        <%else%>
                            <div class='action-dir-align'>
                                <%if $prev_link_allow eq true%>
                                    <input value="Prev" id="ctrl_flow_prev" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Prev' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_prev">&nbsp;</label>
                                    <label for="ctrl_flow_prev" class="inline-elem-margin"><%$this->lang->line('GENERIC_PREV_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <%if $next_link_allow eq true || $mode eq 'Add'%>
                                    <input value="Next" id="ctrl_flow_next" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Next' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_next">&nbsp;</label>
                                    <label for="ctrl_flow_next" class="inline-elem-margin"><%$this->lang->line('GENERIC_NEXT_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <input value="List" id="ctrl_flow_list" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'List' %> checked=true <%/if%> />
                                <label for="ctrl_flow_list">&nbsp;</label>
                                <label for="ctrl_flow_list" class="inline-elem-margin"><%$this->lang->line('GENERIC_LIST_SHORT')%></label>&nbsp;&nbsp;
                                <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq '' || $ctrl_flow eq 'Stay' %> checked=true <%/if%> />
                                <label for="ctrl_flow_stay">&nbsp;</label>
                                <label for="ctrl_flow_stay" class="inline-elem-margin"><%$this->lang->line('GENERIC_STAY_SHORT')%></label>
                            </div>
                        <%/if%>
                        <!-- Form Action Control Unit -->
                        <%if $controls_allow eq false%>
                            <div class="clear">&nbsp;</div>
                        <%/if%>
                        <div class="action-btn-align" id="action_btn_container">
                            <%if $mode eq 'Update'%>
                                <%if $update_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_UPDATE')%>" name="ctrlupdate" type="submit" id="frmbtn_update" class="btn btn-info"/>&nbsp;&nbsp;
                                <%/if%>
                                <%if $delete_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_DELETE')%>" name="ctrldelete" type="button" id="frmbtn_delete" class="btn btn-danger" onclick="return deleteAdminRecordData('<%$enc_id%>', '<%$mod_enc_url.index%>','<%$mod_enc_url.inline_edit_action%>', '<%$extra_qstr%>', '<%$extra_hstr%>');" />&nbsp;&nbsp;
                                <%/if%>
                            <%else%>
                                <input value="<%$this->lang->line('GENERIC_SAVE')%>" name="ctrladd" type="submit" id="frmbtn_add" class="btn btn-info" />&nbsp;&nbsp;
                            <%/if%>
                            <%if $discard_allow eq true%>
                                <input value="<%$this->lang->line('GENERIC_DISCARD')%>" name="ctrldiscard" type="button" id="frmbtn_discard" class="btn" onclick="return loadAdminModuleListing('<%$mod_enc_url.index%>', '<%$extra_hstr%>')" />
                            <%/if%>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </form>
        </div>
    </div>
</div>
<%javascript%>    
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};        
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        <%foreach name=i from=$auto_arr item=v key=k%>
            if($("#<%$k%>").is("select")){
                $("#<%$k%>").ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                    },{
                    loadingImg: admin_image_url+"chosen-loading.gif"
                });
            }
        <%/foreach%>
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['buttons_arr'] = [];
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/sellerproductmanagement_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.sellerproductmanagement.callEvents();
<%/javascript%>