<ul class="nav nav-tabs">
    <li <%if $module_name eq "seller_product_management"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('SELLER_PRODUCT_MANAGEMENT_SELLER_PRODUCT_MANAGEMENT')%>" 
            <%if $module_name eq "seller_product_management"%> 
                href="javascript://"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('sellerproductmanagement/seller_product_management/add')%>|mode|<%$mod_enc_mode['Update']%>|id|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('SELLER_PRODUCT_MANAGEMENT_SELLER_PRODUCT_MANAGEMENT')%>
        </a>
    </li>
    <li <%if $module_name eq "product_options_seller"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('SELLER_PRODUCT_MANAGEMENT_PRODUCT_OPTIONS_SELLER')%> <%$this->lang->line('GENERIC_LIST')%>" 
            <%if $module_name eq "product_options_seller"%> 
                href="javascript://"
            <%elseif $module_name eq "seller_product_management"%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('productoptionsseller/product_options_seller/index')%>|parMod|<%$this->general->getAdminEncodeURL('seller_product_management')%>|parID|<%$this->general->getAdminEncodeURL($data['iMstProductsId'])%>"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('productoptionsseller/product_options_seller/index')%>|parMod|<%$this->general->getAdminEncodeURL('seller_product_management')%>|parID|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('SELLER_PRODUCT_MANAGEMENT_PRODUCT_OPTIONS_SELLER')%>
        </a>
    </li>
</ul>            