<?php
/**
 * Description of Dashboard Controller
 *
 * @module Dashboard
 *
 * @class dashboard.php
 *
 * @path application\admin\dashboard\controllers\dashboard.php
 *
 * @author CIT Dev Team
 *
 * @date 05.02.2016
 */
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Dashboard extends HB_Controller
{
    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->load->model('model_dashboard');
        $this->load->model('model_dashboardpages');
        $this->load->model('model_dashboardpagesblock');
        $this->get_arr = is_array($this->input->get(NULL, TRUE)) ? $this->input->get(NULL, TRUE) : array();
        $this->post_arr = is_array($this->input->post(NULL, TRUE)) ? $this->input->post(NULL, TRUE) : array();
        $this->params_arr = array_merge($this->get_arr, $this->post_arr);
        $this->folder_name = "dashboard";
        $this->module_name = "dashboard";
        $this->mod_enc_mode = $this->general->getCustomEncryptMode(TRUE);
        $this->module_config = array(
            'module_name' => $this->module_name,
            'folder_name' => $this->folder_name,
        );
        $this->dropdown_limit = $this->config->item('ADMIN_DROPDOWN_LIMIT');
        $this->chart_assoc = array(
            "pivot" => "Pivot Table",
            "bar" => "Bar Chart",
            "pie" => "Pie Chart",
            "donut" => "Donut Chart",
            "area" => "Area Chart",
            "line" => "Line Chart",
            "horizbar" => "Horiz. Bar Chart",
            "stackbar" => "Stacked Bar Chart",
            "stackhorizbar" => "Stacked Horiz. Bar Chart",
            "autoupdating" => "Auto Updating Chart",
        );
    }

    /**
     * index method is used to intialize sitemap listing page.
     */
    public function index()
    {
        $menu_assoc_arr = $this->systemsettings->getAdminAccessModulesList();
        $total_arr = $this->systemsettings->getMenuArray($menu_assoc_arr['menuCond']);
        $render_arr = array(
            'total_arr' => $total_arr,
        );
        $this->smarty->assign($render_arr);
    }

    /**
     * sitemap method is used to load sitemap listing page.
     */
    public function sitemap()
    {
        $menu_assoc_arr = $this->systemsettings->getAdminAccessModulesList();
        $total_arr = $this->systemsettings->getMenuArray($menu_assoc_arr['menuCond']);
        $render_arr = array(
            'total_arr' => $total_arr,
        );
        $this->smarty->assign($render_arr);
    }

    public function admin_deshboard()
    {
        $params_arr = $this->params_arr;
        $board_type = base64_decode($params_arr['boardtype']);
        $page_code = "admin_deshboard";
        $view_type = "simple";
        $tab_file = "admin_deshboard_tab.tpl";
        $this->filter->getModuleWiseAccess("admin_deshboard", "View", FALSE, FALSE, TRUE, '');

        $extra_qstr = $extra_hstr = '';
        $return_data = $module_arr = $db_tab_data = $top_detail_view = array();
        $is_tabs_allow = $is_board_module = FALSE;

        $page_cond = $this->db->protect("vPageCode")." = ".$this->db->escape($page_code);
        $data_arr = $this->model_dashboardpages->getData($page_cond);
        $data = $data_arr[0];
        $extra_condition = $this->db->protect("madp.vPageCode")." = ".$this->db->escape($page_code);

        $tab_id = $current_tab = 0;
        $extra_condition .= " AND ".$this->db->protect("madp.eDasboardView")." = ".$this->db->escape("Simple")." AND ".$this->db->protect("madpb.iTabId")." = ".$this->db->escape(0);
        $return_data["users_state"] = $this->model_dashboard->users_state();
        $return_data["seller_state"] = $this->model_dashboard->seller_state();
        $return_data["customer_state"] = $this->model_dashboard->customer_state();
        $return_data["settlement"] = $this->model_dashboard->settlement();
        $return_data["order_status"] = $this->model_dashboard->order_status();
        $return_data["order_status_v1"] = $this->model_dashboard->order_status_v1();
        $return_data["product_statastics"] = $this->model_dashboard->product_statastics();
        $return_data["product_statastics_v1"] = $this->model_dashboard->product_statastics_v1();
        $return_data["monthly_order_summary"] = $this->model_dashboard->monthly_order_summary();
        $return_data["return_summary"] = $this->model_dashboard->return_summary();

        $data['iTabId'] = $tab_id;
        $data['vCurrentTab'] = $current_tab;

        $render_arr = $this->_render_dashboard_page($extra_condition, $return_data);
        $render_arr["data"] = $data;
        $render_arr["tab_file"] = $tab_file;
        $render_arr["board_type"] = $board_type;
        $render_arr["module_arr"] = $module_arr;
        $render_arr["top_detail_view"] = $top_detail_view;
        $render_arr["is_tabs_allow"] = $is_tabs_allow;
        $render_arr["is_board_module"] = $is_board_module;
        $render_arr["extra_hstr"] = $extra_hstr;
        $render_arr["extra_qstr"] = $extra_qstr;

        $this->smarty->assign($render_arr);
        $this->loadView("dashboard");
    }
    public function seller_deshboard()
    {
        $params_arr = $this->params_arr;
        $board_type = base64_decode($params_arr['boardtype']);
        $page_code = "seller_deshboard";
        $view_type = "simple";
        $tab_file = "";
        $this->filter->getModuleWiseAccess("seller_deshboard", "View", FALSE, FALSE, TRUE, '');

        $extra_qstr = $extra_hstr = '';
        $return_data = $module_arr = $db_tab_data = $top_detail_view = array();
        $is_tabs_allow = $is_board_module = FALSE;

        $page_cond = $this->db->protect("vPageCode")." = ".$this->db->escape($page_code);
        $data_arr = $this->model_dashboardpages->getData($page_cond);
        $data = $data_arr[0];
        $extra_condition = $this->db->protect("madp.vPageCode")." = ".$this->db->escape($page_code);

        $tab_id = $current_tab = 0;
        $extra_condition .= " AND ".$this->db->protect("madp.eDasboardView")." = ".$this->db->escape("Simple")." AND ".$this->db->protect("madpb.iTabId")." = ".$this->db->escape(0);
        $return_data["product_statastics"] = $this->model_dashboard->product_statastics();
        $return_data["product_statastics_v1"] = $this->model_dashboard->product_statastics_v1();
        $return_data["product_reviews"] = $this->model_dashboard->product_reviews();
        $return_data["store_review"] = $this->model_dashboard->store_review();
        $return_data["new_orders"] = $this->model_dashboard->new_orders();
        $return_data["return_summary"] = $this->model_dashboard->return_summary();
        $return_data["order_summary_amount"] = $this->model_dashboard->order_summary_amount();
        $return_data["order_summary_qty"] = $this->model_dashboard->order_summary_qty();
        $return_data["return_status"] = $this->model_dashboard->return_status();
        $return_data["settlement_summary"] = $this->model_dashboard->settlement_summary();
        $return_data["monthly_order_summary"] = $this->model_dashboard->monthly_order_summary();

        $data['iTabId'] = $tab_id;
        $data['vCurrentTab'] = $current_tab;

        $render_arr = $this->_render_dashboard_page($extra_condition, $return_data);
        $render_arr["data"] = $data;
        $render_arr["tab_file"] = $tab_file;
        $render_arr["board_type"] = $board_type;
        $render_arr["module_arr"] = $module_arr;
        $render_arr["top_detail_view"] = $top_detail_view;
        $render_arr["is_tabs_allow"] = $is_tabs_allow;
        $render_arr["is_board_module"] = $is_board_module;
        $render_arr["extra_hstr"] = $extra_hstr;
        $render_arr["extra_qstr"] = $extra_qstr;

        $this->smarty->assign($render_arr);
        $this->loadView("dashboard");
    }

    /**
     * _render_dashboard_page method is used to get data array for rendering dashboard pages
     * @param string $extra_cond extra_cond is used to set query extra where condition.
     * @param string $render_data render_data for rendering dashboard pages.
     * @return array $render_arr returns render data records array
     */
    public function _render_dashboard_page($extra_cond = '', $render_data = array())
    {
        $where_cond = ($extra_cond != "") ? $extra_cond : "";
        $fields = "mad.iDashBoardId, mad.vBoardName, mad.vBoardCode, mad.vBoardIcon, mad.eBoardSource, mad.eDefaultChart, mad.eChartType, madpb.tLayoutJSON, madpb.iBlockOrder";
        $order_by = "madpb.iBlockOrder";
        $dashboard_data = $this->model_dashboardpagesblock->getData($where_cond, $fields, $order_by, "", "", "Yes");
        $db_pivot_data = $db_list_data = $db_view_data = $block_data_arr = $block_config_arr = array();
        foreach ((array) $dashboard_data as $key => $val)
        {
            $board_id = $val['iDashBoardId'];
            $board_code = $val['vBoardCode'];
            $board_source = $val['eBoardSource'];
            $chart_type = $val['eChartType'];
            if ($chart_type == "Pivot")
            {
                if (in_array($val['eDefaultChart'], $this->chart_assoc))
                {
                    $key_chart = array_search($val['eDefaultChart'], $this->chart_assoc);
                }
                else
                {
                    $key_chart = "pivot";
                }
                $db_pivot_data[$board_id] = $render_data[$board_code];
                $db_pivot_data[$board_id]["defaultChart"] = $key_chart;
                $ajax_update = "No";
            }
            elseif ($chart_type == "Detail View")
            {
                if ($board_source == "Function")
                {
                    $db_view_data[$board_id] = $render_data[$board_code]['data'];
                }
                else
                {
                    if ($render_data[$board_code]['template'])
                    {
                        $parse_html = $this->parser->parse($render_data[$board_code]['template'], $render_data[$board_code], TRUE);
                        $db_view_data[$board_id] = $parse_html;
                    }
                }
                $ajax_update = "Yes";
            }
            elseif ($chart_type == "Grid List")
            {
                $db_list_data[$board_id] = $render_data[$board_code];
                $ajax_update = "No";
            }

            $tmp_config_arr['id'] = $board_id;
            $tmp_config_arr['chartType'] = $chart_type;
            $tmp_config_arr['ajaxUpdate'] = $ajax_update;
            $tmp_config_arr['autoUpdate'] = $render_data[$board_code]["autoUpdate"];
            $tmp_config_arr['dateFilter'] = $render_data[$board_code]["dateFilter"];
            $tmp_config_arr['filterField'] = $render_data[$board_code]["filterField"];
            $block_config_arr[$board_id] = $tmp_config_arr;

            $val['attr'] = $this->listing->getDashboardAttributes($val);
            $block_data_arr[] = $val;
        }
        $db_list_data_json = $this->filter->getJSONEncodeJSFunction($db_list_data);
        $db_pivot_data_json = $this->filter->getJSONEncodeJSFunction($db_pivot_data);
        $block_config_json = json_encode($block_config_arr);
        $render_arr = array(
            'block_config_json' => $block_config_json,
            'db_pivot_data_json' => $db_pivot_data_json,
            'db_list_data_json' => $db_list_data_json,
            'db_view_data' => $db_view_data,
            'block_data_arr' => $block_data_arr,
            'mod_enc_mode' => $this->mod_enc_mode,
            'folder_name' => $this->folder_name,
            'module_name' => $this->module_name,
        );
        return $render_arr;
    }

    public function __parse_dashboard_block($code = '', $filters = array(), $json = TRUE)
    {
        $ret_data = $func_data = FALSE;

        $where_cond = $this->db->protect("mad.vBoardCode")." = ".$this->db->escape($code);
        $fields = "mad.iDashBoardId, mad.vBoardName, mad.vBoardCode, mad.vBoardIcon, mad.eBoardSource, mad.eDefaultChart, mad.eChartType";
        $dashboard_data = $this->model_dashboard->getData($where_cond, $fields);
        if (!is_array($dashboard_data) || count($dashboard_data) == 0)
        {
            return $ret_data;
        }
        switch ($code)
        {
            case "users_state":
                $func_data = $this->model_dashboard->users_state($filters);
                break;
            case "seller_state":
                $func_data = $this->model_dashboard->seller_state($filters);
                break;
            case "customer_state":
                $func_data = $this->model_dashboard->customer_state($filters);
                break;
            case "settlement":
                $func_data = $this->model_dashboard->settlement($filters);
                break;
            case "order_status":
                $func_data = $this->model_dashboard->order_status($filters);
                break;
            case "order_status_v1":
                $func_data = $this->model_dashboard->order_status_v1($filters);
                break;
            case "product_statastics":
                $func_data = $this->model_dashboard->product_statastics($filters);
                break;
            case "product_statastics_v1":
                $func_data = $this->model_dashboard->product_statastics_v1($filters);
                break;
            case "product_reviews":
                $func_data = $this->model_dashboard->product_reviews($filters);
                break;
            case "store_review":
                $func_data = $this->model_dashboard->store_review($filters);
                break;
            case "new_orders":
                $func_data = $this->model_dashboard->new_orders($filters);
                break;
            case "return_status":
                $func_data = $this->model_dashboard->return_status($filters);
                break;
            case "return_summary":
                $func_data = $this->model_dashboard->return_summary($filters);
                break;
            case "order_summary_amount":
                $func_data = $this->model_dashboard->order_summary_amount($filters);
                break;
            case "order_summary_qty":
                $func_data = $this->model_dashboard->order_summary_qty($filters);
                break;
            case "settlement_summary":
                $func_data = $this->model_dashboard->settlement_summary($filters);
                break;
            case "monthly_order_summary":
                $func_data = $this->model_dashboard->monthly_order_summary($filters);
                break;
            default:
                break;
        }
        $board_source = $dashboard_data[0]['eBoardSource'];
        $chart_type = $dashboard_data[0]['eChartType'];
        $default_chart = $dashboard_data[0]['eDefaultChart'];
        if ($chart_type == "Pivot")
        {
            if (in_array($default_chart, $this->chart_assoc))
            {
                $key_chart = array_search($default_chart, $this->chart_assoc);
            }
            else
            {
                $key_chart = "pivot";
            }
            $ret_data = $func_data;
            $ret_data['defaultChart'] = $key_chart;
        }
        elseif ($chart_type == "Detail View")
        {
            if ($board_source == "Function")
            {
                $ret_data = $func_data['data'];
            }
            else
            {
                if ($func_data['template'])
                {
                    $parse_html = $this->parser->parse($func_data['template'], $func_data, TRUE);
                    $ret_data = $parse_html;
                }
            }
        }
        elseif ($chart_type == "Grid List")
        {
            $ret_data = $func_data;
        }
        if ($chart_type == "Pivot" || $chart_type == "Grid List")
        {
            if ($json === TRUE)
            {
                $ret_data = $this->filter->getJSONEncodeJSFunction($ret_data);
            }
        }
        return $ret_data;
    }

    public function filter_dashboard_block()
    {
        $params_arr = $this->params_arr;
        $board_code = $params_arr['code'];
        $filt_field = $params_arr['field'];
        $from_date = $params_arr['from_date'];
        $to_date = $params_arr['to_date'];

        $filters = array();
        $filters[$filt_field]['start'] = $from_date;
        $filters[$filt_field]['end'] = $to_date;
        $data_set = $this->__parse_dashboard_block($board_code, $filters);
        echo $data_set;
        $this->skip_template_view();
    }

    public function autoload_dashboard_block()
    {
        $params_arr = $this->params_arr;
        $board_code = $params_arr['code'];
        $data_set = $this->__parse_dashboard_block($board_code);
        echo $data_set;
        $this->skip_template_view();
    }

    /**
     * dashboard_sequence_a method is used to save dahsboard page sequences
     */
    public function dashboard_sequence_a()
    {
        $params_arr = $this->params_arr;
        $type = $params_arr['type'];
        $page_id = intval($params_arr['id']);
        $tab_id = intval($params_arr['tab']);
        try
        {
            $page_cond = $this->db->protect("iDashBoardPageId")." = ".$this->db->escape($page_id);
            $data_arr = $this->model_dashboardpages->getData($page_cond);
            $edit_access = $this->filter->getModuleWiseAccess($data_arr[0]['vPageCode'], "Update", FALSE, TRUE, TRUE);
            if (!$edit_access)
            {
                throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_VIEW_THIS_PAGE_C46_C46_C33'));
            }
            if ($type == 'block_sequence')
            {
                $data_str_arr = $params_arr['obj'];
                $data_str_cnt = count($data_str_arr);
                for ($i = 0, $j = 1; $i < $data_str_cnt; $i++, $j++)
                {
                    $board_id = intval($data_str_arr[$i]['chart_id']);
                    $extra_cond = $this->db->protect("iDashBoardId")." = ".$this->db->escape($board_id)." AND ".$this->db->protect("iDashBoardPageId")." = ".$this->db->escape($page_id)." AND ".$this->db->protect("iTabId")." = ".$this->db->escape($tab_id);
                    $update_arr['tLayoutJSON'] = json_encode($data_str_arr[$i]['chart_sequence']);
                    $update_arr['iBlockOrder'] = $j;
                    $result = $this->model_dashboardpagesblock->update($update_arr, $extra_cond);
                }
                if (!$result)
                {
                    throw new Exception('Error in updating the sequence');
                }
                $message = 'Sequence updated successfully';
            }
            elseif ($type == 'block_type')
            {
                $data_str_arr = $params_arr['obj'];
                $data_str_cnt = count($data_str_arr);
                for ($i = 0; $i < $data_str_cnt; $i++)
                {
                    $chart_type = $data_str_arr[$i]['chart_type'];
                    $chart_id = $data_str_arr[$i]['chart_id'];
                    $update_arr['eDefaultChart'] = $this->chart_assoc[$chart_type];
                    $result = $this->model_dashboard->update($update_arr, $chart_id);
                }
                if (!$result)
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_FALIURE_IN_UPDATING_C46_C46_C33'));
                }
                $message = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_UPDATED_SUCCESSFULLY_C46_C46_C33');
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $ret_arr['success'] = $success;
        $ret_arr['message'] = $message;
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }
}
