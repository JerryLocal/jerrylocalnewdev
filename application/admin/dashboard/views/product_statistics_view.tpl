<table id="dashboard_detail_5" class="jqgrid-subview" width="100%" cellpadding="2" cellspacing="2">
    <tr>
        <td width="35%" align="left"><strong><%$this->lang->line('DASHBOARD_PRODUCTNAME')%>: </strong></td>
        <td width="65%"><%$data['ProductName']%></td>
    </tr>
    <tr>
        <td width="35%" align="left"><strong><%$this->lang->line('DASHBOARD_CATEGORYNAME')%>: </strong></td>
        <td width="65%"><%$data['Categoryname']%></td>
    </tr>
    <tr>
        <td width="35%" align="left"><strong><%$this->lang->line('DASHBOARD_STOCKQUANTITY')%>: </strong></td>
        <td width="65%"><%$data['StockQuantity']%></td>
    </tr>
     
</table>