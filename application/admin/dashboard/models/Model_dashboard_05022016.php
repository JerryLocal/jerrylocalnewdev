<?php

/**
 * Description of Dashboard Model
 *
 * @module Dashboard
 *
 * @class model_dashboard.php
 *
 * @path application\admin\dashboard\models\model_dashboard.php
 *
 * @author CIT Dev Team
 *
 * @date 05.02.2016
 */
class Model_Dashboard extends CI_Model {

    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $rec_per_page;
    public $message;

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->table_name = "mod_admin_dashboard";
        $this->table_alias = "mad";
        $this->primary_key = "iDashBoardId";
        $this->primary_alias = "mad_dash_board_id";
        $this->rec_per_page = $this->config->item('REC_LIMIT');
    }

    /**
     * insert method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    public function insert($data = array()) {
        $this->db->insert($this->table_name, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    /**
     * update method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function update($data = array(), $where = '') {
        if (is_numeric($where)) {
            $this->db->where($this->primary_key, $where);
        } else {
            $this->db->where($where, FALSE, FALSE);
        }
        $res = $this->db->update($this->table_name, $data);
        return $res;
    }

    /**
     * delete method is used to delete data records from the database table.
     * @param string $where where is the query condition for deletion.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function delete($where = "") {
        if (is_numeric($where)) {
            $this->db->where($this->primary_key, $where);
        } else {
            $this->db->where($where, FALSE, FALSE);
        }
        return $this->db->delete($this->table_name);
    }

    /**
     * getData method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @return array $data_arr returns data records array.
     */
    public function getData($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No") {
        if (is_array($fields)) {
            $this->listing->addSelectFields($fields);
        } elseif ($fields != "") {
            $this->db->select($fields);
        } else {
            $this->db->select($this->table_alias . ".*");
        }
        $this->db->from($this->table_name . " AS " . $this->table_alias);
        if (is_array($extra_cond) && count($extra_cond) > 0) {
            $this->listing->addWhereFields($extra_cond);
        } elseif ($extra_cond != "") {
            if (is_numeric($extra_cond)) {
                $this->db->where($this->table_alias . "." . $this->primary_key, $extra_cond);
            } else {
                $this->db->where($extra_cond, FALSE, FALSE);
            }
        }
        if ($group_by != "") {
            $this->db->group_by($group_by);
        }
        if ($order_by != "") {
            $this->db->order_by($order_by);
        }
        if ($limit != "") {
            list($offset, $limit) = @explode(",", $limit);
            $this->db->limit($offset, $limit);
        }
        $list_data_obj = $this->db->get();
        $list_data = is_object($list_data_obj) ? $list_data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $list_data;
    }

    //Function related to Users State
    public function users_state($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "dashboardsellerandadmin",
            "module_name" => "dashboardsellerandadmin",
            "table_name" => "mod_admin",
            "table_alias" => "ma",
            "primary_key" => "iAdminId",
        );
        $col_config = array(
            "mgm_group_name" => array(
                "name" => "mgm_group_name",
                "label" => $this->lang->line('DASHBOARD_USER_TYPE'),
                "display_query" => "mgm.vGroupName",
            ),
            "ma_status" => array(
                "name" => "ma_status",
                "label" => $this->lang->line('DASHBOARD_STATUS'),
                "display_query" => "ma.eStatus",
            ),
            "ma_admin_id" => array(
                "name" => "ma_admin_id",
                "label" => $this->lang->line('DASHBOARD_TOTAL'),
                "display_query" => "ma.iAdminId",
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "mgm_group_name",
                "index" => "mgm_group_name",
                "unique" => "mgm_group_name",
                "label" => $this->lang->line('DASHBOARD_USER_TYPE'),
                "labelOrg" => "User Type",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array(
            array(
                "dataName" => "ma_status",
                "index" => "ma_status",
                "unique" => "ma_status",
                "label" => $this->lang->line('DASHBOARD_STATUS'),
                "labelOrg" => "Status",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $aggregates = array(
            array(
                "member" => "ma_admin_id",
                "aggregator" => "count",
                "summaryType" => "count",
                "rowTotalsText" => $this->lang->line('DASHBOARD_TOTAL'),
                "unique" => "ma_admin_id",
                "label" => $this->lang->line('DASHBOARD_TOTAL'),
                "labelOrg" => "Total",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $filters = array();
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "ma.iAdminId AS iAdminId",
        );
        $select_fields[] = array(
            "field" => "mgm.vGroupName AS mgm_group_name",
        );
        $select_fields[] = array(
            "field" => "ma.eStatus AS ma_status",
        );
        $select_fields[] = array(
            "field" => "ma.iAdminId AS ma_admin_id",
        );

        $this->db->start_cache();
        $this->db->from("mod_admin AS ma");
        $this->general->getPhysicalRecordWhere("mod_admin", "ma", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_store_detail AS msd", "msd.iAdminId = ma.iAdminId", "left");
        $this->db->join("mod_group_master AS mgm", "mgm.iGroupId = ma.iGroupId", "left");
        $this->db->where("ma.iGroupId <> 1", FALSE, FALSE);

        $this->db->order_by("mgm.vGroupName", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("mgm.vGroupName");
        $this->db->limit(5);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iAdminId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R1C";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "Yes";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "Yes";
        $assoc_data["dateFilter"] = "";
        $assoc_data["filterField"] = "";
        $assoc_data["dbID"] = "14";
        $assoc_data["gridID"] = "dblist2_14";
        $assoc_data["pagerID"] = "dbpager2_14";

        return $assoc_data;
    }

    //Function related to Seller State
    public function seller_state($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "dashboardsellersstate",
            "module_name" => "dashboardsellersstate",
            "table_name" => "mst_store_detail",
            "table_alias" => "msd",
            "primary_key" => "iMstStoreDetailId",
        );
        $col_config = array(
            "msd_date" => array(
                "name" => "msd_date",
                "label" => $this->lang->line('DASHBOARD_JOINED_MONTH'),
                "display_query" => "msd.dDate",
                "type" => "textbox",
                "php_func" => "getSellarDate",
            ),
            "msd_status" => array(
                "name" => "msd_status",
                "label" => $this->lang->line('DASHBOARD_STATUS'),
                "display_query" => "msd.eStatus",
            ),
            "msd_mst_store_detail_id" => array(
                "name" => "msd_mst_store_detail_id",
                "label" => $this->lang->line('DASHBOARD_TOTAL'),
                "display_query" => "msd.iMstStoreDetailId",
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "msd_date",
                "index" => "msd_date",
                "unique" => "msd_date",
                "label" => $this->lang->line('DASHBOARD_JOINED_MONTH'),
                "labelOrg" => "Joined Month",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => TRUE,
                "sortable" => FALSE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array(
            array(
                "dataName" => "msd_status",
                "index" => "msd_status",
                "unique" => "msd_status",
                "label" => $this->lang->line('DASHBOARD_STATUS'),
                "labelOrg" => "Status",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $aggregates = array(
            array(
                "member" => "msd_mst_store_detail_id",
                "aggregator" => "count",
                "summaryType" => "count",
                "rowTotalsText" => $this->lang->line('DASHBOARD_TOTAL'),
                "unique" => "msd_mst_store_detail_id",
                "label" => $this->lang->line('DASHBOARD_TOTAL'),
                "labelOrg" => "Total",
                "labelClass" => "header-align-center",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "center",
                "sorttype" => "text",
            )
        );
        $filters = array();
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "msd.iMstStoreDetailId AS iMstStoreDetailId",
        );
        $select_fields[] = array(
            "field" => "msd.dDate AS msd_date",
        );
        $select_fields[] = array(
            "field" => "msd.eStatus AS msd_status",
        );
        $select_fields[] = array(
            "field" => "msd.iMstStoreDetailId AS msd_mst_store_detail_id",
        );

        $this->db->start_cache();
        $this->db->from("mst_store_detail AS msd");
        $this->general->getPhysicalRecordWhere("mst_store_detail", "msd", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mod_admin AS ma", "ma.iAdminId = msd.iAdminId", "left");
        $this->db->join("mod_country AS mc", "mc.iCountryId = msd.iCountryId", "left");
        $this->db->join("mod_state AS ms", "ms.iStateId = msd.iStateId", "left");
        $this->db->join("mod_city AS mc1", "mc1.iCityId = msd.iCityId", "left");
        $this->db->join("mst_categories AS mc2", "mc2.iMstCategoriesId = msd.iMstCategoriesId", "left");

        $this->db->order_by("msd.dDate", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("msd.dDate");
        $this->db->limit(1000000);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iMstStoreDetailId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R1C";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "";
        $assoc_data["filterField"] = "";
        $assoc_data["dbID"] = "15";
        $assoc_data["gridID"] = "dblist2_15";
        $assoc_data["pagerID"] = "dbpager2_15";

        return $assoc_data;
    }

    //Function related to Customer State
    public function customer_state($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "dashboardcustomerstate",
            "module_name" => "dashboardcustomerstate",
            "table_name" => "mod_customer",
            "table_alias" => "mc",
            "primary_key" => "iCustomerId",
        );
        $col_config = array(
            "mc_t_registered_date" => array(
                "name" => "mc_t_registered_date",
                "label" => $this->lang->line('DASHBOARD_JOINED_MONTH'),
                "display_query" => "mc.dtRegisteredDate",
                "php_func" => "getSellarDate",
            ),
            "mc_status" => array(
                "name" => "mc_status",
                "label" => $this->lang->line('DASHBOARD_STATUS'),
                "display_query" => "mc.eStatus",
            ),
            "mc_customer_id" => array(
                "name" => "mc_customer_id",
                "label" => $this->lang->line('DASHBOARD_TOTAL'),
                "display_query" => "mc.iCustomerId",
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "mc_t_registered_date",
                "index" => "mc_t_registered_date",
                "unique" => "mc_t_registered_date",
                "label" => $this->lang->line('DASHBOARD_JOINED_MONTH'),
                "labelOrg" => "Joined Month",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array(
            array(
                "dataName" => "mc_status",
                "index" => "mc_status",
                "unique" => "mc_status",
                "label" => $this->lang->line('DASHBOARD_STATUS'),
                "labelOrg" => "Status",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $aggregates = array(
            array(
                "member" => "mc_customer_id",
                "aggregator" => "count",
                "summaryType" => "count",
                "rowTotalsText" => $this->lang->line('DASHBOARD_TOTAL'),
                "unique" => "mc_customer_id",
                "label" => $this->lang->line('DASHBOARD_TOTAL'),
                "labelOrg" => "Total",
                "labelClass" => "header-align-center",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "center",
                "sorttype" => "text",
            )
        );
        $filters = array();
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "mc.iCustomerId AS iCustomerId",
        );
        $select_fields[] = array(
            "field" => "mc.dtRegisteredDate AS mc_t_registered_date",
        );
        $select_fields[] = array(
            "field" => "mc.eStatus AS mc_status",
        );
        $select_fields[] = array(
            "field" => "mc.iCustomerId AS mc_customer_id",
        );

        $this->db->start_cache();
        $this->db->from("mod_customer AS mc");
        $this->general->getPhysicalRecordWhere("mod_customer", "mc", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->order_by("mc.dtRegisteredDate", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("mc.dtRegisteredDate");
        $this->db->limit(1000000);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iCustomerId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R1C";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "";
        $assoc_data["filterField"] = "";
        $assoc_data["dbID"] = "16";
        $assoc_data["gridID"] = "dblist2_16";
        $assoc_data["pagerID"] = "dbpager2_16";

        return $assoc_data;
    }

    //Function related to Settlement
    public function settlement($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "dashboardsettlement",
            "module_name" => "dashboardsettlement",
            "table_name" => "mst_sub_order",
            "table_alias" => "mso",
            "primary_key" => "iMstSubOrderId",
        );
        $col_config = array(
            "msd_store_name" => array(
                "name" => "msd_store_name",
                "label" => $this->lang->line('DASHBOARD_STORE_NAME'),
                "display_query" => "msd.vStoreName",
                "type" => "textbox",
            ),
            "mso_settlement" => array(
                "name" => "mso_settlement",
                "label" => $this->lang->line('DASHBOARD_IS_SETTLEMENT_C63'),
                "display_query" => "mso.eSettlement",
            ),
            "mso_total_cost" => array(
                "name" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_ORDER_TOTAL'),
                "display_query" => "mso.fTotalCost",
                "php_func" => "calculateDueAmount",
            ),
            "mo_date" => array(
                "name" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_DATE'),
                "display_query" => "mo.dDate",
                "type" => "date",
                "format" => 'Y-m-d',
                "php_date" => 'Y-m-d',
            ),
            "trr_total_amount" => array(
                "name" => "trr_total_amount",
                "label" => $this->lang->line('DASHBOARD_TOTAL_AMOUNT'),
                "display_query" => "trr.fTotalAmount",
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "msd_store_name",
                "index" => "msd_store_name",
                "unique" => "msd_store_name",
                "label" => $this->lang->line('DASHBOARD_STORE_NAME'),
                "labelOrg" => "Store Name",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array(
            array(
                "dataName" => "mso_settlement",
                "index" => "mso_settlement",
                "unique" => "mso_settlement",
                "label" => $this->lang->line('DASHBOARD_IS_SETTLEMENT_C63'),
                "labelOrg" => "is Settlement?",
                "labelClass" => "header-align-center",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "center",
                "sorttype" => "text",
            )
        );
        $aggregates = array(
            array(
                "member" => "mso_total_cost",
                "aggregator" => "sum",
                "summaryType" => "sum",
                "rowTotalsText" => $this->lang->line('DASHBOARD_ORDER_TOTAL'),
                "unique" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_ORDER_TOTAL'),
                "labelOrg" => "Order Total",
                "labelClass" => "header-align-right",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "right",
                "formatter" => "currency",
                "sorttype" => "text",
            )
        );
        $filters = array(
            array(
                "name" => "mo_date",
                "index" => "mo_date",
                "unique" => "mo_date",
                "datefmt" => 'Y-m-d',
                "searchoptions" => array(
                    "checkFunc" => TRUE,
                    "dataInit" => "initSearchDSDatePicker",
                    "attr" => array(
                        "aria-date-format" => 'yy-mm-dd',
                    )
                ),
                "sorttype" => "date",
            ),
            array(
                "name" => "trr_total_amount",
                "index" => "trr_total_amount",
                "unique" => "trr_total_amount",
                "sorttype" => "text",
            )
        );
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "mso.iMstSubOrderId AS iMstSubOrderId",
        );
        $select_fields[] = array(
            "field" => "msd.vStoreName AS msd_store_name",
        );
        $select_fields[] = array(
            "field" => "mso.eSettlement AS mso_settlement",
        );
        $select_fields[] = array(
            "field" => "mso.fTotalCost AS mso_total_cost",
        );
        $select_fields[] = array(
            "field" => "mo.dDate AS mo_date",
        );
        $select_fields[] = array(
            "field" => "trr.fTotalAmount AS trr_total_amount",
        );

        $this->db->start_cache();
        $this->db->from("mst_sub_order AS mso");
        $this->general->getPhysicalRecordWhere("mst_sub_order", "mso", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_order AS mo", "mo.iMstOrderId = mso.iMstOrderId", "left");
        $this->db->join("mst_store_detail AS msd", "msd.iMstStoreDetailId = mso.iMstStoreDetailId", "left");
        $this->db->join("mod_admin AS ma", "ma.iAdminId = msd.iAdminId", "left");
        $this->db->join("trn_rma_refund AS trr", "trr.iMstSubOrderId = mso.iMstSubOrderId", "left");
        $this->db->where("mso.eItemStatus = 'Delivered'  AND IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("msd.vStoreName", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("msd.vStoreName");
        $this->db->limit(1000000);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iMstSubOrderId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R1C";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "Yes";
        $assoc_data["filterField"] = "mo_date";
        $assoc_data["dbID"] = "17";
        $assoc_data["gridID"] = "dblist2_17";
        $assoc_data["pagerID"] = "dbpager2_17";

        return $assoc_data;
    }

    //Function related to Order Status (Amount)
    public function order_status($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "neworderlisting",
            "module_name" => "neworderlisting",
            "table_name" => "mst_sub_order",
            "table_alias" => "mso",
            "primary_key" => "iMstSubOrderId",
        );
        $col_config = array(
            "msd_store_name" => array(
                "name" => "msd_store_name",
                "label" => $this->lang->line('DASHBOARD_STORE'),
                "display_query" => "msd.vStoreName",
            ),
            "mso_item_status" => array(
                "name" => "mso_item_status",
                "label" => $this->lang->line('DASHBOARD_ITEM_STATUS'),
                "display_query" => "mso.eItemStatus",
            ),
            "mso_total_cost" => array(
                "name" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_TOTAL_AMT_C46'),
                "display_query" => "mso.fTotalCost",
            ),
            "mo_date" => array(
                "name" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_MONTH'),
                "display_query" => "mo.dDate",
                "type" => "date",
                "format" => 'Y-m-d',
                "php_date" => 'Y-m-d',
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "msd_store_name",
                "index" => "msd_store_name",
                "unique" => "msd_store_name",
                "label" => $this->lang->line('DASHBOARD_STORE'),
                "labelOrg" => "Store",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array(
            array(
                "dataName" => "mso_item_status",
                "index" => "mso_item_status",
                "unique" => "mso_item_status",
                "label" => $this->lang->line('DASHBOARD_ITEM_STATUS'),
                "labelOrg" => "Item Status",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $aggregates = array(
            array(
                "member" => "mso_total_cost",
                "aggregator" => "sum",
                "summaryType" => "sum",
                "rowTotalsText" => $this->lang->line('DASHBOARD_TOTAL_AMT_C46'),
                "unique" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_TOTAL_AMT_C46'),
                "labelOrg" => "Total Amt.",
                "labelClass" => "header-align-right",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "right",
                "formatter" => "currency",
                "sorttype" => "text",
            )
        );
        $filters = array(
            array(
                "name" => "mo_date",
                "index" => "mo_date",
                "unique" => "mo_date",
                "datefmt" => 'Y-m-d',
                "searchoptions" => array(
                    "checkFunc" => TRUE,
                    "dataInit" => "initSearchDSDatePicker",
                    "attr" => array(
                        "aria-date-format" => 'yy-mm-dd',
                    )
                ),
                "sorttype" => "date",
            )
        );
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "mso.iMstSubOrderId AS iMstSubOrderId",
        );
        $select_fields[] = array(
            "field" => "msd.vStoreName AS msd_store_name",
        );
        $select_fields[] = array(
            "field" => "mso.eItemStatus AS mso_item_status",
        );
        $select_fields[] = array(
            "field" => "mso.fTotalCost AS mso_total_cost",
        );
        $select_fields[] = array(
            "field" => "mo.dDate AS mo_date",
        );

        $this->db->start_cache();
        $this->db->from("mst_sub_order AS mso");
        $this->general->getPhysicalRecordWhere("mst_sub_order", "mso", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_order AS mo", "mo.iMstOrderId = mso.iMstOrderId", "inner");
        $this->db->join("mst_store_detail AS msd", "msd.iMstStoreDetailId = mso.iMstStoreDetailId", "left");
        $this->db->join("mod_admin AS ma", "ma.iAdminId = msd.iAdminId", "left");
        $this->db->where("IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("msd.vStoreName", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("msd.vStoreName");
        $this->db->limit(1000000);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iMstSubOrderId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R1C";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "Yes";
        $assoc_data["filterField"] = "mo_date";
        $assoc_data["dbID"] = "19";
        $assoc_data["gridID"] = "dblist2_19";
        $assoc_data["pagerID"] = "dbpager2_19";

        return $assoc_data;
    }

    //Function related to Order Status (Qty.)
    public function order_status_v1($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "neworderlisting",
            "module_name" => "neworderlisting_v1",
            "table_name" => "mst_sub_order",
            "table_alias" => "mso",
            "primary_key" => "iMstSubOrderId",
        );
        $col_config = array(
            "msd_store_name" => array(
                "name" => "msd_store_name",
                "label" => $this->lang->line('DASHBOARD_STORE'),
                "display_query" => "msd.vStoreName",
            ),
            "mso_item_status" => array(
                "name" => "mso_item_status",
                "label" => $this->lang->line('DASHBOARD_ITEM_STATUS'),
                "display_query" => "mso.eItemStatus",
            ),
            "mso_product_qty" => array(
                "name" => "mso_product_qty",
                "label" => $this->lang->line('DASHBOARD_QTY_C46'),
                "display_query" => "mso.iProductQty",
            ),
            "mo_date" => array(
                "name" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_MONTH'),
                "display_query" => "mo.dDate",
                "type" => "date",
                "format" => 'Y-m-d',
                "php_date" => 'Y-m-d',
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "msd_store_name",
                "index" => "msd_store_name",
                "unique" => "msd_store_name",
                "label" => $this->lang->line('DASHBOARD_STORE'),
                "labelOrg" => "Store",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array(
            array(
                "dataName" => "mso_item_status",
                "index" => "mso_item_status",
                "unique" => "mso_item_status",
                "label" => $this->lang->line('DASHBOARD_ITEM_STATUS'),
                "labelOrg" => "Item Status",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $aggregates = array(
            array(
                "member" => "mso_product_qty",
                "aggregator" => "sum",
                "summaryType" => "sum",
                "rowTotalsText" => $this->lang->line('DASHBOARD_TOTAL_QTY_C46'),
                "unique" => "mso_product_qty",
                "label" => $this->lang->line('DASHBOARD_QTY_C46'),
                "labelOrg" => "Qty.",
                "labelClass" => "header-align-center",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "center",
                "sorttype" => "text",
            )
        );
        $filters = array(
            array(
                "name" => "mo_date",
                "index" => "mo_date",
                "unique" => "mo_date",
                "datefmt" => 'Y-m-d',
                "searchoptions" => array(
                    "checkFunc" => TRUE,
                    "dataInit" => "initSearchDSDatePicker",
                    "attr" => array(
                        "aria-date-format" => 'yy-mm-dd',
                    )
                ),
                "sorttype" => "date",
            )
        );
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "mso.iMstSubOrderId AS iMstSubOrderId",
        );
        $select_fields[] = array(
            "field" => "msd.vStoreName AS msd_store_name",
        );
        $select_fields[] = array(
            "field" => "mso.eItemStatus AS mso_item_status",
        );
        $select_fields[] = array(
            "field" => "mso.iProductQty AS mso_product_qty",
        );
        $select_fields[] = array(
            "field" => "mo.dDate AS mo_date",
        );

        $this->db->start_cache();
        $this->db->from("mst_sub_order AS mso");
        $this->general->getPhysicalRecordWhere("mst_sub_order", "mso", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_order AS mo", "mo.iMstOrderId = mso.iMstOrderId", "inner");
        $this->db->join("mst_store_detail AS msd", "msd.iMstStoreDetailId = mso.iMstStoreDetailId", "left");
        $this->db->join("mod_admin AS ma", "ma.iAdminId = msd.iAdminId", "left");
        $this->db->where("IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("msd.vStoreName", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("msd.vStoreName");
        $this->db->limit(1000000);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iMstSubOrderId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R1C";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "Yes";
        $assoc_data["filterField"] = "mo_date";
        $assoc_data["dbID"] = "21";
        $assoc_data["gridID"] = "dblist2_21";
        $assoc_data["pagerID"] = "dbpager2_21";

        return $assoc_data;
    }

    //Function related to Category Statastics
    public function product_statastics($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "productmanagement",
            "module_name" => "productmanagement",
            "table_name" => "mst_products",
            "table_alias" => "mp",
            "primary_key" => "iMstProductsId",
        );
        $col_config = array(
            "tpc_mst_categories_id" => array(
                "name" => "tpc_mst_categories_id",
                "label" => $this->lang->line('DASHBOARD_CATEGORY'),
                "display_query" => "tpc.iMstCategoriesId",
                "type" => "textbox",
                "php_func" => "getTopmostParentCateogry",
            ),
            "mp_stock" => array(
                "name" => "mp_stock",
                "label" => $this->lang->line('DASHBOARD_STOCK'),
                "display_query" => "mp.iStock",
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "tpc_mst_categories_id",
                "index" => "tpc_mst_categories_id",
                "unique" => "tpc_mst_categories_id",
                "label" => $this->lang->line('DASHBOARD_CATEGORY'),
                "labelOrg" => "Category",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array();
        $aggregates = array(
            array(
                "member" => "mp_stock",
                "aggregator" => "sum",
                "summaryType" => "sum",
                "rowTotalsText" => $this->lang->line('DASHBOARD_STOCK'),
                "unique" => "mp_stock",
                "label" => $this->lang->line('DASHBOARD_STOCK'),
                "labelOrg" => "Stock",
                "labelClass" => "header-align-center",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "center",
                "sorttype" => "text",
            )
        );
        $filters = array();
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "mp.iMstProductsId AS iMstProductsId",
        );
        $select_fields[] = array(
            "field" => "tpc.iMstCategoriesId AS tpc_mst_categories_id",
        );
        $select_fields[] = array(
            "field" => "mp.iStock AS mp_stock",
        );

        $this->db->start_cache();
        $this->db->from("mst_products AS mp");
        $this->general->getPhysicalRecordWhere("mst_products", "mp", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_store_detail AS msd", "msd.iAdminId = mp.iAdminId", "left");
        $this->db->join("trn_product_categories AS tpc", "tpc.iMstProductsId = mp.iMstProductsId", "left");
        $this->db->join("mst_categories AS mc", "mc.iMstCategoriesId = tpc.iMstCategoriesId", "left");
        $this->db->where("IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("tpc.iMstCategoriesId", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("tpc.iMstCategoriesId");
        $this->db->limit(1000000);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iMstProductsId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "";
        $assoc_data["filterField"] = "";
        $assoc_data["dbID"] = "22";
        $assoc_data["gridID"] = "dblist2_22";
        $assoc_data["pagerID"] = "dbpager2_22";

        return $assoc_data;
    }

    //Function related to Low Stock Products
    public function product_statastics_v1($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "productmanagement",
            "module_name" => "productmanagement",
            "table_name" => "mst_products",
            "table_alias" => "mp",
            "primary_key" => "iMstProductsId",
        );
        $col_config = array(
            "mp_title" => array(
                "name" => "mp_title",
                "label" => $this->lang->line('DASHBOARD_PRODUCT_NAME'),
                "display_query" => "mp.vTitle",
                "type" => "textbox",
            ),
            "mp_stock" => array(
                "name" => "mp_stock",
                "label" => $this->lang->line('DASHBOARD_STOCK'),
                "display_query" => "mp.iStock",
            ),
            "msd_store_name" => array(
                "name" => "msd_store_name",
                "label" => $this->lang->line('DASHBOARD_STORE_NAME'),
                "display_query" => "msd.vStoreName",
                "type" => "textbox",
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "mp_title",
                "index" => "mp_title",
                "unique" => "mp_title",
                "label" => $this->lang->line('DASHBOARD_PRODUCT_NAME'),
                "labelOrg" => "Product Name",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array();
        $aggregates = array(
            array(
                "member" => "mp_stock",
                "aggregator" => "sum",
                "summaryType" => "sum",
                "rowTotalsText" => $this->lang->line('DASHBOARD_STOCK'),
                "unique" => "mp_stock",
                "label" => $this->lang->line('DASHBOARD_STOCK'),
                "labelOrg" => "Stock",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $filters = array(
            array(
                "name" => "msd_store_name",
                "index" => "msd_store_name",
                "unique" => "msd_store_name",
                "sorttype" => "text",
            )
        );
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "mp.iMstProductsId AS iMstProductsId",
        );
        $select_fields[] = array(
            "field" => "mp.vTitle AS mp_title",
        );
        $select_fields[] = array(
            "field" => "mp.iStock AS mp_stock",
        );
        $select_fields[] = array(
            "field" => "msd.vStoreName AS msd_store_name",
        );

        $this->db->start_cache();
        $this->db->from("mst_products AS mp");
        $this->general->getPhysicalRecordWhere("mst_products", "mp", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_store_detail AS msd", "msd.iAdminId = mp.iAdminId", "left");
        $this->db->join("trn_product_categories AS tpc", "tpc.iMstProductsId = mp.iMstProductsId", "left");
        $this->db->join("mst_categories AS mc", "mc.iMstCategoriesId = tpc.iMstCategoriesId", "left");
        $this->db->where("IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)  AND mp.iStock <=1", FALSE, FALSE);

        $this->db->order_by("mp.vTitle", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("mp.vTitle");
        $this->db->limit(1000000);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iMstProductsId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "";
        $assoc_data["filterField"] = "";
        $assoc_data["dbID"] = "23";
        $assoc_data["gridID"] = "dblist2_23";
        $assoc_data["pagerID"] = "dbpager2_23";

        return $assoc_data;
    }

    //Function related to Product Reviews
    public function product_reviews($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "productreviewforseller",
            "module_name" => "productreviewforseller",
            "table_name" => "trn_product_rating",
            "table_alias" => "tpr",
            "primary_key" => "iTrnProductRatingId",
        );
        $col_config = array(
            "mp_title" => array(
                "name" => "mp_title",
                "label" => $this->lang->line('DASHBOARD_PRODUCT_NAME'),
                "display_query" => "mp.vTitle",
                "type" => "textbox",
                "edit_link" => "Yes",
                "show_in" => "Grid",
            ),
            "tpr_rate" => array(
                "name" => "tpr_rate",
                "label" => $this->lang->line('DASHBOARD_RATING'),
                "display_query" => "tpr.iRate",
                "type" => "textbox",
                "show_in" => "Grid",
            ),
            "tpr_date" => array(
                "name" => "tpr_date",
                "label" => $this->lang->line('DASHBOARD_DATE'),
                "display_query" => "tpr.dDate",
                "type" => "date",
                "format" => $this->general->getAdminPHPFormats('date'),
                "php_date" => $this->general->getAdminPHPFormats('date'),
                "php_func" => "getFormatedDate",
                "show_in" => "Grid",
            )
        );
        $col_names = array(
            array(
                "name" => "mp_title",
                "label" => $this->lang->line('DASHBOARD_PRODUCT_NAME')
            ),
            array(
                "name" => "tpr_rate",
                "label" => $this->lang->line('DASHBOARD_RATING')
            ),
            array(
                "name" => "tpr_date",
                "label" => $this->lang->line('DASHBOARD_DATE')
            )
        );
        $col_model = array(
            array(
                "name" => "mp_title",
                "index" => "mp_title",
                "label" => $this->lang->line('DASHBOARD_PRODUCT_NAME'),
                "labelOrg" => "Product Name",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => TRUE,
                "sortable" => TRUE,
                "align" => "left",
                "checkFunc" => TRUE,
                "formatter" => "formatDashBoardEditLink",
                "unformat" => "unformatDashBoardEditLink",
                "sorttype" => "text",
            ),
            array(
                "name" => "tpr_rate",
                "index" => "tpr_rate",
                "label" => $this->lang->line('DASHBOARD_RATING'),
                "labelOrg" => "Rating",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => TRUE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "int",
            ),
            array(
                "name" => "tpr_date",
                "index" => "tpr_date",
                "label" => $this->lang->line('DASHBOARD_DATE'),
                "labelOrg" => "Date",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => TRUE,
                "sortable" => TRUE,
                "align" => "left",
                "datefmt" => $this->general->getAdminPHPFormats('date'),
                "searchoptions" => array(
                    "checkFunc" => TRUE,
                    "dataInit" => "initSearchDSDatePicker",
                    "attr" => array(
                        "aria-date-format" => $this->general->getAdminJSFormats('date', 'dateFormat')
                    )
                ),
                "sorttype" => "date",
            )
        );

        $data_model = $link_model = $data_rec = array();

        $select_fields = array();
        $select_fields[] = array(
            "field" => "tpr.iTrnProductRatingId AS iTrnProductRatingId",
        );
        $select_fields[] = array(
            "field" => "mp.vTitle AS mp_title",
        );
        $select_fields[] = array(
            "field" => "tpr.iRate AS tpr_rate",
        );
        $select_fields[] = array(
            "field" => "tpr.dDate AS tpr_date",
        );

        $this->listing->addSelectFields($select_fields);
        $this->db->from("trn_product_rating AS tpr");
        $this->general->getPhysicalRecordWhere("trn_product_rating", "tpr", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_products AS mp", "mp.iMstProductsId = tpr.iMstProductsId", "left");
        $this->db->join("mod_admin AS ma", "ma.iAdminId = mp.iAdminId", "left");
        $this->db->where("IF('" . $this->session->userdata("iGroupId") . "' = '3',mp.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("tpr.dDate", "desc");

        $this->db->limit(1000000);
        $data_obj = $this->db->get();
        $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
        //echo $this->db->last_query();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iTrnProductRatingId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["name"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Grid");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["colNames"] = $col_names;
        $assoc_data["colModel"] = $col_model;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "";
        $assoc_data["filterField"] = "";
        $assoc_data["dbID"] = "24";
        $assoc_data["gridID"] = "dblist2_24";
        $assoc_data["pagerID"] = "dbpager2_24";

        return $assoc_data;
    }

    //Function related to Store Review
    public function store_review($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "storereviewforseller",
            "module_name" => "storereviewforseller",
            "table_name" => "trn_store_rating",
            "table_alias" => "tsr",
            "primary_key" => "iTrnStoreRatingId",
        );
        $col_config = array(
            "tsr_name" => array(
                "name" => "tsr_name",
                "label" => $this->lang->line('DASHBOARD_CUSTOMER_NAME'),
                "display_query" => "tsr.vName",
                "type" => "textbox",
                "edit_link" => "Yes",
                "show_in" => "Grid",
            ),
            "tsr_rate" => array(
                "name" => "tsr_rate",
                "label" => $this->lang->line('DASHBOARD_RATING'),
                "display_query" => "tsr.iRate",
                "type" => "textbox",
                "show_in" => "Grid",
            ),
            "tsr_date" => array(
                "name" => "tsr_date",
                "label" => $this->lang->line('DASHBOARD_DATE'),
                "display_query" => "tsr.dDate",
                "type" => "date",
                "format" => $this->general->getAdminPHPFormats('date'),
                "php_date" => $this->general->getAdminPHPFormats('date'),
                "php_func" => "getFormatedDate",
                "show_in" => "Grid",
            )
        );
        $col_names = array(
            array(
                "name" => "tsr_name",
                "label" => $this->lang->line('DASHBOARD_CUSTOMER_NAME')
            ),
            array(
                "name" => "tsr_rate",
                "label" => $this->lang->line('DASHBOARD_RATING')
            ),
            array(
                "name" => "tsr_date",
                "label" => $this->lang->line('DASHBOARD_DATE')
            )
        );
        $col_model = array(
            array(
                "name" => "tsr_name",
                "index" => "tsr_name",
                "label" => $this->lang->line('DASHBOARD_CUSTOMER_NAME'),
                "labelOrg" => "Customer Name",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => TRUE,
                "sortable" => TRUE,
                "align" => "left",
                "checkFunc" => TRUE,
                "formatter" => "formatDashBoardEditLink",
                "unformat" => "unformatDashBoardEditLink",
                "sorttype" => "text",
            ),
            array(
                "name" => "tsr_rate",
                "index" => "tsr_rate",
                "label" => $this->lang->line('DASHBOARD_RATING'),
                "labelOrg" => "Rating",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "left",
                "sorttype" => "int",
            ),
            array(
                "name" => "tsr_date",
                "index" => "tsr_date",
                "label" => $this->lang->line('DASHBOARD_DATE'),
                "labelOrg" => "Date",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "datefmt" => $this->general->getAdminPHPFormats('date'),
                "searchoptions" => array(
                    "checkFunc" => TRUE,
                    "dataInit" => "initSearchDSDatePicker",
                    "attr" => array(
                        "aria-date-format" => $this->general->getAdminJSFormats('date', 'dateFormat')
                    )
                ),
                "sorttype" => "date",
            )
        );

        $data_model = $link_model = $data_rec = array();

        $select_fields = array();
        $select_fields[] = array(
            "field" => "tsr.iTrnStoreRatingId AS iTrnStoreRatingId",
        );
        $select_fields[] = array(
            "field" => "tsr.vName AS tsr_name",
        );
        $select_fields[] = array(
            "field" => "tsr.iRate AS tsr_rate",
        );
        $select_fields[] = array(
            "field" => "tsr.dDate AS tsr_date",
        );

        $this->listing->addSelectFields($select_fields);
        $this->db->from("trn_store_rating AS tsr");
        $this->general->getPhysicalRecordWhere("trn_store_rating", "tsr", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_store_detail AS msd", "msd.iMstStoreDetailId = tsr.iMstStoreDetailId", "left");
        $this->db->where("IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("tsr.dDate", "desc");

        $this->db->limit(1000000);
        $data_obj = $this->db->get();
        $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
        //echo $this->db->last_query();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iTrnStoreRatingId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["name"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Grid");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["colNames"] = $col_names;
        $assoc_data["colModel"] = $col_model;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "";
        $assoc_data["filterField"] = "";
        $assoc_data["dbID"] = "25";
        $assoc_data["gridID"] = "dblist2_25";
        $assoc_data["pagerID"] = "dbpager2_25";

        return $assoc_data;
    }

    //Function related to New Orders
    public function new_orders($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "neworderlisting",
            "module_name" => "neworderlisting",
            "table_name" => "mst_sub_order",
            "table_alias" => "mso",
            "primary_key" => "iMstSubOrderId",
        );
        $col_config = array(
            "mso_mst_sub_order_id" => array(
                "name" => "mso_mst_sub_order_id",
                "label" => $this->lang->line('DASHBOARD_SUB_ORDER_NO'),
                "display_query" => "CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber)",
                "type" => "textbox",
                "show_in" => "Grid",
            ),
            "mso_product_name" => array(
                "name" => "mso_product_name",
                "label" => $this->lang->line('DASHBOARD_PRODUCT_NAME'),
                "display_query" => "mso.vProductName",
                "type" => "textbox",
                "show_in" => "Grid",
            ),
            "mso_product_qty" => array(
                "name" => "mso_product_qty",
                "label" => $this->lang->line('DASHBOARD_QTY_C46'),
                "display_query" => "mso.iProductQty",
                "type" => "textbox",
                "show_in" => "Grid",
            ),
            "mso_total_cost" => array(
                "name" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_TOTAL'),
                "display_query" => "mso.fTotalCost",
                "type" => "textbox",
                "show_in" => "Grid",
            ),
            "mo_date" => array(
                "name" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_DATE'),
                "display_query" => "mo.dDate",
                "type" => "date",
                "format" => $this->general->getAdminPHPFormats('date'),
                "php_date" => $this->general->getAdminPHPFormats('date'),
                "php_func" => "getFormatedDate",
                "show_in" => "Grid",
            )
        );
        $col_names = array(
            array(
                "name" => "mso_mst_sub_order_id",
                "label" => $this->lang->line('DASHBOARD_SUB_ORDER_NO')
            ),
            array(
                "name" => "mso_product_name",
                "label" => $this->lang->line('DASHBOARD_PRODUCT_NAME')
            ),
            array(
                "name" => "mso_product_qty",
                "label" => $this->lang->line('DASHBOARD_QTY_C46')
            ),
            array(
                "name" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_TOTAL')
            ),
            array(
                "name" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_DATE')
            )
        );
        $col_model = array(
            array(
                "name" => "mso_mst_sub_order_id",
                "index" => "mso_mst_sub_order_id",
                "label" => $this->lang->line('DASHBOARD_SUB_ORDER_NO'),
                "labelOrg" => "Sub Order No",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => TRUE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "int",
            ),
            array(
                "name" => "mso_product_name",
                "index" => "mso_product_name",
                "label" => $this->lang->line('DASHBOARD_PRODUCT_NAME'),
                "labelOrg" => "Product Name",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => TRUE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            ),
            array(
                "name" => "mso_product_qty",
                "index" => "mso_product_qty",
                "label" => $this->lang->line('DASHBOARD_QTY_C46'),
                "labelOrg" => "Qty.",
                "labelClass" => "header-align-center",
                "resizable" => FALSE,
                "width" => "",
                "search" => TRUE,
                "sortable" => TRUE,
                "align" => "center",
                "sorttype" => "int",
            ),
            array(
                "name" => "mso_total_cost",
                "index" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_TOTAL'),
                "labelOrg" => "Total ",
                "labelClass" => "header-align-right",
                "resizable" => FALSE,
                "width" => "",
                "search" => TRUE,
                "sortable" => TRUE,
                "align" => "right",
                "formatter" => "currency",
                "sorttype" => "float",
            ),
            array(
                "name" => "mo_date",
                "index" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_DATE'),
                "labelOrg" => "Date",
                "labelClass" => "header-align-center",
                "resizable" => FALSE,
                "width" => "",
                "search" => TRUE,
                "sortable" => TRUE,
                "align" => "center",
                "datefmt" => $this->general->getAdminPHPFormats('date'),
                "searchoptions" => array(
                    "checkFunc" => TRUE,
                    "dataInit" => "initSearchDSDatePicker",
                    "attr" => array(
                        "aria-date-format" => $this->general->getAdminJSFormats('date', 'dateFormat')
                    )
                ),
                "sorttype" => "date",
            )
        );

        $data_model = $link_model = $data_rec = array();

        $select_fields = array();
        $select_fields[] = array(
            "field" => "mso.iMstSubOrderId AS iMstSubOrderId",
        );
        $select_fields[] = array(
            "field" => "CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber) AS mso_mst_sub_order_id",
            "escape" => TRUE
        );
        $select_fields[] = array(
            "field" => "mso.vProductName AS mso_product_name",
        );
        $select_fields[] = array(
            "field" => "mso.iProductQty AS mso_product_qty",
        );
        $select_fields[] = array(
            "field" => "mso.fTotalCost AS mso_total_cost",
        );
        $select_fields[] = array(
            "field" => "mo.dDate AS mo_date",
        );

        $this->listing->addSelectFields($select_fields);
        $this->db->from("mst_sub_order AS mso");
        $this->general->getPhysicalRecordWhere("mst_sub_order", "mso", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_order AS mo", "mo.iMstOrderId = mso.iMstOrderId", "inner");
        $this->db->join("mst_store_detail AS msd", "msd.iMstStoreDetailId = mso.iMstStoreDetailId", "left");
        $this->db->join("mod_admin AS ma", "ma.iAdminId = msd.iAdminId", "left");
        $this->db->where("mso.eItemStatus = 'Pending' AND IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("mo.dDate", "desc");

        $this->db->limit(1000000);
        $data_obj = $this->db->get();
        $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
        //echo $this->db->last_query();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iMstSubOrderId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["name"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Grid");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["colNames"] = $col_names;
        $assoc_data["colModel"] = $col_model;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "";
        $assoc_data["filterField"] = "";
        $assoc_data["dbID"] = "27";
        $assoc_data["gridID"] = "dblist2_27";
        $assoc_data["pagerID"] = "dbpager2_27";

        return $assoc_data;
    }

    //Function related to Latest Return Requests
    public function return_status($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "newrequest",
            "module_name" => "newrequest",
            "table_name" => "trn_rma",
            "table_alias" => "tr",
            "primary_key" => "iTrnRmaId",
        );
        $col_config = array(
            "mso_mst_sub_order_id" => array(
                "name" => "mso_mst_sub_order_id",
                "label" => $this->lang->line('DASHBOARD_SUB_ORDER_NO'),
                "display_query" => "mso.iMstSubOrderId",
                "show_in" => "Grid",
            ),
            "mso_product_name" => array(
                "name" => "mso_product_name",
                "label" => $this->lang->line('DASHBOARD_PRODUCT_NAME'),
                "display_query" => "mso.vProductName",
                "show_in" => "Grid",
            ),
            "tr_request_date" => array(
                "name" => "tr_request_date",
                "label" => $this->lang->line('DASHBOARD_REQUEST_DATE'),
                "display_query" => "tr.dRequestDate",
                "php_func" => "getFormatedDate",
                "show_in" => "Grid",
            ),
            "tr_preference" => array(
                "name" => "tr_preference",
                "label" => $this->lang->line('DASHBOARD_PREFERENCE'),
                "display_query" => "tr.ePreference",
                "show_in" => "Grid",
            ),
            "tr_rquest_type" => array(
                "name" => "tr_rquest_type",
                "label" => $this->lang->line('DASHBOARD_RQUEST_TYPE'),
                "display_query" => "tr.eRquestType",
                "show_in" => "Grid",
            )
        );
        $col_names = array(
            array(
                "name" => "mso_mst_sub_order_id",
                "label" => $this->lang->line('DASHBOARD_SUB_ORDER_NO')
            ),
            array(
                "name" => "mso_product_name",
                "label" => $this->lang->line('DASHBOARD_PRODUCT_NAME')
            ),
            array(
                "name" => "tr_request_date",
                "label" => $this->lang->line('DASHBOARD_REQUEST_DATE')
            ),
            array(
                "name" => "tr_preference",
                "label" => $this->lang->line('DASHBOARD_PREFERENCE')
            ),
            array(
                "name" => "tr_rquest_type",
                "label" => $this->lang->line('DASHBOARD_RQUEST_TYPE')
            )
        );
        $col_model = array(
            array(
                "name" => "mso_mst_sub_order_id",
                "index" => "mso_mst_sub_order_id",
                "label" => $this->lang->line('DASHBOARD_SUB_ORDER_NO'),
                "labelOrg" => "Sub Order No",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "left",
                "sorttype" => "int",
            ),
            array(
                "name" => "mso_product_name",
                "index" => "mso_product_name",
                "label" => $this->lang->line('DASHBOARD_PRODUCT_NAME'),
                "labelOrg" => "Product Name",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            ),
            array(
                "name" => "tr_request_date",
                "index" => "tr_request_date",
                "label" => $this->lang->line('DASHBOARD_REQUEST_DATE'),
                "labelOrg" => "Request Date",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "date",
            ),
            array(
                "name" => "tr_preference",
                "index" => "tr_preference",
                "label" => $this->lang->line('DASHBOARD_PREFERENCE'),
                "labelOrg" => "Preference",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "int",
            ),
            array(
                "name" => "tr_rquest_type",
                "index" => "tr_rquest_type",
                "label" => $this->lang->line('DASHBOARD_RQUEST_TYPE'),
                "labelOrg" => "Rquest Type",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "int",
            )
        );

        $data_model = $link_model = $data_rec = array();

        $select_fields = array();
        $select_fields[] = array(
            "field" => "tr.iTrnRmaId AS iTrnRmaId",
        );
        $select_fields[] = array(
            "field" => "CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber) AS mso_mst_sub_order_id",
            "escape" => TRUE
        );
        $select_fields[] = array(
            "field" => "mso.vProductName AS mso_product_name",
        );
        $select_fields[] = array(
            "field" => "tr.dRequestDate AS tr_request_date",
        );
        $select_fields[] = array(
            "field" => "tr.ePreference AS tr_preference",
        );
        $select_fields[] = array(
            "field" => "tr.eRquestType AS tr_rquest_type",
        );

        $this->listing->addSelectFields($select_fields);
        $this->db->from("trn_rma AS tr");
        $this->general->getPhysicalRecordWhere("trn_rma", "tr", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_store_detail AS msd", "msd.iMstStoreDetailId = tr.iMstStoreDetailId", "left");
        $this->db->join("mst_order AS mo", "mo.iMstOrderId = tr.iOrderId", "inner");
        $this->db->join("mst_sub_order AS mso", "mso.iMstSubOrderId = tr.iSubOrderId", "left");
        $this->db->where("IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("tr.dRequestDate", "desc");

        $this->db->limit(1000000);
        $data_obj = $this->db->get();
        $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
        //echo $this->db->last_query();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iTrnRmaId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["name"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Grid");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["colNames"] = $col_names;
        $assoc_data["colModel"] = $col_model;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "";
        $assoc_data["filterField"] = "";
        $assoc_data["dbID"] = "29";
        $assoc_data["gridID"] = "dblist2_29";
        $assoc_data["pagerID"] = "dbpager2_29";

        return $assoc_data;
    }

    //Function related to Return Summary
    public function return_summary($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "newrequest",
            "module_name" => "newrequest",
            "table_name" => "trn_rma",
            "table_alias" => "tr",
            "primary_key" => "iTrnRmaId",
        );
        $col_config = array(
            "tr_preference" => array(
                "name" => "tr_preference",
                "label" => $this->lang->line('DASHBOARD_PREFERENCE'),
                "display_query" => "tr.ePreference",
            ),
            "tr_rquest_type" => array(
                "name" => "tr_rquest_type",
                "label" => $this->lang->line('DASHBOARD_RQUEST_TYPE'),
                "display_query" => "tr.eRquestType",
            ),
            "mso_total_cost" => array(
                "name" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_ORDER_TOTAL'),
                "display_query" => "mso.fTotalCost",
            ),
            "tr_request_date" => array(
                "name" => "tr_request_date",
                "label" => $this->lang->line('DASHBOARD_REQUEST_DATE'),
                "display_query" => "tr.dRequestDate",
                "type" => "date",
                "format" => 'Y-m-d',
                "php_date" => 'Y-m-d',
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "tr_preference",
                "index" => "tr_preference",
                "unique" => "tr_preference",
                "label" => $this->lang->line('DASHBOARD_PREFERENCE'),
                "labelOrg" => "Preference",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array(
            array(
                "dataName" => "tr_rquest_type",
                "index" => "tr_rquest_type",
                "unique" => "tr_rquest_type",
                "label" => $this->lang->line('DASHBOARD_RQUEST_TYPE'),
                "labelOrg" => "Rquest Type",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $aggregates = array(
            array(
                "member" => "mso_total_cost",
                "aggregator" => "sum",
                "summaryType" => "sum",
                "rowTotalsText" => $this->lang->line('DASHBOARD_ORDER_TOTAL'),
                "unique" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_ORDER_TOTAL'),
                "labelOrg" => "Order Total",
                "labelClass" => "header-align-right",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "right",
                "formatter" => "currency",
                "sorttype" => "text",
            )
        );
        $filters = array(
            array(
                "name" => "tr_request_date",
                "index" => "tr_request_date",
                "unique" => "tr_request_date",
                "datefmt" => 'Y-m-d',
                "searchoptions" => array(
                    "checkFunc" => TRUE,
                    "dataInit" => "initSearchDSDatePicker",
                    "attr" => array(
                        "aria-date-format" => 'yy-mm-dd',
                    )
                ),
                "sorttype" => "date",
            )
        );
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "tr.iTrnRmaId AS iTrnRmaId",
        );
        $select_fields[] = array(
            "field" => "tr.ePreference AS tr_preference",
        );
        $select_fields[] = array(
            "field" => "tr.eRquestType AS tr_rquest_type",
        );
        $select_fields[] = array(
            "field" => "mso.fTotalCost AS mso_total_cost",
        );
        $select_fields[] = array(
            "field" => "tr.dRequestDate AS tr_request_date",
        );

        $this->db->start_cache();
        $this->db->from("trn_rma AS tr");
        $this->general->getPhysicalRecordWhere("trn_rma", "tr", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_store_detail AS msd", "msd.iMstStoreDetailId = tr.iMstStoreDetailId", "left");
        $this->db->join("mst_order AS mo", "mo.iMstOrderId = tr.iOrderId", "inner");
        $this->db->join("mst_sub_order AS mso", "mso.iMstSubOrderId = tr.iSubOrderId", "left");
        $this->db->where("IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("tr.ePreference", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("tr.ePreference");
        $this->db->limit(1000000);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iTrnRmaId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R1C";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "Yes";
        $assoc_data["filterField"] = "tr_request_date";
        $assoc_data["dbID"] = "30";
        $assoc_data["gridID"] = "dblist2_30";
        $assoc_data["pagerID"] = "dbpager2_30";

        return $assoc_data;
    }

    //Function related to Order Summary (Amount)
    public function order_summary_amount($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "neworderlisting",
            "module_name" => "neworderlisting",
            "table_name" => "mst_sub_order",
            "table_alias" => "mso",
            "primary_key" => "iMstSubOrderId",
        );
        $col_config = array(
            "mso_item_status" => array(
                "name" => "mso_item_status",
                "label" => $this->lang->line('DASHBOARD_ORDER__STATUS'),
                "display_query" => "mso.eItemStatus",
            ),
            "mso_total_cost" => array(
                "name" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_TOTAL'),
                "display_query" => "mso.fTotalCost",
            ),
            "mo_date" => array(
                "name" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_MONTH'),
                "display_query" => "mo.dDate",
                "type" => "date",
                "format" => '',
                "php_date" => '',
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "mso_item_status",
                "index" => "mso_item_status",
                "unique" => "mso_item_status",
                "label" => $this->lang->line('DASHBOARD_ORDER__STATUS'),
                "labelOrg" => "Order  Status",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array();
        $aggregates = array(
            array(
                "member" => "mso_total_cost",
                "aggregator" => "sum",
                "summaryType" => "sum",
                "rowTotalsText" => $this->lang->line('DASHBOARD_TOTAL'),
                "unique" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_TOTAL'),
                "labelOrg" => "Total ",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "left",
                "formatter" => "currency",
                "sorttype" => "text",
            )
        );
        $filters = array(
            array(
                "name" => "mo_date",
                "index" => "mo_date",
                "unique" => "mo_date",
                "datefmt" => '',
                "searchoptions" => array(
                    "checkFunc" => TRUE,
                    "dataInit" => "initSearchDSDatePicker",
                    "attr" => array(
                        "aria-date-format" => '',
                    )
                ),
                "sorttype" => "date",
            )
        );
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "mso.iMstSubOrderId AS iMstSubOrderId",
        );
        $select_fields[] = array(
            "field" => "mso.eItemStatus AS mso_item_status",
        );
        $select_fields[] = array(
            "field" => "mso.fTotalCost AS mso_total_cost",
        );
        $select_fields[] = array(
            "field" => "mo.dDate AS mo_date",
        );

        $this->db->start_cache();
        $this->db->from("mst_sub_order AS mso");
        $this->general->getPhysicalRecordWhere("mst_sub_order", "mso", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_order AS mo", "mo.iMstOrderId = mso.iMstOrderId", "inner");
        $this->db->join("mst_store_detail AS msd", "msd.iMstStoreDetailId = mso.iMstStoreDetailId", "left");
        $this->db->join("mod_admin AS ma", "ma.iAdminId = msd.iAdminId", "left");
        $this->db->where("IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("mso.eItemStatus", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("mso.eItemStatus");
        $this->db->limit(5);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iMstSubOrderId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "Yes";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "Yes";
        $assoc_data["filterField"] = "mo_date";
        $assoc_data["dbID"] = "31";
        $assoc_data["gridID"] = "dblist2_31";
        $assoc_data["pagerID"] = "dbpager2_31";

        return $assoc_data;
    }

    //Function related to Order Summary (Qty.)
    public function order_summary_qty($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "neworderlisting",
            "module_name" => "neworderlisting",
            "table_name" => "mst_sub_order",
            "table_alias" => "mso",
            "primary_key" => "iMstSubOrderId",
        );
        $col_config = array(
            "mso_item_status" => array(
                "name" => "mso_item_status",
                "label" => $this->lang->line('DASHBOARD_ORDER_STATUS'),
                "display_query" => "mso.eItemStatus",
            ),
            "mso_product_qty" => array(
                "name" => "mso_product_qty",
                "label" => $this->lang->line('DASHBOARD_ORDER_QTY_C46'),
                "display_query" => "mso.iProductQty",
            ),
            "mo_date" => array(
                "name" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_MONTH'),
                "display_query" => "mo.dDate",
                "type" => "date",
                "format" => 'Y-m-d',
                "php_date" => 'Y-m-d',
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "mso_item_status",
                "index" => "mso_item_status",
                "unique" => "mso_item_status",
                "label" => $this->lang->line('DASHBOARD_ORDER_STATUS'),
                "labelOrg" => "Order Status",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array();
        $aggregates = array(
            array(
                "member" => "mso_product_qty",
                "aggregator" => "sum",
                "summaryType" => "sum",
                "rowTotalsText" => $this->lang->line('DASHBOARD_ORDER_QTY_C46'),
                "unique" => "mso_product_qty",
                "label" => $this->lang->line('DASHBOARD_ORDER_QTY_C46'),
                "labelOrg" => "Order Qty.",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $filters = array(
            array(
                "name" => "mo_date",
                "index" => "mo_date",
                "unique" => "mo_date",
                "datefmt" => 'Y-m-d',
                "searchoptions" => array(
                    "checkFunc" => TRUE,
                    "dataInit" => "initSearchDSDatePicker",
                    "attr" => array(
                        "aria-date-format" => 'yy-mm-dd',
                    )
                ),
                "sorttype" => "date",
            )
        );
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "mso.iMstSubOrderId AS iMstSubOrderId",
        );
        $select_fields[] = array(
            "field" => "mso.eItemStatus AS mso_item_status",
        );
        $select_fields[] = array(
            "field" => "mso.iProductQty AS mso_product_qty",
        );
        $select_fields[] = array(
            "field" => "mo.dDate AS mo_date",
        );

        $this->db->start_cache();
        $this->db->from("mst_sub_order AS mso");
        $this->general->getPhysicalRecordWhere("mst_sub_order", "mso", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_order AS mo", "mo.iMstOrderId = mso.iMstOrderId", "inner");
        $this->db->join("mst_store_detail AS msd", "msd.iMstStoreDetailId = mso.iMstStoreDetailId", "left");
        $this->db->join("mod_admin AS ma", "ma.iAdminId = msd.iAdminId", "left");
        $this->db->where("IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("mso.eItemStatus", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("mso.eItemStatus");
        $this->db->limit(5);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iMstSubOrderId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "Yes";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "Yes";
        $assoc_data["filterField"] = "mo_date";
        $assoc_data["dbID"] = "32";
        $assoc_data["gridID"] = "dblist2_32";
        $assoc_data["pagerID"] = "dbpager2_32";

        return $assoc_data;
    }

    //Function related to Settlement Summary
    public function settlement_summary($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "dashboardsettlement",
            "module_name" => "dashboardsettlement",
            "table_name" => "mst_sub_order",
            "table_alias" => "mso",
            "primary_key" => "iMstSubOrderId",
        );
        $col_config = array(
            "mso_settlement" => array(
                "name" => "mso_settlement",
                "label" => $this->lang->line('DASHBOARD_SETTLEMENT'),
                "display_query" => "mso.eSettlement",
            ),
            "mso_total_cost" => array(
                "name" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_ORDER_TOTAL'),
                "display_query" => "mso.fTotalCost",
                "php_func" => "calculateDueAmount",
            ),
            "mo_date" => array(
                "name" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_MONTH'),
                "display_query" => "mo.dDate",
                "type" => "date",
                "format" => 'Y-m-d',
                "php_date" => 'Y-m-d',
            ),
            "trr_total_amount" => array(
                "name" => "trr_total_amount",
                "label" => $this->lang->line('DASHBOARD_TOTAL_AMOUNT'),
                "display_query" => "trr.fTotalAmount",
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "mso_settlement",
                "index" => "mso_settlement",
                "unique" => "mso_settlement",
                "label" => $this->lang->line('DASHBOARD_SETTLEMENT'),
                "labelOrg" => "Settlement",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "100",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array();
        $aggregates = array(
            array(
                "member" => "mso_total_cost",
                "aggregator" => "sum",
                "summaryType" => "sum",
                "rowTotalsText" => $this->lang->line('DASHBOARD_ORDER_TOTAL'),
                "unique" => "mso_total_cost",
                "label" => $this->lang->line('DASHBOARD_ORDER_TOTAL'),
                "labelOrg" => "Order Total",
                "labelClass" => "header-align-right",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "right",
                "formatter" => "currency",
                "sorttype" => "text",
            )
        );
        $filters = array(
            array(
                "name" => "mo_date",
                "index" => "mo_date",
                "unique" => "mo_date",
                "datefmt" => 'Y-m-d',
                "searchoptions" => array(
                    "checkFunc" => TRUE,
                    "dataInit" => "initSearchDSDatePicker",
                    "attr" => array(
                        "aria-date-format" => 'yy-mm-dd',
                    )
                ),
                "sorttype" => "date",
            ),
            array(
                "name" => "trr_total_amount",
                "index" => "trr_total_amount",
                "unique" => "trr_total_amount",
                "sorttype" => "text",
            )
        );
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "mso.iMstSubOrderId AS iMstSubOrderId",
        );
        $select_fields[] = array(
            "field" => "mso.eSettlement AS mso_settlement",
        );
        $select_fields[] = array(
            "field" => "mso.fTotalCost AS mso_total_cost",
        );
        $select_fields[] = array(
            "field" => "mo.dDate AS mo_date",
        );
        $select_fields[] = array(
            "field" => "trr.fTotalAmount AS trr_total_amount",
        );

        $this->db->start_cache();
        $this->db->from("mst_sub_order AS mso");
        $this->general->getPhysicalRecordWhere("mst_sub_order", "mso", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_order AS mo", "mo.iMstOrderId = mso.iMstOrderId", "left");
        $this->db->join("mst_store_detail AS msd", "msd.iMstStoreDetailId = mso.iMstStoreDetailId", "left");
        $this->db->join("mod_admin AS ma", "ma.iAdminId = msd.iAdminId", "left");
        $this->db->join("trn_rma_refund AS trr", "trr.iMstSubOrderId = mso.iMstSubOrderId", "left");
        $this->db->where("mso.eItemStatus = 'Delivered'  AND IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("mso.eSettlement", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("mso.eSettlement");
        $this->db->limit(5);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iMstSubOrderId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R";
        $assoc_data["aggrMode"] = "single";
        $assoc_data["hidePaging"] = "Yes";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "Yes";
        $assoc_data["filterField"] = "mo_date";
        $assoc_data["dbID"] = "33";
        $assoc_data["gridID"] = "dblist2_33";
        $assoc_data["pagerID"] = "dbpager2_33";

        return $assoc_data;
    }

    //Function related to Monthly Order Summary
    public function monthly_order_summary($search_filters = array()) {
        $assoc_data = array();

        $mod_config = array(
            "folder_name" => "neworderlisting",
            "module_name" => "neworderlisting",
            "table_name" => "mst_sub_order",
            "table_alias" => "mso",
            "primary_key" => "iMstSubOrderId",
        );
        $col_config = array(
            "mo_date" => array(
                "name" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_MONTH'),
                "display_query" => "mo.dDate",
                "php_func" => "getSellarDate",
            ),
            "mso_mst_sub_order_id" => array(
                "name" => "mso_mst_sub_order_id",
                "label" => $this->lang->line('DASHBOARD_ORDER_COUNT'),
                "display_query" => "mso.iMstSubOrderId",
            ),
            "mso_product_qty" => array(
                "name" => "mso_product_qty",
                "label" => $this->lang->line('DASHBOARD_QTY_C46'),
                "display_query" => "mso.iProductQty",
            ),
            "mo_date" => array(
                "name" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_MONTH'),
                "display_query" => "mo.dDate",
                "type" => "date",
                "format" => 'Y-m-d',
                "php_date" => 'Y-m-d',
                "php_func" => "getSellarDate",
            )
        );
        $x_dimension = array(
            array(
                "dataName" => "mo_date",
                "index" => "mo_date",
                "unique" => "mo_date",
                "label" => $this->lang->line('DASHBOARD_MONTH'),
                "labelOrg" => "Month",
                "labelClass" => "header-align-left",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "left",
                "sorttype" => "text",
            )
        );
        $y_dimension = array();
        $aggregates = array(
            array(
                "member" => "mso_mst_sub_order_id",
                "aggregator" => "count",
                "summaryType" => "count",
                "rowTotalsText" => $this->lang->line('DASHBOARD_TOTAL_ORDERS'),
                "unique" => "mso_mst_sub_order_id",
                "label" => $this->lang->line('DASHBOARD_ORDER_COUNT'),
                "labelOrg" => "Order Count",
                "labelClass" => "header-align-center",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => TRUE,
                "align" => "center",
                "sorttype" => "text",
            ),
            array(
                "member" => "mso_product_qty",
                "aggregator" => "sum",
                "summaryType" => "sum",
                "rowTotalsText" => $this->lang->line('DASHBOARD_TOTAL_QTY_C46'),
                "unique" => "mso_product_qty",
                "label" => $this->lang->line('DASHBOARD_QTY_C46'),
                "labelOrg" => "Qty.",
                "labelClass" => "header-align-center",
                "resizable" => FALSE,
                "width" => "",
                "search" => FALSE,
                "sortable" => FALSE,
                "align" => "center",
                "sorttype" => "text",
            )
        );
        $filters = array(
            array(
                "name" => "mo_date",
                "index" => "mo_date",
                "unique" => "mo_date",
                "datefmt" => 'Y-m-d',
                "searchoptions" => array(
                    "checkFunc" => TRUE,
                    "dataInit" => "initSearchDSDatePicker",
                    "attr" => array(
                        "aria-date-format" => 'yy-mm-dd',
                    )
                ),
                "sorttype" => "date",
            )
        );
        $col_model = array_merge($x_dimension, $y_dimension, $aggregates, $filters);

        $data_rec = $data_model = $link_model = $select_fields = array();

        $select_fields[] = array(
            "field" => "mso.iMstSubOrderId AS iMstSubOrderId",
        );
        $select_fields[] = array(
            "field" => "mo.dDate AS mo_date",
        );
        $select_fields[] = array(
            "field" => "mso.iMstSubOrderId AS mso_mst_sub_order_id",
        );
        $select_fields[] = array(
            "field" => "mso.iProductQty AS mso_product_qty",
        );
        $select_fields[] = array(
            "field" => "mo.dDate AS mo_date",
        );

        $this->db->start_cache();
        $this->db->from("mst_sub_order AS mso");
        $this->general->getPhysicalRecordWhere("mst_sub_order", "mso", "AR");
        $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
        if ($filter_sql != "") {
            $this->db->where("(" . $filter_sql . ")", FALSE, FALSE);
        }

        $this->db->join("mst_order AS mo", "mo.iMstOrderId = mso.iMstOrderId", "inner");
        $this->db->join("mst_store_detail AS msd", "msd.iMstStoreDetailId = mso.iMstStoreDetailId", "left");
        $this->db->join("mod_admin AS ma", "ma.iAdminId = msd.iAdminId", "left");
        $this->db->where("IF('" . $this->session->userdata("iGroupId") . "' = '3',msd.iAdminId = '" . $this->session->userdata("iAdminId") . "',1)", FALSE, FALSE);

        $this->db->order_by("mo.dDate", "DESC");

        //  $this->db->order_by("mo.dDate", "ASC");
        $this->db->stop_cache();

        $this->db->select("COUNT(*) AS cnt", FALSE);
        $this->db->group_by("mo.dDate");
        $this->db->limit(1000000);
        $sql_limit = $this->db->_compile_select();
        $db_limit_obj = $this->db->query("SELECT SUM(cnt) AS tot FROM (" . $sql_limit . ") AS result");
        $db_limit = is_object($db_limit_obj) ? $db_limit_obj->result_array() : array();
        $this->db->_reset_select();
        if (intval($db_limit[0]["tot"]) > 0) {
            $this->listing->addSelectFields($select_fields);
            $this->db->limit(intval($db_limit[0]["tot"]));
            $data_obj = $this->db->get();
            $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
            //echo $this->db->last_query();
        }
        $this->db->flush_cache();
        if (is_array($data_rec) && count($data_rec) > 0) {
            foreach ((array) $data_rec as $m => $n) {
                $temp_rec = array();
                $data_arr = $data_rec[$m];
                $id = $data_arr["iMstSubOrderId"];
                $temp_rec["id"] = $id;
                foreach ((array) $col_model as $i => $j) {
                    $name = $col_model[$i]["unique"];
                    $value = $data_arr[$name];
                    list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Pivot");
                    $temp_rec[$name] = $parse_data;
                    if ($col_config[$name]["edit_link"] == "Yes") {
                        $link_model[$id][$name] = $final_data;
                    }
                }
                $data_model[$m] = $temp_rec;
            }
        }
        $assoc_data["xDimension"] = $x_dimension;
        $assoc_data["yDimension"] = $y_dimension;
        $assoc_data["aggregates"] = $aggregates;
        $assoc_data["filters"] = $filters;
        $assoc_data["dataModel"] = $data_model;
        $assoc_data["linkModel"] = $link_model;
        $assoc_data["pivotMode"] = "1R";
        $assoc_data["aggrMode"] = "double";
        $assoc_data["hidePaging"] = "No";
        $assoc_data["rowNumber"] = "5";
        $assoc_data["frozenCols"] = "No";
        $assoc_data["searchMode"] = "No";
        $assoc_data["autoUpdate"] = "No";
        $assoc_data["dateFilter"] = "Yes";
        $assoc_data["filterField"] = "mo_date";
        $assoc_data["dbID"] = "34";
        $assoc_data["gridID"] = "dblist2_34";
        $assoc_data["pagerID"] = "dbpager2_34";

        return $assoc_data;
    }

}
