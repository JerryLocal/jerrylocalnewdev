<?php
            
/**
 * Description of Accepted Requests Extended Controller
 * 
 * @module Extended Accepted Requests
 * 
 * @class HBC_Accepted_requests.php
 * 
 * @path application\admin\acceptedrequest\controllers\HBC_Accepted_requests.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 25.01.2016
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
 
Class HBC_Accepted_requests extends Accepted_requests {
        function __construct() {
    parent::__construct();
}

function inlineEditAction() {
    $params_arr = $this->params_arr;
    $operartor = $params_arr['oper'];
    $all_row_selected = $params_arr['AllRowSelected'];
    $primary_ids = explode(",", $params_arr['id']);
    $primary_ids = count($primary_ids) > 1 ? $primary_ids : $primary_ids[0];
    $filters = $params_arr['filters'];
    if ($this->general->allowStripSlashes()) {
        $filters = $this->general->custom_stripslashes($filters);
    }
    $filters = json_decode($filters, TRUE);
    $extra_cond = '';
    $search_mode = $search_join = $search_alias = 'No';
    if ($all_row_selected == "true" && in_array($mode, array("del", "status"))) {
        $search_mode = ($mode == "del") ? "Delete" : "Update";
        $search_join = $search_alias = "Yes";
        $config_arr['module_name'] = $this->module_name;
        $config_arr['list_config'] = $this->model_accepted_requests->getListConfiguration();
        $config_arr['form_config'] = $this->model_accepted_requests->getFormConfiguration();
        $config_arr['table_name'] = $this->model_accepted_requests->table_name;
        $config_arr['table_alias'] = $this->model_accepted_requests->table_alias;
        $filter_main = $this->filter->applyFilter($filters, $config_arr, $search_mode);
        $filter_left = $this->filter->applyLeftFilter($filters, $config_arr, $search_mode);
        $filter_range = $this->filter->applyRangeFilter($filters, $config_arr, $search_mode);
        if ($filter_main != "") {
            $extra_cond .= ($extra_cond != "") ? " AND (" . $filter_main . ")" : $filter_main;
        }
        if ($filter_left != "") {
            $extra_cond .= ($extra_cond != "") ? " AND (" . $filter_left . ")" : $filter_left;
        }
        if ($filter_range != "") {
            $extra_cond .= ($extra_cond != "") ? " AND (" . $filter_range . ")" : $filter_range;
        }
    }
    if ($search_alias == "Yes") {
        $primary_field = $this->model_accepted_requests->table_alias . "." . $this->model_accepted_requests->primary_key;
    } else {
        $primary_field = $this->model_accepted_requests->primary_key;
    }
    if (is_array($primary_ids)) {
        $pk_condition = $this->db->protect($primary_field) . " IN ('" . implode("','", $primary_ids) . "')";
    } elseif (intval($primary_ids) > 0) {
        $pk_condition = $this->db->protect($primary_field) . " = " . $this->db->escape($primary_ids);
    } else {
        $pk_condition = FALSE;
    }
    if ($pk_condition) {
        $extra_cond .= ($extra_cond != "") ? " AND (" . $pk_condition . ")" : $pk_condition;
    }
    $data_arr = $save_data_arr = array();
    try {
        switch ($operartor) {
            case 'del':
                $mode = "Delete";

                $del_access = $this->filter->getModuleWiseAccess("accepted_requests", "Delete", TRUE, TRUE);
                if (!$del_access) {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_DELETE_THESE_DETAILS_C46_C46_C33'));
                }
                if ($search_mode == "No" && $pk_condition == FALSE) {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                }

                $params_arr = $this->_request_params();
                if (is_array($primary_ids)) {
                    $main_cond = $this->db->protect("tr.iTrnRmaId") . " IN ('" . @implode("','", $primary_ids) . "')";
                } else {
                    $main_cond = $this->db->protect("tr.iTrnRmaId") . " = " . $this->db->escape($primary_ids);
                }
                $main_data = $this->model_accepted_requests->getData($main_cond, array('tr.vRequestImg'));
                $success = $this->model_accepted_requests->delete($extra_cond, $search_alias, $search_join);
                if (!$success) {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                }
                $message = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_DELETED_SUCCESSFULLY_C46_C46_C33');

                $form_config = $this->model_accepted_requests->getFormConfiguration();
                $this->general->deleteMediaFiles($form_config, $main_data, $this->module_config["physical_data_remove"]);
                break;
            case 'edit':
                $mode = "Update";
                $edit_access = $this->filter->getModuleWiseAccess("accepted_requests", "Update", TRUE, TRUE);
                if (!$edit_access) {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
                }
                $post_name = $params_arr['name'];
                $post_val = is_array($params_arr['value']) ? @implode(",", $params_arr['value']) : $params_arr['value'];

                $list_config = $this->model_accepted_requests->getListConfiguration($post_name);
                $form_config = $this->model_accepted_requests->getFormConfiguration($list_config['source_field']);
                if (!is_array($form_config) || count($form_config) == 0) {
                    throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
                }
                if (in_array($form_config['type'], array("date", "date_and_time", "time", 'phone_number'))) {
                    $post_val = $this->filter->formatActionData($post_val, $form_config);
                }
                if ($form_config["encrypt"] == "Yes") {
                    $post_val = $this->listing->adminLocalEncrypt($post_val);
                }
                $field_name = $form_config['field_name'];
                $unique_name = $form_config['name'];

                $db_rec_arr = $this->model_accepted_requests->getData(intval($primary_ids));

                $data_arr[$field_name] = $post_val;
                $success = $this->model_accepted_requests->update($data_arr, intval($primary_ids));
                if (!$success) {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                }
                $message = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                break;
            case 'status':
                $mode = "Status";
                $edit_access = $this->filter->getModuleWiseAccess("accepted_requests", "Update", TRUE, TRUE);
                if (!$edit_access) {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
                }
                if ($search_mode == "No" && $pk_condition == FALSE) {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                }
                $status_field = "eRquestType";
                if ($status_field == "") {
                    throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
                }
                if ($search_mode == "Yes" || $search_alias == "Yes") {
                    $field_name = $this->model_accepted_requests->table_alias . ".eRquestType";
                } else {
                    $field_name = $status_field;
                }
                $data_arr[$field_name] = $params_arr['status'];
                $success = $this->model_accepted_requests->update($data_arr, $extra_cond, $search_alias, $search_join);
                if (!$success) {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_MODIFYING_THESE_RECORDS_C46_C46_C33'));
                }

                /* HB_0566 Start of Custom code for different message */
                if ($params_arr['status'] == 'Received') {
                    $message = "You confirm that you have received return item from customer,<br/> now you have to either issues refund or replace item as per customer preference.";
                } elseif ($params_arr['status'] == 'Replaced') {
                    $message = "You confirm that you have replaced return item now you have to issues refund if any.";
                } else {
                    $message = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_MODIFIED_SUCCESSFULLY_C46_C46_C33');
                }
                break;
        }
        $ret_arr['success'] = "true";
        $ret_arr['message'] = $message;
        if ($mode == "Update") {
            $save_data_arr = $db_rec_arr[0];
            $save_data_arr[$unique_name] = $post_val;
            if ($form_config["type"] == "file") {
                $file_data[$unique_name]["file_name"] = $post_val;
                $file_data[$unique_name]["old_file_name"] = $params_arr["old_file"];
                $file_data[$unique_name]["primary_key"] = "iTrnRmaId";
                $this->listing->uploadFilesOnSaveForm($file_data, array($unique_name => $form_config), $save_data_arr);
            }
        }
    } catch (Exception $e) {
        $ret_arr['success'] = "false";
        $ret_arr['message'] = $e->getMessage();
    }
    echo json_encode($ret_arr);
    $this->skip_template_view();
}
}
