<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<textarea id="txt_module_help" class="perm-elem-hide"></textarea>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line('GENERIC_LISTING')%> :: <%$this->lang->line('GETREFUNDEXPANDEDVIEW_GET_REFUND_EXPANDED_VIEW')%>
            </div>        
        </h3>
        <div class="header-right-drops">
        </div>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing box gradient">
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div id="scrollable_content" class="scrollable-content top-list-spacing">
        <div class="grid-data-container pad-calc-container">
            <div class="top-list-tab-layout" id="top_list_grid_layout">
            </div>
            <table class="grid-table-view " width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td id="grid_data_col">
                        <div id="pager2"></div>
                        <table id="list2"></table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>        
<input type="hidden" name="selAllRows" value="" id="selAllRows" />
<%javascript%>
    $.jgrid.no_legacy_api = true; $.jgrid.useJSON = true;
    var el_grid_settings = {}, js_col_model_json = {}, js_col_name_json = {}; 
                    
    el_grid_settings['module_name'] = '<%$module_name%>';
    el_grid_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_grid_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_grid_settings['enc_location'] = '<%$enc_loc_module%>';
    el_grid_settings['par_module '] = '<%$this->general->getAdminEncodeURL($parMod)%>';
    el_grid_settings['par_data'] = '<%$this->general->getAdminEncodeURL($parID)%>';
    el_grid_settings['par_field'] = '<%$parField%>';
    el_grid_settings['par_type'] = 'parent';
    el_grid_settings['add_page_url'] = '<%$mod_enc_url["add"]%>'; 
    el_grid_settings['edit_page_url'] =  admin_url+'<%$mod_enc_url["inline_edit_action"]%>?<%$extra_qstr%>';
    el_grid_settings['listing_url'] = admin_url+'<%$mod_enc_url["listing"]%>?<%$extra_qstr%>';
    el_grid_settings['search_refresh_url'] = admin_url+'<%$mod_enc_url["get_left_search_content"]%>?<%$extra_qstr%>';
    el_grid_settings['search_autocomp_url'] = admin_url+'<%$mod_enc_url["get_search_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['auto_complete_url'] = admin_url+'<%$mod_enc_url["get_token_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['export_url'] =  admin_url+'<%$mod_enc_url["export"]%>?<%$extra_qstr%>';
    el_grid_settings['subgrid_listing_url'] =  admin_url+'<%$mod_enc_url["get_subgrid_block"]%>?<%$extra_qstr%>';
    el_grid_settings['jparent_switchto_url'] = admin_url+'<%$parent_switch_cit["url"]%>?<%$extra_qstr%>';
    
    el_grid_settings['admin_rec_arr'] = {};
    el_grid_settings['status_arr'] = $.parseJSON('<%$status_array|@json_encode%>');
    el_grid_settings['status_lang_arr'] = $.parseJSON('<%$status_array_label|@json_encode%>');
                
    el_grid_settings['hide_add_btn'] = '1';
    el_grid_settings['hide_del_btn'] = '1';
    el_grid_settings['hide_status_btn'] = '1';
    el_grid_settings['hide_export_btn'] = '1';
    el_grid_settings['hide_columns_btn'] = 'No';
    
    el_grid_settings['hide_advance_search'] = 'No';
    el_grid_settings['hide_search_tool'] = 'No';
    el_grid_settings['hide_multi_select'] = 'No';
    el_grid_settings['popup_add_form'] = 'No';
    el_grid_settings['popup_edit_form'] = 'No';
    el_grid_settings['popup_add_size'] = ['75%', '75%'];
    el_grid_settings['popup_edit_size'] = ['75%', '75%'];
    el_grid_settings['hide_paging_btn'] = 'No';
    el_grid_settings['hide_refresh_btn'] = 'No';
    el_grid_settings['group_search'] = '';
    
    el_grid_settings['permit_add_btn'] = '<%$add_access%>';
    el_grid_settings['permit_del_btn'] = '<%$del_access%>';
    el_grid_settings['permit_edit_btn'] = '<%$view_access%>';
    el_grid_settings['permit_expo_btn'] = '<%$expo_access%>';
    
    el_grid_settings['default_sort'] = 'mso_product_sku';
    el_grid_settings['sort_order'] = 'asc';
    
    el_grid_settings['footer_row'] = 'No';
    el_grid_settings['grouping'] = 'No';
    el_grid_settings['group_attr'] = {};
    
    el_grid_settings['inline_add'] = 'No';
    el_grid_settings['rec_position'] = 'Top';
    el_grid_settings['auto_width'] = 'Yes';
    el_grid_settings['subgrid'] = 'No';
    el_grid_settings['colgrid'] = 'No';
    el_grid_settings['rating_allow'] = 'No';
    el_grid_settings['listview'] = 'list';
    el_grid_settings['filters_arr'] = $.parseJSON('<%$default_filters|@json_encode%>');
    el_grid_settings['top_filter'] = [];
    el_grid_settings['buttons_arr'] = [];
    
    js_col_name_json = [{
        "name": "mso_product_sku",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_PRODUCT_SKU_CODE')%>"
    },
    {
        "name": "mo_buyer_name",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_NAME')%>"
    },
    {
        "name": "mo_buyer_phone",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_PHONE')%>"
    },
    {
        "name": "mo_buyer_email",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_EMAIL')%>"
    },
    {
        "name": "mso_mst_sub_order_id",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_ORDER_ITEM_ID')%>"
    },
    {
        "name": "mso_shipper_name",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_LOGISTIC_COMPANY')%>"
    },
    {
        "name": "trr_reason",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_REASON')%>"
    },
    {
        "name": "mso_delivered_date",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_DELIVERY_DATE')%>"
    },
    {
        "name": "trr_refund",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_ORDER_AMOUNT_FOR_REFUND')%>"
    },
    {
        "name": "trr_comment_to_customer",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_REASON_COMMENT')%>"
    },
    {
        "name": "trr_refund_date",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_REFUND_SENT_DATE')%>"
    },
    {
        "name": "trr_tot_amount",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_REFUND_TOTAL')%>"
    },
    {
        "name": "tr_request_img",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_REQUEST_IMG')%>"
    },
    {
        "name": "mo_mst_order_id",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_ORDER_ID')%>"
    },
    {
        "name": "trr_shipping",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_SHIPPING_CONCESSION')%>"
    },
    {
        "name": "mso_tracking_number",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_TRACKING_NUMBER')%>"
    },
    {
        "name": "mo_buyer_pin_code",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_BUYER_PIN_CODE')%>"
    },
    {
        "name": "mo_buyer_area",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_BUYER_AREA')%>"
    },
    {
        "name": "mo_buyer_address2",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_BUYER_ADDRESS2')%>"
    },
    {
        "name": "mo_buyer_address1",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_BUYER_ADDRESS1')%>"
    },
    {
        "name": "mc1_country",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_BUYER_COUNTRY')%>"
    },
    {
        "name": "ms_state",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_BUYER_STATE')%>"
    },
    {
        "name": "mc2_city",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_BUYER_CITY')%>"
    },
    {
        "name": "custom_address",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_ADDRESS')%>"
    },
    {
        "name": "mso_product_name",
        "label": "<%$this->lang->line('GETREFUNDEXPANDEDVIEW_PRODUCT_NAME')%>"
    }];
    
    js_col_model_json = [{
        "name": "mso_product_sku",
        "index": "mso_product_sku",
        "label": "<%$list_config['mso_product_sku']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mso_product_sku']['width']%>",
        "search": <%if $list_config['mso_product_sku']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_product_sku']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_product_sku']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_product_sku']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_product_sku']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mso_product_sku']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_product_sku']['default']%>",
        "filterSopt": "bw",
        "formatter": formatAdminModuleEditLink,
        "unformat": unformatAdminModuleEditLink
    },
    {
        "name": "mo_buyer_name",
        "index": "mo_buyer_name",
        "label": "<%$list_config['mo_buyer_name']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mo_buyer_name']['width']%>",
        "search": <%if $list_config['mo_buyer_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_buyer_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_buyer_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_buyer_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_buyer_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mo_buyer_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mo_buyer_name']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mo_buyer_phone",
        "index": "mo_buyer_phone",
        "label": "<%$list_config['mo_buyer_phone']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mo_buyer_phone']['width']%>",
        "search": <%if $list_config['mo_buyer_phone']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_buyer_phone']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_buyer_phone']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_buyer_phone']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_buyer_phone']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mo_buyer_phone']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mo_buyer_phone']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mo_buyer_email",
        "index": "mo_buyer_email",
        "label": "<%$list_config['mo_buyer_email']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mo_buyer_email']['width']%>",
        "search": <%if $list_config['mo_buyer_email']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_buyer_email']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_buyer_email']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_buyer_email']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_buyer_email']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mo_buyer_email']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mo_buyer_email']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mso_mst_sub_order_id",
        "index": "mso_mst_sub_order_id",
        "label": "<%$list_config['mso_mst_sub_order_id']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['mso_mst_sub_order_id']['width']%>",
        "search": <%if $list_config['mso_mst_sub_order_id']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_mst_sub_order_id']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_mst_sub_order_id']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_mst_sub_order_id']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_mst_sub_order_id']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "custom_address",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mso_mst_sub_order_id']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "custom_address",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_mst_sub_order_id']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mso_shipper_name",
        "index": "mso_shipper_name",
        "label": "<%$list_config['mso_shipper_name']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mso_shipper_name']['width']%>",
        "search": <%if $list_config['mso_shipper_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_shipper_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_shipper_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_shipper_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_shipper_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mso_shipper_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_shipper_name']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "trr_reason",
        "index": "trr_reason",
        "label": "<%$list_config['trr_reason']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['trr_reason']['width']%>",
        "search": <%if $list_config['trr_reason']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_reason']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_reason']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_reason']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_reason']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "textarea",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "trr_reason",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['trr_reason']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "trr_reason",
            "rows": "1",
            "placeholder": "",
            "dataInit": initEditGridElasticEvent,
            "class": "inline-edit-row inline-textarea-edit "
        },
        "ctrl_type": "textarea",
        "default_value": "<%$list_config['trr_reason']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mso_delivered_date",
        "index": "mso_delivered_date",
        "label": "<%$list_config['mso_delivered_date']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mso_delivered_date']['width']%>",
        "search": <%if $list_config['mso_delivered_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_delivered_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_delivered_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_delivered_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_delivered_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "mso_delivered_date",
                "autocomplete": "off",
                "class": "search-inline-date",
                "aria-date-format": "<%$this->general->getAdminJSMoments('date')%>"
            },
            "sopt": dateSearchOpts,
            "searchhidden": <%if $list_config['mso_delivered_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchGridDateRangePicker
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "mso_delivered_date",
            "aria-date-format": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
            "aria-min": "",
            "aria-max": "",
            "placeholder": "",
            "class": "inline-edit-row inline-date-edit date-picker-icon dateOnly"
        },
        "ctrl_type": "date",
        "default_value": "<%$list_config['mso_delivered_date']['default']%>",
        "filterSopt": "bt"
    },
    {
        "name": "trr_refund",
        "index": "trr_refund",
        "label": "<%$list_config['trr_refund']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['trr_refund']['width']%>",
        "search": <%if $list_config['trr_refund']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_refund']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_refund']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_refund']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_refund']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "trr_refund",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['trr_refund']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "trr_refund",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['trr_refund']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency"
    },
    {
        "name": "trr_comment_to_customer",
        "index": "trr_comment_to_customer",
        "label": "<%$list_config['trr_comment_to_customer']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['trr_comment_to_customer']['width']%>",
        "search": <%if $list_config['trr_comment_to_customer']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_comment_to_customer']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_comment_to_customer']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_comment_to_customer']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_comment_to_customer']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "textarea",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "trr_comment_to_customer",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['trr_comment_to_customer']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "trr_comment_to_customer",
            "rows": "1",
            "placeholder": "",
            "dataInit": initEditGridElasticEvent,
            "class": "inline-edit-row inline-textarea-edit "
        },
        "ctrl_type": "textarea",
        "default_value": "<%$list_config['trr_comment_to_customer']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "trr_refund_date",
        "index": "trr_refund_date",
        "label": "<%$list_config['trr_refund_date']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['trr_refund_date']['width']%>",
        "search": <%if $list_config['trr_refund_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_refund_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_refund_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_refund_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_refund_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "trr_refund_date",
                "autocomplete": "off",
                "class": "search-inline-date",
                "aria-date-format": "<%$this->general->getAdminJSMoments('date')%>"
            },
            "sopt": dateSearchOpts,
            "searchhidden": <%if $list_config['trr_refund_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchGridDateRangePicker
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "trr_refund_date",
            "aria-date-format": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
            "aria-min": "",
            "aria-max": "",
            "placeholder": "",
            "class": "inline-edit-row inline-date-edit date-picker-icon dateOnly"
        },
        "ctrl_type": "date",
        "default_value": "<%$list_config['trr_refund_date']['default']%>",
        "filterSopt": "bt"
    },
    {
        "name": "trr_tot_amount",
        "index": "trr_tot_amount",
        "label": "<%$list_config['trr_tot_amount']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['trr_tot_amount']['width']%>",
        "search": <%if $list_config['trr_tot_amount']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_tot_amount']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_tot_amount']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_tot_amount']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_tot_amount']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "trr_total_amount",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['trr_tot_amount']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "trr_total_amount",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['trr_tot_amount']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency"
    },
    {
        "name": "tr_request_img",
        "index": "tr_request_img",
        "label": "<%$list_config['tr_request_img']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['tr_request_img']['width']%>",
        "search": <%if $list_config['tr_request_img']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['tr_request_img']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['tr_request_img']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['tr_request_img']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['tr_request_img']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "trr_trn_rma_id",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['tr_request_img']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["tr_request_img"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=tr_request_img&mode=<%$mod_enc_mode["Search"]%>&rformat=html'<%/if%>,
            "value": <%if $count_arr["tr_request_img"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["tr_request_img"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['tr_request_img']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["tr_request_img"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "trr_trn_rma_id",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=tr_request_img&mode=<%$mod_enc_mode["Update"]%>&rformat=html',
            "dataInit": <%if $count_arr['tr_request_img']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["tr_request_img"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETREFUNDEXPANDEDVIEW_REQUEST_IMG')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['tr_request_img']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "mo_mst_order_id",
        "index": "mo_mst_order_id",
        "label": "<%$list_config['mo_mst_order_id']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['mo_mst_order_id']['width']%>",
        "search": <%if $list_config['mo_mst_order_id']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_mst_order_id']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_mst_order_id']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_mst_order_id']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_mst_order_id']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "custom_address",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mo_mst_order_id']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "custom_address",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mo_mst_order_id']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "trr_shipping",
        "index": "trr_shipping",
        "label": "<%$list_config['trr_shipping']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['trr_shipping']['width']%>",
        "search": <%if $list_config['trr_shipping']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_shipping']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_shipping']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_shipping']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_shipping']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "trr_shipping",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['trr_shipping']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "trr_shipping",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['trr_shipping']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mso_tracking_number",
        "index": "mso_tracking_number",
        "label": "<%$list_config['mso_tracking_number']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mso_tracking_number']['width']%>",
        "search": <%if $list_config['mso_tracking_number']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_tracking_number']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_tracking_number']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_tracking_number']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_tracking_number']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mso_tracking_number']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_tracking_number']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mo_buyer_pin_code",
        "index": "mo_buyer_pin_code",
        "label": "<%$list_config['mo_buyer_pin_code']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mo_buyer_pin_code']['width']%>",
        "search": <%if $list_config['mo_buyer_pin_code']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_buyer_pin_code']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_buyer_pin_code']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_buyer_pin_code']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_buyer_pin_code']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mo_buyer_pin_code']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mo_buyer_pin_code']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mo_buyer_area",
        "index": "mo_buyer_area",
        "label": "<%$list_config['mo_buyer_area']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mo_buyer_area']['width']%>",
        "search": <%if $list_config['mo_buyer_area']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_buyer_area']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_buyer_area']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_buyer_area']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_buyer_area']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mo_buyer_area']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mo_buyer_area']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mo_buyer_address2",
        "index": "mo_buyer_address2",
        "label": "<%$list_config['mo_buyer_address2']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mo_buyer_address2']['width']%>",
        "search": <%if $list_config['mo_buyer_address2']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_buyer_address2']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_buyer_address2']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_buyer_address2']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_buyer_address2']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mo_buyer_address2']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mo_buyer_address2']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mo_buyer_address1",
        "index": "mo_buyer_address1",
        "label": "<%$list_config['mo_buyer_address1']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mo_buyer_address1']['width']%>",
        "search": <%if $list_config['mo_buyer_address1']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_buyer_address1']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_buyer_address1']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_buyer_address1']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_buyer_address1']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mo_buyer_address1']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mo_buyer_address1']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mc1_country",
        "index": "mc1_country",
        "label": "<%$list_config['mc1_country']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mc1_country']['width']%>",
        "search": <%if $list_config['mc1_country']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mc1_country']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mc1_country']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mc1_country']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mc1_country']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mc1_country']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mc1_country']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "ms_state",
        "index": "ms_state",
        "label": "<%$list_config['ms_state']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['ms_state']['width']%>",
        "search": <%if $list_config['ms_state']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['ms_state']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['ms_state']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['ms_state']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['ms_state']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['ms_state']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['ms_state']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mc2_city",
        "index": "mc2_city",
        "label": "<%$list_config['mc2_city']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mc2_city']['width']%>",
        "search": <%if $list_config['mc2_city']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mc2_city']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mc2_city']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mc2_city']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mc2_city']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mc2_city']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mc2_city']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "custom_address",
        "index": "custom_address",
        "label": "<%$list_config['custom_address']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['custom_address']['width']%>",
        "search": <%if $list_config['custom_address']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['custom_address']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['custom_address']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['custom_address']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['custom_address']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "custom_address",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['custom_address']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "custom_address",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['custom_address']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mso_product_name",
        "index": "mso_product_name",
        "label": "<%$list_config['mso_product_name']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mso_product_name']['width']%>",
        "search": <%if $list_config['mso_product_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_product_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_product_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_product_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_product_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "getrefundexpandedview",
                "aria-unique-name": "custom_address",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mso_product_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "getrefundexpandedview",
            "aria-unique-name": "custom_address",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_product_name']['default']%>",
        "filterSopt": "bw"
    }];
         
    initMainGridListing();
    createTooltipHeading();
    callSwitchToParent();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 