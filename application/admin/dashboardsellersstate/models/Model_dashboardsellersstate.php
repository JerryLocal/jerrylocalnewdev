<?php
/**
 * Description of Dashboard Sellers State Model
 *
 * @module Dashboard Sellers State
 *
 * @class model_dashboardsellersstate.php
 *
 * @path application\admin\dashboardsellersstate\models\model_dashboardsellersstate.php
 *
 * @author CIT Dev Team
 *
 * @date 22.12.2015
 */

class Model_dashboardsellersstate extends CI_Model
{
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $multi_lingual;
    public $physical_data_remove;
    //
    public $listing_data;
    public $rec_per_page;
    public $message;

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->table_name = "mst_store_detail";
        $this->table_alias = "msd";
        $this->primary_key = "iMstStoreDetailId";
        $this->primary_alias = "msd_mst_store_detail_id";
        $this->physical_data_remove = "Yes";
        $this->grid_fields = array(
            "msd_store_code",
            "msd_store_name",
            "msd_company_name",
            "msd_contact_name",
            "msd_area",
            "msd_address1",
            "msd_address2",
            "msd_contact_number",
            "mc_country",
            "ms_state",
            "mc1_city",
            "msd_pin_code",
            "msd_website_url",
            "msd_store_logo",
            "msd_store_description",
            "msd_other_proof_detail",
            "msd_bank_account_proof",
            "msd_g_s_t_i_r_d_proof",
            "msd_g_s_t_no",
            "msd_bank_account_no",
            "msd_bank_account_name",
            "msd_store_url",
            "msd_have_online_store",
            "mc2_title",
            "msd_other_proof",
            "msd_store_rate_avg",
            "msd_store_total_review",
            "msd_accept_term",
            "msd_status",
            "msd_date",
            "msd_modify_date",
            "msd_about_store",
            "msd_return_policy",
            "ma_name",
        );
        $this->join_tables = array(
            array(
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "iAdminId",
                "rel_table_name" => "mst_store_detail",
                "rel_table_alias" => "msd",
                "rel_field_name" => "iAdminId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mod_country",
                "table_alias" => "mc",
                "field_name" => "iCountryId",
                "rel_table_name" => "mst_store_detail",
                "rel_table_alias" => "msd",
                "rel_field_name" => "iCountryId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mod_state",
                "table_alias" => "ms",
                "field_name" => "iStateId",
                "rel_table_name" => "mst_store_detail",
                "rel_table_alias" => "msd",
                "rel_field_name" => "iStateId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mod_city",
                "table_alias" => "mc1",
                "field_name" => "iCityId",
                "rel_table_name" => "mst_store_detail",
                "rel_table_alias" => "msd",
                "rel_field_name" => "iCityId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mst_categories",
                "table_alias" => "mc2",
                "field_name" => "iMstCategoriesId",
                "rel_table_name" => "mst_store_detail",
                "rel_table_alias" => "msd",
                "rel_field_name" => "iMstCategoriesId",
                "join_type" => "left",
                "extra_condition" => "",
            )
        );
        $this->extra_cond = "";
        $this->groupby_cond = array();
        $this->having_cond = "";
        $this->orderby_cond = array();
        $this->unique_type = "OR";
        $this->unique_fields = array();
        $this->switchto_fields = array();
        $this->default_filters = array();
        $this->search_config = array();
        $this->relation_modules = array();
        $this->deletion_modules = array();
        $this->print_rec = "No";
        $this->multi_lingual = "No";

        $this->rec_per_page = $this->config->item('REC_LIMIT');
    }

    /**
     * insert method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    public function insert($data = array())
    {
        $this->db->insert($this->table_name, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    /**
     * update method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while updating records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function update($data = array(), $where = '', $alias = "No", $join = "No")
    {
        if ($alias == "Yes")
        {
            if ($join == "Yes")
            {
                $join_tbls = $this->addJoinTables("NR");
            }
            if (trim($join_tbls) != '')
            {
                $set_cond = array();
                foreach ($data as $key => $val)
                {
                    $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                }
                if (is_numeric($where))
                {
                    $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                }
                elseif ($where)
                {
                    $extra_cond = " WHERE ".$where;
                }
                else
                {
                    return FALSE;
                }
                $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
                $res = $this->db->query($update_query);
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->table_alias.".".$this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
            }
        }
        else
        {
            if (is_numeric($where))
            {
                $this->db->where($this->primary_key, $where);
            }
            elseif ($where)
            {
                $this->db->where($where, FALSE, FALSE);
            }
            else
            {
                return FALSE;
            }
            $res = $this->db->update($this->table_name, $data);
        }
        return $res;
    }

    /**
     * delete method is used to delete data records from the database table.
     * @param string $where where is the query condition for deletion.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while deleting records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function delete($where = "", $alias = "No", $join = "No")
    {
        if ($this->config->item('PHYSICAL_RECORD_DELETE') && $this->physical_data_remove == 'No')
        {
            if ($alias == "Yes")
            {
                if (is_array($join['joins']) && count($join['joins']))
                {
                    $join_tbls = '';
                    if ($join['list'] == "Yes")
                    {
                        $join_tbls = $this->addJoinTables("NR");
                    }
                    $join_tbls .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                }
                elseif ($join == "Yes")
                {
                    $join_tbls = $this->addJoinTables("NR");
                }
                $data = $this->general->getPhysicalRecordUpdate($this->table_alias);
                if (trim($join_tbls) != '')
                {
                    $set_cond = array();
                    foreach ($data as $key => $val)
                    {
                        $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                    }
                    if (is_numeric($where))
                    {
                        $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                    }
                    elseif ($where)
                    {
                        $extra_cond = " WHERE ".$where;
                    }
                    else
                    {
                        return FALSE;
                    }
                    $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
                    $res = $this->db->query($update_query);
                }
                else
                {
                    if (is_numeric($where))
                    {
                        $this->db->where($this->table_alias.".".$this->primary_key, $where);
                    }
                    elseif ($where)
                    {
                        $this->db->where($where, FALSE, FALSE);
                    }
                    else
                    {
                        return FALSE;
                    }
                    $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
                }
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $data = $this->general->getPhysicalRecordUpdate();
                $res = $this->db->update($this->table_name, $data);
            }
        }
        else
        {
            if ($alias == "Yes")
            {
                $del_query = "DELETE ".$this->db->protect($this->table_alias).".* FROM ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias);
                if (is_array($join['joins']) && count($join['joins']))
                {
                    if ($join['list'] == "Yes")
                    {
                        $del_query .= $this->addJoinTables("NR");
                    }
                    $del_query .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                }
                elseif ($join == "Yes")
                {
                    $del_query .= $this->addJoinTables("NR");
                }
                if (is_numeric($where))
                {
                    $del_query .= " WHERE ".$this->db->protect($this->table_alias).".".$this->db->protect($this->primary_key)." = ".$this->db->escape($where);
                }
                elseif ($where)
                {
                    $del_query .= " WHERE ".$where;
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->query($del_query);
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->delete($this->table_name);
            }
        }
        return $res;
    }

    /**
     * getData method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @param boolean $having_cond having cond is the query condition for getting conditional data.
     * @param boolean $list list is to differ listing fields or form fields.
     * @return array $data_arr returns data records array.
     */
    public function getData($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No", $having_cond = '', $list = FALSE)
    {
        if (is_array($fields))
        {
            $this->listing->addSelectFields($fields);
        }
        elseif ($fields != "")
        {
            $this->db->select($fields);
        }
        elseif ($list == TRUE)
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
            if ($this->primary_alias != "")
            {
                $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
            }
            $this->db->select("msd.vStoreCode AS msd_store_code");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("msd.vCompanyName AS msd_company_name");
            $this->db->select("msd.vContactName AS msd_contact_name");
            $this->db->select("msd.vArea AS msd_area");
            $this->db->select("msd.vAddress1 AS msd_address1");
            $this->db->select("msd.vAddress2 AS msd_address2");
            $this->db->select("msd.vContactNumber AS msd_contact_number");
            $this->db->select("mc.vCountry AS mc_country");
            $this->db->select("ms.vState AS ms_state");
            $this->db->select("mc1.vCity AS mc1_city");
            $this->db->select("msd.vPinCode AS msd_pin_code");
            $this->db->select("msd.vWebsiteUrl AS msd_website_url");
            $this->db->select("msd.vStoreLogo AS msd_store_logo");
            $this->db->select("msd.tStoreDescription AS msd_store_description");
            $this->db->select("msd.vOtherProofDetail AS msd_other_proof_detail");
            $this->db->select("msd.vBankAccountProof AS msd_bank_account_proof");
            $this->db->select("msd.vGSTIRDProof AS msd_g_s_t_i_r_d_proof");
            $this->db->select("msd.vGSTNo AS msd_g_s_t_no");
            $this->db->select("msd.vBankAccountNo AS msd_bank_account_no");
            $this->db->select("msd.vBankAccountName AS msd_bank_account_name");
            $this->db->select("msd.vStoreUrl AS msd_store_url");
            $this->db->select("msd.eHaveOnlineStore AS msd_have_online_store");
            $this->db->select("mc2.vTitle AS mc2_title");
            $this->db->select("msd.vOtherProof AS msd_other_proof");
            $this->db->select("msd.fStoreRateAvg AS msd_store_rate_avg");
            $this->db->select("msd.iStoreTotalReview AS msd_store_total_review");
            $this->db->select("msd.eAcceptTerm AS msd_accept_term");
            $this->db->select("msd.eStatus AS msd_status");
            $this->db->select("msd.dDate AS msd_date");
            $this->db->select("msd.dModifyDate AS msd_modify_date");
            $this->db->select("msd.vAboutStore AS msd_about_store");
            $this->db->select("msd.vReturnPolicy AS msd_return_policy");
            $this->db->select("ma.vName AS ma_name");
        }
        else
        {
            $this->db->select("msd.iMstStoreDetailId AS iMstStoreDetailId");
            $this->db->select("msd.iMstStoreDetailId AS msd_mst_store_detail_id");
            $this->db->select("msd.vStoreCode AS msd_store_code");
            $this->db->select("msd.iAdminId AS msd_admin_id");
            $this->db->select("msd.vStoreName AS msd_store_name");
            $this->db->select("msd.vCompanyName AS msd_company_name");
            $this->db->select("msd.vContactName AS msd_contact_name");
            $this->db->select("msd.iStateId AS msd_state_id");
            $this->db->select("msd.iCityId AS msd_city_id");
            $this->db->select("msd.vArea AS msd_area");
            $this->db->select("msd.vAddress2 AS msd_address2");
            $this->db->select("msd.vAddress1 AS msd_address1");
            $this->db->select("msd.vContactNumber AS msd_contact_number");
            $this->db->select("msd.iCountryId AS msd_country_id");
            $this->db->select("msd.vPinCode AS msd_pin_code");
            $this->db->select("msd.vWebsiteUrl AS msd_website_url");
            $this->db->select("msd.vStoreLogo AS msd_store_logo");
            $this->db->select("msd.tStoreDescription AS msd_store_description");
            $this->db->select("msd.vOtherProofDetail AS msd_other_proof_detail");
            $this->db->select("msd.vBankAccountProof AS msd_bank_account_proof");
            $this->db->select("msd.vGSTIRDProof AS msd_g_s_t_i_r_d_proof");
            $this->db->select("msd.vGSTNo AS msd_g_s_t_no");
            $this->db->select("msd.vBankAccountNo AS msd_bank_account_no");
            $this->db->select("msd.vBankAccountName AS msd_bank_account_name");
            $this->db->select("msd.vStoreUrl AS msd_store_url");
            $this->db->select("msd.eHaveOnlineStore AS msd_have_online_store");
            $this->db->select("msd.iMstCategoriesId AS msd_mst_categories_id");
            $this->db->select("msd.vOtherProof AS msd_other_proof");
            $this->db->select("msd.fStoreRateAvg AS msd_store_rate_avg");
            $this->db->select("msd.iStoreTotalReview AS msd_store_total_review");
            $this->db->select("msd.eAcceptTerm AS msd_accept_term");
            $this->db->select("msd.eStatus AS msd_status");
            $this->db->select("msd.dDate AS msd_date");
            $this->db->select("msd.dModifyDate AS msd_modify_date");
            $this->db->select("msd.vAboutStore AS msd_about_store");
            $this->db->select("msd.vReturnPolicy AS msd_return_policy");
        }

        $this->db->from($this->table_name." AS ".$this->table_alias);
        if (is_array($join) && is_array($join['joins']) && count($join['joins']) > 0)
        {
            $this->listing->addJoinTables($join['joins']);
            if ($join["list"] == "Yes")
            {
                $this->addJoinTables("AR");
            }
        }
        else
        {
            if ($join == "Yes")
            {
                $this->addJoinTables("AR");
            }
        }
        if (is_array($extra_cond) && count($extra_cond) > 0)
        {
            $this->listing->addWhereFields($extra_cond);
        }
        elseif (is_numeric($extra_cond))
        {
            $this->db->where($this->table_alias.".".$this->primary_key, intval($extra_cond));
        }
        elseif ($extra_cond)
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if ($group_by != "")
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        if ($order_by != "")
        {
            $this->db->order_by($order_by);
        }
        if ($limit != "")
        {
            if (is_numeric($limit))
            {
                $this->db->limit($limit);
            }
            else
            {
                list($offset, $limit) = @explode(",", $limit);
                $this->db->limit($offset, $limit);
            }
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }

    /**
     * getListingData method is used to get grid listing data records for this module.
     * @param array $config_arr config_arr for grid listing settigs.
     * @return array $listing_data returns data records array for grid.
     */
    public function getListingData($config_arr = array())
    {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $rec_per_page = (intval($rows) > 0) ? intval($rows) : $this->rec_per_page;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->start_cache();
        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "")
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0)
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;
        $filter_config['grid_fields'] = $this->grid_fields;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "")
        {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "")
        {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "")
        {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("msd.vStoreCode AS msd_store_code");
        $this->db->select("msd.vStoreName AS msd_store_name");
        $this->db->select("msd.vCompanyName AS msd_company_name");
        $this->db->select("msd.vContactName AS msd_contact_name");
        $this->db->select("msd.vArea AS msd_area");
        $this->db->select("msd.vAddress1 AS msd_address1");
        $this->db->select("msd.vAddress2 AS msd_address2");
        $this->db->select("msd.vContactNumber AS msd_contact_number");
        $this->db->select("mc.vCountry AS mc_country");
        $this->db->select("ms.vState AS ms_state");
        $this->db->select("mc1.vCity AS mc1_city");
        $this->db->select("msd.vPinCode AS msd_pin_code");
        $this->db->select("msd.vWebsiteUrl AS msd_website_url");
        $this->db->select("msd.vStoreLogo AS msd_store_logo");
        $this->db->select("msd.tStoreDescription AS msd_store_description");
        $this->db->select("msd.vOtherProofDetail AS msd_other_proof_detail");
        $this->db->select("msd.vBankAccountProof AS msd_bank_account_proof");
        $this->db->select("msd.vGSTIRDProof AS msd_g_s_t_i_r_d_proof");
        $this->db->select("msd.vGSTNo AS msd_g_s_t_no");
        $this->db->select("msd.vBankAccountNo AS msd_bank_account_no");
        $this->db->select("msd.vBankAccountName AS msd_bank_account_name");
        $this->db->select("msd.vStoreUrl AS msd_store_url");
        $this->db->select("msd.eHaveOnlineStore AS msd_have_online_store");
        $this->db->select("mc2.vTitle AS mc2_title");
        $this->db->select("msd.vOtherProof AS msd_other_proof");
        $this->db->select("msd.fStoreRateAvg AS msd_store_rate_avg");
        $this->db->select("msd.iStoreTotalReview AS msd_store_total_review");
        $this->db->select("msd.eAcceptTerm AS msd_accept_term");
        $this->db->select("msd.eStatus AS msd_status");
        $this->db->select("msd.dDate AS msd_date");
        $this->db->select("msd.dModifyDate AS msd_modify_date");
        $this->db->select("msd.vAboutStore AS msd_about_store");
        $this->db->select("msd.vReturnPolicy AS msd_return_policy");
        $this->db->select("ma.vName AS ma_name");

        $this->db->stop_cache();
        if ((is_array($group_by) && count($group_by) > 0) || trim($having_cond) != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key);
            $total_records_arr = $this->db->get();
            $total_records = is_object($total_records_arr) ? $total_records_arr->num_rows() : 0;
        }
        else
        {
            $total_records = $this->db->count_all_results();
        }

        $total_pages = $this->listing->getTotalPages($total_records, $rec_per_page);
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0)
        {
            foreach ($order_by as $orK => $orV)
            {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "")
        {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        $limit_offset = $this->listing->getStartIndex($total_records, $page, $rec_per_page);
        $this->db->limit($rec_per_page, $limit_offset);
        $return_data_obj = $this->db->get();
        $return_data = is_object($return_data_obj) ? $return_data_obj->result_array() : array();
        $this->db->flush_cache();
        $listing_data = $this->listing->getDataForJqGrid($return_data, $filter_config, $page, $total_pages, $total_records);
        $this->listing_data = $return_data;
        #echo $this->db->last_query();
        return $listing_data;
    }

    /**
     * getExportData method is used to get grid export data records for this module.
     * @param array $config_arr config_arr for grid export settigs.
     * @return array $export_data returns data records array for export.
     */
    public function getExportData($config_arr = array())
    {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $rowlimit = $config_arr['rowlimit'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "")
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0)
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "")
        {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "")
        {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "")
        {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("msd.vStoreCode AS msd_store_code");
        $this->db->select("msd.vStoreName AS msd_store_name");
        $this->db->select("msd.vCompanyName AS msd_company_name");
        $this->db->select("msd.vContactName AS msd_contact_name");
        $this->db->select("msd.vArea AS msd_area");
        $this->db->select("msd.vAddress1 AS msd_address1");
        $this->db->select("msd.vAddress2 AS msd_address2");
        $this->db->select("msd.vContactNumber AS msd_contact_number");
        $this->db->select("mc.vCountry AS mc_country");
        $this->db->select("ms.vState AS ms_state");
        $this->db->select("mc1.vCity AS mc1_city");
        $this->db->select("msd.vPinCode AS msd_pin_code");
        $this->db->select("msd.vWebsiteUrl AS msd_website_url");
        $this->db->select("msd.vStoreLogo AS msd_store_logo");
        $this->db->select("msd.tStoreDescription AS msd_store_description");
        $this->db->select("msd.vOtherProofDetail AS msd_other_proof_detail");
        $this->db->select("msd.vBankAccountProof AS msd_bank_account_proof");
        $this->db->select("msd.vGSTIRDProof AS msd_g_s_t_i_r_d_proof");
        $this->db->select("msd.vGSTNo AS msd_g_s_t_no");
        $this->db->select("msd.vBankAccountNo AS msd_bank_account_no");
        $this->db->select("msd.vBankAccountName AS msd_bank_account_name");
        $this->db->select("msd.vStoreUrl AS msd_store_url");
        $this->db->select("msd.eHaveOnlineStore AS msd_have_online_store");
        $this->db->select("mc2.vTitle AS mc2_title");
        $this->db->select("msd.vOtherProof AS msd_other_proof");
        $this->db->select("msd.fStoreRateAvg AS msd_store_rate_avg");
        $this->db->select("msd.iStoreTotalReview AS msd_store_total_review");
        $this->db->select("msd.eAcceptTerm AS msd_accept_term");
        $this->db->select("msd.eStatus AS msd_status");
        $this->db->select("msd.dDate AS msd_date");
        $this->db->select("msd.dModifyDate AS msd_modify_date");
        $this->db->select("msd.vAboutStore AS msd_about_store");
        $this->db->select("msd.vReturnPolicy AS msd_return_policy");
        $this->db->select("ma.vName AS ma_name");
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0)
        {
            foreach ($order_by as $orK => $orV)
            {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "")
        {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        if ($rowlimit != "")
        {
            $offset = $rowlimit;
            $limit = ($rowlimit*$page-$rowlimit);
            $this->db->limit($offset, $limit);
        }
        $export_data_obj = $this->db->get();
        $export_data = is_object($export_data_obj) ? $export_data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $export_data;
    }

    /**
     * addJoinTables method is used to make relation tables joins with main table.
     * @param string $type type is to get active record or join string.
     * @param boolean $allow_tables allow_table is to restrict some set of tables.
     * @return string $ret_joins returns relation tables join string.
     */
    public function addJoinTables($type = 'AR', $allow_tables = FALSE)
    {
        $join_tables = $this->join_tables;
        if (!is_array($join_tables) || count($join_tables) == 0)
        {
            return '';
        }
        $ret_joins = $this->listing->addJoinTables($join_tables, $type, $allow_tables);
        return $ret_joins;
    }

    /**
     * getListConfiguration method is used to get listing configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns listing configuration array.
     */
    public function getListConfiguration($name = "")
    {
        $list_config = array(
            "msd_store_code" => array(
                "name" => "msd_store_code",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreCode",
                "source_field" => "msd_store_code",
                "display_query" => "msd.vStoreCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Store Code",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_CODE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
                "edit_link" => "Yes",
            ),
            "msd_store_name" => array(
                "name" => "msd_store_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreName",
                "source_field" => "msd_store_name",
                "display_query" => "msd.vStoreName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Store Name",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_company_name" => array(
                "name" => "msd_company_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vCompanyName",
                "source_field" => "msd_company_name",
                "display_query" => "msd.vCompanyName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Company Name",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_COMPANY_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_contact_name" => array(
                "name" => "msd_contact_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vContactName",
                "source_field" => "msd_contact_name",
                "display_query" => "msd.vContactName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Contact Name",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_CONTACT_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_area" => array(
                "name" => "msd_area",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vArea",
                "source_field" => "msd_area",
                "display_query" => "msd.vArea",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Area",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_AREA'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_address1" => array(
                "name" => "msd_address1",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAddress1",
                "source_field" => "msd_address1",
                "display_query" => "msd.vAddress1",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Address1",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_ADDRESS1'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_address2" => array(
                "name" => "msd_address2",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAddress2",
                "source_field" => "msd_address2",
                "display_query" => "msd.vAddress2",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Address2",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_ADDRESS2'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_contact_number" => array(
                "name" => "msd_contact_number",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vContactNumber",
                "source_field" => "msd_contact_number",
                "display_query" => "msd.vContactNumber",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Contact Number",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_CONTACT_NUMBER'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "mc_country" => array(
                "name" => "mc_country",
                "table_name" => "mod_country",
                "table_alias" => "mc",
                "field_name" => "vCountry",
                "source_field" => "",
                "display_query" => "mc.vCountry",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "center",
                "label" => "Country",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_COUNTRY'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ms_state" => array(
                "name" => "ms_state",
                "table_name" => "mod_state",
                "table_alias" => "ms",
                "field_name" => "vState",
                "source_field" => "",
                "display_query" => "ms.vState",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "State",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mc1_city" => array(
                "name" => "mc1_city",
                "table_name" => "mod_city",
                "table_alias" => "mc1",
                "field_name" => "vCity",
                "source_field" => "",
                "display_query" => "mc1.vCity",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "City",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_CITY'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "msd_pin_code" => array(
                "name" => "msd_pin_code",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vPinCode",
                "source_field" => "msd_pin_code",
                "display_query" => "msd.vPinCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Pin Code",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_PIN_CODE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_website_url" => array(
                "name" => "msd_website_url",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vWebsiteUrl",
                "source_field" => "msd_website_url",
                "display_query" => "msd.vWebsiteUrl",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Website Url",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_WEBSITE_URL'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_store_logo" => array(
                "name" => "msd_store_logo",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreLogo",
                "source_field" => "msd_store_logo",
                "display_query" => "msd.vStoreLogo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Store Logo",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_LOGO'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_store_description" => array(
                "name" => "msd_store_description",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "tStoreDescription",
                "source_field" => "msd_store_description",
                "display_query" => "msd.tStoreDescription",
                "entry_type" => "Table",
                "data_type" => "text",
                "show_in" => "Both",
                "type" => "textarea",
                "align" => "left",
                "label" => "Store Description",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_DESCRIPTION'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_other_proof_detail" => array(
                "name" => "msd_other_proof_detail",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vOtherProofDetail",
                "source_field" => "msd_other_proof_detail",
                "display_query" => "msd.vOtherProofDetail",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Other Proof Detail",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_OTHER_PROOF_DETAIL'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_bank_account_proof" => array(
                "name" => "msd_bank_account_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountProof",
                "source_field" => "msd_bank_account_proof",
                "display_query" => "msd.vBankAccountProof",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Bank Account Proof",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_PROOF'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_g_s_t_i_r_d_proof" => array(
                "name" => "msd_g_s_t_i_r_d_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vGSTIRDProof",
                "source_field" => "msd_g_s_t_i_r_d_proof",
                "display_query" => "msd.vGSTIRDProof",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "GSTIRD Proof",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_GSTIRD_PROOF'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_g_s_t_no" => array(
                "name" => "msd_g_s_t_no",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vGSTNo",
                "source_field" => "msd_g_s_t_no",
                "display_query" => "msd.vGSTNo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "GST No",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_GST_NO'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_bank_account_no" => array(
                "name" => "msd_bank_account_no",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountNo",
                "source_field" => "msd_bank_account_no",
                "display_query" => "msd.vBankAccountNo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Bank Account No",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_NO'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_bank_account_name" => array(
                "name" => "msd_bank_account_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountName",
                "source_field" => "msd_bank_account_name",
                "display_query" => "msd.vBankAccountName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Bank Account Name",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_store_url" => array(
                "name" => "msd_store_url",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreUrl",
                "source_field" => "msd_store_url",
                "display_query" => "msd.vStoreUrl",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Store Url",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_URL'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_have_online_store" => array(
                "name" => "msd_have_online_store",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eHaveOnlineStore",
                "source_field" => "msd_have_online_store",
                "display_query" => "msd.eHaveOnlineStore",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Have Online Store",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_HAVE_ONLINE_STORE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "mc2_title" => array(
                "name" => "mc2_title",
                "table_name" => "mst_categories",
                "table_alias" => "mc2",
                "field_name" => "vTitle",
                "source_field" => "",
                "display_query" => "mc2.vTitle",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "center",
                "label" => "Category Name",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_CATEGORY_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "msd_other_proof" => array(
                "name" => "msd_other_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vOtherProof",
                "source_field" => "msd_other_proof",
                "display_query" => "msd.vOtherProof",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Other Proof",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_OTHER_PROOF'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_store_rate_avg" => array(
                "name" => "msd_store_rate_avg",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "fStoreRateAvg",
                "source_field" => "msd_store_rate_avg",
                "display_query" => "msd.fStoreRateAvg",
                "entry_type" => "Table",
                "data_type" => "double",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "right",
                "label" => "Store Rate Avg",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_RATE_AVG'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_store_total_review" => array(
                "name" => "msd_store_total_review",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iStoreTotalReview",
                "source_field" => "msd_store_total_review",
                "display_query" => "msd.iStoreTotalReview",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Store Total Review",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_TOTAL_REVIEW'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_accept_term" => array(
                "name" => "msd_accept_term",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eAcceptTerm",
                "source_field" => "msd_accept_term",
                "display_query" => "msd.eAcceptTerm",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Accept Term",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_ACCEPT_TERM'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_status" => array(
                "name" => "msd_status",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eStatus",
                "source_field" => "msd_status",
                "display_query" => "msd.eStatus",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Status",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STATUS'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
                "default" => $this->filter->getDefaultValue("msd_status",
                "Text",
                "Pending")
            ),
            "msd_date" => array(
                "name" => "msd_date",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "dDate",
                "source_field" => "msd_date",
                "display_query" => "msd.dDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Date",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_DATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
                "format" => 'Y-m-d',
                "php_date" => "M-Y",
            ),
            "msd_modify_date" => array(
                "name" => "msd_modify_date",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "dModifyDate",
                "source_field" => "msd_modify_date",
                "display_query" => "msd.dModifyDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Modify Date",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_MODIFY_DATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
                "format" => 'Y-m-d',
            ),
            "msd_about_store" => array(
                "name" => "msd_about_store",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAboutStore",
                "source_field" => "msd_about_store",
                "display_query" => "msd.vAboutStore",
                "entry_type" => "Table",
                "data_type" => "text",
                "show_in" => "Both",
                "type" => "textarea",
                "align" => "left",
                "label" => "About Store",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_ABOUT_STORE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "msd_return_policy" => array(
                "name" => "msd_return_policy",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vReturnPolicy",
                "source_field" => "msd_return_policy",
                "display_query" => "msd.vReturnPolicy",
                "entry_type" => "Table",
                "data_type" => "text",
                "show_in" => "Both",
                "type" => "textarea",
                "align" => "left",
                "label" => "Return Policy",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_RETURN_POLICY'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
            ),
            "ma_name" => array(
                "name" => "ma_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vName",
                "source_field" => "msd_admin_id",
                "display_query" => "ma.vName",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "left",
                "label" => "Name",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_NAME'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "Yes",
                "viewedit" => "Yes",
                "related" => "Yes",
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0)
        {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++)
            {
                $config_arr[$name[$i]] = $list_config[$name[$i]];
            }
        }
        elseif ($name != "" && is_string($name))
        {
            $config_arr = $list_config[$name];
        }
        else
        {
            $config_arr = $list_config;
        }
        return $config_arr;
    }

    /**
     * getFormConfiguration method is used to get form configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns form configuration array.
     */
    public function getFormConfiguration($name = "")
    {
        $form_config = array(
            "msd_store_code" => array(
                "name" => "msd_store_code",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Store Code",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_CODE')
            ),
            "msd_admin_id" => array(
                "name" => "msd_admin_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iAdminId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Admin Id",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_ADMIN_ID')
            ),
            "msd_store_name" => array(
                "name" => "msd_store_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Store Name",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_NAME')
            ),
            "msd_company_name" => array(
                "name" => "msd_company_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vCompanyName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Company Name",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_COMPANY_NAME')
            ),
            "msd_contact_name" => array(
                "name" => "msd_contact_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vContactName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Contact Name",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_CONTACT_NAME')
            ),
            "msd_state_id" => array(
                "name" => "msd_state_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iStateId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "State Id",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STATE_ID')
            ),
            "msd_city_id" => array(
                "name" => "msd_city_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iCityId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "City Id",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_CITY_ID')
            ),
            "msd_area" => array(
                "name" => "msd_area",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vArea",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Area",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_AREA')
            ),
            "msd_address2" => array(
                "name" => "msd_address2",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAddress2",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Address2",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_ADDRESS2')
            ),
            "msd_address1" => array(
                "name" => "msd_address1",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAddress1",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Address1",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_ADDRESS1')
            ),
            "msd_contact_number" => array(
                "name" => "msd_contact_number",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vContactNumber",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Contact Number",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_CONTACT_NUMBER')
            ),
            "msd_country_id" => array(
                "name" => "msd_country_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iCountryId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Country Id",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_COUNTRY_ID')
            ),
            "msd_pin_code" => array(
                "name" => "msd_pin_code",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vPinCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Pin Code",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_PIN_CODE')
            ),
            "msd_website_url" => array(
                "name" => "msd_website_url",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vWebsiteUrl",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Website Url",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_WEBSITE_URL')
            ),
            "msd_store_logo" => array(
                "name" => "msd_store_logo",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreLogo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Store Logo",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_LOGO')
            ),
            "msd_store_description" => array(
                "name" => "msd_store_description",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "tStoreDescription",
                "entry_type" => "Table",
                "data_type" => "text",
                "type" => "textarea",
                "label" => "Store Description",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_DESCRIPTION')
            ),
            "msd_other_proof_detail" => array(
                "name" => "msd_other_proof_detail",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vOtherProofDetail",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Other Proof Detail",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_OTHER_PROOF_DETAIL')
            ),
            "msd_bank_account_proof" => array(
                "name" => "msd_bank_account_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountProof",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Bank Account Proof",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_PROOF')
            ),
            "msd_g_s_t_i_r_d_proof" => array(
                "name" => "msd_g_s_t_i_r_d_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vGSTIRDProof",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "G S T I R D Proof",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_G_S_T_I_R_D_PROOF')
            ),
            "msd_g_s_t_no" => array(
                "name" => "msd_g_s_t_no",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vGSTNo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "G S T No",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_G_S_T_NO')
            ),
            "msd_bank_account_no" => array(
                "name" => "msd_bank_account_no",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountNo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Bank Account No",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_NO')
            ),
            "msd_bank_account_name" => array(
                "name" => "msd_bank_account_name",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vBankAccountName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Bank Account Name",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_NAME')
            ),
            "msd_store_url" => array(
                "name" => "msd_store_url",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vStoreUrl",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Store Url",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_URL')
            ),
            "msd_have_online_store" => array(
                "name" => "msd_have_online_store",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eHaveOnlineStore",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Have Online Store",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_HAVE_ONLINE_STORE')
            ),
            "msd_mst_categories_id" => array(
                "name" => "msd_mst_categories_id",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iMstCategoriesId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Mst Categories Id",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_MST_CATEGORIES_ID')
            ),
            "msd_other_proof" => array(
                "name" => "msd_other_proof",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vOtherProof",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Other Proof",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_OTHER_PROOF')
            ),
            "msd_store_rate_avg" => array(
                "name" => "msd_store_rate_avg",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "fStoreRateAvg",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Store Rate Avg",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_RATE_AVG')
            ),
            "msd_store_total_review" => array(
                "name" => "msd_store_total_review",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iStoreTotalReview",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Store Total Review",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STORE_TOTAL_REVIEW')
            ),
            "msd_accept_term" => array(
                "name" => "msd_accept_term",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eAcceptTerm",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Accept Term",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_ACCEPT_TERM')
            ),
            "msd_status" => array(
                "name" => "msd_status",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "eStatus",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Status",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_STATUS'),
                "default" => $this->filter->getDefaultValue("msd_status",
                "Text",
                "Pending")
            ),
            "msd_date" => array(
                "name" => "msd_date",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "dDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "type" => "date",
                "label" => "Date",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_DATE'),
                "format" => 'Y-m-d',
            ),
            "msd_modify_date" => array(
                "name" => "msd_modify_date",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "dModifyDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "type" => "date",
                "label" => "Modify Date",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_MODIFY_DATE'),
                "format" => 'Y-m-d',
            ),
            "msd_about_store" => array(
                "name" => "msd_about_store",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vAboutStore",
                "entry_type" => "Table",
                "data_type" => "text",
                "type" => "textarea",
                "label" => "About Store",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_ABOUT_STORE')
            ),
            "msd_return_policy" => array(
                "name" => "msd_return_policy",
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "vReturnPolicy",
                "entry_type" => "Table",
                "data_type" => "text",
                "type" => "textarea",
                "label" => "Return Policy",
                "label_lang" => $this->lang->line('DASHBOARDSELLERSSTATE_RETURN_POLICY')
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0)
        {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++)
            {
                $config_arr[$name[$i]] = $form_config[$name[$i]];
            }
        }
        elseif ($name != "" && is_string($name))
        {
            $config_arr = $form_config[$name];
        }
        else
        {
            $config_arr = $form_config;
        }
        return $config_arr;
    }

    /**
     * checkRecordExists method is used to check duplication of records.
     * @param array $field_arr field_arr is having fields to check.
     * @param array $field_val field_val is having values of respective fields.
     * @param numeric $id id is to avoid current records.
     * @param string $mode mode is having either Add or Update.
     * @param string $con con is having either AND or OR.
     * @return boolean $exists returns either TRUE of FALSE.
     */
    public function checkRecordExists($field_arr = array(), $field_val = array(), $id = '', $mode = '', $con = 'AND')
    {
        $exists = FALSE;
        if (!is_array($field_arr) || count($field_arr) == 0)
        {
            return $exists;
        }
        foreach ((array) $field_arr as $key => $val)
        {
            $extra_cond_arr[] = $this->db->protect($this->table_alias.".".$field_arr[$key])." =  ".$this->db->escape($field_val[$val]);
        }
        $extra_cond = "(".@implode(" ".$con." ", $extra_cond_arr).")";
        if ($mode == "Add")
        {
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0)
            {
                $exists = TRUE;
            }
        }
        elseif ($mode == "Update")
        {
            $extra_cond = $this->db->protect($this->table_alias.".".$this->primary_key)." <> ".$this->db->escape($id)." AND ".$extra_cond;
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0)
            {
                $exists = TRUE;
            }
        }
        return $exists;
    }

    /**
     * getSwitchTo method is used to get switch to dropdown array.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @return array $switch_data returns data records array.
     */
    public function getSwitchTo($extra_cond = '', $type = 'records', $limit = '')
    {
        $switchto_fields = $this->switchto_fields;
        $switch_data = array();
        if (!is_array($switchto_fields) || count($switchto_fields) == 0)
        {
            if ($type == "count")
            {
                return count($switch_data);
            }
            else
            {
                return $switch_data;
            }
        }
        $fields_arr = array();
        $fields_arr[] = array(
            "field" => $this->table_alias.".".$this->primary_key." AS id",
        );
        $fields_arr[] = array(
            "field" => $this->db->concat($switchto_fields)." AS val",
            "escape" => TRUE,
        );
        if (trim($this->extra_cond) != "")
        {
            $extra_cond = (trim($extra_cond) != "") ? $extra_cond." AND ".$this->extra_cond : $this->extra_cond;
        }
        $switch_data = $this->getData($extra_cond, $fields_arr, "val ASC", "", $limit, "Yes");
        #echo $this->db->last_query();
        if ($type == "count")
        {
            return count($switch_data);
        }
        else
        {
            return $switch_data;
        }
    }
}
