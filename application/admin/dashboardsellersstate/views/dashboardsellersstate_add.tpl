<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('DASHBOARDSELLERSSTATE_DASHBOARD_SELLERS_STATE')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','DASHBOARDSELLERSSTATE_DASHBOARD_SELLERS_STATE')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="dashboardsellersstate" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="dashboardsellersstate" class="frm-elem-block frm-stand-view">
            <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                <div class="main-content-block" id="main_content_block">
                    <div style="width:98%" class="frm-block-layout pad-calc-container">
                        <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                            <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('DASHBOARDSELLERSSTATE_DASHBOARD_SELLERS_STATE')%></h4></div>
                            <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                <div class="form-row row-fluid" id="cc_sh_msd_store_code">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_CODE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_store_code']|@htmlentities%>" name="msd_store_code" id="msd_store_code" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_CODE')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_store_codeErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_admin_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_ADMIN_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['msd_admin_id']%>
                                        <%$this->dropdown->display("msd_admin_id","msd_admin_id","  title='<%$this->lang->line('DASHBOARDSELLERSSTATE_ADMIN_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DASHBOARDSELLERSSTATE_ADMIN_ID')%>'  ", "|||", "", $opt_selected,"msd_admin_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_admin_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_store_name">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_NAME')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_store_name']|@htmlentities%>" name="msd_store_name" id="msd_store_name" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_NAME')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_store_nameErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_company_name">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_COMPANY_NAME')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_company_name']|@htmlentities%>" name="msd_company_name" id="msd_company_name" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_COMPANY_NAME')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_company_nameErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_contact_name">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_CONTACT_NAME')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_contact_name']|@htmlentities%>" name="msd_contact_name" id="msd_contact_name" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_CONTACT_NAME')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_contact_nameErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_state_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_STATE_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['msd_state_id']%>
                                        <%$this->dropdown->display("msd_state_id","msd_state_id","  title='<%$this->lang->line('DASHBOARDSELLERSSTATE_STATE_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DASHBOARDSELLERSSTATE_STATE_ID')%>'  ", "|||", "", $opt_selected,"msd_state_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_state_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_city_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_CITY_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['msd_city_id']%>
                                        <%$this->dropdown->display("msd_city_id","msd_city_id","  title='<%$this->lang->line('DASHBOARDSELLERSSTATE_CITY_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DASHBOARDSELLERSSTATE_CITY_ID')%>'  ", "|||", "", $opt_selected,"msd_city_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_city_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_area">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_AREA')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_area']|@htmlentities%>" name="msd_area" id="msd_area" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_AREA')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_areaErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_address2">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_ADDRESS2')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_address2']|@htmlentities%>" name="msd_address2" id="msd_address2" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_ADDRESS2')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_address2Err'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_address1">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_ADDRESS1')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_address1']|@htmlentities%>" name="msd_address1" id="msd_address1" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_ADDRESS1')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_address1Err'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_contact_number">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_CONTACT_NUMBER')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_contact_number']|@htmlentities%>" name="msd_contact_number" id="msd_contact_number" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_CONTACT_NUMBER')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_contact_numberErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_country_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_COUNTRY_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['msd_country_id']%>
                                        <%$this->dropdown->display("msd_country_id","msd_country_id","  title='<%$this->lang->line('DASHBOARDSELLERSSTATE_COUNTRY_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DASHBOARDSELLERSSTATE_COUNTRY_ID')%>'  ", "|||", "", $opt_selected,"msd_country_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_country_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_pin_code">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_PIN_CODE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_pin_code']|@htmlentities%>" name="msd_pin_code" id="msd_pin_code" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_PIN_CODE')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_pin_codeErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_website_url">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_WEBSITE_URL')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_website_url']|@htmlentities%>" name="msd_website_url" id="msd_website_url" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_WEBSITE_URL')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_website_urlErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_store_logo">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_LOGO')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_store_logo']|@htmlentities%>" name="msd_store_logo" id="msd_store_logo" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_LOGO')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_store_logoErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_store_description">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_DESCRIPTION')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <textarea placeholder=""  name="msd_store_description" id="msd_store_description" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_DESCRIPTION')%>"  class='elastic frm-size-medium'  ><%$data['msd_store_description']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_store_descriptionErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_other_proof_detail">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_OTHER_PROOF_DETAIL')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_other_proof_detail']|@htmlentities%>" name="msd_other_proof_detail" id="msd_other_proof_detail" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_OTHER_PROOF_DETAIL')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_other_proof_detailErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_bank_account_proof">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_PROOF')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_bank_account_proof']|@htmlentities%>" name="msd_bank_account_proof" id="msd_bank_account_proof" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_PROOF')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_bank_account_proofErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_g_s_t_i_r_d_proof">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_G_S_T_I_R_D_PROOF')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_g_s_t_i_r_d_proof']|@htmlentities%>" name="msd_g_s_t_i_r_d_proof" id="msd_g_s_t_i_r_d_proof" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_G_S_T_I_R_D_PROOF')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_g_s_t_i_r_d_proofErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_g_s_t_no">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_G_S_T_NO')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_g_s_t_no']|@htmlentities%>" name="msd_g_s_t_no" id="msd_g_s_t_no" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_G_S_T_NO')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_g_s_t_noErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_bank_account_no">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_NO')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_bank_account_no']|@htmlentities%>" name="msd_bank_account_no" id="msd_bank_account_no" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_NO')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_bank_account_noErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_bank_account_name">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_NAME')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_bank_account_name']|@htmlentities%>" name="msd_bank_account_name" id="msd_bank_account_name" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_NAME')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_bank_account_nameErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_store_url">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_URL')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_store_url']|@htmlentities%>" name="msd_store_url" id="msd_store_url" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_URL')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_store_urlErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_have_online_store">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_HAVE_ONLINE_STORE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['msd_have_online_store']%>
                                        <%$this->dropdown->display("msd_have_online_store","msd_have_online_store","  title='<%$this->lang->line('DASHBOARDSELLERSSTATE_HAVE_ONLINE_STORE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DASHBOARDSELLERSSTATE_HAVE_ONLINE_STORE')%>'  ", "|||", "", $opt_selected,"msd_have_online_store")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_have_online_storeErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_mst_categories_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_MST_CATEGORIES_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['msd_mst_categories_id']%>
                                        <%$this->dropdown->display("msd_mst_categories_id","msd_mst_categories_id","  title='<%$this->lang->line('DASHBOARDSELLERSSTATE_MST_CATEGORIES_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DASHBOARDSELLERSSTATE_MST_CATEGORIES_ID')%>'  ", "|||", "", $opt_selected,"msd_mst_categories_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_mst_categories_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_other_proof">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_OTHER_PROOF')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_other_proof']|@htmlentities%>" name="msd_other_proof" id="msd_other_proof" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_OTHER_PROOF')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_other_proofErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_store_rate_avg">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_RATE_AVG')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['msd_store_rate_avg']|@htmlentities%>" name="msd_store_rate_avg" id="msd_store_rate_avg" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_RATE_AVG')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_store_rate_avgErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_store_total_review">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_TOTAL_REVIEW')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['msd_store_total_review']%>
                                        <%$this->dropdown->display("msd_store_total_review","msd_store_total_review","  title='<%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_TOTAL_REVIEW')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DASHBOARDSELLERSSTATE_STORE_TOTAL_REVIEW')%>'  ", "|||", "", $opt_selected,"msd_store_total_review")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_store_total_reviewErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_accept_term">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_ACCEPT_TERM')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['msd_accept_term']%>
                                        <%$this->dropdown->display("msd_accept_term","msd_accept_term","  title='<%$this->lang->line('DASHBOARDSELLERSSTATE_ACCEPT_TERM')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DASHBOARDSELLERSSTATE_ACCEPT_TERM')%>'  ", "|||", "", $opt_selected,"msd_accept_term")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_accept_termErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_status">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_STATUS')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['msd_status']%>
                                        <%$this->dropdown->display("msd_status","msd_status","  title='<%$this->lang->line('DASHBOARDSELLERSSTATE_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'DASHBOARDSELLERSSTATE_STATUS')%>'  ", "|||", "", $opt_selected,"msd_status")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_statusErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_date">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_DATE')%>
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend   ">
                                        <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['msd_date'])%>" placeholder="" name="msd_date" id="msd_date" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                        <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_dateErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_modify_date">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_MODIFY_DATE')%>
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend   ">
                                        <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['msd_modify_date'])%>" placeholder="" name="msd_modify_date" id="msd_modify_date" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_MODIFY_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                        <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_modify_dateErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_about_store">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_ABOUT_STORE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <textarea placeholder=""  name="msd_about_store" id="msd_about_store" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_ABOUT_STORE')%>"  class='elastic frm-size-medium'  ><%$data['msd_about_store']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_about_storeErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_msd_return_policy">
                                    <label class="form-label span3">
                                        <%$this->lang->line('DASHBOARDSELLERSSTATE_RETURN_POLICY')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <textarea placeholder=""  name="msd_return_policy" id="msd_return_policy" title="<%$this->lang->line('DASHBOARDSELLERSSTATE_RETURN_POLICY')%>"  class='elastic frm-size-medium'  ><%$data['msd_return_policy']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='msd_return_policyErr'></label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%>">
                        <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                            <%assign var='rm_ctrl_directions' value=true%>
                        <%/if%>
                        <!-- Form Redirection Control Unit -->
                        <%if $controls_allow eq false || $rm_ctrl_directions eq true%>
                            <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" type="hidden" />
                        <%else%>
                            <div class='action-dir-align'>
                                <%if $prev_link_allow eq true%>
                                    <input value="Prev" id="ctrl_flow_prev" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Prev' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_prev">&nbsp;</label>
                                    <label for="ctrl_flow_prev" class="inline-elem-margin"><%$this->lang->line('GENERIC_PREV_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <%if $next_link_allow eq true || $mode eq 'Add'%>
                                    <input value="Next" id="ctrl_flow_next" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Next' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_next">&nbsp;</label>
                                    <label for="ctrl_flow_next" class="inline-elem-margin"><%$this->lang->line('GENERIC_NEXT_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <input value="List" id="ctrl_flow_list" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'List' %> checked=true <%/if%> />
                                <label for="ctrl_flow_list">&nbsp;</label>
                                <label for="ctrl_flow_list" class="inline-elem-margin"><%$this->lang->line('GENERIC_LIST_SHORT')%></label>&nbsp;&nbsp;
                                <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq '' || $ctrl_flow eq 'Stay' %> checked=true <%/if%> />
                                <label for="ctrl_flow_stay">&nbsp;</label>
                                <label for="ctrl_flow_stay" class="inline-elem-margin"><%$this->lang->line('GENERIC_STAY_SHORT')%></label>
                            </div>
                        <%/if%>
                        <!-- Form Action Control Unit -->
                        <%if $controls_allow eq false%>
                            <div class="clear">&nbsp;</div>
                        <%/if%>
                        <div class="action-btn-align" id="action_btn_container">
                            <%if $mode eq 'Update'%>
                                <%if $update_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_UPDATE')%>" name="ctrlupdate" type="submit" id="frmbtn_update" class="btn btn-info"/>&nbsp;&nbsp;
                                <%/if%>
                                <%if $delete_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_DELETE')%>" name="ctrldelete" type="button" id="frmbtn_delete" class="btn btn-danger" onclick="return deleteAdminRecordData('<%$enc_id%>', '<%$mod_enc_url.index%>','<%$mod_enc_url.inline_edit_action%>', '<%$extra_qstr%>', '<%$extra_hstr%>');" />&nbsp;&nbsp;
                                <%/if%>
                            <%else%>
                                <input value="<%$this->lang->line('GENERIC_SAVE')%>" name="ctrladd" type="submit" id="frmbtn_add" class="btn btn-info" />&nbsp;&nbsp;
                            <%/if%>
                            <%if $discard_allow eq true%>
                                <input value="<%$this->lang->line('GENERIC_DISCARD')%>" name="ctrldiscard" type="button" id="frmbtn_discard" class="btn" onclick="return loadAdminModuleListing('<%$mod_enc_url.index%>', '<%$extra_hstr%>')" />
                            <%/if%>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </form>
        </div>
    </div>
</div>
<%javascript%>    
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};        
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        <%foreach name=i from=$auto_arr item=v key=k%>
            if($("#<%$k%>").is("select")){
                $("#<%$k%>").ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                    },{
                    loadingImg: admin_image_url+"chosen-loading.gif"
                });
            }
        <%/foreach%>
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['buttons_arr'] = [];
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/dashboardsellersstate_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.dashboardsellersstate.callEvents();
<%/javascript%>