<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('DASHBOARDSELLERSSTATE_DASHBOARD_SELLERS_STATE')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','DASHBOARDSELLERSSTATE_DASHBOARD_SELLERS_STATE')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="dashboardsellersstate" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="dashboardsellersstate" class="frm-view-block frm-stand-view">
            <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
            <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
            <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
            <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
            <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
            <div class="main-content-block" id="main_content_block">
                <div style="width:98%;" class="frm-block-layout pad-calc-container">
                    <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                        <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('DASHBOARDSELLERSSTATE_DASHBOARD_SELLERS_STATE')%></h4></div>
                        <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                            <div class="form-row row-fluid" id="cc_sh_msd_store_code">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_CODE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_store_code']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_admin_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_ADMIN_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['msd_admin_id'], $opt_arr['msd_admin_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_store_name">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_NAME')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_store_name']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_company_name">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_COMPANY_NAME')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_company_name']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_contact_name">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_CONTACT_NAME')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_contact_name']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_state_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_STATE_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['msd_state_id'], $opt_arr['msd_state_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_city_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_CITY_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['msd_city_id'], $opt_arr['msd_city_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_area">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_AREA')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_area']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_address2">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_ADDRESS2')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_address2']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_address1">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_ADDRESS1')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_address1']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_contact_number">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_CONTACT_NUMBER')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_contact_number']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_country_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_COUNTRY_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['msd_country_id'], $opt_arr['msd_country_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_pin_code">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_PIN_CODE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_pin_code']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_website_url">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_WEBSITE_URL')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_website_url']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_store_logo">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_LOGO')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_store_logo']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_store_description">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_DESCRIPTION')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_store_description']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_other_proof_detail">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_OTHER_PROOF_DETAIL')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_other_proof_detail']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_bank_account_proof">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_PROOF')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_bank_account_proof']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_g_s_t_i_r_d_proof">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_G_S_T_I_R_D_PROOF')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_g_s_t_i_r_d_proof']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_g_s_t_no">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_G_S_T_NO')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_g_s_t_no']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_bank_account_no">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_NO')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_bank_account_no']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_bank_account_name">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_BANK_ACCOUNT_NAME')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_bank_account_name']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_store_url">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_URL')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_store_url']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_have_online_store">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_HAVE_ONLINE_STORE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['msd_have_online_store'], $opt_arr['msd_have_online_store'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_mst_categories_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_MST_CATEGORIES_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['msd_mst_categories_id'], $opt_arr['msd_mst_categories_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_other_proof">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_OTHER_PROOF')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_other_proof']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_store_rate_avg">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_RATE_AVG')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_store_rate_avg']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_store_total_review">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_STORE_TOTAL_REVIEW')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['msd_store_total_review'], $opt_arr['msd_store_total_review'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_accept_term">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_ACCEPT_TERM')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['msd_accept_term'], $opt_arr['msd_accept_term'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_status">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_STATUS')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['msd_status'], $opt_arr['msd_status'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_date">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_DATE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
                                    <strong><%$this->general->dateDefinedFormat('Y-m-d',$data['msd_date'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_modify_date">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_MODIFY_DATE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
                                    <strong><%$this->general->dateDefinedFormat('Y-m-d',$data['msd_modify_date'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_about_store">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_ABOUT_STORE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_about_store']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_msd_return_policy">
                                <label class="form-label span3">
                                    <%$this->lang->line('DASHBOARDSELLERSSTATE_RETURN_POLICY')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['msd_return_policy']%></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>        
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
    callSwitchToSelf();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
