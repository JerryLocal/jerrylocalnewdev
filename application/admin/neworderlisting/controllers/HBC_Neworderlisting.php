<?php
            
/**
 * Description of New Order Listing Extended Controller
 * 
 * @module Extended New Order Listing
 * 
 * @class HBC_Neworderlisting.php
 * 
 * @path application\admin\neworderlisting\controllers\HBC_Neworderlisting.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 05.12.2015
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
 
Class HBC_Neworderlisting extends Neworderlisting {
        function __construct() {
    parent::__construct();
    $this->load->model("model_neworderlisting");
}

function shipping_add() {
    $render_arr = $this->input->get();
}

function delivery_add() {
    $render_arr = $this->input->get();
    $id = intval($render_arr['id']);
    if ($id > 0) {
        $fields = "DATE_FORMAT(dShippingDate,'%d/%m/%Y') as dShippingDate";
        $subOrderArr = $this->model_neworderlisting->getSubOrderDetails($id, $fields);
        $render_arr['shipping_date'] = $subOrderArr[0]['dShippingDate'];
    }
    //echo $this->parser->parse("neworderlisting_hbc_delivery_popup", $render_arr, true);
    $this->loadView("neworderlisting_hbc_delivery_popup", $render_arr);
    //  $this->skip_template_view();
}

function delivery_add_action() {
    $postArr = $this->input->post();
    $sub_order_id = intval($postArr['sub_order_id']);
    if ($sub_order_id > 0) {
        $where = "iMstSubOrderId = '" . $sub_order_id . "' ";
        $new_date_arr = @explode("/", $postArr['mso_delivered_date']);
        $new_date = $new_date_arr[2] . "-" . $new_date_arr[1] . "-" . $new_date_arr[0];
        $delivered_date = date("Y-m-d", strtotime($new_date));
        $update_order_arr = array();
        $update_order_arr['dDeliveredDate'] = $delivered_date;
        $update_order_arr['eItemStatus'] = 'Delivered';
        $res = $this->model_neworderlisting->update_order_details($update_order_arr, $where);
        $this->session->set_flashdata('success', 'Delivery status changed successfully');
    } else {
        $this->session->set_flashdata('failure', 'Failure occured while changing the status to delivered');
    }
    $admin_url = $this->config->item("admin_url") . "shippedorderlisting/shippedorderlisting/index";
    redirect($admin_url);
}
}
