<?php

/**
 * Description of New Order Listing Extended Model
 * 
 * @module Extended New Order Listing
 * 
 * @class HBC_Model_neworderlisting.php
 * 
 * @path application\admin\neworderlisting\models\HBC_Model_neworderlisting.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 23.11.2015
 */
   
Class HBC_Model_neworderlisting extends Model_neworderlisting {
        function __construct() {
    parent::__construct();
}

function update_order_details($data=array(),$condition=''){
	$this->db->where($condition);
	$res=$this->db->update("mst_sub_order",$data);
	return $res;
}

function getSubOrderDetails($extracond = "", $field = "*", $orderby = "", $limit = "", $join_arr = '') {
    if ($field == "") {
        $field = "*";
	}
    $this->db->select($field, false);
    $this->db->from("mst_sub_order as msb");
    $this->db->join('mst_order as mo', 'mo.iMstOrderId = msb.iMstOrderId', 'join');
    if (is_array($join_arr) && count($join_arr) > 0) {
        foreach ($join_arr as $key => $val) {
            $this->db->join($val['table'], $val['condition'], $val['jointype']);
        }
    }
    if ($extracond != "") {
        if (intval($extracond)) {
            $this->db->where("msb.iMstSubOrderId", $extracond);
        } else {
            $this->db->where($extracond);
        }
    }
    if ($orderby != "") {
        $this->db->order_by($orderby);
    }
    if ($limit != "") {
        list($offset, $limit) = @explode(",", $limit);
        $this->db->limit($offset, $limit);
    }
    $list_data = $this->db->get()->result_array();
    #   echo $this->db->last_query();
    return $list_data;
}
}
