<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('NEWORDERLISTING_NEW_ORDER_LISTING')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','NEWORDERLISTING_NEW_ORDER_LISTING')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="neworderlisting" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="neworderlisting" class="frm-view-block frm-thclm-view">
            <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
            <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
            <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
            <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
            <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
            <input type="hidden" name="sys_custom_field_4" id="sys_custom_field_4" value="<%$data['sys_custom_field_4']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_mst_order_id" id="mso_mst_order_id" value="<%$data['mso_mst_order_id']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_product_sku" id="mso_product_sku" value="<%$data['mso_product_sku']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mo_mst_order_id" id="mo_mst_order_id" value="<%$data['mo_mst_order_id']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_mst_products_id" id="mso_mst_products_id" value="<%$data['mso_mst_products_id']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_canel_by" id="mso_canel_by" value="<%$data['mso_canel_by']%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_return_period" id="mso_return_period" value="<%$data['mso_return_period']%>"  class='ignore-valid ' />
            <textarea style="display:none;" name="mso_extra_info" id="mso_extra_info"  class='ignore-valid ' ><%$data['mso_extra_info']%></textarea>
            <textarea style="display:none;" name="mso_product_option" id="mso_product_option"  class='ignore-valid ' ><%$data['mso_product_option']%></textarea>
            <textarea style="display:none;" name="mso_cancel_detail" id="mso_cancel_detail"  class='ignore-valid ' ><%$data['mso_cancel_detail']%></textarea>
            <input type="hidden" name="mso_cancel_date" id="mso_cancel_date" value="<%$data['mso_cancel_date']%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
            <input type="hidden" name="mso_cancel_user_id" id="mso_cancel_user_id" value="<%$data['mso_cancel_user_id']%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_gateway_tdr" id="mso_gateway_tdr" value="<%$data['mso_gateway_tdr']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_market_place_fee" id="mso_market_place_fee" value="<%$data['mso_market_place_fee']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_admin_commision_per" id="mso_admin_commision_per" value="<%$data['mso_admin_commision_per']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_admin_commision" id="mso_admin_commision" value="<%$data['mso_admin_commision']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_gateway_tdrper" id="mso_gateway_tdrper" value="<%$data['mso_gateway_tdrper']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_product_regular_price" id="mso_product_regular_price" value="<%$data['mso_product_regular_price']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_product_sale_price" id="mso_product_sale_price" value="<%$data['mso_product_sale_price']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_delivered_date" id="mso_delivered_date" value="<%$data['mso_delivered_date']%>"  class='ignore-valid '  aria-date-format='dd/mm/yy'  aria-format-type='date' />
            <input type="hidden" name="mso_shipper_name" id="mso_shipper_name" value="<%$data['mso_shipper_name']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_item_status" id="mso_item_status" value="<%$data['mso_item_status']%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_action_taken_by" id="mso_action_taken_by" value="<%$data['mso_action_taken_by']%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_product_qty" id="mso_product_qty" value="<%$data['mso_product_qty']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_product_price" id="mso_product_price" value="<%$data['mso_product_price']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_settlement" id="mso_settlement" value="<%$data['mso_settlement']%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_trn_settlement_id" id="mso_trn_settlement_id" value="<%$data['mso_trn_settlement_id']%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_mst_store_detail_id" id="mso_mst_store_detail_id" value="<%$data['mso_mst_store_detail_id']%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_total_cost" id="mso_total_cost" value="<%$data['mso_total_cost']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_shipping_cost" id="mso_shipping_cost" value="<%$data['mso_shipping_cost']|@htmlentities%>"  class='ignore-valid ' />
            <input type="hidden" name="mso_sub_order_number" id="mso_sub_order_number" value="<%$data['mso_sub_order_number']|@htmlentities%>"  readonly='true'  class='ignore-valid ' />
            <input type="hidden" name="mso_sub_order_number_pre" id="mso_sub_order_number_pre" value="<%$data['mso_sub_order_number_pre']|@htmlentities%>"  readonly='true'  class='ignore-valid ' />
            <div class="main-content-block" id="main_content_block">
                <div style="width:98%;" class="frm-block-layout pad-calc-container">
                    <div class="box gradient <%$rl_theme_arr['frm_twclm_content_row']%> <%$rl_theme_arr['frm_twclm_border_view']%>">
                        <div class="title <%$rl_theme_arr['frm_twclm_titles_bar']%>"><h4><%$this->lang->line('NEWORDERLISTING_READY_TO_SHIP')%></h4></div>
                        <div class="content two-column-block <%$rl_theme_arr['frm_twclm_label_align']%>">
                            <div class="column-view-parent form-row row-fluid">
                                <div class="two-block-view" id="cc_sh_mso_product_name"> 
                                    <label class="form-label span3"><%$this->lang->line('NEWORDERLISTING_PRODUCT_NAME')%></label>
                                    <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> " style="width:60%!important">
                                        <strong><%$data['mso_product_name']%></strong>
                                    </div>
                                </div>
                                <div class="two-block-view" id="cc_sh_mso_seller_invoice_no"> 
                                    <label class="form-label span3"><%$this->lang->line('NEWORDERLISTING_INVOICE_NO_C46')%> <em>*</em>
                                </label>
                                <div class="form-right-div frm-elements-div  " style="width:60%!important">
                                    <strong><%$data['mso_seller_invoice_no']%></strong>
                                </div>
                            </div> 
                        </div>
                        <div class="column-view-parent form-row row-fluid">
                            <div class="two-block-view" id="cc_sh_mso_mst_shipper_id"> 
                                <label class="form-label span3"><%$this->lang->line('NEWORDERLISTING_COURIER_COMPANY')%> <em>*</em>
                            </label>
                            <div class="form-right-div frm-elements-div  " style="width:60%!important">
                                <strong><%$this->general->displayKeyValueData($data['mso_mst_shipper_id'], $opt_arr['mso_mst_shipper_id'])%></strong>
                            </div>
                        </div>
                        <div class="two-block-view" id="cc_sh_mso_seller_invoice_date"> 
                            <label class="form-label span3"><%$this->lang->line('NEWORDERLISTING_INVOICE_DATE')%> <em>*</em>
                        </label>
                        <div class="form-right-div frm-elements-div  input-append text-append-prepend  " style="width:60%!important">
                            <strong><%$this->general->dateSystemFormat($data['mso_seller_invoice_date'])%></strong>
                        </div>
                    </div> 
                </div>
                <div class="column-view-parent form-row row-fluid">
                    <div class="two-block-view" id="cc_sh_mso_tracking_number"> 
                        <label class="form-label span3"><%$this->lang->line('NEWORDERLISTING_TRACKING_NUMBER')%> <em>*</em>
                    </label>
                    <div class="form-right-div frm-elements-div  " style="width:60%!important">
                        <strong><%$data['mso_tracking_number']%></strong>
                    </div>
                </div>
                <div class="two-block-view" id="cc_sh_mso_shipping_remark"> 
                    <label class="form-label span3"><%$this->lang->line('NEWORDERLISTING_REMARKS')%></label>
                    <div class="form-right-div frm-elements-div  " style="width:60%!important">
                        <strong><%$data['mso_shipping_remark']%></strong>
                    </div>
                </div> 
            </div>
            <div class="column-view-parent form-row row-fluid">
                <div class="two-block-view" id="cc_sh_mso_shipping_date"> 
                    <label class="form-label span3"><%$this->lang->line('NEWORDERLISTING_SHIPPED_DATE')%> <em>*</em>
                </label>
                <div class="form-right-div frm-elements-div  input-append text-append-prepend  " style="width:60%!important">
                    <strong><%$this->general->dateSystemFormat($data['mso_shipping_date'])%></strong>
                </div>
            </div>
            <div class="two-block-view tab-focus-element">&nbsp;</div> 
        </div>
        <div class="column-view-parent form-row row-fluid">
            <div class="two-block-view" id="cc_sh_order_added_date"> 
                <label class="form-label span3"><%$this->lang->line('NEWORDERLISTING_CUSTOM_FIELD__C45_1')%></label>
                <div class="form-right-div frm-elements-div  " style="width:60%!important">
                    <strong><%$data['order_added_date']%></strong>
                </div>
            </div>
            <div class="two-block-view tab-focus-element">&nbsp;</div> 
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>        
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js("admin/custom/new_order_add_extended.js")%><%$this->css->add_css("custom/new_order_extended_add.css")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
