<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('NEWORDERLISTING_NEW_ORDER_LISTING')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','NEWORDERLISTING_NEW_ORDER_LISTING')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="neworderlisting" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="neworderlisting" class="frm-elem-block frm-thclm-view">
            <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                <input type="hidden" name="sys_custom_field_4" id="sys_custom_field_4" value="<%$data['sys_custom_field_4']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_mst_order_id" id="mso_mst_order_id" value="<%$data['mso_mst_order_id']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_product_sku" id="mso_product_sku" value="<%$data['mso_product_sku']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mo_mst_order_id" id="mo_mst_order_id" value="<%$data['mo_mst_order_id']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_mst_products_id" id="mso_mst_products_id" value="<%$data['mso_mst_products_id']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_canel_by" id="mso_canel_by" value="<%$data['mso_canel_by']%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_return_period" id="mso_return_period" value="<%$data['mso_return_period']%>"  class='ignore-valid ' />
                <textarea style="display:none;" name="mso_extra_info" id="mso_extra_info"  class='ignore-valid ' ><%$data['mso_extra_info']%></textarea>
                <textarea style="display:none;" name="mso_product_option" id="mso_product_option"  class='ignore-valid ' ><%$data['mso_product_option']%></textarea>
                <textarea style="display:none;" name="mso_cancel_detail" id="mso_cancel_detail"  class='ignore-valid ' ><%$data['mso_cancel_detail']%></textarea>
                <input type="hidden" name="mso_cancel_date" id="mso_cancel_date" value="<%$data['mso_cancel_date']%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                <input type="hidden" name="mso_cancel_user_id" id="mso_cancel_user_id" value="<%$data['mso_cancel_user_id']%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_gateway_tdr" id="mso_gateway_tdr" value="<%$data['mso_gateway_tdr']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_market_place_fee" id="mso_market_place_fee" value="<%$data['mso_market_place_fee']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_admin_commision_per" id="mso_admin_commision_per" value="<%$data['mso_admin_commision_per']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_admin_commision" id="mso_admin_commision" value="<%$data['mso_admin_commision']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_gateway_tdrper" id="mso_gateway_tdrper" value="<%$data['mso_gateway_tdrper']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_product_regular_price" id="mso_product_regular_price" value="<%$data['mso_product_regular_price']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_product_sale_price" id="mso_product_sale_price" value="<%$data['mso_product_sale_price']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_delivered_date" id="mso_delivered_date" value="<%$data['mso_delivered_date']%>"  class='ignore-valid '  aria-date-format='dd/mm/yy'  aria-format-type='date' />
                <input type="hidden" name="mso_shipper_name" id="mso_shipper_name" value="<%$data['mso_shipper_name']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_item_status" id="mso_item_status" value="<%$data['mso_item_status']%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_action_taken_by" id="mso_action_taken_by" value="<%$data['mso_action_taken_by']%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_product_qty" id="mso_product_qty" value="<%$data['mso_product_qty']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_product_price" id="mso_product_price" value="<%$data['mso_product_price']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_settlement" id="mso_settlement" value="<%$data['mso_settlement']%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_trn_settlement_id" id="mso_trn_settlement_id" value="<%$data['mso_trn_settlement_id']%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_mst_store_detail_id" id="mso_mst_store_detail_id" value="<%$data['mso_mst_store_detail_id']%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_total_cost" id="mso_total_cost" value="<%$data['mso_total_cost']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_shipping_cost" id="mso_shipping_cost" value="<%$data['mso_shipping_cost']|@htmlentities%>"  class='ignore-valid ' />
                <input type="hidden" name="mso_sub_order_number" id="mso_sub_order_number" value="<%$data['mso_sub_order_number']|@htmlentities%>"  readonly='true'  class='ignore-valid ' />
                <input type="hidden" name="mso_sub_order_number_pre" id="mso_sub_order_number_pre" value="<%$data['mso_sub_order_number_pre']|@htmlentities%>"  readonly='true'  class='ignore-valid ' />
                <div class="main-content-block" id="main_content_block">
                    <div style="width:98%" class="frm-block-layout pad-calc-container">
                        <div class="box gradient <%$rl_theme_arr['frm_twclm_content_row']%> <%$rl_theme_arr['frm_twclm_border_view']%>">
                            <div class="title <%$rl_theme_arr['frm_twclm_titles_bar']%>"><h4><%$this->lang->line('NEWORDERLISTING_READY_TO_SHIP')%></h4></div>
                            <div class="content two-column-block tab-focus-parent <%$rl_theme_arr['frm_twclm_label_align']%>">
                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                    <%if $mode eq "Update"%>
                                        <div class="two-block-view tab-focus-element" id="cc_sh_mso_product_name"> 
                                            <label class="form-label span3">
                                                <%$this->lang->line('NEWORDERLISTING_PRODUCT_NAME')%>
                                            </label> 
                                            <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%>  ">
                                                <%if $mode eq "Update"%>
                                                    <input type="hidden" class="ignore-valid" name="mso_product_name" id="mso_product_name" value="<%$data['mso_product_name']%>" />
                                                    <strong><%if $data['mso_product_name'] neq "" %><%$data['mso_product_name']%><%else%>---<%/if%></strong>
                                                <%else%>
                                                    <input type="text" placeholder="" value="<%$data['mso_product_name']|@htmlentities%>" name="mso_product_name" id="mso_product_name" title="<%$this->lang->line('NEWORDERLISTING_PRODUCT_NAME')%>"  class='frm-size-full_width'  />
                                                <%/if%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='mso_product_nameErr'></label></div>
                                        </div>
                                    <%else%>
                                        <input type="hidden" class="ignore-valid" name="mso_product_name" id="mso_product_name" value="<%$data['mso_product_name']%>" />
                                    <%/if%><div class="two-block-view tab-focus-element" id="cc_sh_mso_seller_invoice_no"> 
                                        <label class="form-label span3">
                                            <%$this->lang->line('NEWORDERLISTING_INVOICE_NO_C46')%> <em>*</em> 
                                        </label> 
                                        <div class="form-right-div   ">
                                            <input type="text" placeholder="" value="<%$data['mso_seller_invoice_no']|@htmlentities%>" name="mso_seller_invoice_no" id="mso_seller_invoice_no" title="<%$this->lang->line('NEWORDERLISTING_INVOICE_NO_C46')%>"  class='frm-size-medium'  />
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='mso_seller_invoice_noErr'></label></div>
                                    </div>
                                </div>
                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                    <div class="two-block-view tab-focus-element" id="cc_sh_mso_mst_shipper_id"> 
                                        <label class="form-label span3">
                                            <%$this->lang->line('NEWORDERLISTING_COURIER_COMPANY')%> <em>*</em> 
                                        </label> 
                                        <div class="form-right-div   ">
                                            <%assign var="opt_selected" value=$data['mso_mst_shipper_id']%>
                                            <%$this->dropdown->display("mso_mst_shipper_id","mso_mst_shipper_id","  title='<%$this->lang->line('NEWORDERLISTING_COURIER_COMPANY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-large'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'NEWORDERLISTING_COURIER_COMPANY')%>'  ", "|||", "", $opt_selected,"mso_mst_shipper_id")%>
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='mso_mst_shipper_idErr'></label></div>
                                    </div>
                                    <div class="two-block-view tab-focus-element" id="cc_sh_mso_seller_invoice_date"> 
                                        <label class="form-label span3">
                                            <%$this->lang->line('NEWORDERLISTING_INVOICE_DATE')%> <em>*</em> 
                                        </label> 
                                        <div class="form-right-div  input-append text-append-prepend   ">
                                            <input type="text" value="<%$this->general->dateSystemFormat($data['mso_seller_invoice_date'])%>" placeholder="" name="mso_seller_invoice_date" id="mso_seller_invoice_date" title="<%$this->lang->line('NEWORDERLISTING_INVOICE_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date'  />
                                            <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='mso_seller_invoice_dateErr'></label></div>
                                    </div>
                                </div>
                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                    <div class="two-block-view tab-focus-element" id="cc_sh_mso_tracking_number"> 
                                        <label class="form-label span3">
                                            <%$this->lang->line('NEWORDERLISTING_TRACKING_NUMBER')%> <em>*</em> 
                                        </label> 
                                        <div class="form-right-div   ">
                                            <input type="text" placeholder="" value="<%$data['mso_tracking_number']|@htmlentities%>" name="mso_tracking_number" id="mso_tracking_number" title="<%$this->lang->line('NEWORDERLISTING_TRACKING_NUMBER')%>"  class='frm-size-large'  />
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='mso_tracking_numberErr'></label></div>
                                    </div>
                                    <div class="two-block-view tab-focus-element" id="cc_sh_mso_shipping_remark"> 
                                        <label class="form-label span3">
                                            <%$this->lang->line('NEWORDERLISTING_REMARKS')%>
                                        </label> 
                                        <div class="form-right-div   ">
                                            <textarea placeholder=""  name="mso_shipping_remark" id="mso_shipping_remark" title="<%$this->lang->line('NEWORDERLISTING_REMARKS')%>"  class='elastic frm-size-full_width'  ><%$data['mso_shipping_remark']%></textarea>
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='mso_shipping_remarkErr'></label></div>
                                    </div>
                                </div>
                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                    <div class="two-block-view tab-focus-element" id="cc_sh_mso_shipping_date"> 
                                        <label class="form-label span3">
                                            <%$this->lang->line('NEWORDERLISTING_SHIPPED_DATE')%> <em>*</em> 
                                        </label> 
                                        <div class="form-right-div  input-append text-append-prepend   ">
                                            <input type="text" value="<%$this->general->dateSystemFormat($data['mso_shipping_date'])%>" placeholder="" name="mso_shipping_date" id="mso_shipping_date" title="<%$this->lang->line('NEWORDERLISTING_SHIPPED_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-large'  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date'  />
                                            <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='mso_shipping_dateErr'></label></div>
                                    </div>
                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                </div>
                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                    <div class="two-block-view tab-focus-element" id="cc_sh_order_added_date"> 
                                        <label class="form-label span3">
                                            <%$this->lang->line('NEWORDERLISTING_CUSTOM_FIELD__C45_1')%>
                                        </label> 
                                        <div class="form-right-div   ">
                                            <input type="text" placeholder="" value="<%$data['order_added_date']|@htmlentities%>" name="order_added_date" id="order_added_date" title="<%$this->lang->line('NEWORDERLISTING_CUSTOM_FIELD__C45_1')%>"  class='frm-size-medium'  />
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='order_added_dateErr'></label></div>
                                    </div>
                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="frm-bot-btn <%$rl_theme_arr['frm_twclm_action_bar']%> <%$rl_theme_arr['frm_twclm_action_btn']%>">
                        <%if $rl_theme_arr['frm_twclm_ctrls_view'] eq 'No'%>
                            <%assign var='rm_ctrl_directions' value=true%>
                        <%/if%>
                        <!-- Form Redirection Control Unit -->
                        <%if $controls_allow eq false || $rm_ctrl_directions eq true%>
                            <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" type="hidden" />
                        <%else%>
                            <div class='action-dir-align'>
                                <%if $prev_link_allow eq true%>
                                    <input value="Prev" id="ctrl_flow_prev" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Prev' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_prev">&nbsp;</label>
                                    <label for="ctrl_flow_prev" class="inline-elem-margin"><%$this->lang->line('GENERIC_PREV_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <%if $next_link_allow eq true || $mode eq 'Add'%>
                                    <input value="Next" id="ctrl_flow_next" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Next' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_next">&nbsp;</label>
                                    <label for="ctrl_flow_next" class="inline-elem-margin"><%$this->lang->line('GENERIC_NEXT_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <input value="List" id="ctrl_flow_list" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'List' %> checked=true <%/if%> />
                                <label for="ctrl_flow_list">&nbsp;</label>
                                <label for="ctrl_flow_list" class="inline-elem-margin"><%$this->lang->line('GENERIC_LIST_SHORT')%></label>&nbsp;&nbsp;
                                <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq '' || $ctrl_flow eq 'Stay' %> checked=true <%/if%> />
                                <label for="ctrl_flow_stay">&nbsp;</label>
                                <label for="ctrl_flow_stay" class="inline-elem-margin"><%$this->lang->line('GENERIC_STAY_SHORT')%></label>
                            </div>
                        <%/if%>
                        <!-- Form Action Control Unit -->
                        <%if $controls_allow eq false%>
                            <div class="clear">&nbsp;</div>
                        <%/if%>
                        <div class="action-btn-align" id="action_btn_container">
                            <%if $mode eq 'Update'%>
                                <%if $update_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_UPDATE')%>" name="ctrlupdate" type="submit" id="frmbtn_update" class="btn btn-info"/>&nbsp;&nbsp;
                                <%/if%>
                                <%if $delete_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_DELETE')%>" name="ctrldelete" type="button" id="frmbtn_delete" class="btn btn-danger" onclick="return deleteAdminRecordData('<%$enc_id%>', '<%$mod_enc_url.index%>','<%$mod_enc_url.inline_edit_action%>', '<%$extra_qstr%>', '<%$extra_hstr%>');" />&nbsp;&nbsp;
                                <%/if%>
                            <%else%>
                                <input value="<%$this->lang->line('GENERIC_SAVE')%>" name="ctrladd" type="submit" id="frmbtn_add" class="btn btn-info" />&nbsp;&nbsp;
                            <%/if%>
                            <%if $discard_allow eq true%>
                                <input value="<%$this->lang->line('GENERIC_DISCARD')%>" name="ctrldiscard" type="button" id="frmbtn_discard" class="btn" onclick="return loadAdminModuleListing('<%$mod_enc_url.index%>', '<%$extra_hstr%>')" />
                            <%/if%>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </form>
        </div>
    </div>
</div>
<%javascript%>    
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};        
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        <%foreach name=i from=$auto_arr item=v key=k%>
            if($("#<%$k%>").is("select")){
                $("#<%$k%>").ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                    },{
                    loadingImg: admin_image_url+"chosen-loading.gif"
                });
            }
        <%/foreach%>
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['buttons_arr'] = [];
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/neworderlisting_add_js.js')%>
<%$this->js->add_js("admin/custom/new_order_add_extended.js")%><%$this->css->add_css("custom/new_order_extended_add.css")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.neworderlisting.callEvents();
<%/javascript%>