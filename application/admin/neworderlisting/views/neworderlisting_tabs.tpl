<ul class="nav nav-tabs">
    <li <%if $module_name eq "neworderlisting"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('NEWORDERLISTING_NEW_ORDER_LISTING')%>" 
            <%if $module_name eq "neworderlisting"%> 
                href="javascript://"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('neworderlisting/neworderlisting/add')%>|mode|<%$mod_enc_mode['Update']%>|id|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('NEWORDERLISTING_NEW_ORDER_LISTING')%>
        </a>
    </li>
    <li <%if $module_name eq "getorderexpandedview"%> class="active" <%/if%>>
        <a title="<%$this->lang->line('NEWORDERLISTING_GET_ORDER_EXPANDED_VIEW')%> <%$this->lang->line('GENERIC_LIST')%>" 
            <%if $module_name eq "getorderexpandedview"%> 
                href="javascript://"
            <%elseif $module_name eq "neworderlisting"%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('getorderexpandedview/getorderexpandedview/index')%>|parMod|<%$this->general->getAdminEncodeURL('neworderlisting')%>|parID|<%$this->general->getAdminEncodeURL($data['iMstSubOrderId'])%>"
            <%else%> 
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('getorderexpandedview/getorderexpandedview/index')%>|parMod|<%$this->general->getAdminEncodeURL('neworderlisting')%>|parID|<%$this->general->getAdminEncodeURL($parID)%>" 
            <%/if%>
            >
            <%$this->lang->line('NEWORDERLISTING_GET_ORDER_EXPANDED_VIEW')%>
        </a>
    </li>
</ul>            