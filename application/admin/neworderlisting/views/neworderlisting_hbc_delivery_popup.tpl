<%$this->js->add_js('admin/custom/delivery_popup_extended.js')%>
<%$this->js->js_src()%>
<%$this->css->add_css("admin/order_developer.css")%>
<%$this->css->css_src()%>	
    <div class="main_area" id="delivery_area">
    <h3 class="heading">Delivered</h3>
    <div class="issue_form">
        <div class="form">
            <form action="<%$admin_url%>neworderlisting/neworderlisting/delivery_add_action" method="post" id="frmDelivery" name="frmDelivery">
                <input type="hidden" name="sub_order_id" id="sub_order_id" value="<%$id%>">
                <div class="clear"></div>
                <div class="clear"></div>
                
                <div class="form-row row-fluid" id="cc_sh_mso_delivered_date">
                    <label class="form-label span3">
                        <%$this->lang->line('NEWORDERLISTING_DELIVERED_DATE')%> <em>*</em>
                    </label> 
                    <div class="form-right-div  input-append text-append-prepend   ">
                        <input type="text" value="<%$shipping_date%>" placeholder="" name="mso_delivered_date" id="mso_delivered_date" title="<%$this->lang->line('NEWORDERLISTING_DELIVERED_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='dd/mm/yy'  aria-format-type='date' shipping-date=<%$shipping_date%>/>
                        <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                    </div>
                    <div class="error-msg-form "><label class='error' id='mso_delivered_dateErr'></label></div>
                </div>
              
                <div class="clear"></div>
                <div class="clear"></div> 
                <div class="form_input submit">
                    <input class="cancel-btn" type="button" id="btnCancel" name="btnCancel" value="Cancel"/>
                    <input class="accept-btn" type="button" id="btnConfirm" name="btnConfirm" value="Confirm Delivery"/>
                    <div class="clear"></div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    Project.modules.delivery_area.init();
</script>
	
