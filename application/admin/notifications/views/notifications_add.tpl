<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('NOTIFICATIONS_NOTIFICATIONS')%>
                <%if $mode eq 'Update' && $recName neq ''%> :: <%$recName%> <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','NOTIFICATIONS_NOTIFICATIONS')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'><%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span></a>            
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                     <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'> <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%></a>            
                </div>
            <%/if%>
        </div>
        <span style="display: none;position:inherit;" id="ajax_lang_loader">
            <img src="<%$this->config->item('admin_images_url')%>loaders/circular/020.gif">
        </span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="notifications">
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout"></div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing">
        <div id="notifications">
            <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                <input type="hidden" id="id" name="id" value="<%$enc_id%>">
                <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>">
                <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>">
                <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>">
                <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>">
                <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>">

                <div class="main-content-block" id="main_content_block">
                    <div style="width:98%" class = "frm-block-layout" >
                        <div class="box gradient">
                            <div class="title"></div>
                            <div class="content">
                                <div class="form-row row-fluid" id="cc_sh_men_subject">
                                    <label class="form-label span3"><%$this->lang->line('NOTIFICATIONS_SUBJECT')%> <em>*</em> </label> 
                                    <div class="form-right-div  ">
                                        <input type="text" placeholder="" value="<%$data['men_subject']|@htmlentities%>" name="men_subject" id="men_subject" title="<%$this->lang->line('NOTIFICATIONS_SUBJECT')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='men_subjectErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_men_content">
                                    <label class="form-label span3"><%$this->lang->line('NOTIFICATIONS_CONTENT')%> <em>*</em> </label> 
                                    <div class="form-right-div  ">
                                        <textarea placeholder=""  name="men_content" id="men_content" title="<%$this->lang->line('NOTIFICATIONS_CONTENT')%>"  class='elastic frm-size-medium'  ><%$data['men_content']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='men_contentErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_men_receiver">
                                    <label class="form-label span3"><%$this->lang->line('NOTIFICATIONS_RECEIVER')%> <em>*</em> </label> 
                                    <div class="form-right-div  ">
                                        <input type="text" placeholder="" value="<%$data['men_receiver']|@htmlentities%>" name="men_receiver" id="men_receiver" title="<%$this->lang->line('NOTIFICATIONS_RECEIVER')%>"  class='frm-size-medium'  />
                                   </div>
                                    <div class="error-msg-form "><label class='error' id='men_receiverErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_men_entity_id">
                                    <label class="form-label span3"><%$this->lang->line('NOTIFICATIONS_RECEIVER_NAME')%></label> 
                                    <div class="form-right-div  ">
                                        <%assign var="opt_selected" value=$data['men_entity_id']%>
                                        <%$this->dropdown->display("men_entity_id","men_entity_id","  title='<%$this->lang->line('NOTIFICATIONS_RECEIVER_NAME')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT_FIELD' ,'#FIELD#', 'NOTIFICATIONS_RECEIVER_NAME')%>'  ", "|||", "", $opt_selected,"men_entity_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='men_entity_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_men_group_id">
                                    <label class="form-label span3"><%$this->lang->line('NOTIFICATIONS_GROUP_NAME')%></label> 
                                    <div class="form-right-div  ">
                                        <%assign var="opt_selected" value=$data['men_group_id']%>
                                        <%$this->dropdown->display("men_group_id","men_group_id","  title='<%$this->lang->line('NOTIFICATIONS_GROUP_NAME')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT_FIELD' ,'#FIELD#', 'NOTIFICATIONS_GROUP_NAME')%>'  ", "|||", "", $opt_selected,"men_group_id")%>
                                   </div>
                                    <div class="error-msg-form "><label class='error' id='men_group_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_men_entity_type">
                                    <label class="form-label span3"><%$this->lang->line('NOTIFICATIONS_ENTITY_TYPE')%></label> 
                                    <div class="form-right-div  ">
                                        <%assign var="opt_selected" value=$data['men_entity_type']%>
                                        <%$this->dropdown->display("men_entity_type","men_entity_type","  title='<%$this->lang->line('NOTIFICATIONS_ENTITY_TYPE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT_FIELD' ,'#FIELD#', 'NOTIFICATIONS_ENTITY_TYPE')%>'  ", "|||", "", $opt_selected,"men_entity_type")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='men_entity_typeErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_men_redirect_link">
                                    <label class="form-label span3"><%$this->lang->line('NOTIFICATIONS_REDIRECT_LINK')%></label> 
                                    <div class="form-right-div  ">
                                        <textarea placeholder=""  name="men_redirect_link" id="men_redirect_link" title="<%$this->lang->line('NOTIFICATIONS_REDIRECT_LINK')%>"  class='elastic frm-size-medium'  ><%$data['men_redirect_link']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='men_redirect_linkErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_men_notification_type">
                                    <label class="form-label span3"><%$this->lang->line('NOTIFICATIONS_NOTIFICATION_TYPE')%> <em>*</em> </label> 
                                    <div class="form-right-div  ">
                                        <%assign var="opt_selected" value=$data['men_notification_type']%>
                                        <%$this->dropdown->display("men_notification_type","men_notification_type","  title='<%$this->lang->line('NOTIFICATIONS_NOTIFICATION_TYPE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT_FIELD' ,'#FIELD#', 'NOTIFICATIONS_NOTIFICATION_TYPE')%>'  ", "|||", "", $opt_selected,"men_notification_type")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='men_notification_typeErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_men_status">
                                    <label class="form-label span3"><%$this->lang->line('NOTIFICATIONS_STATUS')%> <em>*</em> </label> 
                                    <div class="form-right-div  ">
                                        <%assign var="opt_selected" value=$data['men_status']%>
                                        <%$this->dropdown->display("men_status","men_status","  title='<%$this->lang->line('NOTIFICATIONS_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT_FIELD' ,'#FIELD#', 'NOTIFICATIONS_STATUS')%>'  ", "|||", "", $opt_selected,"men_status")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='men_statusErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_men_t_send_date_time">
                                    <label class="form-label span3"><%$this->lang->line('NOTIFICATIONS_SEND_DATE_TIME')%></label> 
                                    <div class="form-right-div  ">
                                        <span >
                                            <input type="text" value="<%$this->general->dateTimeDefinedFormat('Y-m-d H:i:s',$data['men_t_send_date_time'])%>" name="men_t_send_date_time" placeholder=""  id="men_t_send_date_time" title="<%$this->lang->line('NOTIFICATIONS_SEND_DATE_TIME')%>"  class='frm-datepicker date-picker-icon frm-size-medium'  aria-date-format='yy-mm-dd'  aria-time-format='HH:mm:ss'  aria-format-type='datetime'  />
                                        </span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='men_t_send_date_timeErr'></label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <%include file="admin_form_direction.tpl"%>
                    <%include file="admin_form_control.tpl"%>
                </div>
            </form>
        </div>
    </div>
</div>
<%javascript%>    
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
     
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        <%foreach name=i from=$auto_arr item=v key=k%>
            if($("#<%$k%>").is("select")){
                $("#<%$k%>").ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: $get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                    },{
                    loadingImg: admin_image_url+"chosen-loading.gif"
                });
            }
        <%/foreach%>
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
<%/javascript%>
<%$this->js->add_js('admin/notifications_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.notifications.callEvents();
<%/javascript%>