<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="2"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="returnpolicy"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="returnpolicy" />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msp_return_policy">
        <label class="form-label span3">
            <%$this->lang->line('STORESTATICPAGE_RETURN_POLICY')%>
        </label> 
        <div class="form-right-div frm-elements-div  frm-editor-layout ">
            <strong><%$data['msp_return_policy']%></strong>
        </div>
    </div>
</div>