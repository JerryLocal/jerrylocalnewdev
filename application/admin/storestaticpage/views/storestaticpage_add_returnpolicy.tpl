<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="returnpolicy"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="returnpolicy" />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msp_return_policy">
        <label class="form-label span3">
            <%$this->lang->line('STORESTATICPAGE_RETURN_POLICY')%>
        </label> 
        <div class="form-right-div  frm-editor-layout  ">
            <textarea name="msp_return_policy" id="msp_return_policy" title="<%$this->lang->line('STORESTATICPAGE_RETURN_POLICY')%>"  style='width:80%;'  class='frm-size-medium frm-editor-large'  ><%$data['msp_return_policy']%></textarea>
        </div>
        <div class="error-msg-form "><label class='error' id='msp_return_policyErr'></label></div>
    </div>
</div>
<div align="right" class="btn-below-spacing">
    <div style="float:left;">
        <input type="button" value="Prev" name="ctrlprev_1_1_2" class="btn" onclick="return getLoadAdminTab('1','1','1','aboutstore')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Prev" name="ctrlsaveprev_1_1_2" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','1','aboutstore')"/>
    </div>
    <div style="float:right;">
        <input type="submit" value="Save" name="ctrlsave_1_1_2" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','2','returnpolicy')"/>
        &nbsp;&nbsp;
    </div>
    <div class="clear"></div>
</div>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initAdminTabRenderJSScript(eleObj){
Project.modules.storestaticpage.initEvents("returnpolicy");
callGoogleMapEvents();
}
<%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
<%foreach name=i from=$auto_arr item=v key=k%>
    if($("#<%$k%>").is("select")){
    $("#<%$k%>").ajaxChosen({
    dataType: "json",
    type: "POST",
    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
    },{
    loadingImg: admin_image_url+"chosen-loading.gif"
    });
}
<%/foreach%>
<%/if%>        
<%/javascript%>