<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="aboutstore"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="aboutstore" />
<input type="hidden" name="msp_mst_store_detail_id" id="msp_mst_store_detail_id" value="<%$data['msp_mst_store_detail_id']|@htmlentities%>"  class='ignore-valid ' />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msp_about_store">
        <label class="form-label span3">
            <%$this->lang->line('STORESTATICPAGE_ABOUT_STORE')%>
        </label> 
        <div class="form-right-div frm-elements-div  frm-editor-layout ">
            <strong><%$data['msp_about_store']%></strong>
        </div>
    </div>
</div>