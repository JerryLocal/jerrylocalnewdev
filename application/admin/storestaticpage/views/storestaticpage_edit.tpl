<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('STORESTATICPAGE_STORE_STATIC_PAGE')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','STORESTATICPAGE_STORE_STATIC_PAGE')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="storestaticpage" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="storestaticpage" class="frm-edit-block frm-custm-view">
            <div class="main-content-block" id="main_content_block">
                <div>
                    <div style="width:98%" class="frm-block-layout frm-resize-block pad-calc-container">
                        <div class="box gradient resize-box custom-view-box <%$rl_theme_arr['frm_custm_content_row']%> <%$rl_theme_arr['frm_custm_border_view']%>" id="blocktab_1_1">
                            <div class="title"></div>
                            <div class="content resize-content one-column-block <%$rl_theme_arr['frm_custm_label_align']%>">
                                <ul id="formtoptabs_1_1" class="nav nav-tabs show-hide-tab">
                                    <li id="headinglist_1_1_1" class="<%if $mode eq "Add"%>active<%else%>active<%/if%>">
                                        <a id="tabanchor_1_1_1" class="anchor" href="javascript://"  aria-curr="aboutstore" aria-prev="" aria-next="returnpolicy" isdone="<%if $mode eq "Update"%>1<%else%>0<%/if%>">
                                            <%$this->lang->line('STORESTATICPAGE_ABOUT_STORE')%>
                                        </a>
                                    </li>
                                    <li id="headinglist_1_1_2" class="<%if $mode eq "Add"%>active disabled<%else%><%/if%>">
                                        <a id="tabanchor_1_1_2" class="anchor" href="javascript://"  aria-curr="returnpolicy" aria-prev="aboutstore" aria-next="" isdone="<%if $mode eq "Update"%>1<%else%>0<%/if%>">
                                            <%$this->lang->line('STORESTATICPAGE_RETURN_POLICY')%>
                                        </a>
                                    </li>
                                </ul>
                                <div id="tabcontent_1_1" class="content-animate">
                                    <form name="frmaddupdate_1_1" id="frmaddupdate_1_1" action="<%$admin_url%><%$mod_enc_url['save_tab_wise_block']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                                        <div id="tabheading_1_1_1" class="tab-fade active tab-focus-child" style="display:block;">
                                            <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                                            <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                                            <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                                            <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records["prev"]["id"]%>" />
                                            <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records["next"]["id"]%>" />
                                            <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                                            <input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
                                            <input type="hidden" name="load_tab" id="load_tab_1_1" value="aboutstore"/>
                                            <input type="hidden" name="tab_code" id="tab_code_1_1" value="aboutstore" />
                                            <input type="hidden" name="msp_mst_store_detail_id" id="msp_mst_store_detail_id" value="<%$data['msp_mst_store_detail_id']|@htmlentities%>"  class='ignore-valid ' />
                                            <div class="column-view-parent form-row row-fluid tab-focus-element" >
                                                <div class="one-block-view" id="cc_sh_msp_about_store">
                                                    <label class="form-label span3"><%$this->lang->line('STORESTATICPAGE_ABOUT_STORE')%></label>
                                                    <div class="form-right-div  frm-editor-layout  ">
                                                        <textarea name="msp_about_store" id="msp_about_store" title="<%$this->lang->line('STORESTATICPAGE_ABOUT_STORE')%>"  style='width:80%;'  class='frm-size-medium frm-editor-large'  ><%$data['msp_about_store']%></textarea>
                                                    </div>
                                                    <div class="error-msg-form "><label class='error' id='msp_about_storeErr'></label></div>
                                                </div>
                                            </div>
                                            <div align="right" class="btn-below-spacing">
                                                <div style="float:right;">
                                                    <input type="submit" value="Save" name="ctrlsave_1_1_1" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','1','aboutstore')"/>
                                                    &nbsp;&nbsp;
                                                    <input type="submit" value="Save &amp; Next" name="ctrlsavenext_1_1_1" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','2','returnpolicy')" />
                                                    &nbsp;&nbsp;
                                                    <input type="button" value="Next" name="ctrlnext_1_1_1" class="btn" onclick="return getNextAdminTab('1','1','1','2','returnpolicy')" />
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <div id="tabheading_1_1_2" class="tab-fade inactive tab-focus-child" style="display:block;"></div>
                                    </form>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>            
<%javascript%>    
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};        
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        <%foreach name=i from=$auto_arr item=v key=k%>
            if($("#<%$k%>").is("select")){
                $("#<%$k%>").ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                    },{
                    loadingImg: admin_image_url+"chosen-loading.gif"
                });
            }
        <%/foreach%>
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_action_url'] = admin_url+'<%$mod_enc_url["save_tab_wise_block"]%>?<%$extra_qstr%>';
    el_form_settings['buttons_arr'] = [];
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/storestaticpage_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.storestaticpage.callEvents();
<%/javascript%>