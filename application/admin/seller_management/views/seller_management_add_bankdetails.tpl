<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="bankdetails"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="bankdetails" />
<input type="hidden" name="sys_custom_field_1" id="sys_custom_field_1" value="<%$data['sys_custom_field_1']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_g_s_t_i_r_d_proof" id="msd_g_s_t_i_r_d_proof" value="<%$data['msd_g_s_t_i_r_d_proof']%>"  class='ignore-valid ' />
<input type="hidden" name="msd_bank_account_proof" id="msd_bank_account_proof" value="<%$data['msd_bank_account_proof']%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_2" id="sys_custom_field_2" value="<%$data['sys_custom_field_2']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_other_proof" id="msd_other_proof" value="<%$data['msd_other_proof']%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_3" id="sys_custom_field_3" value="<%$data['sys_custom_field_3']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_accept_term" id="msd_accept_term" value="<%$data['msd_accept_term']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_4" id="sys_custom_field_4" value="<%$data['sys_custom_field_4']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_5" id="sys_custom_field_5" value="<%$data['sys_custom_field_5']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_6" id="sys_custom_field_6" value="<%$data['sys_custom_field_6']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="sys_custom_field_7" id="sys_custom_field_7" value="<%$data['sys_custom_field_7']|@htmlentities%>"  class='ignore-valid ' />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_name">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_MANAGEMENT_BANK_ACCOUNT_NAME')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_bank_account_name']|@htmlentities%>" name="msd_bank_account_name" id="msd_bank_account_name" title="<%$this->lang->line('SELLER_MANAGEMENT_BANK_ACCOUNT_NAME')%>"  class='frm-size-medium'  />
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_ENTER_VALID_NEW_ZEALAND_BANK_DETAIL_FOR_GETTING_PAID_BY_US_FOR_EACH_SUCCESSFUL_SALE_C46')%>"><span class="icomoon-icon-help"></span></a>
            </span>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_bank_account_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_bank_account_no">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_MANAGEMENT_BANK_ACCOUNT_NO')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" format="99-9999-9999999-999" value="<%$data['msd_bank_account_no']%>" name="msd_bank_account_no" id="msd_bank_account_no" title="<%$this->lang->line('SELLER_MANAGEMENT_BANK_ACCOUNT_NO')%>"  class='frm-phone-number frm-size-medium'  style='width:auto;' />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_bank_account_noErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_g_s_t_no">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_MANAGEMENT_G_S_T_NO')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" format="999-999-999" value="<%$data['msd_g_s_t_no']%>" name="msd_g_s_t_no" id="msd_g_s_t_no" title="<%$this->lang->line('SELLER_MANAGEMENT_G_S_T_NO')%>"  class='frm-phone-number frm-size-medium'  style='width:auto;' />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_g_s_t_noErr'></label></div>
    </div>
</div>

<!--
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_other_proof_detail">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_MANAGEMENT_OTHER_PROOF_DETAIL')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_other_proof_detail']|@htmlentities%>" name="msd_other_proof_detail" id="msd_other_proof_detail" title="<%$this->lang->line('SELLER_MANAGEMENT_OTHER_PROOF_DETAIL')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_other_proof_detailErr'></label></div>
    </div>
</div>
-->
<div align="right" class="btn-below-spacing">
    <div style="float:left;">
        <input type="button" value="Prev" name="ctrlprev_1_1_3" class="btn" onclick="return getLoadAdminTab('1','1','2','businessaccountdetails')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Prev" name="ctrlsaveprev_1_1_3" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','2','businessaccountdetails')"/>
    </div>
    <div style="float:right;">
        <input type="submit" value="Save" name="ctrlsave_1_1_3" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','3','bankdetails')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Next" name="ctrlsavenext_1_1_3" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','4','aboutstore')" />
        &nbsp;&nbsp;
        <input type="button" value="Next" name="ctrlnext_1_1_3" class="btn" onclick="return getNextAdminTab('1','1','3','4','aboutstore')" />
    </div>
    <div class="clear"></div>
</div>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initAdminTabRenderJSScript(eleObj){
Project.modules.seller_management.initEvents("bankdetails");
callGoogleMapEvents();
}
<%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
<%foreach name=i from=$auto_arr item=v key=k%>
    if($("#<%$k%>").is("select")){
    $("#<%$k%>").ajaxChosen({
    dataType: "json",
    type: "POST",
    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
    },{
    loadingImg: admin_image_url+"chosen-loading.gif"
    });
}
<%/foreach%>
<%/if%>        
<%/javascript%>