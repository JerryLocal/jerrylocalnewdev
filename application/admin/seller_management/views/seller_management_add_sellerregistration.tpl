<input type="hidden" id="id" name="id" value="<%$enc_id%>" />
<input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
<input type="hidden" name="tab_id" id="tab_id_1_1" value="1"/>
<input type="hidden" name="load_tab" id="load_tab_1_1" value="sellerregistration"/>
<input type="hidden" name="tab_code" id="tab_code_1_1" value="sellerregistration" />
<input type="hidden" name="ma_email" id="ma_email" value="<%$data['ma_email']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma_password" id="ma_password" value="<%$data['ma_password']%>"  class='ignore-valid ' />
<input type="hidden" name="msd_admin_id" id="msd_admin_id" value="<%$data['msd_admin_id']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_store_rate_avg" id="msd_store_rate_avg" value="<%$data['msd_store_rate_avg']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_store_total_review" id="msd_store_total_review" value="<%$data['msd_store_total_review']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_status" id="msd_status" value="<%$data['msd_status']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="msd_date" id="msd_date" value="<%$data['msd_date']%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
<input type="hidden" name="msd_modify_date" id="msd_modify_date" value="<%$data['msd_modify_date']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma_group_id" id="ma_group_id" value="<%$data['ma_group_id']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma_status" id="ma_status" value="<%$data['ma_status']|@htmlentities%>"  class='ignore-valid ' />
<input type="hidden" name="ma_last_access" id="ma_last_access" value="<%$data['ma_last_access']|@htmlentities%>"  class='ignore-valid ' />
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_name">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_MANAGEMENT_STORE_NAME')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_store_name']|@htmlentities%>" name="msd_store_name" id="msd_store_name" title="<%$this->lang->line('SELLER_MANAGEMENT_STORE_NAME')%>"  class='frm-size-medium'  />
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_ENTER_UNIQUE_STORE_NAME_THAT_YOUR_CUSTOMER_WILL_SEE_ON_OUR_WEBSITE')%>"><span class="icomoon-icon-help"></span></a>
            </span>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_nameErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_code">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_MANAGEMENT_STORE_CODE')%>
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['msd_store_code']|@htmlentities%>" name="msd_store_code" id="msd_store_code" title="<%$this->lang->line('SELLER_MANAGEMENT_STORE_CODE')%>"  class='frm-size-medium'  readonly='readonly'  />
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_codeErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_logo">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_MANAGEMENT_STORE_LOGO')%>
        </label> 
        <div class="form-right-div   ">
            <div  class='btn-uploadify frm-size-medium' >
                <input type="hidden" value="<%$data['msd_store_logo']%>" name="old_msd_store_logo" id="old_msd_store_logo" />
                <input type="hidden" value="<%$data['msd_store_logo']%>" name="msd_store_logo" id="msd_store_logo"  />
                <input type="hidden" value="<%$data['msd_store_logo']%>" name="temp_msd_store_logo" id="temp_msd_store_logo"  />
                <div id="upload_drop_zone_msd_store_logo" class="upload-drop-zone"></div>
                <div class="uploader upload-src-zone">
                    <input type="file" name="uploadify_msd_store_logo" id="uploadify_msd_store_logo" title="<%$this->lang->line('SELLER_MANAGEMENT_STORE_LOGO')%>" />
                    <span class="filename" id="preview_msd_store_logo"></span>
                    <span class="action">Choose File</span>
                </div>
            </div>
            <div class='upload-image-btn'>
                <%$img_html['msd_store_logo']%>
            </div>
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB.<%$this->lang->line('GENERIC_THIS_WILL_BE_DISPLAYED_ON_YOUR_MARKETPLACE_SHOULD_REPRESENT_YOUR_STORES_BRAND_C46_LOGO_WIDTH_180PX_AND_HEIGHT_IN_PROPORTION_TO_THIS_C46')%>"><span class="icomoon-icon-help"></span></a>
            </span>
            <div class='clear upload-progress' id='progress_msd_store_logo'>
                <div class='upload-progress-bar progress progress-striped active'>
                    <div class='bar' id='practive_msd_store_logo'></div>
                </div>
            </div>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_logoErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_msd_store_description">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_MANAGEMENT_STORE_DESCRIPTION')%>
        </label> 
        <div class="form-right-div  frm-editor-layout  ">
            <textarea name="msd_store_description" id="msd_store_description" title="<%$this->lang->line('SELLER_MANAGEMENT_STORE_DESCRIPTION')%>"  style='width:80%;'  class='frm-size-medium frm-editor-small'  ><%$data['msd_store_description']%></textarea>
            <span class="input-comment">
                <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_ENTER_SHORT_DESCRIPTION_ABOUT_YOUR_STORE_THAT_YOUR_CUSTOMER_WILL_SEE_ON_YOUR_STORE_FRONT_PAGE__C46_MAX_500_WORDS_C46')%>"><span class="icomoon-icon-help"></span></a>
            </span>
        </div>
        <div class="error-msg-form "><label class='error' id='msd_store_descriptionErr'></label></div>
    </div>
</div>
<div class="column-view-parent form-row row-fluid" >
    <div class="one-block-view" id="cc_sh_ma_user_name">
        <label class="form-label span3">
            <%$this->lang->line('SELLER_MANAGEMENT_EMAIL')%> <em>*</em> 
        </label> 
        <div class="form-right-div   ">
            <input type="text" placeholder="" value="<%$data['ma_user_name']|@htmlentities%>" name="ma_user_name" id="ma_user_name" title="<%$this->lang->line('SELLER_MANAGEMENT_EMAIL')%>"  class='frm-size-medium'  />
        </div>
        <div class="error-msg-form "><label class='error' id='ma_user_nameErr'></label></div>
    </div>
</div>
<div align="right" class="btn-below-spacing">
    <div style="float:right;">
        <input type="submit" value="Save" name="ctrlsave_1_1_1" class="btn btn-info" onclick="return getSaveAndLoadAdminTab('1','1','1','sellerregistration')"/>
        &nbsp;&nbsp;
        <input type="submit" value="Save &amp; Next" name="ctrlsavenext_1_1_1" class="btn" onclick="return getSaveAndLoadAdminTab('1','1','2','businessaccountdetails')" />
        &nbsp;&nbsp;
        <input type="button" value="Next" name="ctrlnext_1_1_1" class="btn" onclick="return getNextAdminTab('1','1','1','2','businessaccountdetails')" />
    </div>
    <div class="clear"></div>
</div>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initAdminTabRenderJSScript(eleObj){
Project.modules.seller_management.initEvents("sellerregistration");
callGoogleMapEvents();
}
<%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
<%foreach name=i from=$auto_arr item=v key=k%>
    if($("#<%$k%>").is("select")){
    $("#<%$k%>").ajaxChosen({
    dataType: "json",
    type: "POST",
    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
    },{
    loadingImg: admin_image_url+"chosen-loading.gif"
    });
}
<%/foreach%>
<%/if%>        
<%/javascript%>