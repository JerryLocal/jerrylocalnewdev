<%javascript%>
    $.fn.editable.defaults.mode = 'inline', $.fn.editable.defaults.clear = false;
    var el_subview_settings = {}, view_js_col_model_json = {}, view_token_input_assign = {}, view_token_pre_populates = {};
      
    el_subview_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_subview_settings['extra_qstr'] = '<%$extra_qstr%>';
                    
    el_subview_settings['view_id'] = '<%$subgrid_view_id%>';
    el_subview_settings['edit_id'] = '<%$enc_view_primary_id%>';
    el_subview_settings['module_name'] = '<%$module_name%>';
            
    el_subview_settings['edit_page_url'] = admin_url+'<%$mod_enc_url["inline_edit_action"]%>?&oper=edit&<%$extra_qstr%>';
    el_subview_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_subview_settings['permit_edit_btn'] = '<%$edit_access%>';

    view_js_col_model_json =  {};
    
    initSubGridDetailView();
<%/javascript%>
    
<div class="expand-detail-view">                        
    <table id="detail_view_block" class="jqgrid-subview" width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_PRODUCT_SKU_CODE')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_product_sku']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_NAME')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mo_buyer_name']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_PRODUCT_NAME')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_product_name']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_PHONE')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mo_buyer_phone']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_ORDER_ITEM_ID')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_mst_sub_order_id']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_EMAIL')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mo_buyer_email']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_ORDER_ID')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mo_mst_order_id']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_ADDRESS')%>: </strong></td>
            <td valign="top" width="27%"><%$data['custom_address']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_REASON')%>: </strong></td>
            <td valign="top" width="27%"><%$data['trr_reason']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_REQUEST_IMG')%>: </strong></td>
            <td valign="top" width="27%"><%$data['tr_request_img']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_REASON_COMMENT')%>: </strong></td>
            <td valign="top" width="27%"><%$data['trr_comment_to_customer']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_DELIVERY_DATE')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_delivered_date']%></td>
        </tr>
        <tr> 
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_SHIPPING_CONCESSION')%>: </strong></td>
            <td valign="top" width="27%"><%$data['trr_shipping']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_REFUND_SENT_DATE')%>: </strong></td>
            <td valign="top" width="27%"><%$data['trr_refund_date']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_ORDER_AMOUNT_FOR_REFUND')%>: </strong></td>
            <td valign="top" width="27%"><%$data['trr_refund']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_LOGISTIC_COMPANY')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_shipper_name']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_REFUND_TOTAL')%>: </strong></td>
            <td valign="top" width="27%"><%$data['trr_tot_amount']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETREFUNDEXPANDEDVIEW_TRACKING_NUMBER')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_tracking_number']%></td>
        </tr>
    </table>
</div>