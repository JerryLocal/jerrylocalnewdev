<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<textarea id="txt_module_help" class="perm-elem-hide"></textarea>
<div id="txt_summary_grid_id" class="perm-elem-hide">    
    <!-- Footer Summary -->
    <div>
        <!-- mso_total_cost summary -->
        <div>
            <div id="txt_esum_foot_mso_total_cost_list2">
                ${0}
            </div>
        </div>
        <!-- trr_total_amount summary -->
        <div>
            <div id="txt_esum_foot_trr_total_amount_list2">
                ${0}
            </div>
        </div>
        <!-- mso_gateway_t_d_r summary -->
        <div>
            <div id="txt_esum_foot_mso_gateway_t_d_r_list2">
                ${0}
            </div>
        </div>
        <!-- mso_admin_commision summary -->
        <div>
            <div id="txt_esum_foot_mso_admin_commision_list2">
                ${0}
            </div>
        </div>
        <!-- mso_admin_commision_per summary -->
        <div>
            <div id="txt_esum_foot_mso_admin_commision_per_list2">
                ${0}
            </div>
        </div>
    </div>
</div>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line('GENERIC_LISTING')%> :: Settlement
            </div>        
        </h3>
        <div class="header-right-drops">
        </div>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing box gradient">
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div id="scrollable_content" class="scrollable-content top-list-spacing">
        <div class="grid-data-container pad-calc-container">
            <div class="top-list-tab-layout" id="top_list_grid_layout">
            </div>
            <table class="grid-table-view " width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td id="grid_data_col">
                        <div id="pager2"></div>
                        <table id="list2"></table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>        
<input type="hidden" name="selAllRows" value="" id="selAllRows" />
<%javascript%>
    $.jgrid.no_legacy_api = true; $.jgrid.useJSON = true;
    var el_grid_settings = {}, js_col_model_json = {}, js_col_name_json = {}; 
                    
    el_grid_settings['module_name'] = '<%$module_name%>';
    el_grid_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_grid_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_grid_settings['enc_location'] = '<%$enc_loc_module%>';
    el_grid_settings['par_module '] = '<%$this->general->getAdminEncodeURL($parMod)%>';
    el_grid_settings['par_data'] = '<%$this->general->getAdminEncodeURL($parID)%>';
    el_grid_settings['par_field'] = '<%$parField%>';
    el_grid_settings['par_type'] = 'parent';
    el_grid_settings['add_page_url'] = '<%$mod_enc_url["add"]%>'; 
    el_grid_settings['edit_page_url'] =  admin_url+'<%$mod_enc_url["inline_edit_action"]%>?<%$extra_qstr%>';
    el_grid_settings['listing_url'] = admin_url+'<%$mod_enc_url["listing"]%>?<%$extra_qstr%>';
    el_grid_settings['search_refresh_url'] = admin_url+'<%$mod_enc_url["get_left_search_content"]%>?<%$extra_qstr%>';
    el_grid_settings['search_autocomp_url'] = admin_url+'<%$mod_enc_url["get_search_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['auto_complete_url'] = admin_url+'<%$mod_enc_url["get_token_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['export_url'] =  admin_url+'<%$mod_enc_url["export"]%>?<%$extra_qstr%>';
    el_grid_settings['subgrid_listing_url'] =  admin_url+'<%$mod_enc_url["get_subgrid_block"]%>?<%$extra_qstr%>';
    el_grid_settings['jparent_switchto_url'] = admin_url+'<%$parent_switch_cit["url"]%>?<%$extra_qstr%>';
    
    el_grid_settings['admin_rec_arr'] = {};
    el_grid_settings['status_arr'] = $.parseJSON('<%$status_array|@json_encode%>');
    el_grid_settings['status_lang_arr'] = $.parseJSON('<%$status_array_label|@json_encode%>');
                
    el_grid_settings['hide_add_btn'] = '';
    el_grid_settings['hide_del_btn'] = '';
    el_grid_settings['hide_status_btn'] = '';
    el_grid_settings['hide_export_btn'] = '1';
    el_grid_settings['hide_columns_btn'] = 'No';
    
    el_grid_settings['hide_advance_search'] = 'No';
    el_grid_settings['hide_search_tool'] = 'No';
    el_grid_settings['hide_multi_select'] = 'No';
    el_grid_settings['popup_add_form'] = 'No';
    el_grid_settings['popup_edit_form'] = 'No';
    el_grid_settings['popup_add_size'] = ['75%', '75%'];
    el_grid_settings['popup_edit_size'] = ['75%', '75%'];
    el_grid_settings['hide_paging_btn'] = 'No';
    el_grid_settings['hide_refresh_btn'] = 'No';
    el_grid_settings['group_search'] = '';
    
    el_grid_settings['permit_add_btn'] = '<%$add_access%>';
    el_grid_settings['permit_del_btn'] = '<%$del_access%>';
    el_grid_settings['permit_edit_btn'] = '<%$view_access%>';
    el_grid_settings['permit_expo_btn'] = '<%$expo_access%>';
    
    el_grid_settings['default_sort'] = 'msd_store_name';
    el_grid_settings['sort_order'] = 'asc';
    
    el_grid_settings['footer_row'] = 'Yes';
    el_grid_settings['grouping'] = 'No';
    el_grid_settings['group_attr'] = {};
    
    el_grid_settings['inline_add'] = 'No';
    el_grid_settings['rec_position'] = 'Top';
    el_grid_settings['auto_width'] = 'Yes';
    el_grid_settings['subgrid'] = 'No';
    el_grid_settings['colgrid'] = 'No';
    el_grid_settings['rating_allow'] = 'No';
    el_grid_settings['listview'] = 'list';
    el_grid_settings['filters_arr'] = $.parseJSON('<%$default_filters|@json_encode%>');
    el_grid_settings['top_filter'] = [];
    el_grid_settings['buttons_arr'] = [{
        "name": "add",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_ADD_NEW')%>",
        "title": "<%$this->lang->line('GENERIC_ADD_NEW')%>",
        "icon": "icomoon-icon-plus-2",
        "icon_only": "No"
    },
    {
        "name": "del",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_DELETE')%>",
        "title": "<%$this->lang->line('GENERIC_DELETE_SELECTED_ROW')%>",
        "icon": "icomoon-icon-remove-6",
        "icon_only": "No"
    },
    {
        "name": "status_pending",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_PENDING')%>",
        "title": "<%$this->lang->line('GENERIC_PENDING')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_in-process",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_IN_C45PROCESS')%>",
        "title": "<%$this->lang->line('GENERIC_IN_C45PROCESS')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_shipped",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_SHIPPED')%>",
        "title": "<%$this->lang->line('GENERIC_SHIPPED')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_delivered",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_DELIVERED')%>",
        "title": "<%$this->lang->line('GENERIC_DELIVERED')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_cancel",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_CANCEL')%>",
        "title": "<%$this->lang->line('GENERIC_CANCEL')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_close",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_CLOSE')%>",
        "title": "<%$this->lang->line('GENERIC_CLOSE')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "search",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_SEARCH')%>",
        "title": "<%$this->lang->line('GENERIC_ADVANCE_SEARCH')%>",
        "icon": "icomoon-icon-search-3",
        "icon_only": "No"
    },
    {
        "name": "refresh",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_SHOW_ALL')%>",
        "title": "<%$this->lang->line('GENERIC_SHOW_ALL_LISTING_RECORDS')%>",
        "icon": "icomoon-icon-loop-2",
        "icon_only": "No"
    },
    {
        "name": "columns",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_COLUMNS')%>",
        "title": "<%$this->lang->line('GENERIC_HIDE_C47SHOW_COLUMNS')%>",
        "icon": "silk-icon-columns",
        "icon_only": "No"
    },
    {
        "name": "export",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_EXPORT')%>",
        "title": "<%$this->lang->line('GENERIC_EXPORT')%>",
        "icon": "icomoon-icon-out",
        "icon_only": "No"
    }];
    
    js_col_name_json = [{
        "name": "msd_store_name",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_STORE')%>"
    },
    {
        "name": "sys_custom_field_17",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_ORDER_NO')%>"
    },
    {
        "name": "sys_custom_field_6",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_SUB_ORDER_NO')%>"
    },
    {
        "name": "tr_rquest_type",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_RETURN_STATUS')%>"
    },
    {
        "name": "trr_status",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_REFUND__STATUS')%>"
    },
    {
        "name": "mso_total_cost",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_ORDER_TOTAL')%>"
    },
    {
        "name": "trr_total_amount",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_REFUND_AMT')%>"
    },
    {
        "name": "mso_gateway_t_d_r",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_PAYPAL')%>"
    },
    {
        "name": "mso_admin_commision",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_COMMISION')%>"
    },
    {
        "name": "mso_admin_commision_per",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_SETTLED_AMT')%>"
    },
    {
        "name": "ts_date",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_SETTLED_DATE')%>"
    }];
    
    js_col_model_json = [{
        "name": "msd_store_name",
        "index": "msd_store_name",
        "label": "<%$list_config['msd_store_name']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['msd_store_name']['width']%>",
        "search": <%if $list_config['msd_store_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['msd_store_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['msd_store_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['msd_store_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['msd_store_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['msd_store_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['msd_store_name']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "sys_custom_field_17",
        "index": "sys_custom_field_17",
        "label": "<%$list_config['sys_custom_field_17']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['sys_custom_field_17']['width']%>",
        "search": <%if $list_config['sys_custom_field_17']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['sys_custom_field_17']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['sys_custom_field_17']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['sys_custom_field_17']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['sys_custom_field_17']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "sys_custom_field_17",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['sys_custom_field_17']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "sys_custom_field_17",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['sys_custom_field_17']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "sys_custom_field_6",
        "index": "sys_custom_field_6",
        "label": "<%$list_config['sys_custom_field_6']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['sys_custom_field_6']['width']%>",
        "search": <%if $list_config['sys_custom_field_6']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['sys_custom_field_6']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['sys_custom_field_6']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['sys_custom_field_6']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['sys_custom_field_6']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "sys_custom_field_6",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['sys_custom_field_6']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "sys_custom_field_6",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['sys_custom_field_6']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "tr_rquest_type",
        "index": "tr_rquest_type",
        "label": "<%$list_config['tr_rquest_type']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['tr_rquest_type']['width']%>",
        "search": <%if $list_config['tr_rquest_type']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['tr_rquest_type']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['tr_rquest_type']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['tr_rquest_type']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['tr_rquest_type']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "tr_rquest_type",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['tr_rquest_type']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["tr_rquest_type"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=tr_rquest_type&mode=<%$mod_enc_mode["Search"]%>&rformat=html'<%/if%>,
            "value": <%if $count_arr["tr_rquest_type"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["tr_rquest_type"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['tr_rquest_type']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["tr_rquest_type"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "tr_rquest_type",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=tr_rquest_type&mode=<%$mod_enc_mode["Update"]%>&rformat=html',
            "dataInit": <%if $count_arr['tr_rquest_type']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["tr_rquest_type"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": null,
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['tr_rquest_type']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "trr_status",
        "index": "trr_status",
        "label": "<%$list_config['trr_status']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['trr_status']['width']%>",
        "search": <%if $list_config['trr_status']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_status']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_status']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_status']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_status']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "trr_status",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['trr_status']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "trr_status",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['trr_status']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mso_total_cost",
        "index": "mso_total_cost",
        "label": "<%$list_config['mso_total_cost']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_total_cost']['width']%>",
        "search": <%if $list_config['mso_total_cost']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_total_cost']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_total_cost']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_total_cost']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_total_cost']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "mso_total_cost",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_total_cost']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "mso_total_cost",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_total_cost']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency",
        "footerTpl": {
            "subTPL": $.trim($("#txt_esum_foot_mso_total_cost_list2").html())
        }
    },
    {
        "name": "trr_total_amount",
        "index": "trr_total_amount",
        "label": "<%$list_config['trr_total_amount']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['trr_total_amount']['width']%>",
        "search": <%if $list_config['trr_total_amount']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_total_amount']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_total_amount']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_total_amount']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_total_amount']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['trr_total_amount']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['trr_total_amount']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency",
        "footerTpl": {
            "subTPL": $.trim($("#txt_esum_foot_trr_total_amount_list2").html())
        }
    },
    {
        "name": "mso_gateway_t_d_r",
        "index": "mso_gateway_t_d_r",
        "label": "<%$list_config['mso_gateway_t_d_r']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_gateway_t_d_r']['width']%>",
        "search": <%if $list_config['mso_gateway_t_d_r']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_gateway_t_d_r']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_gateway_t_d_r']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_gateway_t_d_r']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_gateway_t_d_r']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "mso_gateway_t_d_r",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_gateway_t_d_r']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "mso_gateway_t_d_r",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_gateway_t_d_r']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency",
        "footerTpl": {
            "subTPL": $.trim($("#txt_esum_foot_mso_gateway_t_d_r_list2").html())
        }
    },
    {
        "name": "mso_admin_commision",
        "index": "mso_admin_commision",
        "label": "<%$list_config['mso_admin_commision']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_admin_commision']['width']%>",
        "search": <%if $list_config['mso_admin_commision']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_admin_commision']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_admin_commision']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_admin_commision']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_admin_commision']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "mso_admin_commision",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_admin_commision']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "mso_admin_commision",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_admin_commision']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency",
        "footerTpl": {
            "subTPL": $.trim($("#txt_esum_foot_mso_admin_commision_list2").html())
        }
    },
    {
        "name": "mso_admin_commision_per",
        "index": "mso_admin_commision_per",
        "label": "<%$list_config['mso_admin_commision_per']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_admin_commision_per']['width']%>",
        "search": <%if $list_config['mso_admin_commision_per']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_admin_commision_per']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_admin_commision_per']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_admin_commision_per']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_admin_commision_per']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "mso_admin_commision_per",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_admin_commision_per']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "mso_admin_commision_per",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_admin_commision_per']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency",
        "footerTpl": {
            "subTPL": $.trim($("#txt_esum_foot_mso_admin_commision_per_list2").html())
        }
    },
    {
        "name": "ts_date",
        "index": "ts_date",
        "label": "<%$list_config['ts_date']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['ts_date']['width']%>",
        "search": <%if $list_config['ts_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['ts_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['ts_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['ts_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['ts_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "ts_date",
                "autocomplete": "off",
                "class": "search-inline-date",
                "aria-date-format": "<%$this->general->getAdminJSMoments('date')%>"
            },
            "sopt": dateSearchOpts,
            "searchhidden": <%if $list_config['ts_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchGridDateRangePicker
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "ts_date",
            "aria-date-format": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
            "aria-min": "",
            "aria-max": "",
            "placeholder": "",
            "class": "inline-edit-row inline-date-edit date-picker-icon dateOnly"
        },
        "ctrl_type": "date",
        "default_value": "<%$list_config['ts_date']['default']%>",
        "filterSopt": "bt"
    }];
         
    initMainGridListing();
    createTooltipHeading();
    callSwitchToParent();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 