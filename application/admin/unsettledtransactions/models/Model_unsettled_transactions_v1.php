<?php
/**
 * Description of Transactions Model
 *
 * @module Transactions
 *
 * @class model_unsettled_transactions_v1.php
 *
 * @path application\admin\unsettledtransactions\models\model_unsettled_transactions_v1.php
 *
 * @author CIT Dev Team
 *
 * @date 12.01.2016
 */

class Model_unsettled_transactions_v1 extends CI_Model
{
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $multi_lingual;
    public $physical_data_remove;
    //
    public $listing_data;
    public $rec_per_page;
    public $message;

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->table_name = "mst_sub_order";
        $this->table_alias = "mso";
        $this->primary_key = "iMstSubOrderId";
        $this->primary_alias = "mso_mst_sub_order_id";
        $this->physical_data_remove = "Yes";
        $this->grid_fields = array(
            "sys_custom_field_6",
            "tr_rquest_type",
            "trr_status",
            "mso_total_cost",
            "trr_total_amount",
            "mso_gateway_t_d_r",
            "mso_admin_commision",
            "ts_date",
            "sys_custom_field_2",
        );
        $this->join_tables = array(
            array(
                "table_name" => "trn_rma_refund",
                "table_alias" => "trr",
                "field_name" => "iMstSubOrderId",
                "rel_table_name" => "mst_sub_order",
                "rel_table_alias" => "mso",
                "rel_field_name" => "iMstSubOrderId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mst_order",
                "table_alias" => "mo",
                "field_name" => "iMstOrderId",
                "rel_table_name" => "mst_sub_order",
                "rel_table_alias" => "mso",
                "rel_field_name" => "iMstOrderId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "mst_store_detail",
                "table_alias" => "msd",
                "field_name" => "iMstStoreDetailId",
                "rel_table_name" => "mst_sub_order",
                "rel_table_alias" => "mso",
                "rel_field_name" => "iMstStoreDetailId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "iSubOrderId",
                "rel_table_name" => "mst_sub_order",
                "rel_table_alias" => "mso",
                "rel_field_name" => "iMstSubOrderId",
                "join_type" => "left",
                "extra_condition" => "",
            ),
            array(
                "table_name" => "trn_settlement",
                "table_alias" => "ts",
                "field_name" => "iTrnSettlementId",
                "rel_table_name" => "mst_sub_order",
                "rel_table_alias" => "mso",
                "rel_field_name" => "iTrnSettlementId",
                "join_type" => "left",
                "extra_condition" => "",
            )
        );
        $this->extra_cond = "";
        $this->groupby_cond = array(
            "mso.iMstSubOrderId",
        );
        $this->having_cond = "";
        $this->orderby_cond = array();
        $this->unique_type = "OR";
        $this->unique_fields = array();
        $this->switchto_fields = array();
        $this->default_filters = array();
        $this->search_config = array();
        $this->relation_modules = array();
        $this->deletion_modules = array();
        $this->print_rec = "No";
        $this->multi_lingual = "No";

        $this->rec_per_page = $this->config->item('REC_LIMIT');
    }

    /**
     * insert method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    public function insert($data = array())
    {
        $this->db->insert($this->table_name, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    /**
     * update method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while updating records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function update($data = array(), $where = '', $alias = "No", $join = "No")
    {
        if ($alias == "Yes")
        {
            if ($join == "Yes")
            {
                $join_tbls = $this->addJoinTables("NR");
            }
            if (trim($join_tbls) != '')
            {
                $set_cond = array();
                foreach ($data as $key => $val)
                {
                    $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                }
                if (is_numeric($where))
                {
                    $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                }
                elseif ($where)
                {
                    $extra_cond = " WHERE ".$where;
                }
                else
                {
                    return FALSE;
                }
                $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
                $res = $this->db->query($update_query);
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->table_alias.".".$this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
            }
        }
        else
        {
            if (is_numeric($where))
            {
                $this->db->where($this->primary_key, $where);
            }
            elseif ($where)
            {
                $this->db->where($where, FALSE, FALSE);
            }
            else
            {
                return FALSE;
            }
            $res = $this->db->update($this->table_name, $data);
        }
        return $res;
    }

    /**
     * delete method is used to delete data records from the database table.
     * @param string $where where is the query condition for deletion.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while deleting records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function delete($where = "", $alias = "No", $join = "No")
    {
        if ($this->config->item('PHYSICAL_RECORD_DELETE') && $this->physical_data_remove == 'No')
        {
            if ($alias == "Yes")
            {
                if (is_array($join['joins']) && count($join['joins']))
                {
                    $join_tbls = '';
                    if ($join['list'] == "Yes")
                    {
                        $join_tbls = $this->addJoinTables("NR");
                    }
                    $join_tbls .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                }
                elseif ($join == "Yes")
                {
                    $join_tbls = $this->addJoinTables("NR");
                }
                $data = $this->general->getPhysicalRecordUpdate($this->table_alias);
                if (trim($join_tbls) != '')
                {
                    $set_cond = array();
                    foreach ($data as $key => $val)
                    {
                        $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                    }
                    if (is_numeric($where))
                    {
                        $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                    }
                    elseif ($where)
                    {
                        $extra_cond = " WHERE ".$where;
                    }
                    else
                    {
                        return FALSE;
                    }
                    $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".@implode(", ", $set_cond)." ".$extra_cond;
                    $res = $this->db->query($update_query);
                }
                else
                {
                    if (is_numeric($where))
                    {
                        $this->db->where($this->table_alias.".".$this->primary_key, $where);
                    }
                    elseif ($where)
                    {
                        $this->db->where($where, FALSE, FALSE);
                    }
                    else
                    {
                        return FALSE;
                    }
                    $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
                }
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $data = $this->general->getPhysicalRecordUpdate();
                $res = $this->db->update($this->table_name, $data);
            }
        }
        else
        {
            if ($alias == "Yes")
            {
                $del_query = "DELETE ".$this->db->protect($this->table_alias).".* FROM ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias);
                if (is_array($join['joins']) && count($join['joins']))
                {
                    if ($join['list'] == "Yes")
                    {
                        $del_query .= $this->addJoinTables("NR");
                    }
                    $del_query .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                }
                elseif ($join == "Yes")
                {
                    $del_query .= $this->addJoinTables("NR");
                }
                if (is_numeric($where))
                {
                    $del_query .= " WHERE ".$this->db->protect($this->table_alias).".".$this->db->protect($this->primary_key)." = ".$this->db->escape($where);
                }
                elseif ($where)
                {
                    $del_query .= " WHERE ".$where;
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->query($del_query);
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->delete($this->table_name);
            }
        }
        return $res;
    }

    /**
     * getData method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @param boolean $having_cond having cond is the query condition for getting conditional data.
     * @param boolean $list list is to differ listing fields or form fields.
     * @return array $data_arr returns data records array.
     */
    public function getData($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No", $having_cond = '', $list = FALSE)
    {
        if (is_array($fields))
        {
            $this->listing->addSelectFields($fields);
        }
        elseif ($fields != "")
        {
            $this->db->select($fields);
        }
        elseif ($list == TRUE)
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
            if ($this->primary_alias != "")
            {
                $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
            }
            $this->db->select("(CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber)) AS sys_custom_field_6", FALSE);
            $this->db->select("tr.eRquestType AS tr_rquest_type");
            $this->db->select("trr.eStatus AS trr_status");
            $this->db->select("mso.fTotalCost AS mso_total_cost");
            $this->db->select("trr.fTotalAmount AS trr_total_amount");
            $this->db->select("mso.fGatewayTDR AS mso_gateway_t_d_r");
            $this->db->select("mso.fAdminCommision AS mso_admin_commision");
            $this->db->select("ts.dDate AS ts_date");
            $this->db->select("(IF(trr.fTotalAmount > 0,mso.fTotalCost - trr.fTotalAmount,mso.fTotalCost-fGatewayTDR-fAdminCommision)) AS sys_custom_field_2", FALSE);
        }
        else
        {
            $this->db->select("mso.iMstSubOrderId AS iMstSubOrderId");
            $this->db->select("mso.iMstSubOrderId AS mso_mst_sub_order_id");
            $this->db->select("mso.iMstOrderId AS mso_mst_order_id");
            $this->db->select("mso.iMstStoreDetailId AS mso_mst_store_detail_id");
            $this->db->select("mso.iMstProductsId AS mso_mst_products_id");
            $this->db->select("mso.vProductName AS mso_product_name");
            $this->db->select("mso.vProductSku AS mso_product_sku");
            $this->db->select("mso.fProductRegularPrice AS mso_product_regular_price");
            $this->db->select("mso.fProductSalePrice AS mso_product_sale_price");
            $this->db->select("mso.fProductPrice AS mso_product_price");
            $this->db->select("mso.iProductQty AS mso_product_qty");
            $this->db->select("mso.eCanelBy AS mso_canel_by");
            $this->db->select("mso.eItemStatus AS mso_item_status");
            $this->db->select("mso.fShippingCost AS mso_shipping_cost");
            $this->db->select("mso.fTotalCost AS mso_total_cost");
            $this->db->select("mso.iReturnPeriod AS mso_return_period");
            $this->db->select("mso.tExtraInfo AS mso_extra_info");
            $this->db->select("mso.tProductOption AS mso_product_option");
            $this->db->select("mso.vSellerInvoiceDate AS mso_seller_invoice_date");
            $this->db->select("mso.vSellerInvoiceNo AS mso_seller_invoice_no");
            $this->db->select("mso.dShippingDate AS mso_shipping_date");
            $this->db->select("mso.vShipperName AS mso_shipper_name");
            $this->db->select("mso.iMstShipperId AS mso_mst_shipper_id");
            $this->db->select("mso.tCancelDetail AS mso_cancel_detail");
            $this->db->select("mso.dCancelDate AS mso_cancel_date");
            $this->db->select("mso.iCancelUserId AS mso_cancel_user_id");
            $this->db->select("mso.fGatewayTDR AS mso_gateway_t_d_r");
            $this->db->select("mso.fMarketPlaceFee AS mso_market_place_fee");
            $this->db->select("mso.fAdminCommisionPer AS mso_admin_commision_per");
            $this->db->select("mso.fAdminCommision AS mso_admin_commision");
            $this->db->select("mso.tShippingRemark AS mso_shipping_remark");
            $this->db->select("mso.fGatewayTDRPer AS mso_gateway_t_d_r_per");
            $this->db->select("mso.vTrackingNumber AS mso_tracking_number");
            $this->db->select("mso.dDeliveredDate AS mso_delivered_date");
            $this->db->select("mso.eActionTakenBy AS mso_action_taken_by");
            $this->db->select("mso.eSettlement AS mso_settlement");
            $this->db->select("mso.vSubOrderNumberPre AS mso_sub_order_number_pre");
            $this->db->select("mso.iSubOrderNumber AS mso_sub_order_number");
            $this->db->select("mso.iTrnSettlementId AS mso_trn_settlement_id");
        }

        $this->db->from($this->table_name." AS ".$this->table_alias);
        if (is_array($join) && is_array($join['joins']) && count($join['joins']) > 0)
        {
            $this->listing->addJoinTables($join['joins']);
            if ($join["list"] == "Yes")
            {
                $this->addJoinTables("AR");
            }
        }
        else
        {
            if ($join == "Yes")
            {
                $this->addJoinTables("AR");
            }
        }
        if (is_array($extra_cond) && count($extra_cond) > 0)
        {
            $this->listing->addWhereFields($extra_cond);
        }
        elseif (is_numeric($extra_cond))
        {
            $this->db->where($this->table_alias.".".$this->primary_key, intval($extra_cond));
        }
        elseif ($extra_cond)
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if ($group_by != "")
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        if ($order_by != "")
        {
            $this->db->order_by($order_by);
        }
        if ($limit != "")
        {
            if (is_numeric($limit))
            {
                $this->db->limit($limit);
            }
            else
            {
                list($offset, $limit) = @explode(",", $limit);
                $this->db->limit($offset, $limit);
            }
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }

    /**
     * getListingData method is used to get grid listing data records for this module.
     * @param array $config_arr config_arr for grid listing settigs.
     * @return array $listing_data returns data records array for grid.
     */
    public function getListingData($config_arr = array())
    {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $rec_per_page = (intval($rows) > 0) ? intval($rows) : $this->rec_per_page;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->start_cache();
        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "")
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0)
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;
        $filter_config['grid_fields'] = $this->grid_fields;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "")
        {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "")
        {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "")
        {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("(CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber)) AS sys_custom_field_6", FALSE);
        $this->db->select("tr.eRquestType AS tr_rquest_type");
        $this->db->select("trr.eStatus AS trr_status");
        $this->db->select("mso.fTotalCost AS mso_total_cost");
        $this->db->select("trr.fTotalAmount AS trr_total_amount");
        $this->db->select("mso.fGatewayTDR AS mso_gateway_t_d_r");
        $this->db->select("mso.fAdminCommision AS mso_admin_commision");
        $this->db->select("ts.dDate AS ts_date");
        $this->db->select("(IF(trr.fTotalAmount > 0,mso.fTotalCost - trr.fTotalAmount,mso.fTotalCost-fGatewayTDR-fAdminCommision)) AS sys_custom_field_2", FALSE);

        $this->db->stop_cache();
        if ((is_array($group_by) && count($group_by) > 0) || trim($having_cond) != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key);
            $total_records_arr = $this->db->get();
            $total_records = is_object($total_records_arr) ? $total_records_arr->num_rows() : 0;
        }
        else
        {
            $total_records = $this->db->count_all_results();
        }

        $total_pages = $this->listing->getTotalPages($total_records, $rec_per_page);
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0)
        {
            foreach ($order_by as $orK => $orV)
            {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "")
        {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        $limit_offset = $this->listing->getStartIndex($total_records, $page, $rec_per_page);
        $this->db->limit($rec_per_page, $limit_offset);
        $return_data_obj = $this->db->get();
        $return_data = is_object($return_data_obj) ? $return_data_obj->result_array() : array();
        $this->db->flush_cache();
        $listing_data = $this->listing->getDataForJqGrid($return_data, $filter_config, $page, $total_pages, $total_records);
        $this->listing_data = $return_data;
        #echo $this->db->last_query();
        return $listing_data;
    }

    /**
     * getExportData method is used to get grid export data records for this module.
     * @param array $config_arr config_arr for grid export settigs.
     * @return array $export_data returns data records array for export.
     */
    public function getExportData($config_arr = array())
    {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $rowlimit = $config_arr['rowlimit'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "")
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0)
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "")
        {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "")
        {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "")
        {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("(CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber)) AS sys_custom_field_6", FALSE);
        $this->db->select("tr.eRquestType AS tr_rquest_type");
        $this->db->select("trr.eStatus AS trr_status");
        $this->db->select("mso.fTotalCost AS mso_total_cost");
        $this->db->select("trr.fTotalAmount AS trr_total_amount");
        $this->db->select("mso.fGatewayTDR AS mso_gateway_t_d_r");
        $this->db->select("mso.fAdminCommision AS mso_admin_commision");
        $this->db->select("ts.dDate AS ts_date");
        $this->db->select("(IF(trr.fTotalAmount > 0,mso.fTotalCost - trr.fTotalAmount,mso.fTotalCost-fGatewayTDR-fAdminCommision)) AS sys_custom_field_2", FALSE);
        if ($sdef == "Yes" && is_array($order_by) && count($order_by) > 0)
        {
            foreach ($order_by as $orK => $orV)
            {
                $sort_filed = $orV['field'];
                $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                $this->db->order_by($sort_filed, $sort_order);
            }
        }
        if ($sidx != "")
        {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        if ($rowlimit != "")
        {
            $offset = $rowlimit;
            $limit = ($rowlimit*$page-$rowlimit);
            $this->db->limit($offset, $limit);
        }
        $export_data_obj = $this->db->get();
        $export_data = is_object($export_data_obj) ? $export_data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $export_data;
    }

    /**
     * addJoinTables method is used to make relation tables joins with main table.
     * @param string $type type is to get active record or join string.
     * @param boolean $allow_tables allow_table is to restrict some set of tables.
     * @return string $ret_joins returns relation tables join string.
     */
    public function addJoinTables($type = 'AR', $allow_tables = FALSE)
    {
        $join_tables = $this->join_tables;
        if (!is_array($join_tables) || count($join_tables) == 0)
        {
            return '';
        }
        $ret_joins = $this->listing->addJoinTables($join_tables, $type, $allow_tables);
        return $ret_joins;
    }

    /**
     * getListConfiguration method is used to get listing configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns listing configuration array.
     */
    public function getListConfiguration($name = "")
    {
        $list_config = array(
            "sys_custom_field_6" => array(
                "name" => "sys_custom_field_6",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "",
                "source_field" => "sys_custom_field_6",
                "display_query" => "CONCAT_WS('',mso.vSubOrderNumberPre,mso.iSubOrderNumber)",
                "entry_type" => "Custom",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Sub Order No",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SUB_ORDER_NO'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "tr_rquest_type" => array(
                "name" => "tr_rquest_type",
                "table_name" => "trn_rma",
                "table_alias" => "tr",
                "field_name" => "eRquestType",
                "source_field" => "tr_rquest_type",
                "display_query" => "tr.eRquestType",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Return Status",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_RETURN_STATUS'),
                "width" => 150,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "trr_status" => array(
                "name" => "trr_status",
                "table_name" => "trn_rma_refund",
                "table_alias" => "trr",
                "field_name" => "eStatus",
                "source_field" => "trr_status",
                "display_query" => "trr.eStatus",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Refund  Status",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_REFUND__STATUS'),
                "width" => 125,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mso_total_cost" => array(
                "name" => "mso_total_cost",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fTotalCost",
                "source_field" => "mso_total_cost",
                "display_query" => "mso.fTotalCost",
                "entry_type" => "Table",
                "data_type" => "double",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "right",
                "label" => "Order Total",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_ORDER_TOTAL'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "trr_total_amount" => array(
                "name" => "trr_total_amount",
                "table_name" => "trn_rma_refund",
                "table_alias" => "trr",
                "field_name" => "fTotalAmount",
                "source_field" => "",
                "display_query" => "trr.fTotalAmount",
                "entry_type" => "Table",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "right",
                "label" => "Refund Amount",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_REFUND_AMOUNT'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mso_gateway_t_d_r" => array(
                "name" => "mso_gateway_t_d_r",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fGatewayTDR",
                "source_field" => "mso_gateway_t_d_r",
                "display_query" => "mso.fGatewayTDR",
                "entry_type" => "Table",
                "data_type" => "double",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "right",
                "label" => "Paypal",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_PAYPAL'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "mso_admin_commision" => array(
                "name" => "mso_admin_commision",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fAdminCommision",
                "source_field" => "mso_admin_commision",
                "display_query" => "mso.fAdminCommision",
                "entry_type" => "Table",
                "data_type" => "double",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "right",
                "label" => "Commision",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_COMMISION'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ts_date" => array(
                "name" => "ts_date",
                "table_name" => "trn_settlement",
                "table_alias" => "ts",
                "field_name" => "dDate",
                "source_field" => "ts_date",
                "display_query" => "ts.dDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Settled Date",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SETTLED_DATE'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => $this->general->getAdminPHPFormats('date')
            ),
            "sys_custom_field_2" => array(
                "name" => "sys_custom_field_2",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "",
                "source_field" => "sys_custom_field_2",
                "display_query" => "IF(trr.fTotalAmount > 0,mso.fTotalCost - trr.fTotalAmount,mso.fTotalCost-fGatewayTDR-fAdminCommision)",
                "entry_type" => "Custom",
                "data_type" => "",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Settled Amt",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SETTLED_AMT'),
                "width" => 120,
                "search" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "php_func" => "calculateDueAmount",
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0)
        {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++)
            {
                $config_arr[$name[$i]] = $list_config[$name[$i]];
            }
        }
        elseif ($name != "" && is_string($name))
        {
            $config_arr = $list_config[$name];
        }
        else
        {
            $config_arr = $list_config;
        }
        return $config_arr;
    }

    /**
     * getFormConfiguration method is used to get form configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns form configuration array.
     */
    public function getFormConfiguration($name = "")
    {
        $form_config = array(
            "mso_mst_order_id" => array(
                "name" => "mso_mst_order_id",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iMstOrderId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Mst Order Id",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_MST_ORDER_ID')
            ),
            "mso_mst_store_detail_id" => array(
                "name" => "mso_mst_store_detail_id",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iMstStoreDetailId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Mst Store Detail Id",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_MST_STORE_DETAIL_ID')
            ),
            "mso_mst_products_id" => array(
                "name" => "mso_mst_products_id",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iMstProductsId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Mst Products Id",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_MST_PRODUCTS_ID')
            ),
            "mso_product_name" => array(
                "name" => "mso_product_name",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vProductName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Product Name",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_PRODUCT_NAME')
            ),
            "mso_product_sku" => array(
                "name" => "mso_product_sku",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vProductSku",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Product Sku",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_PRODUCT_SKU')
            ),
            "mso_product_regular_price" => array(
                "name" => "mso_product_regular_price",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fProductRegularPrice",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Product Regular Price",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_PRODUCT_REGULAR_PRICE')
            ),
            "mso_product_sale_price" => array(
                "name" => "mso_product_sale_price",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fProductSalePrice",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Product Sale Price",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_PRODUCT_SALE_PRICE')
            ),
            "mso_product_price" => array(
                "name" => "mso_product_price",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fProductPrice",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Product Price",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_PRODUCT_PRICE')
            ),
            "mso_product_qty" => array(
                "name" => "mso_product_qty",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iProductQty",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Product Qty",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_PRODUCT_QTY')
            ),
            "mso_canel_by" => array(
                "name" => "mso_canel_by",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "eCanelBy",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Canel By",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_CANEL_BY')
            ),
            "mso_item_status" => array(
                "name" => "mso_item_status",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "eItemStatus",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Item Status",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_ITEM_STATUS'),
                "default" => $this->filter->getDefaultValue("mso_item_status",
                "Text",
                "Pending")
            ),
            "mso_shipping_cost" => array(
                "name" => "mso_shipping_cost",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fShippingCost",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Shipping Cost",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SHIPPING_COST')
            ),
            "mso_total_cost" => array(
                "name" => "mso_total_cost",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fTotalCost",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Total Cost",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_TOTAL_COST')
            ),
            "mso_return_period" => array(
                "name" => "mso_return_period",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iReturnPeriod",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Return Period",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_RETURN_PERIOD')
            ),
            "mso_extra_info" => array(
                "name" => "mso_extra_info",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "tExtraInfo",
                "entry_type" => "Table",
                "data_type" => "text",
                "type" => "textarea",
                "label" => "Extra Info",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_EXTRA_INFO')
            ),
            "mso_product_option" => array(
                "name" => "mso_product_option",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "tProductOption",
                "entry_type" => "Table",
                "data_type" => "text",
                "type" => "textarea",
                "label" => "Product Option",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_PRODUCT_OPTION')
            ),
            "mso_seller_invoice_date" => array(
                "name" => "mso_seller_invoice_date",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vSellerInvoiceDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "type" => "date",
                "label" => "Seller Invoice Date",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SELLER_INVOICE_DATE'),
                "format" => $this->general->getAdminPHPFormats('date')
            ),
            "mso_seller_invoice_no" => array(
                "name" => "mso_seller_invoice_no",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vSellerInvoiceNo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Seller Invoice No",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SELLER_INVOICE_NO')
            ),
            "mso_shipping_date" => array(
                "name" => "mso_shipping_date",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "dShippingDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "type" => "date",
                "label" => "Shipping Date",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SHIPPING_DATE'),
                "format" => $this->general->getAdminPHPFormats('date')
            ),
            "mso_shipper_name" => array(
                "name" => "mso_shipper_name",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vShipperName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Shipper Name",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SHIPPER_NAME')
            ),
            "mso_mst_shipper_id" => array(
                "name" => "mso_mst_shipper_id",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iMstShipperId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Mst Shipper Id",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_MST_SHIPPER_ID')
            ),
            "mso_cancel_detail" => array(
                "name" => "mso_cancel_detail",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "tCancelDetail",
                "entry_type" => "Table",
                "data_type" => "text",
                "type" => "textarea",
                "label" => "Cancel Detail",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_CANCEL_DETAIL')
            ),
            "mso_cancel_date" => array(
                "name" => "mso_cancel_date",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "dCancelDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "type" => "date",
                "label" => "Cancel Date",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_CANCEL_DATE'),
                "format" => $this->general->getAdminPHPFormats('date')
            ),
            "mso_cancel_user_id" => array(
                "name" => "mso_cancel_user_id",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iCancelUserId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Cancel User Id",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_CANCEL_USER_ID')
            ),
            "mso_gateway_t_d_r" => array(
                "name" => "mso_gateway_t_d_r",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fGatewayTDR",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Gateway T D R",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_GATEWAY_T_D_R')
            ),
            "mso_market_place_fee" => array(
                "name" => "mso_market_place_fee",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fMarketPlaceFee",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Market Place Fee",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_MARKET_PLACE_FEE')
            ),
            "mso_admin_commision_per" => array(
                "name" => "mso_admin_commision_per",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fAdminCommisionPer",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Admin Commision Per",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_ADMIN_COMMISION_PER')
            ),
            "mso_admin_commision" => array(
                "name" => "mso_admin_commision",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fAdminCommision",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Admin Commision",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_ADMIN_COMMISION')
            ),
            "mso_shipping_remark" => array(
                "name" => "mso_shipping_remark",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "tShippingRemark",
                "entry_type" => "Table",
                "data_type" => "text",
                "type" => "textarea",
                "label" => "Shipping Remark",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SHIPPING_REMARK')
            ),
            "mso_gateway_t_d_r_per" => array(
                "name" => "mso_gateway_t_d_r_per",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "fGatewayTDRPer",
                "entry_type" => "Table",
                "data_type" => "double",
                "type" => "textbox",
                "label" => "Gateway T D R Per",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_GATEWAY_T_D_R_PER')
            ),
            "mso_tracking_number" => array(
                "name" => "mso_tracking_number",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vTrackingNumber",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Tracking Number",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_TRACKING_NUMBER')
            ),
            "mso_delivered_date" => array(
                "name" => "mso_delivered_date",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "dDeliveredDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "type" => "date",
                "label" => "Delivered Date",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_DELIVERED_DATE'),
                "format" => $this->general->getAdminPHPFormats('date')
            ),
            "mso_action_taken_by" => array(
                "name" => "mso_action_taken_by",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "eActionTakenBy",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Action Taken By",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_ACTION_TAKEN_BY')
            ),
            "mso_settlement" => array(
                "name" => "mso_settlement",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "eSettlement",
                "entry_type" => "Table",
                "data_type" => "enum",
                "type" => "dropdown",
                "label" => "Settlement",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SETTLEMENT'),
                "default" => $this->filter->getDefaultValue("mso_settlement",
                "Text",
                "No")
            ),
            "mso_sub_order_number_pre" => array(
                "name" => "mso_sub_order_number_pre",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "vSubOrderNumberPre",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "type" => "textbox",
                "label" => "Sub Order Number Pre",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SUB_ORDER_NUMBER_PRE')
            ),
            "mso_sub_order_number" => array(
                "name" => "mso_sub_order_number",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iSubOrderNumber",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Sub Order Number",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_SUB_ORDER_NUMBER')
            ),
            "mso_trn_settlement_id" => array(
                "name" => "mso_trn_settlement_id",
                "table_name" => "mst_sub_order",
                "table_alias" => "mso",
                "field_name" => "iTrnSettlementId",
                "entry_type" => "Table",
                "data_type" => "int",
                "type" => "dropdown",
                "label" => "Trn Settlement Id",
                "label_lang" => $this->lang->line('UNSETTLED_TRANSACTIONS_V1_TRN_SETTLEMENT_ID')
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0)
        {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++)
            {
                $config_arr[$name[$i]] = $form_config[$name[$i]];
            }
        }
        elseif ($name != "" && is_string($name))
        {
            $config_arr = $form_config[$name];
        }
        else
        {
            $config_arr = $form_config;
        }
        return $config_arr;
    }

    /**
     * checkRecordExists method is used to check duplication of records.
     * @param array $field_arr field_arr is having fields to check.
     * @param array $field_val field_val is having values of respective fields.
     * @param numeric $id id is to avoid current records.
     * @param string $mode mode is having either Add or Update.
     * @param string $con con is having either AND or OR.
     * @return boolean $exists returns either TRUE of FALSE.
     */
    public function checkRecordExists($field_arr = array(), $field_val = array(), $id = '', $mode = '', $con = 'AND')
    {
        $exists = FALSE;
        if (!is_array($field_arr) || count($field_arr) == 0)
        {
            return $exists;
        }
        foreach ((array) $field_arr as $key => $val)
        {
            $extra_cond_arr[] = $this->db->protect($this->table_alias.".".$field_arr[$key])." =  ".$this->db->escape($field_val[$val]);
        }
        $extra_cond = "(".@implode(" ".$con." ", $extra_cond_arr).")";
        if ($mode == "Add")
        {
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0)
            {
                $exists = TRUE;
            }
        }
        elseif ($mode == "Update")
        {
            $extra_cond = $this->db->protect($this->table_alias.".".$this->primary_key)." <> ".$this->db->escape($id)." AND ".$extra_cond;
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0)
            {
                $exists = TRUE;
            }
        }
        return $exists;
    }

    /**
     * getSwitchTo method is used to get switch to dropdown array.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @return array $switch_data returns data records array.
     */
    public function getSwitchTo($extra_cond = '', $type = 'records', $limit = '')
    {
        $switchto_fields = $this->switchto_fields;
        $switch_data = array();
        if (!is_array($switchto_fields) || count($switchto_fields) == 0)
        {
            if ($type == "count")
            {
                return count($switch_data);
            }
            else
            {
                return $switch_data;
            }
        }
        $fields_arr = array();
        $fields_arr[] = array(
            "field" => $this->table_alias.".".$this->primary_key." AS id",
        );
        $fields_arr[] = array(
            "field" => $this->db->concat($switchto_fields)." AS val",
            "escape" => TRUE,
        );
        if (trim($this->extra_cond) != "")
        {
            $extra_cond = (trim($extra_cond) != "") ? $extra_cond." AND ".$this->extra_cond : $this->extra_cond;
        }
        $switch_data = $this->getData($extra_cond, $fields_arr, "val ASC", "", $limit, "Yes");
        #echo $this->db->last_query();
        if ($type == "count")
        {
            return count($switch_data);
        }
        else
        {
            return $switch_data;
        }
    }
}
