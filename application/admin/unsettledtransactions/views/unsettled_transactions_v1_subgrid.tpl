            

<div class="jq-subgrid-block">
    <div id="<%$subgrid_pager_id%>"></div>
    <table id="<%$subgrid_table_id%>"></table>
</div>
<%javascript%>
    var el_subgrid_settings = {}, sub_js_col_name_json = {}, sub_js_col_model_json = {};
           
    el_subgrid_settings['table_id'] = '<%$subgrid_table_id%>';
    el_subgrid_settings['pager_id'] = '<%$subgrid_pager_id%>';
    el_subgrid_settings['module_name'] = '<%$module_name%>';
    el_subgrid_settings['advanced_grid'] = '<%$exp_advanced_grid%>';
    el_subgrid_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_subgrid_settings['extra_qstrs'] = '<%$extra_qstr%>';
    el_subgrid_settings['par_module'] = '<%$exp_module_name%>';
    el_subgrid_settings['par_data'] = '<%$exp_par_id%>';
    el_subgrid_settings['par_field'] = '<%$exp_par_field%>';
    el_subgrid_settings['par_type'] = 'grid';
            
    el_subgrid_settings['add_page_url'] = '<%$mod_enc_url["add"]%>'; 
    el_subgrid_settings['edit_page_url'] = admin_url+'<%$mod_enc_url["inline_edit_action"]%>?<%$extra_qstr%>';
    el_subgrid_settings['listing_url'] = admin_url+'<%$mod_enc_url["listing"]%>?<%$extra_qstr%>';
    el_subgrid_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_subgrid_settings['auto_complete_url'] = admin_url+'<%$mod_enc_url["get_token_auto_complete"]%>?<%$extra_qstr%>';
    el_subgrid_settings['nesgrid_listing_url'] =  admin_url+'<%$mod_enc_url["get_subgrid_block"]%>?<%$extra_qstr%>';
    
    el_subgrid_settings['admin_rec_arr'] = {};
    el_subgrid_settings['status_arr'] = $.parseJSON('<%$status_array|@json_encode%>');
    el_subgrid_settings['status_lang_arr'] = $.parseJSON('<%$status_array_label|@json_encode%>');
            
    el_subgrid_settings['hide_add_btn'] = '';
    el_subgrid_settings['hide_del_btn'] = '';
    el_subgrid_settings['hide_status_btn'] = '';
    
    el_subgrid_settings['hide_advance_search'] = 'Yes';
    el_subgrid_settings['hide_search_tool'] = 'Yes';
    el_subgrid_settings['hide_multi_select'] = 'Yes';
    el_subgrid_settings['popup_add_form'] = 'No';
    el_subgrid_settings['popup_edit_form'] = 'No';
    el_subgrid_settings['popup_add_size'] = ['75%', '75%'];
    el_subgrid_settings['popup_edit_size'] = ['75%', '75%'];
    el_subgrid_settings['hide_paging_btn'] = 'Yes';
    el_subgrid_settings['hide_refresh_btn'] = 'Yes';
    el_subgrid_settings['group_search'] = '';
    
    el_subgrid_settings['permit_add_btn'] = '<%$add_access%>';
    el_subgrid_settings['permit_del_btn'] = '<%$del_access%>';
    el_subgrid_settings['permit_edit_btn'] = '<%$view_access%>';
    
    el_subgrid_settings['default_sort'] = 'sys_custom_field_6';
    el_subgrid_settings['sort_order'] = 'asc';
    
    el_subgrid_settings['footer_row'] = 'No';
    el_subgrid_settings['grouping'] = 'No';
    el_subgrid_settings['group_attr'] = {};
    
    el_subgrid_settings['inline_add'] = 'No';
    el_subgrid_settings['rec_position'] = 'Top';
    el_subgrid_settings['auto_width'] = 'Yes';
    el_subgrid_settings['nesgrid'] = '<%$exp_nested_grid%>';
    el_subgrid_settings['rating_allow'] = 'No';
    el_subgrid_settings['listview'] = 'list';
    el_subgrid_settings['top_filter'] = [];
    el_subgrid_settings['buttons_arr'] = [];
    
    sub_js_col_name_json = [{
        "name": "sys_custom_field_6",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_SUB_ORDER_NO')%>"
    },
    {
        "name": "tr_rquest_type",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_RETURN_STATUS')%>"
    },
    {
        "name": "trr_status",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_REFUND__STATUS')%>"
    },
    {
        "name": "mso_total_cost",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_ORDER_TOTAL')%>"
    },
    {
        "name": "trr_total_amount",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_REFUND_AMOUNT')%>"
    },
    {
        "name": "mso_gateway_t_d_r",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_PAYPAL')%>"
    },
    {
        "name": "mso_admin_commision",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_COMMISION')%>"
    },
    {
        "name": "ts_date",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_SETTLED_DATE')%>"
    },
    {
        "name": "sys_custom_field_2",
        "label": "<%$this->lang->line('UNSETTLED_TRANSACTIONS_V1_SETTLED_AMT')%>"
    }];

    sub_js_col_model_json = [{
        "name": "sys_custom_field_6",
        "index": "sys_custom_field_6",
        "label": "<%$list_config['sys_custom_field_6']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['sys_custom_field_6']['width']%>",
        "search": <%if $list_config['sys_custom_field_6']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['sys_custom_field_6']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['sys_custom_field_6']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['sys_custom_field_6']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['sys_custom_field_6']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "sys_custom_field_6",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['sys_custom_field_6']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "sys_custom_field_6",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['sys_custom_field_6']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "tr_rquest_type",
        "index": "tr_rquest_type",
        "label": "<%$list_config['tr_rquest_type']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['tr_rquest_type']['width']%>",
        "search": <%if $list_config['tr_rquest_type']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['tr_rquest_type']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['tr_rquest_type']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['tr_rquest_type']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['tr_rquest_type']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "tr_rquest_type",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['tr_rquest_type']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["tr_rquest_type"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=tr_rquest_type&mode=<%$mod_enc_mode["Search"]%>&rformat=html'<%/if%>,
            "value": <%if $count_arr["tr_rquest_type"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["tr_rquest_type"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['tr_rquest_type']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["tr_rquest_type"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "tr_rquest_type",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=tr_rquest_type&mode=<%$mod_enc_mode["Update"]%>&rformat=html',
            "dataInit": <%if $count_arr['tr_rquest_type']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["tr_rquest_type"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": null,
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['tr_rquest_type']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "trr_status",
        "index": "trr_status",
        "label": "<%$list_config['trr_status']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['trr_status']['width']%>",
        "search": <%if $list_config['trr_status']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_status']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_status']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_status']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_status']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "trr_status",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['trr_status']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["trr_status"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=trr_status&mode=<%$mod_enc_mode["Search"]%>&rformat=html'<%/if%>,
            "value": <%if $count_arr["trr_status"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["trr_status"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['trr_status']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["trr_status"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "trr_status",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=trr_status&mode=<%$mod_enc_mode["Update"]%>&rformat=html',
            "dataInit": <%if $count_arr['trr_status']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["trr_status"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": null,
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['trr_status']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "mso_total_cost",
        "index": "mso_total_cost",
        "label": "<%$list_config['mso_total_cost']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_total_cost']['width']%>",
        "search": <%if $list_config['mso_total_cost']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_total_cost']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_total_cost']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_total_cost']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_total_cost']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "mso_total_cost",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_total_cost']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "mso_total_cost",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_total_cost']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency"
    },
    {
        "name": "trr_total_amount",
        "index": "trr_total_amount",
        "label": "<%$list_config['trr_total_amount']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['trr_total_amount']['width']%>",
        "search": <%if $list_config['trr_total_amount']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['trr_total_amount']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['trr_total_amount']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['trr_total_amount']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['trr_total_amount']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['trr_total_amount']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['trr_total_amount']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency"
    },
    {
        "name": "mso_gateway_t_d_r",
        "index": "mso_gateway_t_d_r",
        "label": "<%$list_config['mso_gateway_t_d_r']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_gateway_t_d_r']['width']%>",
        "search": <%if $list_config['mso_gateway_t_d_r']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_gateway_t_d_r']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_gateway_t_d_r']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_gateway_t_d_r']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_gateway_t_d_r']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "mso_gateway_t_d_r",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_gateway_t_d_r']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "mso_gateway_t_d_r",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_gateway_t_d_r']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency"
    },
    {
        "name": "mso_admin_commision",
        "index": "mso_admin_commision",
        "label": "<%$list_config['mso_admin_commision']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_admin_commision']['width']%>",
        "search": <%if $list_config['mso_admin_commision']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_admin_commision']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_admin_commision']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_admin_commision']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_admin_commision']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "mso_admin_commision",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_admin_commision']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "mso_admin_commision",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_admin_commision']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency"
    },
    {
        "name": "ts_date",
        "index": "ts_date",
        "label": "<%$list_config['ts_date']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['ts_date']['width']%>",
        "search": <%if $list_config['ts_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['ts_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['ts_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['ts_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['ts_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "ts_date",
                "autocomplete": "off",
                "class": "search-inline-date",
                "aria-date-format": "<%$this->general->getAdminJSMoments('date')%>"
            },
            "sopt": dateSearchOpts,
            "searchhidden": <%if $list_config['ts_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchGridDateRangePicker
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "ts_date",
            "aria-date-format": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
            "aria-min": "",
            "aria-max": "",
            "placeholder": "",
            "class": "inline-edit-row inline-date-edit date-picker-icon dateOnly"
        },
        "ctrl_type": "date",
        "default_value": "<%$list_config['ts_date']['default']%>",
        "filterSopt": "bt"
    },
    {
        "name": "sys_custom_field_2",
        "index": "sys_custom_field_2",
        "label": "<%$list_config['sys_custom_field_2']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['sys_custom_field_2']['width']%>",
        "search": <%if $list_config['sys_custom_field_2']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['sys_custom_field_2']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['sys_custom_field_2']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['sys_custom_field_2']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['sys_custom_field_2']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "unsettled_transactions_v1",
                "aria-unique-name": "sys_custom_field_2",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['sys_custom_field_2']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "unsettled_transactions_v1",
            "aria-unique-name": "sys_custom_field_2",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['sys_custom_field_2']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency"
    }];
                  
    initSubGridListing();
<%/javascript%>
    