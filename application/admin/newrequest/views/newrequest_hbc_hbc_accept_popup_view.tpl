<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>    
<%$this->js->add_js('jquery-1.9.1.min.js')%>
<%$this->js->add_js('project.js')%>
<%$this->js->add_js('admin/bootstrap.min.js')%>
<%$this->js->js_src()%>
<%$this->css->add_css('admin/bootstrap.min.css')%>	
<%$this->css->add_css('admin/style_developer.css')%>		
<%$this->css->css_src()%> 
<style type="text/css">
    .form_input input[type="button"]{
        background: #333333;
        border: 1px solid #797979;
        border-radius: 4px;
        color: #fff;
        font-size: 14px;
        font-weight: bold;
        font-style: normal;
        text-decoration: none;
        color: #F2F2F2;
        padding: 10px 20px;}
    </style>
    <div class="main_area">
    <h3 class="heading">Accepted Refunded</h3>
    <input type='hidden' name='rma_id' id='rma_id' value='<%$id%>'>
    <div class="issue_form">
        <div class="title_issue_refund">Accepted Refunded</div>
        <div class="form">
            <form action="<%$admin_url%>accept_rma_action" method="post" id="frmAccept" name="frmAccept">
                <input type="hidden" name="rma_id" id="rma_id" value="<%$id%>">
                <div class="form_input">
                    <label class="fleft">Comment to Customer</label>
                    <span class="fleft"><textarea id='tCommentToCustomer' name='tCommentToCustomer'>