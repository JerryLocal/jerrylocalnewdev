<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>
<%$this->css->add_css('admin/style_developer.css')%>		
<%$this->css->css_src()%> 
<%$this->js->add_js('admin/custom/rma_accept_popup.js')%>
<%$this->js->js_src()%>	
<style type="text/css">
    .form_input input[type="button"]{
        background: #333333;
        border: 1px solid #797979;
        border-radius: 4px;
        color: #fff;
        font-size: 14px;
        font-weight: bold;
        font-style: normal;
        text-decoration: none;
        color: #F2F2F2;
        padding: 10px 20px;}
    </style>
    <div class="main_area">
    <h3 class="pop-heading">Accept Request</h3>
    <input type='hidden' name='rma_id' id='rma_id' value='<%$id%>'>
    <div class="issue_form">

        <div class="form">
            <form enctype="multipart/form-data" action="<%$this->config->item('admin_url')%>newrequest/newrequest/accept_add_action" method="post" id="frmAccept" name="frmAccept">
                <input type="hidden" name="rma_id" id="rma_id" value="<%$id%>">
                <div class="form_input">
                    <label class="fleft">Comment to Customer</label>
                    <span class="fleft"><textarea id='tCommentToCustomer' name='tCommentToCustomer'></textarea></span>
                    <div class="clear"></div>
                </div>

                <div class="form_input submit">
                    <input class="cancel-btn" type="button" id="btnCancel" name="btnCancel" value="Cancel"/>
                    <input class="accept-btn" type="button" id="btnAccept" name="btnAccept" value="Accept Request"/>
                    <div class="clear"></div>
                </div>
            </form>
            <div><b>Note: After you update this , Request will be moved to "Accepted" list and customer will get notification.</b></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    Project.modules.accept_rma.init();
</script> 
