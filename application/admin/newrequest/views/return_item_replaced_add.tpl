<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('RETURN_ITEM_REPLACED_RETURN_ITEM_REPLACED')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','RETURN_ITEM_REPLACED_RETURN_ITEM_REPLACED')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display: none;position:inherit;" id="ajax_lang_loader">
            <img src="<%$this->config->item('admin_images_url')%>loaders/circular/020.gif">
        </span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="return_item_replaced" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="return_item_replaced" class="frm-elem-block frm-stand-view">
            <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                <input type="hidden" name="sys_custom_field_2" id="sys_custom_field_2" value="<%$data['sys_custom_field_2']|@htmlentities%>"  class='ignore-valid ' />
                <div class="main-content-block" id="main_content_block">
                    <div style="width:98%" class="frm-block-layout pad-calc-container">
                        <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                            <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('RETURN_ITEM_REPLACED_RETURN_ITEM_REPLACED')%></h4></div>
                            <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                <div class="form-row row-fluid" id="cc_sh_tr_request_img">
                                    <label class="form-label span3">
                                        <%$this->lang->line('RETURN_ITEM_REPLACED_REQUEST_IMG')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <div  class='btn-uploadify frm-size-medium' >
                                            <input type="hidden" value="<%$data['tr_request_img']%>" name="old_tr_request_img" id="old_tr_request_img" />
                                            <input type="hidden" value="<%$data['tr_request_img']%>" name="tr_request_img" id="tr_request_img"  />
                                            <input type="hidden" value="<%$data['tr_request_img']%>" name="temp_tr_request_img" id="temp_tr_request_img"  />
                                            <div id="upload_drop_zone_tr_request_img" class="upload-drop-zone"></div>
                                            <div class="uploader upload-src-zone">
                                                <input type="file" name="uploadify_tr_request_img" id="uploadify_tr_request_img" title="<%$this->lang->line('RETURN_ITEM_REPLACED_REQUEST_IMG')%>" />
                                                <span class="filename" id="preview_tr_request_img"></span>
                                                <span class="action">Choose File</span>
                                            </div>
                                        </div>
                                        <div class='upload-image-btn'>
                                            <%$img_html['tr_request_img']%>
                                        </div>
                                        <span class="input-comment">
                                            <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif,png,jpg,jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 2 MB."><span class="icomoon-icon-help"></span></a>
                                        </span>
                                        <div class='clear upload-progress' id='progress_tr_request_img'>
                                            <div class='upload-progress-bar progress progress-striped active'>
                                                <div class='bar' id='practive_tr_request_img'></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='tr_request_imgErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_tr_request_detail">
                                    <label class="form-label span3">
                                        <%$this->lang->line('RETURN_ITEM_REPLACED_REQUEST_DETAIL')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <textarea placeholder=""  name="tr_request_detail" id="tr_request_detail" title="<%$this->lang->line('RETURN_ITEM_REPLACED_REQUEST_DETAIL')%>"  class='elastic frm-size-medium'  ><%$data['tr_request_detail']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='tr_request_detailErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_tr_request_date">
                                    <label class="form-label span3">
                                        <%$this->lang->line('RETURN_ITEM_REPLACED_REQUEST_DATE')%>
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend   ">
                                        <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['tr_request_date'])%>" placeholder="" name="tr_request_date" id="tr_request_date" title="<%$this->lang->line('RETURN_ITEM_REPLACED_REQUEST_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                        <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='tr_request_dateErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_tr_buyer_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('RETURN_ITEM_REPLACED_BUYER_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['tr_buyer_id']%>
                                        <%$this->dropdown->display("tr_buyer_id","tr_buyer_id","  title='<%$this->lang->line('RETURN_ITEM_REPLACED_BUYER_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'RETURN_ITEM_REPLACED_BUYER_ID')%>'  ", "|||", "", $opt_selected,"tr_buyer_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='tr_buyer_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_tr_seller_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('RETURN_ITEM_REPLACED_SELLER_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['tr_seller_id']%>
                                        <%$this->dropdown->display("tr_seller_id","tr_seller_id","  title='<%$this->lang->line('RETURN_ITEM_REPLACED_SELLER_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'RETURN_ITEM_REPLACED_SELLER_ID')%>'  ", "|||", "", $opt_selected,"tr_seller_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='tr_seller_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_tr_store_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('RETURN_ITEM_REPLACED_STORE_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['tr_store_id']%>
                                        <%$this->dropdown->display("tr_store_id","tr_store_id","  title='<%$this->lang->line('RETURN_ITEM_REPLACED_STORE_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'RETURN_ITEM_REPLACED_STORE_ID')%>'  ", "|||", "", $opt_selected,"tr_store_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='tr_store_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_tr_sub_order_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('RETURN_ITEM_REPLACED_SUB_ORDER_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['tr_sub_order_id']%>
                                        <%$this->dropdown->display("tr_sub_order_id","tr_sub_order_id","  title='<%$this->lang->line('RETURN_ITEM_REPLACED_SUB_ORDER_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'RETURN_ITEM_REPLACED_SUB_ORDER_ID')%>'  ", "|||", "", $opt_selected,"tr_sub_order_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='tr_sub_order_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_tr_order_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('RETURN_ITEM_REPLACED_ORDER_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['tr_order_id']%>
                                        <%$this->dropdown->display("tr_order_id","tr_order_id","  title='<%$this->lang->line('RETURN_ITEM_REPLACED_ORDER_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'RETURN_ITEM_REPLACED_ORDER_ID')%>'  ", "|||", "", $opt_selected,"tr_order_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='tr_order_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_tr_rquest_type">
                                    <label class="form-label span3">
                                        <%$this->lang->line('RETURN_ITEM_REPLACED_RQUEST_TYPE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['tr_rquest_type']%>
                                        <%$this->dropdown->display("tr_rquest_type","tr_rquest_type","  title='<%$this->lang->line('RETURN_ITEM_REPLACED_RQUEST_TYPE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'RETURN_ITEM_REPLACED_RQUEST_TYPE')%>'  ", "|||", "", $opt_selected,"tr_rquest_type")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='tr_rquest_typeErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_tr_preference">
                                    <label class="form-label span3">
                                        <%$this->lang->line('RETURN_ITEM_REPLACED_PREFERENCE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['tr_preference']%>
                                        <%$this->dropdown->display("tr_preference","tr_preference","  title='<%$this->lang->line('RETURN_ITEM_REPLACED_PREFERENCE')%>'  aria-chosen-valid='Yes'  class='chosen-select'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'RETURN_ITEM_REPLACED_PREFERENCE')%>'  ", "", "", $opt_selected,"tr_preference")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='tr_preferenceErr'></label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%>">
                        <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                            <%assign var='rm_ctrl_directions' value=true%>
                        <%/if%>
                        <!-- Form Redirection Control Unit -->
                        <%if $controls_allow eq false || $rm_ctrl_directions eq true%>
                            <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" type="hidden" />
                        <%else%>
                            <div class='action-dir-align'>
                                <%if $prev_link_allow eq true%>
                                    <input value="Prev" id="ctrl_flow_prev" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Prev' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_prev">&nbsp;</label>
                                    <label for="ctrl_flow_prev" class="inline-elem-margin"><%$this->lang->line('GENERIC_PREV_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <%if $next_link_allow eq true || $mode eq 'Add'%>
                                    <input value="Next" id="ctrl_flow_next" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Next' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_next">&nbsp;</label>
                                    <label for="ctrl_flow_next" class="inline-elem-margin"><%$this->lang->line('GENERIC_NEXT_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <input value="List" id="ctrl_flow_list" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'List' %> checked=true <%/if%> />
                                <label for="ctrl_flow_list">&nbsp;</label>
                                <label for="ctrl_flow_list" class="inline-elem-margin"><%$this->lang->line('GENERIC_LIST_SHORT')%></label>&nbsp;&nbsp;
                                <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq '' || $ctrl_flow eq 'Stay' %> checked=true <%/if%> />
                                <label for="ctrl_flow_stay">&nbsp;</label>
                                <label for="ctrl_flow_stay" class="inline-elem-margin"><%$this->lang->line('GENERIC_STAY_SHORT')%></label>
                            </div>
                        <%/if%>
                        <!-- Form Action Control Unit -->
                        <%if $controls_allow eq false%>
                            <div class="clear">&nbsp;</div>
                        <%/if%>
                        <div class="action-btn-align" id="action_btn_container">
                            <%if $mode eq 'Update'%>
                                <%if $update_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_UPDATE')%>" name="ctrlupdate" type="submit" id="frmbtn_update" class="btn btn-info"/>&nbsp;&nbsp;
                                <%/if%>
                                <%if $delete_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_DELETE')%>" name="ctrldelete" type="button" id="frmbtn_delete" class="btn btn-danger" onclick="return deleteAdminRecordData('<%$enc_id%>', '<%$mod_enc_url.index%>','<%$mod_enc_url.inline_edit_action%>', '<%$extra_qstr%>', '<%$extra_hstr%>');" />&nbsp;&nbsp;
                                <%/if%>
                            <%else%>
                                <input value="<%$this->lang->line('GENERIC_SAVE')%>" name="ctrladd" type="submit" id="frmbtn_add" class="btn btn-info" />&nbsp;&nbsp;
                            <%/if%>
                            <%if $discard_allow eq true%>
                                <input value="<%$this->lang->line('GENERIC_DISCARD')%>" name="ctrldiscard" type="button" id="frmbtn_discard" class="btn" onclick="return loadAdminModuleListing('<%$mod_enc_url.index%>', '<%$extra_hstr%>')" />
                            <%/if%>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </form>
        </div>
    </div>
</div>
<%javascript%>    
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};        
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        <%foreach name=i from=$auto_arr item=v key=k%>
            if($("#<%$k%>").is("select")){
                $("#<%$k%>").ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                    },{
                    loadingImg: admin_image_url+"chosen-loading.gif"
                });
            }
        <%/foreach%>
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['buttons_arr'] = [];
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/return_item_replaced_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.return_item_replaced.callEvents();
<%/javascript%>