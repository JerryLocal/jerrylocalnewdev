<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>   
<%$this->css->add_css('admin/bootstrap.min.css')%>	
<%$this->css->add_css('admin/style_developer.css')%>	
<%$this->js->add_js('admin/custom/rma_reject_popup.js')%>
<%$this->js->js_src()%>	
<%$this->css->css_src()%> 
<style type="text/css">
    .form_input input[type="button"]{
        background: #333333;
        border: 1px solid #797979;
        border-radius: 4px;
        color: #fff;
        font-size: 14px;
        font-weight: bold;
        font-style: normal;
        text-decoration: none;
        color: #F2F2F2;
        padding: 10px 20px;}
    </style>
    <div class="main_area">
    <h3 class="pop-heading">Reject Request</h3>
    <input type='hidden' name='rma_id' id='rma_id' value='<%$id%>'>
    <div class="issue_form">
        <div class="form">
            <form action="<%$this->config->item('admin_url')%>newrequest/newrequest/reject_add_action" method="post" id="frmReject" name="frmReject">
                <input type="hidden" name="rma_id" id="rma_id" value="<%$id%>">
                <div class="form_input">
                    <label class="fleft">Comment to Customer</label>
                    <span class="fleft"><textarea id='tCommentToCustomer' name='tCommentToCustomer'></textarea></span>
                    <div class="clear"></div>
                </div>

                <div class="form_input submit">
                    <input class="cancel-btn" type="button" id="btnCancel" name="btnCancel" value="Cancel"/>
                    <input class="accept-btn" type="button" id="btnReject" name="btnReject" value="Reject Request"/>
                    <div class="clear"></div>
                </div>
            </form>
        </div>
        <div><b>After you update this, Request will be moved to "Rejected" list and customer will get notification.</b></div>
    </div>
</div>
<script type="text/javascript">
    Project.modules.reject_rma.init();
</script> 