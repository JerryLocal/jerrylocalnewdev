<?php

/**
 * Description of New Requests Extended Model
 * 
 * @module Extended New Requests
 * 
 * @class HBC_Model_newrequest.php
 * 
 * @path application\admin\newrequest\models\HBC_Model_newrequest.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 25.11.2015
 */
   
Class HBC_Model_newrequest extends Model_newrequest {
        function __construct() {
    parent::__construct();
}

function update_rma_details($data = array(), $condition = '') {
    $this->db->where($condition);
    $this->db->update("trn_rma", $data);
}

function insert_rma($data = array()) {
    $success = $this->db->insert("trn_rma_refund", $data);
    return $success;
}

function insert_rma_history($data = array()) {
    $success = $this->db->insert("trn_rma_history", $data);
	
    return $success;
}


function get_rma_details($extracond = "", $field = "*", $orderby = "", $limit = "", $join_arr = '') {
    if ($field == "") {
        $field = "*";
    }
    $this->db->select($field, false);
    $this->db->from("trn_rma as trn");
    $this->db->join('mod_customer as mc', ' trn.iCustomerId = mc.iCustomerId', 'left');
   	$this->db->join('mst_sub_order as mso', 'trn.iSubOrderId = mso.iMstSubOrderId', 'left');
	 $this->db->join('mst_order as mo', 'trn.iSubOrderId = mo.iMstOrderId', 'left');
    if (is_array($join_arr) && count($join_arr) > 0) {
        foreach ($join_arr as $key => $val) {
            $this->db->join($val['table'], $val['condition'], $val['jointype']);
        }
    }
    if ($extracond != "") {
        if (intval($extracond)) {
            $this->db->where("trn.iTrnRmaId", $extracond);
        } else {
            $this->db->where($extracond,null,false);
        }
    }
    if ($orderby != "") {
        $this->db->order_by($orderby);
    }
    if ($limit != "") {
        list($offset, $limit) = @explode(",", $limit);
        $this->db->limit($offset, $limit);
    }
    $list_data = $this->db->get()->result_array();
//	pr($list_data);exit;
    //  echo $this->db->last_query();exit;
    return $list_data;
}
}
