<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<textarea id="txt_module_help" class="perm-elem-hide"></textarea>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line('GENERIC_LISTING')%> :: <%$this->lang->line('NEWREQUEST_NEW_REQUESTS')%>
            </div>        
        </h3>
        <div class="header-right-drops">
        </div>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing box gradient">
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div id="scrollable_content" class="scrollable-content top-list-spacing">
        <div class="grid-data-container pad-calc-container">
            <div class="top-list-tab-layout" id="top_list_grid_layout">
            </div>
            <table class="grid-table-view " width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td id="grid_data_col">
                        <div id="pager2"></div>
                        <table id="list2"></table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>        
<input type="hidden" name="selAllRows" value="" id="selAllRows" />
<%javascript%>
    $.jgrid.no_legacy_api = true; $.jgrid.useJSON = true;
    var el_grid_settings = {}, js_col_model_json = {}, js_col_name_json = {}; 
                    
    el_grid_settings['module_name'] = '<%$module_name%>';
    el_grid_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_grid_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_grid_settings['enc_location'] = '<%$enc_loc_module%>';
    el_grid_settings['par_module '] = '<%$this->general->getAdminEncodeURL($parMod)%>';
    el_grid_settings['par_data'] = '<%$this->general->getAdminEncodeURL($parID)%>';
    el_grid_settings['par_field'] = '<%$parField%>';
    el_grid_settings['par_type'] = 'parent';
    el_grid_settings['add_page_url'] = '<%$mod_enc_url["add"]%>'; 
    el_grid_settings['edit_page_url'] =  admin_url+'<%$mod_enc_url["inline_edit_action"]%>?<%$extra_qstr%>';
    el_grid_settings['listing_url'] = admin_url+'<%$mod_enc_url["listing"]%>?<%$extra_qstr%>';
    el_grid_settings['search_refresh_url'] = admin_url+'<%$mod_enc_url["get_left_search_content"]%>?<%$extra_qstr%>';
    el_grid_settings['search_autocomp_url'] = admin_url+'<%$mod_enc_url["get_search_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['auto_complete_url'] = admin_url+'<%$mod_enc_url["get_token_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['export_url'] =  admin_url+'<%$mod_enc_url["export"]%>?<%$extra_qstr%>';
    el_grid_settings['subgrid_listing_url'] =  admin_url+'<%$mod_enc_url["get_subgrid_block"]%>?<%$extra_qstr%>';
    el_grid_settings['jparent_switchto_url'] = admin_url+'<%$parent_switch_cit["url"]%>?<%$extra_qstr%>';
    
    el_grid_settings['admin_rec_arr'] = {};
    el_grid_settings['status_arr'] = $.parseJSON('<%$status_array|@json_encode%>');
    el_grid_settings['status_lang_arr'] = $.parseJSON('<%$status_array_label|@json_encode%>');
                
    el_grid_settings['hide_add_btn'] = '';
    el_grid_settings['hide_del_btn'] = '';
    el_grid_settings['hide_status_btn'] = '1';
    el_grid_settings['hide_export_btn'] = '1';
    el_grid_settings['hide_columns_btn'] = 'No';
    
    el_grid_settings['hide_advance_search'] = 'No';
    el_grid_settings['hide_search_tool'] = 'No';
    el_grid_settings['hide_multi_select'] = 'No';
    el_grid_settings['popup_add_form'] = 'No';
    el_grid_settings['popup_edit_form'] = 'No';
    el_grid_settings['popup_add_size'] = ['75%', '75%'];
    el_grid_settings['popup_edit_size'] = ['75%', '75%'];
    el_grid_settings['hide_paging_btn'] = 'No';
    el_grid_settings['hide_refresh_btn'] = 'No';
    el_grid_settings['group_search'] = '';
    
    el_grid_settings['permit_add_btn'] = '<%$add_access%>';
    el_grid_settings['permit_del_btn'] = '<%$del_access%>';
    el_grid_settings['permit_edit_btn'] = '<%$view_access%>';
    el_grid_settings['permit_expo_btn'] = '<%$expo_access%>';
    
    el_grid_settings['default_sort'] = 'sys_custom_field_2';
    el_grid_settings['sort_order'] = 'asc';
    
    el_grid_settings['footer_row'] = 'No';
    el_grid_settings['grouping'] = 'No';
    el_grid_settings['group_attr'] = {};
    
    el_grid_settings['inline_add'] = 'No';
    el_grid_settings['rec_position'] = 'Top';
    el_grid_settings['auto_width'] = 'Yes';
    el_grid_settings['subgrid'] = 'Yes';
    el_grid_settings['colgrid'] = 'No';
    el_grid_settings['rating_allow'] = 'No';
    el_grid_settings['listview'] = 'list';
    el_grid_settings['filters_arr'] = $.parseJSON('<%$default_filters|@json_encode%>');
    el_grid_settings['top_filter'] = [];
    el_grid_settings['buttons_arr'] = [{
        "name": "add",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_ADD_NEW')%>",
        "title": "<%$this->lang->line('GENERIC_ADD_NEW')%>",
        "icon": "icomoon-icon-plus-2",
        "icon_only": "No"
    },
    {
        "name": "del",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_DELETE')%>",
        "title": "<%$this->lang->line('GENERIC_DELETE_SELECTED_ROW')%>",
        "icon": "icomoon-icon-remove-6",
        "icon_only": "No"
    },
    {
        "name": "status_pending",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_PENDING')%>",
        "title": "<%$this->lang->line('GENERIC_PENDING')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_accepted",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_ACCEPTED')%>",
        "title": "<%$this->lang->line('GENERIC_ACCEPTED')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_rejected",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_REJECTED')%>",
        "title": "<%$this->lang->line('GENERIC_REJECTED')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_received",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_MARK_AS_RECEIVED')%>",
        "title": "<%$this->lang->line('GENERIC_MARK_AS_RECEIVED')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_refunded",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_REFUNDED')%>",
        "title": "<%$this->lang->line('GENERIC_REFUNDED')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_replaced",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_MARKED_AS_REPLACED')%>",
        "title": "<%$this->lang->line('GENERIC_MARKED_AS_REPLACED')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_close",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_CLOSE')%>",
        "title": "<%$this->lang->line('GENERIC_CLOSE')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "custom_btn_1",
        "type": "custom",
        "text": "<%$this->lang->line('GENERIC_ACCEPT')%>",
        "title": "<%$this->lang->line('GENERIC_ACCEPT')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No",
        "alert": {
            "title": "<%$this->lang->line('GENERIC_ALERT')%>",
            "body": "<%$this->lang->line('GENERIC_PLEASE_SELECT_AT_LEAST_ONE_RECORD_C63')%>",
            "button": ["<%$this->lang->line('GENERIC_OK')%>"],
            "width": "300",
            "height": "150"
        },
        "confirm": {
            "type": "redirect",
            "module": "<%$this->general->getAdminEncodeURL('newrequest\/newrequest')%>",
            "url": "<%$this->config->item('admin_url')%>#newrequest\/newrequest\/accept_add|width|730|height|288|autoSize|false",
            "id": "id",
            "open": "popup",
            "width": "730",
            "height": "288",
            "params": []
        }
    },
    {
        "name": "custom_btn_2",
        "type": "custom",
        "text": "<%$this->lang->line('GENERIC_REJECT')%>",
        "title": "<%$this->lang->line('GENERIC_REJECT')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No",
        "alert": {
            "title": "<%$this->lang->line('GENERIC_ALERT')%>",
            "body": "<%$this->lang->line('GENERIC_PLEASE_SELECT_AT_LEAST_ONE_RECORD_C63')%>",
            "button": ["<%$this->lang->line('GENERIC_OK')%>"],
            "width": "300",
            "height": "150"
        },
        "confirm": {
            "type": "redirect",
            "module": "<%$this->general->getAdminEncodeURL('newrequest\/newrequest')%>",
            "url": "<%$this->config->item('admin_url')%>#newrequest\/newrequest\/reject_add|width|730|height|288|autoSize|false",
            "id": "id",
            "open": "popup",
            "width": "730",
            "height": "288",
            "params": []
        }
    },
    {
        "name": "search",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_SEARCH')%>",
        "title": "<%$this->lang->line('GENERIC_ADVANCE_SEARCH')%>",
        "icon": "icomoon-icon-search-3",
        "icon_only": "No"
    },
    {
        "name": "refresh",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_SHOW_ALL')%>",
        "title": "<%$this->lang->line('GENERIC_SHOW_ALL_LISTING_RECORDS')%>",
        "icon": "icomoon-icon-loop-2",
        "icon_only": "No"
    },
    {
        "name": "columns",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_COLUMNS')%>",
        "title": "<%$this->lang->line('GENERIC_HIDE_C47SHOW_COLUMNS')%>",
        "icon": "silk-icon-columns",
        "icon_only": "No"
    },
    {
        "name": "export",
        "type": "default",
        "text": "<%$this->lang->line('GENERIC_EXPORT')%>",
        "title": "<%$this->lang->line('GENERIC_EXPORT')%>",
        "icon": "icomoon-icon-out",
        "icon_only": "No"
    }];
    
    js_col_name_json = [{
        "name": "sys_custom_field_2",
        "label": "<%$this->lang->line('NEWREQUEST_RETURN_ID')%>"
    },
    {
        "name": "tr_order_id",
        "label": "<%$this->lang->line('NEWREQUEST_ORDER_NO')%>"
    },
    {
        "name": "tr_preference",
        "label": "<%$this->lang->line('NEWREQUEST_PREFERENCE')%>"
    },
    {
        "name": "tr_sub_order_id",
        "label": "<%$this->lang->line('NEWREQUEST_SUB_ORDER_NO')%>"
    },
    {
        "name": "mo_buyer_name",
        "label": "<%$this->lang->line('NEWREQUEST_CUSTOMER_NAME')%>"
    },
    {
        "name": "msd_store_name",
        "label": "<%$this->lang->line('NEWREQUEST_STORE_NAME')%>"
    },
    {
        "name": "tr_request_date",
        "label": "<%$this->lang->line('NEWREQUEST_REQUEST_DATE')%>"
    },
    {
        "name": "mso_total_cost",
        "label": "<%$this->lang->line('NEWREQUEST_ORDER_TOTAL')%>"
    }];
    
    js_col_model_json = [{
        "name": "sys_custom_field_2",
        "index": "sys_custom_field_2",
        "label": "<%$list_config['sys_custom_field_2']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['sys_custom_field_2']['width']%>",
        "search": <%if $list_config['sys_custom_field_2']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['sys_custom_field_2']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['sys_custom_field_2']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['sys_custom_field_2']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['sys_custom_field_2']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "newrequest",
                "aria-unique-name": "sys_custom_field_2",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['sys_custom_field_2']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "newrequest",
            "aria-unique-name": "sys_custom_field_2",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['sys_custom_field_2']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "tr_order_id",
        "index": "tr_order_id",
        "label": "<%$list_config['tr_order_id']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['tr_order_id']['width']%>",
        "search": <%if $list_config['tr_order_id']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['tr_order_id']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['tr_order_id']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['tr_order_id']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['tr_order_id']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "newrequest",
                "aria-unique-name": "tr_order_id",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['tr_order_id']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "newrequest",
            "aria-unique-name": "tr_order_id",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['tr_order_id']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "tr_preference",
        "index": "tr_preference",
        "label": "<%$list_config['tr_preference']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['tr_preference']['width']%>",
        "search": <%if $list_config['tr_preference']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['tr_preference']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['tr_preference']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['tr_preference']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['tr_preference']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "newrequest",
                "aria-unique-name": "tr_preference",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['tr_preference']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["tr_preference"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=tr_preference&mode=<%$mod_enc_mode["Search"]%>&rformat=html'<%/if%>,
            "value": <%if $count_arr["tr_preference"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["tr_preference"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['tr_preference']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["tr_preference"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "newrequest",
            "aria-unique-name": "tr_preference",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=tr_preference&mode=<%$mod_enc_mode["Update"]%>&rformat=html',
            "dataInit": <%if $count_arr['tr_preference']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["tr_preference"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'NEWREQUEST_PREFERENCE')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['tr_preference']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "tr_sub_order_id",
        "index": "tr_sub_order_id",
        "label": "<%$list_config['tr_sub_order_id']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['tr_sub_order_id']['width']%>",
        "search": <%if $list_config['tr_sub_order_id']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['tr_sub_order_id']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['tr_sub_order_id']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['tr_sub_order_id']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['tr_sub_order_id']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "newrequest",
                "aria-unique-name": "tr_sub_order_id",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['tr_sub_order_id']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "newrequest",
            "aria-unique-name": "tr_sub_order_id",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['tr_sub_order_id']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mo_buyer_name",
        "index": "mo_buyer_name",
        "label": "<%$list_config['mo_buyer_name']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['mo_buyer_name']['width']%>",
        "search": <%if $list_config['mo_buyer_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mo_buyer_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mo_buyer_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mo_buyer_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mo_buyer_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "newrequest",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mo_buyer_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "newrequest",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mo_buyer_name']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "msd_store_name",
        "index": "msd_store_name",
        "label": "<%$list_config['msd_store_name']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['msd_store_name']['width']%>",
        "search": <%if $list_config['msd_store_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['msd_store_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['msd_store_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['msd_store_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['msd_store_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "newrequest",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['msd_store_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "newrequest",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['msd_store_name']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "tr_request_date",
        "index": "tr_request_date",
        "label": "<%$list_config['tr_request_date']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['tr_request_date']['width']%>",
        "search": <%if $list_config['tr_request_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['tr_request_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['tr_request_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['tr_request_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['tr_request_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "newrequest",
                "aria-unique-name": "tr_request_date",
                "autocomplete": "off",
                "class": "search-inline-date",
                "aria-date-format": "<%$this->general->getAdminJSMoments('date')%>"
            },
            "sopt": dateSearchOpts,
            "searchhidden": <%if $list_config['tr_request_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchGridDateRangePicker
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "newrequest",
            "aria-unique-name": "tr_request_date",
            "aria-date-format": "<%$this->general->getAdminJSFormats('date', 'dateFormat')%>",
            "aria-min": "",
            "aria-max": "",
            "placeholder": "",
            "class": "inline-edit-row inline-date-edit date-picker-icon dateOnly"
        },
        "ctrl_type": "date",
        "default_value": "<%$list_config['tr_request_date']['default']%>",
        "filterSopt": "bt"
    },
    {
        "name": "mso_total_cost",
        "index": "mso_total_cost",
        "label": "<%$list_config['mso_total_cost']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['mso_total_cost']['width']%>",
        "search": <%if $list_config['mso_total_cost']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mso_total_cost']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mso_total_cost']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mso_total_cost']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mso_total_cost']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "newrequest",
                "aria-unique-name": "mso_total_cost",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['mso_total_cost']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "newrequest",
            "aria-unique-name": "mso_total_cost",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mso_total_cost']['default']%>",
        "filterSopt": "bw",
        "formatter": "currency"
    }];
         
    initMainGridListing();
    createTooltipHeading();
    callSwitchToParent();
<%/javascript%>
<%$this->js->add_js("admin/custom/new_request_extended.js")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 