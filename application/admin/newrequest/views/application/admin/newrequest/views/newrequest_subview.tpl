<%javascript%>
    $.fn.editable.defaults.mode = 'inline', $.fn.editable.defaults.clear = false;
    var el_subview_settings = {}, view_js_col_model_json = {}, view_token_input_assign = {}, view_token_pre_populates = {};
      
    el_subview_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_subview_settings['extra_qstr'] = '<%$extra_qstr%>';
                    
    el_subview_settings['view_id'] = '<%$subgrid_view_id%>';
    el_subview_settings['edit_id'] = '<%$enc_view_primary_id%>';
    el_subview_settings['module_name'] = '<%$module_name%>';
            
    el_subview_settings['edit_page_url'] = admin_url+'<%$mod_enc_url["inline_edit_action"]%>?&oper=edit&<%$extra_qstr%>';
    el_subview_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_subview_settings['permit_edit_btn'] = '<%$edit_access%>';

    view_js_col_model_json =  {};
    
    initSubGridDetailView();
<%/javascript%>
    
<div class="expand-detail-view">                        
    <table id="detail_view_block" class="jqgrid-subview" width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_SKU')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_product_sku']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_QTY')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_product_qty']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_NAME')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mo_buyer_name']%></td>
        </tr>
        <tr>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_PRODUCT_NAME')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_product_name']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_PRICE')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_product_price']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_PHONE')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mo_buyer_phone']%></td>
        </tr>
        <tr>
            <td valign="top" width="12%"><strong>Sub Order No: </strong></td>
            <td valign="top" width="18%"><%$data['mso_mst_sub_order_id']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_SHIPPING')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_shipping_cost']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_EMAIL')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mo_buyer_email']%></td>
        </tr>
        <tr>
            <td valign="top" width="12%"><strong>Order No: </strong></td>
            <td valign="top" width="18%"><%$data['mo_mst_order_id']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_TOTAL')%>: </strong></td>
            <td valign="top" width="18%"><%$data['mso_total_cost']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_BUYER_ADDRESS')%>: </strong></td>
            <td valign="top" width="18%"><%$data['sys_custom_field_2']%></td>
        </tr>
        <tr>  
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_REASON')%>: </strong></td>
            <td valign="top" width="18%"><%$data['tr_request_detail']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="12%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_REQUEST_IMAGE')%>: </strong></td>
            <td valign="top" width="18%"><%$data['tr_request_img']%></td>
        </tr>
    </table>
    <br/>
    <b>&nbsp;&nbsp;Shipping and invoice details:-</b>
    <br/>
    <table id="detail_view_block" class="jqgrid-subview" width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_INVOICE_NO')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_seller_invoice_no']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_COURIER_COMPANY')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_shipper_name']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_INVOICE_DATE')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_seller_invoice_date']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_TRACKING_NO')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_tracking_number']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong>Remarks: </strong></td>
            <td valign="top" width="27%"><%$data['mso_shipping_remark']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"><strong>Shipped Date: </strong></td>
            <td valign="top" width="27%"><%$data['mso_shipping_date']%></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><%$this->lang->line('GETRETURNEXPANDEDVIEW_DELIVERED_DATE')%>: </strong></td>
            <td valign="top" width="27%"><%$data['mso_delivered_date']%></td>
            <td valign="top" width="2%">&nbsp; </td>
            <td valign="top" width="20%"></strong></td>
            <td valign="top" width="27%"></td> 
        </tr>
    </table>
</div>