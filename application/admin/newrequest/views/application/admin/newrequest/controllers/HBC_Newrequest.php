<?php
            
/**
 * Description of New Requests Extended Controller
 * 
 * @module Extended New Requests
 * 
 * @class HBC_Newrequest.php
 * 
 * @path application\admin\newrequest\controllers\HBC_Newrequest.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 22.12.2015
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
 
Class HBC_Newrequest extends Newrequest {
        function __construct() {
    parent::__construct();
    $this->load->model("model_newrequest");
}

function issue_refund() {
    $render_arr = $this->input->get();
   // pr($render_arr);exit;
    $rma_id = intval($render_arr['id']);
    if ($rma_id) {
        $fields = "trn.iSubOrderId as sub_order_id,mso.vProductName,CONCAT_WS(' ',mso.vSubOrderNumberPre,mso.iSubOrderNumber) as sub_order_no,mso.fTotalCost,mso.vSellerInvoiceNo,trn.iTrnRmaId,CONCAT_WS(' ',mc.vFirstName,mc.vLastName) as buyer_name,CONCAT_WS(' ',mo.vOrderNumberPre,mo.iOrderNumber) as order_number,trn.tRequestDetail";
        $rmaDetailsArr = $this->model_newrequest->get_rma_details($rma_id, $fields);
        $rmaDetailsArr = $rmaDetailsArr[0];
    }
  // pr($rmaDetailsArr);exit;
    $this->smarty->assign("rmaDetailsArr", $rmaDetailsArr);
    echo $this->parser->parse("newrequest_hbc_refund_popup_view", $render_arr, true);
    $this->skip_template_view();
    //  exit;
}

function accept_refund_action() {
    $postVar = $this->input->get_post();
    $rma_id = intval($postVar['rma_id']);
    if ($rma_id > 0) {
        $update_rma_details = array();
        $update_rma_details['eRquestType'] = 'Refunded';
        $where = "iTrnRmaId = '" . $rma_id . "'";
        $res = $this->model_newrequest->update_rma_details($update_rma_details, $where);

        $add_rma_refund_arr = array();
        $add_rma_refund_arr['iTrnRmaId'] = $rma_id;
        $add_rma_refund_arr['iMstSubOrderId'] = $postVar['iMstSubOrderId'];
        $add_rma_refund_arr['tReason'] = $postVar['tReason'];
        $add_rma_refund_arr['fShipping'] = floatval($postVar['fShipping']);
        $add_rma_refund_arr['fRefund'] = floatval($postVar['fRefund']);
        $add_rma_refund_arr['tCommentToCustomer'] = $postVar['tCommentToCustomer'];
        // $add_rma_refund_arr['dRefundDate'] = date('Y-m-d H:i:s');
        $add_rma_refund_arr['dDate'] = date('Y-m-d H:i:s');
        $add_rma_refund_arr['fTotalAmount'] = floatval($postVar['fRefund']);
        $add_rma_refund_arr['eStatus'] = 'Unpaid';
        $add_rma_refund_arr['eType'] = 'Return';
        $status = $this->model_newrequest->insert_rma($add_rma_refund_arr);

        if ($status) {
            $add_rma_hist_arr = array();
            $add_rma_hist_arr['iTrnRmaId'] = $rma_id;
            $add_rma_hist_arr['dActionDate'] = date('Y-m-d H:i:s');
            $add_rma_hist_arr['tActionDetail'] = $postVar['tCommentToCustomer'];
            $add_rma_hist_arr['eActionType'] = 'Refunded';
            if ($this->session->userdata("iGroupId") == 3) {
                $action_taken_by = 'Seller';
            } else {
                $action_taken_by = 'Admin';
            }
            $add_rma_hist_arr['eActionTakenBy'] = $action_taken_by;
            $status = $this->model_newrequest->insert_rma_history($add_rma_hist_arr);
            $this->session->set_flashdata('success', 'Refund issued Successfully.');
        } else {
            $this->session->set_flashdata('failure', 'Failured occured while issuing refund.');
        }
    } else {
        $this->session->set_flashdata('failure', 'Failured occured while issuing refund.');
    }
    $admin_url = $this->config->item("admin_url") . "returnreplaced/return_item_replaced/index";
    redirect($admin_url);
}

function accept_add() {
    $render_arr = $this->input->get();
    $this->loadView("newrequest_hbc_accept_popup_view", $render_arr);
    //  echo $this->parser->parse("newrequest_hbc_accept_popup_view", $render_arr, true);
    // $this->skip_template_view();
}

function accept_add_action($input_data) {
    $return_arr = array();
    $postVar = $this->input->post();
    $rma_id = intval($postVar['rma_id']);
    if ($rma_id > 0) {
        $update_rma_details = array();
        $update_rma_details['eRquestType'] = 'Accepted';
        $where = "iTrnRmaId = '" . $rma_id . "'";
        $this->model_newrequest->update_rma_details($update_rma_details, $where);
        $add_rma_hist_arr = array();
        $add_rma_hist_arr['iTrnRmaId'] = $rma_id;
        $add_rma_hist_arr['dActionDate'] = date('Y-m-d H:i:s');
        $add_rma_hist_arr['tActionDetail'] = $postVar['tCommentToCustomer'];
        $add_rma_hist_arr['eActionType'] = 'Accepted';
        if ($this->session->userdata("iGroupId") == 3) {
            $action_taken_by = 'Seller';
        } else {
            $action_taken_by = 'Admin';
        }
        $add_rma_hist_arr['eActionTakenBy'] = $action_taken_by;
        $status = $this->model_newrequest->insert_rma_history($add_rma_hist_arr);
        if ($status) {
            $return_arr['success'] = "1";
            $return_arr['message'] = "Rma Accepted Successfully.";
            $this->session->set_flashdata('success', 'Rma Accepted Successfully.');
        } else {
            $return_arr['success'] = "0";
            $return_arr['message'] = "Failured occured while Accepting the rma.";
            $this->session->set_flashdata('failure', 'Failured occured while Accepting the rma.');
        }
    } else {
        $return_arr['success'] = "0";
        $return_arr['message'] = "Failured occured while Accepting the rma.";
        $this->session->set_flashdata('failure', 'Failured occured while Accepting the rma.');
    }
    echo json_encode($return_arr);
    exit;
}

function reject_add() {
    $render_arr = $this->input->get();
    $this->loadView("newrequest_hbc_reject_popup_view", $render_arr);
    //  echo $this->parser->parse("newrequest_hbc_reject_popup_view", $render_arr, true);
    //  $this->skip_template_view();
}

function reject_add_action() {
    $return_arr = array();
    $postVar = $this->input->get_post();
    $rma_id = intval($postVar['rma_id']);
    if ($rma_id > 0) {
        $update_rma_details = array();
        $update_rma_details['eRquestType'] = 'Rejected';
        $where = "iTrnRmaId = '" . $rma_id . "'";
        $this->model_newrequest->update_rma_details($update_rma_details, $where);

        $add_rma_hist_arr = array();
        $add_rma_hist_arr['iTrnRmaId'] = $rma_id;
        $add_rma_hist_arr['dActionDate'] = date('Y-m-d H:i:s');
        $add_rma_hist_arr['tActionDetail'] = $postVar['tCommentToCustomer'];
        $add_rma_hist_arr['eActionType'] = 'Rejected';
        if ($this->session->userdata("iGroupId") == 3) {
            $action_taken_by = 'Seller';
        } else {
            $action_taken_by = 'Admin';
        }
        $add_rma_hist_arr['eActionTakenBy'] = $action_taken_by;
        $status = $this->model_newrequest->insert_rma_history($add_rma_hist_arr);
        if ($status) {
            $return_arr['success'] = "1";
            $return_arr['message'] = "Rma Rejected Successfully.";
            $this->session->set_flashdata('success', 'Rma Rejected Successfully.');
        } else {
            $return_arr['success'] = "0";
            $return_arr['message'] = "Failured occured while Rejected the rma.";
            $this->session->set_flashdata('failure', 'Failured occured while Rejected the rma.');
        }
    } else {
        $return_arr['success'] = "0";
        $return_arr['message'] = "Failured occured while Rejected the rma.";
        $this->session->set_flashdata('failure', 'Failured occured while Rejected the rma.');
    }
    echo json_encode($return_arr);
    exit;
}
}
