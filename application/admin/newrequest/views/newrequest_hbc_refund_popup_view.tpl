<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>    
<%$this->css->add_css('admin/style_developer.css')%>        
<%$this->css->css_src()%> 
<style type="text/css">
    .form_input label{line-height:12px;}    
    .form_input input[type="button"]{
        background: #333333;
        border: 1px solid #797979;
        border-radius: 4px;
        color: #fff;
        font-size: 14px;
        font-weight: bold;
        font-style: normal;
        text-decoration: none;
        color: #F2F2F2;
        padding: 10px 20px;}
    .error{color:#ff7878 !important;}
</style>
<div class="main_area">
    <h3 class="pop-heading">Refund Request</h3>

    <div class="issue_form">
        <div class="form">
            <form action="<%$admin_url%>newrequest/newrequest/accept_refund_action" method="post" id="frmRefund" name="frmRefund">
                <input type='hidden' name='rma_id' id='rma_id' value="<%$rmaDetailsArr['iTrnRmaId']%>">
                <input type='hidden' name='iMstSubOrderId' id='iMstSubOrderId' value="<%$rmaDetailsArr['sub_order_id']%>">
                <%*ssign var=order_total value=$this->general->getOrderTotal($id)*%>
                <div class="form_input">
                    <label class="fleft"><strong>Refund No: <%$this->general->get_admin_rma_number($rmaDetailsArr['iTrnRmaId'])%></strong></label>
                    <label class="fright"><strong>Customer Name:</strong> <%$rmaDetailsArr['buyer_name']%></label>
                    <div class="clear"></div>
                </div>

                <div class="form_input">
                    <label class="fleft"><strong>Product Name:</strong> <%$rmaDetailsArr['vProductName']%></label>
                    <label class="fright" ><strong>Invoice Number:</strong> <%$rmaDetailsArr['vSellerInvoiceNo']%></label>
                    <div class="clear"></div>
                </div>

                <div class="form_input">
                    <label class="fleft"><strong>Order Total:</strong> <%$this->general->get_formated_currency_details($rmaDetailsArr['fTotalCost'])%></label>
                    <label class="fright"><strong>Sub Order Number :</strong> <%$rmaDetailsArr['sub_order_no']%></label>
                    <div class="clear"></div>
                </div>

                <div class="form_input">
                    <label class="fleft"><strong>Reason for Refund :</strong> <%$rmaDetailsArr['tRequestDetail']%></label>
                    <div class="clear"></div>
                </div>


                <div class="form_input">
                    <label class="fleft"><strong>Refund Amount Original :</strong> <%$this->general->get_formated_currency_details($rmaDetailsArr['fTotalCost'])%></label>
                </div>
                <div class="clear" style="margin-bottom:12px;"></div>
                <!--
                <div class="form_input">
                    <label class="fleft">Shipping Consession</label>
                    <span class="fleft"><input type="text" id="fShipping" name="fShipping"/></span>
                    <div class="clear"></div>
                </div> -->
                <input type='hidden' id='min_amount' name='min_amount' value="<%$rmaDetailsArr['fTotalCost']%>" />

                <div class="form_input">
                    <label id="lbl_fRefund"  class="fleft">Refund Amount (<%$this->config->item("ADMIN_CURRENCY_PREFIX")%>)<em style="color:#d9534f"> *</em> </label>
                    <span class="fleft"><input type="text" id="fRefund" name="fRefund" value="<%$rmaDetailsArr['fTotalCost']%>" /></span>
                    <div class="error-msg-form" style="padding-left: 49px;"><label class='error' id='fRefundErr' style="width:303px;"></label></div>
                    <div class="clear"></div>
                </div>
                <!--  <div class="form_input">
                      <label id="lbl_fTotalAmount"  class="fleft">Total Refund Amount (<%$this->config->item("ADMIN_CURRENCY_PREFIX")%>) <em style="color:#d9534f"> *</em></label>
                      <span class="fleft"><input readonly="true" type="text"  id="fTotalAmount" name="fTotalAmount" value="<%$rmaDetailsArr['fTotalCost']%>"/></span>
                      <div class="error-msg-form" style="padding-left: 49px;width: 440px;"><label class='error' id='fTotalAmountErr'></label></div>
                      <div class="clear"></div>
                  </div> -->
                <div class="clear"></div>
                <div class="form_input">
                    <label class="fleft">Comment to Customer</label>
                    <span class="fleft"><textarea id='tCommentToCustomer' name='tCommentToCustomer'></textarea></span>
                    <div class="clear"></div>
                </div>

                <div class="form_input submit">
                    <input class="cancel-btn" type="button" id="btnCancel" name="btnCancel" value="Cancel"/>
                    <input class="accept-btn" type="button" id="btnRefund" name="btnRefund" value="Confirm Refund"/>
                    <div class="clear"></div>
                </div>
            </form>
            <div><b>After you update refund amount , Jerry local admin will get notification to process refund and request will be moved to "Pending Refund" list.</b></div>
        </div>
    </div>
</div>
<script type="text/javascript">

    Project.modules.main_area = {
        init: function () {
            this.performcomputation();
            this.validate();
        },
        performcomputation: function () {

            $('input[type=text]').keypress(function (event) {
                var unicode = event.charCode ? event.charCode : event.keyCode;
                if (unicode == 13 && $("#frmRefund").valid()) {
                    $('#frmRefund').submit();
                }
            });

            $.validator.addMethod("lessThanEqualTo", function (value, element) {
                var total_amount = parseFloat($("#min_amount").val());
                var total_refund = parseFloat($("#fRefund").val());
                if (total_amount < total_refund) {
                    return false;
                } else {
                    return true;
                }
            }, "Refund amount cannot exceed refund original amount.");
            /*
             $("#fTotalAmount").on("keydown", function () {
             $("#lbl_fTotalAmount").css("color", "#666666");
             });
             $("#fRefund").on("keydown", function () {
             $("#lbl_fRefund").css("color", "#666666");
             });
             */
            $("#btnRefund").on('click', function () {
                if ($("#frmRefund").valid()) {
                    $('#frmRefund').submit();
                }
            });
            $("#btnCancel").on('click', function () {
                parent.$.fancybox.close();
            });
            /*  $("#fShipping,#fRefund").keyup(function () {
             var refundamount = parseFloat($("#fRefund").val());
             var shipamount = parseFloat($("#fShipping").val());
             if (isNaN(refundamount)) {
             refundamount = 0;
             }
             if (isNaN(shipamount)) {
             shipamount = 0;
             }
             
             var final_refund = parseFloat(refundamount + shipamount);
             if (isNaN(final_refund)) {
             final_refund = 0;
             }
             $("#fTotalAmount").val(final_refund);
             });
             $("#fShipping,#fRefund").keydown(function (e) {
             // Allow: backspace, delete, tab, escape, enter and .
             if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
             (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
             (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
             (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
             (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
             }
             // Ensure that it is a number and stop the keypress
             if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
             e.preventDefault();
             }
             });*/
        },
        validate: function () {
            $("#frmRefund").validate({
                onfocusout: false,
                ignore: ".ignore-valid, .ignore-show-hide",
                rules: {
                    "fRefund": {
                        "required": true,
                        "number": true,
                        "lessThanEqualTo": true
                    }
                },
                messages: {
                    "fRefund": {
                        "required": "Please enter a value for the refund amount field.",
                        "number": "Please enter valid number for the refund amount field.",
                    }
                },
                errorPlacement: function (error, element) {
                    switch (element.attr("name")) {
                        case 'fRefund':
                            //  $('#' + element.attr('id')).focus();
                            $('#lbl_' + element.attr('id')).css("color", "#ff7878");
                            // $('#' + element.attr('id') + 'Err').css("padding-left", "49px");
                            // $('#' + element.attr('id') + 'Err').css("width", "620px");
                            $('#' + element.attr('id') + 'Err').html(error);
                            break;
                        default:
                            printErrorMessage(element, valid_more_elements, error);
                            break;
                    }
                }
            });
        }
    };
    Project.modules.main_area.init();
</script> 

