<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
<%$this->js->clean_js()%>
<%/if%>

<%$this->js->add_js('jquery-1.9.1.min.js')%>
<%$this->js->add_js('admin/bootstrap.min.js')%>
<%$this->js->js_src()%>
<%$this->css->add_css('admin/bootstrap.min.css')%>	
<%$this->css->add_css('admin/style_developer.css')%>		
<%$this->css->css_src()%> 
<style type="text/css">
    h1.invoice_heading{padding-bottom:28px !important;}
    table.invoice_seller_buyer th {background:none;}
    bottom:0px;}
</style>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" style="border: none; border-bottom: solid 1px #c1c1c1; margin: 0 1%; width: 98% !important; height: auto;" id="top_heading_fix"> <!--<h3 style="margin-top:0px;">Transaction :: Invoice </h3>-->
        <h3 style="padding: 5px 0; margin: 0; text-align:right; width:100%; line-height: 40px; float: left; font-size:24px;">TAX INVOICE</h3>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" style="margin-top: 50px;">
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="shippedorderlisting" class="frm-view-block frm-stand-view">
            <div class="container" style="width: 97%; box-sizing: border-box; margin: 0 auto;">
                <!--<h1 align="center" class="invoice_heading">TAX INVOICE</h1>-->
                <div class="row">
                    <table width="100%" border="0" class="invoice_seller_buyer" style="font-size: 14px; line-height: 22px;">
                        <tr>
                            <td scope="col" width="30%" style="text-align:left; vertical-align: top;">
                                <table width="100%">
                                    <tr>
                                        <td><strong>JerryLocal</strong></td>
                                    </tr>
                                    <tr>
                                        <td><%$admin_address%></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Email : </strong><a href="mailto:<%$company_support_email%>"><%$company_support_email%></a></td>
                                    </tr>
                                    <tr><td><strong>GST No : </strong><%$fax_no%></td></tr>
                                    <tr>
                                        <td width="50%"><strong>Invoice No : </strong><%$settelment_no%></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Invoice Date : </strong><%$trans_date%></td>
                                    </tr>
                                </table>
                            </td>
                            <td scope="col" width="30%" style="text-align:left; vertical-align: top;">
                                <table width="100%">
                                    <tr>
                                        <td style="text-align:left; font-size:16px; line-height:24px;"><strong>Bill To:</strong></td>
                                    </tr>
                                    <!--<tr>
                                        <td style="text-align:left;"><strong><%$store_name%></strong></td>
                                    </tr>-->
                                    <tr>
                                        <td style="text-align:left;"><strong><%$store_company_name%></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;"><%$store_address%></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;"><strong>Email : </strong><a href="mailto:<%$seller_email%>"><%$seller_email%></a></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-bottom:10px; border-top:solid 1px #c1c1c1;"></td>
                        </tr>

                        <tr><td><strong style="font-size:18px; line-height:26px;">Sub :- JerryLocal market place service fees	</strong></td></tr>
                        <tr>
                            <td><strong>Settelment Referance</strong></td>
                            <td style="text-align:right;"><%$trans_ref%></td>
                        </tr>
                        <tr>
                            <td><strong>Settelment Date</strong></td>
                            <td style="text-align:right;"><%$trans_date%></td>
                        </tr>

                        <tr>
                            <td><strong>Total Order Values</strong></td>
                            <td style="text-align:right;"><%$this->general->get_formated_currency_details($total_amount)%></td>
                        </tr>
                        
                        <tr>
                            <td><strong>Transaction Fees</strong></td>
                            <td style="text-align:right;"><%$this->general->get_formated_currency_details($total_gateway_tdr)%></td>
                        </tr>

                        <tr>
                            <td><strong>Commissions</strong></td>
                            <td style="text-align:right;"><%$this->general->get_formated_currency_details($total_commission)%></td>
                        </tr>

                        <tr>
                            <td><strong>Total Commissions (Inc GST)</strong></td>
                            <td style="text-align:right;"><%$this->general->get_formated_currency_details($total_commission+$total_gateway_tdr)%></td>
                        </tr>

                        <tr>
                            <td><strong>Settled Values (Amount Credit)</strong></td>
                            <td style="text-align:right;"><%$this->general->get_formated_currency_details($trans_total)%></td>
                        </tr>

                        <tr>
                            <td style="margin-bottom:20px;padding-bottom:20px"></td>
                        </tr>
                        <tr>
                            <td><strong>Remark: </strong><p><%$trans_text%></p></td>
                            <td style="text-align:left;">&nbsp;</td>
                        </tr>
                    </table>

                    <!--<div class="row" style="margin: 0 auto;">
                        <div class="prise">
                            <h2 style="font-size:14px;"> Note:Prices includes G.S.T as rate shown On Fees/Concession and Payment Gateway Charges.</h2>
                        </div>
                    </div>-->


                </div>
            </div>
        </div>        
    </div>
</div>
