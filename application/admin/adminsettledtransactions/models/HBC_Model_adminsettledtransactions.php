<?php

/**
 * Description of Admin Settled Transactions Extended Model
 * 
 * @module Extended Admin Settled Transactions
 * 
 * @class HBC_Model_adminsettledtransactions.php
 * 
 * @path application\admin\adminsettledtransactions\models\HBC_Model_adminsettledtransactions.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 01.12.2015
 */
   
Class HBC_Model_adminsettledtransactions extends Model_adminsettledtransactions {
        function __construct() {
    parent::__construct();
}

function getSettelmentInvoiceDetails($extracond = "", $field = "*", $orderby = "", $limit = "", $join_arr = '') {

    if ($field == "") {
        $field = "setle.vTransactionCode as trans_ref,setle.tTransactionDetail as  trans_text,setle.dDate as trans_date,setle.fSettlementTotal as trans_total,SUM(msb.fAdminCommision) as total_commission,SUM(msb.fTotalCost) as total_amount,SUM(msb.fGatewayTDR) as total_gateway_tdr,msd.vStoreName as store_name,msd.vCompanyName as store_company_name,ma.vPhonenumber as seller_contact_number,ma.vEmail as seller_email,mcc1.vCity as seller_city,ms1.vState as seller_state,mc1.vCountry as seller_country,msd.vArea as seller_area,msd.vAddress1 as seller_address1,msd.vAddress2 as seller_address2,msd.vPinCode as seller_pin_code";
    }
    $this->db->select($field, false);
    $this->db->from("mst_sub_order as msb");
    $this->db->join('mst_store_detail as msd', 'msd.iMstStoreDetailId = msb.iMstStoreDetailId');
    $this->db->join('mod_state as ms1', 'ms1.iStateId = msd.iStateId', 'left');
    $this->db->join('mod_country as mc1', 'mc1.iCountryId = msd.iCountryId', 'left');
    $this->db->join('mod_city as mcc1', 'mcc1.iCityId = msd.iCityId', 'left');
    $this->db->join('mod_admin as ma', 'ma.iAdminId = msd.iAdminId', 'left');
    $this->db->join('trn_settlement as setle', 'setle.iTrnSettlementId = msb.iTrnSettlementId', 'left');
    if (is_array($join_arr) && count($join_arr) > 0) {
        foreach ($join_arr as $key => $val) {
            $this->db->join($val['table'], $val['condition'], $val['jointype']);
        }
    }
    if ($extracond != "") {
        if (intval($extracond)) {
            $this->db->where("msb.iTrnSettlementId", $extracond);
        } else {
            $this->db->where($extracond);
        }
    }
    if ($orderby != "") {
        $this->db->order_by($orderby);
    }
    if ($limit != "") {
        list($offset, $limit) = @explode(",", $limit);
        $this->db->limit($offset, $limit);
    }
    $list_data = $this->db->get()->result_array();
   //   echo $this->db->last_query();
//		pr($list_data);exit;
    return $list_data;
}
}
