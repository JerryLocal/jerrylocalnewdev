<?php
            
/**
 * Description of Admin Settled Transactions Extended Controller
 * 
 * @module Extended Admin Settled Transactions
 * 
 * @class HBC_Adminsettledtransactions.php
 * 
 * @path application\admin\adminsettledtransactions\controllers\HBC_Adminsettledtransactions.php
 * 
 * @author Simhachalam Gulla
 * 
 * @date 04.12.2015
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
 
Class HBC_Adminsettledtransactions extends Adminsettledtransactions {
        function __construct() {
    parent::__construct();
}
function settelment_invoice() {
    $settleArr = $this->params_arr;
    $settelment_id = $settleArr['id'];
    $download = intval($settleArr['download']);
    $invoiceArr1 = $this->model_adminsettledtransactions->getSettelmentInvoiceDetails($settelment_id, '');
    $invoiceArr = $invoiceArr1[0];
    if (is_array($invoiceArr) && count($invoiceArr) > 0) {
		
        $invoiceArr['trans_date'] = $this->general->dateSystemFormat($invoiceArr['trans_date']);
		
        $company_support_email = $this->config->item("COMPANY_SUPPORT_EMAIL");
        $company_name = $this->config->item("COMPANY_NAME");
        $company_contact_no = $this->config->item("COMPANY_TOLL_FREE");
        $fax_no = $this->config->item("COMPANY_FAX");
        $invoiceArr['company_support_email'] = $company_support_email;
        $invoiceArr['company_name'] = $company_name;
        $invoiceArr['company_contact_no'] = $company_contact_no;
        $invoiceArr['fax_no'] = $fax_no;

        $invoiceArr['settelment_no'] = $this->general->get_admin_rma_number($settelment_id);


        $data = array();
        $data['mo_buyer_area'] = '';
        $data['mo_buyer_address1'] = $this->config->item("COMPANY_ADDRESS");
        $data['mo_buyer_address2'] = '';
        $data['mc1_country'] = $this->config->item("COMPANY_COUNTRY");
        $data['ms_state'] = $this->config->item("COMPANY_STATE");
        $data['mc2_city'] = $this->config->item("COMPANY_CITY");
        $data['mo_buyer_pin_code'] = $this->config->item("COMPANY_ZIP");
        $buyer_address = $this->general->getBuyerShippingAddressExpandedView('', '', $data);
        $invoiceArr['admin_address'] = $buyer_address;

        $data = array();
        $data['mo_buyer_area'] = $invoiceArr['seller_area'];
        $data['mo_buyer_address1'] = $invoiceArr['seller_address1'];
        $data['mo_buyer_address2'] = $invoiceArr['seller_address2'];
        $data['mc1_country'] = $invoiceArr['seller_country'];
        $data['ms_state'] = $invoiceArr['seller_state'];
        $data['mc2_city'] = $invoiceArr['seller_city'];
        $data['mo_buyer_pin_code'] = $invoiceArr['seller_pin_code'];
        $seller_address = $this->general->getBuyerShippingAddressExpandedView('', '', $data);

        $invoiceArr['store_address'] = $seller_address;

        if ($download == 1) {
	
            require_once APPPATH . 'third_party/dompdf/dompdf_config.inc.php';
            $html = $this->parser->parse("adminsettledtransactions_hbc_settelment_invoice.tpl", $invoiceArr, true);
            $dompdf = new DOMPDF();
            $dompdf->load_html($html);
            $dompdf->render();
            $file_name = time() . "_trans_invoice.pdf";
            $pdffile = "public/upload/pdf/" . $file_name;
            $this->general->custom_file_put_contents($pdffile, $dompdf->output());
            $dir_path = $this->config->item("upload_path") . "pdf/";
            $pdf_url = $this->config->item("upload_url") . 'pdf/' . $file_name;
		
            $this->general->force_download($file_name, $dir_path);
            exit;
        } else {
            $this->loadView("adminsettledtransactions_hbc_settelment_invoice", $invoiceArr);
        }
    } else {
        $this->session->set_flashdata('failure', 'Failure Occured in displaying the invoice');
        $redirect_url = $this->config->item("admin_url") . "adminsettledtransactions/adminsettledtransactions/index";
        redirect($redirect_url);
    }
}
}
