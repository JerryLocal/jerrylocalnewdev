<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('GETORDEREXPANDEDVIEW_GET_ORDER_EXPANDED_VIEW')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','GETORDEREXPANDEDVIEW_GET_ORDER_EXPANDED_VIEW')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="getorderexpandedview" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="getorderexpandedview" class="frm-view-block frm-stand-view">
            <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
            <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
            <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
            <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
            <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
            <div class="main-content-block" id="main_content_block">
                <div style="width:98%;" class="frm-block-layout pad-calc-container">
                    <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                        <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('GETORDEREXPANDEDVIEW_GET_ORDER_EXPANDED_VIEW')%></h4></div>
                        <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                            <div class="form-row row-fluid" id="cc_sh_mso_mst_order_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_MST_ORDER_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_mst_order_id'], $opt_arr['mso_mst_order_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_mst_store_detail_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_MST_STORE_DETAIL_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_mst_store_detail_id'], $opt_arr['mso_mst_store_detail_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_mst_products_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_MST_PRODUCTS_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_mst_products_id'], $opt_arr['mso_mst_products_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_product_name">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_NAME')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_product_name']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_product_sku">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_SKU')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_product_sku']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_product_price">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_PRICE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_product_price']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_product_qty">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_QTY')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_product_qty'], $opt_arr['mso_product_qty'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_canel_by">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_CANEL_BY')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_canel_by'], $opt_arr['mso_canel_by'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_item_status">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_ITEM_STATUS')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_item_status'], $opt_arr['mso_item_status'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_shipping_cost">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING_COST')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_shipping_cost']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_total_cost">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_TOTAL_COST')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_total_cost']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_return_period">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_RETURN_PERIOD')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_return_period'], $opt_arr['mso_return_period'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_extra_info">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_EXTRA_INFO')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_extra_info']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_product_option">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_OPTION')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_product_option']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_seller_invoice_date">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_SELLER_INVOICE_DATE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
                                    <strong><%$this->general->dateSystemFormat($data['mso_seller_invoice_date'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_seller_invoice_no">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_SELLER_INVOICE_NO')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_seller_invoice_no']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_shipping_date">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING_DATE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
                                    <strong><%$this->general->dateSystemFormat($data['mso_shipping_date'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_shipper_name">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPER_NAME')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_shipper_name']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_mst_shipper_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_MST_SHIPPER_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_mst_shipper_id'], $opt_arr['mso_mst_shipper_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_cancel_detail">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_CANCEL_DETAIL')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_cancel_detail']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_cancel_date">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_CANCEL_DATE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
                                    <strong><%$this->general->dateSystemFormat($data['mso_cancel_date'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_cancel_user_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_CANCEL_USER_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_cancel_user_id'], $opt_arr['mso_cancel_user_id'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_gateway_tdr">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_GATEWAY_TDR')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_gateway_tdr']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_market_place_fee">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_MARKET_PLACE_FEE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_market_place_fee']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_admin_commision_per">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_ADMIN_COMMISION_PER')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_admin_commision_per']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_admin_commision">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_ADMIN_COMMISION')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_admin_commision']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_shipping_remark">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING_REMARK')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_shipping_remark']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_gateway_tdrper">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_GATEWAY_TDRPER')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_gateway_tdrper']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_tracking_number">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_TRACKING_NUMBER')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_tracking_number']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_product_regular_price">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_REGULAR_PRICE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_product_regular_price']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_product_sale_price">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_SALE_PRICE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_product_sale_price']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_delivered_date">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_DELIVERED_DATE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  input-append text-append-prepend  ">
                                    <strong><%$this->general->dateSystemFormat($data['mso_delivered_date'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_action_taken_by">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_ACTION_TAKEN_BY')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_action_taken_by'], $opt_arr['mso_action_taken_by'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_settlement">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_SETTLEMENT')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_settlement'], $opt_arr['mso_settlement'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_sub_order_number_pre">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_SUB_ORDER_NUMBER_PRE')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$data['mso_sub_order_number_pre']%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_sub_order_number">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_SUB_ORDER_NUMBER')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_sub_order_number'], $opt_arr['mso_sub_order_number'])%></strong>
                                </div>
                            </div>
                            <div class="form-row row-fluid" id="cc_sh_mso_trn_settlement_id">
                                <label class="form-label span3">
                                    <%$this->lang->line('GETORDEREXPANDEDVIEW_TRN_SETTLEMENT_ID')%>
                                </label> 
                                <div class="form-right-div frm-elements-div  ">
                                    <strong><%$this->general->displayKeyValueData($data['mso_trn_settlement_id'], $opt_arr['mso_trn_settlement_id'])%></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>        
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
    callSwitchToSelf();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
