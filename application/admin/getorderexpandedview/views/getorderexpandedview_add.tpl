<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%assign var="mod_label_text" value=$this->general->getDisplayLabel("Generic",$mode,"label")%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                <%$this->lang->line($mod_label_text)%> :: <%$this->lang->line('GETORDEREXPANDEDVIEW_GET_ORDER_EXPANDED_VIEW')%>
                <%if $mode eq 'Update' && $recName neq ''%>
                    :: <%$recName%> 
                <%/if%>
            </div>
        </h3>
        <div class="header-right-btns">
            <%if $backlink_allow eq true%>
                <div class="frm-back-to">
                    <a hijacked="yes" href="<%$admin_url%>#<%$mod_enc_url['index']%><%$extra_hstr%>"class="backlisting-link" title="<%$this->general->parseLabelMessage('GENERIC_BACK_TO_MODULE_LISTING','#MODULE_HEADING#','GETORDEREXPANDEDVIEW_GET_ORDER_EXPANDED_VIEW')%>">
                        <span class="icon16 minia-icon-arrow-left"></span>
                    </a>
                </div>
            <%/if%>
            <%if $next_link_allow eq true%>
                <div class="frm-next-rec">
                    <a hijacked="yes" title="<%$next_prev_records['next']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['next']['enc_id']%><%$extra_hstr%>" class='btn next-btn'>
                        <%$this->lang->line('GENERIC_NEXT_SHORT')%> <span class='icon12 icomoon-icon-arrow-right'></span>
                    </a>
                </div>
            <%/if%>
            <%if $switchto_allow eq true%>
                <div class="frm-switch-drop">
                    <%if $switch_combo|is_array && $switch_combo|@count gt 0%>
                        <%$this->dropdown->display('vSwitchPage',"vSwitchPage","style='width:100%;' aria-switchto-self='<%$switch_cit.param%>' class='chosen-select' onchange='return loadAdminModuleAddSwitchPage(\"<%$mod_enc_url.add%>\",this.value, \"<%$extra_hstr%>\")' ",'','',$enc_id)%>
                    <%/if%>
                </div>
            <%/if%>
            <%if $prev_link_allow eq true%>  
                <div class="frm-prev-rec">
                    <a hijacked="yes" title="<%$next_prev_records['prev']['val']%>" href="<%$admin_url%>#<%$mod_enc_url['add']%>|mode|<%$mod_enc_mode['Update']%>|id|<%$next_prev_records['prev']['enc_id']%><%$extra_hstr%>" class='btn prev-btn'>
                        <span class='icon12 icomoon-icon-arrow-left'></span> <%$this->lang->line('GENERIC_PREV_SHORT')%>
                    </a>            
                </div>
            <%/if%>
            <div class="clear"></div>
        </div>
        <span style="display:none;position:inherit;" id="ajax_lang_loader"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
    <input type="hidden" id="projmod" name="projmod" value="getorderexpandedview" />
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    <div class="top-frm-tab-layout" id="top_frm_tab_layout">
    </div>
    <div id="scrollable_content" class="scrollable-content top-block-spacing ">
        <div id="getorderexpandedview" class="frm-elem-block frm-stand-view">
            <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                <div class="main-content-block" id="main_content_block">
                    <div style="width:98%" class="frm-block-layout pad-calc-container">
                        <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                            <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('GETORDEREXPANDEDVIEW_GET_ORDER_EXPANDED_VIEW')%></h4></div>
                            <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                <div class="form-row row-fluid" id="cc_sh_mso_mst_order_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_MST_ORDER_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_mst_order_id']%>
                                        <%$this->dropdown->display("mso_mst_order_id","mso_mst_order_id","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_MST_ORDER_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_MST_ORDER_ID')%>'  ", "|||", "", $opt_selected,"mso_mst_order_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_mst_order_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_mst_store_detail_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_MST_STORE_DETAIL_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_mst_store_detail_id']%>
                                        <%$this->dropdown->display("mso_mst_store_detail_id","mso_mst_store_detail_id","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_MST_STORE_DETAIL_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_MST_STORE_DETAIL_ID')%>'  ", "|||", "", $opt_selected,"mso_mst_store_detail_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_mst_store_detail_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_mst_products_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_MST_PRODUCTS_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_mst_products_id']%>
                                        <%$this->dropdown->display("mso_mst_products_id","mso_mst_products_id","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_MST_PRODUCTS_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_MST_PRODUCTS_ID')%>'  ", "|||", "", $opt_selected,"mso_mst_products_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_mst_products_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_product_name">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_NAME')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_product_name']|@htmlentities%>" name="mso_product_name" id="mso_product_name" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_NAME')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_product_nameErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_product_sku">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_SKU')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_product_sku']|@htmlentities%>" name="mso_product_sku" id="mso_product_sku" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_SKU')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_product_skuErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_product_price">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_PRICE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_product_price']|@htmlentities%>" name="mso_product_price" id="mso_product_price" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_PRICE')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_product_priceErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_product_qty">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_QTY')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_product_qty']%>
                                        <%$this->dropdown->display("mso_product_qty","mso_product_qty","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_QTY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_PRODUCT_QTY')%>'  ", "|||", "", $opt_selected,"mso_product_qty")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_product_qtyErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_canel_by">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_CANEL_BY')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_canel_by']%>
                                        <%$this->dropdown->display("mso_canel_by","mso_canel_by","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_CANEL_BY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_CANEL_BY')%>'  ", "|||", "", $opt_selected,"mso_canel_by")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_canel_byErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_item_status">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_ITEM_STATUS')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_item_status']%>
                                        <%$this->dropdown->display("mso_item_status","mso_item_status","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_ITEM_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_ITEM_STATUS')%>'  ", "|||", "", $opt_selected,"mso_item_status")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_item_statusErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_shipping_cost">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING_COST')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_shipping_cost']|@htmlentities%>" name="mso_shipping_cost" id="mso_shipping_cost" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING_COST')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_shipping_costErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_total_cost">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_TOTAL_COST')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_total_cost']|@htmlentities%>" name="mso_total_cost" id="mso_total_cost" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_TOTAL_COST')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_total_costErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_return_period">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_RETURN_PERIOD')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_return_period']%>
                                        <%$this->dropdown->display("mso_return_period","mso_return_period","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_RETURN_PERIOD')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_RETURN_PERIOD')%>'  ", "|||", "", $opt_selected,"mso_return_period")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_return_periodErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_extra_info">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_EXTRA_INFO')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <textarea placeholder=""  name="mso_extra_info" id="mso_extra_info" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_EXTRA_INFO')%>"  class='elastic frm-size-medium'  ><%$data['mso_extra_info']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_extra_infoErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_product_option">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_OPTION')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <textarea placeholder=""  name="mso_product_option" id="mso_product_option" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_OPTION')%>"  class='elastic frm-size-medium'  ><%$data['mso_product_option']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_product_optionErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_seller_invoice_date">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_SELLER_INVOICE_DATE')%>
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend   ">
                                        <input type="text" value="<%$this->general->dateSystemFormat($data['mso_seller_invoice_date'])%>" placeholder="" name="mso_seller_invoice_date" id="mso_seller_invoice_date" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_SELLER_INVOICE_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date'  />
                                        <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_seller_invoice_dateErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_seller_invoice_no">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_SELLER_INVOICE_NO')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_seller_invoice_no']|@htmlentities%>" name="mso_seller_invoice_no" id="mso_seller_invoice_no" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_SELLER_INVOICE_NO')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_seller_invoice_noErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_shipping_date">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING_DATE')%>
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend   ">
                                        <input type="text" value="<%$this->general->dateSystemFormat($data['mso_shipping_date'])%>" placeholder="" name="mso_shipping_date" id="mso_shipping_date" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date'  />
                                        <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_shipping_dateErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_shipper_name">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPER_NAME')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_shipper_name']|@htmlentities%>" name="mso_shipper_name" id="mso_shipper_name" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPER_NAME')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_shipper_nameErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_mst_shipper_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_MST_SHIPPER_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_mst_shipper_id']%>
                                        <%$this->dropdown->display("mso_mst_shipper_id","mso_mst_shipper_id","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_MST_SHIPPER_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_MST_SHIPPER_ID')%>'  ", "|||", "", $opt_selected,"mso_mst_shipper_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_mst_shipper_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_cancel_detail">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_CANCEL_DETAIL')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <textarea placeholder=""  name="mso_cancel_detail" id="mso_cancel_detail" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_CANCEL_DETAIL')%>"  class='elastic frm-size-medium'  ><%$data['mso_cancel_detail']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_cancel_detailErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_cancel_date">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_CANCEL_DATE')%>
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend   ">
                                        <input type="text" value="<%$this->general->dateSystemFormat($data['mso_cancel_date'])%>" placeholder="" name="mso_cancel_date" id="mso_cancel_date" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_CANCEL_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date'  />
                                        <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_cancel_dateErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_cancel_user_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_CANCEL_USER_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_cancel_user_id']%>
                                        <%$this->dropdown->display("mso_cancel_user_id","mso_cancel_user_id","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_CANCEL_USER_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_CANCEL_USER_ID')%>'  ", "|||", "", $opt_selected,"mso_cancel_user_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_cancel_user_idErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_gateway_tdr">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_GATEWAY_TDR')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_gateway_tdr']|@htmlentities%>" name="mso_gateway_tdr" id="mso_gateway_tdr" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_GATEWAY_TDR')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_gateway_tdrErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_market_place_fee">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_MARKET_PLACE_FEE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_market_place_fee']|@htmlentities%>" name="mso_market_place_fee" id="mso_market_place_fee" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_MARKET_PLACE_FEE')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_market_place_feeErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_admin_commision_per">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_ADMIN_COMMISION_PER')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_admin_commision_per']|@htmlentities%>" name="mso_admin_commision_per" id="mso_admin_commision_per" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_ADMIN_COMMISION_PER')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_admin_commision_perErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_admin_commision">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_ADMIN_COMMISION')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_admin_commision']|@htmlentities%>" name="mso_admin_commision" id="mso_admin_commision" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_ADMIN_COMMISION')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_admin_commisionErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_shipping_remark">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING_REMARK')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <textarea placeholder=""  name="mso_shipping_remark" id="mso_shipping_remark" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_SHIPPING_REMARK')%>"  class='elastic frm-size-medium'  ><%$data['mso_shipping_remark']%></textarea>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_shipping_remarkErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_gateway_tdrper">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_GATEWAY_TDRPER')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_gateway_tdrper']|@htmlentities%>" name="mso_gateway_tdrper" id="mso_gateway_tdrper" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_GATEWAY_TDRPER')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_gateway_tdrperErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_tracking_number">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_TRACKING_NUMBER')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_tracking_number']|@htmlentities%>" name="mso_tracking_number" id="mso_tracking_number" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_TRACKING_NUMBER')%>"  class='frm-size-medium'  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_tracking_numberErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_product_regular_price">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_REGULAR_PRICE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_product_regular_price']|@htmlentities%>" name="mso_product_regular_price" id="mso_product_regular_price" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_REGULAR_PRICE')%>"  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_product_regular_priceErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_product_sale_price">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_SALE_PRICE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_product_sale_price']|@htmlentities%>" name="mso_product_sale_price" id="mso_product_sale_price" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_PRODUCT_SALE_PRICE')%>"  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_product_sale_priceErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_delivered_date">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_DELIVERED_DATE')%>
                                    </label> 
                                    <div class="form-right-div  input-append text-append-prepend   ">
                                        <input type="text" value="<%$this->general->dateSystemFormat($data['mso_delivered_date'])%>" placeholder="" name="mso_delivered_date" id="mso_delivered_date" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_DELIVERED_DATE')%>"  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date'  />
                                        <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_delivered_dateErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_action_taken_by">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_ACTION_TAKEN_BY')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_action_taken_by']%>
                                        <%$this->dropdown->display("mso_action_taken_by","mso_action_taken_by","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_ACTION_TAKEN_BY')%>'  aria-chosen-valid='Yes'  class='chosen-select'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_ACTION_TAKEN_BY')%>'  ", "", "", $opt_selected,"mso_action_taken_by")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_action_taken_byErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_settlement">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_SETTLEMENT')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_settlement']%>
                                        <%$this->dropdown->display("mso_settlement","mso_settlement","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_SETTLEMENT')%>'  aria-chosen-valid='Yes'  class='chosen-select'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_SETTLEMENT')%>'  ", "", "", $opt_selected,"mso_settlement")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_settlementErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_sub_order_number_pre">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_SUB_ORDER_NUMBER_PRE')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <input type="text" placeholder="" value="<%$data['mso_sub_order_number_pre']|@htmlentities%>" name="mso_sub_order_number_pre" id="mso_sub_order_number_pre" title="<%$this->lang->line('GETORDEREXPANDEDVIEW_SUB_ORDER_NUMBER_PRE')%>"  />
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_sub_order_number_preErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_sub_order_number">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_SUB_ORDER_NUMBER')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_sub_order_number']%>
                                        <%$this->dropdown->display("mso_sub_order_number","mso_sub_order_number","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_SUB_ORDER_NUMBER')%>'  aria-chosen-valid='Yes'  class='chosen-select'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_SUB_ORDER_NUMBER')%>'  ", "", "", $opt_selected,"mso_sub_order_number")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_sub_order_numberErr'></label></div>
                                </div>
                                <div class="form-row row-fluid" id="cc_sh_mso_trn_settlement_id">
                                    <label class="form-label span3">
                                        <%$this->lang->line('GETORDEREXPANDEDVIEW_TRN_SETTLEMENT_ID')%>
                                    </label> 
                                    <div class="form-right-div   ">
                                        <%assign var="opt_selected" value=$data['mso_trn_settlement_id']%>
                                        <%$this->dropdown->display("mso_trn_settlement_id","mso_trn_settlement_id","  title='<%$this->lang->line('GETORDEREXPANDEDVIEW_TRN_SETTLEMENT_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'GETORDEREXPANDEDVIEW_TRN_SETTLEMENT_ID')%>'  ", "", "", $opt_selected,"mso_trn_settlement_id")%>
                                    </div>
                                    <div class="error-msg-form "><label class='error' id='mso_trn_settlement_idErr'></label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%>">
                        <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                            <%assign var='rm_ctrl_directions' value=true%>
                        <%/if%>
                        <!-- Form Redirection Control Unit -->
                        <%if $controls_allow eq false || $rm_ctrl_directions eq true%>
                            <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" type="hidden" />
                        <%else%>
                            <div class='action-dir-align'>
                                <%if $prev_link_allow eq true%>
                                    <input value="Prev" id="ctrl_flow_prev" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Prev' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_prev">&nbsp;</label>
                                    <label for="ctrl_flow_prev" class="inline-elem-margin"><%$this->lang->line('GENERIC_PREV_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <%if $next_link_allow eq true || $mode eq 'Add'%>
                                    <input value="Next" id="ctrl_flow_next" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'Next' %> checked=true <%/if%> />
                                    <label for="ctrl_flow_next">&nbsp;</label>
                                    <label for="ctrl_flow_next" class="inline-elem-margin"><%$this->lang->line('GENERIC_NEXT_SHORT')%></label>&nbsp;&nbsp;
                                <%/if%>
                                <input value="List" id="ctrl_flow_list" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq 'List' %> checked=true <%/if%> />
                                <label for="ctrl_flow_list">&nbsp;</label>
                                <label for="ctrl_flow_list" class="inline-elem-margin"><%$this->lang->line('GENERIC_LIST_SHORT')%></label>&nbsp;&nbsp;
                                <input value="Stay" id="ctrl_flow_stay" name="ctrl_flow" class="regular-radio" type="radio" <%if $ctrl_flow eq '' || $ctrl_flow eq 'Stay' %> checked=true <%/if%> />
                                <label for="ctrl_flow_stay">&nbsp;</label>
                                <label for="ctrl_flow_stay" class="inline-elem-margin"><%$this->lang->line('GENERIC_STAY_SHORT')%></label>
                            </div>
                        <%/if%>
                        <!-- Form Action Control Unit -->
                        <%if $controls_allow eq false%>
                            <div class="clear">&nbsp;</div>
                        <%/if%>
                        <div class="action-btn-align" id="action_btn_container">
                            <%if $mode eq 'Update'%>
                                <%if $update_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_UPDATE')%>" name="ctrlupdate" type="submit" id="frmbtn_update" class="btn btn-info"/>&nbsp;&nbsp;
                                <%/if%>
                                <%if $delete_allow eq true%>
                                    <input value="<%$this->lang->line('GENERIC_DELETE')%>" name="ctrldelete" type="button" id="frmbtn_delete" class="btn btn-danger" onclick="return deleteAdminRecordData('<%$enc_id%>', '<%$mod_enc_url.index%>','<%$mod_enc_url.inline_edit_action%>', '<%$extra_qstr%>', '<%$extra_hstr%>');" />&nbsp;&nbsp;
                                <%/if%>
                            <%else%>
                                <input value="<%$this->lang->line('GENERIC_SAVE')%>" name="ctrladd" type="submit" id="frmbtn_add" class="btn btn-info" />&nbsp;&nbsp;
                            <%/if%>
                            <%if $discard_allow eq true%>
                                <input value="<%$this->lang->line('GENERIC_DISCARD')%>" name="ctrldiscard" type="button" id="frmbtn_discard" class="btn" onclick="return loadAdminModuleListing('<%$mod_enc_url.index%>', '<%$extra_hstr%>')" />
                            <%/if%>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </form>
        </div>
    </div>
</div>
<%javascript%>    
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};        
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        <%foreach name=i from=$auto_arr item=v key=k%>
            if($("#<%$k%>").is("select")){
                $("#<%$k%>").ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                    },{
                    loadingImg: admin_image_url+"chosen-loading.gif"
                });
            }
        <%/foreach%>
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['buttons_arr'] = [];
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/getorderexpandedview_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.getorderexpandedview.callEvents();
<%/javascript%>