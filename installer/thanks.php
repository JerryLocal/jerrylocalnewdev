<?php
$success = $_GET['success'];
$password = $_GET['p'];
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="<?php echo $site_path; ?>images/favicon.ico" type="image/x-icon">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Installation Process</title>
        <script>
            var installer_url = '<?php echo $installer_url; ?>';
        </script>
        <link href="<?php echo $installer_url; ?>assets/css/style.css" rel='stylesheet'  type='text/css'/>
        <link href="<?php echo $installer_url; ?>assets/css/base.css" rel='stylesheet'  type='text/css'/>
    </head>
    <body>
        <div id="wrapper" class="wapper">
            <?php
            include_once("top.php");
            include_once("navigation.php");
            ?>
            <div id="content" class="contaniner">
                <?php if ($success == 1) { ?>

                    <div class="congrats">
                        <div class="content">
                            <img src="<?php echo $installer_image_url; ?>congrats_logo.png" align="Congratulations" />
                            <div class="right">
                                <h1>YOU'RE DONE!</h1>
                                <p>You've successfully installed your project.<br />You can access it right away with details below...</p>
                            </div>
                            <div class="clear"></div>
                            <div class="site_info">
                                <div>Admin :<a href="<?php echo $site_url . "admin/"; ?>" title="Admin Panel" ><?php echo $site_url . "admin/"; ?></a></div>
                                <span class="first">Username: admin</span>
                                <span>Password: <?php echo (trim($password) != "") ? base64_decode($password) : ""; ?></span>
                            </div>
                            <div class="site_info">
                                <div>Front : <a href="<?php echo $site_url; ?>" title="Front Panel" ><?php echo $site_url; ?></a></div>
                            </div>

                        </div>
                    </div>
                    <?php
                }
                ?>
                <!-- middle part end here-->
            </div>
        </div>
    </body>
</html>