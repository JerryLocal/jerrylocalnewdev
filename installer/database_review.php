<?php
pre_page_conditions("databasereview", true);
$db_detail = $_SESSION['install_process_arr']['db_detail'];
$not_proceed_ahead = true;

if(!in_array(strtolower($_GET['vDatabaseType']),array('mysql','sqlsrv','pgsql'))){
    header("Location:" . $installer_url);
    exit;
}

?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="<?php echo $site_path; ?>images/favicon.ico" type="image/x-icon">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Installation Process</title>
        <script>
            var installer_url = '<?php echo $installer_url; ?>';
        </script>
        <link href="<?php echo $installer_url; ?>assets/css/style.css" rel='stylesheet'  type='text/css'/>
        <link href="<?php echo $installer_url; ?>assets/css/base.css" rel='stylesheet'  type='text/css'/>
        <script language='JavaScript1.2' src="<?php echo $installer_url; ?>assets/js/jquery.js"></script>
        <script language='JavaScript1.2' src="<?php echo $installer_url; ?>assets/js/jquery.form.js"></script>
        <script language='JavaScript1.2' src="<?php echo $installer_url; ?>assets/js/jquery.validate.min.js"></script>
        <script language='JavaScript1.2' src="<?php echo $installer_url; ?>assets/js/project_review.js"></script>
    </head>
    <body>
        <div id="wrapper" class="wapper">
            <div class="ajax_qLoverlay"></div>
            <div class="ajax_qLbar"></div>
            <div class="ajax_qLbarText"><strong>Please wait while processing..</strong></div>
            <?php
            include_once("top.php");
            include_once("navigation.php");
            ?>
            <form name="frmdatabase" id="frmdatabase" method="post" action="installationaction.php" class="form-horizontal" onSubmit="return submitform();">
            <div id="content" class="contaniner databasefrm">
                    <input type="hidden" id="vDatabaseHostName" name="vDatabaseType" maxlength="50" size="40" autocomplete="off" value="<?php echo $db_detail['vDatabaseType']; ?>" />
                    <input type="hidden" id="vDatabaseHostName" name="vDatabaseHostName" maxlength="50" size="40" autocomplete="off" value="<?php echo $db_detail['vDatabaseHostName']; ?>" />
                    <input type="hidden" id="vDatabaseUserName" name="vDatabaseUserName"  size="40" autocomplete="off" value="<?php echo $db_detail['vDatabaseUserName']; ?>"  />
                    <input type="hidden" id="vDatabasePassword" name="vDatabasePassword"  size="40" autocomplete="off" value="<?php echo $db_detail['vDatabasePassword']; ?>"/>
                    <input type="hidden" id="vDatabasePort" name="vDatabasePort" maxlength="50" size="40" autocomplete="off" value="<?php echo $db_detail['vDatabasePort']; ?>"/>
                    <input type="hidden" id="vDatabaseName" name="vDatabaseName"  size="40" autocomplete="off" value="<?php echo $db_detail['vDatabaseName']; ?>"  />
                    <div class="intstall">
                        <h1>You're about to begin Installation...</h1>
                        <p>Review the details below one final time!</p>
                        <div class="info">
                            <div>
                                <span>Target</span>
                                <b><?php echo $site_url; ?></b>
                            </div>
                            <div>
                                <span>Database Type</span>
                                <b><?php 
                                if($_GET['vDatabaseType']=='mysql'){
                                    echo 'MySql';
                                }else if($_GET['vDatabaseType']=='pgsql'){
                                    echo 'Postgre Sql';
                                }else if($_GET['vDatabaseType']=='sqlsrv'){
                                    echo 'Sql Server';
                                }
                                ?></b>
                            </div>
                            <div>
                                <span>Database Host</span>
                                <b><?php echo $db_detail['vDatabaseHostName']; ?></b>
                            </div>
                            <div>
                                <span>Database Username</span>
                                <b><?php echo $db_detail['vDatabaseUserName']; ?></b>
                            </div>
                            <div>
                                <span>Database Password</span>
                                <b>&nbsp;<?php
                                    if ($db_detail['vDatabasePassword'] != "") {
                                        echo str_pad("", strlen($db_detail['vDatabasePassword']), "*");
                                    }
                                    ?></b>
                            </div>
                            <div>
                                <span>Database Name</span>
                                <b><?php echo $db_detail['vDatabaseName']; ?></b>
                            </div>
                            <div>
                                <span>Database Port</span>
                                <b><?php echo $db_detail['vDatabasePort']; ?></b>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
            </div>
            <div id="footer">
                <div id="footer_action">
                    <div class="left"><a href="<?php echo $installer_url . "index.php?step=3&_t=" . time() ?>">Back</a></div>
                    <div class="right">
                        <?php
                        if ($not_proceed_ahead) {
                            ?>
                            <input type="hidden" name="installer_step" id="installer_step" value="databasereview">
                            <input  type="submit" value="Install Project" name="database_submission" class="smallbutton">
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>