<?php

/**
 * Verifies the state of the specified file.
 *
 * @param $file
 *   The file to check for.
 * @param $mask
 *   An optional bitmask created from various FILE_* constants.
 * @param $type
 *   The type of file. Can be file (default), dir, or link.
 *
 * @return
 *   TRUE on success or FALSE on failure. A message is set for the latter.
 */
function hb_verify_install_file($file, $mask = NULL, $type = 'file')
{
    $return = TRUE;
    // Check for files that shouldn't be there.
    if (isset($mask) && ($mask & FILE_NOT_EXIST) && file_exists($file)) {
        return FALSE;
    }
    // Verify that the file is the type of file it is supposed to be.
    if (isset($type) && file_exists($file)) {
        $check = 'is_' . $type;
        if (!function_exists($check) || !$check($file)) {
            $return = FALSE;
        }
    }

    // Verify file permissions.
    if (isset($mask)) {
        $masks = array(FILE_EXIST, FILE_READABLE, FILE_WRITABLE, FILE_EXECUTABLE, FILE_NOT_READABLE, FILE_NOT_WRITABLE, FILE_NOT_EXECUTABLE);
        foreach ($masks as $current_mask) {
            if ($mask & $current_mask) {
                switch ($current_mask) {
                    case FILE_EXIST:
                        if (!file_exists($file)) {
                            if ($type == 'dir') {
                                hb_install_mkdir($file, $mask);
                            }
                            if (!file_exists($file)) {
                                $return = FALSE;
                            }
                        }
                        break;
                    case FILE_READABLE:
                        if (!is_readable($file) && !hb_install_fix_file($file, $mask)) {
                            $return = FALSE;
                        }
                        break;
                    case FILE_WRITABLE:
                        if (!is_writable($file) && !hb_install_fix_file($file, $mask)) {
                            $return = FALSE;
                        }
                        break;
                    case FILE_EXECUTABLE:
                        if (!is_executable($file) && !hb_install_fix_file($file, $mask)) {
                            $return = FALSE;
                        }
                        break;
                    case FILE_NOT_READABLE:
                        if (is_readable($file) && !hb_install_fix_file($file, $mask)) {
                            $return = FALSE;
                        }
                        break;
                    case FILE_NOT_WRITABLE:
                        if (is_writable($file) && !hb_install_fix_file($file, $mask)) {
                            $return = FALSE;
                        }
                        break;
                    case FILE_NOT_EXECUTABLE:
                        if (is_executable($file) && !hb_install_fix_file($file, $mask)) {
                            $return = FALSE;
                        }
                        break;
                }
            }
        }
    }
    return $return;
}

/**
 * Attempts to fix file permissions.
 *
 * The general approach here is that, because we do not know the security
 * setup of the webserver, we apply our permission changes to all three
 * digits of the file permission (i.e. user, group and all).
 *
 * To ensure that the values behave as expected (and numbers don't carry
 * from one digit to the next) we do the calculation on the octal value
 * using bitwise operations. This lets us remove, for example, 0222 from
 * 0700 and get the correct value of 0500.
 *
 * @param $file
 *  The name of the file with permissions to fix.
 * @param $mask
 *  The desired permissions for the file.
 * @param $message
 *  (optional) Whether to output messages. Defaults to TRUE.
 *
 * @return
 *  TRUE/FALSE whether or not we were able to fix the file's permissions.
 */
function hb_install_fix_file($file, $mask, $message = TRUE)
{
    // If $file does not exist, fileperms() issues a PHP warning.
    if (!file_exists($file)) {
        return FALSE;
    }

    $mod = fileperms($file) & 0777;
    $masks = array(FILE_READABLE, FILE_WRITABLE, FILE_EXECUTABLE, FILE_NOT_READABLE, FILE_NOT_WRITABLE, FILE_NOT_EXECUTABLE);

    // FILE_READABLE, FILE_WRITABLE, and FILE_EXECUTABLE permission strings
    // can theoretically be 0400, 0200, and 0100 respectively, but to be safe
    // we set all three access types in case the administrator intends to
    // change the owner of settings.php after installation.
    foreach ($masks as $m) {
        if ($mask & $m) {
            switch ($m) {
                case FILE_READABLE:
                    if (!is_readable($file)) {
                        $mod |= 0444;
                    }
                    break;
                case FILE_WRITABLE:
                    if (!is_writable($file)) {
                        $mod |= 0222;
                    }
                    break;
                case FILE_EXECUTABLE:
                    if (!is_executable($file)) {
                        $mod |= 0111;
                    }
                    break;
                case FILE_NOT_READABLE:
                    if (is_readable($file)) {
                        $mod &= ~0444;
                    }
                    break;
                case FILE_NOT_WRITABLE:
                    if (is_writable($file)) {
                        $mod &= ~0222;
                    }
                    break;
                case FILE_NOT_EXECUTABLE:
                    if (is_executable($file)) {
                        $mod &= ~0111;
                    }
                    break;
            }
        }
    }

    // chmod() will work if the web server is running as owner of the file.
    // If PHP safe_mode is enabled the currently executing script must also
    // have the same owner.
    if (@chmod($file, $mod)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * Creates a directory using HB's default mode.
 *
 *
 * Compatibility: normal paths and stream wrappers.
 *
 * @param $uri
 *   A URI or pathname.
 * @param $mode
 *   By default the Drupal mode is used.
 * @param $recursive
 *   Default to FALSE.
 * @param $context
 *   Refer to http://php.net/manual/ref.stream.php
 *
 * @return
 *   Boolean TRUE on success, or FALSE on failure.
 *
 * @see mkdir()
 * @ingroup php_wrappers
 */
function hb_mkdir($uri, $mode = NULL, $recursive = FALSE, $context = NULL)
{
    if (!isset($mode)) {
        $mode = variable_get('file_chmod_directory', 0775);
    }

    if (!isset($context)) {
        return mkdir($uri, $mode, $recursive);
    } else {
        return mkdir($uri, $mode, $recursive, $context);
    }
}

/**
 * Creates a directory with the specified permissions.
 *
 * @param $file
 *  The name of the directory to create;
 * @param $mask
 *  The permissions of the directory to create.
 * @param $message
 *  (optional) Whether to output messages. Defaults to TRUE.
 *
 * @return
 *  TRUE/FALSE whether or not the directory was successfully created.
 */
function hb_install_mkdir($file, $mask, $message = TRUE)
{
    $mod = 0;
    $masks = array(FILE_READABLE, FILE_WRITABLE, FILE_EXECUTABLE, FILE_NOT_READABLE, FILE_NOT_WRITABLE, FILE_NOT_EXECUTABLE);
    foreach ($masks as $m) {
        if ($mask & $m) {
            switch ($m) {
                case FILE_READABLE:
                    $mod |= 0444;
                    break;
                case FILE_WRITABLE:
                    $mod |= 0222;
                    break;
                case FILE_EXECUTABLE:
                    $mod |= 0111;
                    break;
            }
        }
    }

    if (@hb_mkdir($file, $mod)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function system_requirements($phase = "")
{
    global $site_path;
    $installer_temp_assets_directory = $site_path . 'installer/assets/temp/';
    $requirements = array();

    $software = $_SERVER['SERVER_SOFTWARE'];
    $requirements['webserver'] = array(
        'title' => 'Web server',
        'value' => $software,
    );

    // Test PHP version and show link to phpinfo() if it's available
    $phpversion = phpversion();
    if (function_exists('phpinfo')) {
        $requirements['php'] = array(
            'title' => 'PHP',
            'value' => $phpversion,
        );
    } else {
        $requirements['php'] = array(
            'title' => 'PHP',
            'value' => $phpversion,
            'description' => 'The phpinfo() function has been disabled for security reasons. To see your server\'s phpinfo() information, change your PHP settings or contact your server administrator. ',
            'severity' => REQUIREMENT_INFO,
        );
    }

    if (version_compare($phpversion, INSTALLER_PHP_VERSION) < 0) {
        $requirements['php']['description'] = 'Your PHP installation is too old. HB requires at least PHP ' . INSTALLER_PHP_VERSION;
        $requirements['php']['severity'] = REQUIREMENT_ERROR;
        // If PHP is old, it's not safe to continue with the requirements check.
        return $requirements;
    }

    // Test for PHP extensions.
    $requirements['php_extensions'] = array(
        'title' => 'PHP extensions',
    );

    $missing_extensions = array();
    $required_extensions = get_required_extension();

    foreach ($required_extensions as $extension) {
        if (!extension_loaded($extension)) {
            $missing_extensions[] = $extension;
        }
    }

    if (!empty($missing_extensions)) {
        $description = 'Project Instllation requires you to enable the PHP extensions in the following list';

        if (is_array($missing_extensions) && count($missing_extensions) > 0) {
            $description.="<br>";
            foreach ($missing_extensions as $key_i => $val)
                $description .= ($key_i + 1) . ") " . $val . "<br>";
        }


        $requirements['php_extensions']['value'] = 'Disabled';
        $requirements['php_extensions']['severity'] = REQUIREMENT_ERROR;
        $requirements['php_extensions']['description'] = $description;
    } else {
        $requirements['php_extensions']['value'] = 'Enabled';
    }
//pgsql sqlsrv
    $database_type = $_SESSION['install_process_arr']['db_detail']['vDatabaseType'];
    switch ($database_type) {
        case'mysql':
        default :
            if (!extension_loaded("mysql") || !extension_loaded("mysqli")) {
                $requirements['mysql']['title'] = 'MySQL';
                $requirements['mysql']['value'] = 'MySQL is not Installed !!';
                $requirements['mysql']['severity'] = REQUIREMENT_ERROR;
                $requirements['mysql']['description'] = "The project installer requires MySQL for the project setup";
            } else {
                $mysql_version = get_mysql_version();
                $requirements['mysql']['title'] = 'MySQL';
                $requirements['mysql']['value'] = 'MySQL is Installed';
                $requirements['mysql']['severity'] = REQUIREMENT_OK;
            }
            break;

        case'sqlsrv':
            if (!extension_loaded("sqlsrv")) {
                $requirements['mysql']['title'] = 'SQLSrv';
                $requirements['mysql']['value'] = 'SQLServer is not Installed !!';
                $requirements['mysql']['severity'] = REQUIREMENT_ERROR;
                $requirements['mysql']['description'] = "The project installer requires SQLServer for the project setup";
            } else {
                $requirements['mysql']['title'] = 'SQLSrv';
                $requirements['mysql']['value'] = 'SQLServer is Installed';
                $requirements['mysql']['severity'] = REQUIREMENT_OK;
            }
            break;
        case'pgsql':
            if (!extension_loaded("pgsql")) {
                $requirements['mysql']['title'] = 'PgSQL';
                $requirements['mysql']['value'] = 'PostgreSQL is not Installed !!';
                $requirements['mysql']['severity'] = REQUIREMENT_ERROR;
                $requirements['mysql']['description'] = "The project installer requires PgSQL for the project setup";
            } else {
                $requirements['mysql']['title'] = 'PgSQL';
                $requirements['mysql']['value'] = 'PostgreSQL is Installed';
                $requirements['mysql']['severity'] = REQUIREMENT_OK;
            }
            break;
    }


    if (!extension_loaded("imagick")) {
        $requirements['image_magic']['title'] = 'Image Magic';
        $requirements['image_magic']['value'] = 'Image Magic is not Installed !!';
        $requirements['image_magic']['severity'] = REQUIREMENT_WARNING;
        $requirements['image_magic']['description'] = "The project installer requires Image Magic to run project properly";
    }
    if (!extension_loaded("mcrypt")) {
        $requirements['image_magic']['title'] = 'mcrypt';
        $requirements['image_magic']['value'] = 'mcrypt extension is not Installed !!';
        $requirements['image_magic']['severity'] = REQUIREMENT_WARNING;
        $requirements['image_magic']['description'] = "mcrypt extension must be required while you are using any encryption in this project.";
    }
    $identify_image_magic = imageMagicIdentify();
    $identify_image_convert = imageMagicConvert();
    $installer_temp_assets_directory_res = smartPermission($installer_temp_assets_directory);

    if (!$identify_image_magic && $installer_temp_assets_directory_res['success']) {
        $requirements['image_magic_identify']['title'] = 'Image Magic Identification';
        $requirements['image_magic_identify']['value'] = 'Image Magic is not identified';
        $requirements['image_magic_identify']['severity'] = REQUIREMENT_WARNING;
        $requirements['image_magic_identify']['description'] = "Image Magic is not identified in your system.Please check Image magic path in '" . $site_path . "installer_settings.php'";
    }
    if (!$identify_image_convert && $installer_temp_assets_directory_res['success']) {
        $requirements['image_magic_convert']['title'] = 'Image Magic Convert Command';
        $requirements['image_magic_convert']['value'] = 'Image Magic Convert Command is not installed or enabled';
        $requirements['image_magic_convert']['severity'] = REQUIREMENT_WARNING;
        $requirements['image_magic_convert']['description'] = "Image Magic Convert Command is not installed or enabled in your system.Please check Image magic path in '" . $site_path . "installer_settings.php'";
    }

    $enable_exec = check_exec();
    if (!$enable_exec) {
        $requirements['enable_exec'] = array(
            'title' => 'PHP EXEC Command',
            'value' => "PHP EXEC Command in not enabled.",
            'severity' => REQUIREMENT_ERROR,
            'description' => "php exec function must be enable to setup the project.Please check check your php.ini file for enabling this command"
        );
    }


    return $requirements;
}

/**
 * Checks installation requirements and reports any errors.
 */
function install_check_requirements($database_type = '')
{

    switch ($database_type) {
        case 'mysql':
        default :
            global $installer_sql_path;
            $installer_sql_path = $installer_sql_path;
            break;
        case 'pgsql':
            global $installer_pgsql_path;
            $installer_sql_path = $installer_pgsql_path;
            break;
        case 'sqlsrv':
            global $installer_sqlsrv_path;
            $installer_sql_path = $installer_sqlsrv_path;
            break;
    }

    global $site_url, $site_path;
    $requirements = system_requirements();

    $writable = FALSE;

    $database_file = $site_path . "application/config/database.php";
    $default_database_file = $site_path . 'application/config/default.database.php';

    $exists = FALSE;
    // Check if a database.php file already exists.
    $file = $database_file;
    if (hb_verify_install_file($database_file, FILE_EXIST)) {
        // If it does, make sure it is writable.
        $writable = hb_verify_install_file($database_file, FILE_READABLE | FILE_WRITABLE);
        if (!$writable) {
            smartPermission($database_file);
            $writable = hb_verify_install_file($database_file, FILE_READABLE | FILE_WRITABLE);
        }
        $exists = TRUE;
    }
    // If default.settings.php does not exist, or is not readable, throw an
    // error.
    if (!hb_verify_install_file($installer_sql_path, FILE_EXIST | FILE_READABLE)) {
        $requirements['default project file exists'] = array(
            'title' => 'SQL file',
            'value' => 'The SQL file does not exist.',
            'severity' => REQUIREMENT_ERROR,
            'description' => 'The project installer requires that sql file not be modified in any way from the original download.',
        );
    }
    if (!hb_verify_install_file($default_database_file, FILE_EXIST | FILE_READABLE)) {
        $requirements['default database file exists'] = array(
            'title' => 'Default Database file',
            'value' => 'The default database file does not exist.',
            'severity' => REQUIREMENT_ERROR,
            'description' => 'The project installer requires that the default database file not be modified in any way from the original download.it is not readable',
        );
    }
    // Otherwise, if database.php does not exist yet, we can try to copy
    // default.settings.php to create it.
    elseif (!$exists) {
        $copied = @copy($default_database_file, $database_file);
        if ($copied) {
            // If the new database file has the same owner as default.database.php,
            // this means default.database.php is owned by the webserver user.
            // This is an inherent security weakness because it allows a malicious
            // webserver process to append arbitrary PHP code and then execute it.
            // However, it is also a common configuration on shared hosting, and
            // there is nothing HB can do to prevent it. In this situation,
            // having database.php also owned by the webserver does not introduce
            // any additional security risk, so we keep the file in place.
            #echo fileowner($default_database_file) ."<<<<<".fileowner($database_file);exit;
            if (fileowner($default_database_file) === fileowner($database_file)) {
                $writable = hb_verify_install_file($database_file, FILE_READABLE | FILE_WRITABLE);
                if (!$writable) {
                    smartPermission($database_file);
                    $writable = hb_verify_install_file($database_file, FILE_READABLE | FILE_WRITABLE);
                }
                $exists = TRUE;
            }
            // If database.php and default.database.php have different owners, this
            // probably means the server is set up "securely" (with the webserver
            // running as its own user, distinct from the user who owns all the
            // CIBase PHP files), although with either a group or world writable
            // sites directory. Keeping settings.php owned by the webserver would
            // therefore introduce a security risk. It would also cause a usability
            // problem, since site owners who do not have root access to the file
            // system would be unable to edit their database file later on. We
            // therefore must delete the file we just created and force the
            // administrator to log on to the server and create it manually.
            else {
                $deleted = @unlink($database_file);
                // We expect deleting the file to be successful (since we just
                // created it ourselves above), but if it fails somehow, we set a
                // variable so we can display a one-time error message to the
                // administrator at the bottom of the requirements list. We also try
                // to make the file writable, to eliminate any conflicting error
                // messages in the requirements list.
                $exists = !$deleted;
                if ($exists) {
                    $settings_file_ownership_error = TRUE;
                    $writable = hb_verify_install_file($database_file, FILE_READABLE | FILE_WRITABLE);
                    if (!$writable) {
                        smartPermission($database_file);
                        $writable = hb_verify_install_file($database_file, FILE_READABLE | FILE_WRITABLE);
                    }
                }
            }
        }
    }

    // If settings.php does not exist, throw an error.
    if (!$exists) {
        $requirements['database file exists'] = array(
            'title' => 'Database file',
            'value' => 'The database file does not exist.',
            'severity' => REQUIREMENT_ERROR,
            'description' => 'The installer requires that you create a database file "' . $database_file . '" as part of the installation process.<br><br> You may copy default.database.php and rename it "database.php"',
        );
    } else {
//        $requirements['database file exists'] = array(
//            'title' => 'database file',
//            'value' => 'The ' . $file . ' file exists.',
//        );
        // If database.php is not writable, throw an error.
        if (!$writable) {
            $requirements['database file writable'] = array(
                'title' => 'Database file',
                'value' => 'The database file is not writable.',
                'severity' => REQUIREMENT_ERROR,
                'description' => 'The project installer requires write permissions to "' . $file . '" during the installation process.',
            );
        } else {
            $requirements['database file'] = array(
                'title' => 'Database File',
                'value' => "The database file '" . $file . "' is writable.",
            );
        }
        if (!empty($settings_file_ownership_error)) {
            $requirements['database file ownership'] = array(
                'title' => 'Database file',
                'value' => 'The database file is owned by the web server.',
                'severity' => REQUIREMENT_ERROR,
                'description' => 'The project installer failed to create a database file with proper file ownership. ',
            );
        }
    }

    $files_requirements = file_folder_requirements();
    if (is_array($files_requirements) && count($files_requirements) > 0) {
        $requirements = array_merge($requirements, $files_requirements);
    }

    return $requirements;
}

function file_folder_requirements()
{
    global $site_url, $site_path, $installer_sql_path;

    $javascript_label_file = $site_path . "application/admin/views/javascript_label.tpl";
    $language_label_directory = $site_path . 'application/language/';
    $application_cache = $site_path . 'application/cache/';
    $upload_directory = $site_path . 'public/upload/';
    $installer_assets_directory = $site_path . 'installer/assets/';
    $installer_temp_assets_directory = $site_path . 'installer/assets/temp/';

    /*
      // Check if a javascript_label_file.tpl file already exists.
      if (!hb_verify_install_file($javascript_label_file, FILE_EXIST)) {
      // If it does, make sure it is writable.
      $requirements['javascript_label_tpl'] = array(
      'title' => 'Javascript Label file',
      'value' => 'The javascript_label.tpl file does not exist.',
      'severity' => REQUIREMENT_ERROR,
      'description' => 'The installer requires ' . $javascript_label_file . ' file as part of the installation process.',
      );
      } else {
      $writable = hb_verify_install_file($javascript_label_file, FILE_READABLE | FILE_WRITABLE);
      if (!$writable) {

      smartPermission($javascript_label_file);
      $writable = hb_verify_install_file($javascript_label_file, FILE_READABLE | FILE_WRITABLE);
      if (!$writable) {
      $requirements['language_label_tpl'] = array(
      'title' => 'Javascript Label file',
      'value' => 'The Javascript Label file is not writable.',
      'severity' => REQUIREMENT_ERROR,
      'description' => 'The project installer requires write permissions to ' . $javascript_label_file . ' during the installation process.',
      );
      }
      }
      }
     */
    if (!is_dir($application_cache)) {
        hb_install_mkdir($application_cache, FILE_WRITABLE);
    }
    if (!is_dir($application_cache)) {
        $requirements['application_cache'] = array(
            'title' => 'Cache Directory',
            'value' => "Cache directory does not exist",
            'severity' => REQUIREMENT_ERROR,
            'description' => 'The project installer requires cache - "' . $application_cache . '" directory during the installation process.',
        );
    } else {
        $application_cache_res = smartPermission($application_cache);
        if (!$application_cache_res['success']) {
            $requirements['application_cache'] = array(
                'title' => 'Cache Directory',
                'value' => "Cache directory doesn't have 0755 or 0777 permission",
                'severity' => REQUIREMENT_ERROR,
                'description' => 'The project installer requires write permissions to cache - "' . $application_cache . '" directory to run the project properly.',
            );
        } else {
            $requirements['application_cache'] = array(
                'title' => 'Cache Directory',
                'value' => "Cache directory '" . $application_cache . "' have proper permission",
                'severity' => REQUIREMENT_OK
            );
        }
    }

    if (!is_dir($language_label_directory)) {
        $requirements['language_label_directory'] = array(
            'title' => 'Language Label Directory',
            'value' => "Language label directory does not exist",
            'severity' => REQUIREMENT_ERROR,
            'description' => 'The project installer requires Language Label Directory - "' . $language_label_directory . '" directory during the installation process.',
        );
    } else {
        $language_label_res = smartPermission($language_label_directory);
        if (!$language_label_res['success']) {
            $requirements['language_label_directory'] = array(
                'title' => 'Language Label Directory',
                'value' => "Language label directory doesn't have 0755 or 0777 permission",
                'severity' => REQUIREMENT_ERROR,
                'description' => 'The project installer requires write permissions to "' . $language_label_directory . '" directory to run the project properly',
            );
        } else {
            $requirements['language_label_directory'] = array(
                'title' => 'Language Label Directory',
                'value' => "Language label directory '" . $language_label_directory . "' have proper permission",
                'severity' => REQUIREMENT_OK
            );
        }
    }
    if (!is_dir($upload_directory)) {
        $requirements['upload_directory'] = array(
            'title' => 'Upload Directory',
            'value' => "Upload directory does not exist",
            'severity' => REQUIREMENT_ERROR,
            'description' => 'The project installer requires upload - "' . $upload_directory . '" directory during the installation process.',
        );
    } else {
        $upload_directory_res = smartPermission($upload_directory);
        if (!$upload_directory_res['success']) {
            $requirements['upload_directory'] = array(
                'title' => 'Upload Directory',
                'value' => "Upload directory doesn't have 0755 or 0777 permission",
                'severity' => REQUIREMENT_ERROR,
                'description' => 'The project installer requires write permissions to "' . $upload_directory . '" directory to run the project properly.',
            );
        } else {
            $requirements['upload_directory'] = array(
                'title' => 'Upload Directory',
                'value' => "Upload directory '" . $upload_directory . "' have proper permission",
                'severity' => REQUIREMENT_OK
            );
        }
    }
    if (!is_dir($installer_assets_directory)) {
        $requirements['installer_assets_directory'] = array(
            'title' => 'Installer Assets Directory',
            'value' => "Installer assets directory does not exist",
            'severity' => REQUIREMENT_ERROR,
            'description' => 'The project installer requires assets - "' . $installer_assets_directory . '" directory during the installation process.',
        );
    } else {
        $installer_assets_directory_res = smartPermission($installer_assets_directory);
        if (!$installer_assets_directory_res['success']) {
            $requirements['installer_assets_directory'] = array(
                'title' => 'Installer Assets Directory',
                'value' => "Installer assets directory doesn't have 0755 or 0777 permission",
                'severity' => REQUIREMENT_ERROR,
                'description' => 'The project installer requires write permissions to "' . $installer_assets_directory . '" directory to run the project properly.',
            );
        } else {
            $requirements['installer_assets_directory'] = array(
                'title' => 'Installer Assets Directory',
                'value' => "Installer assets directory '" . $installer_assets_directory . "' have proper permission",
                'severity' => REQUIREMENT_OK
            );
        }
    }
    if (!is_dir($installer_temp_assets_directory)) {
        $requirements['installer_temp_assets_directory'] = array(
            'title' => 'Installer Assets Temp Directory',
            'value' => "Installer assets temp directory does not exist",
            'severity' => REQUIREMENT_ERROR,
            'description' => 'The project installer requires assets - "' . $installer_temp_assets_directory . '" directory during the installation process.',
        );
    } else {
        $installer_temp_assets_directory_res = smartPermission($installer_temp_assets_directory);
        if (!$installer_temp_assets_directory_res['success']) {
            $requirements['installer_temp_assets_directory'] = array(
                'title' => 'Installer Assets Temp Directory',
                'value' => "Installer assets temp Directory doesn't have 0755 or 0777 permission",
                'severity' => REQUIREMENT_ERROR,
                'description' => 'The project installer requires write permissions to "' . $installer_temp_assets_directory . '" directory to run the project properly.',
            );
        }
    }
    return $requirements;
}

function requirement_status_report_old($requirements)
{
    if (!is_array($requirements) || count($requirements) <= 0) {
        return "";
    }

    $severities = array(
        REQUIREMENT_INFO => array(
            'title' => 'Info',
            'class' => 'info',
        ),
        REQUIREMENT_OK => array(
            'title' => 'OK',
            'class' => 'ok',
        ),
        REQUIREMENT_WARNING => array(
            'title' => 'Warning',
            'class' => 'warning',
        ),
        REQUIREMENT_ERROR => array(
            'title' => 'Error',
            'class' => 'error',
        ),
    );
    $output = '<table class="system-status-report" cellspacing=0 >';

    foreach ($requirements as $requirement) {
        //if (empty($requirement['#type'])) {
        $severity = $severities[isset($requirement['severity']) ? (int) $requirement['severity'] : REQUIREMENT_OK];
        $severity['icon'] = '<div title="' . $severity['title'] . '"><span class="element-invisible">' . $severity['title'] . '</span></div>';

        // Output table row(s)
        if (!empty($requirement['description'])) {
            $output .= '<tr class="' . $severity['class'] . ' merge-down"><td class="status-icon">' . $severity['icon'] . '</td><td class="status-title">' . $requirement['title'] . '</td><td class="status-value">' . $requirement['value'] . '</td></tr>';
            $output .= '<tr class="' . $severity['class'] . ' merge-up"><td colspan="3" class="status-description">' . $requirement['description'] . '</td></tr>';
        } else {
            $output .= '<tr class="' . $severity['class'] . '"><td class="status-icon">' . $severity['icon'] . '</td><td class="status-title">' . $requirement['title'] . '</td><td class="status-value">' . $requirement['value'] . '</td></tr>';
        }
        // }
    }

    $output .= '</table>';
    return $output;
}

function requirement_status_report($requirements)
{
    global $installer_image_url;
    if (!is_array($requirements) || count($requirements) <= 0) {
        return "";
    }

    $severities = array(
        REQUIREMENT_INFO => array(
            'title' => 'Info',
            'class' => '',
            'icon' => 'success_icon.png'
        ),
        REQUIREMENT_OK => array(
            'title' => 'OK',
            'class' => '',
            'icon' => 'success_icon.png'
        ),
        REQUIREMENT_WARNING => array(
            'title' => 'Warning',
            'class' => 'alert',
            'icon' => 'alert_icon.png'
        ),
        REQUIREMENT_ERROR => array(
            'title' => 'Error',
            'class' => 'error',
            'icon' => 'error_icon.png'
        ),
    );


    $output = "";
    foreach ($requirements as $requirement) {

        $severity = $severities[isset($requirement['severity']) ? (int) $requirement['severity'] : REQUIREMENT_OK];
        $output .= '<div class="' . $severity['class'] . '">';

        $severity['icon'] = '<img src="' . $installer_image_url . $severity['icon'] . '" alt="' . $severity['title'] . '">';

        // Output table row(s)
        $output .= '<b>' . $requirement['title'] . '</b>';
        $output .= $severity['icon'];
        $output .= "<span>" . $requirement['value'] . '';
        if (!empty($requirement['description'])) {
            $output .= '<br>' . $requirement['description'];
        }
        $output .= '</span>';
        $output .= '</div>';
    }


    return $output;
}

function hb_requirements_severity($requirements)
{
    $severity = REQUIREMENT_OK;
    foreach ($requirements as $requirement) {
        if (isset($requirement['severity'])) {
            $severity = max($severity, $requirement['severity']);
        }
    }
    return $severity;
}

function pre_page_conditions($page, $redirect_flag = false)
{
    global $installer_url;
    $install_process_arr = (is_array($_SESSION['install_process_arr'])) ? $_SESSION['install_process_arr'] : array();
    $success = true;
    switch ($page) {
        case "database" :
            if (!is_array($install_process_arr) || count($install_process_arr) <= 0 || $install_process_arr['installation']['step1'] != 1 || $install_process_arr['installation']['step2'] != 1) {
                $success = false;
            }
            $redirect_url = $installer_url . "index.php?step=2&_t=" . time();
            break;
        case "databasereview" :
            if (!is_array($install_process_arr) || count($install_process_arr) <= 0 || $install_process_arr['installation']['step1'] != 1 || $install_process_arr['installation']['step2'] != 1 || $install_process_arr['installation']['step3'] != 1) {
                $success = false;
            }
            $redirect_url = $installer_url . "index.php?step=3&_t=" . time();
            break;
    }
    if ($success) {
        return $success;
    } else {
        if ($redirect_flag) {

            header("Location:" . $redirect_url);
            exit;
        } else {
            return $success;
        }
    }
}

function rewrite_database($db_detail = array())
{
    global $site_path;
    $ret_arr['success'] = 0;
    $database_file = $site_path . "application/config/database.php";

    $vDatabaseType = $db_detail['vDatabaseType'];
    if ($vDatabaseType == 'mysql') {
        $vDatabaseType = 'mysqli';
    } else if ($vDatabaseType == 'pgsql') {
        $vDatabaseType = 'postgre';
    } else if ($vDatabaseType == 'sqlsrv') {
        $vDatabaseType = 'sqlsrv';
    }
    $vDatabaseHostName = $db_detail['vDatabaseHostName'];
    $vDatabaseUserName = $db_detail['vDatabaseUserName'];
    $vDatabasePassword = $db_detail['vDatabasePassword'];
    $vDatabaseName = $db_detail['vDatabaseName'];
    $vDatabasePort = $db_detail['vDatabasePort'];

    $database_code = <<<EOD
<?php
    
defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------
  | DATABASE CONNECTIVITY SETTINGS
  | -------------------------------------------------------------------
  | This file will contain the settings needed to access your database.
  |
  | For complete instructions please consult the 'Database Connection'
  | page of the User Guide.
  |
  | -------------------------------------------------------------------
  | EXPLANATION OF VARIABLES
  | -------------------------------------------------------------------
  |
  |	['dsn']      The full DSN string describe a connection to the database.
  |	['hostname'] The hostname of your database server.
  |	['username'] The username used to connect to the database
  |	['password'] The password used to connect to the database
  |	['database'] The name of the database you want to connect to
  |	['dbdriver'] The database driver. e.g.: mysqli.
  |			Currently supported:
  |				 cubrid, ibase, mssql, mysql, mysqli, oci8,
  |				 odbc, pdo, postgre, sqlite, sqlite3, sqlsrv
  |	['dbprefix'] You can add an optional prefix, which will be added
  |				 to the table name when using the  Query Builder class
  |	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
  |	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
  |	['cache_on'] TRUE/FALSE - Enables/disables query caching
  |	['cachedir'] The path to the folder where cache files should be stored
  |	['char_set'] The character set used in communicating with the database
  |	['dbcollat'] The character collation used in communicating with the database
  |				 NOTE: For MySQL and MySQLi databases, this setting is only used
  | 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
  |				 (and in table creation queries made with DB Forge).
  | 				 There is an incompatibility in PHP with mysql_real_escape_string() which
  | 				 can make your site vulnerable to SQL injection if you are using a
  | 				 multi-byte character set and are running versions lower than these.
  | 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
  |	['swap_pre'] A default table prefix that should be swapped with the dbprefix
  |	['encrypt']  Whether or not to use an encrypted connection.
  |	['compress'] Whether or not to use client compression (MySQL only)
  |	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
  |							- good for ensuring strict SQL while developing
  |	['failover'] array - A array with 0 or more data for connections if the main should fail.
  |	['save_queries'] TRUE/FALSE - Whether to "save" all executed queries.
  | 				NOTE: Disabling this will also effectively disable both
  | 				\$this->db->last_query() and profiling of DB queries.
  | 				When you run a query, with this setting set to TRUE (default),
  | 				CodeIgniter will store the SQL statement for debugging purposes.
  | 				However, this may cause high memory usage, especially if you run
  | 				a lot of SQL queries ... disable this to avoid that problem.
  |
  | The \$active_group variable lets you choose which connection group to
  | make active.  By default there is only one group (the 'default' group).
  |
  | The \$query_builder variables lets you determine whether or not to load
  | the query builder class.
 */

\$active_group = 'default';
\$query_builder = TRUE;

\$db['default'] = array(
    'dsn' => '',
    'hostname' => '{$vDatabaseHostName}',
    'username' => '{$vDatabaseUserName}',
    'password' => '{$vDatabasePassword}',
    'database' => '{$vDatabaseName}',
    'port' => '{$vDatabasePort}',
    'dbdriver' => '{$vDatabaseType}',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => TRUE,
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

/* End of file database.php */
/* Location: ./application/config/database.php */
EOD;


    $fp = fopen($database_file, 'w');
    if ($fp) {
        $written = fwrite($fp, $database_code);
        @fclose($fp);

        if (!$written) {
            $ret_arr['success'] = 0;
            $ret_arr['message'] = "error in writing database file";
        } else {
            $ret_arr['success'] = 1;
        }
    } else {
        $ret_arr['success'] = 0;
        $ret_arr['message'] = "error in writing database file";
    }
    return $ret_arr;
}

function smartPermission($source)
{
    try {
        $options = get_file_permission();
        $result = false;
        $fPermission = $fOwner = false;
        $ret_arr['success'] = 0;

        if (is_file($source)) {
            $fPermission = @chmod($source, $options['filePermission']);
            $fOwner = @chown($source, $options['fileOwner']);
            clearstatcache();
            $file_permission = substr(sprintf('%o', fileperms($source)), -4);
            if ($file_permission != "0777" && $file_permission != "0755") {
                throw new Exception("File doesn't have proper permission or file owner rights.Give 0775 or 0777 Permission ");
            }
            $ret_arr['success'] = 1;
        } elseif (is_dir($source)) {
            $fPermission = @chmod($source, $options['folderPermission']);
            $fOwner = @chown($source, $options['fileOwner']);
            clearstatcache();
            $file_permission = substr(sprintf('%o', fileperms($source)), -4);

            if ($file_permission != "0777" && $file_permission != "0755") {
                throw new Exception("Directory doesn't have proper permission or file owner rights.Give 0775 or 0777 Permission to this directory and all its sub directory");
            }
            $dir_handle = opendir($source);
            while ($file = readdir($dir_handle)) {
                if ($file != "." && $file != "..") {
                    $result = smartPermission(rtrim($source, "/") . "/" . $file);
                }
            }
            closedir($dir_handle);
            $ret_arr['success'] = 1;
        } else {
            throw new Exception("source file/directory not found");
        }
    } catch (Exception $e) {
        $error_msg = $e->getMessage();
        $ret_arr['success'] = 0;
        $ret_arr['message'] = $error_msg;
    }
    return $ret_arr;
}

function get_file_permission()
{
    $permission_arr['folderPermission'] = 0755;
    $permission_arr['filePermission'] = 0755;
    $permission_arr['fileOwner'] = get_current_user();
    return $permission_arr;
}

function get_mysql_version()
{
    exec('mysql -V', $output, $ret_arr);
    $version = "";
    if (is_array($output) && count($output) > 0) {
        preg_match('@[0-9]+\.[0-9]+\.[0-9]+@', $output[0], $version);
        $version = $version[0];
    }

    return $version;
}

function check_exec()
{
    $flag = false;
    if (@exec('echo EXEC') == 'EXEC') {
        $flag = true;
    }
    return $flag;
}

function get_required_extension()
{
    $required_extensions = array(
        'date',
        'dom',
        'filter',
        'gd',
        'hash',
        'json',
        'pcre',
        'session',
        'SimpleXML',
        'SPL',
        'xml',
        'curl'
    );
    return $required_extensions;
}

function imageMagicIdentify()
{
    global $installer_url;
    $flag = true;
    $image_file = $installer_url . "assets/temp/ok.png";
    $command = IMAGE_MAGIC_INSTALL_DIR . "/identify -verbose '" . $image_file . "'";
    exec($command, $returnarray, $returnvalue);
    if ($returnvalue) {
        $flag = false;
    }
    return $flag;
}

function imageMagicConvert()
{
    global $installer_url, $installer_path;
    $flag = true;
    $image_file = $installer_path . "assets/temp/ok.png";
    $temp_image_file = $installer_path . "assets/temp/ok_temp.png";
    $command = IMAGE_MAGIC_INSTALL_DIR . "/convert -colorspace RGB -quality 70 '" . $image_file . "' '" . $temp_image_file . "'";
    exec($command, $returnarray, $returnvalue);
    if ($returnvalue) {
        $flag = false;
    }
    if (file_exists($temp_image_file)) {
        @unlink($temp_image_file);
    }
    return $flag;
}

function pr($var, $ex = 0)
{
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    if ($ex == 0) {
        exit;
    }
}
