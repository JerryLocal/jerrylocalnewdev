<?php
session_start();
include_once("../installer_settings.php");
include_once("functions.php");
$post_arr = $_POST;
$installer_step = $post_arr['installer_step'];
$action_array = array("dbselection", "requirementchecking", "databasechecking", "databasereview");
global $installer_sql_path;
if (!is_array($post_arr) || count($post_arr) <= 0 || !in_array($installer_step, $action_array)) {
    header("Location:" . $installer_url);
    exit;
}

if ($installer_step == 'dbselection') {
    #pr($post_arr);
    $ret['success'] = 0;
    try {
        if (!empty($post_arr['vDatabaseType']) && in_array($post_arr['vDatabaseType'], array('mysql', 'sqlsrv', 'pgsql'))) {
            $_SESSION['install_process_arr']['db_detail']['vDatabaseType'] = $post_arr['vDatabaseType'];
            header("Location:" . $installer_url . "index.php?step=2"); exit;
        } else {
            throw new Exception('Please select database.');
        }
    } catch (Exception $exc) {
        echo $exc->getMessage();
        $ret['message'] = $error_msg;
    }
} else if ($installer_step == "requirementchecking") {
    $success_arr['step1'] = 1;
    $success_arr['step2'] = 1;

    $_SESSION['install_process_arr']['installation'] = $success_arr;
    header("Location:" . $installer_url . "index.php?step=3");
    exit;
} else if ($installer_step == "databasechecking") {
    $database_type = $post_arr['vDatabaseType'];
    $requirements_checking_arr = install_check_requirements($database_type);
    $severity = hb_requirements_severity($requirements_checking_arr);
    $proceed_ahead = true;
    if ($severity == REQUIREMENT_ERROR) {
        $proceed_ahead = false;
    }
    $ret['success'] = 0;
    $install_process_arr = $_SESSION['install_process_arr'];
    try {
        
        if($_SESSION['install_process_arr']['db_detail']['vDatabaseType']!=$database_type){
            throw new Exception("The database type specified is not same as selected in step 1");
        }
        
        if (!$proceed_ahead) {
            $ret['success'] = 2;
            throw new Exception("some requirement are not sufficient to install project");
        }

        $database_checking = pre_page_conditions("database");
        if ($database_checking) {


            $vDatabaseType = $db_detail['vDatabaseType'] = trim($post_arr['vDatabaseType']);
            $vDatabaseHostName = $db_detail['vDatabaseHostName'] = trim($post_arr['vDatabaseHostName']);
            $vDatabaseUserName = $db_detail['vDatabaseUserName'] = trim($post_arr['vDatabaseUserName']);
            $vDatabasePassword = $db_detail['vDatabasePassword'] = trim($post_arr['vDatabasePassword']);
            $vDatabaseName = $db_detail['vDatabaseName'] = trim($post_arr['vDatabaseName']);
            $vDatabasePort = $db_detail['vDatabasePort'] = trim($post_arr['vDatabasePort']);

            if ($vDatabaseHostName != "" && $vDatabaseUserName != "" && $vDatabaseName != "") {
                require_once($installer_path . "DBConnection.php");

                switch ($database_type) {
                    case 'mysql':
                        $dbconnection = new mysqlclass();
                        $db_response = @$dbconnection->mysqli_connection($vDatabaseHostName, $vDatabaseName, $vDatabaseUserName, $vDatabasePassword, $vDatabasePort);
                        if ($db_response['success']) {
                            $db_tables = $dbconnection->getDBTables();
                            $mysql_version = $dbconnection->getMySqlVersion();
                            if (version_compare($mysql_version, MYSQL_VERSION) < 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your MySQL Version is too old. project installation requires at least MySQL " . MYSQL_VERSION);
                            }

                            if (is_array($db_tables) && count($db_tables) > 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your database is already contain tables.Please make sure that your database must not have any tables while installing project. ");
                            }

                            if (file_exists($installer_sql_path)) {
//                        $ret['success'] = 0;
//                         throw new Exception("proper testing");

                                $ret['success'] = 1;
                                $success_arr['step1'] = 1;
                                $success_arr['step2'] = 1;
                                $success_arr['step3'] = 1;
                                $_SESSION['install_process_arr']['installation'] = $success_arr;
                                $_SESSION['install_process_arr']['db_detail'] = $db_detail;
                            } else {
                                $ret['success'] = 0;
                                throw new Exception("project.sql not found in your downloaded code");
                            }
                        } else {
                            $ret['success'] = 0;
                            throw new Exception($db_response['message']);
                        }
                        break;

                    case 'pgsql':
                        $dbconnection = new pgsqlclass();
                        $db_response = @$dbconnection->pgsql_connection($vDatabaseHostName, $vDatabaseName, $vDatabaseUserName, $vDatabasePassword, $vDatabasePort);
//                        pr($db_response);die;
                        if ($db_response['success']) {
                            $db_tables = $dbconnection->getDBTables($db_response['connection']);
                            $pgsql_version = $dbconnection->getPgSqlVersion($db_response['connection']);

                            if (version_compare($pgsql_version['server'], PGSQL_VERSION) < 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your PgSQL Version is too old. project installation requires at least PgSQL " . PGSQL_VERSION);
                            }

                            if (is_array($db_tables) && count($db_tables) > 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your database is already contain tables.Please make sure that your database must not have any tables while installing project. ");
                            }
                            global $installer_pgsql_path;
                            if (file_exists($installer_pgsql_path)) {
                                $ret['success'] = 1;
                                $success_arr['step1'] = 1;
                                $success_arr['step2'] = 1;
                                $success_arr['step3'] = 1;
                                $_SESSION['install_process_arr']['installation'] = $success_arr;
                                $_SESSION['install_process_arr']['db_detail'] = $db_detail;
                            } else {
                                $ret['success'] = 0;
                                throw new Exception("migrate_pgsql.sql not found in your downloaded code");
                            }
                        } else {
                            $ret['success'] = 0;
                            throw new Exception($db_response['message']);
                        }

                        break;
                    case 'sqlsrv':
                        $dbconnection = new sqlsrvclass();
                        $db_response = @$dbconnection->sqlsrv_connection($vDatabaseHostName, $vDatabaseName, $vDatabaseUserName, $vDatabasePassword, $vDatabasePort);
                        if ($db_response['success']) {
                            $db_tables = $dbconnection->getDBTables();
                            $sqlsrv_version = $dbconnection->getSqlSrvVersion();

                            if (version_compare($sqlsrv_version['SQLServerVersion'], SQLSRV_VERSION) < 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your SQLServer Version is too old. project installation requires at least PgSQL " . SQLSRV_VERSION);
                            }

                            if (is_array($db_tables) && count($db_tables) > 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your database is already contain tables.Please make sure that your database must not have any tables while installing project. ");
                            }
                            global $installer_sqlsrv_path;
                            if (file_exists($installer_sqlsrv_path)) {
                                $ret['success'] = 1;
                                $success_arr['step1'] = 1;
                                $success_arr['step2'] = 1;
                                $success_arr['step3'] = 1;
                                $_SESSION['install_process_arr']['installation'] = $success_arr;
                                $_SESSION['install_process_arr']['db_detail'] = $db_detail;
                            } else {
                                $ret['success'] = 0;
                                throw new Exception("migrate_sqlsrv.sql not found in your downloaded code");
                            }
                        } else {
                            $ret['success'] = 0;
                            throw new Exception($db_response['message']);
                        }

                        break;
                }
            }
        } else {
            $ret['success'] = 2;
            throw new Exception("Please complete your previous installation steps!!");
        }
    } catch (Exception $e) {
        $error_msg = $e->getMessage();
        $ret['message'] = $error_msg;
    }

    if ($ret['success'] == 1) {
        $redirect_url = $installer_url . "index.php?step=4&vDatabaseType=" . $database_type;
    } elseif ($ret['success'] == 2) {
        $redirect_url = $installer_url . "index.php?step=2&vDatabaseType=" . $database_type;
    } else {
        $redirect_url = $installer_url . "index.php?step=3";
    }
    $ret['redirect_url'] = $redirect_url;
    echo json_encode($ret);
    exit;
} else if ($installer_step == "databasereview") {

    $database_type = $post_arr['vDatabaseType'];
    $requirements_checking_arr = install_check_requirements($database_type);
    $severity = hb_requirements_severity($requirements_checking_arr);
    $proceed_ahead = true;
    if ($severity == REQUIREMENT_ERROR) {
        $proceed_ahead = false;
    }
    $ret['success'] = 0;
    $install_process_arr = $_SESSION['install_process_arr'];
    try {
        if (!$proceed_ahead) {
            $ret['success'] = 2;
            throw new Exception("some requirement are not sufficient to install project");
        }

        $database_checking = pre_page_conditions("databasereview");
        if ($database_checking) {

            $vDatabaseType = $db_detail['vDatabaseType'] = trim($post_arr['vDatabaseType']);
            $vDatabaseHostName = $db_detail['vDatabaseHostName'] = trim($post_arr['vDatabaseHostName']);
            $vDatabaseUserName = $db_detail['vDatabaseUserName'] = trim($post_arr['vDatabaseUserName']);
            $vDatabasePassword = $db_detail['vDatabasePassword'] = trim($post_arr['vDatabasePassword']);
            $vDatabaseName = $db_detail['vDatabaseName'] = trim($post_arr['vDatabaseName']);
            $vDatabasePort = $db_detail['vDatabasePort'] = trim($post_arr['vDatabasePort']);

            if ($vDatabaseHostName != "" && $vDatabaseUserName != "" && $vDatabaseName != "") {
                require_once($installer_path . "DBConnection.php");

                switch ($database_type) {

                    case 'pgsql':
                        $dbconnection = new pgsqlclass();
                        $db_response = @$dbconnection->pgsql_connection($vDatabaseHostName, $vDatabaseName, $vDatabaseUserName, $vDatabasePassword, $vDatabasePort);
                        if ($db_response['success']) {
                            $db_tables = $dbconnection->getDBTables();
                            $pgsql_version = $dbconnection->getPgSqlVersion();

                            if (version_compare($pgsql_version, PGSQL_VERSION) < 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your PgSQL Version is too old. project installation requires at least PgSQL " . PGSQL_VERSION);
                            }

                            if (is_array($db_tables) && count($db_tables) > 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your database is already contain tables.Please make sure that your database must not have any tables while installing project. ");
                            }

                            $dbobj = $db_response['connection'];
                            global $installer_pgsql_path;
                            if (file_exists($installer_pgsql_path)) {
                                $response = $dbconnection->import_database_file($installer_pgsql_path);
                                if ($response['success']) {
                                    $file_res = rewrite_database($db_detail);
                                    if ($file_res['success']) {
                                        $admin_data = $dbconnection->getSuperAdminData();
                                        $password_str = "";
                                        if ($admin_data[0]['vPassword'] != "") {
                                            $password = base64_encode($admin_data[0]['vPassword']);
                                            $password_str = "&p=" . $password;
                                        }
                                        $ret['success'] = 1;
                                        session_destroy();
                                        $error_msg_str = "&success=1" . $password_str;
                                    } else {
                                        $ret['success'] = 0;
                                        throw new Exception($file_res['message']);
                                    }
                                } else {
                                    $ret['success'] = 0;
                                    throw new Exception($response['message']);
                                }
                            } else {
                                $ret['success'] = 0;
                                throw new Exception("project.sql not found in your downloaded code");
                            }
                        } else {
                            $ret['success'] = 0;
                            throw new Exception($db_response['message']);
                        }
                        break;
                    case 'sqlsrv':

                        $dbconnection = new sqlsrvclass();
                        $db_response = @$dbconnection->sqlsrv_connection($vDatabaseHostName, $vDatabaseName, $vDatabaseUserName, $vDatabasePassword, $vDatabasePort);

                        if ($db_response['success']) {

                            $db_tables = $dbconnection->getDBTables();

                            $sqlsrv_version = $dbconnection->getSqlSrvVersion();

                            if (version_compare($sqlsrv_version, SQLSRV_VERSION) < 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your SQLServer Version is too old. project installation requires at least SQLServer " . SQLSRV_VERSION);
                            }

                            if (is_array($db_tables) && count($db_tables) > 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your database is already contain tables.Please make sure that your database must not have any tables while installing project. ");
                            }

                            $dbobj = $db_response['connection'];
                            global $installer_sqlsrv_path;
                            if (file_exists($installer_sqlsrv_path)) {
                                $response = $dbconnection->import_database_file($installer_sqlsrv_path);
                                if ($response['success']) {
                                    $file_res = rewrite_database($db_detail);
                                    if ($file_res['success']) {
                                        $admin_data = $dbconnection->getSuperAdminData();
                                        $password_str = "";
                                        if ($admin_data[0]['vPassword'] != "") {
                                            $password = base64_encode($admin_data[0]['vPassword']);
                                            $password_str = "&p=" . $password;
                                        }
                                        $ret['success'] = 1;
                                        session_destroy();
                                        $error_msg_str = "&success=1" . $password_str;
                                    } else {
                                        $ret['success'] = 0;
                                        throw new Exception($file_res['message']);
                                    }
                                } else {
                                    $ret['success'] = 0;
                                    throw new Exception($response['message']);
                                }
                            } else {
                                $ret['success'] = 0;
                                throw new Exception("migrate_sqlsrv.sql not found in your downloaded code");
                            }
                        } else {
                            $ret['success'] = 0;
                            throw new Exception($db_response['message']);
                        }
                        break;
                    case 'mysql':
                    default :
                        $dbconnection = new mysqlclass();
                        $db_response = @$dbconnection->mysqli_connection($vDatabaseHostName, $vDatabaseName, $vDatabaseUserName, $vDatabasePassword, $vDatabasePort);
                        if ($db_response['success']) {
                            $db_tables = $dbconnection->getDBTables();
                            $mysql_version = $dbconnection->getMySqlVersion();
                            if (version_compare($mysql_version, MYSQL_VERSION) < 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your MySQL Version is too old. project installation requires at least MySQL " . MYSQL_VERSION);
                            }

                            if (is_array($db_tables) && count($db_tables) > 0) {
                                $ret['success'] = 0;
                                throw new Exception("Your database is already contain tables.Please make sure that your database must not have any tables while installing project. ");
                            }

                            $dbobj = $db_response['connection'];
                            if (file_exists($installer_sql_path)) {
                                $response = $dbconnection->import_database_file($installer_sql_path);
                                if ($response['success']) {
                                    $file_res = rewrite_database($db_detail);
                                    if ($file_res['success']) {
                                        $admin_data = $dbconnection->getSuperAdminData();
                                        $password_str = "";
                                        if ($admin_data[0]['vPassword'] != "") {
                                            $password = base64_encode($admin_data[0]['vPassword']);
                                            $password_str = "&p=" . $password;
                                        }
                                        $ret['success'] = 1;
                                        session_destroy();
                                        $error_msg_str = "&success=1" . $password_str;
                                    } else {
                                        $ret['success'] = 0;
                                        throw new Exception($file_res['message']);
                                    }
                                } else {
                                    $ret['success'] = 0;
                                    throw new Exception($response['message']);
                                }
                            } else {
                                $ret['success'] = 0;
                                throw new Exception("project.sql not found in your downloaded code");
                            }
                        } else {
                            $ret['success'] = 0;
                            throw new Exception($db_response['message']);
                        }
                        break;
                }
            } else {
                $ret['success'] = 2;
                throw new Exception("Provided database information is not proper !!");
            }
        } else {
            $ret['success'] = 2;
            throw new Exception("Please complete your previous installation steps!!");
        }
    } catch (Exception $e) {
        $error_msg = $e->getMessage();
        $_SESSION['errmsg'] = $error_msg;
        $ret['message'] = $error_msg;
        //$error_msg_str="&errmsg=".$error_msg;
    }

    if ($ret['success'] == 1) {
        $redirect_url = $installer_url . "index.php?step=5" . $error_msg_str;
    } elseif ($ret['success'] == 2) {
        $redirect_url = $installer_url . "index.php?step=3&_t=" . time();
    } else {
        $redirect_url = $installer_url . "index.php?step=3&_t=" . time();
    }
    $ret['redirect_url'] = $redirect_url;
    echo json_encode($ret);
    exit;
}