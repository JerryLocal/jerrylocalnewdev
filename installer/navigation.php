<div class="step_nav">
    <ul>
        <li class="<?php echo ($install_steps > 1) ? "sucess" : (($install_steps == 1) ? "active" : ""); ?>">
            <span></span>
            <p><b>Getting Started</b><br />Current server environment including Database setup and permissions</p>
        </li>
        <li class="<?php echo ($install_steps > 2) ? "sucess" : (($install_steps == 2) ? "active" : ""); ?>">
            <span></span>
            <p><b>Server<br />Configuration</b><br />checklist of required<br />system configurations</p>
        </li>
        <li class="<?php echo ($install_steps > 3) ? "sucess" : (($install_steps == 3) ? "active" : ""); ?>">
            <span></span>
            <p><b>Database Info</b><br />Provide credentials to access Database.</p>
        </li>
        <li class="<?php echo ($install_steps > 4) ? "sucess" : (($install_steps == 4) ? "active" : ""); ?>">
            <span></span>
            <p><b>Install</b><br />Begin installation of<br /> scripts and database.</p>
        </li>
        <li class="<?php echo ($install_steps > 5) ? "sucess" : (($install_steps == 5) ? "active" : ""); ?>">
            <span></span>
            <p><b>Congratulations!</b><br />You're finished with<br /> installation.</p>
        </li>
    </ul>
    <div class="clear"></div>
</div>