<?php
/*
 * checking for file and system requirements 
 * 
 */
$database_type = $_SESSION['install_process_arr']['db_detail']['vDatabaseType'] != '' ? $_SESSION['install_process_arr']['db_detail']['vDatabaseType'] : $_GET['vDatabaseType'];
if(empty($database_type) || !in_array($database_type, array('mysql','sqlsrv','pgsql'))){
    header("Location: $installer_url");die;
}
$requirements_checking_arr = install_check_requirements($database_type);
/*
 * checking severity  from requirements if its error then do not proceed for the installation process
 * 
 */
$severity = hb_requirements_severity($requirements_checking_arr);

$proceed_ahead = true;
if ($severity == REQUIREMENT_ERROR) {
    $proceed_ahead = false;
}
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="<?php echo $site_path; ?>images/favicon.ico" type="image/x-icon">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Installation Process</title>
        <script>
            var installer_url = '<?php echo $installer_url; ?>';
        </script>
        <link href="<?php echo $installer_url; ?>assets/css/style.css" rel='stylesheet'  type='text/css'/>
        <link href="<?php echo $installer_url; ?>assets/css/base.css" rel='stylesheet'  type='text/css'/>
    </head>
    <body>
        <div id="wrapper" class="wapper">
            <?php include_once("top.php"); ?>
            <?php include_once("navigation.php"); ?>
            <div id="content">
                <!-- middle part start here-->
                <h1>Server Configuration - Status</h1>
                <div class="server_confi">
                    <?php echo requirement_status_report($requirements_checking_arr); ?>
                </div>
            </div>
            <div id="footer">
                <div class="left"><a href="<?php echo $installer_url . "index.php?step=1&_t=" . time() ?>">Back</a></div>
                <div class="right">
                    <?php
                    if ($proceed_ahead) {
                        ?>
                        <form name="frmrequirement" id="frmrequirement" method="post" action="installationaction.php" class="form-horizontal">
                            <input type="hidden" name="installer_step" id="installer_step" value="requirementchecking">
                            <input  type="submit" value="Next" name="requirement_submission" class="smallbutton">
                        </form>    
                        <?php
                    } else {
                        ?>
                        <a href="<?php echo $installer_url . "index.php?step=2&_t=" . time() ?>" title="Recheck Requirement" class="smallbutton recheck" >Recheck</a>
                        <span class="error" style="color:#e62c2d;">Fix your requirements and click here to recheck it again.</span>
                        <?php
                    }
                    ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </body>
</html>