<?php
$required_extensions = get_required_extension();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
            <link rel="shortcut icon" href="<?php echo $site_path; ?>images/favicon.ico" type="image/x-icon" />
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Installation Process</title>
            <script>
                var installer_url = '<?php echo $installer_url; ?>';
            </script>
            <link href="<?php echo $installer_url; ?>assets/css/style.css" rel='stylesheet'  type='text/css'/>
            <link href="<?php echo $installer_url; ?>assets/css/base.css" rel='stylesheet'  type='text/css'/>
    </head>
    <body>
        <div id="wrapper" class="wapper">
            <?php
            include_once("top.php");
            include_once("navigation.php");

            ?>
            <form action="installationaction.php" method="post">
            <div id="content">
                <h1>Geting Started</h1>
                <h2>Server Requirements</h2>
                <p>
                    <strong>PHP</strong> Version must be <strong><?php echo INSTALLER_PHP_VERSION; ?> or higher.</strong><br />
                    <!--<strong>MySQL</strong> Version must be <strong><?php echo MYSQL_VERSION; ?> or higher.</strong><br />-->
                    <strong>PHP EXEC</strong> command must enabled.<br />
                    Following <strong>Extension</strong> should be enabled.<br />
                    <strong style="padding-left:50px;"><?php echo implode(" , ", $required_extensions); ?></strong><br />
                    <strong>Image Magic</strong> extension should be install for image resize functionality.<br />
                    <strong>Mcrypt</strong> extension should be install if you are using any encryption in this project.
                </p>
                <h2>Database Requirements</h2>
                <p>
                    <strong>MySQL</strong> Version must be <strong><?php echo MYSQL_VERSION; ?> or higher.</strong><br />
                    <strong>PgSQL</strong> Version must be <strong><?php echo PGSQL_VERSION; ?> or higher.</strong><br />
                    <strong>SQLServer</strong> Version must be <strong><?php echo SQLSRV_VERSION; ?> or higher.</strong><br />
                    Before getting started, we need some information on the database. You will need to know the following items before proceeding.
                    <br /><strong style="padding-left:0px;">Database server host, Database name, Database username, Database password, Database port</strong><br />
                    Make sure that while installing project database should be blank.<br />
                    This script will automatically create database.php file according to server permission.<br />
                    If you see database configuration file is not automatically created, you shall locate default database file here at - <strong>/application/config/default.database.php</strong> - and rename it as <strong>database.php</strong>
                </p>
                <h2>File Permissions</h2>
                <p>
                    You have to provide 0777 or 0755 permission to <strong>/application/config/database.php file.</strong><br />
                    You have to provide 0777 or 0755 permission to <strong>/application/views/javascript_label.tpl file.</strong><br />
                    You have to provide 0777 or 0755 permission to <strong>/application/cache/ directory.</strong><br />
                    You have to provide 0777 or 0755 permission to <strong>/application/language/ directory.</strong><br />
                    You have to provide 0777 or 0755 permission to  <strong>public/upload/ directory.</strong><br />
                    You have to provide 0777 or 0755 permission to <strong>/installer/assets/ directory.</strong>
                </p>
                
                <h2>Database Type</h2>
                <p>
                    
                    <!--<label>Select Database Type: </label><br>-->
                    <?php
                    $db_detail = $_SESSION['install_process_arr']['db_detail'];
                    ?>
                    <input type="radio" <?php echo ($db_detail['vDatabaseType'] == 'mysql' || $db_detail['vDatabaseType'] == '' ) ? 'checked="checked"' : '' ?> name="vDatabaseType" id="vDatabaseTypeM" value="mysql" />
                    <label for="vDatabaseTypeM">Mysql</label>
                    <br>
                        <input type="radio" <?php echo $db_detail['vDatabaseType'] == 'pgsql' ? 'checked="checked"' : '' ?> name="vDatabaseType" id="vDatabaseTypeP" value="pgsql" />
                    <label for="vDatabaseTypeP">Postgre Sql</label>
                    
                    <br>
                    <input type="radio" <?php echo $db_detail['vDatabaseType'] == 'sqlsrv' ? 'checked="checked"' : '' ?> name="vDatabaseType" id="vDatabaseTypeS" value="sqlsrv" />
                    <label for="vDatabaseTypeS">Sql Server</label>

                    <input type="hidden" name="installer_step" value="dbselection"/>
                </p>
            </div>
            <div id="footer">
                <div class="left">&nbsp;</div>
                <!--<div class="right"><a href="<?php // echo $installer_url . "index.php?step=2"; ?>">Next</a></div>-->
                <div class="right"><input type="submit" name="" value="Next"/></div>
                <div class="clear"></div>
            </div>
            </form>
        </div>
    </body>
</html>