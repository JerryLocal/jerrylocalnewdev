<?php
pre_page_conditions("database", true);
$db_detail = $_SESSION['install_process_arr']['db_detail'];
$not_proceed_ahead = true;
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="<?php echo $site_path; ?>images/favicon.ico" type="image/x-icon">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Installation Process</title>
        <script>
            var installer_url = '<?php echo $installer_url; ?>';
        </script>
        <link href="<?php echo $installer_url; ?>assets/css/style.css" rel='stylesheet'  type='text/css'/>
        <link href="<?php echo $installer_url; ?>assets/css/base.css" rel='stylesheet'  type='text/css'/>
        <script language='JavaScript1.2' src="<?php echo $installer_url; ?>assets/js/jquery.js"></script>
        <script language='JavaScript1.2' src="<?php echo $installer_url; ?>assets/js/jquery.form.js"></script>
        <script language='JavaScript1.2' src="<?php echo $installer_url; ?>assets/js/jquery.validate.min.js"></script>
        <script language='JavaScript1.2' src="<?php echo $installer_url; ?>assets/js/project_setup.js"></script>
    </head>
    <body>
        <div id="wrapper" class="wapper">
            <div class="ajax_qLoverlay"></div>
            <div class="ajax_qLbar"></div>
            <div class="ajax_qLbarText"><strong>Please wait while processing..</strong></div>
            <?php
            include_once("top.php");
            include_once("navigation.php");
            ?>
            <form name="frmdatabase" id="frmdatabase" method="post" action="installationaction.php" class="form-horizontal" onSubmit="return submitform();">
                <div id="content" class="contaniner databasefrm">
                    <h1>Database Information</h1>
                    <div id="text" class="database_info">
                        <div >
                            
                            <label >Database Type <span class="required">*</span></label>
                            <input type="hidden" name="vDatabaseType" value="<?php echo $db_detail['vDatabaseType'] ?>"/>
                            <?php /*<select id="vDatabaseType" name="vDatabaseType">
                                <option <?php echo ($db_detail['vDatabaseType'] == 'mysql' || $db_detail['vDatabaseType'] == '' ) ? 'selected="selected"' : '' ?> value="mysql">MySql</option>
                                <option <?php echo $db_detail['vDatabaseType'] == 'sqlsrv' ? 'selected="selected"' : '' ?> value="sqlsrv">Sql Server</option>
                                <option <?php echo $db_detail['vDatabaseType'] == 'pgsql' ? 'selected="selected"' : '' ?> value="pgsql">PostgreSql Server</option>
                            </select> */?>
                            <?php if($db_detail['vDatabaseType'] == 'mysql'){
                                echo 'MySql';
                            }else if($db_detail['vDatabaseType'] == 'pgsql'){
                                echo 'PostgreSql';
                            }else if($db_detail['vDatabaseType'] == 'sqlsrv'){
                                echo 'SQLServer';
                            } ?>
                            <p>If you wish to change the preferred database then goto step 1.</p>
                            
                        </div>
                        <div >
                            <label >DB server host <span class="required">*</span></label>
                            <input type="text" id="vDatabaseHostName" name="vDatabaseHostName" maxlength="50" size="40" autocomplete="off" value="<?php echo $db_detail['vDatabaseHostName']; ?>" />
                            <div class="frmerrormessage" id="vDatabaseHostNameErr"></div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">DB username <span class="required">*</span></label>
                            <input type="text" id="vDatabaseUserName" name="vDatabaseUserName"  size="40" autocomplete="off" value="<?php echo $db_detail['vDatabaseUserName']; ?>"  />
                            <div class="frmerrormessage" id="vDatabaseUserNameErr"></div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">DB password <span class="required">&nbsp;</span></label>
                            <input type="password" id="vDatabasePassword" name="vDatabasePassword"  size="40" autocomplete="off" value=""/>
                            <div id="vDatabasePasswordErr" class="frmerrormessage"></div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">DB Name <span class="required">*</span></label>
                            <input type="text" id="vDatabaseName" name="vDatabaseName"  size="40" autocomplete="off" value="<?php echo $db_detail['vDatabaseName']; ?>"  />
                            <div id="vDatabaseNameErr" class="frmerrormessage"></div>
                            <p>Mention the name of the database that you want to use for this installation.<br>It must exist on your server before project can be installed.</p>
                            <p class="imp"><b>IMPORTANT NOTE</b><br>Your Database will be OVERWRITTEN completely with this installation.<br>Make sure you've taken necessary backup.</p>
                        </div>
                        <div class="control-group">
                            <label class="control-label">DB Port Number <span class="required">&nbsp;</span></label>
                            <input type="text" id="vDatabasePort" name="vDatabasePort" maxlength="50" size="40" autocomplete="off" value="<?php echo $db_detail['vDatabasePort']; ?>"/>
                            <div class="frmerrormessage" id="vDatabasePortErr"></div>
                        </div>
                    </div>
                </div>
                <div id="footer">
                    <div id="footer_action">
                        <div class="left"><a href="<?php echo $installer_url . "index.php?step=2&_t=" . time() ?>">Back</a></div>
                        <div class="right">
                            <?php
                            if ($not_proceed_ahead) {
                                ?>
                                <input type="hidden" name="installer_step" id="installer_step" value="databasechecking">
                                <input  type="submit" value="Next" name="database_submission" class="smallbutton">
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>