<?php

session_start();
error_reporting(0);
include_once("../installer_settings.php");
include_once("functions.php");

$install_steps = intval((isset($_REQUEST['step'])) ? $_REQUEST['step'] : 0);
$install_steps = ($install_steps > 0) ? $install_steps : 1;
switch ($install_steps) {
    case 1: include_once("requirement.php");
        break;
    case 2: include_once("requirement_checking.php");
        break;
    case 3: include_once("database_checking.php");
        break;
    case 4: include_once("database_review.php");
        break;
    case 5: include_once("thanks.php");
        break;
}