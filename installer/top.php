<div id="header">
    <div class="left"><img src="<?php echo $installer_image_url; ?>logo.png" alt="Configure.IT Help Center" /></div>
    <div class="right">
        <p>Install Wizard<br /></p>
    </div>
</div>
<div class="flashrecmessage" id="successMessage" >
    <?php
    $error_msg = (isset($_SESSION['errmsg']) && $_SESSION['errmsg'] != "") ? $_SESSION['errmsg'] : "";
    unset($_SESSION['errmsg']);
    if ($error_msg != "") {
        ?>
        <div class="flasherrnorec-red"><?php echo $error_msg; ?></div>
        <?php
    }
    ?>
</div>

<!--top-part End here-->