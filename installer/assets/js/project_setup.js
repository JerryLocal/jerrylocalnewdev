$(document).ready(function() {
    $('#frmdatabase').validate({
        rules: {
            'vDatabaseName': {
                required: true
            },
            'vDatabaseHostName': {
                required: true
            },
            'vDatabaseUserName': {
                required: true
            },
            'vDatabasePort': {
                required: false,
                digits: true

            }
        },
        messages: {
            'vDatabaseName': {
                required: 'Please enter a database name'
            },
            'vDatabaseHostName': {
                required: 'Please enter a database host name'
            },
            'vDatabaseUserName': {
                required: 'Please enter a database user name'
            },
            'vDatabasePort': {
                required: 'Please enter a database port',
                digits: 'Please enter valid numeric port number'
            }
        },
        errorPlacement: function(error, element) {
            error.appendTo("#" + $(element).attr('id') + "Err");
            //return false;

        }
    });


    setTimeout(function() {
        $("#successMessage").slideUp();
    }, 7000);
});
function submitform()
{
    if ($('#frmdatabase').valid())
    {
        /*if (confirm("Make sure your database is empty as we are over write your database.\n\n Are you sure you want to setup project ?"))
         {*/

        //document.frmdatabase.submit();
        displayLoader();

        $("#frmdatabase").ajaxSubmit(function(res) {
            if (res != "")
            {
                response = $.parseJSON(res);
                if (response['success'] == 0)
                {
                    hideLoader();
                    displayMessage(response['message'], "error");
                }
                else
                {
                    window.location = response['redirect_url'];
                }
            }

        });
        return false;

    } else {
        return false;
    }
    /*}
     else
     {
     return false;
     }*/
}
function displayLoader()
{
    $("#footer_action").hide();
    $(".ajax_qLoverlay").show();
    $(".ajax_qLbar").show();
    $(".ajax_qLbarText").show();

}
function hideLoader()
{
    $("#footer_action").show();
    $(".ajax_qLoverlay").hide();
    $(".ajax_qLbar").hide();
    $(".ajax_qLbarText").hide();
}
function displayMessage(message, type)
{
    if (type == "error")
    {
        $("#successMessage").html('<div class="flasherrnorec-red">' + message + '</div>');
    } else {
        $("#successMessage").html('<div class="flasherrnorec-green">' + message + '</div>');
    }

    $("#successMessage").slideDown();
    $("html, body").animate({scrollTop: 0});
    setTimeout(function() {
        $("#successMessage").slideUp();
    }, 7000);
}


