<?php

/**
 * This page is useful for Database connection
 */
class mysqlclass
{

    public $DBASE = "";
    public $CONN = "";
    public $_HOSTNAME;
    public $_DATABASE;
    public $_USER;
    public $_PASS;
    public $_PORT;

    function mysqli_connection($server = "", $dbase = "", $user = "", $pass = "", $port = "")
    {
        $port = trim($port);
        $this->_HOSTNAME = $server;
        $this->_DATABASE = $dbase;
        $this->_USER = $user;
        $this->_PASS = $pass;
        $this->DBASE = $dbase;
        $this->_PORT = $port;
        $ret_arr['success'] = 1;
        if ($port != "")
            $conn = @mysqli_connect($server, $user, $pass, $dbase, $port);
        else
            $conn = @mysqli_connect($server, $user, $pass, $dbase);
        if (!$conn) {
            $ret_arr['success'] = 0;
            $ret_arr['message'] = "Database connection attempt failed.Please verify your database username ,password and port number";
        } else {
            if ($dbase != '') {
                if (!$conn->select_db($dbase)) {
                    $ret_arr['success'] = 0;
                    $ret_arr['message'] = "We are not able to connect database.Please verify your database username ,password and port number";
                }
            }
            $this->CONN = $conn;
            $ret_arr['connection'] = $conn;
        }
        return $ret_arr;
    }

    function select_database($dbase)
    {
        $conn = $this->CONN;
        $this->_DATABASE = $dbase;
        $this->DBASE = $dbase;
        if (!$this->CONN->select_db($dbase)) {
            $this->error("Dbase Select failed");
        }
    }

    function close()
    {
        $conn = $this->CONN;
        $close = mysqli_close($this->CONN);
        unset($this->CONN);
        if (!$close) {
            return false;
        }
        return true;
    }

    function executeQuery($sql = "", $query_type = '')
    {
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $query_start_time = microtime(true);
        $result = @mysqli_query($conn, $sql) or die("Query failed: " . $sql . mysqli_error($conn));
        $query_end_time = microtime(true);
        return $result;
    }

    function sql_query($sql = "", $fetch = "mysqli_fetch_assoc")
    {
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = $this->executeQuery($sql, "query");
        if (!$results) {
            $message = "Query went bad!";
            return false;
        }
        if (!@eregi("^select", $sql) && !@eregi("^call", $sql)) {
            return true;
        } else {
            $count = 0;
            $data = array();
            while ($row = @$fetch($results)) {
                $data[$count] = $row;
                $count++;
            }
            @mysqli_free_result($results);
            return $data;
        }
    }

    function insert($sql = "")
    {
        if (empty($sql)) {
            return false;
        }
        if (!@eregi("^insert", $sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = $this->executeQuery($sql, "insert");
        if (!$results) {
            return false;
        }
        $id = @mysqli_insert_id($conn);
        if ($id > 0) {
            return $id;
        } else {
            return $results;
        }
    }

    function update($sql = "")
    {
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = $this->executeQuery($sql, "update");
        if (!$results) {
            return false;
        }
        return $results;
    }

    function delete($sql = "")
    {
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = $this->executeQuery($sql, "delete");
        if (!$results) {
            return false;
        }
        return $results;
    }

    function select($sql = "", $fetch = "mysqli_fetch_assoc", $flag = 'No')
    {
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = $this->executeQuery($sql, "select");
        if ($flag == 'Yes') {
            print_r($results);
        }
        if ((!$results) or ( empty($results))) {
            return false;
        }
        $count = 0;
        $data = array();
        while ($row = $fetch($results)) {
            $data[$count] = $row;
            $count++;
        }
        @mysqli_free_result($results);
        return $data;
    }

    function get_mysql_directory_path()
    {
        $conn = $this->CONN;
        $path = "mysql";
        $detect_mysql_path = $this->select('SELECT @@basedir AS mysql_home');
        if (!is_array($detect_mysql_path) || count($detect_mysql_path) == 0) {
            return $path;
        }
        if ($detect_mysql_path[0]['mysql_home'] == "") {
            return $path;
        }
        $org_path = $detect_mysql_path[0]['mysql_home'];
        $org_path = rtrim($org_path, "/");
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $path = $org_path . "/bin/mysql.exe";
        } else if (strtoupper(substr(PHP_OS, 0, 3)) === 'DAR') {
            $path = $org_path . "/bin/mysql";
        } else {
            $path = $org_path . "/bin/mysql";
        }
        return $path;
    }

    function import_database_file($installer_sql_path)
    {
        $ret_arr['success'] = 0;
        $mysql_path = $this->get_mysql_directory_path();
        $pass_str = ($this->_PASS != "") ? "-p" . $this->_PASS : "";
        $port_str = ($this->_PORT != "") ? "--port=" . $this->_PORT : "";
        $command = $mysql_path . " -u" . $this->_USER . " " . $port_str . " " . $pass_str . " -h" . $this->_HOSTNAME . " " . $this->DBASE . " < " . $installer_sql_path . "";
        exec($command, $out, $error);
        if (!$error) {
            $ret_arr['success'] = 1;
        } else {
            $ret_arr['message'] = "Error in importing database";
        }
        return $ret_arr;
    }

    function getDBTables()
    {
        $sql_tables = "SHOW FULL TABLES WHERE Table_type = 'BASE TABLE'";
        $db_tables = $this->select($sql_tables);
        if (!is_array($db_tables) || count($db_tables) == 0) {
            $db_tables = array();
        }
        return $db_tables;
    }

    function getMySqlVersion()
    {
        $sql_version = "SELECT VERSION() as version";
        $mysql_version_data = $this->select($sql_version);
        $version = "";
        if (is_array($mysql_version_data) && count($mysql_version_data) > 0) {
            $version = $mysql_version_data[0]['version'];
        }
        return $version;
    }

    function getSuperAdminData()
    {
        $sql = "SELECT ma.vUserName, ma.vPassword FROM mod_admin AS ma WHERE ma.vUserName = 'admin'";
        $db_data = $this->select($sql);
        if (!is_array($db_data) || count($db_data) == 0) {
            $db_data = array();
        }
        return $db_data;
    }
}

class pgsqlclass
{

    public $DBASE = "";
    public $CONN = "";
    public $_HOSTNAME;
    public $_DATABASE;
    public $_USER;
    public $_PASS;
    public $_PORT;

    function pgsql_connection($server = "", $dbase = "", $user = "", $pass = "", $port = "")
    {
        $port = trim($port);
        $this->_HOSTNAME = $server;
        $this->_DATABASE = $dbase;
        $this->_USER = $user;
        $this->_PASS = $pass;
        $this->DBASE = $dbase;
        $this->_PORT = $port;
        $ret_arr['success'] = 1;
        $conn = @pg_connect("host=" . $server . " port=" . $port . " user=" . $user . " password=" . $pass . " dbname=" . $dbase);
        if (!$conn) {
            $ret_arr['success'] = 0;
            $ret_arr['message'] = "Database connection attempt failed.Please verify your database username ,password and port number";
        } else {
            $this->CONN = $conn;
            $ret_arr['connection'] = $conn;
        }
        return $ret_arr;
    }

    public function getDBTables()
    {
        $sql_tables = "SELECT quote_ident(table_name) AS table_name,
                       table_schema AS schema_name
                       FROM information_schema.tables
                       WHERE table_schema NOT LIKE 'pg_%'
                       AND table_schema != 'information_schema'
                       AND table_name != 'geometry_columns'
                       AND table_name != 'spatial_ref_sys'
                       AND table_type != 'VIEW'";
        $db_tables = $this->select($sql_tables);
        if (!is_array($db_tables) || count($db_tables) == 0) {
            $db_tables = array();
        }
        return $db_tables;
    }

    public function getPgSqlVersion()
    {
        return pg_version($this->CONN);
    }

    public function select($sql = "", $fetch = "pg_fetch_array", $flag = 'No')
    {
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = $this->executeQuery($sql, "select");
        if ($flag == 'Yes') {
            print_r($results);
        }
        if ((!$results) or ( empty($results))) {
            return false;
        }
        $count = 0;
        $data = array();
        while ($row = $fetch($results)) {
            $data[$count] = $row;
            $count++;
        }
        @mysqli_free_result($results);
        return $data;
    }

    function executeQuery($sql = "", $query_type = '')
    {
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $query_start_time = microtime(true);
        $result = @pg_query($conn, $sql) or die("Query failed: " . $sql . pg_errormessage($conn));
        $query_end_time = microtime(true);
        return $result;
    }

    function import_database_file($installer_sql_path)
    {
        // This whole function isn't very encapsulated, but hey...
        $conn = $this->CONN;
        if (!file_exists($installer_sql_path))
            return false;
        $fd = fopen($installer_sql_path, 'r');
        if (!$fd)
            return false;
        // Build up each SQL statement, they can be multiline
        $query_buf = null;
        $query_start = 0;
        $in_quote = 0;
        $in_xcomment = 0;
        $bslash_count = 0;
        $dol_quote = null;
        $paren_level = 0;
        $len = 0;
        $i = 0;
        $prevlen = 0;
        $thislen = 0;
        $lineno = 0;
        // Loop over each line in the file
        while (!feof($fd)) {
            $line = fgets($fd);
            $lineno++;
            // Nothing left on line? Then ignore...
            if (trim($line) == '')
                continue;
            $len = strlen($line);
            $query_start = 0;
            /*
             * Parse line, looking for command separators.
             *
             * The current character is at line[i], the prior character at line[i
             * - prevlen], the next character at line[i + thislen].
             */
            $prevlen = 0;
            $thislen = ($len > 0) ? 1 : 0;
            for ($i = 0; $i < $len; $this->advance_1($i, $prevlen, $thislen)) {
                /* was the previous character a backslash? */
                if ($i > 0 && substr($line, $i - $prevlen, 1) == '\\')
                    $bslash_count++;
                else
                    $bslash_count = 0;
                /*
                 * It is important to place the in_* test routines before the
                 * in_* detection routines. i.e. we have to test if we are in
                 * a quote before testing for comments.
                 */
                /* in quote? */
                if ($in_quote !== 0) {
                    /*
                     * end of quote if matching non-backslashed character.
                     * backslashes don't count for double quotes, though.
                     */
                    if (substr($line, $i, 1) == $in_quote &&
                        ($bslash_count % 2 == 0 || $in_quote == '"'))
                        $in_quote = 0;
                }
                /* in or end of $foo$ type quote? */
                else if ($dol_quote) {
                    if (strncmp(substr($line, $i), $dol_quote, strlen($dol_quote)) == 0) {
                        $this->advance_1($i, $prevlen, $thislen);
                        while (substr($line, $i, 1) != '$')
                            $this->advance_1($i, $prevlen, $thislen);
                        $dol_quote = null;
                    }
                }
                /* start of extended comment? */ else if (substr($line, $i, 2) == '/*') {
                    $in_xcomment++;
                    if ($in_xcomment == 1)
                        $this->advance_1($i, $prevlen, $thislen);
                }
                /* in or end of extended comment? */
                else if ($in_xcomment) {
                    if (substr($line, $i, 2) == '*/' && !--$in_xcomment)
                        $this->advance_1($i, $prevlen, $thislen);
                }
                /* start of quote? */
                else if (substr($line, $i, 1) == '\'' || substr($line, $i, 1) == '"') {
                    $in_quote = substr($line, $i, 1);
                }
                /*
                 * start of $foo$ type quote?
                 */ else if (!$dol_quote && $this->valid_dolquote(substr($line, $i))) {
                    $dol_end = strpos(substr($line, $i + 1), '$');
                    $dol_quote = substr($line, $i, $dol_end + 1);
                    $this->advance_1($i, $prevlen, $thislen);
                    while (substr($line, $i, 1) != '$') {
                        $this->advance_1($i, $prevlen, $thislen);
                    }
                }
                /* single-line comment? truncate line */ else if (substr($line, $i, 2) == '--') {
                    $line = substr($line, 0, $i); /* remove comment */
                    break;
                }
                /* count nested parentheses */ else if (substr($line, $i, 1) == '(') {
                    $paren_level++;
                } else if (substr($line, $i, 1) == ')' && $paren_level > 0) {
                    $paren_level--;
                }
                /* semicolon? then send query */ else if (substr($line, $i, 1) == ';' && !$bslash_count && !$paren_level) {
                    $subline = substr(substr($line, 0, $i), $query_start);
                    /* is there anything else on the line? */
                    if (strspn($subline, " \t\n\r") != strlen($subline)) {
                        /*
                         * insert a cosmetic newline, if this is not the first
                         * line in the buffer
                         */
                        if (strlen($query_buf) > 0)
                            $query_buf .= "\n";
                        /* append the line to the query buffer */
                        $query_buf .= $subline;
                        $query_buf .= ';';
                        // Execute the query. PHP cannot execute
                        // empty queries, unlike libpq
//                        echo $query_buf."\n+\n+\n";//die;
                        $res = @pg_query($conn, $query_buf);
                        // Call the callback function for display
                        if ($callback !== null)
                            $callback($query_buf, $res, $lineno);
                        // Check for COPY request
                        if (pg_result_status($res) == 4) { // 4 == PGSQL_COPY_FROM
                            while (!feof($fd)) {
                                $copy = fgets($fd, 32768);
                                $lineno++;
                                pg_put_line($conn, $copy);
                                if ($copy == "\\.\n" || $copy == "\\.\r\n") {
                                    pg_end_copy($conn);
                                    break;
                                }
                            }
                        }
                    }
                    $query_buf = null;
                    $query_start = $i + $thislen;
                }
                /*
                 * keyword or identifier?
                 * We grab the whole string so that we don't
                 * mistakenly see $foo$ inside an identifier as the start
                 * of a dollar quote.
                 */
                // XXX: multibyte here
                else if (preg_match('/^[_[:alpha:]]$/', substr($line, $i, 1))) {
                    $sub = substr($line, $i, $thislen);
                    while (preg_match('/^[\$_A-Za-z0-9]$/', $sub)) {
                        /* keep going while we still have identifier chars */
                        $this->advance_1($i, $prevlen, $thislen);
                        $sub = substr($line, $i, $thislen);
                    }
                    // Since we're now over the next character to be examined, it is necessary
                    // to move back one space.
                    $i-=$prevlen;
                }
            } // end for
            /* Put the rest of the line in the query buffer. */
            $subline = substr($line, $query_start);
            if ($in_quote || $dol_quote || strspn($subline, " \t\n\r") != strlen($subline)) {
                if (strlen($query_buf) > 0)
                    $query_buf .= "\n";
                $query_buf .= $subline;
            }
            $line = null;
        } // end while
        /*
         * Process query at the end of file without a semicolon, so long as
         * it's non-empty.
         */
        if (strlen($query_buf) > 0 && strspn($query_buf, " \t\n\r") != strlen($query_buf)) {
            // Execute the query
            $res = @pg_query($conn, $query_buf);
            // Call the callback function for display
            if ($callback !== null)
                $callback($query_buf, $res, $lineno);
            // Check for COPY request
            if (pg_result_status($res) == 4) { // 4 == PGSQL_COPY_FROM
                while (!feof($fd)) {
                    $copy = fgets($fd, 32768);
                    $lineno++;
                    pg_put_line($conn, $copy);
                    if ($copy == "\\.\n" || $copy == "\\.\r\n") {
                        pg_end_copy($conn);
                        break;
                    }
                }
            }
        }
        fclose($fd);
        $ret_arr['success'] = 1;
        return $ret_arr;
    }

    private function advance_1(&$i, &$prevlen, &$thislen)
    {
        $prevlen = $thislen;
        $i += $thislen;
        $thislen = 1;
    }

    private function valid_dolquote($dquote)
    {

        return (preg_match('/^[$][$]/', $dquote) || preg_match('/^[$][_[:alpha:]][_[:alnum:]]*[$]/', $dquote));
    }

    function getSuperAdminData()
    {
        $sql = 'SELECT "vUserName", "vPassword" FROM "mod_admin" WHERE "vUserName" = ' . "'admin'";
        $db_data = $this->select($sql);
        if (!is_array($db_data) || count($db_data) == 0) {
            $db_data = array();
        }
        return $db_data;
    }
}

class sqlsrvclass
{

    public $DBASE = "";
    public $CONN = "";
    public $_HOSTNAME;
    public $_DATABASE;
    public $_USER;
    public $_PASS;
    public $_PORT;

    function sqlsrv_connection($server = "", $dbase = "", $user = "", $pass = "", $port = "")
    {
        $port = trim($port);
        $this->_HOSTNAME = $server;
        $this->_DATABASE = $dbase;
        $this->_USER = $user;
        $this->_PASS = $pass;
        $this->DBASE = $dbase;
        $this->_PORT = $port;
        $ret_arr['success'] = 1;
        $connection_info = array("UID" => $this->_USER, "PWD" => $this->_PASS, "Database" => $this->DBASE);
        $conn = @sqlsrv_connect($this->_HOSTNAME, $connection_info);
        if (!$conn) {
            $ret_arr['success'] = 0;
            $ret_arr['message'] = "Database connection attempt failed.Please verify your database username ,password and port number";
        } else {
            $this->CONN = $conn;
            $ret_arr['connection'] = $conn;
        }
        return $ret_arr;
    }

    public function getDBTables()
    {
        $sql_tables = "SELECT TABLE_NAME
                        FROM INFORMATION_SCHEMA.TABLES
                        WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG='" . $this->_DATABASE . "'";
        $db_tables = $this->select($sql_tables);
        if (!is_array($db_tables) || count($db_tables) == 0) {
            $db_tables = array();
        }
        return $db_tables;
    }

    public function select($sql = "", $fetch = "sqlsrv_fetch_array", $flag = 'No')
    {
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $results = $this->executeQuery($sql, "select");
        if ($flag == 'Yes') {
            print_r($results);
        }
        if ((!$results) or ( empty($results))) {
            return false;
        }
        $count = 0;
        $data = array();
        while ($row = $fetch($results)) {
            $data[$count] = $row;
            $count++;
        }
//        @mysqli_free_result($results);
        return $data;
    }

    function executeQuery($sql = "", $query_type = '')
    {
        if (empty($sql)) {
            return false;
        }
        if (empty($this->CONN)) {
            return false;
        }
        $conn = $this->CONN;
        $query_start_time = microtime(true);
        $result = @sqlsrv_query($conn, $sql, array(), array('Scrollable' => SQLSRV_CURSOR_KEYSET)) or die("Query failed: " . $sql . mysqli_error($conn));
        $query_end_time = microtime(true);
        return $result;
    }

    public function getSqlSrvVersion()
    {
        $version = sqlsrv_server_info($this->CONN);
        return $version;
    }

    function getSuperAdminData()
    {
        $sql = "SELECT ma.vUserName, ma.vPassword FROM mod_admin AS ma WHERE ma.vUserName = 'admin'";
        $db_data = $this->select($sql);
        if (!is_array($db_data) || count($db_data) == 0) {
            $db_data = array();
        }
        return $db_data;
    }

    function import_database_file($installer_sql_path)
    {
        $ret_arr['success'] = 0;
        $installer_sql_path = str_replace('/', DS, $installer_sql_path);
        $pass_str = '';
        if($this->_PASS!=''){
            $pass_str = ' -P '.$this->_PASS;
        }
        $command = "sqlcmd -S " . $this->_HOSTNAME . " -U " . $this->_USER  . $pass_str . " -d " . $this->DBASE . " -i " . $installer_sql_path . "";
        exec($command, $out, $error);
        if (!$error) {
            $ret_arr['success'] = 1;
        } else {
            $ret_arr['message'] = "Error in importing database";
        }
        return $ret_arr;
    }
}
