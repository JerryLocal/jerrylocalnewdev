<?php
    if(isset($_REQUEST['f'])){
        $path = './public/upload/err_log/';
        $file = base64_decode($_REQUEST['f']);
        //$path = getcwd();
        $file =  $path.$file;
        if ( base64_encode(base64_decode($_REQUEST['f'], true)) === $_REQUEST['f']){
            //if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off');	}
            // get the file mime type using the file extension
            switch(strtolower(substr(strrchr($file, '.'), 1))) {
                case 'pdf': $mime = 'application/pdf'; break;
                case 'zip': $mime = 'application/zip'; break;
                case 'jpeg':
                case 'jpg': $mime = 'image/jpg'; break;
                default: $mime = 'application/force-download';
            }
            header('Pragma: public'); 	// required
            header('Expires: 0');		// no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime ($file)).' GMT');
            header('Cache-Control: private',false);
            header('Content-Type: '.$mime);
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.filesize($file));	// provide file size
            header('Connection: close');
            ob_clean();
            flush();
            readfile($file);		// push it out
            exit();
        }
    }
?>