<?php
/**
 * Description of reg_email_nm Model
 *
 * @module reg_email_nm
 *
 * @class model_reg_email_nm.php
 *
 * @path applicationront\webservice\models\model_reg_email_nm.php
 *
 * @author Steve Smith
 *
 * @date 14.08.2015
 */

class Model_reg_email_nm extends CI_Model {

    public $_default_lang = 'EN';

    public function __construct() {
        parent::__construct();
        $this->load->helper('listing');
        $this->load->library('wsexternal');
    }

    function email_notification_1($input_params = array()) {

        try {

            $email_arr["vEmail"] = $input_params["email_address"];
            $email_arr["vFromEmail"] = "admin@wed-me.com";
            $email_arr["vFromName"] = "Administrator";
            $email_arr["vCCEmail"] = $input_params[""];
            $email_arr["vCCName"] = $input_params[""];
            $email_arr["vBCCEmail"] = $input_params[""];
            $email_arr["vBCCName"] = $input_params[""];
            $email_arr["vSubject"] = "Your password on #SYSTEM.COMPANY_NAME#";

            $email_arr["vUserName"] = $input_params["name"];
            $email_arr["vName"] = $input_params[""];
            $email_arr["vPassword"] = $input_params[""];
            $email_arr["vEmail"] = $input_params[""];
            $email_arr["SYSTEM.COMPANY_NAME"] = $input_params[""];
            $email_arr["SYSTEM.site_url"] = $input_params[""];

            $success = $this->general->sendMail($email_arr, "FORGOT_PASSWORD");
            if (!$success) {
                throw new Exception("Failure in sending mail");
            }
            $success = 1;
        } catch(Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        return $return_arr;
    }
}
?>