Please copy the below file(s) from downloaded zip folder to your project folder


Source and Destination Path(s)
------------------------------

Downloaded Zip --> reg_email_nm/controllers/*
Project Folder --> application/front/webservice/controllers/*

Downloaded Zip --> reg_email_nm/models/*
Project Folder --> application/front/webservice/models/*

[English - Language]
Downloaded Zip --> reg_email_nm/webservice_lang.php
Project Folder --> application/language/en/webservice_lang.php