<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of reg_email_nm Controller
 *
 * @module reg_email_nm
 *
 * @class reg_email_nm.php
 *
 * @path applicationront\webservice\controllers\reg_email_nm.php
 *
 * @author Steve Smith
 *
 * @date 14.08.2015
 */

class Reg_email_nm extends HB_Controller {

    public $settings_params;
    public $output_params;
    public $single_keys;
    public $multiple_keys;
    public $custom_keys;
    public $inner_loop;

    function __construct() {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array();
        $this->multiple_keys = array();
        $this->custom_keys = array();
        $this->inner_loop = array();

        $this->load->library('wsresponse');
        $this->load->library('wsexternal');
        $this->load->library('wschecker');
        $this->load->model('model_reg_email_nm');
    }

    function handler($params_fwd = array(), $return_op = false) {
        $validation_arr = array(
            "email_address" => array(
                array(
                    "rule" => "required",
                    "value" => true,
                    "message_code" => "email_address_required",
                    "message" => "Please enter a value for the email_address field.",
                ),
                array(
                    "rule" => "email",
                    "value" => true,
                    "message_code" => "email_address_email",
                    "message" => "Please enter valid email address for the email_address field.",
                )
            ),
            "name" => array(
                array(
                    "rule" => "required",
                    "value" => true,
                    "message_code" => "name_required",
                    "message" => "Please enter a value for the name field.",
                )
            )
        );
        try {
            $get_arr = is_array($this->input->get(null, true)) ? $this->input->get(null, true) : array();
            $post_arr = is_array($this->input->post(null, true)) ? $this->input->post(null, true) : array();
            $request_arr = array_merge($get_arr, $post_arr);
            $request_arr = is_array($params_fwd) ? array_merge($request_arr, $params_fwd) : $request_arr;

            $validation_res = $this->wsresponse->validateInputParams($validation_arr, $request_arr, "reg_email_nm");
            if ($validation_res['success'] == "-5") {
                if ($return_op === true) {
                    return $validation_res;
                } else {
                    $validation_final_res = $this->wsresponse->makeValidationResponse($validation_res);
                    $this->wsresponse->sendWSResponse($validation_final_res);
                }
            }
            /*
            if ($return_op !== true) {
                $verify_res = $this->wschecker->verify_webservice($request_arr);
                if ($verify_res['success'] != "1") {
                    $this->wschecker->show_error_code($verify_res);
                }
            }
            */
             
            $this->Model_reg_email_nm->_default_lang = $this->wsresponse->getLangRequestValue();
            $input_params = $validation_res['input_params'];
            $output_array = $func_array = array();

            $output_arr = $this->model_reg_email_nm->email_notification_1($input_params);
            $input_params["email_notification_1"] = $output_arr["success"];

            $setting_fields = array(
                "success" => "1",
                "message_code" => "finish_success",
                "message" => "email set to user",
            );
            $output_fields = array();

            $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
            $output_array["settings"]["fields"] = array_merge($this->output_params, $output_fields);
            $output_array["data"] = $input_params;

            $func_array["function"]["name"] = "reg_email_nm";
            $func_array["function"]["output_keys"] = array();
            $func_array["function"]["output_alias"] = array();
            $func_array["function"]["inner_keys"] = array();
            $func_array["function"]["single_keys"] = $this->single_keys;
            $func_array["function"]["multiple_keys"] = $this->multiple_keys;
            $func_array["function"]["custom_keys"] = $this->custom_keys;

            $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);
            if ($return_op === true) {
                return $responce_arr;
            } else {
                $this->wsresponse->sendWSResponse($responce_arr);
            }
        } catch(Exception $e) {
            $message = $e->getMessage();
        }
    }
}
