<?php  

//Registration Messages
    $lang["registration"]["first_name_required"] = "Please enter a value for the first_name field." ;
    $lang["registration"]["last_name_required"] = "Please enter a value for the last_name field." ;
    $lang["registration"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["registration"]["password_required"] = "Please enter a value for the password field." ;
    $lang["registration"]["gender_required"] = "Please enter a value for the gender field." ;
    $lang["registration"]["age_required"] = "Please enter a value for the age field." ;
    $lang["registration"]["duplicate"] = "Email address already exist. Please try with some different Email address." ;
    $lang["registration"]["success"] = "You will be informed by email when your account is activated." ;
    $lang["registration"]["failed"] = "Please try later." ;

//Facebooks Messages
    $lang["facebooks"]["first_name_required"] = "Please enter a value for the first_name field." ;
    $lang["facebooks"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["facebooks"]["facebook_id_required"] = "Please enter a value for the facebook_id field." ;
    $lang["facebooks"]["duplicate"] = "Email address already exist. Please try with some different Email address." ;
    $lang["facebooks"]["failed"] = "Please try later." ;
    $lang["facebooks"]["fb_success"] = "Login successfully." ;
    $lang["facebooks"]["duplicate"] = "Email address already exist. Please try with some different Email address." ;
    $lang["facebooks"]["failed"] = "Please try later." ;
    $lang["facebooks"]["fb_success"] = "Login successfully." ;
    $lang["facebooks"]["duplicate"] = "Email address already exist. Please try with some different Email address." ;
    $lang["facebooks"]["success"] = "You will be informed by email when your account is activated." ;
    $lang["facebooks"]["failed"] = "Please try later." ;

//Facebook Friend Messages
    $lang["facebook_friend"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["facebook_friend"]["facebook_ids_required"] = "Please enter a value for the facebook_ids field." ;
    $lang["facebook_friend"]["accesstoken_required"] = "Please enter a value for the accesstoken field." ;
    $lang["facebook_friend"]["first_name_required"] = "Please enter a value for the first_name field." ;
    $lang["facebook_friend"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["facebook_friend"]["device_token_required"] = "Please enter a value for the device_token field." ;
    $lang["facebook_friend"]["device_type_required"] = "Please enter a value for the device_type field." ;
    $lang["facebook_friend"]["facebook_id_required"] = "Please enter a value for the facebook_id field." ;
    $lang["facebook_friend"]["first_name_required"] = "Please enter a value for the first_name field." ;
    $lang["facebook_friend"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["facebook_friend"]["device_token_required"] = "Please enter a value for the device_token field." ;
    $lang["facebook_friend"]["device_type_required"] = "Please enter a value for the device_type field." ;
    $lang["facebook_friend"]["facebook_id_required"] = "Please enter a value for the facebook_id field." ;
    $lang["facebook_friend"]["finish"] = "Success." ;
    $lang["facebook_friend"]["duplicate"] = "Email address already exist. Please try with some different Email address." ;
    $lang["facebook_friend"]["failed"] = "Please try later." ;
    $lang["facebook_friend"]["fb_success"] = "Login successfully." ;
    $lang["facebook_friend"]["duplicate"] = "Email address already exist. Please try with some different Email address." ;
    $lang["facebook_friend"]["failed"] = "Please try later." ;
    $lang["facebook_friend"]["fb_success"] = "Login successfully." ;

//Sign In Messages
    $lang["sign_in"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["sign_in"]["password_required"] = "Please enter a value for the password field." ;
    $lang["sign_in"]["failed"] = "Please enter valid Email/Password." ;
    $lang["sign_in"]["fb_success"] = "Login successfully." ;
    $lang["sign_in"]["failed"] = "Please enter valid Email/Password." ;
    $lang["sign_in"]["fb_success"] = "Login successfully." ;
    $lang["sign_in"]["failed_status"] = "Your account has not activated. Please contact Administrator." ;
    $lang["sign_in"]["success"] = "Login successfully." ;

//Confirm Registration Messages
    $lang["confirm_registration"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["confirm_registration"]["verification_code_required"] = "Please enter a value for the verification_code field." ;
    $lang["confirm_registration"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["confirm_registration"]["device_token_required"] = "Please enter a value for the device_token field." ;
    $lang["confirm_registration"]["device_type_required"] = "Please enter a value for the device_type field." ;
    $lang["confirm_registration"]["password_required"] = "Please enter a value for the password field." ;
    $lang["confirm_registration"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["confirm_registration"]["age_required"] = "Please enter a value for the age field." ;
    $lang["confirm_registration"]["gender_required"] = "Please enter a value for the gender field." ;
    $lang["confirm_registration"]["facebook_id_required"] = "Please enter a value for the facebook_id field." ;
    $lang["confirm_registration"]["first_name_required"] = "Please enter a value for the first_name field." ;
    $lang["confirm_registration"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["confirm_registration"]["device_token_required"] = "Please enter a value for the device_token field." ;
    $lang["confirm_registration"]["device_type_required"] = "Please enter a value for the device_type field." ;
    $lang["confirm_registration"]["facebook_id_required"] = "Please enter a value for the facebook_id field." ;
    $lang["confirm_registration"]["first_name_required"] = "Please enter a value for the first_name field." ;
    $lang["confirm_registration"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["confirm_registration"]["device_token_required"] = "Please enter a value for the device_token field." ;
    $lang["confirm_registration"]["device_type_required"] = "Please enter a value for the device_type field." ;
    $lang["confirm_registration"]["facebook_id_required"] = "Please enter a value for the facebook_id field." ;
    $lang["confirm_registration"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["confirm_registration"]["subscription_id_required"] = "Please enter a value for the subscription_id field." ;
    $lang["confirm_registration"]["mod_admin_finish_success"] = "Success" ;
    $lang["confirm_registration"]["failed"] = "You've entered a wrong code." ;
    $lang["confirm_registration"]["failed"] = "Please enter valid Email/Password." ;
    $lang["confirm_registration"]["fb_success"] = "Login successfully." ;
    $lang["confirm_registration"]["failed"] = "Please enter valid Email/Password." ;
    $lang["confirm_registration"]["fb_success"] = "Login successfully." ;
    $lang["confirm_registration"]["failed_status"] = "Your account has not activated. Please contact Administrator." ;
    $lang["confirm_registration"]["success"] = "Login successfully." ;
    $lang["confirm_registration"]["success"] = "success" ;
    $lang["confirm_registration"]["duplicate"] = "Email address already exist. Please try with some different Email address." ;
    $lang["confirm_registration"]["failed"] = "Please try later." ;
    $lang["confirm_registration"]["fb_success"] = "Login successfully." ;
    $lang["confirm_registration"]["duplicate"] = "Email address already exist. Please try with some different Email address." ;
    $lang["confirm_registration"]["failed"] = "Please try later." ;
    $lang["confirm_registration"]["fb_success"] = "Login successfully." ;
    $lang["confirm_registration"]["not_found"] = "Subscription not found.Please contact us." ;
    $lang["confirm_registration"]["success"] = "Package purchased successfully." ;
    $lang["confirm_registration"]["failed"] = "Failed to purchase. Please contact us." ;

//Edit Profile Additional Info Messages
    $lang["edit_profile_additional_info"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["edit_profile_additional_info"]["finish"] = "Success." ;

//Get Data Messages
    $lang["get_data"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["get_data"]["success"] = "Success." ;

//Save Search Preference Messages
    $lang["save_search_preference"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["save_search_preference"]["issave_required"] = "Please enter a value for the issave field." ;
    $lang["save_search_preference"]["issearch_required"] = "Please enter a value for the issearch field." ;
    $lang["save_search_preference"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["save_search_preference"]["prospect_id_required"] = "Please enter a value for the prospect_id field." ;
    $lang["save_search_preference"]["rating_required"] = "Please enter a value for the comment field." ;
    $lang["save_search_preference"]["finish"] = "success" ;
    $lang["save_search_preference"]["fail"] = "Result not found." ;
    $lang["save_search_preference"]["success"] = "Success." ;
    $lang["save_search_preference"]["failed"] = "User is not friend of you." ;

//Get Subscription History Messages
    $lang["get_subscription_history"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["get_subscription_history"]["user_subscription_history_finish_success"] = "Subscription History found successfully." ;
    $lang["get_subscription_history"]["user_subscription_history_finish_success_1"] = "No record found." ;

//Like Dislike Messages
    $lang["like_dislike"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["like_dislike"]["prospect_id_required"] = "Please enter a value for the prospect_id field." ;
    $lang["like_dislike"]["islike_required"] = "Please enter a value for the islike field." ;
    $lang["like_dislike"]["success"] = "Success." ;

//Get Profile and Saved Pref Messages
    $lang["get_profile_and_saved_pref"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["get_profile_and_saved_pref"]["success"] = "success." ;

//facebooks Step2 Messages
    $lang["facebooks_step2"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["facebooks_step2"]["age_required"] = "Please enter a value for the age field." ;
    $lang["facebooks_step2"]["gender_required"] = "Please enter a value for the gender field." ;
    $lang["facebooks_step2"]["facebook_id_required"] = "Please enter a value for the facebook_id field." ;
    $lang["facebooks_step2"]["first_name_required"] = "Please enter a value for the first_name field." ;
    $lang["facebooks_step2"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["facebooks_step2"]["device_token_required"] = "Please enter a value for the device_token field." ;
    $lang["facebooks_step2"]["device_type_required"] = "Please enter a value for the device_type field." ;
    $lang["facebooks_step2"]["facebook_id_required"] = "Please enter a value for the facebook_id field." ;
    $lang["facebooks_step2"]["first_name_required"] = "Please enter a value for the first_name field." ;
    $lang["facebooks_step2"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["facebooks_step2"]["device_token_required"] = "Please enter a value for the device_token field." ;
    $lang["facebooks_step2"]["device_type_required"] = "Please enter a value for the device_type field." ;
    $lang["facebooks_step2"]["facebook_id_required"] = "Please enter a value for the facebook_id field." ;
    $lang["facebooks_step2"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["facebooks_step2"]["subscription_id_required"] = "Please enter a value for the subscription_id field." ;
    $lang["facebooks_step2"]["success"] = "success" ;
    $lang["facebooks_step2"]["duplicate"] = "Email address already exist. Please try with some different Email address." ;
    $lang["facebooks_step2"]["failed"] = "Please try later." ;
    $lang["facebooks_step2"]["fb_success"] = "Login successfully." ;
    $lang["facebooks_step2"]["duplicate"] = "Email address already exist. Please try with some different Email address." ;
    $lang["facebooks_step2"]["failed"] = "Please try later." ;
    $lang["facebooks_step2"]["fb_success"] = "Login successfully." ;
    $lang["facebooks_step2"]["not_found"] = "Subscription not found.Please contact us." ;
    $lang["facebooks_step2"]["success"] = "Package purchased successfully." ;
    $lang["facebooks_step2"]["failed"] = "Failed to purchase. Please contact us." ;

//Block Unblock Messages
    $lang["block_unblock"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["block_unblock"]["prospect_id_required"] = "Please enter a value for the prosepect_id field." ;
    $lang["block_unblock"]["is_block_required"] = "Please enter a value for the is_block field." ;
    $lang["block_unblock"]["success"] = "Success" ;
    $lang["block_unblock"]["failed_to_insert"] = "Please try later." ;

//Purchase Subscription Messages
    $lang["purchase_subscription"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["purchase_subscription"]["subscription_id_required"] = "Please enter a value for the subscription_id field." ;
    $lang["purchase_subscription"]["not_found"] = "Subscription not found.Please contact us." ;
    $lang["purchase_subscription"]["success"] = "Package purchased successfully." ;
    $lang["purchase_subscription"]["failed"] = "Failed to purchase. Please contact us." ;

//Set PushNotification Messages
    $lang["set_push_notification"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["set_push_notification"]["set_value_required"] = "Please enter a value for the set_value field." ;
    $lang["set_push_notification"]["success"] = "Success." ;

//Change Password Messages
    $lang["change_password"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["change_password"]["old_password_required"] = "Please enter a value for the old_password field." ;
    $lang["change_password"]["new_password_required"] = "Please enter a value for the new_password field." ;
    $lang["change_password"]["success"] = "New password will be applied now." ;
    $lang["change_password"]["failed"] = "Previous Password is not getting match. Please confirm and enter correct Password." ;

//Get Subscription packages Messages
    $lang["get_subscription_packages"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["get_subscription_packages"]["subscription_packages_finish_success"] = "Success." ;
    $lang["get_subscription_packages"]["failed"] = "Subscription not found." ;

//My Users Messages
    $lang["my_users"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["my_users"]["requestfor_required"] = "Please enter a value for the requestfor field." ;
    $lang["my_users"]["finish"] = "success" ;
    $lang["my_users"]["fail"] = "Result not found." ;

//Is Active Chat Messages
    $lang["is_active_chat"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["is_active_chat"]["prospect_id_required"] = "Please enter a value for the prospect_id field." ;
    $lang["is_active_chat"]["success"] = "Success" ;
    $lang["is_active_chat"]["like_failed"] = "Both side like needed." ;
    $lang["is_active_chat"]["sub_failed"] = "Subscription Expired. Please renew your subscription." ;
    $lang["is_active_chat"]["success1"] = "Success" ;
    $lang["is_active_chat"]["never_subscribed"] = "You've not subscribed yet. Please subscribe for a package to continue." ;
    $lang["is_active_chat"]["expired"] = "Subscription Expired. Please renew your subscription." ;
    $lang["is_active_chat"]["user_deleted"] = "User not available." ;

//Report User Messages
    $lang["report_user"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["report_user"]["prospect_id_required"] = "Please enter a value for the prospect_id field." ;
    $lang["report_user"]["comment_required"] = "Please enter a value for the comment field." ;
    $lang["report_user"]["success"] = "Success." ;
    $lang["report_user"]["failed"] = "User is not friend of you." ;

//Forgot Password Messages
    $lang["forgot_password"]["email_address_required"] = "Please enter a value for the email_id field." ;
    $lang["forgot_password"]["email_address_required"] = "Please enter a value for the email_id field." ;
    $lang["forgot_password"]["mod_admin_finish_success"] = "Email-Id is Incorrect." ;
    $lang["forgot_password"]["mod_admin_finish_success_1"] = "New Password sent to email.Please check email." ;
    $lang["forgot_password"]["mod_admin_finish_success"] = "Email-Id is Incorrect." ;
    $lang["forgot_password"]["mod_admin_finish_success_1"] = "New Password sent to email.Please check email." ;

//Prospect Details Messages
    $lang["prospect_details"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["prospect_details"]["prospect_jabber_id_required"] = "Please enter a value for the prospect_jabber_id field." ;
    $lang["prospect_details"]["success"] = "Success." ;
    $lang["prospect_details"]["failed"] = "Member info not available." ;

//Endorse User Messages
    $lang["endorse_user"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["endorse_user"]["prospect_id_required"] = "Please enter a value for the prospect_id field." ;
    $lang["endorse_user"]["rating_required"] = "Please enter a value for the comment field." ;
    $lang["endorse_user"]["success"] = "Success." ;
    $lang["endorse_user"]["failed"] = "User is not friend of you." ;

//Get Chat Users Messages
    $lang["get_chat_users"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["get_chat_users"]["finish"] = "success" ;
    $lang["get_chat_users"]["fail"] = "Result not found." ;

//Resend Pin Messages
    $lang["resend_pin"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["resend_pin"]["mod_admin_finish_success_2"] = "Please check your inbox for the verification code." ;
    $lang["resend_pin"]["failed"] = "You will be informed by email when your account is activated." ;
    $lang["resend_pin"]["success"] = "Please check your inbox for the verification code." ;

//Static Pages Messages
    $lang["static_pages"]["page_code_required"] = "" ;
    $lang["static_pages"]["success"] = "Success" ;

//send_notification Messages
    $lang["send_notification"]["receiver_id_required"] = "Please enter a value for the receiver_id field." ;
    $lang["send_notification"]["message_text_required"] = "Please enter a value for the message_text field." ;
    $lang["send_notification"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["send_notification"]["success"] = "Message sent successfully." ;
    $lang["send_notification"]["failed"] = "Message is not sent." ;

//Delete Image Messages
    $lang["delete_image"]["image_id_required"] = "Please enter a value for the image_id field." ;
    $lang["delete_image"]["user_image_finish_success"] = "Image deleted successfully." ;

//add_devices Messages
    $lang["add_devices"]["device_token_required"] = "Please enter a value for the devicetoken field." ;
    $lang["add_devices"]["device_type_required"] = "Please enter a value for the devicetype field." ;
    $lang["add_devices"]["videocontroller_notifications_finish_success"] = "Details added successfully." ;

//send_notifications Messages
    $lang["send_notifications"]["message_required"] = "Please enter a value for the message field." ;
    $lang["send_notifications"]["videocontroller_notifications_finish_success_1"] = "Notification sent successfully." ;

//check_subscription_expired Messages
    $lang["check_subscription_expired"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["check_subscription_expired"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["check_subscription_expired"]["device_token_required"] = "Please enter a value for the device_token field." ;
    $lang["check_subscription_expired"]["device_type_required"] = "Please enter a value for the device_type field." ;
    $lang["check_subscription_expired"]["password_required"] = "Please enter a value for the password field." ;
    $lang["check_subscription_expired"]["failed"] = "Please enter valid Email/Password." ;
    $lang["check_subscription_expired"]["fb_success"] = "Login successfully." ;
    $lang["check_subscription_expired"]["failed"] = "Please enter valid Email/Password." ;
    $lang["check_subscription_expired"]["fb_success"] = "Login successfully." ;
    $lang["check_subscription_expired"]["failed_status"] = "Your account has not activated. Please contact Administrator." ;
    $lang["check_subscription_expired"]["success"] = "Login successfully." ;

//logout Messages
    $lang["logout"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["logout"]["success"] = "Successfully logged out." ;

//Delete User Messages
    $lang["delete_user"]["user_id_required"] = "Please enter a value for the user_id field." ;
    $lang["delete_user"]["mod_admin_finish_success_2"] = "Record deleted successfully." ;

//reg_email_nm Messages
    $lang["reg_email_nm"]["email_address_required"] = "Please enter a value for the email_address field." ;
    $lang["reg_email_nm"]["email_address_email"] = "Please enter valid email address for the email_address field." ;
    $lang["reg_email_nm"]["name_required"] = "Please enter a value for the name field." ;
    $lang["reg_email_nm"]["finish_success"] = "email set to user" ;