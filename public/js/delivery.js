Project.modules.delivery = {
    init: function() { 
        // try {
        //     var telInput = $("#mba_phone");
        //             // errorMsg = $("#vMobileErr"),
        //             // validMsg = $("#valid-msg");
        //     telInput.intlTelInput({
        //         defaultCountry: "NZ",
        //         onlyCountries: ["NZ"],
        //         // geoIpLookup: function (callback) {
        //         //     $.get(site_url + 'get-ipinfo.html', function () {
        //         //     }).always(function (resptxt) {
        //         //         var countryCode = 'US';
        //         //         try {
        //         //             var resp = JSON.parse(resptxt);
        //         //             var countryCode = (resp && resp.country_code) ? resp.country_code : "";
        //         //         } catch (e) {
        //         //             console.log(e);
        //         //         }
        //         //         callback(countryCode);
        //         //     });
        //         // },
        //         utilsScript: site_url + "public/js/intlTelInput/utils.js"
        //     });
        // } catch (e) {
        //     console.log(e);
        // }

        // $(".flag-container ul li").click(function() {
        //     $("#mba_phone").val("");
        // });
    }
};
$(document).ready(function(){
    Project.modules.delivery.init();
    $("#mba_phone").keyup(function(e) {
        value = $(this).val();
        if(value.substr(0,3)!="+64") {
            if(e.keyCode==8) {
                $(this).val("+64");
            } else {
                val = '+64'+value; // set both the value
                $(this).val(val); // also update the form element
            }
        }
    });
});
/*$('#delivery_add_btn').click(function () {
    if ($('#delivery-address-form').valid()) {
        $('#delivery-address-form').submit();
    }
});*/

$.validator.addMethod("validatestate", function(value, element) {                    
    return this.optional(element) || (parseFloat(value) > 0);    
}, "Please select Region");

$.validator.addMethod("validatecity", function(value, element) {                    
    return this.optional(element) || (parseFloat(value) > 0);    
}, "Please select City");

// $.validator.addMethod("mobilenumber", function (value, element) {
//     var isValid = $("#mba_phone").intlTelInput("isValidNumber");
//     return isValid;
// }, "Please enter valid mobile number");
$.validator.addMethod("validphnumberwithcountry", function (value, element) {
    // if no url, don't do anything
    if (value.length == 0) { return true; }

    // if user has not entered +64

    if(value.substr(0,3)!="+64") {
        val = '+64'+value; // set both the value
        $(element).val(val); // also update the form element
    }
    // now check if valid url
    return /\+64[0-9]{1,12}$/.test(value);
}, "Please enter valid mobile number");

$('#delivery-address-form').validate({
    rules:{
        'mba_name': {
            required : true
        },
        'mba_address1': {
            required:true
        },
        'mba_area': {
            required:true
        },
        'mba_country_id':{
            required:true
        },
        'mba_state_id':{                    
            validatestate:true
        },
        'mba_city_id':{
            validatecity:true
        },
        'mba_pin_code': {
            digits: true,
            maxlength:20,
            nospace: true
        },
        'mba_phone': {
            required:true,
            // mobilenumber:true
            // digits:true
            // maxlength:16,
            validphnumberwithcountry:true
        }
    },
    messages:{
        'mba_name':{
            required:'Please enter a Receiver Name'
        },
        'mba_address1':{
            required:'Please enter a Street No'
        },
        'mba_area':{
            required:'Please enter a Area'
        },
        'mba_country_id':{
            required:'Please select Country'
        },
        'mba_pin_code':{
            required:'Please enter a Pincode'
        },
        'mba_phone':{
            required:'Please enter a Contact No'
        }
    }, 
    errorPlacement: function (error, element) {
         // default error scheme
            error.insertAfter(element);
        
    }, submitHandler: function (form) {
        var form_id = $(form).attr("id");
        // var valid_number = $("#mba_phone").intlTelInput("getNumber");
        // $("#mba_phone_hidden").val(valid_number);
        document.getElementById(form_id).submit();
    }
});