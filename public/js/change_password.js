Project.modules.change_pass = {
    init: function() {

        $('#save_chng_btn').click(function() {	
            if ($('#frmChangepassword').valid()) {
                $('#frmChangepassword').submit();
            }
        });
        
        $('#cancel_btn').click(function() {
            
            if(!$('#frmChangepassword').valid()) {
                $('#frmChangepassword').submit();
            }
        });
        

        $.validator.addMethod("notStartWithSpace", function(value, element) { 
          return value.indexOf(" ") < 0 && value != ""; 
        }, "Password Can not start with space");

        $('#frmChangepassword').validate({
            rules: {
                new_psw: {
                    required: true,
                    notStartWithSpace: true,
                    minlength:6
                },
                old_psw: {
                    required: true,
                    notStartWithSpace: true,
                    minlength:6
                },
                confirm_new_psw: {
                    required: true,
                    notStartWithSpace: true,
                    equalTo:"#new_psw"
                }
            },
            messages: {
                new_psw: {
                    required: 'Please Enter New Password',
                    // notStartWithSpace: 'Password Can not start with space'
                },
                old_psw: {
                    required: 'Please Enter Old Password',
                    // notStartWithSpace: 'Password Can not start with space'

                },
                confirm_new_psw: {
                    required: 'Please Retype New Password',
					equalTo: 'Password do not match with New Password'
                }
                                
            }, errorPlacement: function(error, element) {
                    error.insertAfter(element);
            }, submitHandler: function(form) {                
                var form_id = $(form).attr("id");
                document.getElementById(form_id).submit();
            }
            
        });

    }
};

