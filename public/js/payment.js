Project.modules.payment = {
    init: function() {}
};
$('#pay_btn').click(function () {
            if ($('#payment_form').valid()) {
                $('#payment_form').submit();
            }
        });
        $('#payment_form').validate({
            rules:{
                'card-type': {
                    required : true
                },
                'card-number': {
                    required:true,
                    digits: true
                },
                'expiry-month': {
                    required:true
                },
                'expiry-year':{
                    required:true
                },
                'cvv':{                    
                    required:true,
                    digits: true  
                }
            },
            messages:{
                'card-type':{
                    required:'Please Select Cart Type'
                },
                'card-number':{
                    required:'Please Enter a Cart Number'
                },
                'expiry-month':{
                    required:'Please Select Expiry Month'
                },
                'expiry-year':{
                    required:'Please Select Expiry Year'
                },
                'cvv':{
                    required:'Please Enter a CVV'
                }
            }, 
            errorPlacement: function (error, element) {
                 // default error scheme
                    error.insertAfter(element);
                
            }, submitHandler: function (form) {
                var form_id = $(form).attr("id");
                document.getElementById(form_id).submit();

            }
        });