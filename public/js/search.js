Project.modules.categoryPage = {
	init: function() {   

	}
};

$(document).ready(function(){
	// $("#price_by").change();
	sort_by_product_PRICE();
	$( "#slider-range" ).slider({
		range: true,
		min: 0,
		max: maxprice,
		values: [ 0, maxprice ],
		slide: function( event, ui ) {
			$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		}
	});
	$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
			" - $" + $( "#slider-range" ).slider( "values", 1 ) );
});

// $("#sort_by").change(function(){
function sort_by_product_MARKET() {
	$("#loader").show();
	$(".short-bar").hide();
	$("#catalog-listing").hide();
	$(".pagination-box").hide();
	var chkvalue = [];
	var i = 0;
	$(".accordion .accordion-body .brand-list input.chk").each(function() {
		if($(this).is(":checked")) {
			chkvalue[i] = $(this).val();
			i++;
		}
	});
	var postchkvalue = chkvalue.join(",");

	var price_id = $('#price_by').val();
	var sort_id = $('#sort_by').val();
	var cid = $('#cid').val();
	var start_price = $( "#slider-range" ).slider( "values", 0 );
	var end_price = $( "#slider-range" ).slider( "values", 1 );

	$.ajax({
		type: "POST",
		url: site_url + "productlist/productlist/productListBySort",
		data:{
			'sort_id':sort_id,
			'price_id':price_id,
			'cid':cid,
			'start_price':start_price,
			'end_price':end_price,
			'filter':postchkvalue
		},
		success: function(result) {
			$("#loader").hide();
			var data = $.parseJSON(result);
			$('.sort_result').html('');
			if(data.status==1) {
	    		$(".short-bar").show();
	    		$("#catalog-listing").show();
	    		$(".pagination-box").show();
	    		$(".pagination-box .pagination").html(data.paging);
	    		$(".pagination-box .pagination-list").html(data.pagingstring);
	    		$("#product-title .row span").html("("+data.totalRecordFound+")");
	    	} else {
	    		$(".short-bar").hide();
	    		$(".pagination-box").hide();
	    	}
	    	$('.sort_result').html(data.content);

			// $('.sort_result').html('');
			// $('.sort_result').html(result);
			filterproductlistcounter(cid, start_price, end_price);
		}
	});
}
// });

// $("#price_by").change(function(){
function sort_by_product_PRICE() {
	$("#loader").show();
	$(".short-bar").hide();
	$("#catalog-listing").hide();
	$(".pagination-box").hide();
	var chkvalue = [];
	var i = 0;
	$(".accordion .accordion-body .brand-list input.chk").each(function() {
		if($(this).is(":checked")) {
			chkvalue[i] = $(this).val();
			i++;
		}
	});
	var postchkvalue = chkvalue.join(",");

	var sort_id  = $('#sort_by').val();
	var price_id = $('#price_by').val();
	var cid = $('#cid').val();
	var start_price = $( "#slider-range" ).slider( "values", 0 );
	var end_price = $( "#slider-range" ).slider( "values", 1 );
	$.ajax({
		type: "POST",
		url: site_url + "productlist/productlist/productListBySort",
		data:{
			'sort_id':sort_id,
			'price_id':price_id,
			'cid':cid,
			'start_price':start_price,
			'end_price':end_price,
			'filter':postchkvalue
		},
		success: function(result) {
			$("#loader").hide();
			var data = $.parseJSON(result);
			$('.sort_result').html('');
			if(data.status==1) {
	    		$(".short-bar").show();
	    		$("#catalog-listing").show();
	    		$(".pagination-box").show();
	    		$(".pagination-box .pagination").html(data.paging);
	    		$(".pagination-box .pagination-list").html(data.pagingstring);
	    		$("#product-title .row span").html("("+data.totalRecordFound+")");
	    	} else {
                        $("#catalog-listing").show();
	    		$(".short-bar").hide();
	    		$(".pagination-box").hide();
	    	}
	    	$('.sort_result').html(data.content);
			// $('.sort_result').html('');
			// $('.sort_result').html(result);
			filterproductlistcounter(cid, start_price, end_price);
		}
	});
}
// });

$("#clear_all_filter").click(function(){
	$("#loader").show();
	$(".short-bar").hide();
	$("#catalog-listing").hide();
	$(".pagination-box").hide();

	// reset price
	$(function() {
		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: maxprice,
			values: [ 0, maxprice ],
			slide: function( event, ui ) {
				$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			}
		});
		$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
			" - $" + $( "#slider-range" ).slider( "values", 1 ) );
	});

	// reset products
	var cid = $("#cid").val();   	            
	$.ajax({
		type: "POST",
		url : site_url+'productlist/productlist/productListBySort',
		data : {
			start_price:0, 
			end_price:maxprice,
			cid : cid
		},
		success:function(response){
			$("#loader").hide();
			var data = $.parseJSON(response);
			$('.sort_result').html('');
			if(data.status==1) {
	    		$(".short-bar").show();
	    		$("#catalog-listing").show();
	    		$(".pagination-box").show();
	    		$(".pagination-box .pagination").html(data.paging);
	    		$(".pagination-box .pagination-list").html(data.pagingstring);
	    		$("#product-title .row span").html("("+data.totalRecordFound+")");
	    	} else {
	    		$(".short-bar").hide();
	    		$(".pagination-box").hide();
	    	}
	    	$('.sort_result').html(data.content);
			// $('.sort_result').html('');
			// $('.sort_result').html(response);
			filterproductlistcounter(cid, 0, maxprice);
		}
	});	  

	// reset brand,color,etc..
	$('input:checkbox').removeAttr('checked');   
});


$( "#slider-range" ).slider({
	range: true,
	min: 0,
	max: maxprice,
	values: [ 0, maxprice ],
	slide: function( event, ui ) {
		$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
	},
	change: function(event, ui) {
        // when the user change the slider	            
    },
    stop: function(event, ui) {
		$("#loader").show();
		$(".short-bar").hide();
		$("#catalog-listing").hide();
		$(".pagination-box").hide();
        // when the user stopped changing the slider	
        var chkvalue = [];
		var i = 0;
		$(".accordion .accordion-body .brand-list input.chk").each(function() {
			if($(this).is(":checked")) {
				chkvalue[i] = $(this).val();
				i++;
			}
		});
		var postchkvalue = chkvalue.join(",");
		var sort_id  = $('#sort_by').val();
		var price_id = $('#price_by').val();
		var start_price = ui.values[0];
		var end_price = ui.values[1]

        var cid = $("#cid").val();   	            
        $.ajax({
        	type: "POST",
        	url : site_url+'productlist/productlist/productListBySort',
        	data : {
        		start_price:start_price, 
        		end_price:end_price,
        		cid : cid,
        		filter : postchkvalue,
        		sort_id:sort_id,
				price_id:price_id
        	},
        	success:function(response){
				$("#loader").hide();
				var data = $.parseJSON(response);
				$('.sort_result').html('');
				if(data.status==1) {
		    		$(".short-bar").show();
		    		$("#catalog-listing").show();
		    		$(".pagination-box").show();
		    		$(".pagination-box .pagination").html(data.paging);
		    		$(".pagination-box .pagination-list").html(data.pagingstring);
	    			$("#product-title .row span").html("("+data.totalRecordFound+")");
	    			filterproductcounter(data);
	    			filterproductlistcounter(cid, start_price, end_price);
		    	} else {
		    		$(".short-bar").hide();
		    		$("#catalog-listing").show();
		    		$(".pagination-box").hide();
		    		$(".accordion .accordion-body .brand-list input.chk").each(function() {
	    				$(this).parent().find("label span").html("(0)");
	    			});
		    	}
		    	$('.sort_result').html(data.content);
        		// $('.sort_result').html('');
        		// $('.sort_result').html(response);
        	}
        });	            
    }
}); 

function productListingAjax(catid, cleanurl, class1) {
	$(".selected-cat").removeClass("selected-cat");
	$("."+class1).addClass("selected-cat");
	$("#loader").show();
	$(".short-bar").hide();
	$("#catalog-listing").hide();
	$(".pagination-box").hide();
	var chkvalue = [];
	var i = 0;
	$(".accordion .accordion-body .brand-list input.chk").each(function() {
		if($(this).is(":checked")) {
			chkvalue[i] = $(this).val();
			i++;
		}
	});
	var postchkvalue = chkvalue.join(",");
	var start_price = $("#slider-range").slider( "values", 0 );
	var end_price = $("#slider-range").slider( "values", 1 );
	var sort_id  = $('#sort_by').val();
	var price_id = $('#price_by').val();
	var cid = $("#cid").val();
	$.ajax({
		type : "POST",
		url  : site_url+"c/"+catid+"/"+cleanurl,
		data : "filter="+postchkvalue+"&startPrice="+start_price+"&endPrice="+end_price+"&catid="+catid+"&price_id="+price_id+"&sort_id="+sort_id+"&cid="+cid,
		success : function(response) {
			maxpricebycategory(catid);
			$("#loader").hide();
			productTitle = $(response).find("#product-title").html();
			catalogListing = $(response).find("#catalog-listing").html();
			paginationBox = $(response).find(".pagination-box").html();
			$("#product-title").html(productTitle);
			$("#catalog-listing").html(catalogListing);
			$(".pagination-box").html(paginationBox);
			// var prodlist_with_header_and_paging = $(response).find(".product-list .row .col-main").html();
			// $(".product-list .row .col-main").html(prodlist_with_header_and_paging);
                        if($('.page-product-list').length > 0){
				$(".short-bar").show();
				$("#catalog-listing").show();
				$(".pagination-box").show();
			} else {
				//$("#catalog-listing .sort_result").html("<center><h1>"+$(response).find("center h1").html()+"</h1></center>");
				$(".short-bar").hide();
				$("#catalog-listing").show();
                                $(".pagination-box").hide();
			}
			$("#cid").val($(response).find("#cid").val());
			$("#catId").val(catid);
			disablecheckboxes($("#cid").val());
			filterproductlistcounter($("#cid").val(), start_price, end_price);
		}
	});
}

function disablecheckboxes(cid) {
	$.ajax({
		url  : site_url+"productlist/productlist/checkDisableFilterValue",
		type : "POST",
		data : "cid="+cid,
		success : function(response) {
			var data = $.parseJSON(response);
			var result = data.filtervalue.split(",");
			$(".accordion .accordion-body .brand-list input.chk").each(function() {
				var splitvalueloaded = $(this).val().split("|");
				if($.inArray($(this).val(), result)!=-1 || splitvalueloaded[1]=="other" || splitvalueloaded[1]=="freeshipping") {
					// $(this).removeAttr("disabled");
					// $(this).parent().find("label").removeAttr("style");
					// $(this).parent().find("label span#tot_count").show();
					// $(this).parent().find("label span#tot_count").html("("+data.totalProduct+")")
					// $(this).parent().find("label span#zero_count").hide();
					$(this).parent().parent().show();
				} else {
					// $(this).attr("disabled", "disabled");
					// $(this).parent().find("label").css("color","#A1A1A1");
					// $(this).parent().find("label span#tot_count").hide();
					// $(this).parent().find("label span#zero_count").show();
					$(this).parent().parent().hide();
				}
			});
		}
	});
	// $(".accordion .accordion-body .brand-list input.chk").each(function() {
	// 	var splitvalue = $(this).val().split("|");
	// 	if(splitvalue[1]!="other") {
	// 		$(this).attr("disabled", "disabled");
	// 		$(this).parent().find("label").css("color","#A1A1A1");
	// 		$(this).parent().find("label span#tot_count").hide();
	// 		$(this).parent().find("label span#zero_count").show();
	// 	} else {
	// 		$(this).removeAttr("disabled");
	// 		$(this).removeAttr("style");
	// 		$(this).parent().find("label span#tot_count").show();
	// 		$(this).parent().find("label span#zero_count").hide();
	// 	}
	// });
}

function maxpricebycategory(catid) {
	$.ajax({
		url  : site_url+"productlist/productlist/maxpricebycategory",
		type : "POST",
		data : "catid="+catid,
		success : function(response) {
			maxprice = response;
			$("#slider-range").slider({
		  		range: true,
				min: 0,
				max: maxprice,
				values: [ 0, maxprice ],
				slide: function( event, ui ) {
					$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
				}
			});
			$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
		  		" - $" + $( "#slider-range" ).slider( "values", 1 ) );
		}
	});
}

// $(".accordion .accordion-body .brand-list input.chk").live("click", function(){
function filterlistcheckbox() {
	$("#loader").show();
	$(".short-bar").hide();
	$("#catalog-listing").hide();
	$(".pagination-box").hide();
	var catId = $("#catId").val();
	// var cleanurl = $("#cleanurl").val();
	var chkvalue = [];
	var i = 0;
	$(".accordion .accordion-body .brand-list input.chk").each(function() {
		if($(this).is(":checked")) {
			chkvalue[i] = $(this).val();
			i++;
		}
	});
	var postchkvalue = chkvalue.join(",");
	var sort_id  = $('#sort_by').val();
	var price_id = $('#price_by').val();
	// url = window.location.href;
	// cleanurl = url.substr(url.lastIndexOf('/') + 1);
	var cid = $('#cid').val();
	var start_price = $( "#slider-range" ).slider( "values", 0 );
	var end_price = $( "#slider-range" ).slider( "values", 1 );
	if(postchkvalue) {
		postdata = "filter="+postchkvalue+"&start_price="+start_price+"&end_price="+end_price+"&catId="+catId+"&price_id="+price_id+"&sort_id="+sort_id+"&cid="+cid;
	} else {
		postdata = "start_price="+start_price+"&end_price="+end_price+"&catId="+catId+"&price_id="+price_id+"&sort_id="+sort_id+"&cid="+cid;
	}
	$.ajax({
		type : "POST",
		url  : site_url+"productlist/productlist/searchProducts",
		data : postdata,
		success : function(response) {
			$("#loader").hide();
			var data = $.parseJSON(response);
			$('.sort_result').html('');
			if(data.status==1) {
	    		$(".short-bar").show();
	    		$("#catalog-listing").show();
	    		$(".pagination-box").show();
	    		$(".pagination-box .pagination").html(data.paging);
	    		$(".pagination-box .pagination-list").html(data.pagingstring);
	    		$("#product-title .row span").html("("+data.totalRecordFound+")");
	    	} else {
	    		$(".short-bar").hide();
	    		$("#catalog-listing").show();
	    		$(".pagination-box").hide();
	    	}
	    	$('.sort_result').html(data.content);
		}
	});
}
// });

$.ajax({
	type : "POST",
	url  : site_url+"productlist/productlist/checkCategoryOpton",
	data : "catid="+$("#catId").val(),
	success : function(response) {
		var data =  $.parseJSON(response);
		var filterDataDB = [];
		$.each(data, function(i) {
			// filterDataDB[i] = data[i].vTitle+'|'+data[i].vValue;
			filterDataDB[i] = data[i].vValue+'|'+data[i].vTitle;
		});
		$(".accordion .accordion-body .brand-list input.chk").each(function() {
			var splitvalue = $(this).val().split("|");
			if($.inArray($(this).val(), filterDataDB)==-1 && splitvalue[1]!="other" && splitvalue[0]!="freeshipping") {
				// $(this).attr("disabled", "disabled");
				// $(this).next().css("color", "#949494");
				$(this).parent().parent().hide();
			}
		})
	}
});

$(".filter-block .filter-search input").keyup(function() {
	var filter = $(this).attr("data-value");
	var filterid = $(this).attr("data-id");
	var catid = $("#catId").val();
	var thisvalue = $(this).val();
	var start_price = $( "#slider-range" ).slider( "values", 0 );
	var end_price = $( "#slider-range" ).slider( "values", 1 );
	if(thisvalue.length > 1) {
		$.ajax({
			url  : site_url+"productlist/productlist/loadfilterlist",
			type : "POST",
			data : "key="+thisvalue+"&filter="+filter+"&filterid="+filterid+"&catid="+catid+"&start_price="+start_price+"&end_price="+end_price,
			success : function(response) {
				var data = $.parseJSON(response);
				// $("#"+filter+" #brands ul").html(data.filterchklist);
				$("#"+filterid+" .brand-list ul").html(data.filterchklist);
    			filterproductcounter(data);
			}
		});
	} else if(thisvalue.length == 0) {
		$.ajax({
			url  : site_url+"productlist/productlist/loadfilterlist",
			type : "POST",
			data : "filter="+filter+"&filterid="+filterid+"&catid="+catid+"&start_price="+start_price+"&end_price="+end_price,
			success : function(response) {
				var data = $.parseJSON(response);
				// $("#"+filter+" #brands ul").html(data.filterchklist);
				$("#"+filterid+" .brand-list ul").html(data.filterchklist);
    			filterproductcounter(data);
			}
		});
	}
});

function filterproductcounter(data) {
	keys = Object.keys(data.productCounter);
	$(".accordion .accordion-body .brand-list input.chk").each(function() {
		if($.inArray($(this).val(), keys)!=-1) {
			$(this).parent().find("label span").html("("+data.productCounter[$(this).val()]+")");
		}
	});
}

function filterproductlistcounter(cid, start_price, end_price) {
	$.ajax({
		url  : site_url+"productlist/productlist/filterKeywordListProductCounterAjax",
		type : "POST",
		data : "cid="+cid+"&start_price="+start_price+"&end_price="+end_price,
		success : function(response) {
			var data = $.parseJSON(response);
			filterproductcounter(data);
		}
	});
}