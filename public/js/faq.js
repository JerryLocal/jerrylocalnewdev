$(document).ready(function(){
  $(function() {
  $('.faqs-label a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
            scrollTop: target.offset().top-105
        }, 1000);
        return false;
      }
    }
  });
});
});
$(document).ready(function(){
var WIDTH = jQuery(window).width();
jQuery(window).resize(function(){ WIDTH = jQuery(window).width(); });

jQuery(window).load(function(){
  // Scroll past Address Bar in iPhone.
  setTimeout(function(){ window.scrollTo(0,1); },0);
});

jQuery(window).load(function(){
  
  // Slide in the sidebar.
  var sidebar = jQuery("#faq-wrap .faqs-label");
  
  setTimeout(function(){
    sidebar.find("*").animate({opacity:1}, 400);
    sidebar.animate({marginRight: 0}, 700);
  }, 350);
  
  
  // Load the first images visible in view-port.
  jQuery("#faq-wrap .all-faq").each(function() {
    // loadImage(jQuery(this));
  });

});

jQuery(document).ready(function(){

  /* Scroll to top on load */
  jQuery('html, body').stop().animate({ scrollTop: 0 }, 500);

  /* Cache Elements for Use */
    var project = jQuery("#faq-wrap"),
    images = jQuery("#faq-wrap .all-faq"),
    sidebar = jQuery("#faq-wrap .faqs-label");


  /* Set up variables for calculations */

  var top = sidebar.offset().top-35;
  
  /* Catch the main scroll event */
  
  jQuery(window).scroll(function (e) {
    var scrollTop = jQuery(this).scrollTop();
    /*=======================
      
      Image Loading
      
    =======================*/
    if (jQuery(window).width() < 1100) {
      jQuery("#faq-wrap .all-faq ").each(function(){
        // loadImage( jQuery(this) );
      });
    } else {

      /* Make sidebar stick while scrolling. */
      if (scrollTop > top) {
          var id= jQuery("#faq-wrap .all-faq .faqs-content").attr('id');
          jQuery('.faqs-label h4').css('border-right','0px;');
          jQuery('.faqs-label h4 #'+id).css('border-right','5px solid #dd3d0b');
          jQuery(".all-faq .faqs-content").each(function() {
            var h = jQuery(this).offset().top - 200;
            
            if(scrollTop-75 > h){
              var cl = jQuery(this).attr('id');
              console.log('a[href="faq.html#'+cl+'"]');
              jQuery('.faqs-label h4').removeClass('f-active');
              jQuery('a[href="faq.html#'+cl+'"]').parent('h4').addClass('f-active');
            }
          });
        if (!sidebar.hasClass("fixed")) {
          sidebar.css({marginLeft: 0}).addClass("fixed");
          
        }
      } else {
        sidebar.removeClass("fixed").css({marginLeft: 0});
      }
  
      
      /*
       *  Stick at bottom
       */
      
      var bottom = project.offset().top + project.outerHeight() - sidebar.height() - 440;
      if (scrollTop > bottom) {
        sidebar.addClass("bottom");
        sidebar.css({ top: project.outerHeight() -  sidebar.height() });
      } else {
        sidebar.removeClass("bottom");
        if($('.smaller').length == 1){
          sidebar.css({top: "95px"}); 
        }else
        {
          sidebar.css({top: "88px"});
        }
      }
    }
    
  });
  
  /* Scroll To Top */
  
  sidebar.find("a.back-to-top").click(function(e){
    e.preventDefault();
  
    jQuery('html, body').stop().animate({ scrollTop: 0 }, 500);
  
  });
});
/* Loads and fades in image */

function loadImage(image, callback) {
  if (typeof callback == 'undefined') { callback = function(){}; }

  if (image.attr("src") == "/_images/core/transparent.gif") {
  
    image.animate({opacity:0}, 50, function() {
      image.attr("src", image.attr("data-original")).bind('load', function(){
        image.animate({opacity:1}, 1000).css({height:'auto'});
        callback();
      });
    });

  }
}
});