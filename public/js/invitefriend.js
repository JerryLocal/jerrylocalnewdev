Project.modules.inviterfriend = {
    init: function() {    
      
    }
};

$('#send_friend').click(function () {
    if ($('#invite_friend').valid()) {
            $('#invite_friend').submit();
        }
    });
$.validator.addMethod("emailvalidate", function(value, element) {            
            var emailarr =[];
            emailarr = value.split(',');
            var error = '';
            for (var i = 0; i < emailarr.length; i++) {
                 // error =this.optional(element) || emailarr[i] ==emailarr[i].match(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/);
                 error = this.optional(element) || /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailarr[i]);
            };
            return error;
         },'Enter valid email Id');
    $('#invite_friend').validate({
        rules:{
            'toEmail': {
                required : true,
                emailvalidate:true
            }
            ,
            'Message': {
                required:true,
            }
        },
        messages:{
            'toEmail':{
                required:'Please enter atleast one email',
                // email:'Please enter valid email address'
            },
            'Message':{
                required:'Please enter a Message'
            }
        }, 
        errorPlacement: function (error, element) {
             // default error scheme
                error.insertAfter(element);
            
        }, submitHandler: function (form) {
            var form_id = $(form).attr("id");
            document.getElementById(form_id).submit();

        }
    }); 