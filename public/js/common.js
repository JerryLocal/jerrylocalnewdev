var Common = (function () {
    var returnObj = {};
    function initValidator() {
        if (typeof $.validator != 'undefined') {
            $.validator.setDefaults({
                errorClass: "help-block",
                errorElement: "span",
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                }
            });
        }
    }
    returnObj.initValidator = initValidator;
    return returnObj;
})();