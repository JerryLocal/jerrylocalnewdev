Project.modules.wishlist = {
    init: function() {           
	}
};

$("#sort_by").change(function(){      
      var price_id = '0';
      var sort_id =$('#sort_by').val();
      var cid = $('#cid').val();


      $.ajax({
           type: "POST",
           url: site_url + "productlist/productlist/productListBySort",
           data:{'sort_id':sort_id,
                 'price_id':price_id,
                 'cid':cid
               },
           success: function(result) {
             $('.sort_result').html('');
           	$('.sort_result').html(result);
           }
       });
});

$("#price_by").change(function(){      
      var sort_id  = '0';
      var price_id = $('#price_by').val();
      var cid = $('#cid').val();
      $.ajax({
           type: "POST",
           url: site_url + "productlist/productlist/productListBySort",
           data:{'sort_id':sort_id,
                 'price_id':price_id,
                 'cid':cid
               },
           success: function(result) {
             $('.sort_result').html('');
            $('.sort_result').html(result);
           }
       });
});
