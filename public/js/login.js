Project.modules.login = {
    init: function() {
        Common.initValidator();
        this.setloginValidate();
    },
    setloginValidate:function(){
        $('#login-form-normal').validate({
            rules:{
                'User[vUserName]': {
                    required : true,
                },
                'User[vPassword]': {
                    required:true
                }
            },
            messages:{
                'User[vUserName]' :{
                    required:'Please enter a Username/Email'
                },
                'User[vPassword]' :{
                    required:'Please enter a password'
                }
            }     
        });
    
    }
};


