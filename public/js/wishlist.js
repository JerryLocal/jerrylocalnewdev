Project.modules.wishlist = {
    init: function() {   
      
      if(Review == "Y"){
        $("#seeReviews").click();  
      }
	}
};
$(".remove_wishlist").click(function(){        
      var id =$(this).attr('data-rel');
      var r = confirm("Are you sure to remove this product ?");
      if(r == true){
          $.ajax({
               type: "POST",
               url: site_url + "wishlist/wishlist/removeWishlist",
               data:{'id':id},
               success: function(result) {
                var data = $.parseJSON(result);
               		if(data.msg = '1') {
                      	$('li#'+id).remove();
                      	var total = $('span.wishlist_total').attr('data-rel')-1;
                      	if(total == 0){
                      		$('.wishlist-list ol').remove();
                      		$('.wishlist-list').append('<div class="no-wishlist"><p>No product available</p></div>');
                      	}
                      	$('span.wishlist_total').attr('data-rel',total);
                      	$('span.wishlist_total').text(total);
                        Project.setMessage("Item Removed Successfully.", "alert-success");
                        // test_confirm('Product is removed from your wishlist', 'Yes', 'Yes');
                   	}
               }
           });
      }
});

// $(".add_wish_list").click(function(){ 
function addtowishlist(id,page){
  // var id = $(this).attr('data-id');
  var id = id;
  
      $.ajax({
           type: "POST",
           url: site_url + "wishlist/wishlist/addToWishlist",
           data:{'id':id},
           success: function(result) {
              var data = $.parseJSON(result);
              if(data.msg == 1) {
                    // test_confirm('Product is added to your wishlist', 'Yes', 'Yes');  
                    Project.setMessage("Product is Added to Your Wishlist Successfully.", "alert-success");
                    if(page == "detailpage"){
                      $(".fa-heart").css('color','orange');
                      $('#wishlist_count').html('('+data.count+')');                    
                    }else{                  
                      $('.cls'+id).addClass('in-wishlist'); 
                      $('.cls'+id).html('('+data.count+')');                    
                    }
                }else if(data.msg == 0)
                {
                  $.ajax({
                       type: "POST",
                       url: site_url + "wishlist/wishlist/removeWishlist",
                       data:{'id':id},
                       success: function(result) {
                          var data = $.parseJSON(result);
                            if(data.msg = '1') {
                                $('li#'+id).remove();
                                var total = $('span.wishlist_total').attr('data-rel')-1;
                                if(total == 0){
                                  $('.wishlist-list ol').remove();
                                  $('.wishlist-list').append('<div class="no-wishlist"><p>No product available</p></div>');
                                }
                                if(page == "detailpage"){
                                  $(".fa-heart").css('color','');
                                  $('#wishlist_count').html('('+data.count+')');                    
                                }else{                  
                                  $('.cls'+id).removeClass('in-wishlist'); 
                                  $('.cls'+id).html('('+data.count+')');
                                }
                                $('span.wishlist_total').attr('data-rel',total);
                                $('span.wishlist_total').text(total);
                                Project.setMessage("Item Removed Successfully.", "alert-success");
                                // test_confirm('Product is removed from your wishlist', 'Yes', 'Yes');
                            }
                       }
                   });
                  // test_confirm('Product is already in your Wishlist', 'Yes', 'Yes');
                }else
                {
                  // test_confirm('Please Login First to Add this product on your wishlist', 'Yes', 'Yes');
                  var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
                  var encodedString = Base64.encode(window.location.href);
                  window.location = site_url+'login.html?&ref='+encodedString;
                }
           }
       });
}      

$("#seeReviews").click(function(){  
  // navigate to div
  $('html,body').animate({
        scrollTop: $(".product-page-content").offset().top},
  'slow');
  // Remove Every class
  $("#myTab li.active a").attr("aria-expanded",'false');
  $('#myTab li:nth-child(1)').removeClass('active');
  $("#Description").removeClass("active in");

  // Add Every class
  $('#myTab li:nth-child(2)').addClass('active');
  $("#myTab li.active a").attr("aria-expanded",'true');
  $("#Information").addClass("active in");
});