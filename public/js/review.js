Project.modules.review = {
    init: function() {              
    	$('#hidden_star').val('');
    	$("#starEr").html('');
      //  Below code to close the review tab after submit and reload the parent page - start
      if(c){
        window.close();   
        window.onunload = refreshParent;
           function refreshParent() {
               window.opener.location.reload();
           } 
      }
      // above code to close the review tab after submit and reload the parent page - end

    	$('#postReview').click(function () {

    	    if ($('#frmReview').valid()) {
    	    	if($('#hidden_star').val() > 0){
    	    		$('#frmReview').submit();   
              //  Below code to close the review tab after submit and reload the parent page - start              
              /*window.close();   
              window.onunload = refreshParent;
                 function refreshParent() {
                     window.opener.location.reload();
                 }*/
              // above code to close the review tab after submit and reload the parent page - end
    	    	}else{
    	    		$("#starEr").html("Please Select rating").css('color','#c93605');
    	    		return false;
    	    	}
    	    }
    	});

    	$('#frmReview').validate({
    	    rules:{
    	        'review': {
    	            required : true
    	        }
    	    },
    	    messages:{
    	        'review' :{
    	            required:'Please enter a Review / Rating'
    	        }
    	    }, 
    	    errorPlacement: function (error, element) {    	        
    	            error.insertAfter(element);    	        
    	    }, submitHandler: function (form) {
    	        var form_id = $(form).attr("id");
    	        document.getElementById(form_id).submit();
    	    }
    	});         
	}
};
$(".add_your_review").click(function(){ 
      $('#add_review').submit();
});

$(".review-star").click(function(){        
      var count = $(this).attr('data-rel');
      $('.review-star').removeClass('active') ;
      for (var i = 1; i <= count; i++) {
         $('#review-star-'+i).addClass('active');
      };
      $('#hidden_star').val(count);
});