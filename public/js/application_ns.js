var time = parseInt(Number(new Date()) / (3600 * 24 * 1000));
var api_token = null;

function callp(method, data, success_callback, error_callback) {
    call(method, data, success_callback, error_callback, true)
}

function call(method, data, success_callback, error_callback, post) {
    if (typeof(data) == 'undefined')
        data = {};
    if (typeof(data) == 'string') {
        data += "&api_key=" + api_key + "&api_token=" + api_token;
    } else {
        data.api_key = api_key;
        data.api_token = api_token;
    }

    $.ajax({
        url: ns_base + method,
        type: typeof(post) == 'boolean' ? "POST" : "GET",
        dataType: 'json',
        data: data,
        crossDomain: true,
        error: function(jqXHR, textStatus, errorThrown) {
            alert(textStatus);
            if (typeof(error_callback) == 'function')
                error_callback(jqXHR, textStatus, errorThrown)
        },
        success: function(data, status, jqXHR) {
            if (typeof(success_callback) == 'function')
                success_callback(data, status, jqXHR)
        }
    });
}


$(function() {
    // login
    $('form.ns').submit(function(e) {
        var el = $(this);
        e.preventDefault();
        if ($(this).attr('method') == 'post') {
            callp($(this).attr('action'), $(this).serialize(), function(a, b, c) {
                el.next().show().html(c.responseText);
            });
        } else {
            call($(this).attr('action'), $(this).serialize(), function(a, b, c) {
                el.next().show().html(c.responseText);
            });
        }
    });
});

$.validator.addMethod("regex", function(value, element, regexp) {
    var re = new RegExp(regexp);
    return this.optional(element) || re.test(value);
}, "Please check your input.");