Project.modules.profiles = {
    init: function() {               

        // Below code for the signup process

        $('#saveChangesbtn').click(function () {
            if ($('#frmProfile').valid()) {
                $('#frmProfile').submit();
            }
        });

        $("#dDOB").datepicker({
               dateFormat: 'yy-mm-dd' 
            });
        
        $.validator.addMethod("alpha", function(value, element) {            
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
         },'Enter only alpha characters');

        // for phone number validation
        $.validator.addMethod("checkPhone", function(value, element) {            
            return this.optional(element) || value == value.match(/[0-9-()+]+$/);
         },'Enter valid Phone Number');

        // $.validator.addMethod("phonenumber", function (value, element) {
        //     var isValid = $("#vPhoneNo").intlTelInput("isValidNumber");
        //     return isValid;
        // }, "Please enter valid mobile number");
        $.validator.addMethod("validphnumberwithcountry", function (value, element) {
            // if no url, don't do anything
            if (value.length == 0) { return true; }

            // if user has not entered +64
            if(value.substr(0,3)!="+64") {
                val = '+64'+value; // set both the value
                $(element).val(val); // also update the form element
            }
            // now check if valid url
            return /(\+64)[0-9]*$/.test(value);
        }, "Please enter valid mobile number");

        $('#frmProfile').validate({
            rules:{
                'fname': {
                    required : true,
                    alpha: true
                },
                'lname': {                    
                    required:true,
                    alpha: true
                },
                'vPhoneNo': {                                        
                    // phonenumber: true
                    validphnumberwithcountry:true
                },
                'vEmail': {
                    required : true,
                    email:true,
                    /*remote: {
                        url: site_url+"user/user/check_customer_email",
                        type: "post"
                    }*/
                }/*,
                'acnt1': {
                    digits : true
                },
                'acnt2': {
                    digits : true
                },
                'acnt3': {
                    digits : true
                },
                'acnt4': {
                    digits : true
                }*/

            },
            messages:{
                'fname' :{
                    required:'Please enter a Firstname'
                },
                'lname' :{
                    required:'Please enter a Lastname'
                },
                'vEmail':{
                    required:'Please enter a email',
                    email:'Please enter valid email address',
                    remote:'Email already exists'

                }
            }/*, 
            groups: {
                username: "acnt1 acnt2 acnt3 acnt4"
            }*/,
            errorPlacement: function (error, element) {
                /*if (element.attr("name") == "acnt1" || element.attr("name") == "acnt2" || element.attr("name") == "acnt3" || element.attr("name") == "acnt4" ) {
                      error.insertAfter("#acntErr");
                }else{*/
                    error.insertAfter(element);                
                // }
            }, submitHandler: function (form) {
                var form_id = $(form).attr("id");
                var valid_number = $("#vPhoneNo").intlTelInput("getNumber");
                $("#vPhoneNo_hidden").val(valid_number);
                document.getElementById(form_id).submit();

            }
        }); 

    }
};

$(document).ready(function() {
    // try {
    //     var telInput = $("#vPhoneNo");
    //             // errorMsg = $("#vMobileErr"),
    //             // validMsg = $("#valid-msg");
    //     telInput.intlTelInput({
    //         defaultCountry: "NZ",
    //         onlyCountries: ["NZ"],
    //         // geoIpLookup: function (callback) {
    //         //     $.get(site_url + 'get-ipinfo.html', function () {
    //         //     }).always(function (resptxt) {
    //         //         var countryCode = 'US';
    //         //         try {
    //         //             var resp = JSON.parse(resptxt);
    //         //             var countryCode = (resp && resp.country_code) ? resp.country_code : "";
    //         //         } catch (e) {
    //         //             console.log(e);
    //         //         }
    //         //         callback(countryCode);
    //         //     });
    //         // },
    //         utilsScript: site_url + "public/js/intlTelInput/utils.js"
    //     });
    // } catch (e) {
    //     console.log(e);
    // }
    $("#vPhoneNo").keyup(function(e) {
        value = $(this).val();
        if(value.substr(0,3)!="+64") {
            if(e.keyCode==8) {
                $(this).val("+64");
            } else {
                val = '+64'+value; // set both the value
                $(this).val(val); // also update the form element
            }
        }
    });
});