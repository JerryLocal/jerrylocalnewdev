/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Project.modules.forgotpassword = {
    init: function() {
        Common.initValidator();
        this.setForgotPasswordValidate();
    },
    setForgotPasswordValidate:function(){
        $('#forgotpassword-form-normal').validate({
            rules:{
                'User[vUserName]': {
                    required : true
                }
             
            },
            messages:{
                'User[vUserName]' :{
                    required:'Please enter a Username/Email'
                }
            }      
        });
    
    }
};

