Project.modules.add_to_cart = {
	init: function() {		
		this.addToCartHandler();
		this.spinnerHandler();
		this.loadCartInfo();
		this.updateItemQtyHandler();
		this.removeItemHandler();

		this.countryStateHandler();
	},
	addToCartHandler: function() {
		var btnHandler 		= $(".atc-btn-handler");
		var clsObj			= this;
		btnHandler.on('click',function(e){
			var buynow 		=$(this).data('buynow');
			var atc_url		=site_url+'cart/add';
			var postdata 	= 	{};
			postdata.pid	=	$(this).data('id');
			var qtyInputId	=	'#qty-handler-'+postdata.pid;
			var sizeInputname	=	"#size-handler-"+postdata.pid+" input[type='radio'][name='productoption']";
			if(parseInt(postdata.pid)<=0){
				return false;
			}
			// quantity			
			if($(qtyInputId).length == 0) {
  				postdata.qty = 1;
			}else{
				postdata.qty = $(qtyInputId).val();
				if(parseInt(postdata.qty)<=0){
					return false;
				}
			}
			// size			
			if($(sizeInputname).length != 0) { 
				postdata.itemsize = $(sizeInputname+":checked").val();
			}			
			clsObj.fullScreenLoader();

			$.post( atc_url, postdata, function( data ) {
				data = jQuery.parseJSON(data);				 
				if(data.settings.success==2 && data.settings.continue_url!=''){
					window.location=data.settings.continue_url;
					return true;
				}
				if(data.settings.success==0){
					$("#var_msg_cnt").show();
					$("#err_msg_cnt").addClass('alert-danger').text(data.settings.message);	
				}else{
					if(buynow=='buynow'){
						window.location=site_url+'checkout/myshoppingcart';
						return true;
					}else{
						$("#var_msg_cnt").show();
						$("#err_msg_cnt").removeClass('alert-danger').addClass('alert-success').text(data.settings.message);	
					}
				}				
				clsObj.loadCartInfo();
			  	clsObj.removefullScreenLoader();
			}).fail(function() {
				$("#var_msg_cnt").show();
				$("#err_msg_cnt").addClass('alert-danger').text("Issue: Server not responding.");
				clsObj.removefullScreenLoader();   
			});

		});
	},
	fullScreenLoader: function(){
		$('body').append('<div class="customeoverlay"><i class="icomoon-icon-loading-8 fa-spin"></i></div>');
	}
	,
	removefullScreenLoader: function(){
		$('.customeoverlay').remove();
	},
	spinnerHandler: function(){		
	    $('.product-quantity-spinner').spinner();
	},
	loadCartInfo: function(){
		$("#cart-information-block").load(site_url+'cart/cart_info');
	},
	updateItemQtyHandler: function(){
		var qtyInput = $('.product-qty-control');
		$(".product-quantity-spinner button").on('click',function(e){
			$(this).closest('td').find('.updateqtylink').removeClass('hidden');
		});
		var clsObj			= this;
		$(".updateqtylink").on('click',function(e){
			var postdata 	= 	{};
			postdata.rowid 	= $(this).data('rowid');
			postdata.pid	= $(this).data('pid');
			postdata.qty  	= $("#"+postdata.rowid).val();
			var atc_url		=site_url+'cart/update_cart';
			clsObj.fullScreenLoader();
			$.post( atc_url, postdata, function( data ) {
				data = jQuery.parseJSON(data);				 
				 
				if(data.settings.success==0){
					$("#var_msg_cnt").show();
					$("#err_msg_cnt").addClass('alert-danger').html(data.settings.message);	
				}else{
					$("#var_msg_cnt").show();
					$("#err_msg_cnt").removeClass('alert-danger').addClass('alert-info').html(data.settings.message);
					$("#cart_item_box").load(site_url+'checkout/myshoppingcart/cartItem',{},function(e){
						clsObj.init();
					});
				}				
				//clsObj.loadCartInfo();
			  	clsObj.removefullScreenLoader();
			}).fail(function() {
				$("#var_msg_cnt").show();
				$("#err_msg_cnt").addClass('alert-danger').html("Issue: Server not responding.");
				clsObj.removefullScreenLoader();   
			});
		});
	},	
	removeItemHandler: function(){		
		var clsObj			= this;
		$(".removeitemfromcart").on('click',function(e){
			var postdata 	= 	{};
			postdata.rowid 	= $(this).data('rowid');			
			postdata.pname 	= $(this).data('pname');			
			var atc_url		=site_url+'cart/removeitem_from_cart';
			clsObj.fullScreenLoader();
			$.post( atc_url, postdata, function( data ) {
				data = jQuery.parseJSON(data);				 
				$("#var_msg_cnt").show();
				$("#err_msg_cnt").removeClass('alert-danger').addClass('alert-info').html(data.settings.message);
				$("#cart_item_box").load(site_url+'checkout/myshoppingcart/cartItem',{},function(e){
				clsObj.init();
				});
				clsObj.removefullScreenLoader();
			}).fail(function() {
				$("#var_msg_cnt").show();
				$("#err_msg_cnt").addClass('alert-danger').html("Issue: Server not responding.");
				clsObj.removefullScreenLoader();   
			});
		});
	},
	countryStateHandler: function(){
		var country=$("#mba_country_id_dd");
		var state=$("#mba_state_id_dd");
		var city=$("#mba_city_id_dd");
		var obj=this;

		var statediv=$("#statedropdowncon");
		var citydiv=$("#citydropdowncon");


		country.on('change',function(e){
			var countryid=$(this).val();
			state.html('');
			city.html('');
			statediv.load(site_url+'checkout/deliveryaddress/getState/'+countryid+'?ajax=1','',function(){
				obj.countryStateHandler();
			});
		});


		state.on('change',function(e){
			var stateid=$(this).val();			
			city.html('');
			citydiv.load(site_url+'checkout/deliveryaddress/getCity/'+stateid+'?ajax=1');
		});
	}
}
Project.modules.add_to_cart.init();

$(document).ready(function() {
	var clsObj			= Project.modules.add_to_cart;
	$(document).on('click', '.atc-btn-handler1', function() {
		var buynow 		=$(this).data('buynow');
		var atc_url		=site_url+'cart/add';
		var postdata 	= 	{};
		postdata.pid	=	$(this).data('id');
		var qtyInputId	=	'#qty-handler-'+postdata.pid;
		var sizeInputname	=	"#size-handler-"+postdata.pid+" input[type='radio'][name='productoption']";
		if(parseInt(postdata.pid)<=0){
			return false;
		}
		// quantity			
		if($(qtyInputId).length == 0) {
				postdata.qty = 1;
		}else{
			postdata.qty = $(qtyInputId).val();
			if(parseInt(postdata.qty)<=0){
				return false;
			}
		}
		// size			
		if($(sizeInputname).length != 0) { 
			postdata.itemsize = $(sizeInputname+":checked").val();
		}			
		clsObj.fullScreenLoader();

		$.post( atc_url, postdata, function( data ) {
			data = jQuery.parseJSON(data);				 
			if(data.settings.success==2 && data.settings.continue_url!=''){
				window.location=data.settings.continue_url;
				return true;
			}
			if(data.settings.success==0){
				$("#var_msg_cnt").show();
				$("#err_msg_cnt").addClass('alert-danger').text(data.settings.message);	
			}else{
				if(buynow=='buynow'){
					window.location=site_url+'checkout/myshoppingcart';
					return true;
				}else{
					$("#var_msg_cnt").show();
					$("#err_msg_cnt").removeClass('alert-danger').addClass('alert-success').text(data.settings.message);	
				}
			}				
			clsObj.loadCartInfo();
		  	clsObj.removefullScreenLoader();
		}).fail(function() {
			$("#var_msg_cnt").show();
			$("#err_msg_cnt").addClass('alert-danger').text("Issue: Server not responding.");
			clsObj.removefullScreenLoader();   
		});
	});
});