Project.modules.productlist = {
    init: function() { }
};

i=0;
var recipCount  = 1;
var maxRecip    = 10;
function remove_recipient(i){
    // $('li #recipients_name'+i).remove();
    $('.add_friend-'+i).remove();
    recipCount--;
    if(recipCount < maxRecip &&  maxRecip != 0) {
        $('#add_recipient_button').show();
        $('#max_recipient_message').hide();
    }
    return false;
}

function add_recipient(){
    var li_mail = document.createElement("LI");
    li_mail.className = "fields additional-row add_friend-"+i;
    li_mail.setAttribute('data-count', i);
    li_mail.innerHTML = '<a title="Remove Email" onclick="remove_recipient('+i+'); return false" class="btn-remove">X</a>';
    // <div class="add-row-err add-name-row-err'+i+'"></div>
    li_mail.innerHTML += '<div class="field"><label for="recipients_name'+i+'" class="required"><em>*</em>Name:</label><div class="input-box"><input name="recipients[name]['+(i+1)+']" type="text" class="input-text required-entry required" id="recipients_name'+i+'" val="" /></div>';
    // <div class="add-row-err add-email-row-err'+i+'"></div>
    li_mail.innerHTML += '<div class="field"><label for="recipients_email'+i+'" class="required"><em>*</em>Email Address:</label><div class="input-box"><input name="recipients[email]['+(i+1)+']" value="" id="recipients_email'+i+'" type="text" class="input-text required-entry validate-email required email"/></div></div>';
    i++;
    recipCount++;
    if(recipCount >= maxRecip && maxRecip != 0) {
        $('#add_recipient_button').hide();
        $('#max_recipient_message').show();
    }

    $('#recipients_options').append(li_mail);
}


$('#send_mail').click(function () {
    additionalRecipientValidation();
    if ($('#product_sendtofriend_form').valid()) {
        $('#product_sendtofriend_form').submit();
    }
});
validateForm();
function validateForm() {
    $('#product_sendtofriend_form').validate({
        rules:{
            'sendername': {
                required : true,},
            'sender[email]': {
                required:true,
                email:true
            },
            'sender[message]': {
                required:true,
            },
            'recipients[name][0]': {
                required:true,
            },
            'recipients[email][0]': {
                required:true,
                email:true
            },
            'Captcha':{
                required:true,
                maxlength: 6,
                remote: {
                    url: site_url + "contactus/contactus/check_captcha",
                    type: "POST"
                }
            }
            
        },
        messages:{
            'sendername':{
                required:'Please enter a name'
                
            },
            'sender[email]':{
                required:'Please enter a Email',
                email:'Please enter valid email address'
            },
            'sender[message]':{
                required:'Please enter a Message'
            },
            'recipients[name][0]':{
                required:'Please enter a recipients name'
            },
            'recipients[email][0]':{
                required:'Please enter a recipients Email',
                email:'Please enter valid email address'
            },
            'Captcha':{
                required:'Please enter Captcha Code',
                remote:'Please Enter Correct Captcha'                
            }
        }, 
        errorPlacement: function (error, element) {
            if (element.attr("name") == "Captcha") {
                error.appendTo("#CaptchaErr");
            } else { // default error scheme
                error.insertAfter(element);
            }
            
        }, submitHandler: function (form) {
            var form_id = $(form).attr("id");
            document.getElementById(form_id).submit();

        }
    }); 
}

function additionalRecipientValidation() {
    $(".additional-row").each(function(i) {
        $(this).find("input#recipients_name"+i).rules("add", {required: true,messages: {required: "Please enter a recipients name"}});
        $(this).find("input#recipients_email"+i).rules("add", {required: true, email:true, messages: {required: "Please enter a recipients Email", email:'Please enter valid email address'}});
    })
}

$("#resetcaptcha").click(function(){        
       $("#Captcha").val('');
       $('#capchareset').css('opacity', '0.5');
       $('#capchareset').css('filter', '50');
       $.ajax({
           type: "POST",
           url: site_url + "contactus/contactus/resetCaptcha",
           success: function(result) {            
               if (result != '') {
                   $("#capchareset").attr('src', result)
                   $('#capchareset').css('opacity', '1');
                   $('#capchareset').css('filter', '100');
               }
           }
       });
});