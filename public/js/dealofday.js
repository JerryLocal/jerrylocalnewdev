Project.modules.dealofday = {
    init: function() {   
    
    }
};
$(document).ready(function(){
	$("#price_by").change();
});
$("#sort_by").change(function(){      
  $("#loader").show();
  $(".short-bar").hide();
  // $("#catalog-listing").hide();
  $(".pagination-box").hide();
      var price_id = $('#price_by').val();
      var sort_id =$('#sort_by').val();
      var start_price = $( "#slider-range" ).slider( "values", 0 );
      var end_price = $( "#slider-range" ).slider( "values", 1 );
      $.ajax({
           type: "POST",
           url: site_url + "productlist/productlist/dealOfDayBySort",
           data:{'sort_by':sort_id,
                 'price_by':price_id,
                 'start_price':start_price,
                 'end_price':end_price
               },
           success: function(result) {
            $("#loader").hide();
             /*$('.sort_result').html('');
           	 $('.sort_result').html(result);*/
   	 		var data = $.parseJSON(result);           	 		
   	 		$('.sort_result').html('');
   	 		if(data.status==1) {
   	     		$(".short-bar").show();
   	     		$("#catalog-listing").show();
   	     		$(".pagination-box").show();
   	     		$(".pagination-box .pagination").html(data.paging);
   	     		$(".pagination-box .pagination-list").html(data.pagingstring);
   	     	} else {
   	     		$(".short-bar").hide();
   	     		$(".pagination-box").hide();
   	     	}
   	     	$('.sort_result').html(data.content);
           	 		
           }
       });
});

$("#price_by").change(function(){      
  $("#loader").show();
  $(".short-bar").hide();
  $("#catalog-listing").hide();
  $(".pagination-box").hide();
      var sort_id  = $('#sort_by').val();;
      var price_id = $('#price_by').val();
      var start_price = $( "#slider-range" ).slider( "values", 0 );
      var end_price = $( "#slider-range" ).slider( "values", 1 );
      $.ajax({
           type: "POST",
           url: site_url + "productlist/productlist/dealOfDayBySort",
           data:{'sort_by':sort_id,
                 'price_by':price_id,
                 'start_price':start_price,
                 'end_price':end_price
               },
           success: function(result) {
            $("#loader").hide();
             /*$('.sort_result').html('');
             $('.sort_result').html(result);*/
             		var data = $.parseJSON(result);           	 		
             		$('.sort_result').html('');
             		if(data.status==1) {
                 		$(".short-bar").show();
                 		$("#catalog-listing").show();
                 		$(".pagination-box").show();
                 		$(".pagination-box .pagination").html(data.paging);
                 		$(".pagination-box .pagination-list").html(data.pagingstring);
                 	} else {
                 		$(".short-bar").hide();
                 		$(".pagination-box").hide();
                 	}
                 	$('.sort_result').html(data.content);
           }
       });
});

$("#clear_all_filter").click(function(){

	// reset price
	$(function() {
		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: maxprice,
			values: [ 0, maxprice ],
			slide: function( event, ui ) {
				$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			}
		});
		$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
			" - $" + $( "#slider-range" ).slider( "values", 1 ) );
	});

	// reset brand,color,etc..
 	$('input:checkbox').removeAttr('checked');   
});

pricefilter();
function pricefilter(){
	$(function() {
		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: maxprice,
			values: [ 0, maxprice ],
			slide: function( event, ui ) {
				$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			},
	        change: function(event, ui) {
	            // when the user change the slider	            
	        },
	        stop: function(event, ui) {
            $("#loader").show();
            $(".short-bar").hide();
            $("#catalog-listing").hide();
            $(".pagination-box").hide();
	            // when the user stopped changing the slider
	            var sort_by = $('#sort_by').val();
	            var price_by = $('#price_by').val();
	            $.ajax({
	            	type: "POST",
	            	url : site_url+'productlist/productlist/dealOfDayBySort',
	            	data : {
	            			start_price:ui.values[0], 
	            			end_price:ui.values[1],
	            			price_by:price_by,
	            			sort_by:sort_by	
	            		},
	            	success:function(response){
                  $("#loader").hide(); 
                  $("#catalog-listing").show();
	            		// alert(response);
	            		 /*$('.sort_result').html('');
	            		 $('.sort_result').html(response);*/
        		 		var data = $.parseJSON(response);           	 		
        		 		$('.sort_result').html('');
        		 		if(data.status==1) {
        		     		$(".short-bar").show();
        		     		$(".pagination-box").show();
        		     		$(".pagination-box .pagination").html(data.paging);
        		     		$(".pagination-box .pagination-list").html(data.pagingstring);
        		     	} else {
        		     		$(".short-bar").hide();
        		     		$(".pagination-box").hide();
        		     	}
        		     	$('.sort_result').html(data.content);
	            	}
	            });	            
	        }
	    }); 		
	});
}