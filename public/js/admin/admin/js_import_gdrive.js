Project.modules.import_gdrive = {
    init: function () {
        $("#auth_drive_btn").click(function () {
            //_nA = New Account login
            var gd_auth_uri = admin_url + "tools/gdrivemanager/importDriveAuth?_nA=1";
            var gd_auth_pop_opts = "width=700, height=400, location=no, menubar=no, resizable=no, scrollbars=no, top=100, left=100";
            window.open(gd_auth_uri, "_blank", gd_auth_pop_opts);
        });
        if ($("input[name='tokenEnable']").val() == "Yes") {
            Project.modules.importGdrive.getDriveData({'type': 'docs'}, "showDULink");
        } else {
            $("#drive_btn").show();
        }
        $("#change_drive_user").click(function () {
            $("#drive_btn").show();
            $("#drive_cont_block").hide();
        });
        $(document).on('click', '#gd_next_btn', function (e) {
            if ($.trim($("input[name='docId']:checked").val()) == "") {
                jqueryUIalertBox("Please select any document.");
                return false;
            }
            Project.modules.importGdrive.getDriveData({'type': 'sheets', 'docId': $("input[name='docId']:checked").val()});
        });
        $(document).on('click', '#gd_submit_btn', function () {
            var sindx = $("input[name='sheetId']:checked").attr("index");
            if (typeof ($("#gd_sheet_block_"+sindx+" tr:eq(0) td:eq(0)").attr("colspan")) != "undefined") {
                jqueryUIalertBox("Please select sheet which has data.");
                return false;
            }
            var gsb_dialog = $("<div/>").html("Do you want to continue with the selected data?").dialog({
                title: "Select sheet",
                buttons: {
                    'Yes': function () {
                        parent.Project.show_adaxloading_div();
                        $("#gd_sheet_form").ajaxSubmit(function (resp) {
                            parent.Project.hide_adaxloading_div();
                            var respjson = $.parseJSON(resp);
                            parent.$.fancybox.close();
                            parent.responseAjaxDataSubmission(respjson);
                        });
                        $(this).remove();
                    },
                    'No': function () {
                        $(this).remove();
                    }
                },
                modal: true
            });
        });
        $(document).on('click', "#gd_back_btn", function () {
            Project.modules.importGdrive.getDriveData({'type': 'docs'});
        });
    },
    getDriveData: function (opts, exp1) {
        var exp1 = (typeof exp1 != "undefined") ? exp1 : "";
        parent.Project.show_adaxloading_div();
        var gd_data_uri = admin_url + "tools/gdrivemanager/getDriveData";
        $.ajax({
            type: 'POST',
            url: gd_data_uri,
            data: opts,
            success: function (resp) {
                parent.Project.hide_adaxloading_div();
                $("#drive_cont_block").html(resp);
                if (exp1 == "showDULink") {
                    $("#change_drive_user").show();
                }
            }
        });
    }
};
Project.modules.import_gdrive.init();