/** sendnewsletter module script */
Project.modules.sendnewsletter = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "tnc_title": {
                    "required": true
                },
                "tnc_ns_group_id": {
                    "required": true
                },
                "tnc_mst_newsletter_template_id": {
                    "required": true
                }
            },
            messages: {
                "tnc_title": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SENDNEWSLETTER_CAMPAIN_NAME)
                },
                "tnc_ns_group_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SENDNEWSLETTER_NEWS_LETTER_GROUP)
                },
                "tnc_mst_newsletter_template_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SENDNEWSLETTER_SELECT_NEWS_LETTER)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'tnc_title':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'tnc_ns_group_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'tnc_mst_newsletter_template_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {

        $('#tnc_send_date').datepicker({
            dateFormat: getAdminJSFormat('date'),
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#tnc_send_date').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.sendnewsletter.init();