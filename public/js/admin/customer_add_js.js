/** customer module script */
Project.modules.customer = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "ma_email": {
                    "required": true,
                    "email": true
                },
                "ma_user_name": {
                    "required": true
                },
                "ma_password": {
                    "required": true
                },
                "mp_bank_account_no": {
                    "required": true,
                    "number": true
                }
            },
            messages: {
                "ma_email": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMER_EMAIL),
                    "email": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_EMAIL_ADDRESS_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMER_EMAIL)
                },
                "ma_user_name": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMER_USER_NAME)
                },
                "ma_password": {
                    "required": true
                },
                "mp_bank_account_no": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMER_BANK_ACCOUNT_NO),
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMER_BANK_ACCOUNT_NO)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'ma_email':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'ma_user_name':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'ma_password':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_bank_account_no':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        $('#ma_phonenumber').mask(getAdminJSFormat('phone'));

        $('#mp_dob').datepicker({
            dateFormat: getAdminJSFormat('date'),
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#mp_dob').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.customer.init();