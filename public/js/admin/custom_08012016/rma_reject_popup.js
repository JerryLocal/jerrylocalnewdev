Project.modules.reject_rma = {
    init: function () {
        $('input[type=text]').keypress(function (event) {
            var unicode = event.charCode ? event.charCode : event.keyCode;
            if (unicode == 13) {
                //  $('#frmReject').submit();
                reject_action();
            }
        });
        $("#btnReject").on('click', function () {
            // $('#frmReject').submit();
            reject_action();
        });
        $("#btnCancel").on('click', function () {
            parent.$.fancybox.close();
        });
    },
    callEvents: function () {
    }
}

function reject_action() {
    var form_data = $('#frmReject').serialize();
    var url = admin_url + "newrequest/newrequest/reject_add_action";
    var data = "";
    $.post(url, form_data, function (response) {
        tempArr = $.parseJSON(response);
        var status = tempArr.success;
        var message = tempArr.message;
        Project.setMessage(message, status);
        parent.$.fancybox.close();
        parent.window.location.reload();
        Project.setMessage(message, status);
    });
}