Project.modules.accept_rma = {
    init: function () {
        $('input[type=text]').keypress(function (event) {
            var unicode = event.charCode ? event.charCode : event.keyCode;
            if (unicode == 13) {
                //  $('#frmAccept').submit();
                accept_action();
            }
        });
        $("#btnAccept").on('click', function () {
            // $('#frmAccept').submit();
            accept_action()
        });
        $("#btnCancel").on('click', function () {
            $.fancybox.close();
        });
    },
    callEvents: function () {
    }
}

function accept_action() {
    var form_data = $('#frmAccept').serialize();
    var url = admin_url + "newrequest/newrequest/accept_add_action";
    var data = "";
    $.post(url, form_data, function (response) {
        tempArr = $.parseJSON(response);
        var status = tempArr.success;
        var message = tempArr.message;
        Project.setMessage(message, status);
        parent.$.fancybox.close();
        parent.window.location.reload();
         Project.setMessage(message, status);
        //redirect_module = admin_url + "#managephotos/managephotos/index";
        // window.location.href = redirect_module;
    });
}