Project.modules.main_area = {
    init: function () {alert(1);
         $('input[type=text]').keypress(function (event) {
            var unicode = event.charCode ? event.charCode : event.keyCode;
            if (unicode == 13) {
                $('#frmRefund').submit();
            }
        });
        $("#btnRefund").on('click', function () {
            $('#frmRefund').submit();
        });
        $("#btnCancel").on('click', function () {
            parent.$.fancybox.close();
        });
        $("#fShipping").keyup(function () {

            var refundamount = parseFloat($("#fRefund").val());
            var shipamount = parseFloat($("#fShipping").val());
            var final_refund = refundamount + shipamount;
            $("#fTotalAmount").val(final_refund);
        });
    }
};
Project.modules.main_area.init();