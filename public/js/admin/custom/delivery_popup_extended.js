Project.modules.delivery_area = {
    init: function () {
        // alert($("#mso_delivered_date").attr("shipping-date"));
        this.validate();
        this.initEvents();
        valid_more_elements = [];
        $('input[type=text]').keypress(function (event) {
            var unicode = event.charCode ? event.charCode : event.keyCode;
            if (unicode == 13) {
                if ($('#frmDelivery').valid()) {
                    $('#frmDelivery').submit();
                }
            }
        });
        $("#btnConfirm").live('click', function () {
            if ($('#frmDelivery').valid()) {
                $('#frmDelivery').submit();
            }
        });
        $("#btnCancel").live('click', function () {
            parent.$.fancybox.close();
        });

    },
    validate: function () {
        $("#frmDelivery").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",
            rules: {
                "mso_delivered_date": {
                    "required": true
                }
            },
            messages: {
                "mso_delivered_date": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWORDERLISTING_DELIVERED_DATE)
                }
            },
            errorPlacement: function (error, element) {
                switch (element.attr("name")) {
                    case 'mso_delivered_date':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    default:
                        printErrorMessage(element, valid_more_elements, error);
                        break;
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });
    },
    callEvents: function () {

    },
    initEvents: function (elem) {
        $('#mso_delivered_date').datepicker({
            dateFormat: 'dd/mm/yy',
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            // minDate:0,
            //minDate :"24/10/2015",
            minDate: $("#mso_delivered_date").attr("shipping-date"),
            yearRange: 'c-100:c+100',
            beforeShow: function (input, inst) {
             //  var $fancy = window.parent.document.getElementsByClassName('fancybox-inner');
            //   var $fancy1 = window.parent.document.getElementsByClassName('fancybox-skin);
             //  console.log($fancy[0]);
//console.log($(".fancybox-skin").length);
        $(".fancybox-skin").css("height", "800px !important");
            //    $($fancy[0]).css("height", "800px !important");
            //      $($fancy1[0]).css("height", "800px !important");
                 //  $.fancybox.update();
                  //   $.parent.fancybox.update();
            //    $(".fancybox-skin").css("height", "800px !important");
              //  $(".fancybox-inner").css("height", "800px !important");
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function () {
                    cal.css({
                        'left': left
                    });
                }, 10);
            },
            onSelect: function (dateText) {
               $(".fancybox-inner").css("height", "800px !important");
            }
        });

    }
}