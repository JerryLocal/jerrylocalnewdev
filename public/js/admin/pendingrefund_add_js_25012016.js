/** pendingrefund module script */
Project.modules.pendingrefund = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "trr_status": {
                    "required": true
                }
            },
            messages: {
                "trr_status": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PENDINGREFUND_STATUS)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'trr_status':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        $('#trr_reason').elastic();
        $('#trr_comment_to_customer').elastic();

        $('#trr_refund_date').datepicker({
            dateFormat: getAdminJSFormat('date'),
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#trr_refund_date').attr('readonly', true);
        }


        $('#trr_date').datepicker({
            dateFormat: getAdminJSFormat('date'),
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#trr_date').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.pendingrefund.init();