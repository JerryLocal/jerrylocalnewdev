/** dashboardcustomerstate module script */
Project.modules.dashboardcustomerstate = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "mc_phone_no": {
                    "required": true
                },
                "mc_d_o_b": {
                    "required": true,
                    "date": true
                },
                "mc_gender": {
                    "required": true
                }
            },
            messages: {
                "mc_phone_no": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.DASHBOARDCUSTOMERSTATE_PHONE_NO)
                },
                "mc_d_o_b": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.DASHBOARDCUSTOMERSTATE_D_O_B),
                    "date": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_DATE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.DASHBOARDCUSTOMERSTATE_D_O_B)
                },
                "mc_gender": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.DASHBOARDCUSTOMERSTATE_GENDER)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'mc_phone_no':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mc_d_o_b':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mc_gender':
                    $('#mc_genderErr').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        $('#mc_phone_no').mask('(999) 999-9999');

        $('#mc_d_o_b').datepicker({
            dateFormat: 'yy-mm-dd',
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#mc_d_o_b').attr('readonly', true);
        }


        $('#mc_t_registered_date').datepicker({
            dateFormat: 'yy-mm-dd',
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#mc_t_registered_date').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.dashboardcustomerstate.init();