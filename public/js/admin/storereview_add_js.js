/** storereview module script */
Project.modules.storereview = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {

        $('#rating_tsr_rate').raty({
            number: 5,
            cancel: false,
            half: false,
            targetKeep: true,
            ratyIconSize: 'icon18',
            cancelSize: 'icon15',
            clear: (($('#tsr_rate').attr('aria-raty-clear') == 'false') ? false : true),
            readOnly: (($('#tsr_rate').attr('aria-raty-readonly') == 'false') ? true : false),
            score: $('#tsr_rate').val(),
            hints: ['1', '2', '3', '4', '5'],
            target: '#tsr_rate'
        });
        $('#tsr_review').elastic();

        $('#tsr_date').datepicker({
            dateFormat: 'MM d, yy',
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#tsr_date').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.storereview.init();