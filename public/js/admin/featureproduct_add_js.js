/** featureproduct module script */
Project.modules.featureproduct = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "tfp_mst_products_id": {
                    "required": true
                },
                "tfp_status": {
                    "required": true
                }
            },
            messages: {
                "tfp_mst_products_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.FEATUREPRODUCT_PRODUCT)
                },
                "tfp_status": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.FEATUREPRODUCT_STATUS)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'tfp_mst_products_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'tfp_status':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {},
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.featureproduct.init();