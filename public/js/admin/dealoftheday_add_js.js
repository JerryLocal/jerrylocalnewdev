/** dealoftheday module script */
Project.modules.dealoftheday = {
    init: function() {
        $(document).off("change", "#sys_custom_field_1");
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "td_mst_products_id": {
                    "required": true
                },
                "td_status": {
                    "required": true
                }
            },
            messages: {
                "td_mst_products_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.DEALOFTHEDAY_PRODUCT)
                },
                "td_status": {
                    "required": true
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'td_mst_products_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'td_status':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {

        $(document).on('change', '#sys_custom_field_1', function() {
            adminAjaxChangeEventData(this, 'td_mst_products_id', el_form_settings.parent_source_options_url, 'td_mst_products_id', $('#mode').val(), $(this).val(), $('#id').val())
        });
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.dealoftheday.init();