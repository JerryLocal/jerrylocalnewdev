/** faqcategory module script */
Project.modules.faqcategory = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "mfc_type": {
                    "required": true
                }
            },
            messages: {
                "mfc_type": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.FAQCATEGORY_TYPE)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'mfc_type':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {},
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.faqcategory.init();