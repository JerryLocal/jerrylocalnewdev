/** download module script */
Project.modules.download = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",
            rules: {
                "mc_parent_id": {
                    "required": true
                }
            },
            messages: {
                "mc_parent_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.DOWNLOAD_CATEGORY)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'mc_parent_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {},
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {
        // $(document).on('click', ".pdf-button", function() {
        $(document).on('click', ".pdf-button-dnld", function() {
            
            var attr_type = $(this).attr("area-attr");
            $("#pdf_type").val(attr_type);
            var extra_params = frmaddupdate.elements["mc_parent_id"].value;
            if($("#frmaddupdate").valid()){
                window.location.href = admin_url + 'download/download/listing?categoryId=' + extra_params;
            }
        });

    }
}
Project.modules.download.init();