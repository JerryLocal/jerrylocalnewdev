/** sellerproductmanagement module script */
Project.modules.sellerproductmanagement = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "mp_title": {
                    "required": true
                },
                "mp_regular_price": {
                    "number": true
                },
                "mp_sale_price": {
                    "required": true,
                    "number": true
                },
                "mp_stock": {
                    "required": true,
                    "number": true
                },
                "mp_return_days": {
                    "number": true
                },
                "mp_allow_max_purchase": {
                    "number": true
                }
            },
            messages: {
                "mp_title": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLERPRODUCTMANAGEMENT_TITLE)
                },
                "mp_regular_price": {
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLERPRODUCTMANAGEMENT_REGULAR_PRICE)
                },
                "mp_sale_price": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLERPRODUCTMANAGEMENT_SALE_PRICE),
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLERPRODUCTMANAGEMENT_SALE_PRICE)
                },
                "mp_stock": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLERPRODUCTMANAGEMENT_STOCK),
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLERPRODUCTMANAGEMENT_STOCK)
                },
                "mp_return_days": {
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLERPRODUCTMANAGEMENT_RETURN_DAYS)
                },
                "mp_allow_max_purchase": {
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLERPRODUCTMANAGEMENT_ALLOW_MAX_PURCHASE)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'mp_title':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_regular_price':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_sale_price':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_stock':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_return_days':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_allow_max_purchase':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        $('#mp_short_description').elastic();

        removeIndividualTinyMCEEditor('mp_description');
        $('#mp_description').tinymce({
            script_url: admin_js_url + 'forms/tinymce/tinymce.min.js',
            content_css: admin_style_url + 'style.css',
            theme: 'modern',
            height: 200,
            width: '51%',
            resize: 'both',
            image_advtab: true,
            external_filemanager_path: el_tpl_settings.js_libraries_url + 'filemanager/',
            filemanager_title: js_lang_label.GENERIC_RESPONSIVE_FILEMANAGER,
            skin: 'light',
            plugins: tinmce_editor_plugins_basic,
            toolbar: timymce_editor_tollbar_basic,
            templates: tinmce_editor_templates,
            external_plugins: {
                'filemanager': el_tpl_settings.js_libraries_url + 'filemanager/plugin.min.js'
            },
            convert_urls: true,
            relative_urls: false,
            setup: function(ed) {
                ed.on('change', function(e) {
                    tinyMCE.triggerSave();
                });
                ed.on('click', function(e) {
                    tinyMCE.get(ed.id).focus();
                });
            }
        });


        $('#rating_mp_rating_avg').raty({
            number: 5,
            cancel: false,
            half: true,
            targetKeep: true,
            precision: true,
            cancelOff: 'cancel-custom-off.png',
            cancelOn: 'cancel-custom-on.png',
            starOff: 'star-off.png',
            starOn: 'star-on.png',
            starHalf: 'star-half.png',
            clear: (($('#mp_rating_avg').attr('aria-raty-clear') == 'false') ? false : true),
            readOnly: (($('#mp_rating_avg').attr('aria-raty-readonly') == 'false') ? true : false),
            score: $('#mp_rating_avg').val(),
            hints: ['1', '2', '3', '4', '5'],
            target: '#mp_rating_avg'
        });

        $('#mp_date').datepicker({
            dateFormat: 'MM d, yy',
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#mp_date').attr('readonly', true);
        }


        $('#mp_modify_date').datepicker({
            dateFormat: 'yy-mm-dd',
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#mp_modify_date').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.sellerproductmanagement.init();