/** storestaticpage module script */
Project.modules.storestaticpage = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        var mode = $("#mode").val();
        if (mode == cus_enc_mode_json["Update"]) {

            $("#frmaddupdate_1_1").validate({
                onfocusout: false,
                ignore: ".ignore-valid, .ignore-show-hide",

                invalidHandler: function(form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                },
                submitHandler: function(form) {
                    getAdminTabLevelFormValidate("1_1");
                    return false;
                }
            });

        } else {

            $("#frmaddupdate").validate({
                onfocusout: false,
                ignore: ".ignore-valid, .ignore-show-hide",

                invalidHandler: function(form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                },
                submitHandler: function(form) {
                    getAdminFormValidate();
                    return false;
                }
            });

        }
    },
    callEvents: function() {
        this.validate();
        this.initEvents("aboutstore");
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        switch (elem) {

        case "aboutstore":


            removeIndividualTinyMCEEditor('msp_about_store');
            $('#msp_about_store').tinymce({
                script_url: admin_js_url + 'forms/tinymce/tinymce.min.js',
                content_css: admin_style_url + 'style.css',
                theme: 'modern',
                height: 300,
                width: '51%',
                resize: 'both',
                image_advtab: true,
                external_filemanager_path: el_tpl_settings.js_libraries_url + 'filemanager/',
                filemanager_title: js_lang_label.GENERIC_RESPONSIVE_FILEMANAGER,
                skin: 'light',
                plugins: tinmce_editor_plugins_basic,
                toolbar: timymce_editor_tollbar_basic,
                templates: tinmce_editor_templates,
                external_plugins: {
                    'filemanager': el_tpl_settings.js_libraries_url + 'filemanager/plugin.min.js'
                },
                convert_urls: true,
                relative_urls: false,
                setup: function(ed) {
                    ed.on('change', function(e) {
                        tinyMCE.triggerSave();
                    });
                    ed.on('click', function(e) {
                        tinyMCE.get(ed.id).focus();
                    });
                }
            });

            break;
        case "returnpolicy":


            removeIndividualTinyMCEEditor('msp_return_policy');
            $('#msp_return_policy').tinymce({
                script_url: admin_js_url + 'forms/tinymce/tinymce.min.js',
                content_css: admin_style_url + 'style.css',
                theme: 'modern',
                height: 300,
                width: '51%',
                resize: 'both',
                image_advtab: true,
                external_filemanager_path: el_tpl_settings.js_libraries_url + 'filemanager/',
                filemanager_title: js_lang_label.GENERIC_RESPONSIVE_FILEMANAGER,
                skin: 'light',
                plugins: tinmce_editor_plugins_basic,
                toolbar: timymce_editor_tollbar_basic,
                templates: tinmce_editor_templates,
                external_plugins: {
                    'filemanager': el_tpl_settings.js_libraries_url + 'filemanager/plugin.min.js'
                },
                convert_urls: true,
                relative_urls: false,
                setup: function(ed) {
                    ed.on('change', function(e) {
                        tinyMCE.triggerSave();
                    });
                    ed.on('click', function(e) {
                        tinyMCE.get(ed.id).focus();
                    });
                }
            });

            break;
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.storestaticpage.init();