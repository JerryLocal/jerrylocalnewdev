/** newslettertemplate module script */
Project.modules.newslettertemplate = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "mnt_subject": {
                    "required": true
                },
                "mnt_message": {
                    "required": true
                },
                "mnt_sender_name": {
                    "required": true
                },
                "mnt_sender_email": {
                    "email": true
                },
                "mnt_date": {
                    "required": true
                },
                "mnt_status": {
                    "required": true
                }
            },
            messages: {
                "mnt_subject": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWSLETTERTEMPLATE_NEWSLETTER_TITLE)
                },
                "mnt_message": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWSLETTERTEMPLATE_NEWSLETTER_CONTENT)
                },
                "mnt_sender_name": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWSLETTERTEMPLATE_FROM_NAME)
                },
                "mnt_sender_email": {
                    "email": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_EMAIL_ADDRESS_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWSLETTERTEMPLATE_FROM_EMAIL)
                },
                "mnt_date": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWSLETTERTEMPLATE_DATE)
                },
                "mnt_status": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWSLETTERTEMPLATE_STATUS)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'mnt_subject':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mnt_message':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mnt_sender_name':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mnt_sender_email':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mnt_date':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mnt_status':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {

        removeIndividualTinyMCEEditor('mnt_message');
        $('#mnt_message').tinymce({
            script_url: admin_js_url + 'forms/tinymce/tinymce.min.js',
            content_css: admin_style_url + 'style.css',
            theme: 'modern',
            height: 200,
            width: '51%',
            resize: 'both',
            image_advtab: true,
            external_filemanager_path: el_tpl_settings.js_libraries_url + 'filemanager/',
            filemanager_title: js_lang_label.GENERIC_RESPONSIVE_FILEMANAGER,
            skin: 'light',
            plugins: tinmce_editor_plugins_basic,
            toolbar: timymce_editor_tollbar_basic,
            templates: tinmce_editor_templates,
            external_plugins: {
                'filemanager': el_tpl_settings.js_libraries_url + 'filemanager/plugin.min.js'
            },
            convert_urls: true,
            relative_urls: false,
            setup: function(ed) {
                ed.on('change', function(e) {
                    tinyMCE.triggerSave();
                });
                ed.on('click', function(e) {
                    tinyMCE.get(ed.id).focus();
                });
            }
        });


        $('#mnt_date').datepicker({
            dateFormat: 'yy-mm-dd',
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#mnt_date').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.newslettertemplate.init();