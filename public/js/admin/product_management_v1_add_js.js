/** product_management_v1 module script */
Project.modules.product_management_v1 = {
    init: function() {
        $(document).off("change", "#mp_do_return");
        $(document).off("change", "#mp_free_shipping");
        valid_more_elements = ["child[productimages][mpi_img_path]", "child[productimages][mpi_default]"];
        cc_json_1 = [{
            "cod_type": "AND",
            "show_list": [{
                "id": "mp_return_days"
            }],
            "cond_list": [{
                "id": "mp_do_return",
                "type": "dropdown",
                "oper": "eq",
                "value": ["Yes"]
            }]
        }];
        cc_json_2 = [{
            "cod_type": "AND",
            "show_list": [{
                "id": "mp_shipping_charge"
            }],
            "cond_list": [{
                "id": "mp_free_shipping",
                "type": "dropdown",
                "oper": "eq",
                "value": ["No"]
            }]
        }];
        $(document).on("change", "[name='mp_do_return']", function() {
            checkCCEventValues(cc_json_1);
        });
        $(document).on("change", "[name='mp_free_shipping']", function() {
            checkCCEventValues(cc_json_2);
        });
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "tpc_mst_categories_id": {
                    "required": true
                },
                "mp_title": {
                    "required": true
                },
                "mp_sku": {
                    "required": true
                },
                "mp_regular_price": {
                    "required": true,
                    "number": true,
                    "min": "1"
                },
                "mp_sale_price": {
                    "required": true,
                    "number": true,
                    "min": "1",
                    "numLessEqual": "#mp_regular_price"
                },
                "mp_allow_max_purchase": {
                    "number": true
                },
                "mp_stock": {
                    "required": true,
                    "number": true
                },
                "mp_shipping_charge": {
                    "required": true,
                    "number": true
                },
                "mp_return_days": {
                    "number": true
                },
                "mp_description": {
                    "required": true
                }
            },
            messages: {
                "tpc_mst_categories_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_CATEGORY)
                },
                "mp_title": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_PRODUCT_NAME)
                },
                "mp_sku": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_SKU)
                },
                "mp_regular_price": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_REGULAR_PRICE),
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_REGULAR_PRICE),
                    "min": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MINIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_REGULAR_PRICE)
                },
                "mp_sale_price": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_SALE_PRICE),
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_SALE_PRICE),
                    "min": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MINIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_SALE_PRICE),
                    "numLessEqual": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_SALE_PRICE)
                },
                "mp_allow_max_purchase": {
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_ALLOW_MAX_PURCHASE)
                },
                "mp_stock": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_CURRENT_STOCK),
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_CURRENT_STOCK)
                },
                "mp_shipping_charge": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_SHIPPING_CHARGE),
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_SHIPPING_CHARGE)
                },
                "mp_return_days": {
                    "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_RETURN_DAYS)
                },
                "mp_description": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.PRODUCT_MANAGEMENT_V1_DESCRIPTION)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'tpc_mst_categories_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_title':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_sku':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_regular_price':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_sale_price':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_allow_max_purchase':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_stock':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_shipping_charge':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_return_days':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mp_description':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        this.childEvents("productimages", "#child_module_productimages");
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        $('#mp_short_description').elastic();

        removeIndividualTinyMCEEditor('mp_description');
        $('#mp_description').tinymce({
            script_url: admin_js_url + 'forms/tinymce/tinymce.min.js',
            content_css: admin_style_url + 'style.css',
            theme: 'modern',
            height: 200,
            width: '51%',
            resize: 'both',
            image_advtab: true,
            external_filemanager_path: el_tpl_settings.js_libraries_url + 'filemanager/',
            filemanager_title: js_lang_label.GENERIC_RESPONSIVE_FILEMANAGER,
            skin: 'light',
            plugins: tinmce_editor_plugins_basic,
            toolbar: timymce_editor_tollbar_basic,
            templates: tinmce_editor_templates,
            external_plugins: {
                'filemanager': el_tpl_settings.js_libraries_url + 'filemanager/plugin.min.js'
            },
            convert_urls: true,
            relative_urls: false,
            setup: function(ed) {
                ed.on('change', function(e) {
                    tinyMCE.triggerSave();
                });
                ed.on('click', function(e) {
                    tinyMCE.get(ed.id).focus();
                });
            }
        });

        $('#mp_search_tag').elastic();
        this.childEvents("productimages", "#child_module_productimages");
    },
    childEvents: function(elem, eleObj) {
        switch (elem) {

        case "productimages":
            var is_popup = $("#childModulePopup_productimages").val();
            if (is_popup != "Yes") {



                $(eleObj).find("[name^='uploadify_child[productimages][mpi_img_path]']").each(function() {
                    var ele_id = $(this).attr('id');
                    var last_id = ele_id.split('_').pop();
                    var act_id = ele_id.split('_').slice(1).join('_');
                    var temp_id = 'child_productimages_temp_mpi_img_path_' + last_id;
                    var id_val = $('#child_productimages_enc_id_' + last_id).val();
                    $('#upload_drop_zone_' + act_id).width($(this).width() + 18);
                    $(this).fileupload({
                        name: act_id,
                        temp: temp_id,
                        url: admin_url + '' + $('#childModuleUploadURL_productimages').val() + '?',
                        paramName: 'Filedata',
                        maxFileSize: '2048',
                        acceptFileTypes: 'gif|png|jpg|jpeg',
                        dropZone: $('#upload_drop_zone_' + act_id + ', #upload_drop_zone_' + act_id + ' + .upload-src-zone'),
                        formData: {
                            'unique_name': 'mpi_img_path',
                            'id': id_val,
                            'type': 'uploadify'
                        },
                        add: function(e, data) {
                            var upload_errors = [];
                            var _input_name = $(this).fileupload('option', 'name');
                            var _temp_name = $(this).fileupload('option', 'temp');
                            var _form_data = $(this).fileupload('option', 'formData');
                            var _file_size = $(this).fileupload('option', 'maxFileSize');
                            var _file_type = $(this).fileupload('option', 'acceptFileTypes');

                            var _input_val = data.originalFiles[0]['name'];
                            var _input_size = data.originalFiles[0]['size'];
                            if (_file_type != '*') {
                                var _input_ext = (_input_val) ? _input_val.substr(_input_val.lastIndexOf('.')) : '';
                                var accept_file_types = new RegExp('(\.|\/)(' + _file_type + ')$', 'i');
                                if (_input_ext && !accept_file_types.test(_input_ext)) {
                                    upload_errors.push(js_lang_label.ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE);
                                }
                            }
                            _file_size = _file_size * 1000;
                            if (_input_size && _input_size > _file_size) {
                                upload_errors.push(js_lang_label.ACTION_FILE_SIZE_IS_TOO_LARGE);
                            }
                            if (upload_errors.length > 0) {
                                Project.setMessage(upload_errors.join('\n'), 0);
                            } else {
                                $('#practive_' + _input_name).css('width', '0%');
                                $('#progress_' + _input_name).show();
                                _form_data['oldFile'] = $('#' + _temp_name).val();
                                $(this).fileupload('option', 'formData', _form_data);
                                $('#preview_' + _input_name).html(data.originalFiles[0]['name']);
                                data.submit();
                            }
                        },
                        done: function(e, data) {
                            if (data && data.result) {
                                var _input_name = $(this).fileupload('option', 'name');
                                var _temp_name = $(this).fileupload('option', 'temp');
                                var jparse_data = $.parseJSON(data.result);
                                if (jparse_data.success == '0') {
                                    Project.setMessage(jparse_data.message, 0);
                                } else {
                                    $('#' + _input_name).val(jparse_data.uploadfile);
                                    $('#' + _temp_name).val(jparse_data.oldfile);
                                    displayAdminOntheFlyImage(_input_name, jparse_data);
                                    setTimeout(function() {
                                        $('#progress_' + _input_name).hide();
                                    }, 1000);
                                }
                            }
                        },
                        fail: function(e, data) {
                            $.each(data.messages, function(index, error) {
                                Project.setMessage(error, 0);
                            });
                        },
                        progressall: function(e, data) {
                            var _input_name = $(this).fileupload('option', 'name');
                            var progress = parseInt(data.loaded / data.total * 100, 10);
                            $('#practive_' + _input_name).css('width', progress + '%');
                        }
                    });
                });

            }
            break;
        }
    },
    CCEvents: function() {
        pre_cond_code_arr.push(cc_json_1);
        pre_cond_code_arr.push(cc_json_2);
    }
}
Project.modules.product_management_v1.init();