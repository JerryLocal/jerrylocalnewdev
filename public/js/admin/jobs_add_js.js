/** jobs module script */
Project.modules.jobs = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "mj_job_code": {
                    "required": true
                },
                "mj_job_title": {
                    "required": true
                },
                "mj_job_description": {
                    "required": true
                },
                "mj_from_salary": {
                    "required": true
                },
                "mj_to_salary": {
                    "required": true
                },
                "mj_contact_email": {
                    "required": true
                }
            },
            messages: {
                "mj_job_code": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.JOBS_JOB_CODE)
                },
                "mj_job_title": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.JOBS_JOB_TITLE)
                },
                "mj_job_description": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.JOBS_JOB_DESCRIPTION)
                },
                "mj_from_salary": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.JOBS_FROM_SALARY)
                },
                "mj_to_salary": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.JOBS_TO_SALARY)
                },
                "mj_contact_email": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.JOBS_CONTACT_EMAIL)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'mj_job_code':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mj_job_title':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mj_job_description':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mj_from_salary':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mj_to_salary':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mj_contact_email':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        $('#mj_job_description').elastic();
        $('#mj_notes').elastic();
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.jobs.init();