/** refundedlisting module script */
Project.modules.refundedlisting = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        $('#trr_reason').elastic();
        $('#trr_comment_to_customer').elastic();

        $('#trr_refund_date').datepicker({
            dateFormat: 'yy-mm-dd',
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#trr_refund_date').attr('readonly', true);
        }


        $('#trr_date').datepicker({
            dateFormat: 'yy-mm-dd',
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#trr_date').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.refundedlisting.init();