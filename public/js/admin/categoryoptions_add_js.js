/** categoryoptions module script */
Project.modules.categoryoptions = {
    init: function() {
        valid_more_elements = ["child[optionvaluemaster][mcov_value]"];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "mco_mst_categories_id": {
                    "required": true
                },
                "mco_mst_options_id": {
                    "required": true
                },
                "mco_order_id": {
                    "digits": true
                }
            },
            messages: {
                "mco_mst_categories_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CATEGORYOPTIONS_CATEGORY_NAME)
                },
                "mco_mst_options_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CATEGORYOPTIONS_OPTION_NAME)
                },
                "mco_order_id": {
                    "digits": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_DIGITS_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CATEGORYOPTIONS_SORT_ORDER)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'mco_mst_categories_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mco_mst_options_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mco_order_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        this.childEvents("optionvaluemaster", "#child_module_optionvaluemaster");
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        this.childEvents("optionvaluemaster", "#child_module_optionvaluemaster");
    },
    childEvents: function(elem, eleObj) {
        switch (elem) {

        case "optionvaluemaster":
            var is_popup = $("#childModulePopup_optionvaluemaster").val();
            if (is_popup != "Yes") {

                if ($("[name^='child[optionvaluemaster][mcov_value]'").length) {
                    $("[name^='child[optionvaluemaster][mcov_value]'").each(function() {
                        $(this).rules("add", {
                            "required": true,
                            "messages": {
                                "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.OPTIONVALUEMASTER_VALUE)
                            }
                        });
                    });
                }

            }
            break;
        }
    },
    CCEvents: function() {}
}
Project.modules.categoryoptions.init();