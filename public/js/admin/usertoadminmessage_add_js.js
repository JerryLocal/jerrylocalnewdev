/** usertoadminmessage module script */
Project.modules.usertoadminmessage = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "tm_sender_name": {
                    "required": true
                },
                "tm_sender_email": {
                    "required": true,
                    "email": true
                },
                "tm_subject": {
                    "required": true
                }
            },
            messages: {
                "tm_sender_name": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.USERTOADMINMESSAGE_SENDER_NAME)
                },
                "tm_sender_email": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.USERTOADMINMESSAGE_SENDER_EMAIL),
                    "email": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_EMAIL_ADDRESS_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.USERTOADMINMESSAGE_SENDER_EMAIL)
                },
                "tm_subject": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.USERTOADMINMESSAGE_SUBJECT)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'tm_sender_name':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'tm_sender_email':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'tm_subject':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        $('#tm_detail').elastic();

        $('#tm_date').datepicker({
            dateFormat: 'MM d, yy',
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#tm_date').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.usertoadminmessage.init();