/** city module script */
Project.modules.city = {
    init: function() {
        $(document).off("change", "#mc_country_id");
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "mc_country_id": {
                    "required": true
                },
                "mc_state_id": {
                    "required": true
                }
            },
            messages: {
                "mc_country_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CITY_COUNTRY)
                },
                "mc_state_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CITY_STATE)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'mc_country_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mc_state_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {

        $(document).on('change', '#mc_country_id', function() {
            adminAjaxChangeEventData(this, 'mc_state_id', el_form_settings.parent_source_options_url, 'mc_state_id', $('#mode').val(), $(this).val(), $('#id').val())
        });
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.city.init();