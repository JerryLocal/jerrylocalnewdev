/** menuitems module script */
Project.modules.menuitems = {
    init: function() {
        $(document).off("change", "#mmi_resource_type");
        valid_more_elements = [];
        cc_json_1 = [{
            "cod_type": "AND",
            "show_list": [{
                "id": "mmi_external_link"
            }],
            "hide_list": [{
                "id": "mmi_product_id"
            }, {
                "id": "mmi_category_id"
            }, {
                "id": "mmi_static_page_id"
            }],
            "cond_list": [{
                "id": "mmi_resource_type",
                "type": "dropdown",
                "oper": "eq",
                "value": ["External_Link"]
            }]
        }];
        cc_json_2 = [{
            "cod_type": "AND",
            "show_list": [{
                "id": "mmi_product_id"
            }],
            "hide_list": [{
                "id": "mmi_category_id"
            }, {
                "id": "mmi_static_page_id"
            }, {
                "id": "mmi_external_link"
            }],
            "cond_list": [{
                "id": "mmi_resource_type",
                "type": "dropdown",
                "oper": "eq",
                "value": ["Product"]
            }]
        }];
        cc_json_3 = [{
            "cod_type": "AND",
            "show_list": [{
                "id": "mmi_category_id"
            }],
            "hide_list": [{
                "id": "mmi_product_id"
            }, {
                "id": "mmi_static_page_id"
            }, {
                "id": "mmi_external_link"
            }],
            "cond_list": [{
                "id": "mmi_resource_type",
                "type": "dropdown",
                "oper": "eq",
                "value": ["Category"]
            }]
        }];
        cc_json_4 = [{
            "cod_type": "AND",
            "show_list": [{
                "id": "mmi_static_page_id"
            }],
            "hide_list": [{
                "id": "mmi_product_id"
            }, {
                "id": "mmi_category_id"
            }, {
                "id": "mmi_external_link"
            }],
            "cond_list": [{
                "id": "mmi_resource_type",
                "type": "dropdown",
                "oper": "eq",
                "value": ["Static_Page"]
            }]
        }];
        $(document).on("change", "[name='mmi_resource_type']", function() {
            checkCCEventValues((cc_json_1).concat(cc_json_2).concat(cc_json_3).concat(cc_json_4));
        });
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {},
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {
        pre_cond_code_arr.push((cc_json_1).concat(cc_json_2).concat(cc_json_3).concat(cc_json_4));
    }
}
Project.modules.menuitems.init();