/** systememails module script */
Project.modules.systememails = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {

        removeIndividualTinyMCEEditor('mse_email_message');
        $('#mse_email_message').tinymce({
            script_url: admin_js_url + 'forms/tinymce/tinymce.min.js',
            content_css: admin_style_url + 'style.css',
            theme: 'modern',
            height: 200,
            width: '51%',
            resize: 'both',
            image_advtab: true,
            external_filemanager_path: el_tpl_settings.js_libraries_url + 'filemanager/',
            filemanager_title: js_lang_label.GENERIC_RESPONSIVE_FILEMANAGER,
            skin: 'light',
            plugins: tinmce_editor_plugins_basic,
            toolbar: timymce_editor_tollbar_basic,
            templates: tinmce_editor_templates,
            external_plugins: {
                'filemanager': el_tpl_settings.js_libraries_url + 'filemanager/plugin.min.js'
            },
            convert_urls: true,
            relative_urls: false,
            setup: function(ed) {
                ed.on('change', function(e) {
                    tinyMCE.triggerSave();
                });
                ed.on('click', function(e) {
                    tinyMCE.get(ed.id).focus();
                });
            }
        });
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.systememails.init();