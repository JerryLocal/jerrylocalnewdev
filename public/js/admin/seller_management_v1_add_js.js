/** seller_management_v1 module script */
Project.modules.seller_management_v1 = {
    init: function() {
        $(document).off("change", "#msd_country_id");
        $(document).off("change", "#msd_state_id");
        $(document).off("click", "#msd_have_online_store");
        valid_more_elements = [];
        cc_json_1 = [{
            "cod_type": "AND",
            "show_list": [{
                "id": "msd_store_url"
            }],
            "cond_list": [{
                "id": "msd_have_online_store",
                "type": "radio_buttons",
                "oper": "eq",
                "value": ["Yes"]
            }]
        }];
        $(document).on("click", "[name='msd_have_online_store']", function() {
            checkCCEventValues(cc_json_1);
        });
    },
    validate: function() {
        var mode = $("#mode").val();
        if (mode == cus_enc_mode_json["Update"]) {

            $("#frmaddupdate_1_1").validate({
                onfocusout: false,
                ignore: ".ignore-valid, .ignore-show-hide",

                rules: {
                    "ma_email": {
                        "required": true,
                        "email": true
                    },
                    "ma_password": {
                        "required": true
                    },
                    "retypema_password": {
                        "required": true,
                        "equalTo": "#ma_password"
                    },
                    "msd_store_name": {
                        "required": true
                    },
                    "msd_pin_code": {
                        "number": true
                    },
                    "msd_have_online_store": {
                        "required": true
                    }
                },
                messages: {
                    "ma_email": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_EMAIL),
                        "email": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_EMAIL_ADDRESS_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_EMAIL)
                    },
                    "ma_password": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_PASSWORD)
                    },
                    "retypema_password": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALUE_FOR_RE_C45TYPE__C35FIELD_C35_FIELD, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_PASSWORD),
                        "equalTo": ci_js_validation_message(js_lang_label.GENERIC_C35FIELD_C35_DOES_NOT_MATCH, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_PASSWORD)
                    },
                    "msd_store_name": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_STORE_NAME)
                    },
                    "msd_pin_code": {
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_PIN_CODE)
                    },
                    "msd_have_online_store": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_DO_YOU_SELL_PRODUCT_ONLINE_ALREADY_C63)
                    }
                },
                errorPlacement: function(error, element) {
                    switch (element.attr("name")) {

                    case 'ma_email':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'ma_password':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'retypema_password':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_store_name':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_pin_code':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_have_online_store':
                        $('#msd_have_online_storeErr').html(error);
                        break;
                    default:
                        printErrorMessage(element, valid_more_elements, error);
                        break;
                    }
                },
                invalidHandler: function(form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                },
                submitHandler: function(form) {
                    getAdminTabLevelFormValidate("1_1");
                    return false;
                }
            });

        } else {

            $("#frmaddupdate").validate({
                onfocusout: false,
                ignore: ".ignore-valid, .ignore-show-hide",

                rules: {
                    "ma_email": {
                        "required": true,
                        "email": true
                    },
                    "ma_password": {
                        "required": true
                    },
                    "retypema_password": {
                        "required": true,
                        "equalTo": "#ma_password"
                    },
                    "msd_store_name": {
                        "required": true
                    },
                    "msd_pin_code": {
                        "number": true
                    },
                    "msd_have_online_store": {
                        "required": true
                    },
                    "msd_accept_term[]": {
                        "required": true
                    }
                },
                messages: {
                    "ma_email": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_EMAIL),
                        "email": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_EMAIL_ADDRESS_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_EMAIL)
                    },
                    "ma_password": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_PASSWORD)
                    },
                    "retypema_password": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALUE_FOR_RE_C45TYPE__C35FIELD_C35_FIELD, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_PASSWORD),
                        "equalTo": ci_js_validation_message(js_lang_label.GENERIC_C35FIELD_C35_DOES_NOT_MATCH, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_PASSWORD)
                    },
                    "msd_store_name": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_STORE_NAME)
                    },
                    "msd_pin_code": {
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_PIN_CODE)
                    },
                    "msd_have_online_store": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_DO_YOU_SELL_PRODUCT_ONLINE_ALREADY_C63)
                    },
                    "msd_accept_term[]": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SELLER_MANAGEMENT_V1_ACCEPT_TERM)
                    }
                },
                errorPlacement: function(error, element) {
                    switch (element.attr("name")) {

                    case 'ma_email':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'ma_password':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'retypema_password':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_store_name':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_pin_code':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_have_online_store':
                        $('#msd_have_online_storeErr').html(error);
                        break;
                    case 'msd_accept_term[]':
                        $('#msd_accept_termErr').html(error);
                        break;
                    default:
                        printErrorMessage(element, valid_more_elements, error);
                        break;
                    }
                },
                invalidHandler: function(form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                },
                submitHandler: function(form) {
                    getAdminFormValidate();
                    return false;
                }
            });

        }
    },
    callEvents: function() {
        this.validate();
        this.initEvents("sellerregistration");
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        switch (elem) {

        case "sellerregistration":


            $('#upload_drop_zone_msd_store_logo').width($('#uploadify_msd_store_logo').width() + 18);
            $('#uploadify_msd_store_logo').fileupload({
                url: el_form_settings.upload_form_file_url,
                name: 'msd_store_logo',
                temp: 'temp_msd_store_logo',
                paramName: 'Filedata',
                maxFileSize: '2048',
                acceptFileTypes: 'gif|png|jpg|jpeg',
                dropZone: $('#upload_drop_zone_msd_store_logo, #upload_drop_zone_msd_store_logo + .upload-src-zone'),
                formData: {
                    'unique_name': 'msd_store_logo',
                    'id': $('#id').val(),
                    'type': 'uploadify'
                },
                add: function(e, data) {
                    var upload_errors = [];
                    var _input_name = $(this).fileupload('option', 'name');
                    var _temp_name = $(this).fileupload('option', 'temp');
                    var _form_data = $(this).fileupload('option', 'formData');
                    var _file_size = $(this).fileupload('option', 'maxFileSize');
                    var _file_type = $(this).fileupload('option', 'acceptFileTypes');

                    var _input_val = data.originalFiles[0]['name'];
                    var _input_size = data.originalFiles[0]['size'];
                    if (_file_type != '*') {
                        var _input_ext = (_input_val) ? _input_val.substr(_input_val.lastIndexOf('.')) : '';
                        var accept_file_types = new RegExp('(\.|\/)(' + _file_type + ')$', 'i');
                        if (_input_ext && !accept_file_types.test(_input_ext)) {
                            upload_errors.push(js_lang_label.ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE);
                        }
                    }
                    _file_size = _file_size * 1000;
                    if (_input_size && _input_size > _file_size) {
                        upload_errors.push(js_lang_label.ACTION_FILE_SIZE_IS_TOO_LARGE);
                    }
                    if (upload_errors.length > 0) {
                        Project.setMessage(upload_errors.join('\n'), 0);
                    } else {
                        $('#practive_' + _input_name).css('width', '0%');
                        $('#progress_' + _input_name).show();
                        _form_data['oldFile'] = $('#' + _temp_name).val();
                        $(this).fileupload('option', 'formData', _form_data);
                        $('#preview_' + _input_name).html(data.originalFiles[0]['name']);
                        data.submit();
                    }
                },
                done: function(e, data) {
                    if (data && data.result) {
                        var _input_name = $(this).fileupload('option', 'name');
                        var _temp_name = $(this).fileupload('option', 'temp');
                        var jparse_data = $.parseJSON(data.result);
                        if (jparse_data.success == '0') {
                            Project.setMessage(jparse_data.message, 0);
                        } else {
                            $('#' + _input_name).val(jparse_data.uploadfile);
                            $('#' + _temp_name).val(jparse_data.oldfile);
                            displayAdminOntheFlyImage(_input_name, jparse_data);
                            setTimeout(function() {
                                $('#progress_' + _input_name).hide();
                            }, 1000);
                        }
                    }
                },
                fail: function(e, data) {
                    $.each(data.messages, function(index, error) {
                        Project.setMessage(error, 0);
                    });
                },
                progressall: function(e, data) {
                    var _input_name = $(this).fileupload('option', 'name');
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#practive_' + _input_name).css('width', progress + '%');
                }
            });

            $('#msd_store_description').elastic();
            break;
        case "businessaccountdetails":

            $('#msd_contact_number').mask(getAdminJSFormat('phone'));

            $(document).on('change', '#msd_country_id', function() {
                adminAjaxChangeEventData(this, 'msd_state_id', el_form_settings.parent_source_options_url, 'msd_state_id', $('#mode').val(), $(this).val(), $('#id').val())
            });

            $(document).on('change', '#msd_state_id', function() {
                adminAjaxChangeEventData(this, 'msd_city_id', el_form_settings.parent_source_options_url, 'msd_city_id', $('#mode').val(), $(this).val(), $('#id').val())
            });
            break;
        case "bankdetails":

            $('#msd_bank_account_no').mask('9999-9999-9999-9999');
            $('#msd_g_s_t_no').mask('999-999-999');

            $('#upload_drop_zone_msd_g_s_t_i_r_d_proof').width($('#uploadify_msd_g_s_t_i_r_d_proof').width() + 18);
            $('#uploadify_msd_g_s_t_i_r_d_proof').fileupload({
                url: el_form_settings.upload_form_file_url,
                name: 'msd_g_s_t_i_r_d_proof',
                temp: 'temp_msd_g_s_t_i_r_d_proof',
                paramName: 'Filedata',
                maxFileSize: '2048',
                acceptFileTypes: 'gif|png|jpg|jpeg',
                dropZone: $('#upload_drop_zone_msd_g_s_t_i_r_d_proof, #upload_drop_zone_msd_g_s_t_i_r_d_proof + .upload-src-zone'),
                formData: {
                    'unique_name': 'msd_g_s_t_i_r_d_proof',
                    'id': $('#id').val(),
                    'type': 'uploadify'
                },
                add: function(e, data) {
                    var upload_errors = [];
                    var _input_name = $(this).fileupload('option', 'name');
                    var _temp_name = $(this).fileupload('option', 'temp');
                    var _form_data = $(this).fileupload('option', 'formData');
                    var _file_size = $(this).fileupload('option', 'maxFileSize');
                    var _file_type = $(this).fileupload('option', 'acceptFileTypes');

                    var _input_val = data.originalFiles[0]['name'];
                    var _input_size = data.originalFiles[0]['size'];
                    if (_file_type != '*') {
                        var _input_ext = (_input_val) ? _input_val.substr(_input_val.lastIndexOf('.')) : '';
                        var accept_file_types = new RegExp('(\.|\/)(' + _file_type + ')$', 'i');
                        if (_input_ext && !accept_file_types.test(_input_ext)) {
                            upload_errors.push(js_lang_label.ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE);
                        }
                    }
                    _file_size = _file_size * 1000;
                    if (_input_size && _input_size > _file_size) {
                        upload_errors.push(js_lang_label.ACTION_FILE_SIZE_IS_TOO_LARGE);
                    }
                    if (upload_errors.length > 0) {
                        Project.setMessage(upload_errors.join('\n'), 0);
                    } else {
                        $('#practive_' + _input_name).css('width', '0%');
                        $('#progress_' + _input_name).show();
                        _form_data['oldFile'] = $('#' + _temp_name).val();
                        $(this).fileupload('option', 'formData', _form_data);
                        $('#preview_' + _input_name).html(data.originalFiles[0]['name']);
                        data.submit();
                    }
                },
                done: function(e, data) {
                    if (data && data.result) {
                        var _input_name = $(this).fileupload('option', 'name');
                        var _temp_name = $(this).fileupload('option', 'temp');
                        var jparse_data = $.parseJSON(data.result);
                        if (jparse_data.success == '0') {
                            Project.setMessage(jparse_data.message, 0);
                        } else {
                            $('#' + _input_name).val(jparse_data.uploadfile);
                            $('#' + _temp_name).val(jparse_data.oldfile);
                            displayAdminOntheFlyImage(_input_name, jparse_data);
                            setTimeout(function() {
                                $('#progress_' + _input_name).hide();
                            }, 1000);
                        }
                    }
                },
                fail: function(e, data) {
                    $.each(data.messages, function(index, error) {
                        Project.setMessage(error, 0);
                    });
                },
                progressall: function(e, data) {
                    var _input_name = $(this).fileupload('option', 'name');
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#practive_' + _input_name).css('width', progress + '%');
                }
            });


            $('#upload_drop_zone_msd_bank_account_proof').width($('#uploadify_msd_bank_account_proof').width() + 18);
            $('#uploadify_msd_bank_account_proof').fileupload({
                url: el_form_settings.upload_form_file_url,
                name: 'msd_bank_account_proof',
                temp: 'temp_msd_bank_account_proof',
                paramName: 'Filedata',
                maxFileSize: '2048',
                acceptFileTypes: 'gif|png|jpg|jpeg',
                dropZone: $('#upload_drop_zone_msd_bank_account_proof, #upload_drop_zone_msd_bank_account_proof + .upload-src-zone'),
                formData: {
                    'unique_name': 'msd_bank_account_proof',
                    'id': $('#id').val(),
                    'type': 'uploadify'
                },
                add: function(e, data) {
                    var upload_errors = [];
                    var _input_name = $(this).fileupload('option', 'name');
                    var _temp_name = $(this).fileupload('option', 'temp');
                    var _form_data = $(this).fileupload('option', 'formData');
                    var _file_size = $(this).fileupload('option', 'maxFileSize');
                    var _file_type = $(this).fileupload('option', 'acceptFileTypes');

                    var _input_val = data.originalFiles[0]['name'];
                    var _input_size = data.originalFiles[0]['size'];
                    if (_file_type != '*') {
                        var _input_ext = (_input_val) ? _input_val.substr(_input_val.lastIndexOf('.')) : '';
                        var accept_file_types = new RegExp('(\.|\/)(' + _file_type + ')$', 'i');
                        if (_input_ext && !accept_file_types.test(_input_ext)) {
                            upload_errors.push(js_lang_label.ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE);
                        }
                    }
                    _file_size = _file_size * 1000;
                    if (_input_size && _input_size > _file_size) {
                        upload_errors.push(js_lang_label.ACTION_FILE_SIZE_IS_TOO_LARGE);
                    }
                    if (upload_errors.length > 0) {
                        Project.setMessage(upload_errors.join('\n'), 0);
                    } else {
                        $('#practive_' + _input_name).css('width', '0%');
                        $('#progress_' + _input_name).show();
                        _form_data['oldFile'] = $('#' + _temp_name).val();
                        $(this).fileupload('option', 'formData', _form_data);
                        $('#preview_' + _input_name).html(data.originalFiles[0]['name']);
                        data.submit();
                    }
                },
                done: function(e, data) {
                    if (data && data.result) {
                        var _input_name = $(this).fileupload('option', 'name');
                        var _temp_name = $(this).fileupload('option', 'temp');
                        var jparse_data = $.parseJSON(data.result);
                        if (jparse_data.success == '0') {
                            Project.setMessage(jparse_data.message, 0);
                        } else {
                            $('#' + _input_name).val(jparse_data.uploadfile);
                            $('#' + _temp_name).val(jparse_data.oldfile);
                            displayAdminOntheFlyImage(_input_name, jparse_data);
                            setTimeout(function() {
                                $('#progress_' + _input_name).hide();
                            }, 1000);
                        }
                    }
                },
                fail: function(e, data) {
                    $.each(data.messages, function(index, error) {
                        Project.setMessage(error, 0);
                    });
                },
                progressall: function(e, data) {
                    var _input_name = $(this).fileupload('option', 'name');
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#practive_' + _input_name).css('width', progress + '%');
                }
            });


            $('#upload_drop_zone_msd_other_proof').width($('#uploadify_msd_other_proof').width() + 18);
            $('#uploadify_msd_other_proof').fileupload({
                url: el_form_settings.upload_form_file_url,
                name: 'msd_other_proof',
                temp: 'temp_msd_other_proof',
                paramName: 'Filedata',
                maxFileSize: '2048',
                acceptFileTypes: 'gif|png|jpg|jpeg',
                dropZone: $('#upload_drop_zone_msd_other_proof, #upload_drop_zone_msd_other_proof + .upload-src-zone'),
                formData: {
                    'unique_name': 'msd_other_proof',
                    'id': $('#id').val(),
                    'type': 'uploadify'
                },
                add: function(e, data) {
                    var upload_errors = [];
                    var _input_name = $(this).fileupload('option', 'name');
                    var _temp_name = $(this).fileupload('option', 'temp');
                    var _form_data = $(this).fileupload('option', 'formData');
                    var _file_size = $(this).fileupload('option', 'maxFileSize');
                    var _file_type = $(this).fileupload('option', 'acceptFileTypes');

                    var _input_val = data.originalFiles[0]['name'];
                    var _input_size = data.originalFiles[0]['size'];
                    if (_file_type != '*') {
                        var _input_ext = (_input_val) ? _input_val.substr(_input_val.lastIndexOf('.')) : '';
                        var accept_file_types = new RegExp('(\.|\/)(' + _file_type + ')$', 'i');
                        if (_input_ext && !accept_file_types.test(_input_ext)) {
                            upload_errors.push(js_lang_label.ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE);
                        }
                    }
                    _file_size = _file_size * 1000;
                    if (_input_size && _input_size > _file_size) {
                        upload_errors.push(js_lang_label.ACTION_FILE_SIZE_IS_TOO_LARGE);
                    }
                    if (upload_errors.length > 0) {
                        Project.setMessage(upload_errors.join('\n'), 0);
                    } else {
                        $('#practive_' + _input_name).css('width', '0%');
                        $('#progress_' + _input_name).show();
                        _form_data['oldFile'] = $('#' + _temp_name).val();
                        $(this).fileupload('option', 'formData', _form_data);
                        $('#preview_' + _input_name).html(data.originalFiles[0]['name']);
                        data.submit();
                    }
                },
                done: function(e, data) {
                    if (data && data.result) {
                        var _input_name = $(this).fileupload('option', 'name');
                        var _temp_name = $(this).fileupload('option', 'temp');
                        var jparse_data = $.parseJSON(data.result);
                        if (jparse_data.success == '0') {
                            Project.setMessage(jparse_data.message, 0);
                        } else {
                            $('#' + _input_name).val(jparse_data.uploadfile);
                            $('#' + _temp_name).val(jparse_data.oldfile);
                            displayAdminOntheFlyImage(_input_name, jparse_data);
                            setTimeout(function() {
                                $('#progress_' + _input_name).hide();
                            }, 1000);
                        }
                    }
                },
                fail: function(e, data) {
                    $.each(data.messages, function(index, error) {
                        Project.setMessage(error, 0);
                    });
                },
                progressall: function(e, data) {
                    var _input_name = $(this).fileupload('option', 'name');
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#practive_' + _input_name).css('width', progress + '%');
                }
            });

            break;
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {
        pre_cond_code_arr.push(cc_json_1);
    }
}
Project.modules.seller_management_v1.init();