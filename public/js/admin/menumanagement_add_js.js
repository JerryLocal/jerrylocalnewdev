/** menumanagement module script */
Project.modules.menumanagement = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "mm_title": {
                    "required": true
                },
                "mm_menu_key": {
                    "required": true
                }
            },
            messages: {
                "mm_title": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.MENUMANAGEMENT_TITLE)
                },
                "mm_menu_key": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.MENUMANAGEMENT_MENU_KEY)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'mm_title':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mm_menu_key':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {},
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.menumanagement.init();