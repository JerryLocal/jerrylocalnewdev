/** storemanagement module script */
Project.modules.storemanagement = {
    init: function() {
        $(document).off("change", "#msd_country_id");
        $(document).off("change", "#msd_state_id");
        $(document).off("click", "#msd_have_online_store");
        valid_more_elements = [];
        cc_json_1 = [{
            "cod_type": "AND",
            "show_list": [{
                "id": "msd_store_url"
            }],
            "cond_list": [{
                "id": "msd_have_online_store",
                "type": "radio_buttons",
                "oper": "eq",
                "value": ["Yes"]
            }]
        }];
        $(document).on("click", "[name='msd_have_online_store']", function() {
            checkCCEventValues(cc_json_1);
        });
    },
    validate: function() {
        var mode = $("#mode").val();
        if (mode == cus_enc_mode_json["Update"]) {

            $("#frmaddupdate_1_1").validate({
                onfocusout: false,
                ignore: ".ignore-valid, .ignore-show-hide",

                rules: {
                    "msd_store_logo": {
                        "required": true
                    },
                    "msd_store_name": {
                        "required": true
                    },
                    "msd_company_name": {
                        "required": true
                    },
                    "msd_contact_name": {
                        "required": true
                    },
                    "msd_contact_number": {
                        "required": true
                    },
                    "msd_country_id": {
                        "required": true
                    },
                    "msd_state_id": {
                        "required": true
                    },
                    "msd_city_id": {
                        "required": true
                    },
                    "msd_mst_categories_id": {
                        "required": true
                    },
                    "msd_have_online_store": {
                        "required": true
                    },
                    "sys_custom_field_39": {
                        "required": true,
                        "number": true,
                        "max": "4"
                    },
                    "sys_custom_field_40": {
                        "required": true,
                        "number": true,
                        "max": "4"
                    },
                    "sys_custom_field_41": {
                        "required": true,
                        "number": true,
                        "max": "4"
                    },
                    "sys_custom_field_42": {
                        "required": true,
                        "number": true,
                        "max": "4"
                    },
                    "sys_custom_field_43": {
                        "required": true,
                        "number": true,
                        "max": "3"
                    },
                    "sys_custom_field_44": {
                        "required": true,
                        "number": true,
                        "max": "3"
                    },
                    "sys_custom_field_45": {
                        "required": true,
                        "number": true,
                        "max": "3"
                    },
                    "msd_g_s_t_i_r_d_proof": {
                        "required": true
                    },
                    "msd_bank_account_proof": {
                        "required": true
                    },
                    "msd_other_proof": {
                        "required": true
                    }
                },
                messages: {
                    "msd_store_logo": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_STORE_LOGO)
                    },
                    "msd_store_name": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_STORE_NAME)
                    },
                    "msd_company_name": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_COMPANY_NAME)
                    },
                    "msd_contact_name": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_CONTACT_NAME)
                    },
                    "msd_contact_number": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_CONTACT_NUMBER)
                    },
                    "msd_country_id": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_COUNTRY)
                    },
                    "msd_state_id": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_STATE)
                    },
                    "msd_city_id": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_CITY)
                    },
                    "msd_mst_categories_id": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_PRIMARY_CATEGORY)
                    },
                    "msd_have_online_store": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_DO_YOU_SELL_PRODUCT_ONLINE_ALREADY_C63)
                    },
                    "sys_custom_field_39": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_ACCOUNT_NO),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_ACCOUNT_NO),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_ACCOUNT_NO)
                    },
                    "sys_custom_field_40": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C452),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C452),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C452)
                    },
                    "sys_custom_field_41": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C453),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C453),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C453)
                    },
                    "sys_custom_field_42": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C454),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C454),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C454)
                    },
                    "sys_custom_field_43": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C451),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C451),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C451)
                    },
                    "sys_custom_field_44": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C452),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C452),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C452)
                    },
                    "sys_custom_field_45": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C453),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C453),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C453)
                    },
                    "msd_g_s_t_i_r_d_proof": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_UPLOAD_GST_C47IRD_DETAIL_PROOF)
                    },
                    "msd_bank_account_proof": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_UPLOAD_BANK_ACCOUNT_PROOF)
                    },
                    "msd_other_proof": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_UPLOAD_ANY_OTHER_PROOF)
                    }
                },
                errorPlacement: function(error, element) {
                    switch (element.attr("name")) {

                    case 'msd_store_logo':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_store_name':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_company_name':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_contact_name':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_contact_number':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_country_id':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_state_id':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_city_id':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_mst_categories_id':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_have_online_store':
                        $('#msd_have_online_storeErr').html(error);
                        break;
                    case 'sys_custom_field_39':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_40':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_41':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_42':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_43':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_44':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_45':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_g_s_t_i_r_d_proof':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_bank_account_proof':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_other_proof':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    default:
                        printErrorMessage(element, valid_more_elements, error);
                        break;
                    }
                },
                invalidHandler: function(form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                },
                submitHandler: function(form) {
                    getAdminTabLevelFormValidate("1_1");
                    return false;
                }
            });

        } else {

            $("#frmaddupdate").validate({
                onfocusout: false,
                ignore: ".ignore-valid, .ignore-show-hide",

                rules: {
                    "msd_store_logo": {
                        "required": true
                    },
                    "msd_store_name": {
                        "required": true
                    },
                    "msd_company_name": {
                        "required": true
                    },
                    "msd_contact_name": {
                        "required": true
                    },
                    "msd_contact_number": {
                        "required": true
                    },
                    "msd_country_id": {
                        "required": true
                    },
                    "msd_state_id": {
                        "required": true
                    },
                    "msd_city_id": {
                        "required": true
                    },
                    "msd_mst_categories_id": {
                        "required": true
                    },
                    "msd_have_online_store": {
                        "required": true
                    },
                    "sys_custom_field_39": {
                        "required": true,
                        "number": true,
                        "max": "4"
                    },
                    "sys_custom_field_40": {
                        "required": true,
                        "number": true,
                        "max": "4"
                    },
                    "sys_custom_field_41": {
                        "required": true,
                        "number": true,
                        "max": "4"
                    },
                    "sys_custom_field_42": {
                        "required": true,
                        "number": true,
                        "max": "4"
                    },
                    "sys_custom_field_43": {
                        "required": true,
                        "number": true,
                        "max": "3"
                    },
                    "sys_custom_field_44": {
                        "required": true,
                        "number": true,
                        "max": "3"
                    },
                    "sys_custom_field_45": {
                        "required": true,
                        "number": true,
                        "max": "3"
                    },
                    "msd_g_s_t_i_r_d_proof": {
                        "required": true
                    },
                    "msd_bank_account_proof": {
                        "required": true
                    },
                    "msd_other_proof": {
                        "required": true
                    },
                    "msd_accept_term[]": {
                        "required": true
                    }
                },
                messages: {
                    "msd_store_logo": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_STORE_LOGO)
                    },
                    "msd_store_name": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_STORE_NAME)
                    },
                    "msd_company_name": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_COMPANY_NAME)
                    },
                    "msd_contact_name": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_CONTACT_NAME)
                    },
                    "msd_contact_number": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_CONTACT_NUMBER)
                    },
                    "msd_country_id": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_COUNTRY)
                    },
                    "msd_state_id": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_STATE)
                    },
                    "msd_city_id": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_CITY)
                    },
                    "msd_mst_categories_id": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_PRIMARY_CATEGORY)
                    },
                    "msd_have_online_store": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_DO_YOU_SELL_PRODUCT_ONLINE_ALREADY_C63)
                    },
                    "sys_custom_field_39": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_ACCOUNT_NO),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_ACCOUNT_NO),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_ACCOUNT_NO)
                    },
                    "sys_custom_field_40": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C452),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C452),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C452)
                    },
                    "sys_custom_field_41": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C453),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C453),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C453)
                    },
                    "sys_custom_field_42": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C454),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C454),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_BANK_AC_C454)
                    },
                    "sys_custom_field_43": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C451),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C451),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C451)
                    },
                    "sys_custom_field_44": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C452),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C452),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C452)
                    },
                    "sys_custom_field_45": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C453),
                        "number": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_NUMBER_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C453),
                        "max": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MAXIMUM_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_G_S_T_NO_C453)
                    },
                    "msd_g_s_t_i_r_d_proof": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_UPLOAD_GST_C47IRD_DETAIL_PROOF)
                    },
                    "msd_bank_account_proof": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_UPLOAD_BANK_ACCOUNT_PROOF)
                    },
                    "msd_other_proof": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_UPLOAD_ANY_OTHER_PROOF)
                    },
                    "msd_accept_term[]": {
                        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.STOREMANAGEMENT_ACCEPT_TERM)
                    }
                },
                errorPlacement: function(error, element) {
                    switch (element.attr("name")) {

                    case 'msd_store_logo':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_store_name':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_company_name':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_contact_name':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_contact_number':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_country_id':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_state_id':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_city_id':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_mst_categories_id':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_have_online_store':
                        $('#msd_have_online_storeErr').html(error);
                        break;
                    case 'sys_custom_field_39':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_40':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_41':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_42':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_43':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_44':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'sys_custom_field_45':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_g_s_t_i_r_d_proof':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_bank_account_proof':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_other_proof':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'msd_accept_term[]':
                        $('#msd_accept_termErr').html(error);
                        break;
                    default:
                        printErrorMessage(element, valid_more_elements, error);
                        break;
                    }
                },
                invalidHandler: function(form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                },
                submitHandler: function(form) {
                    getAdminFormValidate();
                    return false;
                }
            });

        }
    },
    callEvents: function() {
        this.validate();
        this.initEvents("sellerregistration");
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        switch (elem) {

        case "sellerregistration":


            $('#upload_drop_zone_msd_store_logo').width($('#uploadify_msd_store_logo').width() + 18);
            $('#uploadify_msd_store_logo').fileupload({
                url: el_form_settings.upload_form_file_url,
                name: 'msd_store_logo',
                temp: 'temp_msd_store_logo',
                paramName: 'Filedata',
                maxFileSize: '2048',
                acceptFileTypes: 'gif|png|jpg|jpeg',
                dropZone: $('#upload_drop_zone_msd_store_logo, #upload_drop_zone_msd_store_logo + .upload-src-zone'),
                formData: {
                    'unique_name': 'msd_store_logo',
                    'id': $('#id').val(),
                    'type': 'uploadify'
                },
                add: function(e, data) {
                    var upload_errors = [];
                    var _input_name = $(this).fileupload('option', 'name');
                    var _temp_name = $(this).fileupload('option', 'temp');
                    var _form_data = $(this).fileupload('option', 'formData');
                    var _file_size = $(this).fileupload('option', 'maxFileSize');
                    var _file_type = $(this).fileupload('option', 'acceptFileTypes');

                    var _input_val = data.originalFiles[0]['name'];
                    var _input_size = data.originalFiles[0]['size'];
                    if (_file_type != '*') {
                        var _input_ext = (_input_val) ? _input_val.substr(_input_val.lastIndexOf('.')) : '';
                        var accept_file_types = new RegExp('(\.|\/)(' + _file_type + ')$', 'i');
                        if (_input_ext && !accept_file_types.test(_input_ext)) {
                            upload_errors.push(js_lang_label.ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE);
                        }
                    }
                    _file_size = _file_size * 1000;
                    if (_input_size && _input_size > _file_size) {
                        upload_errors.push(js_lang_label.ACTION_FILE_SIZE_IS_TOO_LARGE);
                    }
                    if (upload_errors.length > 0) {
                        Project.setMessage(upload_errors.join('\n'), 0);
                    } else {
                        $('#practive_' + _input_name).css('width', '0%');
                        $('#progress_' + _input_name).show();
                        _form_data['oldFile'] = $('#' + _temp_name).val();
                        $(this).fileupload('option', 'formData', _form_data);
                        $('#preview_' + _input_name).html(data.originalFiles[0]['name']);
                        data.submit();
                    }
                },
                done: function(e, data) {
                    if (data && data.result) {
                        var _input_name = $(this).fileupload('option', 'name');
                        var _temp_name = $(this).fileupload('option', 'temp');
                        var jparse_data = $.parseJSON(data.result);
                        if (jparse_data.success == '0') {
                            Project.setMessage(jparse_data.message, 0);
                        } else {
                            $('#' + _input_name).val(jparse_data.uploadfile);
                            $('#' + _temp_name).val(jparse_data.oldfile);
                            displayAdminOntheFlyImage(_input_name, jparse_data);
                            setTimeout(function() {
                                $('#progress_' + _input_name).hide();
                            }, 1000);
                        }
                    }
                },
                fail: function(e, data) {
                    $.each(data.messages, function(index, error) {
                        Project.setMessage(error, 0);
                    });
                },
                progressall: function(e, data) {
                    var _input_name = $(this).fileupload('option', 'name');
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#practive_' + _input_name).css('width', progress + '%');
                }
            });

            $('#msd_store_description').elastic();
            break;
        case "businessaccountdetails":

            $('#msd_contact_number').mask(getAdminJSFormat('phone'));

            $(document).on('change', '#msd_country_id', function() {
                adminAjaxChangeEventData(this, 'msd_state_id', el_form_settings.parent_source_options_url, 'msd_state_id', $('#mode').val(), $(this).val(), $('#id').val())
            });

            $(document).on('change', '#msd_state_id', function() {
                adminAjaxChangeEventData(this, 'msd_city_id', el_form_settings.parent_source_options_url, 'msd_city_id', $('#mode').val(), $(this).val(), $('#id').val())
            });
            break;
        case "bankdetails":


            $('#upload_drop_zone_msd_g_s_t_i_r_d_proof').width($('#uploadify_msd_g_s_t_i_r_d_proof').width() + 18);
            $('#uploadify_msd_g_s_t_i_r_d_proof').fileupload({
                url: el_form_settings.upload_form_file_url,
                name: 'msd_g_s_t_i_r_d_proof',
                temp: 'temp_msd_g_s_t_i_r_d_proof',
                paramName: 'Filedata',
                maxFileSize: '2048',
                acceptFileTypes: 'gif|png|jpg|jpeg',
                dropZone: $('#upload_drop_zone_msd_g_s_t_i_r_d_proof, #upload_drop_zone_msd_g_s_t_i_r_d_proof + .upload-src-zone'),
                formData: {
                    'unique_name': 'msd_g_s_t_i_r_d_proof',
                    'id': $('#id').val(),
                    'type': 'uploadify'
                },
                add: function(e, data) {
                    var upload_errors = [];
                    var _input_name = $(this).fileupload('option', 'name');
                    var _temp_name = $(this).fileupload('option', 'temp');
                    var _form_data = $(this).fileupload('option', 'formData');
                    var _file_size = $(this).fileupload('option', 'maxFileSize');
                    var _file_type = $(this).fileupload('option', 'acceptFileTypes');

                    var _input_val = data.originalFiles[0]['name'];
                    var _input_size = data.originalFiles[0]['size'];
                    if (_file_type != '*') {
                        var _input_ext = (_input_val) ? _input_val.substr(_input_val.lastIndexOf('.')) : '';
                        var accept_file_types = new RegExp('(\.|\/)(' + _file_type + ')$', 'i');
                        if (_input_ext && !accept_file_types.test(_input_ext)) {
                            upload_errors.push(js_lang_label.ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE);
                        }
                    }
                    _file_size = _file_size * 1000;
                    if (_input_size && _input_size > _file_size) {
                        upload_errors.push(js_lang_label.ACTION_FILE_SIZE_IS_TOO_LARGE);
                    }
                    if (upload_errors.length > 0) {
                        Project.setMessage(upload_errors.join('\n'), 0);
                    } else {
                        $('#practive_' + _input_name).css('width', '0%');
                        $('#progress_' + _input_name).show();
                        _form_data['oldFile'] = $('#' + _temp_name).val();
                        $(this).fileupload('option', 'formData', _form_data);
                        $('#preview_' + _input_name).html(data.originalFiles[0]['name']);
                        data.submit();
                    }
                },
                done: function(e, data) {
                    if (data && data.result) {
                        var _input_name = $(this).fileupload('option', 'name');
                        var _temp_name = $(this).fileupload('option', 'temp');
                        var jparse_data = $.parseJSON(data.result);
                        if (jparse_data.success == '0') {
                            Project.setMessage(jparse_data.message, 0);
                        } else {
                            $('#' + _input_name).val(jparse_data.uploadfile);
                            $('#' + _temp_name).val(jparse_data.oldfile);
                            displayAdminOntheFlyImage(_input_name, jparse_data);
                            setTimeout(function() {
                                $('#progress_' + _input_name).hide();
                            }, 1000);
                        }
                    }
                },
                fail: function(e, data) {
                    $.each(data.messages, function(index, error) {
                        Project.setMessage(error, 0);
                    });
                },
                progressall: function(e, data) {
                    var _input_name = $(this).fileupload('option', 'name');
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#practive_' + _input_name).css('width', progress + '%');
                }
            });


            $('#upload_drop_zone_msd_bank_account_proof').width($('#uploadify_msd_bank_account_proof').width() + 18);
            $('#uploadify_msd_bank_account_proof').fileupload({
                url: el_form_settings.upload_form_file_url,
                name: 'msd_bank_account_proof',
                temp: 'temp_msd_bank_account_proof',
                paramName: 'Filedata',
                maxFileSize: '2048',
                acceptFileTypes: 'gif|png|jpg|jpeg',
                dropZone: $('#upload_drop_zone_msd_bank_account_proof, #upload_drop_zone_msd_bank_account_proof + .upload-src-zone'),
                formData: {
                    'unique_name': 'msd_bank_account_proof',
                    'id': $('#id').val(),
                    'type': 'uploadify'
                },
                add: function(e, data) {
                    var upload_errors = [];
                    var _input_name = $(this).fileupload('option', 'name');
                    var _temp_name = $(this).fileupload('option', 'temp');
                    var _form_data = $(this).fileupload('option', 'formData');
                    var _file_size = $(this).fileupload('option', 'maxFileSize');
                    var _file_type = $(this).fileupload('option', 'acceptFileTypes');

                    var _input_val = data.originalFiles[0]['name'];
                    var _input_size = data.originalFiles[0]['size'];
                    if (_file_type != '*') {
                        var _input_ext = (_input_val) ? _input_val.substr(_input_val.lastIndexOf('.')) : '';
                        var accept_file_types = new RegExp('(\.|\/)(' + _file_type + ')$', 'i');
                        if (_input_ext && !accept_file_types.test(_input_ext)) {
                            upload_errors.push(js_lang_label.ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE);
                        }
                    }
                    _file_size = _file_size * 1000;
                    if (_input_size && _input_size > _file_size) {
                        upload_errors.push(js_lang_label.ACTION_FILE_SIZE_IS_TOO_LARGE);
                    }
                    if (upload_errors.length > 0) {
                        Project.setMessage(upload_errors.join('\n'), 0);
                    } else {
                        $('#practive_' + _input_name).css('width', '0%');
                        $('#progress_' + _input_name).show();
                        _form_data['oldFile'] = $('#' + _temp_name).val();
                        $(this).fileupload('option', 'formData', _form_data);
                        $('#preview_' + _input_name).html(data.originalFiles[0]['name']);
                        data.submit();
                    }
                },
                done: function(e, data) {
                    if (data && data.result) {
                        var _input_name = $(this).fileupload('option', 'name');
                        var _temp_name = $(this).fileupload('option', 'temp');
                        var jparse_data = $.parseJSON(data.result);
                        if (jparse_data.success == '0') {
                            Project.setMessage(jparse_data.message, 0);
                        } else {
                            $('#' + _input_name).val(jparse_data.uploadfile);
                            $('#' + _temp_name).val(jparse_data.oldfile);
                            displayAdminOntheFlyImage(_input_name, jparse_data);
                            setTimeout(function() {
                                $('#progress_' + _input_name).hide();
                            }, 1000);
                        }
                    }
                },
                fail: function(e, data) {
                    $.each(data.messages, function(index, error) {
                        Project.setMessage(error, 0);
                    });
                },
                progressall: function(e, data) {
                    var _input_name = $(this).fileupload('option', 'name');
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#practive_' + _input_name).css('width', progress + '%');
                }
            });


            $('#upload_drop_zone_msd_other_proof').width($('#uploadify_msd_other_proof').width() + 18);
            $('#uploadify_msd_other_proof').fileupload({
                url: el_form_settings.upload_form_file_url,
                name: 'msd_other_proof',
                temp: 'temp_msd_other_proof',
                paramName: 'Filedata',
                maxFileSize: '2048',
                acceptFileTypes: 'gif|png|jpg|jpeg',
                dropZone: $('#upload_drop_zone_msd_other_proof, #upload_drop_zone_msd_other_proof + .upload-src-zone'),
                formData: {
                    'unique_name': 'msd_other_proof',
                    'id': $('#id').val(),
                    'type': 'uploadify'
                },
                add: function(e, data) {
                    var upload_errors = [];
                    var _input_name = $(this).fileupload('option', 'name');
                    var _temp_name = $(this).fileupload('option', 'temp');
                    var _form_data = $(this).fileupload('option', 'formData');
                    var _file_size = $(this).fileupload('option', 'maxFileSize');
                    var _file_type = $(this).fileupload('option', 'acceptFileTypes');

                    var _input_val = data.originalFiles[0]['name'];
                    var _input_size = data.originalFiles[0]['size'];
                    if (_file_type != '*') {
                        var _input_ext = (_input_val) ? _input_val.substr(_input_val.lastIndexOf('.')) : '';
                        var accept_file_types = new RegExp('(\.|\/)(' + _file_type + ')$', 'i');
                        if (_input_ext && !accept_file_types.test(_input_ext)) {
                            upload_errors.push(js_lang_label.ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE);
                        }
                    }
                    _file_size = _file_size * 1000;
                    if (_input_size && _input_size > _file_size) {
                        upload_errors.push(js_lang_label.ACTION_FILE_SIZE_IS_TOO_LARGE);
                    }
                    if (upload_errors.length > 0) {
                        Project.setMessage(upload_errors.join('\n'), 0);
                    } else {
                        $('#practive_' + _input_name).css('width', '0%');
                        $('#progress_' + _input_name).show();
                        _form_data['oldFile'] = $('#' + _temp_name).val();
                        $(this).fileupload('option', 'formData', _form_data);
                        $('#preview_' + _input_name).html(data.originalFiles[0]['name']);
                        data.submit();
                    }
                },
                done: function(e, data) {
                    if (data && data.result) {
                        var _input_name = $(this).fileupload('option', 'name');
                        var _temp_name = $(this).fileupload('option', 'temp');
                        var jparse_data = $.parseJSON(data.result);
                        if (jparse_data.success == '0') {
                            Project.setMessage(jparse_data.message, 0);
                        } else {
                            $('#' + _input_name).val(jparse_data.uploadfile);
                            $('#' + _temp_name).val(jparse_data.oldfile);
                            displayAdminOntheFlyImage(_input_name, jparse_data);
                            setTimeout(function() {
                                $('#progress_' + _input_name).hide();
                            }, 1000);
                        }
                    }
                },
                fail: function(e, data) {
                    $.each(data.messages, function(index, error) {
                        Project.setMessage(error, 0);
                    });
                },
                progressall: function(e, data) {
                    var _input_name = $(this).fileupload('option', 'name');
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#practive_' + _input_name).css('width', progress + '%');
                }
            });

            break;
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {
        pre_cond_code_arr.push(cc_json_1);
    }
}
Project.modules.storemanagement.init();