/** registerduser module script */
Project.modules.registerduser = {
    init: function() {
        valid_more_elements = [];
        $.validator.addMethod("alpha_with_spaces", function(value, element) {
            return this.optional(element) || /^[a-zA-Z ]+$/.test(value);
        }, ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ONLY_ENTER_LETTERS_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.REGISTERDUSER_LAST_NAME));
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "mc_first_name": {
                    "required": true,
                    "alpha_with_spaces": true
                },
                "mc_last_name": {
                    "required": true,
                    "alpha_with_spaces": true
                },
                "mc_email": {
                    "required": true,
                    "email": true
                },
                "mc_password": {
                    "required": true,
                    "minlength": "6"
                }
            },
            messages: {
                "mc_first_name": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.REGISTERDUSER_FIRST_NAME),
                    "alpha_with_spaces": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ONLY_ENTER_LETTERS_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.REGISTERDUSER_FIRST_NAME)
                },
                "mc_last_name": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.REGISTERDUSER_LAST_NAME),
                    "alpha_with_spaces": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ONLY_ENTER_LETTERS_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.REGISTERDUSER_LAST_NAME)
                },
                "mc_email": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.REGISTERDUSER_EMAIL),
                    "email": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_EMAIL_ADDRESS_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.REGISTERDUSER_EMAIL)
                },
                "mc_password": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.REGISTERDUSER_PASSWORD),
                    "minlength": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MINIMUM_LENGTH_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.REGISTERDUSER_PASSWORD)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'mc_first_name':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mc_last_name':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mc_email':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mc_password':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {},
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.registerduser.init();