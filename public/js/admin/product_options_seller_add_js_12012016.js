/** product_options_seller module script */
Project.modules.product_options_seller = {
    init: function() {
        $(document).off("change", "#tpo_category_option_id");
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {

        $(document).on('change', '#tpo_category_option_id', function() {
            adminAjaxChangeEventData(this, 'tpo_option_value', el_form_settings.parent_source_options_url, 'tpo_option_value', $('#mode').val(), $(this).val(), $('#id').val())
        });
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.product_options_seller.init();