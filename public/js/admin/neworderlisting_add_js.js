/** neworderlisting module script */
Project.modules.neworderlisting = {
    init: function () {
        valid_more_elements = [];
    },
    validate: function () {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",
            rules: {
                "mso_mst_shipper_id": {
                    "required": true
                },
                "mso_tracking_number": {
                    "required": true
                },
                "mso_shipping_date": {
                    "required": true
                },
                "mso_seller_invoice_no": {
                    "required": true
                },
                "mso_seller_invoice_date": {
                    "required": true
                },
                "mso_shipping_remark": {
                    "required": false
                }
            },
            messages: {
                "mso_mst_shipper_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWORDERLISTING_COURIER_COMPANY)
                },
                "mso_tracking_number": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWORDERLISTING_TRACKING_NUMBER)
                },
                "mso_shipping_date": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWORDERLISTING_SHIPPED_DATE)
                },
                "mso_seller_invoice_no": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWORDERLISTING_INVOICE_NO_C46)
                },
                "mso_seller_invoice_date": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWORDERLISTING_INVOICE_DATE)
                },
                "mso_shipping_remark": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.NEWORDERLISTING_REMARKS)
                }
            },
            errorPlacement: function (error, element) {
                switch (element.attr("name")) {

                    case 'mso_mst_shipper_id':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'mso_tracking_number':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'mso_shipping_date':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'mso_seller_invoice_no':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'mso_seller_invoice_date':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    case 'mso_shipping_remark':
                        $('#' + element.attr('id') + 'Err').html(error);
                        break;
                    default:
                        printErrorMessage(element, valid_more_elements, error);
                        break;
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function (form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function () {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function () {
        callGoogleMapEvents();
    },
    initEvents: function (elem) {
        $('#mso_shipping_remark').elastic();
        // alert($("#order_added_date").val());
        $('#mso_shipping_date').datepicker({
            dateFormat: getAdminJSFormat('date'),
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            // minDate: "11 15,2015",
            minDate: $("#order_added_date").val(),
            yearRange: 'c-100:c+100',
            beforeShow: function (input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function () {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#mso_shipping_date').attr('readonly', true);
        }


        $('#mso_seller_invoice_date').datepicker({
            dateFormat: getAdminJSFormat('date'),
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            minDate: $("#order_added_date").val(),
            // minDate: "2015-11-11",
            beforeShow: function (input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function () {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#mso_seller_invoice_date').attr('readonly', true);
        }
    },
    childEvents: function (elem, eleObj) {
    },
    CCEvents: function () {
    }
}
Project.modules.neworderlisting.init();