var Project = {
    init: function () {
        this.initModules();
        if ($('.listing-error').html() != '') {
            $('.listing-error').show();
            $(".listing-error").delay(10000).slideUp('slow');
        }
        $("textarea").css('resize', 'none');
        $("checkbox").css('border', 'none');
        //this.hijaxPopups();
    },
    modules: [],
    initModules: function () {
        for (var module in Project.modules) {
            var id = (module || "").replace(/([A-Z])/g, '-$1').toLowerCase();
            id = id.substring(0, 1) == '-' ? id.substring(1) : id;
            if ($('#' + id).length && typeof (this.modules[module].init) == 'function') {
                Project.modules[module].init($('#' + id));
            }
        }
    },
    hide_adaxloading_div: function () {
        if ($('#body_ajaxloading_div').length) {
            $('#body_ajaxloading_div').remove();
        }
    },
    show_adaxloading_div: function () {
        $('body').append('<div id="body_ajaxloading_div" align="center"><i class="fa fa-cog fa-spin fa-32"></i><br>Loading...</div>');
        $('#body_ajaxloading_div').css({
            position: 'absolute',
            zIndex: 10000,
            left: (el_general_settings.page_temp_left - 42) + 'px',
            top: (el_general_settings.page_temp_right - 18) + 'px'
        });
        setTimeout(function () {
            Project.hide_adaxloading_div()
        }, 20000);
    },
    setMessage: function (msgText, msgClass, timeOut) {
        
        var timer = 13000, cnt_class, close_class;
        //  var timer = 500, cnt_class, close_class;
        //  Client wants delay for flash data of 12 sec
        if (msgClass == 0) {
            cnt_class = "alert-error";
            close_class = 'error';
        } else if (msgClass == 1) {
            cnt_class = "alert-success";
            close_class = 'success';
        } else if (msgClass == 2) {
            cnt_class = "";
            close_class = 'success';
        } else {
            cnt_class = "alert-info";
            close_class = 'success';
        }
        $("#closebtn_errorbox").removeClass("success").removeClass("error").addClass(close_class);
        $('#err_msg_cnt').html(msgText).removeClass("alert-success").removeClass("alert-error").addClass(cnt_class);
        if ($.isNumeric(timeOut)) {
            setTimeout(function () {
                $('#var_msg_cnt').fadeIn('slow');
                setTimeout("closeMessage()", 13000);
            }, timeOut);
        } else {
            $('#var_msg_cnt').fadeIn('slow');
            setTimeout("closeMessage()", timer);
        }
    },
    closeMessage: function () {
        $('#var_msg_cnt').fadeOut('slow');
        return false;
    },
    checkmsg: function () {
        if ($('#err_msg_cnt').length > 0 && $.trim($('#err_msg_cnt').text()) != '') {
            $('#var_msg_cnt').fadeIn('slow');
            setTimeout(function () {
                Project.closeMessage();
            }, 13000);
        }
    }
};