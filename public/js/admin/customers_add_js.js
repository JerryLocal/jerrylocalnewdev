/** customers module script */
Project.modules.customers = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "mc_first_name": {
                    "required": true
                },
                "mc_last_name": {
                    "required": true
                },
                "mc_email": {
                    "required": true,
                    "email": true
                },
                "mc_password": {
                    "required": true,
                    "minlength": "6"
                },
                "mc_status": {
                    "required": true
                }
            },
            messages: {
                "mc_first_name": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMERS_FIRST_NAME)
                },
                "mc_last_name": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMERS_LAST_NAME)
                },
                "mc_email": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMERS_EMAIL),
                    "email": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_EMAIL_ADDRESS_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMERS_EMAIL)
                },
                "mc_password": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMERS_PASSWORD),
                    "minlength": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_MINIMUM_LENGTH_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMERS_PASSWORD)
                },
                "mc_status": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.CUSTOMERS_STATUS)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'mc_first_name':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mc_last_name':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mc_email':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mc_password':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'mc_status':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {

        $('#mc_d_o_b').datepicker({
            dateFormat: 'yy-mm-dd',
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#mc_d_o_b').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.customers.init();