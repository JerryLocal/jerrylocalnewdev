/** settelment module script */
Project.modules.settelment = {
    init: function() {
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "ts_transection_code": {
                    "required": true
                },
                "ts_settlement_total": {
                    "required": true
                },
                "ts_date": {
                    "required": true
                }
            },
            messages: {
                "ts_transection_code": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SETTELMENT_ENTER_PAYMENT_REFERENCE_NO)
                },
                "ts_settlement_total": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SETTELMENT_ENTER_TOTAL_PAYMENT_AMOUNT)
                },
                "ts_date": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.SETTELMENT_SETTELMENT_DATE)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'ts_transection_code':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'ts_settlement_total':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'ts_date':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {
        $('#ts_transection_detail').elastic();

        $('#ts_date').datepicker({
            dateFormat: getAdminJSFormat('date'),
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if (el_general_settings.mobile_platform) {
            $('#ts_date').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.settelment.init();