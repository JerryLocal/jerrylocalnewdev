/** homecategory module script */
Project.modules.homecategory = {
    init: function() {
        $(document).off("change", "#thc_mst_categories_id");
        valid_more_elements = [];
    },
    validate: function() {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore: ".ignore-valid, .ignore-show-hide",

            rules: {
                "thc_mst_categories_id": {
                    "required": true
                },
                "thc_subcategory[]": {
                    "required": true
                }
            },
            messages: {
                "thc_mst_categories_id": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.HOMECATEGORY_CATEGORY)
                },
                "thc_subcategory[]": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46, "#FIELD#", js_lang_label.HOMECATEGORY_SUBCATEGORY)
                }
            },
            errorPlacement: function(error, element) {
                switch (element.attr("name")) {

                case 'thc_mst_categories_id':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                case 'thc_subcategory[]':
                    $('#' + element.attr('id') + 'Err').html(error);
                    break;
                default:
                    printErrorMessage(element, valid_more_elements, error);
                    break;
                }
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function(form) {
                getAdminFormValidate();
                return false;
            }
        });
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
    },
    callChilds: function() {
        callGoogleMapEvents();
    },
    initEvents: function(elem) {

        $('#upload_drop_zone_thc_img').width($('#uploadify_thc_img').width() + 18);
        $('#uploadify_thc_img').fileupload({
            url: el_form_settings.upload_form_file_url,
            name: 'thc_img',
            temp: 'temp_thc_img',
            paramName: 'Filedata',
            maxFileSize: '2048',
            acceptFileTypes: 'gif|png|jpg|jpeg',
            dropZone: $('#upload_drop_zone_thc_img, #upload_drop_zone_thc_img + .upload-src-zone'),
            formData: {
                'unique_name': 'thc_img',
                'id': $('#id').val(),
                'type': 'uploadify'
            },
            add: function(e, data) {
                var upload_errors = [];
                var _input_name = $(this).fileupload('option', 'name');
                var _temp_name = $(this).fileupload('option', 'temp');
                var _form_data = $(this).fileupload('option', 'formData');
                var _file_size = $(this).fileupload('option', 'maxFileSize');
                var _file_type = $(this).fileupload('option', 'acceptFileTypes');

                var _input_val = data.originalFiles[0]['name'];
                var _input_size = data.originalFiles[0]['size'];
                if (_file_type != '*') {
                    var _input_ext = (_input_val) ? _input_val.substr(_input_val.lastIndexOf('.')) : '';
                    var accept_file_types = new RegExp('(\.|\/)(' + _file_type + ')$', 'i');
                    if (_input_ext && !accept_file_types.test(_input_ext)) {
                        upload_errors.push(js_lang_label.ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE);
                    }
                }
                _file_size = _file_size * 1000;
                if (_input_size && _input_size > _file_size) {
                    upload_errors.push(js_lang_label.ACTION_FILE_SIZE_IS_TOO_LARGE);
                }
                if (upload_errors.length > 0) {
                    Project.setMessage(upload_errors.join('\n'), 0);
                } else {
                    $('#practive_' + _input_name).css('width', '0%');
                    $('#progress_' + _input_name).show();
                    _form_data['oldFile'] = $('#' + _temp_name).val();
                    $(this).fileupload('option', 'formData', _form_data);
                    $('#preview_' + _input_name).html(data.originalFiles[0]['name']);
                    data.submit();
                }
            },
            done: function(e, data) {
                if (data && data.result) {
                    var _input_name = $(this).fileupload('option', 'name');
                    var _temp_name = $(this).fileupload('option', 'temp');
                    var jparse_data = $.parseJSON(data.result);
                    if (jparse_data.success == '0') {
                        Project.setMessage(jparse_data.message, 0);
                    } else {
                        $('#' + _input_name).val(jparse_data.uploadfile);
                        $('#' + _temp_name).val(jparse_data.oldfile);
                        displayAdminOntheFlyImage(_input_name, jparse_data);
                        setTimeout(function() {
                            $('#progress_' + _input_name).hide();
                        }, 1000);
                    }
                }
            },
            fail: function(e, data) {
                $.each(data.messages, function(index, error) {
                    Project.setMessage(error, 0);
                });
            },
            progressall: function(e, data) {
                var _input_name = $(this).fileupload('option', 'name');
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#practive_' + _input_name).css('width', progress + '%');
            }
        });


        $(document).on('change', '#thc_mst_categories_id', function() {
            adminAjaxChangeEventData(this, 'thc_subcategory', el_form_settings.parent_source_options_url, 'thc_subcategory', $('#mode').val(), $(this).val(), $('#id').val())
        });
    },
    childEvents: function(elem, eleObj) {},
    CCEvents: function() {}
}
Project.modules.homecategory.init();