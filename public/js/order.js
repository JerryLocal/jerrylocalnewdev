Project.modules.order = {
    init: function() {
    }
};

$(document).ready(function(){
    $('a.order_id').click(function(){
      var order_id = $(this).attr('data-id');
      $("#ord_id").val(order_id);
      $('#order_list_form').submit();
    });

    $('a.product_id').click(function(){
      var product_id = $(this).attr('data-id');
      $("#pId").val(product_id);
      $('#review_list_form').submit();
    });

    $('a.tOrder_id').click(function(){
      var order_id = $(this).attr('data-id');
      $("#oId").val(order_id);
      $('#trackorder_list_form').submit();
    });

    $('a.return_comman_btn').click(function(){
      var order_id = $(this).attr('data-id');
      var url = $(this).attr('data-rel');
      $('#return_order_form').attr('action',url);
      $('.order_id').val(order_id);
      $('#return_order_form').submit();

      /*$.ajax({
             type: "POST",
             url: site_url + "order/order/checkReturnOrderReq",
             data:{'order_id':order_id},
             success: function(result) {
                if(result == "true"){
                  $('#return_order_form').submit();
                }else{
                  Project.setMessage("You have already sent request to return order.","alert-info");
                }     
             }
      });*/
    });
    
    $('a.btn_invoice').click(function(){
      var id = $(this).attr('data-id');
        $.ajax({
               type: "POST",
               url: site_url + "order/order/Invoice",
               data:{'id':id},
               success: function(result) {
                   window.open(result);
                /*$('body').html(result);
                var output = $('body').html();
                $('body').html(output);
                window.print();
                window.location.reload();
                location.reload();*/
                // below code for the print invoice will open in new window
                /*var myWindow=window.open('','','width=500,height=500');
                    myWindow.document.write(result);                    
                    myWindow.document.close();
                    myWindow.focus();
                    myWindow.print();
                    myWindow.close();*/
               }
           });

    });

    $('a.btn_email').click(function(){
      var id = $(this).attr('data-id');
        $.ajax({
               type: "POST",
               url: site_url + "order/order/Invoice",
               data:{'id':id ,'section':'sendEmail'},
               success: function(result) {
                if(result == '1'){
                  // alert('Mail Send Successfully');
                  Project.setMessage("Mail Send Successfully", "alert-success");
                }else
                {
                  // alert('Error in Sending Mail , Please Try After Some time');
                  Project.setMessage("Error in Sending Mail , Please Try After Some time", "alert-failure");

                }
                location.reload();
               }
           });

    });

    $('#cancel_btn').click(function(){
      if($('#cancel_order_form').valid()) {
        test_confirm('Are you Sure you want to cancel this order ??', function(){
          $('#cancel_order_form').submit();
          return true;
        },function(){
          $('#test_confirm .btn_cancel').parent().removeClass('set_prompt_btn');
          $('.close').click();
          return true;
        } );
      }
    });

    $('#cancel_order_form').validate({
      rules:{
        'cancel_detail': {
          required : true
        }
      },
      messages:{
        'cancel_detail': {
          required:'Please enter cancel detail.'
        }
      },
      errorPlacement: function (error, element) {
        // default error scheme
        error.insertAfter(element);
      },
      submitHandler: function (form) {
        var form_id = $(form).attr("id");
        document.getElementById(form_id).submit();
      }
    });
});