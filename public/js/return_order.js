Project.modules.returnOrder = {
    init: function() {}
};
$('#return_btn').click(function () {
            if ($('#return_order_form').valid()) {
                $('#return_order_form').submit();
            }
        });
        $('#return_order_form').validate({
            rules:{
                'reason': {
                    required : true
                },
                'preference': {
                    required:true
                },
                'comment': {
                    required:true
                }
            },
            messages:{
                'reason':{
                    required:'Please Select Reason'
                },
                'preference':{
                    required:'Please Select Preference'
                },
                'comment':{
                    required:'Please enter a Comment'
                }
            }, 
            errorPlacement: function (error, element) {
                 // default error scheme
                    error.insertAfter(element);
                
            }, submitHandler: function (form) {
                var form_id = $(form).attr("id");
                document.getElementById(form_id).submit();

            }
        });

// $(function() {
//     $('#upload_btn').uploadify({
//         'swf'   : site_url+'public/js/libraries/uploadify.swf',
//         'uploader'  : site_url+'upload/uploadify',
//     });
// });

function previewFile() {
    // dynamic_previewFile('preview-img', 'return_image', 'preview-img-noimage', 'close-btn');
    dynamic_previewFile('preview-img', 'return_image', true, true);
}

function removeSelectedImage() {
    // dynamic_removeFile('preview-img', 'return_image', 'preview-img-noimage', 'close-btn');
    dynamic_removeFile('preview-img', 'return_image');
}