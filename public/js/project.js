var Project = {
    init: function() {
        this.initModules();
        if ($('.listing-error').html() != '') {
            $('.listing-error').show();
            $(".listing-error").delay(10000).slideUp('slow');
        }
        $("textarea").css('resize', 'none');
        $("checkbox").css('border', 'none');
        //this.hijaxPopups();
    },
    modules: [],
    initModules: function() {
        for (var module in Project.modules) {
            var id = module.replace(/([A-Z])/g, '-$1').toLowerCase();
            id = id.substring(0, 1) == '-' ? id.substring(1) : id;
            if ($('#' + id).length && typeof (this.modules[module].init) == 'function') {
                Project.modules[module].init($('#' + id));
            }
        }
        if(Project.modules.processAjax && Project.modules.processAjax.init && typeof Project.modules.processAjax.init == "function"){
            Project.modules.processAjax.init();
        }
    },
    closeMessage: function() {
        $('#var_msg_cnt').fadeOut('slow');
    },
    setMessage: function(msgText, msgClass) {
       
        msgClass = (msgClass != "") ? msgClass : "alert-success";
        $('#err_msg_cnt').html(msgText).attr("class", "content-errorbox alert " + msgClass);
        $('#var_msg_cnt').fadeIn('slow');
        setTimeout(function() {
            Project.closeMessage();
        }, 10000);
    },
    checkmsg: function() {
        if ($('#err_msg_cnt').length > 0 && $.trim($('#err_msg_cnt').text()) != '') {
            $('#var_msg_cnt').fadeIn('slow');
            setTimeout(function() {
                Project.closeMessage();
            }, 10000);
        }
    }
};