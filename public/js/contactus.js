Project.modules.contactus = {
    init: function() {               


        $('#contactusSubmit').click(function () {

            if ($('#frmContactus').valid()) {
                var Email = $.trim($("#Email").val());
                if(Email != null && Email.length > 1){
                    $('#frmContactus').submit();    
                }
            }
        });

        $('#frmContactus').validate({
            rules:{
                'fullname': {
                    required : true
                },
                'EmailSubject': {                    
                    required:{
                        depends: function(element) {
                            return $("#EmailSubject").val() == '';
                        }
                    }
                },
                'Email': {
                    required : true,
                    email:true
                },
                'Comment': {
                    required:true,
                    minlength: 10
                },
                'Captcha':{
                    required:true,
                    maxlength: 6,
                    remote: {
                        url: site_url + "contactus/contactus/check_captcha",
                        type: "POST"
                    }
                }
            },
            messages:{
                'fullname' :{
                    required:'Please enter a Fullname'
                },
                'EmailSubject' :{
                    required:'Please select subject for email'
                },
                'Email':{
                    required:'Please enter a email',
                    email:'Please enter valid email address'
                },
                'Comment':{
                    required:'Please enter a Comment'
                },
                'Captcha':{
                    required:'Please enter Captcha Code',
                    remote:'Please Enter Correct Captcha'                
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.attr("name") == "Captcha") {
                    error.appendTo("#CaptchaErr");
                } else { // default error scheme
                    error.insertAfter(element);
                }
            }, submitHandler: function (form) {
                var form_id = $(form).attr("id");
                document.getElementById(form_id).submit();

            }
        }); 
    }
};

$("#resetcaptcha").click(function(){        
       $("#Captcha").val('');
       $('#capchareset').css('opacity', '0.5');
       $('#capchareset').css('filter', '50');
       $.ajax({
           type: "POST",
           url: site_url + "contactus/contactus/resetCaptcha",
           success: function(result) {            
               if (result != '') {
                   $("#capchareset").attr('src', result)
                   $('#capchareset').css('opacity', '1');
                   $('#capchareset').css('filter', '100');
               }
           }
       });
});