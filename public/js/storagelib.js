//****************************************
// Check Local Storage Avaliable in Browser or not
//****************************************
function checkBrowserSupport(){
	if(window.localStorage!==undefined){
	    return true;
	}else{
	   	return false;
	}
}
//****************************************
// Check Local Storage Avaliable or not
//****************************************
function checkLocalStorage(StorageName){

 	if (localStorage.getItem(StorageName) !== null) {
 		return true;
 	}else{
 		return false;
 	}
}
//****************************************
// Check Local Storage Value avaliable or not
//****************************************
function InsertToStorage(StorageName,StorageValue,Overwrite,StorageKey,MaxStorageLimit){
	Overwrite = typeof Overwrite !== 'undefined' ? Overwrite : true;
	if(Overwrite){
		MaxStorageLimit = typeof MaxStorageLimit !== 'undefined' ? MaxStorageLimit : 10;
	}

 	if (checkLocalStorage(StorageName)) {
 		// alert("A");
 		// alert(); 		
 		var obj = jQuery.parseJSON(localStorage.getItem(StorageName));
 		// JSON.stringify(obj);
 		// var pids = [StorageValue];

 		var isarr = $(obj).toArray();
 		var alreadyStored = false;
 		if($(obj).length < MaxStorageLimit){
	 		for (var i = 0; i < $(obj).length; i++) {
	 			if($(obj)[i] == StorageValue){
	 				alreadyStored = true;
	 				break;
	 			}
	 			
	 		}
	 		if(!alreadyStored){
	 			isarr.push(StorageValue);
	 			localStorage.setItem(StorageName,JSON.stringify(isarr)); 		
	 			return true;
	 		} 		
 		}else{
 			var rmeovedItem = isarr.shift();
 			isarr.push(StorageValue);
 			localStorage.setItem(StorageName,JSON.stringify(isarr)); 		
 			return true; 			
 		}
 	}else{
 		// alert("B"); 	
 		// alert(GetFromStorage(StorageName));	
 		localStorage.setItem(StorageName, JSON.stringify(StorageValue));
 		return true;
 	}
}
//****************************************
// Get Value from Storage
//****************************************
function GetFromStorage(StorageName){
 	if (checkLocalStorage(StorageName)) {
 		var StorageObj = localStorage.StorageName;
 		if(StorageObj != null){
 			var obj = jQuery.parseJSON(StorageObj);
 			return obj;
 		}else{
 			return '';
 		}
 	}
}