Project.modules.seller = {
    init: function() {    
      $('#step1-submit').click(function () {
            if ($('#seller-basic').valid()) {
                $('#seller-basic').submit();
            }
        });
      if(country != ""){
        load_state(country);
      }
      if(state != ""){        
        load_city(state);
      }
      $.validator.addMethod("alphaNumeric", function(value, element) {            
            return this.optional(element) || value == value.match(/^[a-zA-Z0-9\s]+$/);
         },'Enter only alphaNumeric characters');

      $.validator.addMethod("validatestate", function(value, element) {                    
          return this.optional(element) || (parseFloat(value) > 0);    
      }, "Please select Region");

      $.validator.addMethod("validatecity", function(value, element) {                    
          return this.optional(element) || (parseFloat(value) > 0);    
      }, "Please select City");

        $('#seller-basic').validate({
            rules:{
                'User_Email': {
                    required : true,
                    email:true
                },
                'User_Password': {
                    required:true,
                    minlength:6,
                }
            },
            messages:{
                'User_Email':{
                    required:'Please enter a email',
                    email:'Please enter valid email address'
                },
                'User_Password':{
                    required:'Please enter a password'
                }
            }, 
            errorPlacement: function (error, element) {
                 // default error scheme
                    error.insertAfter(element);
                
            }, submitHandler: function (form) {
                var form_id = $(form).attr("id");
                document.getElementById(form_id).submit();

            }
        }); 

        $.validator.addMethod("complete_url", function(val, elem) {
            
            // if no url, don't do anything
            if (val.length == 0) { return true; }

            // if user has not entered http:// https:// or ftp:// assume they mean http://
            if(!/^(https?|ftp):\/\//i.test(val)) {
                val = 'http://'+val; // set both the value
                $(elem).val(val); // also update the form element
            }
            // now check if valid url            
            return /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(val);
        },'Please Enter Valid URL');

        $('#step2-submit').click(function () {

            if ($('#seller-store').valid()) {
                $('#seller-store').submit();
            }
        });

        // $.validator.addMethod("contactnumber", function (value, element) {
        //     var isValid = $("#contact_no").intlTelInput("isValidNumber");
        //     return isValid;
        // }, "Please enter valid phone number");
        $.validator.addMethod("validphnumberwithcountry", function (value, element) {
            // if no url, don't do anything
            if (value.length == 0) { return true; }

            // if user has not entered +64
            if(value.substr(0,3)!="+64") {
                val = '+64'+value; // set both the value
                $(element).val(val); // also update the form element
            }
            // now check if valid url
            return /(\+64)[0-9]*$/.test(value);
        }, "Please enter valid mobile number");
        
        $.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space are not allowed");
        
        $('#seller-store').validate({
            rules:{
                'StoreName': {
                    required : true,
                    maxlength:100,
                    alphaNumeric: true,
                    notStartWithSpace:true,
                    noSpace: true,
                    remote: {
                        url: site_url+"sellerregistration/sellerregistration/checkStoreNameExist",
                        type: "post"
                    }
                },
                'CompanyName': {
                    required:true,
                    maxlength:100,
                    alphaNumeric: true
                },
                'ContactName': {
                    required:true
                },
                'address1': {
                    required:true,
                    maxlength:100,
                    // nospace: true
                },
                'website_url': {  
                    complete_url: true,
                    maxlength:100,
                    // nospace: true
                },
                'pincode': {  
                    digits: true,
                    maxlength:20,
                    nospace: true
                },
                'contact_no': {
                    // contactnumber: true
                    required:true,
                    validphnumberwithcountry:true
                },
                'vCountry': {
                    required: true
                },
                'vState': {
                    required: true
                },
                'vCity': {
                    required: true
                },
                'online_store_url': {
                    complete_url: function () {
                        if($("input.store_url:checked").val()=="yes") {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    maxlength:100,
                    required: function() {
                        if($("input.store_url:checked").val()=="yes") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    // nospace: function() {
                    //     if($("input.store_url:checked").val()=="yes") {
                    //         return true;
                    //     } else {
                    //         return false;
                    //     }
                    // }
                }
            },
            messages:{
                'StoreName':{
                    required:'Please enter a Store Name',
                    remote:'Store name is already Exist.' 
                },
                'CompanyName':{
                    required:'Please enter a Company Name'
                },
                'ContactName':{
                    required:'Please enter a Contact Name'
                },
                'address1':{
                    required:'Please enter a Street No / Name'
                },
                'vCountry':{
                    required:'Please enter a Country'
                },
                'vState':{
                    required:'Please enter a State'
                },
                'vCity':{
                    required:'Please enter a City'
                }

            }, 
            errorPlacement: function (error, element) {
                 // default error scheme
                    error.insertAfter(element);
                
            }, submitHandler: function (form) {
                var form_id = $(form).attr("id");
                // var valid_number = $("#contact_no").intlTelInput("getNumber");
                // $("#contact_no_hidden").val(valid_number);
                document.getElementById(form_id).submit();

            }
        }); 
        $('#step3-submit').click(function () {

            if ($('#seller-bank').valid()) {
                $('#seller-bank').submit();
            }
        });
        $('#seller-bank').validate({
            rules:{
                'Bank_name': {
                    required : true,
                },
                'bank_account_1': {
                    required:true,
                    minlength:2,
                    maxlength:2,
                    digits:true

                },
                'bank_account_2': {
                    required:true,
                    minlength:4,
                    maxlength: 4,
                    digits:true
                },
                'bank_account_3': {
                    required:true,
                    minlength:7,
                    maxlength:7,
                    digits:true
                },
                'bank_account_4': {
                    required:true,
                    minlength:3,
                    maxlength:3,
                    digits:true
                },
                /*'gst_1': {
                    required:true,
                    minlength: 3,
                    maxlength: 3,
                    digits:true

                },
                'gst_2': {
                    required:true,
                    minlength:3,
                    maxlength:3,
                    digits:true
                },
                'gst_3': {
                    required:true,
                    minlength: 3,
                    maxlength: 3,
                    digits:true
                },*/
                'accept_term':{
                    required:true
                }
            },
            messages:{
                'Bank_name':{
                    required:'Please enter a Bank Name',
                },
                'bank_account_1':{
                    required:'Please enter a Bank Account No',
                    maxlength:'Please enter 2 digits only',
                    minlength:'Please enter 2 digits only'
                },
                'bank_account_2':{
                    required:'Please enter a Bank Account No',
                    maxlength:'Please enter 4 digits only',
                    minlength:'Please enter 4 digits only'
                },
                'bank_account_3':{
                    required:'Please enter a Bank Account No',
                    maxlength:'Please enter 7 digits only',
                    minlength:'Please enter 7 digits only'
                },
                'bank_account_4':{
                    required:'Please enter a Bank Account No',
                    maxlength:'Please enter 3 digits only',
                    minlength:'Please enter 3 digits only'
                },
                /*'gst_1':{
                    required:'Please enter a GST No',
                    maxlength:'Please enter 3 digits only',
                    minlength:'Please enter 3 digits only'
                },
                'gst_2':{
                    required:'Please enter a GST No',
                    maxlength:'Please enter 3 digits only',
                    minlength:'Please enter 3 digits only'
                },
                'gst_3':{
                    required:'Please enter a GST No',
                    maxlength:'Please enter 3 digits only',
                    minlength:'Please enter 3 digits only'
                },*/
                'accept_term':{
                    required:'Please accept Terms & Condition'
                }
            }, 
            errorPlacement: function (error, element) {
                 // default error scheme
                 if (element.attr("name") == "accept_term") {
                     error.appendTo("#accept_termErr");
                 }else if (element.attr("name") == "bank_account_1") {
                     error.appendTo("#bank_account_1Err");
                 }else if (element.attr("name") == "bank_account_2") {
                     error.appendTo("#bank_account_2Err");
                 }else if (element.attr("name") == "bank_account_3") {
                     error.appendTo("#bank_account_3Err");
                 }else if (element.attr("name") == "bank_account_4") {
                     error.appendTo("#bank_account_4Err");
                 }else{
                    error.insertAfter(element);
                }
                
            }, submitHandler: function (form) {
                var form_id = $(form).attr("id");
                document.getElementById(form_id).submit();
            }
        }); 
    }
};
$(document).ready(function(){
    $('.hastip').tooltipsy();
    $('#online_store_url_row').hide();
    $('.store_url').click(function(){
        if($(this).val() != 'yes'){
            $("#online_store_url").val('');
            $('#online_store_url_row').hide();
        }else
        {
            $('#online_store_url_row').show();
        }
    });
    $("#contact_no").keyup(function(e) {
        value = $(this).val();
        if(value.substr(0,3)!="+64") {
            if(e.keyCode==8) {
                $(this).val("+64");
            } else {
                val = '+64'+value; // set both the value
                $(this).val(val); // also update the form element
            }
        }
    });

    // try {
    //     var telInput = $("#contact_no");
    //             // errorMsg = $("#vMobileErr"),
    //             // validMsg = $("#valid-msg");
    //     telInput.intlTelInput({
    //         defaultCountry: "NZ",
    //         onlyCountries: ["NZ"],
    //         autoHideDialCode: false,
    //         // geoIpLookup: function (callback) {
    //         //     $.get(site_url + 'get-ipinfo.html', function () {
    //         //     }).always(function (resptxt) {
    //         //         var countryCode = 'US';
    //         //         try {
    //         //             var resp = JSON.parse(resptxt);
    //         //             var countryCode = (resp && resp.country_code) ? resp.country_code : "";
    //         //         } catch (e) {
    //         //             console.log(e);
    //         //         }
    //         //         callback(countryCode);
    //         //     });
    //         // },
    //         utilsScript: site_url + "public/js/intlTelInput/utils.js"
    //     });
    // } catch (e) {
    //     console.log(e);
    // }

    $('.bank_box').keyup(function(){
       if(this.value.length == $(this).attr('maxlength')){
            $(this).parent().next().next().find("input").focus();
       }
    });

    $('.gst_box').keyup(function(){
       if(this.value.length == $(this).attr('maxlength')){
            $(this).parent().next().next().find("input").focus();
       }
    });
});


$("#vCountry").change(function(){
    var countryId = $(this).val();
    $.ajax({
        type: "POST",
        url: site_url + "sellerregistration/sellerregistration/getStateDataOnCountry",
        data:{'countryId':countryId},
        success: function(response) {
            var data = $.parseJSON(response);
            $("#vState").replaceWith(data.dropdown);
        }
    });
});


// $("#vState").change(function(){
function city(stateId){
    // var stateId = $(this).val();
    $.ajax({
        type: "POST",
        url: site_url + "sellerregistration/sellerregistration/getCityDataOnState",
        data:{'stateId':stateId},
        success: function(response) {
            var data = $.parseJSON(response);                       
            $("#vCity").replaceWith(data.dropdown);
        }
    });
}

function load_state(country) {    
    if (country != '') {
        $.ajax({
            type: "POST",
            url: site_url + "sellerregistration/sellerregistration/getStateDataOnCountry",
            data:{'countryId':country},
            success: function(response) {
                var data = $.parseJSON(response);
                $("#vState").replaceWith(data.dropdown);
                $("#vState").val(state);
            }
        });
    }
}

function load_city(state) {    
    if (state != '') {
        $.ajax({
            type: "POST",
            url: site_url + "sellerregistration/sellerregistration/getCityDataOnState",
            data:{'stateId':state},
            success: function(response) {
                var data = $.parseJSON(response);                       
                $("#vCity").replaceWith(data.dropdown);
                $("#vCity").val(selcity);
            }
        });
    }
}

// function previewFile(previewimgid, fileid, noimagePreviewId, removeImageAction) {
//     dynamic_previewFile(previewimgid, fileid, noimagePreviewId, removeImageAction);
// }

// function removeSelectedImage(previewimgid, fileid, noimagePreviewId, removeImageAction) {
//     dynamic_removeFile(previewimgid, fileid, noimagePreviewId, removeImageAction);
// }
function previewFile(previewimgid, fileid) {
    dynamic_previewFile(previewimgid, fileid, false, false);
}

function removeSelectedImage(previewimgid, fileid) {
    dynamic_removeFile(previewimgid, fileid);
}