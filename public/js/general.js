// JavaScript Document
// Below function for the load recently viewd products - BY SA

var pid = jQuery.parseJSON(localStorage.getItem('Recent'));
if(pid != null){
	$.ajax({
		type: "POST",
		url: site_url + "home/home/getRecentPrduct",
		data:{'pid':pid},
		success: function(result) {  
			$("#recently-view").remove();
			$("#recentpr").html(result);
			$("#recentpr").owlCarousel({
				items : 2,
				// Navigation
				navigation : true,
				navigationText : ["prev","next"],
			});
		}
	});
}

// below function for the silder - start
$(document).ready(function() {
	$(".accordion #category ul li > a").each(function() {    
		if(typeof $(this).attr('class') !== "undefined") {
			var val = $(this).attr('class');           
			var total = 0;      
			$(".accordion #category ul li ul#"+val+"  li span").each(function(){
				var cur_val = $(this).html();     
				total += Number(cur_val.replace(/[()]/g,''));       
				$('.'+val+' span').html('('+total+')');      
			});
		}
	});

	$("#owl-example").owlCarousel({
		items : 4,
		// Navigation
		navigation : true,
		navigationText : ["prev","next"],
	});

	for(var i = 0; i <= 100; i++) {
		$("#owl-example"+i).owlCarousel({
			items : 3,
			// Navigation
			navigation : true,
			navigationText : ["prev","next"],
		});
	};

	$("#popular-paroduct").owlCarousel({
		items : 2,
		// Navigation
		navigation : true,
		navigationText : ["prev","next"],
	});

	$("#recently-view").owlCarousel({
		items : 2,
		// Navigation
		navigation : true,
		navigationText : ["prev","next"],
	});

	/* $("#owl-example1").owlCarousel({
		items : 3,
		// Navigation
		navigation : true,
		navigationText : ["prev","next"],
		itemsDesktop : [1199,2],
	});

	$("#owl-example2").owlCarousel({
		items : 3,
		// Navigation
		navigation : true,
		navigationText : ["prev","next"],
		itemsDesktop : [1199,2],
	});

	$("#owl-example3").owlCarousel({
		items : 3,
		// Navigation
		navigation : true,
		navigationText : ["prev","next"],
		itemsDesktop : [1199,2],
	});

	$("#owl-example4").owlCarousel({
		items : 3,
		// Navigation
		navigation : true,
		navigationText : ["prev","next"],
		itemsDesktop : [1199,2],
	});

	$("#owl-example5").owlCarousel({
		items : 4,
		// Navigation
		navigation : true,
		navigationText : ["prev","next"],
	});

	$("#owl-example6").owlCarousel({
		items : 4,
		// Navigation
		navigation : true,
		navigationText : ["prev","next"],
	});*/

	$("#footwear").mCustomScrollbar({
		scrollButtons:{enable:true},
		theme:"light-thick",
		scrollbarPosition:"outside"
	});

	$("#brands").mCustomScrollbar({
		scrollButtons:{enable:true},
		theme:"light-thick",
		scrollbarPosition:"outside"
	});

	$("#sizes").mCustomScrollbar({
		scrollButtons:{enable:true},
		theme:"light-thick",
		scrollbarPosition:"outside"
	});

	$(".prod-opt").each(function() {
		var p_opt = $(this).attr("data-value");
		$("#"+p_opt+" .brand-list").mCustomScrollbar({
			scrollButtons:{enable:true},
			theme:"light-thick",
			scrollbarPosition:"outside"
		});
	})

	$(".navbar").click(function(){
		$(".mainnav").toggleClass("open");
	});

	// Product detail page image zoom 
	$('.bxslider').bxSlider({
		minSlides: 3,
		maxSlides: 3,
		slideWidth: 110,
		slideMargin: 4,
		responsive: true,
		pager:false
	});
	$('li a.img_sub').click(function(){
		var img = $(this).attr('data-large');
		$('.product-main-image a.lightview').attr('href',img);
	});
	$('#multizoom2').addimagezoom({
		// multi-zoom: options same as for previous Featured Image Zoomer's addimagezoom unless noted as '- new'
		magnifiersize: [400,400],
		zIndex:5,
		disablewheel: true // even without variable zoom, mousewheel will not shift image position while mouse is over image (optional) - new
		//^-- No comma after last option!
	});

	$(window).scroll(function () {
		if ($(this).scrollTop() > 60) {
			$('div.header').addClass('smaller');
			$('div.nav-header').addClass('jlhide');
		} else {
			$('div.header').removeClass('smaller');
			$('div.nav-header').removeClass('jlhide');
		}
	});

	$("#myModal form#track-order-form-dr2").submit(function() {
		var retval = validateTrackOrder();
		if(retval==0) {
			return false;
		}
	});

	$(".container form#track-order-form-dr").submit(function() {
		var retval = validateTrackOrder();
		if(retval==0) {
			return false;
		}
	});
});

function validateTrackOrder() {
	rettrue = 1;
	if($("form #inputname").val()=="") {
		$("form .orderid").show();
		rettrue = 0;
	} else {
		$("form .orderid").hide();
		rettrue = 1;
	}

	if($("form #inputEmail").val()=="") {
		$("form .emailid").show();
		rettrue = 0;
	} else {
		$("form .emailid").hide();
		if(rettrue==1) {
			rettrue = 1;
		} else {
			rettrue = 0;
		}
	}

	return rettrue;
}
// below function for the silder - end

$(function() {
	if(typeof maxprice!="undefined") {
		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: maxprice,
			values: [ 0, maxprice ],
			slide: function( event, ui ) {
				$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			}
		});
		$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
			" - $" + $( "#slider-range" ).slider( "values", 1 ) );
	}
});




$("#subscribe").click(function(){
	var vEmail = $("#subscriberEmail").val();
	if(vEmail == ''){
		test_confirm('Please Enter Email Address.','YES','YES');
		return false;
	}
	if(vEmail != ''){
		$.ajax({
			type:"POST",
			url: site_url+"newslatter/newslatter/subscribeNewsLatter",          
			data:{"vEmail":vEmail},
			success:function(response){
             var dat = $.parseJSON(response);//alert(data);             
             test_confirm(dat.msg, 'Yes', 'Yes');
             $('#subscriberEmail').val(''); 
             // location.reload();

         }
     });
	}
});


function test_confirm(str, ok_callback, cancel_callback, is_reload,title) {
	if(title != '')
	{
		$('.popuptitle').html(title);
	}
	$('#test_confirm_message').html(str);
	$.fancybox({
		href: '#test_confirm',
		type: 'inline'
	});

	if (typeof (cancel_callback) != 'undefined' && typeof (ok_callback) != 'function') {
        // $('#inlinecancel').parent().removeClass('btn_cancel');
        $('#inlinecancel').hide();
        // $('.btn_cancel').hide();
    } else {
        // $('.btn_cancel').show();
        $('#inlinecancel').parent().addClass('btn_cancel');
        $('#test_confirm .btn_cancel').parent().addClass('set_prompt_btn');
        $('#inlinecancel').show();
    }
    $('#inlineok, #inlinecancel').unbind('click');
    $('#inlineok, #inlinecancel').click(function(e) {
    	e.preventDefault();
    	if (is_reload == "reload") {
    		parent.window.location.reload();
    		return false;
    	}
    	$.fancybox.close();
    	if (typeof (ok_callback) != 'undefined' && typeof (ok_callback) == 'function' && $(this).is('#inlineok')) {
    		ok_callback();
    	}

    	if (typeof (cancel_callback) != 'undefined' && typeof (cancel_callback) == 'function' && $(this).is('#inlinecancel')) {
    		cancel_callback();
    	}
    });

    if (ok_callback == true) {
    	$('.popuptitle').html('Alert');
    	$('#inlinecancel').hide();
    }
}
// $('#print_invoice').click(function(){
//     window.print();
// });
$(document).click(function(){
	jQuery('#quicksearch').html('');
})
$("#seach_text").on('change keyup paste click', function() {
	var vDetail = $(this).val();
	if(vDetail.length >= 2){
		jQuery.ajax({
			type:"POST",
			url: site_url+"productlist/productlist/quicksearch",          
			data:{"vDetail":vDetail},
			success:function(response){
				jQuery('#quicksearch').html('');
				var data = jQuery.parseJSON(response);
				if(data['settings']['success'] == 1){
					var product = data['data']['product_search'];
					var category =  data['data']['category_search'];
					var result = '';
					result +='<div class="search_main_div">';
					result += '<ul class="search_ul">';
					if(category.length > 0){
						for (var i = 0; i <category.length; i++) {
							result += '<li class="seach_category"><span class="cat_span"> </span> <a href="'+category[i]['cleanurl_category']+'">'+category[i]['mc_title']+'</a></li>';
						}
					}

					if(product.length > 0){
						result +='<li class="header_product"><span class="text">Products </span></li>';
						for (var i = 0; i <product.length; i++) {
							result +="<li class='search_product'><a href='"+product[i]['cleanurl_product']+"'><div class='product-autosuggest'><span class='image'><img src='"+product[i]['mp_default_img']+"'></span><span class='title'>"+product[i]['mp_title']+"</span><br></div></a></li>";
						}
					}
					result +='</ul></div>';
					jQuery('#quicksearch').append(result);
				}else{
					jQuery('#quicksearch').append('');
				}
			}
		});
}else
{
	jQuery('#quicksearch').html('');
}
});

function pagingAjax(url,page,tpages){
	$("#loader").show();
	$(".short-bar").hide();
	$("#catalog-listing").hide();
	$(".pagination-box").hide;
	$.ajax({
		url:url,
		data: {'page':page,'tpages':tpages},
		success:function(result){     
			$("#loader").hide();
			$(".short-bar").show();
			$("#catalog-listing").show();
			$(".pagination-box").show;
			var productlist = $(result).find("#catalog-listing").html();
			var paging = $(result).find(".pagination-box").html();
			$('#catalog-listing').html('');
			$('.pagination-box').html('');
			$('#catalog-listing').html(productlist);
			$('.pagination-box').html(paging);
		}
	});
}

function pagingAjaxBySearchFilter(url, page, tpages){
	var chkvalue = [];
	var i = 0;
	$(".accordion .accordion-body .brand-list input.chk").each(function() {
		if($(this).is(":checked")) {
			chkvalue[i] = $(this).val();
			i++;
		}
	});
	var postchkvalue = chkvalue.join(",");
	var start_price = $( "#slider-range" ).slider( "values", 0 );
	var end_price = $( "#slider-range" ).slider( "values", 1 );
	var sort_id  = $('#sort_by').val();
	var price_id = $('#price_by').val();
	var catId = $("#catId").val();
	var cid = $("#cid").val();
	var vDetail = $("#vDetail").val();
	$.ajax({
		url  : url,
		type : "POST",
		data : {'page':page, 'tpages':tpages, 'filter':postchkvalue, 'start_price':start_price, 'end_price':end_price, 'catId':catId, 'sort_id':sort_id, 'price_id':price_id, 'cid':cid, 'vDetail':vDetail},
		success:function(result){
			var data = $.parseJSON(result);
			var productlist = data.content;
			var paging = data.paging;
			var pagestring = data.pagingstring;
			$('#catalog-listing .category-products .sort_result').html('');
			$('.pagination-box div ul.pagination').html('');
			$(".pagination-box .pagination-list").html('');
			$('#catalog-listing .category-products .sort_result').html(productlist);
			$('.pagination-box div ul.pagination').html(paging);
			$(".pagination-box .pagination-list").html(data.pagingstring);
		}
	});
}

function dynamic_previewFile(previewImgId, selectedFileId, checkFileSize, checkFileType) {
	var preview = document.getElementById(previewImgId);
	var file    = document.getElementById(selectedFileId).files[0];
	var reader  = new FileReader();

	reader.onloadend = function () {
		preview.src = reader.result;
	}

	if (file) {
		if(checkFileSize===true) {
			filesizeinmb = parseFloat(file.size / (1024 * 1024)).toFixed(2);
			if(filesizeinmb > 2) {
				Project.setMessage("Please upload file upto 2MB.", "alert-error");
			}
		}
		if(checkFileType===true) {
			fileType = ['image/jpeg', 'image/png'];
			if($.inArray(file.type, fileType)==-1) {
				Project.setMessage("Please upload jpg/png file.", "alert-error");
			}
		}
		reader.readAsDataURL(file);
		document.getElementById(previewImgId).parentNode.style.display = "inline-block";
      // document.getElementById(removeImageAction).style.display = "block";
      // document.getElementById(noimagePreviewId).style.display = "none";
  } else {
  	preview.src = "";
  }
}

function dynamic_removeFile(previewImgId, selectedFileId) {
	var preview = document.getElementById(previewImgId);
	var file    = document.getElementById(selectedFileId).files[0];
	var reader  = new FileReader();

	preview.src = "";
	document.getElementById(selectedFileId).value = "";
	document.getElementById(previewImgId).parentNode.style.display = "none";
  // document.getElementById(removeImageAction).style.display = "none";
  // document.getElementById(noimagePreviewId).style.display = "block";
}

// Stick Header Menu JS Code Added By JJ



