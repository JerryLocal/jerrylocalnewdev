Project.modules.my_address = {
    init: function() {               
        $("#vPhoneNo").keyup(function(e) {
            value = $(this).val();
            if(value.substr(0,3)!="+64") {
                if(e.keyCode==8) {
                    $(this).val("+64");
                } else {
                    val = '+64'+value; // set both the value
                    $(this).val(val); // also update the form element
                }
            }
        });
        // try {
        //     var telInput = $("#vPhoneNo");
        //             // errorMsg = $("#vMobileErr"),
        //             // validMsg = $("#valid-msg");
        //     telInput.intlTelInput({
        //         defaultCountry: "NZ",
        //         onlyCountries: ["NZ"],
        //         // geoIpLookup: function (callback) {
        //         //     $.get(site_url + 'get-ipinfo.html', function () {
        //         //     }).always(function (resptxt) {
        //         //         var countryCode = 'US';
        //         //         try {
        //         //             var resp = JSON.parse(resptxt);
        //         //             var countryCode = (resp && resp.country_code) ? resp.country_code : "";
        //         //         } catch (e) {
        //         //             console.log(e);
        //         //         }
        //         //         callback(countryCode);
        //         //     });
        //         // },
        //         utilsScript: site_url + "public/js/intlTelInput/utils.js"
        //     });
        // } catch (e) {
        //     console.log(e);
        // }
        // Below code for the addredd validation

        // $.validator.addMethod("phonenumber", function (value, element) {
        //     var isValid = $("#vPhoneNo").intlTelInput("isValidNumber");
        //     return isValid;
        // }, "Please enter valid mobile number");
        $.validator.addMethod("validphnumberwithcountry", function (value, element) {
            // if no url, don't do anything
            if (value.length == 0) { return true; }

            // if user has not entered +64
            if(value.substr(0,3)!="+64") {
                val = '+64'+value; // set both the value
                $(element).val(val); // also update the form element
            }
            // now check if valid url
            return /(\+64)[0-9]+$/.test(value);
        }, "Please enter valid mobile number");

        $('#submitAddr').click(function () {
            if ($('#frmAddress').valid()) {
                $('#frmAddress').submit();
            }
        });

        $('#frmAddress').validate({
            rules:{
                'vStreet': {
                    required : true
                },
                'vArea': {                    
                    required:true
                },
                'vRegion': {
                    required : true
                },
                'vCity': {
                    required:true
                },
                'iPincode':{
                    required:true,
                    digits:true
                },
                'vCountry':{
                    required:true
                },
                'vPhoneNo':{
                    required:true,
                    // phonenumber:true
                    validphnumberwithcountry:true
                }
            },
            messages:{
                'vStreet' :{
                    required:'Please enter a Street Name'
                },
                'vArea' :{
                    required:'Please enter a Area Name'
                },
                'vRegion':{
                    required:'Please Select Region'
                },
                'vCity':{
                    required:'Please Select City'
                },
                'iPincode':{
                    required:'Please enter Pincode'                
                },
                'vCountry':{
                    required:'Please Select Country'                
                },
                'vPhoneNo':{
                    required:'Please enter PhoneNo'                
                }

            }, 
            errorPlacement: function (error, element) {
                if (element.attr("name") == "termscondition") {
                    error.appendTo("#termsconditionErr");
                } else { // default error scheme
                    error.insertAfter(element);
                }
            }, submitHandler: function (form) {
                var form_id = $(form).attr("id");
                var valid_number = $("#vPhoneNo").intlTelInput("getNumber");
                $("#vPhoneNo_hidden").val(valid_number);
                document.getElementById(form_id).submit();

            }
        }); 

    }
};

$(document).ready(function(){
    Project.modules.my_address.init();
});


$("#vCountry").change(function(){
    var countryId = $(this).val();
    $.ajax({
        type: "POST",
        url: site_url + "user/user/getStateDataOnCountry",
        data:{'countryId':countryId},
        success: function(response) {
            var data = $.parseJSON(response);
            $("#vRegion").replaceWith(data.dropdown);
        }
    });
});


// $("#vState").change(function(){
function city(stateId){
    // var stateId = $(this).val();
    $.ajax({
        type: "POST",
        url: site_url + "user/user/getCityDataOnState",
        data:{'stateId':stateId},
        success: function(response) {
            var data = $.parseJSON(response);                       
            $("#vCity").replaceWith(data.dropdown);
        }
    });
}
// });