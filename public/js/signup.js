Project.modules.signup = {
    init: function() {               

        // Below code for the signup process

        $('#signupbtn').click(function () {
            if ($('#frmSignup').valid()) {
                $('#frmSignup').submit();
            }
        });

        $.validator.addMethod("alpha", function(value, element) {            
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
         },'Enter only alpha characters');

        $('#frmSignup').validate({
            rules:{
                'fname': {
                    required : true,
                    alpha: true
                },
                'lname': {                    
                    required:true,
                    alpha: true

                },
                'vEmail': {
                    required : true,
                    email:true,
                    remote: {
                        url: site_url+"user/user/check_customer_email",
                        type: "post"
                    }
                },
                'password': {
                    required:true,
                    minlength: 6
                },
                'termscondition':{
                    required:true
                }
            },
            messages:{
                'fname' :{
                    required:'Please enter a Firstname'
                },
                'lname' :{
                    required:'Please enter a Lastname'
                },
                'vEmail':{
                    required:'Please enter a email',
                    email:'Please enter valid email address',
                    remote:'Email already exists'
                },
                'password':{
                    required:'Please enter a password'
                },
                'termscondition':{
                    required:'Please agree to our Terms of Use and Privacy Policy to proceed'                
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.attr("name") == "termscondition") {
                    error.appendTo("#termsconditionErr");
                } else { // default error scheme
                    error.insertAfter(element);
                }
            }, submitHandler: function (form) {
                var form_id = $(form).attr("id");
                document.getElementById(form_id).submit();

            }
        }); 



        // Below code for the sign in Process

        $('#signinbtn').click(function () {

            if ($('#frmSignin').valid()) {
                $('#frmSignin').submit();
            }
        });

        $('#frmSignin').validate({
            rules:{                
                'vEmail': {
                    required : true,
                    email:true
                },
                'Password': {
                    required:true,
                    remote: {
                        url: site_url+"user/user/validateUserDetail",    
                        type: "post",                    
                        data: {
                          vEmail: function() {
                             return $(".dev_SR").val();
                          }
                        }
                    }
                }
            },
            messages:{                
                'vEmail':{
                    required:'Please enter a email',
                    email:'Please enter valid email address'
                },
                'Password':{
                    required:'Please enter a password',
                    remote:'Your username or password didn\'t match'
                }
            }, 
            errorPlacement: function (error, element) {
                    error.insertAfter(element);
            }, submitHandler: function (form) {
                var form_id = $(form).attr("id");
                document.getElementById(form_id).submit();

            }
        }); 


        // Below code for the Forgot password

        $('#forgotbtn').click(function () {

            if ($('#frmforgotpsw').valid()) {
                $('#frmforgotpsw').submit();
            }
        });
        $('#frmforgotpsw').validate({
            rules:{                
                'email': {
                    required : true,
                    email:true,
                    remote: {
                        url: site_url+"user/user/check_customer_email_for_forgotpassword",
                        type: "post"
                    }
                }
            },
            messages:{                
                'email':{
                    required:'Please enter an email',
                    email:'Please enter valid email address',
                    remote:'Email is not available'
                }
            }, 
            errorPlacement: function (error, element) {
                    error.insertAfter(element);
            }, submitHandler: function (form) {
                var form_id = $(form).attr("id");
                document.getElementById(form_id).submit();

            }
        }); 
    }
};
